﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Data;
using Community.CsharpSqlite;

namespace NetMap.Sqlite
{
    /// <summary>
    /// Sqlite数据库工厂
    /// </summary>
    public class SqliteDatabaseFactory : IDatabaseFactory
    {
        const string WellknownName = "sqlite";

        string IDatabaseFactory.Name
        {
            get { return WellknownName; }
        }

        IDatabase IDatabaseFactory.Create(IConnectProperties pro)
        {
            Sqlite3.sqlite3 db = null;
            int err = Sqlite3.SQLITE_OK;
            if (string.IsNullOrEmpty(pro.Database))
            {
                //内存数据库
                err = Sqlite3.sqlite3_open(":memory:", out db);
                if (err != Sqlite3.SQLITE_OK)
                {
                    throw new Exception("空间数据库创建失败");
                }
                SqliteSpatialDatabase database = new SqliteSpatialDatabase();
                database.FactoryName = WellknownName;
                database.open(db, true);
                return database;
            }
            else
            {
                err = Sqlite3.sqlite3_open_v2(pro.Database, out db,
                    Sqlite3.SQLITE_OPEN_NOMUTEX | Sqlite3.SQLITE_OPEN_READWRITE | Sqlite3.SQLITE_OPEN_CREATE, null);
                if (err != Sqlite3.SQLITE_OK)
                {
                    throw new Exception("数据库创建失败");
                }
                SqliteDatabase database = new SqliteDatabase();
                database.FactoryName = WellknownName;
                database.open(db, true);
                return database;
            }
        }

        IDatabase IDatabaseFactory.Open(IConnectProperties pro)
        {
            Sqlite3.sqlite3 db = null;
            int err = Sqlite3.sqlite3_open_v2(pro.Database, out db,
                Sqlite3.SQLITE_OPEN_NOMUTEX | Sqlite3.SQLITE_OPEN_READWRITE | Sqlite3.SQLITE_OPEN_CREATE, null);
            if (err != Sqlite3.SQLITE_OK)
            {
                throw new ApplicationException("数据库打开失败");
            }
            SqliteDatabase database = new SqliteDatabase();
            database.FactoryName = WellknownName;
            database.open(db, false);
            return database;
        }        
    }

    /// <summary>
    /// Sqlite空间数据库工厂
    /// </summary>
    public class SqliteSpatialDatabaseFactory : ISpatialDatabaseFactory
    {
        const string WellknownName = "sqlite spatial";

        string ISpatialDatabaseFactory.Name
        {
            get
            {
                return WellknownName;
            }
        }

        ISpatialDatabase ISpatialDatabaseFactory.Create(IConnectProperties pro)
        {
            Sqlite3.sqlite3 db = null;
            int err = Sqlite3.SQLITE_OK;
            if (string.IsNullOrEmpty(pro.Database))
            {
                //内存数据库
                err = Sqlite3.sqlite3_open(":memory:", out db);
                if (err != Sqlite3.SQLITE_OK)
                {
                    throw new Exception("空间数据库创建失败");
                }
                SqliteSpatialDatabase database = new SqliteSpatialDatabase();
                database.FactoryName = WellknownName;
                database.open(db, true);
                return database;
            }
            else
            {
                err = Sqlite3.sqlite3_open_v2(pro.Database, out db,
                    Sqlite3.SQLITE_OPEN_NOMUTEX | Sqlite3.SQLITE_OPEN_READWRITE | Sqlite3.SQLITE_OPEN_CREATE, null);
                if (err != Sqlite3.SQLITE_OK)
                {
                    throw new Exception("空间数据库创建失败");
                }
                SqliteSpatialDatabase database = new SqliteSpatialDatabase();
                database.FactoryName = WellknownName;
                database.open(db, true);
                return database;
            }
        }

        ISpatialDatabase ISpatialDatabaseFactory.Open(IConnectProperties pro)
        {
            Sqlite3.sqlite3 db = null;
            int err = Sqlite3.sqlite3_open_v2(pro.Database, out db,
                Sqlite3.SQLITE_OPEN_NOMUTEX | Sqlite3.SQLITE_OPEN_READWRITE | Sqlite3.SQLITE_OPEN_CREATE, null);
            if (err != Sqlite3.SQLITE_OK)
            {
                string errMsg = "空间数据库打开失败:" + Sqlite3.sqlite3_errmsg(db);
                throw new ApplicationException(errMsg);
            }
            SqliteSpatialDatabase database = new SqliteSpatialDatabase();
            database.FactoryName = WellknownName;
            database.open(db, false);
            return database;
        }        
    }
}
