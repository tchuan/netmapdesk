﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Aga.Controls.Tree.NodeControls;
using Aga.Controls.Tree;
using NetMap.Interfaces;
using NetMap.Interfaces.Common;
using NetMap.Interfaces.Data;
using NetMap.Interfaces.Geometry;
using NetMap.Geometry;
using NetMap.UI.Properties;

namespace NetMap.UI
{   
    /// <summary>
    /// Toc 控件
    /// </summary>
    public partial class TocCtrl : UserControl
    {
        private IMapView _view = null;
        private IMapDocument _document = null;
        private NodeTextBox _layerName = null;
        private LayerNodeIcon _layerIcon = null;
        private TreeModel _layerModel = null;
        private ContextMenuStrip _layerMenu = new ContextMenuStrip();
        private ToolStripMenuItem _setCurrentLayer = new ToolStripMenuItem("设置为当前图层");
        private ToolStripMenuItem _editLayerProperties = new ToolStripMenuItem("属性");
        private ToolStripMenuItem _deleteLayer = new ToolStripMenuItem("删除");
        private LayerProperties _propertiesWindow = new LayerProperties();

        public TocCtrl()
        {
            InitializeComponent();
            InitLayerTree();
            _layerName.DrawText += new EventHandler<DrawEventArgs>(_layerName_DrawText);
            // 初始化图层右键菜单
            _layerMenu.Items.Add(_setCurrentLayer);
            _layerMenu.Items.Add(_editLayerProperties);
            _layerMenu.Items.Add(_deleteLayer);
            _setCurrentLayer.Click += new EventHandler(_setCurrentLayer_Click);
            _editLayerProperties.Click += new EventHandler(_editLayerProperties_Click);
            _deleteLayer.Click += new EventHandler(_deleteLayer_Click);
        }                       

        private void InitLayerTree()
        {
            // 图层类型
            _layerIcon = new LayerNodeIcon();
            // 图层名字
            _layerName = new NodeTextBox();
            _layerName.DataPropertyName = "Name";
            _layerName.EditEnabled = true;

            treeViewAdvLayers.NodeControls.Add(_layerIcon);
            treeViewAdvLayers.NodeControls.Add(_layerName);
            treeViewAdvLayers.AllowDrop = true;
            treeViewAdvLayers.DisplayDraggingNodes = true;
            treeViewAdvLayers.ItemDrag += new ItemDragEventHandler(treeViewAdvLayers_ItemDrag);
            treeViewAdvLayers.DragDrop += new DragEventHandler(treeViewAdvLayers_DragDrop);
            treeViewAdvLayers.DragOver += new DragEventHandler(treeViewAdvLayers_DragOver);
            treeViewAdvLayers.MouseDown += new MouseEventHandler(treeViewAdvLayers_MouseDown);
        }

        // 加载图层
        // 设置当前层
        public void SetMapView(IMapView view)
        {
            _view = view;
            IMapViewEvent viewEvent = _view as IMapViewEvent;
            // 处理文档改变事件
            viewEvent.MapDocumentChanged += new MapDocumentChangedHandler(viewEvent_MapDocumentChanged);            
        }

        /// <summary>
        /// 获取图层右键菜单
        /// </summary>
        public ContextMenuStrip LayerMenu
        {
            get { return _layerMenu; }
        }

        #region 处理MapCtrl的事件

        void viewEvent_MapDocumentChanged(IMapDocument newDocument)
        {
            _document = newDocument;
            if (_document != null)
            {
                InitList();
                // 处理MapDocument事件
                IMapDocumentEvent mapdocumentEvent = _document as IMapDocumentEvent;
                mapdocumentEvent.LayerChanged += new LayerChangedHandler(mapdocumentEvent_LayerChanged);
            }
        }

        void mapdocumentEvent_LayerChanged(ILayer layer, LayerChangeType type)
        {
            switch (type)
            {
                case LayerChangeType.Add:
                    {   
                        InitList();
                        break;
                    }
                case LayerChangeType.AllRemove:
                    {
                        _layerModel.Nodes.Clear();
                        treeViewAdvLayers.Refresh();
                        break;
                    }
                case LayerChangeType.CurrentChange:
                    {                        
                        break;
                    }
                case LayerChangeType.Move:
                    {
                        InitList();
                        break;
                    }
                case LayerChangeType.Remove:
                    {
                        InitList();
                        break;
                    }
            }
        }

        #endregion             

        #region 图层树事件处理

        void treeViewAdvLayers_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(TreeNodeAdv[]))
                && treeViewAdvLayers.DropPosition.Node != null
                && treeViewAdvLayers.DropPosition.Position != NodePosition.Inside)
            {
                e.Effect = e.AllowedEffect;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        void treeViewAdvLayers_DragDrop(object sender, DragEventArgs e)
        {
            TreeNodeAdv[] node = (TreeNodeAdv[])e.Data.GetData(typeof(TreeNodeAdv[]));
            LayerNode curNode = node[0].Tag as LayerNode;
            LayerNode dropNode = treeViewAdvLayers.DropPosition.Node.Tag as LayerNode;
            Node parent = dropNode.Parent;           
            ILayer layer = curNode.Tag as ILayer;
            ILayer refLayer = dropNode.Tag as ILayer;

            treeViewAdvLayers.BeginUpdate();
            int curindex = parent.Nodes.IndexOf(curNode);
            parent.Nodes.Remove(curNode);
            int index = parent.Nodes.IndexOf(dropNode);
            switch (treeViewAdvLayers.DropPosition.Position)
            {
                case NodePosition.After:
                    {                        
                        parent.Nodes.Insert(index + 1, curNode);
                        _document.MoveLayer(layer, refLayer, MutualRelationship.After);
                        break;
                    }
                case NodePosition.Before:
                    {
                        parent.Nodes.Insert(index, curNode);
                        _document.MoveLayer(layer, refLayer, MutualRelationship.Before);
                        break;
                    }
            }                      
            treeViewAdvLayers.EndUpdate();
        }

        void treeViewAdvLayers_ItemDrag(object sender, ItemDragEventArgs e)
        {
            treeViewAdvLayers.DoDragDropSelectedNodes(DragDropEffects.Move);
        }

        void _layerName_DrawText(object sender, DrawEventArgs e)
        {
            // 表现当前图层
            LayerNode node = e.Node.Tag as LayerNode;
            ILayer layer = node.Tag as ILayer;
            if (_document.CurrentLayer != null && _document.CurrentLayer.Tag == layer.Tag)
            {
                e.TextColor = Color.Red;
            }
            else
            {
                e.TextColor = Color.Black;
            }
        }

        void treeViewAdvLayers_MouseDown(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case System.Windows.Forms.MouseButtons.Right:
                    {
                        TreeNodeAdv node = treeViewAdvLayers.SelectedNode;
                        if (node != null)
                        {
                            LayerNode lnode = node.Tag as LayerNode;
                            ILayer layer = lnode.Tag as ILayer;
                            _layerMenu.Tag = layer;
                            _layerMenu.Show(treeViewAdvLayers, e.X, e.Y);
                        }
                        else
                        {
                            _layerMenu.Tag = null;
                        }
                        break;
                    }
            }
        } 

        #endregion        

        #region 右键菜单

        void _deleteLayer_Click(object sender, EventArgs e)
        {
            _document.DeleteLayer(_layerMenu.Tag as ILayer);
        } 

        void _editLayerProperties_Click(object sender, EventArgs e)
        {
            _propertiesWindow.SetLayer(_layerMenu.Tag as ILayer);
            _propertiesWindow.ShowDialog();
        }

        void _setCurrentLayer_Click(object sender, EventArgs e)
        {
            _document.CurrentLayer = _layerMenu.Tag as ILayer;
            treeViewAdvLayers.UpdateView();
            //当前图层变化时，地图全图范围设置为当前图层的全图范围
            _view.DisplayTransformation.MapBounds = new Envelope((_layerMenu.Tag as ILayer).Extent);
        }

        #endregion

        private void InitList()
        {
            _layerModel = new TreeModel();
            int count = _document.LayerCount;
            LayerNode currentLayerNode = null;
            LayerNode node = null;
            ILayer currentLayer = _document.CurrentLayer;
            ILayer layer = null;
            for (int i = 0; i < count; i++)
            {
                layer = _document.GetLayer(i);
                node = new LayerNode(layer);
                if (currentLayer == layer)
                {
                    currentLayerNode = node;
                }
                _layerModel.Nodes.Add(node);
            }
            treeViewAdvLayers.Model = _layerModel;
            if (currentLayerNode != null)
            {
                treeViewAdvLayers.SelectedNode = treeViewAdvLayers.FindNode(_layerModel.GetPath(currentLayerNode));
            }
            treeViewAdvLayers.Invalidate();
        }

        #region 工具条

        // 更新时重新图层信息
        private void toolStripUpdate_Click(object sender, EventArgs e)
        {
            InitList();
        }

        // 应用时直接更新地图即可
        private void toolStripApply_Click(object sender, EventArgs e)
        {
            if (_view != null)
            {
                _view.Invalidate();
            }
        }

        #endregion        
    }

    // 表现图层类型
    internal class LayerNodeIcon : NodeIcon
    {
        protected override Image GetIcon(TreeNodeAdv node)
        {
            Image img = Resources.其他;
            LayerNode layerNode = node.Tag as LayerNode;
            ILayer layer = layerNode.Tag as ILayer;
            if (layer is IFeatureLayer)
            {
                IFeatureLayer fl = layer as IFeatureLayer;
                switch (fl.FeatureType)
                {
                    case geoFeatureType.GEO_FEATURE_ANNOTATION:
                        {
                            img = Resources.注记;
                            break;
                        }
                    case geoFeatureType.GEO_FEATURE_SIMPLE:
                        {
                            switch (fl.GeometryType)
                            {
                                case geoGeometryType.GEO_GEOMETRY_POINT:
                                    {
                                        img = Resources.点;
                                        break;
                                    }
                                case geoGeometryType.GEO_GEOMETRY_LINESTRING:
                                    {
                                        img = Resources.线;
                                        break;
                                    }
                                case geoGeometryType.GEO_GEOMETRY_POLYGON:
                                    {
                                        img = Resources.面;
                                        break;
                                    }
                                case geoGeometryType.GEO_GEOMETRY_MULTIPOINT:
                                    {
                                        img = Resources.点;
                                        break;
                                    }
                                case geoGeometryType.GEO_GEOMETRY_MULTILINESTRING:
                                    {
                                        img = Resources.线;
                                        break;
                                    }
                                case geoGeometryType.GEO_GEOMETRY_MULTIPOLYGON:
                                    {
                                        img = Resources.面;
                                        break;
                                    }
                            }
                            break;
                        }
                }
            }
            else if (layer is IImageLayer)
            {
                img = Resources.影像;
            }
            return img;
        }
    }

    // Toc中的图层节点
    internal class LayerNode : Node
    {
        public LayerNode(ILayer layer)
        {
            Tag = layer;
        }

        public string Name
        {
            get
            {
                return (Tag as ILayer).Name;
            }
            set
            {
                (Tag as ILayer).Name = value;
            }
        }
    }   
}
