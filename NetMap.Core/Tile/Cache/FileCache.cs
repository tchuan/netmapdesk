﻿using NetMap.Interfaces.Tile;
using NetMap.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace NetMap.Tile.Cache
{
    /// <summary>
    /// 瓦片文件式缓存
    /// </summary>
    public class FileCache : ITileCache<byte[]>
    {
        private readonly object _syncRoot = new object();
        private readonly string _directory;
        private readonly string _format;
        private readonly TimeSpan _cacheExpireTime = TimeSpan.Zero;

        /// <summary>
        /// 如果目录不存在，则创建目录
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="format"></param>
        /// <param name="cacheExpireTime"></param>
        public FileCache(string directory, string format, TimeSpan cacheExpireTime)
        {
            _directory = directory;
            _format = format;
            _cacheExpireTime = cacheExpireTime;
            
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
        }

        /// <summary>
        /// 如果目录不存在，则创建目录
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="format"></param>
        public FileCache(string directory, string format)
            : this(directory, format, TimeSpan.Zero)
        {
        }

        #region ITileCache 成员

        public void Add(TileIndex index, byte[] tile)
        {
            lock (_syncRoot)
            {
                if (Exists(index))
                {
                    return; //ignore
                }
                string dir = GetDirectoryName(index);
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
                WriteToFile(tile, index);
            }
        }

        public void Remove(TileIndex index)
        {
            lock (_syncRoot)
            {
                if (Exists(index))
                {
                    File.Delete(GetFileName(index));
                }
            }
        }

        public byte[] Find(TileIndex index)
        {
            lock (_syncRoot)
            {
                if (!Exists(index)) return null; // to indicate not found
                using (var fileStream = new FileStream(GetFileName(index), FileMode.Open, FileAccess.Read))
                {
                    return StreamUtil.ReadFully(fileStream);
                }
            }
        }

        #endregion

        public bool Exists(TileIndex index)
        {
            if (File.Exists(GetFileName(index)))
            {
                return _cacheExpireTime == TimeSpan.Zero || (DateTime.Now - new FileInfo(GetFileName(index)).LastWriteTime) <= _cacheExpireTime;
            }
            return false;
        }

        public string GetFileName(TileIndex index)
        {
            return Path.Combine(GetDirectoryName(index), string.Format("{0}.{1}", index.Row, _format));
        }

        private string GetDirectoryName(TileIndex index)
        {
            return Path.Combine(_directory, index.Level.ToString(),index.Col.ToString());
        }

        private void WriteToFile(byte[] image, TileIndex index)
        {
            using (FileStream fileStream = File.Open(GetFileName(index), FileMode.Create))
            {
                fileStream.Write(image, 0, image.Length);
                fileStream.Flush();
                fileStream.Close();
            }
        }
    }
}
