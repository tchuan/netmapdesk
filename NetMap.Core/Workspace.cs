﻿using NetMap.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Persist;
using System.Xml;
using System.IO;
using NetMap.Interfaces.Symbol;
using NetMap.Symbol;
using NetMap.Core;

namespace NetMap
{
    /// <summary>
    /// 工作空间
    /// </summary>
    public class Workspace : IWorkspace, IFile
    {
        public string Name
        {
            get;
            set;
        }

        public ISpatialDataSource DataSource
        {
            get;
            set;
        }

        public ISymbolLibrary SymbolLibrary 
        { 
            get;
            set; 
        }

        public IMapDocumentCollection Maps
        {
            get;
            set;
        }

        public IMapViewCollection MapViews
        {
            get;
            set;
        }

        #region IFile 接口

        private const string NAME_WORKSPACE = "Workspace";
        private const string NAME_FLAG = "Flag";
        private const string NAME_VERSION = "Version";
        private const string NAME_TIME = "Time";
        private const string NAME_NAME = "Name";
        private const string NAME_DataSource = "DataSource";
        private const string NAME_SYMBOLLIBRARY = "SymbolLibrary";
        private const string NAME_MAPS = "Maps";
        private const string NAME_MAPVIEWS = "Views";

        private string _path = string.Empty;

        public string Flag
        {
            get { return Defines.FILEFLAG_PRJ; }
        }

        public int Version
        {
            get;
            set;
        }

        public DateTime Time
        {
            get;
            set;
        }

        public void Save()
        {
            SaveAs(_path);
        }

        public void Load(string path)
        {
            _path = path;
            using (TextReader reader = File.OpenText(path))
            {
                using (XmlTextReader xmlReader = new XmlTextReader(reader, null))
                {
                    while (xmlReader.Read())
                    {
                        if(xmlReader.IsStartElement())
                        {
                            switch (xmlReader.Name)
                            {
                                case NAME_WORKSPACE:
                                    {

                                        break;
                                    }
                                default:
                                    {
                                        throw new FileIOException("未知工程文件");
                                    }
                            }
                        }
                    }
                }
            }
        }

        public void SaveAs(string path)
        {
            XmlDocument document = new XmlDocument();
            XmlElement prj = document.CreateElement(NAME_WORKSPACE);
            prj.SetAttribute(NAME_VERSION, Version.ToString());
            prj.SetAttribute(NAME_TIME, Time.ToString("yy-MM-dd hh:mm:ss"));
            prj.SetAttribute(NAME_FLAG, Flag);
            prj.SetAttribute(NAME_NAME, Name);
            XmlElement datasource = document.CreateElement(NAME_DataSource);
            IPersistResource dataSourcePR = DataSource as IPersistResource;
            dataSourcePR.Save(datasource, document);
            XmlElement maps = document.CreateElement(NAME_MAPS);
            IPersistResource mapsPR = Maps as IPersistResource;
            mapsPR.Save(maps, document);
            XmlElement mapviews = document.CreateElement(NAME_MAPVIEWS);
            IPersistResource mapviewsPR = MapViews as IPersistResource;
            mapviewsPR.Save(maps, document);
            prj.AppendChild(datasource);
            prj.AppendChild(maps);
            prj.AppendChild(mapviews);
            document.AppendChild(prj);
            document.Save(path);
        }

        #endregion

        private void ReadWorkspace(XmlReader reader)
        {
            string flag = reader.GetAttribute(NAME_FLAG);
            if (flag != Defines.FILEFLAG_PRJ)
            {
                throw new FileIOException("未知工程文件");
            }
            Version = int.Parse(reader.GetAttribute(NAME_VERSION));
            Time = DateTime.Parse(reader.GetAttribute(NAME_TIME));
            Name = reader.GetAttribute(NAME_NAME);
            while (reader.Read())
            {
                if (reader.IsStartElement())
                {
                    switch (reader.Name)
                    {
                        case NAME_DataSource:
                            {
                                DataSource = new SpatialDataSource();
                                IPersistResource dataSourcePR = DataSource as IPersistResource;
                                dataSourcePR.Load(reader, null);
                                break;
                            }
                        case NAME_SYMBOLLIBRARY:
                            {
                                SymbolLibrary = new SymbolLibrary();
                                IPersistResource symbolLibraryPR = SymbolLibrary as IPersistResource;
                                symbolLibraryPR.Load(reader, null);
                                break;
                            }
                        case NAME_MAPS:
                            {
                                Maps = new MapDocumentCollection();
                                IPersistResource mapsPR = Maps as IPersistResource;
                                mapsPR.Load(reader, SymbolLibrary);
                                break;
                            }
                        case NAME_MAPVIEWS:
                            {
                                MapViews = new MapViewCollection();
                                IPersistResource mapviewsPR = MapViews as IPersistResource;
                                mapviewsPR.Load(reader, null);
                                break;
                            }
                        default:
                            {
                                throw new NotSupportedException("不支持的节点");
                            }
                    }
                }
            }
        }
    }
}
