﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Tile;

namespace NetMap.Interfaces.Tile
{
    /// <summary>
    /// 获取瓦片接口
    /// </summary>
    public interface ITileProvider
    {
        /// <summary>
        /// 获取瓦片
        /// </summary>
        /// <param name="tileInfo"></param>
        /// <returns></returns>
        byte[] GetTile(TileInfo tileInfo);
    }
}
