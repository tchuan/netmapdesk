﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Geometry
{
    /// <summary>
    /// Path节点接口
    /// </summary>
    public interface IPathPoint : IPoint
    {
        /// <summary>
        /// 进入控制点
        /// </summary>
        IPoint InPoint { get; set; }
        /// <summary>
        /// 出口控制点
        /// </summary>
        IPoint OutPoint { get; set; }
    }
}
