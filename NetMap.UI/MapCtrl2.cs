﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using NetMap.Data;
using NetMap.Geometry;
using NetMap.Interfaces;
using NetMap.Interfaces.Data;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Render;
using NetMap.Render;
/*
using StoneGround;
using StoneGround.Dialog;
*/

namespace NetMap.UI
{
    [DesignerAttribute(typeof(MapCtrlDesigner))]
    public partial class MapCtrl2 : UserControl, IMapViewEvent, IMapEditView, IMapEditViewEvent
    {
        private int _x;
        private int _y;
        private bool _zooming;
        private double _viewX;
        private double _viewY;
        private readonly IMapView _view;

        public MapCtrl2()
        {
            InitializeComponent();

            BackColor = Color.White; //默认背景色

            _renderCancel = new RenderCancel {Cancel = AbortRender};

            _view = this;
        }        

        #region Overrides

        protected IEnvelope GetEnvelope(double x, double y)
        {
            const int tolerance = 10;
            double x1 = 0, y1 = 0, x2 = 0, y2 = 0;
            _display.DisplayTransformation.ToMapPoint(x - tolerance, y - tolerance, ref x1, ref y1);
            _display.DisplayTransformation.ToMapPoint(x + tolerance, y + tolerance, ref x2, ref y2);
            return new Envelope(x1, y1, x2, y2);
        }

        protected void ShowLabel(MouseEventArgs e)
        {
            /*
            ISpatialFilter filter = new SpatialFilter();
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_NAME);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_ADDRESS);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_TEL);
            filter.Geometry = GetEnvelope(e.X, e.Y) as IGeometry;
            filter.SpatialRelationship = geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_ENVELOPEINTERSECTS;
            var featureClass = StoneGroundDatabaseService.Instance.StoneGroundFeatureClass;
            var cursor = featureClass.Search(filter);
            var feature = cursor.Next();
            if (feature != null)
            {
                var content = "名称：" + feature.GetValue(0) + Environment.NewLine +
                              "电话：" + feature.GetValue(2);
                ((IMapView) this).ShowTip(content, true);
            }
            else
            {
                ((IMapView) this).ShowTip(null, false);
            }
            */
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (ValidMouseTool())
            {
                _currentTool.OnMouseDown(e);
            }
            else
            {
                base.OnMouseDown(e);

                if (e.Button != MouseButtons.Left)
                    return;
                _x = e.X;
                _y = e.Y;
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            //2014-10-30 展示鼠标位置
            if (_displayTF != null && _status != null)
            {
                double x = 0, y = 0;
                _displayTF.ToMapPoint(e.X, e.Y, ref x, ref y);
                _status.ShowMousePosition(x, y);
            }
            //end
            ShowLabel(e);
            if (ValidMouseTool())
            {
                _currentTool.OnMouseMove(e);
            }
            else
            {
                base.OnMouseMove(e);

                _zooming = false;

                if (e.Button != MouseButtons.Left)
                    return;

                var disTrans = _view.DisplayTransformation;
                var xMapOff = (e.X - _x) / disTrans.ScaleRatio;
                var yMapOff = (e.Y - _y) / disTrans.ScaleRatio;
                var center = disTrans.VisibleMapBounds.Center;
                _view.ZoomToCenter(center.X - xMapOff, center.Y + yMapOff, disTrans.ScaleRatio);
                _x = e.X;
                _y = e.Y;
                _view.Invalidate();
            }
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            if (ValidMouseTool())
            {
                _currentTool.OnMouseUp(e);
            }
            else
            {
                base.OnMouseUp(e);

                if (e.Button != MouseButtons.Left)
                    return;
                if (e.X == _x && e.Y == _y)
                    ShowFrmInfo(e);
            }
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            if (ValidMouseTool())
            {
                _currentTool.OnMouseWheel(e);
            }
            else
            {
                base.OnMouseWheel(e);

                var document = _view.MapDocument;
                var disTrans = _view.DisplayTransformation;
                var scale = disTrans.ScaleRatio;
                if (e.Delta > 0)
                {
                    scale *= 2;
                    if (scale > document.MaxScale)
                    {
                        scale = document.MaxScale;
                    }
                }
                else
                {
                    scale *= 0.5;
                    if (scale < document.MinScale)
                    {
                        scale = document.MinScale;
                    }
                }

                if (!_zooming)
                {
                    _zooming = true;
                    disTrans.ToMapPoint(e.X, e.Y, ref _viewX, ref _viewY);
                }
                _view.ZoomToCenter(_viewX, _viewY, scale);
                _view.Invalidate();
            }
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            if (ValidMouseTool())
            {
                _currentTool.OnMouseEnter(e);
            }
            else
            {
                base.OnMouseEnter(e);
            }
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            if (ValidMouseTool())
            {
                _currentTool.OnMouseLeave(e);
            }
            else
            {
                base.OnMouseLeave(e);
            }
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (ValidMouseTool())
            {
                _currentTool.OnKeyDown(e);
            }
            else
            {
                base.OnKeyDown(e);
            }
        }

        protected override void OnKeyUp(KeyEventArgs e)
        {
            //2014-10-30 esc键清空当前地图工具
            if (e.KeyCode == Keys.Escape)
            {
                if (_currentTool != null)
                {
                    _currentTool = null;
                    if (_status != null)
                    {
                        Cursor = Cursors.Arrow; //光标还原
                        _status.ShowInfo("");   //状态清空
                        Invalidate();
                    }
                }
                else
                {
                    base.OnKeyUp(e);
                }
                return;
            }
            //end
            if (ValidMouseTool())
            {
                _currentTool.OnKeyUp(e);
            }
            else
            {
                base.OnKeyUp(e);
            }
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            if (Width == 0 || Height == 0)
            {
                return;
            }

            UpdateRenderContext();
            _displayTF.ZoomToDataBounds(_displayTF.VisibleMapBounds);

            ViewChanged(true);
            InvalidateMap(true);
        }

        #endregion

        #region IMapViewEvent 接口

        public event MapDocumentChangedHandler MapDocumentChanged;

        public event MapViewDrawHandler BeforeDraw;

        public event MapViewDrawHandler AfterDraw;

        public event FileDragDropHandler FileDragDrop;

        public event MapViewChangedHandler MapViewChanged;

        public event MapViewScaleChangedHandler MapViewScaleChanged;

        #endregion

        #region IMapEditViewEvent 接口

        public event MapToolChangeHandler MapToolChange;

        #endregion        

        #region 事件处理

        protected virtual void OnMapDocumentChanged(IMapDocument newDocument)
        {
            MapDocumentChanged?.Invoke(newDocument);
        }

        protected virtual void OnBeforeDraw(IDisplay display)
        {
            BeforeDraw?.Invoke(display);
        }

        protected virtual void OnAfterDraw(IDisplay display)
        {
            AfterDraw?.Invoke(display);
        }

        protected virtual void OnMapViewScaleChanged(double scale)
        {
            MapViewScaleChanged?.Invoke(scale);
        }

        protected virtual void OnMapToolChange()
        {
            MapToolChange?.Invoke(_currentTool);
        }

        #endregion

        private void ShowFrmInfo(MouseEventArgs e)
        {
            /*
            const int tolerance = 10;
            double x = 0, y = 0;
            var displayTranformation = _view.DisplayTransformation;
            displayTranformation.ToMapPoint(e.X, e.Y, ref x, ref y);

            var featureClass = StoneGroundDatabaseService.Instance.StoneGroundFeatureClass;
            ISpatialFilter filter = new SpatialFilter();
            filter.Geometry = new Circle(x, y, tolerance / displayTranformation.ScaleRatio);
            filter.SpatialRelationship = geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_INTERSECTS;
            var fields = featureClass.Fields;
            for (int i = 0; i < fields.Count; i++)
            {
                var field = fields.GetField(i);
                filter.Fields.Add(field.Name);
            }

            var cursor = featureClass.Search(filter);
            var feature = cursor.Next();
            cursor.Close();
            if (feature == null)
                return;
            var dialog = new FrmSGInfo2
            {
                StartPosition = FormStartPosition.CenterParent
            };
            dialog.InitInfo(feature);
            dialog.ShowDialog();
            */
        }

        private bool ValidMouseTool()
        {
            return _currentTool != null && _currentTool.Name != "道路周围料场分析";
        }
    }
}
