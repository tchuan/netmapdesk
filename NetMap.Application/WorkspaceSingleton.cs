﻿using NetMap.Interfaces;
using NetMap.Interfaces.Persist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Application
{
    public static class WorkspaceSingleton
    {
        static IWorkspace _workspace;

        /// <summary>
        /// 获取工作空间
        /// </summary>
        public static IWorkspace Workspace
        {
            get
            {
                if (_workspace != null)
                {
                    _workspace = new Workspace();
                }
                return _workspace;
            }
        }

        /// <summary>
        /// 获取和设置当前的地图视图
        /// </summary>
        public static IMapView ActiveMapView
        {
            get;
            set;
        }
    }
}
