﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Symbol;
using System.Drawing;

namespace NetMap.Symbol
{
    /// <summary>
    /// 符号虚基类
    /// </summary>
    public abstract class Symbol : ISymbol
    {
        public Symbol()
        {
            Name = "";
            Code = -1;
            ZoomWithMap = false; //默认不随地图缩放
            ReferenceScale = 1;
        }

        public string Name
        {
            get;
            set;
        }

        public int Code
        {
            get;
            set;
        }

        public abstract ISymbol Clone();

        public abstract SymbolType SymbolType { get; }

        public abstract string KindAbstract { get; }

        public bool ZoomWithMap
        {
            get;
            set;
        }

        public double ReferenceScale
        {
            get;
            set;
        }
    }

    /// <summary>
    /// 点符号虚基类
    /// </summary>
    public abstract class PointSymbol : Symbol, IPointSymbol
    {
        public PointSymbol()
        {
            Color = Color.FromArgb(255, 255, 255, 255); // 默认为白色
            XOffset = YOffset = 0;
            Angle = 0;
            Size = 10;
            Rotate = 0;
        }

        public Color Color
        {
            get;
            set;
        }

        public double XOffset
        {
            get;
            set;
        }

        public double YOffset
        {
            get;
            set;
        }

        public double Angle
        {
            get;
            set;
        }

        public double Size
        {
            get;
            set;
        }

        public double Rotate { get; set; }

        public override SymbolType SymbolType
        {
            get { return SymbolType.GEO_SYMBOL_POINT; }
        }
    }

    /// <summary>
    /// 线符号虚基类
    /// </summary>
    public abstract class LineSymbol : Symbol, ILineSymbol
    {
        public LineSymbol()
        {
            //默认首尾节点没有方向
            IsStartNodeHasDirection = false;
            IsEndNodeHasDirection = false;
            //默认不表现辅助线
            IsAssistLine = false;
            //默认节点符号无角度控制
            NodeSymbolRotateType = LineNodeSymbolRotateType.None;
        }

        public override SymbolType SymbolType
        {
            get { return SymbolType.GEO_SYMBOL_LINE; }
        }
        private IPointSymbol _startNodeSymbol = null;
        public IPointSymbol StartNodeSymbol
        {
            get
            {
                return _startNodeSymbol;
            }
            set
            {
                _startNodeSymbol = value;
            }
        }
        private IPointSymbol _endNodeSymbol = null;

        public IPointSymbol EndNodeSymbol
        {
            get
            {
                return _endNodeSymbol;
            }
            set
            {
                _endNodeSymbol=value;
            }
        }
        private IPointSymbol _nodeSymbol = null;
        public IPointSymbol NodeSymbol
        {
            get
            {
                return _nodeSymbol;
            }
            set
            {
                _nodeSymbol = value;
            }
        }

        private IPointSymbol _additionalNodeSymbol = null;
        public IPointSymbol AdditionalNodeSymbol
        {
            get
            {
                return _additionalNodeSymbol;
            }
            set
            {
                _additionalNodeSymbol = value;
            }
        }
        private IOnEdgePointSymbol _onEdgeNodeSymbol=null;
        public IOnEdgePointSymbol OnEdgeNodeSymbol
        {
            get
            {
                return _onEdgeNodeSymbol;
            }
            set
            {
                _onEdgeNodeSymbol=value;
            }
        }
        private bool _nodeAvailable = false;
        public bool NodeAvailable
        {
            get
            {
                return _nodeAvailable;
            }
            set
            {
                _nodeAvailable = value; ;
            }
        }
        private bool _additionalNodeAvailable = false;
        public bool AdditionalNodeAvailable
        {
            get
            {
                return _additionalNodeAvailable; ;
            }
            set
            {
                _additionalNodeAvailable=value;
            }
        }
        private bool _onEdgeNodeAvailable = false;
        public bool OnEdgeNodeAvailable
        {
            get
            {
                return _onEdgeNodeAvailable; ;
            }
            set
            {
                _onEdgeNodeAvailable=value;
            }
        }
        public bool IsStartNodeHasDirection { get; set; }
        public bool IsEndNodeHasDirection { get; set; }
        public bool IsAssistLine { get; set; }
        public LineNodeSymbolRotateType NodeSymbolRotateType { get; set; }
    }

    /// <summary>
    /// 填充符号基类
    /// </summary>
    public abstract class FillSymbol : Symbol, IFillSymbol
    {
        public override SymbolType SymbolType
        {
            get { return SymbolType.GEO_SYMBOL_FILL; }
        }

        public ILineSymbol OutLine
        {
            get;
            set;
        }
    }
}
