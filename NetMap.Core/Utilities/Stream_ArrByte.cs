﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace NetMap.Core
{
    /// <summary>
    /// stream和byte[]的相互转换
    /// </summary>
    public class Stream_ArrByte
    {
        public static byte[] StreamToBytes(Stream stream)
        {
            byte[] bytes=new byte[stream.Length];
            stream.Seek(0,SeekOrigin.Begin);
            stream.Read(bytes,0,bytes.Length);
            return bytes;
        }
        public static Stream BytesToStream(byte[] bytes)
        {
            Stream stream = new MemoryStream(bytes);
            return stream;
        }
    }
}
