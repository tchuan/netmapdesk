﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Data;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Symbol;
using System.Drawing;

namespace NetMap.Interfaces.Render
{
    /// <summary>
    /// 符号绘制接口
    /// </summary>
    public interface ISymbolRender
    {
        /// <summary>
        /// 设置渲染环境
        /// </summary>
        /// <param name="display"></param>
        void Setup(IDisplay display);
        /// <summary>
        /// 绘制几何图形
        /// </summary>
        /// <param name="symbol">符号</param>
        /// <param name="geometry">几何图形</param>
        void Draw(ISymbol symbol, IGeometry geometry);
        /// <summary>
        /// 绘制影像
        /// </summary>
        void Draw(IEnvelope bounds, Image image);
        /// <summary>
        /// 绘制文本
        /// </summary>
        /// <param name="symbol">文本符号</param>
        /// <param name="geometry">与注记关联的图形</param>
        /// <param name="text">注记内容</param>
        void DrawText(ITextSymbol symbol, IGeometry geometry, string text);
        /// <summary>
        /// 直接绘制图形
        /// </summary>
        /// <param name="display">绘图环境</param>
        /// <param name="symbol">符号</param>
        /// <param name="geometry">目标图形</param>
        void DirectDraw(IDisplay display, ISymbol symbol, IGeometry geometry);
        /// <summary>
        /// 直接绘制影像
        /// </summary>
        /// <param name="display">绘图环境</param>
        /// <param name="bounds">影像范围</param>
        /// <param name="image">影像</param>
        void DirectDraw(IDisplay display, IEnvelope bounds, Bitmap image);
        /// <summary>
        /// 直接绘制注记
        /// </summary>
        /// <param name="display">绘图环境</param>
        /// <param name="symbol">注记符号</param>
        /// <param name="geometry">目标图形</param>
        /// <param name="text">注记内容</param>
        void DirectDraw(IDisplay display, ITextSymbol symbol, IGeometry geometry, string text);
        /// <summary>
        /// 获取符号化范围
        /// 备注：可以用于局部刷新
        /// </summary>
        /// <param name="geometry">几何图形</param>
        /// <returns></returns>
        IEnvelope QueryBounds(ISymbol symbol, IGeometry geometry);
        /// <summary>
        /// 清理资源
        /// </summary>
        void Clear();
    }
}
