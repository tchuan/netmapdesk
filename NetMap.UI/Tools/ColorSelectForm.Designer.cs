﻿namespace NetMap.Tools
{
    partial class ColorSelectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.colorComboboxCtrl = new NetMap.UI.Ctrl.ColorCombobox();
            this.SuspendLayout();
            // 
            // colorComboboxCtrl
            // 
            this.colorComboboxCtrl.Location = new System.Drawing.Point(21, 29);
            this.colorComboboxCtrl.Name = "colorComboboxCtrl";
            this.colorComboboxCtrl.Size = new System.Drawing.Size(131, 28);
            this.colorComboboxCtrl.TabIndex = 0;
            // 
            // ColorSelectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(165, 84);
            this.Controls.Add(this.colorComboboxCtrl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ColorSelectForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "颜色选择";
            this.ResumeLayout(false);

        }

        #endregion

        public NetMap.UI.Ctrl.ColorCombobox colorComboboxCtrl;
    }
}