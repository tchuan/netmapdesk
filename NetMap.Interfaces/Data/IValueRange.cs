﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Data
{
    /// <summary>
    /// 数据范围接口
    /// </summary>
    public interface IValueRange : IEquatable<IValueRange>
    {
        /// <summary>
        /// 值是否在范围内
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        bool IsValueInRange(object value);
    }
}
