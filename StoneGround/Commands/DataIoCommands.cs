﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Application.Core;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data;
using NetMap.Interfaces.Data;
using System.Diagnostics;
using NetMap.Interfaces.Geometry;
using NetMap.Data;
using NetMap.Sqlite;

namespace StoneGround.Commands
{
    /// <summary>
    /// 数据导出
    /// </summary>
    public class DataExportCommand : AbstractCommand
    {
        public override void Run()
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "料场数据 (*.db)|*.db";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                string file = dialog.FileName;
                string srcFile = System.IO.Path.Combine(Application.StartupPath, "map", StoneGroundDatabaseService.DB_STONEGROUND);
                System.IO.File.Copy(srcFile, file, true);
                MessageService.ShowMessage("导出数据成功!", "提示");
            }
        }
    }

    /// <summary>
    /// 数据导入
    /// </summary>
    public class DataImportCommand : AbstractCommand
    {
        public override void Run()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "料场数据 (*.db)|*.db";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                string file = dialog.FileName;
                //打开数据包，检查是否正确
                try
                {
                    IConnectProperties connectProperties = new ConnectProperties();
                    connectProperties.Database = file;
                    ISpatialDatabaseFactory factory = new SqliteSpatialDatabaseFactory2();
                    ISpatialDatabase newSdb = factory.Open(connectProperties);
                    IFeatureClass stoneGroundFeatureClass = newSdb.OpenFeatureClass(StoneGroundDatabaseService.TABLE_STONEGROUND);
                    newSdb.Close();
                }
                catch (Exception ex)
                {
                    Debug.Print(ex.Message);
                    MessageService.ShowMessage("料场数据包导入失败，请确认料场数据包是否正确", "提示");
                    return;
                }
                //替换料场数据
                StoneGroundDatabaseService.Instance.Close();
                string destFile = System.IO.Path.Combine(Application.StartupPath, "map", StoneGroundDatabaseService.DB_STONEGROUND);
                System.IO.File.Copy(file, destFile, true);
                StoneGroundDatabaseService.Instance.Init();
                //刷新地图
                NetMap.Application.WorkspaceSingleton.ActiveMapView.Invalidate();
                MessageService.ShowMessage("料场数据包导入成功", "提示");
            }
        }
    }
}
