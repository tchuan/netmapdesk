﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces;
using NetMap.UI.Properties;

namespace NetMap.Tools
{
    /// <summary>
    /// 全图工具
    /// </summary>
    public class ZoomExtent : Command
    {
        public ZoomExtent()
            : base("全图")
        {            
        }

        public ZoomExtent(IMapView view)
            : base("全图", view)
        {
        }

        protected override void SetIcon()
        {
            _icon = Resources.全屏显示_Big;
        }

        /*
         * 全图工具执行时，更改地图的显示范围，并刷新地图
         */
        public override void Execute()
        {
            IDisplayTransformation disTrans = View.DisplayTransformation;
            View.ZoomToMap();
            View.Invalidate();
        }
    }
}
