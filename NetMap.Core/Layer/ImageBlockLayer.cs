﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using ProjNet.CoordinateSystems;
using NetMap.Render;
using NetMap.Geometry;
using NetMap.Interfaces;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Persist;
using System.Xml;

namespace NetMap
{
    /// <summary>
    /// 普通图片图层
    /// </summary>
    public class ImageBlockLayer : Layer, IPersistResource
    {
        private Image _image = null;
        private IEnvelope _box = null;

        public ImageBlockLayer()
        {
        }

        public ImageBlockLayer(Image img)
        {
            _image = img;
            if (_image != null)
            {
                _box = new Envelope(0, 0, img.Width, img.Height);
            }
            else
            {
                _box = new Envelope(0, 0, 100, 100); //默认大小
            }
        }

        public ImageBlockLayer(Image img, IEnvelope box)
        {
            _image = img;
            _box = box;
        }

        public override void Draw(IDisplay display)
        {
            if (_image != null)
            {
                IDisplayTransformation trans = display.DisplayTransformation;
                double x = 0, y = 0, x1 = 0, y1 = 0;
                trans.FromMapPoint(_box.XMin, _box.YMin, ref x, ref y);
                trans.FromMapPoint(_box.XMax, _box.YMax, ref x1, ref y1);
                Rectangle rect = new Rectangle((int)Math.Min(x, x1), (int)Math.Min(y, y1),
                    (int)Math.Abs(x - x1), (int)Math.Abs(y - y1));
                display.G.DrawImage(_image, rect);
            }
        }

        /// <summary>
        /// 获取和设置图片内容
        /// </summary>
        public Image Image
        {
            get { return _image; }
            set 
            { 
                _image = value;
                if (_image != null)
                {
                    _box.Update(0, 0, _image.Width, _image.Height);
                }
            }
        }

        /// <summary>
        /// 获取和设置图片范围
        /// </summary>
        public override IEnvelope Extent
        {
            get { return _box; }
        }

        public override ICoordinateSystem CoordinateSystem
        {
            get { return null; }
        }

        #region IPersistResource 成员

        public void Save(XmlElement node, XmlDocument document)
        {
            throw new NotImplementedException();
        }

        public void Load(XmlReader reader, object context)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
