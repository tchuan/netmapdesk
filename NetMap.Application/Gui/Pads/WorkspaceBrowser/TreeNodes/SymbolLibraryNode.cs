﻿using NetMap.Interfaces.Symbol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Application.WorkspaceBrowser.TreeNodes
{
    public class SymbolLibraryNode : AbstractWorkspaceBrowserTreeNode
    {
        ISymbolLibrary _symbolLibrary;

        public ISymbolLibrary SymbolLibrary
        {
            get { return _symbolLibrary; }
        }

        public SymbolLibraryNode(ISymbolLibrary library)
        {
            _symbolLibrary = library;

            ContextmenuAddinTreePath = "/Application/Pads/WorkspaceBrowser/ContextMenu/SymbolLibraryNode";

            SetIcon("WorkspaceBrowser.SymbolLibrary");
            Tag = library;
        }

        void UpdateText()
        {
            //TODO
        }

        public override object AcceptVisitor(WorkspaceBrowserTreeNodeVisitor visitor, object data)
        {
            throw new NotImplementedException();
        }
    }
}
