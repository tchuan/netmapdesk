﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetMap.UI.Ctrl
{
    /// <summary>
    /// 在工具栏上使用的LineJoinComboBox
    /// </summary>
    public class ToolStripLineJoinComboBox : ToolStripControlHost
    {
        public ToolStripLineJoinComboBox()
            : base(new LineJoinComboBox())
        {
        }

        /// <summary>
        /// 获取LineJoinComboBox
        /// </summary>
        /// <returns></returns>
        public LineJoinComboBox GetLineJoinComboBox()
        {
            return Control as LineJoinComboBox;
        }
    }
}
