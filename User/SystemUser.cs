﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace User
{
    /// <summary>
    /// 系统用户类型
    /// </summary>
    public enum SystemUserType : int
    {
        /// <summary>
        /// 维护者
        /// </summary>
        Support = 0,
        /// <summary>
        /// 管理员
        /// </summary>
        Admin = 1,
        /// <summary>
        /// 用户
        /// </summary>
        User = 2,
        /// <summary>
        /// 未知
        /// </summary>
        Unknown = 10
    }

    /// <summary>
    /// 系统用户
    /// </summary>
    public class SystemUser
    {
        /// <summary>
        /// 账号
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string Pwd { get; set; }
        /// <summary>
        /// 用户类型
        /// </summary>
        public SystemUserType UserType { get; set; }
        /// <summary>
        /// 信息
        /// </summary>
        public string Info { get; set; }
    }
}
