﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Data
{
    /// <summary>
    /// 记录集接口
    /// 备注：提供数据记录的顺序读取
    /// </summary>
    public interface IRecordset
    {
        /// <summary>
        /// 获取字段信息
        /// </summary>
        IFields Fields { get; }
        /// <summary>
        /// 获取字段值
        /// </summary>
        /// <param name="iField">字段索引</param>
        /// <returns></returns>
        object GetValue(int iField);
        /// <summary>
        /// 获取字段值
        /// </summary>
        /// <param name="field">字段名字</param>
        /// <returns></returns>
        object GetValue(string field);
        /// <summary>
        /// 是否到记录集结尾
        /// </summary>
        /// <returns></returns>
        bool IsEOF();
        /// <summary>
        /// 下一条记录
        /// </summary>
        void Next();
        /// <summary>
        /// 关闭
        /// </summary>
        void Close();
    }
}
