﻿namespace StoneGround.Dialog
{
    partial class FrmSGInfo2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.textBoxTrafficConditions = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.dateTimePickerRecordTime = new System.Windows.Forms.DateTimePicker();
            this.textBoxMiningConditions = new System.Windows.Forms.TextBox();
            this.textBoxReserves = new System.Windows.Forms.TextBox();
            this.textBoxGeologicage = new System.Windows.Forms.TextBox();
            this.textBoxStrtumNature = new System.Windows.Forms.TextBox();
            this.textBoxManageStatus = new System.Windows.Forms.TextBox();
            this.textBoxRecordStatus = new System.Windows.Forms.TextBox();
            this.textBoxPositionLat = new System.Windows.Forms.TextBox();
            this.textBoxPositionLon = new System.Windows.Forms.TextBox();
            this.textBoxTel = new System.Windows.Forms.TextBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.textBoxPitchAdhere = new System.Windows.Forms.TextBox();
            this.textBoxGill = new System.Windows.Forms.TextBox();
            this.textBoxAbrasion = new System.Windows.Forms.TextBox();
            this.textBoxPolish = new System.Windows.Forms.TextBox();
            this.textBoxCrushing = new System.Windows.Forms.TextBox();
            this.textBoxSturdiness = new System.Windows.Forms.TextBox();
            this.textBoxWaterAbsorption = new System.Windows.Forms.TextBox();
            this.textBoxDensity = new System.Windows.Forms.TextBox();
            this.textBoxCompressive = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.buttonPic1Del = new System.Windows.Forms.Button();
            this.buttonPic1Add = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.buttonPic2Del = new System.Windows.Forms.Button();
            this.buttonPic2Add = new System.Windows.Forms.Button();
            this.listView2 = new System.Windows.Forms.ListView();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.buttonApply = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(573, 599);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.textBoxTrafficConditions);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.dateTimePickerRecordTime);
            this.tabPage1.Controls.Add(this.textBoxMiningConditions);
            this.tabPage1.Controls.Add(this.textBoxReserves);
            this.tabPage1.Controls.Add(this.textBoxGeologicage);
            this.tabPage1.Controls.Add(this.textBoxStrtumNature);
            this.tabPage1.Controls.Add(this.textBoxManageStatus);
            this.tabPage1.Controls.Add(this.textBoxRecordStatus);
            this.tabPage1.Controls.Add(this.textBoxPositionLat);
            this.tabPage1.Controls.Add(this.textBoxPositionLon);
            this.tabPage1.Controls.Add(this.textBoxTel);
            this.tabPage1.Controls.Add(this.textBoxName);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage1.Size = new System.Drawing.Size(565, 569);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "基本";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // textBoxTrafficConditions
            // 
            this.textBoxTrafficConditions.Location = new System.Drawing.Point(120, 433);
            this.textBoxTrafficConditions.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxTrafficConditions.Multiline = true;
            this.textBoxTrafficConditions.Name = "textBoxTrafficConditions";
            this.textBoxTrafficConditions.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxTrafficConditions.Size = new System.Drawing.Size(436, 107);
            this.textBoxTrafficConditions.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Blue;
            this.label9.Location = new System.Drawing.Point(42, 442);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 17);
            this.label9.TabIndex = 25;
            this.label9.Text = "交通条件：";
            // 
            // dateTimePickerRecordTime
            // 
            this.dateTimePickerRecordTime.Location = new System.Drawing.Point(285, 357);
            this.dateTimePickerRecordTime.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateTimePickerRecordTime.Name = "dateTimePickerRecordTime";
            this.dateTimePickerRecordTime.Size = new System.Drawing.Size(244, 23);
            this.dateTimePickerRecordTime.TabIndex = 9;
            // 
            // textBoxMiningConditions
            // 
            this.textBoxMiningConditions.Location = new System.Drawing.Point(118, 239);
            this.textBoxMiningConditions.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxMiningConditions.Multiline = true;
            this.textBoxMiningConditions.Name = "textBoxMiningConditions";
            this.textBoxMiningConditions.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxMiningConditions.Size = new System.Drawing.Size(436, 107);
            this.textBoxMiningConditions.TabIndex = 8;
            // 
            // textBoxReserves
            // 
            this.textBoxReserves.Location = new System.Drawing.Point(118, 201);
            this.textBoxReserves.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxReserves.Name = "textBoxReserves";
            this.textBoxReserves.Size = new System.Drawing.Size(188, 23);
            this.textBoxReserves.TabIndex = 6;
            // 
            // textBoxGeologicage
            // 
            this.textBoxGeologicage.Location = new System.Drawing.Point(420, 201);
            this.textBoxGeologicage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxGeologicage.Name = "textBoxGeologicage";
            this.textBoxGeologicage.Size = new System.Drawing.Size(133, 23);
            this.textBoxGeologicage.TabIndex = 7;
            // 
            // textBoxStrtumNature
            // 
            this.textBoxStrtumNature.Location = new System.Drawing.Point(118, 163);
            this.textBoxStrtumNature.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxStrtumNature.Name = "textBoxStrtumNature";
            this.textBoxStrtumNature.Size = new System.Drawing.Size(188, 23);
            this.textBoxStrtumNature.TabIndex = 5;
            // 
            // textBoxManageStatus
            // 
            this.textBoxManageStatus.Location = new System.Drawing.Point(118, 91);
            this.textBoxManageStatus.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxManageStatus.Multiline = true;
            this.textBoxManageStatus.Name = "textBoxManageStatus";
            this.textBoxManageStatus.Size = new System.Drawing.Size(436, 62);
            this.textBoxManageStatus.TabIndex = 4;
            // 
            // textBoxRecordStatus
            // 
            this.textBoxRecordStatus.Location = new System.Drawing.Point(118, 52);
            this.textBoxRecordStatus.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxRecordStatus.Name = "textBoxRecordStatus";
            this.textBoxRecordStatus.Size = new System.Drawing.Size(188, 23);
            this.textBoxRecordStatus.TabIndex = 2;
            // 
            // textBoxPositionLat
            // 
            this.textBoxPositionLat.Location = new System.Drawing.Point(411, 395);
            this.textBoxPositionLat.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxPositionLat.Name = "textBoxPositionLat";
            this.textBoxPositionLat.Size = new System.Drawing.Size(118, 23);
            this.textBoxPositionLat.TabIndex = 11;
            // 
            // textBoxPositionLon
            // 
            this.textBoxPositionLon.Location = new System.Drawing.Point(285, 395);
            this.textBoxPositionLon.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxPositionLon.Name = "textBoxPositionLon";
            this.textBoxPositionLon.Size = new System.Drawing.Size(118, 23);
            this.textBoxPositionLon.TabIndex = 10;
            // 
            // textBoxTel
            // 
            this.textBoxTel.Location = new System.Drawing.Point(420, 52);
            this.textBoxTel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxTel.Name = "textBoxTel";
            this.textBoxTel.Size = new System.Drawing.Size(133, 23);
            this.textBoxTel.TabIndex = 3;
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(118, 14);
            this.textBoxName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(188, 23);
            this.textBoxName.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.Blue;
            this.label11.Location = new System.Drawing.Point(115, 404);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(144, 17);
            this.label11.TabIndex = 8;
            this.label11.Text = "料场位置（经度 纬度）：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Blue;
            this.label10.Location = new System.Drawing.Point(150, 365);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(116, 17);
            this.label10.TabIndex = 6;
            this.label10.Text = "料场信息录入日期：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(38, 248);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 17);
            this.label8.TabIndex = 4;
            this.label8.Text = "开采条件：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Blue;
            this.label7.Location = new System.Drawing.Point(10, 210);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 17);
            this.label7.TabIndex = 5;
            this.label7.Text = "储量（万m³）：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(314, 210);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 17);
            this.label6.TabIndex = 12;
            this.label6.Text = "所属地质年代：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(40, 171);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 17);
            this.label5.TabIndex = 13;
            this.label5.Text = "岩层性质：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(12, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 17);
            this.label4.TabIndex = 11;
            this.label4.Text = "料场经营状态：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(12, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "料场备案情况：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(342, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "联系方式：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(38, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "料场名称：";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.textBoxPitchAdhere);
            this.tabPage2.Controls.Add(this.textBoxGill);
            this.tabPage2.Controls.Add(this.textBoxAbrasion);
            this.tabPage2.Controls.Add(this.textBoxPolish);
            this.tabPage2.Controls.Add(this.textBoxCrushing);
            this.tabPage2.Controls.Add(this.textBoxSturdiness);
            this.tabPage2.Controls.Add(this.textBoxWaterAbsorption);
            this.tabPage2.Controls.Add(this.textBoxDensity);
            this.tabPage2.Controls.Add(this.textBoxCompressive);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage2.Size = new System.Drawing.Size(565, 569);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "碎石指标";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // textBoxPitchAdhere
            // 
            this.textBoxPitchAdhere.Location = new System.Drawing.Point(272, 449);
            this.textBoxPitchAdhere.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxPitchAdhere.Name = "textBoxPitchAdhere";
            this.textBoxPitchAdhere.Size = new System.Drawing.Size(149, 23);
            this.textBoxPitchAdhere.TabIndex = 9;
            // 
            // textBoxGill
            // 
            this.textBoxGill.Location = new System.Drawing.Point(272, 399);
            this.textBoxGill.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxGill.Name = "textBoxGill";
            this.textBoxGill.Size = new System.Drawing.Size(149, 23);
            this.textBoxGill.TabIndex = 8;
            // 
            // textBoxAbrasion
            // 
            this.textBoxAbrasion.Location = new System.Drawing.Point(272, 350);
            this.textBoxAbrasion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxAbrasion.Name = "textBoxAbrasion";
            this.textBoxAbrasion.Size = new System.Drawing.Size(149, 23);
            this.textBoxAbrasion.TabIndex = 7;
            // 
            // textBoxPolish
            // 
            this.textBoxPolish.Location = new System.Drawing.Point(272, 300);
            this.textBoxPolish.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxPolish.Name = "textBoxPolish";
            this.textBoxPolish.Size = new System.Drawing.Size(149, 23);
            this.textBoxPolish.TabIndex = 6;
            // 
            // textBoxCrushing
            // 
            this.textBoxCrushing.Location = new System.Drawing.Point(272, 251);
            this.textBoxCrushing.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxCrushing.Name = "textBoxCrushing";
            this.textBoxCrushing.Size = new System.Drawing.Size(149, 23);
            this.textBoxCrushing.TabIndex = 5;
            // 
            // textBoxSturdiness
            // 
            this.textBoxSturdiness.Location = new System.Drawing.Point(272, 201);
            this.textBoxSturdiness.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxSturdiness.Name = "textBoxSturdiness";
            this.textBoxSturdiness.Size = new System.Drawing.Size(149, 23);
            this.textBoxSturdiness.TabIndex = 4;
            // 
            // textBoxWaterAbsorption
            // 
            this.textBoxWaterAbsorption.Location = new System.Drawing.Point(272, 152);
            this.textBoxWaterAbsorption.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxWaterAbsorption.Name = "textBoxWaterAbsorption";
            this.textBoxWaterAbsorption.Size = new System.Drawing.Size(149, 23);
            this.textBoxWaterAbsorption.TabIndex = 3;
            // 
            // textBoxDensity
            // 
            this.textBoxDensity.Location = new System.Drawing.Point(272, 102);
            this.textBoxDensity.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxDensity.Name = "textBoxDensity";
            this.textBoxDensity.Size = new System.Drawing.Size(149, 23);
            this.textBoxDensity.TabIndex = 2;
            // 
            // textBoxCompressive
            // 
            this.textBoxCompressive.Location = new System.Drawing.Point(272, 52);
            this.textBoxCompressive.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxCompressive.Name = "textBoxCompressive";
            this.textBoxCompressive.Size = new System.Drawing.Size(149, 23);
            this.textBoxCompressive.TabIndex = 1;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.Blue;
            this.label20.Location = new System.Drawing.Point(106, 450);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(152, 17);
            this.label20.TabIndex = 6;
            this.label20.Text = "与沥青粘附性等级（级）：";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.Color.Blue;
            this.label19.Location = new System.Drawing.Point(167, 402);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(91, 17);
            this.label19.TabIndex = 7;
            this.label19.Text = "针片状（%）：";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.Color.Blue;
            this.label18.Location = new System.Drawing.Point(167, 354);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(91, 17);
            this.label18.TabIndex = 5;
            this.label18.Text = "磨耗值（%）：";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.Color.Blue;
            this.label17.Location = new System.Drawing.Point(156, 306);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(102, 17);
            this.label17.TabIndex = 3;
            this.label17.Text = "磨光值（PSV）：";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.Blue;
            this.label16.Location = new System.Drawing.Point(167, 258);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(91, 17);
            this.label16.TabIndex = 4;
            this.label16.Text = "压碎值（%）：";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.Blue;
            this.label15.Location = new System.Drawing.Point(167, 210);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(91, 17);
            this.label15.TabIndex = 10;
            this.label15.Text = "坚固性（%）：";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.Blue;
            this.label14.Location = new System.Drawing.Point(167, 161);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(91, 17);
            this.label14.TabIndex = 11;
            this.label14.Text = "吸水率（%）：";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.Blue;
            this.label13.Location = new System.Drawing.Point(166, 113);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(92, 17);
            this.label13.TabIndex = 8;
            this.label13.Text = "表观相对密度：";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Blue;
            this.label12.Location = new System.Drawing.Point(115, 65);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(143, 17);
            this.label12.TabIndex = 9;
            this.label12.Text = "单轴抗压强度（Mpa）：";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.buttonPic1Del);
            this.tabPage3.Controls.Add(this.buttonPic1Add);
            this.tabPage3.Controls.Add(this.listView1);
            this.tabPage3.Location = new System.Drawing.Point(4, 26);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(565, 569);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "现场情况";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // buttonPic1Del
            // 
            this.buttonPic1Del.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPic1Del.Image = Properties.Resources.delete16;
            this.buttonPic1Del.Location = new System.Drawing.Point(43, 523);
            this.buttonPic1Del.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonPic1Del.Name = "buttonPic1Del";
            this.buttonPic1Del.Size = new System.Drawing.Size(29, 35);
            this.buttonPic1Del.TabIndex = 2;
            this.buttonPic1Del.UseVisualStyleBackColor = true;
            // 
            // buttonPic1Add
            // 
            this.buttonPic1Add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPic1Add.Image = Properties.Resources.add16_3;
            this.buttonPic1Add.Location = new System.Drawing.Point(8, 523);
            this.buttonPic1Add.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonPic1Add.Name = "buttonPic1Add";
            this.buttonPic1Add.Size = new System.Drawing.Size(29, 35);
            this.buttonPic1Add.TabIndex = 2;
            this.buttonPic1Add.UseVisualStyleBackColor = true;
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.listView1.LargeImageList = this.imageList1;
            this.listView1.Location = new System.Drawing.Point(8, 4);
            this.listView1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(549, 511);
            this.listView1.TabIndex = 1;
            this.listView1.TileSize = new System.Drawing.Size(256, 256);
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Tile;
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(256, 256);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.buttonPic2Del);
            this.tabPage4.Controls.Add(this.buttonPic2Add);
            this.tabPage4.Controls.Add(this.listView2);
            this.tabPage4.Location = new System.Drawing.Point(4, 26);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(565, 569);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "碎石加工设备";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // buttonPic2Del
            // 
            this.buttonPic2Del.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPic2Del.Image = Properties.Resources.delete16;
            this.buttonPic2Del.Location = new System.Drawing.Point(43, 523);
            this.buttonPic2Del.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonPic2Del.Name = "buttonPic2Del";
            this.buttonPic2Del.Size = new System.Drawing.Size(29, 35);
            this.buttonPic2Del.TabIndex = 4;
            this.buttonPic2Del.UseVisualStyleBackColor = true;
            // 
            // buttonPic2Add
            // 
            this.buttonPic2Add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPic2Add.Image = Properties.Resources.add16_3;
            this.buttonPic2Add.Location = new System.Drawing.Point(8, 523);
            this.buttonPic2Add.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonPic2Add.Name = "buttonPic2Add";
            this.buttonPic2Add.Size = new System.Drawing.Size(29, 35);
            this.buttonPic2Add.TabIndex = 5;
            this.buttonPic2Add.UseVisualStyleBackColor = true;
            // 
            // listView2
            // 
            this.listView2.LargeImageList = this.imageList2;
            this.listView2.Location = new System.Drawing.Point(8, 4);
            this.listView2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(549, 511);
            this.listView2.TabIndex = 1;
            this.listView2.TileSize = new System.Drawing.Size(256, 256);
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Tile;
            // 
            // imageList2
            // 
            this.imageList2.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit;
            this.imageList2.ImageSize = new System.Drawing.Size(256, 256);
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // buttonApply
            // 
            this.buttonApply.Location = new System.Drawing.Point(457, 608);
            this.buttonApply.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(91, 44);
            this.buttonApply.TabIndex = 1;
            this.buttonApply.Text = "应用";
            this.buttonApply.UseVisualStyleBackColor = true;
            // 
            // FrmSGInfo2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 657);
            this.Controls.Add(this.buttonApply);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FrmSGInfo2";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "料场信息";
            this.TopMost = true;
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DateTimePicker dateTimePickerRecordTime;
        private System.Windows.Forms.TextBox textBoxMiningConditions;
        private System.Windows.Forms.TextBox textBoxReserves;
        private System.Windows.Forms.TextBox textBoxGeologicage;
        private System.Windows.Forms.TextBox textBoxStrtumNature;
        private System.Windows.Forms.TextBox textBoxManageStatus;
        private System.Windows.Forms.TextBox textBoxRecordStatus;
        private System.Windows.Forms.TextBox textBoxPositionLat;
        private System.Windows.Forms.TextBox textBoxPositionLon;
        private System.Windows.Forms.TextBox textBoxTel;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxTrafficConditions;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxPitchAdhere;
        private System.Windows.Forms.TextBox textBoxGill;
        private System.Windows.Forms.TextBox textBoxAbrasion;
        private System.Windows.Forms.TextBox textBoxPolish;
        private System.Windows.Forms.TextBox textBoxCrushing;
        private System.Windows.Forms.TextBox textBoxSturdiness;
        private System.Windows.Forms.TextBox textBoxWaterAbsorption;
        private System.Windows.Forms.TextBox textBoxDensity;
        private System.Windows.Forms.TextBox textBoxCompressive;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button buttonPic1Del;
        private System.Windows.Forms.Button buttonPic1Add;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button buttonApply;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button buttonPic2Del;
        private System.Windows.Forms.Button buttonPic2Add;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ColumnHeader columnHeader1;
    }
}