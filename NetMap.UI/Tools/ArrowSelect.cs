﻿using NetMap.Data;
using NetMap.Geometry;
using NetMap.Interfaces;
using NetMap.Interfaces.Data;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Render;
using NetMap.UI.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetMap.Tools
{
    /// <summary>
    /// 选择工具
    /// </summary>
    public class ArrowSelect : Tool
    {
        private int _downx = 0, _downy = 0;
        private int _movex = 0, _movey = 0;
        private bool _isDown = false;

        public ArrowSelect()
            : base("选择")
        {
        }

        public ArrowSelect(IMapEditView view)
            : base("选择", view)
        {
        }

        protected override void SetIcon()
        {
            _icon = Resources.选择;
        }

        protected override void SetCursor()
        {
            _cursor = Cursors.Arrow;
        }

        public override void GainFocus()
        {
            _isDown = false;
            base.GainFocus();
        }

        public override void LostFocus()
        {
            _isDown = false;
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (_isDown) return;
                _downx = e.X;
                _downy = e.Y;
                _isDown = true;
            }
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (_isDown)
                {
                    _movex = e.X;
                    _movey = e.Y;
                    View.Refresh();
                }
            }
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (!_isDown) return;
                _isDown = false;

                IDisplayTransformation trans = View.DisplayTransformation;
                //构建范围
                double x1 = 0, y1 = 0, x2 = 0, y2 = 0;
                trans.ToMapPoint(_downx, _downy, ref x1, ref y1);
                trans.ToMapPoint(e.X, e.Y, ref x2, ref y2);
                IEnvelope box = new Envelope(x1, y1, x2, y2);
                //构建查询器
                ISpatialFilter filter = new SpatialFilter();
                filter.Geometry = box as IGeometry;
                filter.SpatialRelationship = geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_ENVELOPEINTERSECTS;
                //选择
                IMapDocument document = View.MapDocument;
                ILayer layer = null;                
                for (int i = 0; i < document.LayerCount; i++)
                {                    
                    layer = document.GetLayer(i);
                    if(layer.Visible
                    && trans.ScaleRatio >= layer.MinScale
                    && trans.ScaleRatio <= layer.MaxScale
                    && (layer is IFeatureSelection))
                    {
                        IFeatureSelection featureSelection = layer as IFeatureSelection;
                        featureSelection.Select(filter, geoSelectionResultEnum.GEO_SELECTIONRESULT_NEW);
                    }
                }
                View.Invalidate();
            }
        }

        public override void Draw(Graphics g)
        {
            if (_isDown)
            {
                g.DrawRectangle(Pens.Green, Math.Min(_downx, _movex),
                    Math.Min(_downy, _movey), Math.Abs(_downx - _movex),
                    Math.Abs(_downy - _movey));
            }
        }
    }
}
