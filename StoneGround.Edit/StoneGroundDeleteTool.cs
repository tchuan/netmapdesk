﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap;
using NetMap.Interfaces;
using System.Windows.Forms;
using System.Drawing;
using NetMap.Interfaces.Render;
using NetMap.Geometry;
using NetMap.Interfaces.Geometry;
using NetMap.Data;
using NetMap.Interfaces.Data;
using NetMap.Application.Core;

namespace StoneGround.Edit
{
    /// <summary>
    /// 删除料场
    /// </summary>
    public class StoneGroundDeleteTool : Tool
    {
        private int _downx, _downy;
        private int _movex, _movey;
        private bool _down = false;

        public StoneGroundDeleteTool()
            : base("删除料场")
        {
        }

        public StoneGroundDeleteTool(IMapEditView view)
            : base("删除料场", view)
        {
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            //base.OnMouseDown(e);
            _downx = e.X;
            _downy = e.Y;
            _down = true;
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            //base.OnMouseMove(e);
            if (e.Button == MouseButtons.Left)
            {
                if (_down)
                {
                    _movex = e.X;
                    _movey = e.Y;
                    View.Refresh();
                }
            }
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            //base.OnMouseUp(e);
            if (e.Button == MouseButtons.Left)
            {
                _down = false;
                if (e.X == _downx || e.Y == _downy)
                {
                    return; // 呈线状的范围
                }
                IDisplayTransformation displayTranformation = View.DisplayTransformation;
                double x1 = 0, y1 = 0, x2 = 0, y2 = 0;
                displayTranformation.ToMapPoint(_downx, _downy, ref x1, ref y1);
                displayTranformation.ToMapPoint(e.X, e.Y, ref x2, ref y2);
                IEnvelope box = new Envelope(x1, y1, x2, y2);
                ISpatialFilter filter = new SpatialFilter();
                filter.Geometry = box as IGeometry;
                filter.SpatialRelationship = geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_ENVELOPEINTERSECTS;
                //选择
                IFeatureClass featureClass = StoneGroundDatabaseService.Instance.StoneGroundFeatureClass;
                IFeatureCursor cursor = featureClass.Search(filter);
                IFeature feature = cursor.Next();
                if (feature == null)
                {
                    return;
                }
                if (MessageService.AskQuestion("是否删除选中的料场？", "提示"))
                {
                    List<long> oids = new List<long>();
                    while (feature != null)
                    {
                        feature.Delete();
                        oids.Add(feature.OID);
                        feature = cursor.Next();
                    }
                    if (oids.Count > 0)
                    {
                        StringBuilder builder = new StringBuilder();
                        bool first = true;
                        foreach (long oid in oids)
                        {
                            if (first)
                            {
                                first = false;
                                builder.Append("(");
                            }
                            else
                            {
                                builder.Append(",");
                            }
                            builder.Append(oid);
                        }
                        builder.Append(")");
                        string oidsString = builder.ToString();
                        //删除现场情况                
                        string sql1 = "delete from " + StoneGroundDatabaseService.TABLE_SGPIC
                            + " where " + StoneGroundDatabaseService.FIELD_SGOID + " in " + oidsString;
                        StoneGroundDatabaseService.Instance.StoneGroundSdb.ExecuteSQL(sql1);
                        //删除碎石加工设备
                        string sql2 = "delete from " + StoneGroundDatabaseService.TABLE_SGMACHINEPIC
                            + " where " + StoneGroundDatabaseService.FIELD_SGOID + " in " + oidsString;
                        StoneGroundDatabaseService.Instance.StoneGroundSdb.ExecuteSQL(sql2);
                    }
                    View.Invalidate();
                }
            }
        }

        public override void Draw(Graphics g)
        {
            //base.Draw(g);
            if (_down)
            {
                g.DrawRectangle(Pens.Green, Math.Min(_downx, _movex),
                    Math.Min(_downy, _movey), Math.Abs(_downx - _movex),
                    Math.Abs(_downy - _movey));
            }
        }
    }
}
