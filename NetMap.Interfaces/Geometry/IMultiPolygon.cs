﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Geometry
{
    /// <summary>
    /// 多多边形接口
    /// </summary>
    public interface IMultiPolygon : IGeometry, IList<IPolygon>
    {
    }
}
