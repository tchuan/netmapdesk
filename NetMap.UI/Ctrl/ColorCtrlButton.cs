﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetMap.UI.Ctrl
{
    public partial class ColorCtrlButton : UserControl
    {
        private Color currentColor = Color.Empty;
        private Color defaultColor = Color.Red;
        public delegate void ColorChangedHandle(Color color);
        public event ColorChangedHandle ColorChangedEvent;

        public ColorCtrlButton()
        {
            InitializeComponent();
            colorbt.BackColor = defaultColor;
            colorbt.Click += new EventHandler(colorbt_Click);
        }
        public Color GetColor { get { return currentColor; } }
        public void SetDefaultColor(Color color)
        {
            defaultColor = color;
            colorbt.BackColor = defaultColor;
        }
        void colorbt_Click(object sender, EventArgs e)
        {
            ColorForm ct2 = new ColorForm(colorbt.BackColor);
            ct2.Show(colorbt);
            ct2.SelectColorChanged += new ColorForm.ActionEventHandler(ct2_SelectColorChanged);
        }

        void ct2_SelectColorChanged(Color color)
        {
            currentColor = color;
            colorbt.BackColor = currentColor;
            if (ColorChangedEvent != null) { ColorChangedEvent(color); }
        }
       
    }
}
