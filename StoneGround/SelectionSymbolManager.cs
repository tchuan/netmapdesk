﻿using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Symbol;
using NetMap.Symbol;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace StoneGround
{
    /// <summary>
    /// 选择符号管理器
    /// </summary>
    public class SelectionSymbolManager
    {
        private static SelectionSymbolManager _instance;
        public static SelectionSymbolManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SelectionSymbolManager();                    
                }
                return _instance;
            }
        }            

        private SelectionSymbolManager()
        {
        }

        /// <summary>
        /// 获取指定符号
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public ISymbol GetSymbol(geoGeometryType type)
        {
            ISymbol symbol = null;
            switch (type)
            {
                case geoGeometryType.GEO_GEOMETRY_POINT:
                case geoGeometryType.GEO_GEOMETRY_DIRECTIONPOINT:
                case geoGeometryType.GEO_GEOMETRY_MULTIPOINT:
                    {
                        IArcPointSymbol arcPointSymbol = new ArcPointSymbol();
                        arcPointSymbol.Outline = false;
                        arcPointSymbol.Fill = true;
                        arcPointSymbol.FillColor = Color.Cyan; //青色
                        arcPointSymbol.Size = 5;
                        symbol = arcPointSymbol;
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_LINESTRING:
                case geoGeometryType.GEO_GEOMETRY_MULTILINESTRING:
                case geoGeometryType.GEO_GEOMETRY_PATH:
                case geoGeometryType.GEO_GEOMETRY_POLYGON:
                case geoGeometryType.GEO_GEOMETRY_MULTIPOLYGON: 
                case geoGeometryType.GEO_GEOMETRY_RECT:
                    {
                        IStrokeSymbol strokeSymbol = new StrokeSymbol();
                        strokeSymbol.Pen.Color = Color.Cyan; //青色
                        strokeSymbol.Pen.Width = 2;
                        symbol = strokeSymbol;
                        break;
                    }
            }
            return symbol;
        }
    }
}
