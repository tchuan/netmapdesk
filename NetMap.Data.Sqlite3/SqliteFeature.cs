﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Data;
using NetMap.Interfaces.Geometry;
using Community.CsharpSqlite;
using NetMap.Geometry;
using NetMap.Interfaces;

namespace NetMap.Sqlite
{
    /// <summary>
    /// Sqlite数据库要素
    /// </summary>
    internal class SqliteFeature : SqliteRow, IFeature
    {
        protected IGeometry _geometry = null;   //图形
        protected bool _geometryModify = false; //图形改变标志
        protected SqliteFeatureClass _featureClass = null;        

        internal SqliteFeature()
        {
        }

        #region IFeature 成员

        public IGeometry Geometry
        {
            get
            {
                return GetGeometry();
            }
            set
            {
                SetGeometry(value);
            }
        }

        public virtual geoFeatureType FeatureType
        {
            get { return geoFeatureType.GEO_FEATURE_SIMPLE; }
        }

        public IFeatureClass FeatureClass
        {
            get { return _featureClass; }
        }

        #endregion   
        
        internal virtual void InitFeatureFields(SqliteFeatureClass featureClass, IFields fields)
        {
            InitFields(featureClass, fields);
            _featureClass = featureClass;
            _geometryModify = false;
        }

        internal override void InitValue(Sqlite3.Vdbe stmt)
        {
            base.InitValue(stmt);
            int count = _fields.Count;                
            additional_init(stmt, count + 1);
        }

        //附加初始化：初始化图形
        protected virtual void additional_init(Sqlite3.Vdbe stmt, int index)
        {
            byte[] data = Sqlite3.sqlite3_column_blob(stmt, index);
            bool bRecordExtent = true; //是否记录了范围
            
            //获取图形类型
            switch (_featureClass.GeometryType)
            {
                case geoGeometryType.GEO_GEOMETRY_POINT:
                    {
                        _geometry = new Point();
                        bRecordExtent = false;
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_DIRECTIONPOINT:
                    {
                        _geometry = new NetMap.Geometry.DirectionPoint();
                        bRecordExtent = false;
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_LINESTRING:
                    {
                        _geometry = new NetMap.Geometry.LineString();
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_PATH:
                    {
                        _geometry = new NetMap.Geometry.Path();
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_POLYGON:
                    {
                        _geometry = new NetMap.Geometry.Polygon();
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_RECT:
                    {
                        _geometry = new NetMap.Geometry.Envelope();
                        bRecordExtent = false;
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTIPOINT:
                    {
                        _geometry = new NetMap.Geometry.MultiPoint();
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTILINESTRING:
                    {
                        _geometry = new NetMap.Geometry.MultiLineString();
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTIPOLYGON:
                    {
                        _geometry = new NetMap.Geometry.MultiPolygon();
                        break;
                    }
            }
            _geometry.ImportFromWkb(data);
            if (bRecordExtent)
            {
                byte[] boxData = Sqlite3.sqlite3_column_blob(stmt, index + 1);
                Envelope box = new Envelope();
                box.ImportFromWkb(boxData);
                _geometry.Envelope = box;
            }
            else
            {
                _geometry.UpdateEnvelope(); //没有记录图形范围的图形需要手动更新范围
            }
        }

        /// <summary>
        /// 设置图形
        /// </summary>
        /// <param name="geometry"></param>
        protected virtual void SetGeometry(IGeometry geometry)
        {
            _geometry = geometry;
            _geometryModify = true;
        }

        /// <summary>
        /// 获取图形
        /// </summary>
        /// <returns></returns>
        protected virtual IGeometry GetGeometry()
        {
            return _geometry;
        }

        #region 插入

        protected override void insert_check()
        {
            base.insert_check();
            //检查要素
            if (_geometry == null)
            {
                throw new NullValueException("要素的图形为空");
            }
            if (_geometry.Envelope == null)
            {
                _geometry.UpdateEnvelope();
            }
        }

        protected override int insert_sql(ref Sqlite3.Vdbe stmt)
        {            
            StringBuilder builder = new StringBuilder();
            builder.Append("insert into ").Append(_featureClass._name).Append("(");
            int count = _fields.Count;
            for (int i = 0; i < count; i++)
            {
                if (i != 0)
                {
                    builder.Append(",");
                }
                builder.Append(_fields.GetField(i).Name);
            }
            if (count != 0)
            {
                builder.Append(",");
            }
            builder.Append(SpatialDbDefine.FIELD_GEOMETRY);
            if (_featureClass.GeometryType != geoGeometryType.GEO_GEOMETRY_POINT
                && _featureClass.GeometryType != geoGeometryType.GEO_GEOMETRY_RECT)
            {
                builder.Append(",").Append(SpatialDbDefine.FIELD_ENVELOPE);
            }
            builder.Append(") values(");
            for (int i = 0; i < count; i++)
            {
                if (i != 0)
                {
                    builder.Append(",");
                }
                builder.Append(":").Append(_fields.GetField(i).Name);
            }
            if (count != 0)
            {
                builder.Append(",");
            }
            builder.Append(":").Append(SpatialDbDefine.FIELD_GEOMETRY);
            if (_featureClass.GeometryType != geoGeometryType.GEO_GEOMETRY_POINT
                && _featureClass.GeometryType != geoGeometryType.GEO_GEOMETRY_RECT)
            {
                builder.Append(",:").Append(SpatialDbDefine.FIELD_ENVELOPE);
            }
            builder.Append(")");

            string sql = builder.ToString();
            string tail = "";
            int err = Sqlite3.sqlite3_prepare_v2(_featureClass._db,
                sql, sql.Length, ref stmt, ref tail);
            if (err != Sqlite3.SQLITE_OK)
            {
                Error.Error.Record(err, tail);
                return err;
            }
            IField field = null;
            for (int i = 0; i < count; i++)
            {
                field = _fields.GetField(i);
                bind_value(stmt, field.Type, ":" + field.Name, _values[i]);
            }
            bind_value(stmt, geoFieldType.GEO_GEOMETRY, ":" + SpatialDbDefine.FIELD_GEOMETRY, _geometry);
            if (_featureClass.GeometryType != geoGeometryType.GEO_GEOMETRY_POINT
                && _featureClass.GeometryType != geoGeometryType.GEO_GEOMETRY_RECT)
            {
                bind_value(stmt, geoFieldType.GEO_GEOMETRY, ":" + SpatialDbDefine.FIELD_ENVELOPE, _geometry.Envelope);
            }
            return err;
        }

        protected override void insert_over()
        {
            _geometryModify = false;
        }

        #endregion

        #region 更新

        protected override int update_check()
        {
            if (_modifies.IndexOf(true) < 0 && !_geometryModify)
            {
                Error.Error.Record(SpatialDbDefine.GIS_DBERR_NOMODIFY, "图形值未修改");
                return SpatialDbDefine.GIS_DBERR_NOMODIFY;
            }
            return Sqlite3.SQLITE_OK;
        }

        protected override int update_sql(ref Sqlite3.Vdbe stmt)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("update ").Append(_featureClass._name).Append(" set ");
            int count = _fields.Count;
            IField field = null;
            bool first = true;
            for (int i = 0; i < count; i++)
            {
                field = _fields.GetField(i);
                if (_modifies[i])
                {
                    if (first)
                    {
                        first = false;
                    }
                    else
                    {
                        builder.Append(",");
                    }
                    builder.Append(field.Name).Append("=:").Append(field.Name);
                }
            }
            if (_geometryModify)
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    builder.Append(",");
                }
                builder.Append(SpatialDbDefine.FIELD_GEOMETRY).Append("=:").Append(SpatialDbDefine.FIELD_GEOMETRY);
                if (_featureClass.GeometryType != geoGeometryType.GEO_GEOMETRY_POINT
                && _featureClass.GeometryType != geoGeometryType.GEO_GEOMETRY_RECT)
                {
                    builder.Append(",");
                    builder.Append(SpatialDbDefine.FIELD_ENVELOPE).Append("=:").Append(SpatialDbDefine.FIELD_ENVELOPE);
                }
            }
            builder.Append(" where ").Append(SpatialDbDefine.FIELD_OID);
            builder.Append("=").Append(_oid);

            string sql = builder.ToString();
            string tail = "";
            int err = Sqlite3.sqlite3_prepare_v2(_featureClass._db, sql,
                sql.Length, ref stmt, ref tail);
            if (err != Sqlite3.SQLITE_OK)
            {
                Error.Error.Record(err, tail); //记录错误信息
                return err;
            }
            for (int i = 0; i < count; i++)
            {
                if (_modifies[i])
                {
                    field = _fields.GetField(i);
                    bind_value(stmt, field.Type, ":" + field.Name, _values[i]);
                }
            }
            if (_geometryModify)
            {
                bind_value(stmt, geoFieldType.GEO_GEOMETRY, ":" + SpatialDbDefine.FIELD_GEOMETRY, _geometry);
                if (_featureClass.GeometryType != geoGeometryType.GEO_GEOMETRY_POINT
                && _featureClass.GeometryType != geoGeometryType.GEO_GEOMETRY_RECT)
                {
                    bind_value(stmt, geoFieldType.GEO_GEOMETRY, ":" + SpatialDbDefine.FIELD_GEOMETRY, _geometry.Envelope);
                }
            }
            return err;
        }

        protected override void update_over()
        {
            base.update_over();
            _geometryModify = false;
        }

        #endregion
    }
}
