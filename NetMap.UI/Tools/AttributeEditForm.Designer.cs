﻿namespace NetMap.UI.Tools
{
    partial class AttributeEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.propEditorMain = new NetMap.UI.Ctrl.Prop.PropEditor();
            this.SuspendLayout();
            // 
            // propEditorMain
            // 
            this.propEditorMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propEditorMain.Location = new System.Drawing.Point(0, 0);
            this.propEditorMain.Name = "propEditorMain";
            this.propEditorMain.Size = new System.Drawing.Size(292, 266);
            this.propEditorMain.TabIndex = 0;
            // 
            // AttributeEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.Controls.Add(this.propEditorMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "AttributeEditForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "属性";
            this.ResumeLayout(false);

        }

        #endregion

        private Ctrl.Prop.PropEditor propEditorMain;

    }
}