﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Geometry;
using ProjNet.CoordinateSystems;
using ProjNet.CoordinateSystems.Transformations;

namespace NetMap
{
    public abstract class Layer : ILayer
    {
        protected string _name = string.Empty;
        protected bool _visible = true;
        protected double _maxScale = double.MaxValue;
        protected double _minScale = double.MinValue;
        protected int _tag = Defines.INITIAL_VALUE;

        public Layer()
        {
        }

        #region ILayer 成员

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public bool Visible
        {
            get
            {
                return _visible;
            }
            set
            {
                _visible = value;
            }
        }

        public double MaxScale
        {
            get
            {
                return _maxScale;
            }
            set
            {
                _maxScale = value;
            }
        }

        public double MinScale
        {
            get
            {
                return _minScale;
            }
            set
            {
                _minScale = value;
            }
        }

        public int Tag
        {
            get
            {
                return _tag;
            }
            set
            {
                _tag = value;
            }
        }

        public abstract void Draw(IDisplay display);

        public abstract IEnvelope Extent { get; }

        public abstract ICoordinateSystem CoordinateSystem { get; } 
   
        public ICoordinateTransformation CoordinateTransformation
        {
            get;
            set;
        }

        public ICoordinateTransformation ReverseCoordinateTransformation { get; set; }

        #endregion
    }
}
