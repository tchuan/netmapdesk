﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MapDesk
{
    /// <summary>
    /// 登录配置
    /// </summary>
    [System.Serializable()]
    [System.Xml.Serialization.XmlType(AnonymousType = true)]
    [System.Xml.Serialization.XmlRoot(Namespace = "", IsNullable = false)]
    public class LoginConfig
    {
        /// <summary>
        /// 是否保持登录
        /// </summary>
        public bool IsKeepLogin
        {
            get;
            set;
        }

        /// <summary>
        /// 用户账号
        /// </summary>
        public string UserId
        {
            get;
            set;
        }

        /// <summary>
        /// 用户密码
        /// </summary>
        public string UserPwd
        {
            get;
            set;
        }
    }
}
