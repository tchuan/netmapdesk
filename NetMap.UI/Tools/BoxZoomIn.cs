﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Render;
using System.Drawing;
using NetMap.UI.Properties;
using NetMap.Interfaces;
using System.Windows.Forms;

namespace NetMap.Tools
{
    /// <summary>
    /// 拉框放大
    /// </summary>
    public class BoxZoomIn : Tool
    {
        private IDisplayTransformation _disTrans = null;

        public BoxZoomIn()
            : base("拉框放大")
        {            
        }

        public BoxZoomIn(IMapEditView view)
            : base("拉框放大",view)
        {
        }

        protected override void SetIcon()
        {
            _icon = Resources.中心放大_Big;
        }

        public override void GainFocus()
        {
            _disTrans = View.DisplayTransformation;
        }

        public override void LostFocus()
        {
            _disTrans = null;
        }

        private int _downx, _downy;
        private int _movex, _movey;
        private bool _down = false;

        public override void OnMouseDown(MouseEventArgs e)
        {
            _downx = e.X;
            _downy = e.Y;
            _down = true;
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            _down = false;
            if (e.X == _downx || e.Y == _downy)
            {
                return; // 呈线状的范围
            }
            double x = 0, y = 0;
            _disTrans.ToMapPoint(0.5 * (e.X + _downx), 0.5 * (e.Y + _downy), ref x, ref y);
            double scale = _disTrans.ScaleRatio * Math.Min(_disTrans.WindowBounds.Width / (double)Math.Abs(e.X - _downx),
                _disTrans.WindowBounds.Height / (double)Math.Abs(e.Y - _downy));
            View.ZoomToCenter(x, y, scale);
            View.Refresh();
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            if (_down)
            {
                _movex = e.X; _movey = e.Y;
                View.Refresh();
            }
        }

        public override void Draw(Graphics g)
        {
            if (_down)
            {
                g.DrawRectangle(Pens.Green, Math.Min(_downx, _movex),
                    Math.Min(_downy, _movey), Math.Abs(_downx - _movex),
                    Math.Abs(_downy - _movey));
            }
        }
    }
}
