﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

/*
 *  2014-04-24 加入Rotate，其不为符号特性，是符号根据外部对符号旋转的要求
 */

namespace NetMap.Interfaces.Symbol
{
    /// <summary>
    /// 点符号接口
    /// </summary>
    public interface IPointSymbol : ISymbol
    {
        /// <summary>
        /// 获取和设置颜色
        /// </summary>
        Color Color { get; set; }
        /// <summary>
        /// 获取和设置符号化位置相对对象坐标的水平偏移
        /// </summary>
        double XOffset { get; set; }
        /// <summary>
        /// 获取和设置符号化位置相对对象坐标的垂直偏移
        /// </summary>
        double YOffset { get; set; }
        /// <summary>
        /// 获取和设置符号本身的旋转角度（以°为单位）
        /// </summary>
        double Angle { get; set; }
        /// <summary>
        /// 获取和设置符号大小
        /// </summary>
        double Size { get; set; }
        /// <summary>
        /// 获取和设置旋转角度（以°为单位）
        /// </summary>
        double Rotate { get; set; }
    }
}
