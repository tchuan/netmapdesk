﻿using NetMap.Geometry;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Symbol;
using NetMap.Symbol;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace NetMap
{
    /// <summary>
    /// 水印图层
    /// </summary>
    public class WaterMarkLayer : Layer
    {
        private ITextSymbol _textSymbol;

        /// <summary>
        /// 获取和设置文本符号
        /// </summary>
        public ITextSymbol TextSymbol
        {
            get
            {
                return _textSymbol;
            }
            set
            {
                _textSymbol = value;
            }
        }

        /// <summary>
        /// 获取和设置水印内容
        /// </summary>
        public string Content
        {
            get;
            set;
        }

        /// <summary>
        /// 获取和设置格网大小
        /// </summary>
        public int Grid { get; set; }

        public WaterMarkLayer()
        {
            _textSymbol = new TextSymbol()
            {
                Color = Color.FromArgb(128, 255, 0, 0),
                Height = 20,
                Width = 20,
                Angle = 45,
                Font = "Impact"
            };
            Content = "NETMAP";
            Grid = 200;
        }

        #region ILayer 成员

        public override void Draw(IDisplay display)
        {
            if (CoordinateTransformation != null)
            {
                //不支持影像的投影变换
                return;
            }
            ISymbolRender render = display.SymbolRender;
            render.Setup(display);
            IEnvelope box = display.DisplayTransformation.WindowBounds;
            List<IPoint> pts = CalculateWarterMarks(box, box.Center, Grid);
            foreach (IPoint point in pts)
            {
                render.DirectDraw(display, _textSymbol, point, Content);
            }
        }

        public override ProjNet.CoordinateSystems.ICoordinateSystem CoordinateSystem
        {
            get
            {
                //TODO 支持坐标系统
                return null;
            }
        }

        //不占范围
        public override IEnvelope Extent
        {
            get { return null; }
        }

        #endregion

        private List<IPoint> CalculateWarterMarks(IEnvelope box, IPoint center, double grid)
        {
            List<IPoint> pts = new List<IPoint>();

            double width = box.Width * 0.5;
            double height = box.Height * 0.5;
            int wnumber = (int)(width / grid) + 1; //Modify 2014-05-28 向外扩充，填满图面
            int hnumber = (int)(height / grid) + 1;
            for (int i = -wnumber; i <= wnumber; i++)
            {
                for (int j = -hnumber; j <= hnumber; j++)
                {
                    double x = center.X + i * grid;
                    double y = center.Y + j * grid;
                    pts.Add(new NetMap.Geometry.Point(x, y));
                }
            }

            return pts;
        }
    }
}
