﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Geometry;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.SpatialAnalysis;

namespace NetMap.Core.SpatialAnalysis
{
    /// <summary>
    /// 
    /// </summary>
    public class SpatialAnalysis : ISpatialAnalysis
    {
        #region Fields

        private IGeometry _geometry = null;

        #endregion

        #region ISpatialAnalysis Members

        /// <summary>
        /// 参考图形，目前参考图形为简单多边形
        /// </summary>
        public IGeometry Geometry
        {
            get
            {
                
                return _geometry;
            }
            set
            {
                _geometry=value;
                _geometry.UpdateEnvelope();
                
            }
        }
        /// <summary>
        /// 参考图形与给定图形的空间关系
        /// 是否相交
        /// </summary>
        /// <param name="geometry">给定图形</param>
        /// <returns></returns>
        public bool Evaluate(IGeometry geometry)
        {
            if (_geometry == null || geometry == null) return false;
            if (!_geometry.Envelope.IsIntersect(geometry.Envelope)) return false;
            else
            {
                
                IPolygon  polgon = new Polygon((_geometry as IMultiLineString) as List<ILineString>,null);
                if (geometry is IPoint)
                {
                    return isPointInPolygonByRay(geometry as IPoint, polgon);
                }
                else if (geometry is IDirectionPoint)
                {
                    IDirectionPoint tempt = geometry as IDirectionPoint;
                    IPoint pt = new Point(tempt.X,tempt.Y);
                    return isPointInPolygonByRay(pt, polgon);
                }
                else if (geometry is IMultiPoint)
                {
                    foreach (IPoint pt in (geometry as IMultiPoint))
                    {
                        if (isPointInPolygonByRay(pt, polgon))
                        {
                            return true;
                        }
                    }
                }
                else if (geometry is ILineString)
                {
                    return isLineInPolygon(geometry as ILineString,polgon);
                }
                else if (geometry is IMultiLineString)
                {
                    foreach (ILineString line in (geometry as IMultiLineString))
                    {
                        
                        if (isLineInPolygon(line,polgon )) return true;
                    }
                }
                else if (geometry is IPolygon)
                {
                    isPolygonInPolygon(geometry as IPolygon, polgon);
                }
                else if (geometry is IMultiPolygon)
                {
                    foreach (IPolygon p in (geometry as IMultiPolygon))
                    {
                        if (isPolygonInPolygon(p, polgon))
                            return true;
                    }
                }
                else
                {
                    throw new Exception("图形不存在！");
                }
            }
            return false;
        }

        #endregion

        #region Private Members

        /// <summary>
        /// 线和面是否相交
        /// </summary>
        /// <param name="line"></param>
        /// <param name="reference"></param>
        /// <returns></returns>
        private bool isLineInPolygon(ILineString line, IPolygon reference)
        {
            foreach (IPoint pt in line)
            {
                if (isPointInPolygonByRay(pt, reference)) return true;
            }
            return false;
        }

        /// <summary>
        ///  面和面是否相交
        /// </summary>
        /// <param name="target"></param>
        /// <param name="reference">参考多边形 简单多边形</param>
        /// <returns></returns>
        private bool isPolygonInPolygon(IPolygon target, IPolygon reference)
        {
            bool tem1 = false;
            bool tem2 = false;
            foreach (IPoint pt in target.ExteriorRing)
            {
                if (isPointInPolygonByRay(pt, _geometry as IPolygon))
                {
                    tem1 = true;
                    break;
                }
            }
            foreach (ILineString lineString in target.InteriorRings)
            {
                if (tem2) break;
                foreach (IPoint pt in lineString)
                {
                    if (isPointInPolygonByRay(pt, reference))
                    { tem2 = true; break; }
                }
            }

            if (tem1 == true && tem2 == false) return true;
            return false;
        }

        /// <summary>
        /// 点是否在多边形内部
        /// 
        /// 射线法
        /// </summary>
        /// <param name="pt"></param>
        /// <param name="polygon">简单多边形</param>
        /// <returns></returns>
        private bool isPointInPolygonByRay(IPoint p, IPolygon polygon)
        {
            int i;
            int i1;             // point index; i1 = i-1
            int crossings = 0;  // number of segment/ray crossings
            if (polygon.ExteriorRing == null) return false;
            int nPts = polygon.ExteriorRing.Count;

            /*
            *  For each segment l = (i-1, i), see if it crosses ray from test point in positive x direction.
            */
            for (i = 1; i < nPts; i++)
            {
                i1 = i - 1;
                IPoint p1 = polygon.ExteriorRing[i];
                IPoint p2 = polygon.ExteriorRing[i1];

                if(intersect3(p1,p2,p,new Point(double.MaxValue,p.Y)))
                    crossings++;
            }

            /*
            *  p is inside if number of crossings is odd.
            */
            if ((crossings % 2) == 1)
                return true;
            else return false;

        }

        /// <summary>
        /// 两线段是否相交
        /// </summary>
        /// <param name="aa"></param>
        /// <param name="bb"></param>
        /// <param name="cc"></param>
        /// <param name="dd"></param>
        /// <returns></returns>
        private bool intersect3(IPoint aa, IPoint bb, IPoint cc, IPoint dd)
        {
            double delta = determinant(bb.X - aa.X, cc.X - dd.X, bb.Y - aa.Y, cc.Y - dd.Y);
            if (delta <= (1e-6) && delta >= -(1e-6))  // delta=0，表示两线段重合或平行
            {
                return false;
            }
            double namenda = determinant(cc.X - aa.X, cc.X - dd.X, cc.Y - aa.Y, cc.Y - dd.Y) / delta;
            if (namenda > 1 || namenda < 0)
            {
                return false;
            }
            double miu = determinant(bb.X - aa.X, cc.X - aa.X, bb.Y - aa.Y, cc.Y - aa.Y) / delta;
            if (miu > 1 || miu < 0)
            {
                return false;
            }
            return true;
        }


        /// <summary>
        /// 行列式
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <param name="v3"></param>
        /// <param name="v4"></param>
        /// <returns></returns>
        private double determinant(double v1, double v2, double v3, double v4)
        {
            return (v1 * v3 - v2 * v4);
        }

        /// <summary>
        /// 点是否在多边形内 
        ///  如果点在边上 也算在内部
        /// 内角和法
        /// </summary>
        /// <param name="pt"></param>
        /// <param name="polygon">简单多边形</param>
        /// <returns></returns>
        private bool isPointInPolygon(IPoint pt ,IPolygon polygon)
        {
            if (polygon == null) { throw new Exception("多边形不存在！"); }
            ILineString lineString = (polygon.ExteriorRing == null || polygon.ExteriorRing.Count <= 0) ?  polygon.InteriorRings[0]:polygon.ExteriorRing;
            if (lineString.Count < 3) { throw new Exception("多边形不存在！"); }//多边形不存在
            double temangle = 0;//记录角度和0
            bool result = false;
            double linshi=chacheng(pt,lineString[0],lineString[1]);
            temangle+=linePtAngle(pt,lineString[0],lineString[1]);
            for (int i = 1; i < lineString.Count - 1; i++)
            {
                if (linshi * chacheng(pt, lineString[i], lineString[i + 1]) < 0)
                {
                    temangle += (-linePtAngle(pt, lineString[i], lineString[i + 1]));
                }
                else
                {
                    temangle += linePtAngle(pt, lineString[i], lineString[i + 1]);
                }
            }
            IPoint pt1 = lineString[lineString.Count - 1];
            IPoint pt2=lineString[0];
            if (linshi * chacheng(pt, pt1, pt2) < 0)
            {
                temangle+=(-linePtAngle(pt,pt1,pt2));
            }
            else
            {
                temangle += linePtAngle(pt,pt1,pt2);
            }

            if (temangle > 355 && temangle < 365)
            {
                result = true;
            }
            else if (temangle > -0.5 && temangle < 0.5)
            {
                result = false;
            }
            else result = true;

            return result;
        }

        /// <summary>
        /// pt和pt1、pt2组成的夹角 
        /// </summary>
        /// <param name="pt"></param>
        /// <param name="pt1"></param>
        /// <param name="pt2"></param>
        /// <returns></returns>
        private double linePtAngle(IPoint pt, IPoint pt1,IPoint pt2)
        {
            return (Math.Acos((Math.Pow(distance(pt, pt1), 2) + Math.Pow(distance(pt, pt2), 2 - Math.Pow(distance(pt1,pt2), 2))
               /(distance(pt,pt1)*distance(pt,pt2)*2))));
        }

        /// <summary>
        /// 两点之间的距离
        /// </summary>
        /// <param name="pt1"></param>
        /// <param name="pt2"></param>
        /// <returns></returns>
        private double distance(IPoint pt1, IPoint pt2)
        {
            return (Math.Sqrt(Math.Pow((pt1.X - pt2.X), 2) + Math.Pow((pt1.Y - pt2.Y), 2)));
        }

        private double chacheng(IPoint pt,IPoint pt1,IPoint pt2)
        {
            return ((pt1.X - pt.X) * (pt2.Y - pt.Y) - (pt2.X - pt.X) * (pt1.Y - pt.Y));
        }

        #endregion
    }
}
