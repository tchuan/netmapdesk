﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;
using System.Collections;
using System.IO;
using NetMap.Core;

namespace NetMap.Geometry
{
    public class LineString : Geometry, ILineString
    {
        private IList<IPoint> _pts = null;

        /// <summary>
        /// 空构造
        /// </summary>
        public LineString()
        {
            _pts = new List<IPoint>();
        }

        /// <summary>
        /// 构造多段线
        /// </summary>
        /// <param name="pts"></param>
        public LineString(IList<IPoint> pts)
        {
            _pts = pts;
        }

        /// <summary>
        /// 带完整数据的构造
        /// </summary>
        /// <param name="pts"></param>
        /// <param name="box"></param>
        public LineString(IList<IPoint> pts, IEnvelope box)
        {
            _pts = pts;            
            _box = new Envelope(box);
        }

        public override geoGeometryType GeometryType
        {
            get { return geoGeometryType.GEO_GEOMETRY_LINESTRING; }
        }

        public override void UpdateEnvelope()
        {
            if (_pts.Count == 0)
            {
                _box = null;
                return;
            }
            foreach (IPoint point in _pts)
            {
                point.UpdateEnvelope();
            }
            if (_box == null)
            {
                _box = new Envelope(_pts[0]);
            }
            else
            {
                _box.Update(_pts[0].Envelope);
            }
            foreach (IPoint point in _pts)
            {
                _box.Union(point);
            }
        }

        #region ILineString 成员

        public IPoint StartPoint
        {
            get 
            {
                if (_pts.Count > 0)
                {
                    return _pts[0];
                }
                else
                {
                    return null;
                }
            }
        }

        public IPoint EndPoint
        {
            get 
            {
                if (_pts.Count > 0)
                {
                    return _pts[_pts.Count - 1];
                }
                else
                {
                    return null;
                }
            }
        }

        #endregion

        #region IList<IPoint> 成员

        public int IndexOf(IPoint item)
        {
            return _pts.IndexOf(item);
        }

        public void Insert(int index, IPoint item)
        {
            _pts.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            _pts.RemoveAt(index);
        }

        public IPoint this[int index]
        {
            get
            {
                return _pts[index];
            }
            set
            {
                _pts[index] = value;
            }
        }

        #endregion

        #region ICollection<IPoint> 成员

        public void Add(IPoint item)
        {
            _pts.Add(item);
        }

        public void Clear()
        {
            _pts.Clear();
        }

        public bool Contains(IPoint item)
        {
            return _pts.Contains(item);
        }

        public void CopyTo(IPoint[] array, int arrayIndex)
        {
            _pts.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _pts.Count; }
        }

        public bool IsReadOnly
        {
            get { return true; }
        }

        public bool Remove(IPoint item)
        {
            return _pts.Remove(item);
        }

        #endregion

        #region IEnumerable<IPoint> 成员

        public IEnumerator<IPoint> GetEnumerator()
        {
            foreach (IPoint point in _pts)
            {
                yield return point;
            }
        }

        #endregion

        #region IEnumerable 成员

        IEnumerator IEnumerable.GetEnumerator()
        {
            foreach (IPoint point in _pts)
            {
                yield return point;
            }
        }

        #endregion
        
        public override IGeometry CloneGeometry()
        {
            ILineString geometry = new LineString(_pts, this.Envelope);
            return geometry;
        }

        public override byte[] ExportToWkb()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    writer.Write(_pts.Count);
                    foreach (IPoint point in _pts)
                    {
                        point.Export(writer);
                    }
                }
                return stream.ToArray();
            }
        }

        public override string ExportToWkt()
        {
            throw new NotImplementedException();
        }

        //认为多段线内全部是Point
        public override void ImportFromWkb(byte[] data)
        {
            using (MemoryStream stream = new MemoryStream(data))
            {
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    int nCount = reader.ReadInt32();
                    for (int i = 0; i < nCount; i++)
                    {
                        IPoint point = new Point();
                        point.Import(reader);
                        _pts.Add(point);
                    }
                }
            }
        }

        public override void ImportFromWkt(string data)
        {
            throw new NotImplementedException();
        }

        public override void Export(BinaryWriter writer)
        {
            writer.Write(_pts.Count);
            foreach (IPoint point in _pts)
            {
                point.Export(writer);
            }
        }

        //导入时认为都是普通点
        public override void Import(BinaryReader reader)
        {
            int count = reader.ReadInt32();
            for (int i = 0; i < count; i++)
            {
                IPoint point = new Point();
                point.Import(reader);
                _pts.Add(point);
            }
        }
    }
}
