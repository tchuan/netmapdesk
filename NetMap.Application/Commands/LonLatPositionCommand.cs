﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces;
using System.Windows.Forms;
using NetMap.Application.Gui.Dialogs;
using NetMap.Application.Gui.Pads;

namespace NetMap.Application.Commands
{
    class LonLatPositionCmd : Command
    {
        public LonLatPositionCmd()
            : base("经纬度精确定位")
        {
        }

        public LonLatPositionCmd(IMapView view)
            : base("经纬度精确定位", view)
        {
        }

        public override void Execute()
        {
            //base.Execute();
            LonLatInput input = new LonLatInput();
            input.StartPosition = FormStartPosition.CenterParent;
            if (input.ShowDialog(NetMap.Application.Gui.WorkbenchSingleton.MainWin32Window) == DialogResult.OK)
            {
                View.ZoomToCenter(input.Longitude, input.Latitude, View.DisplayTransformation.ScaleRatio);
                View.Invalidate();
            }
        }
    }

    /// <summary>
    /// 经纬度精确定位
    /// </summary>
    public class LonLatPositionCommand : MapCommand
    {
        public LonLatPositionCommand()
        {
            InnerCmd = new LonLatPositionCmd();
        }
    }
}
