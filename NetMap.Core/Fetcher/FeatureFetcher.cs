﻿using System;
using System.Collections.Generic;
using System.Linq;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Data;
using NetMap.Interfaces.Render;
using NetMap.Data;

/*
 * 2013-6-9：修改加入NetMap.Core
 */

namespace NetMap.Fetcher
{
    /// <summary>
    /// 要素获取
    /// </summary>
    class FeatureFetcher
    {
        private readonly IDisplayTransformation trans;
        private readonly DataArrivedDelegate dataArrived;
        private readonly IFeatureClass provider;
        private readonly long timeOfRequest;

        internal delegate void DataArrivedDelegate(IEnumerable<IFeature> features, object state = null);

        public FeatureFetcher(IDisplayTransformation trans,
            IFeatureClass provider, DataArrivedDelegate dataArrived, 
            long timeOfRequest = default(long))
        {
            this.dataArrived = dataArrived;
            this.trans = trans;
            this.provider = provider;
            this.timeOfRequest = timeOfRequest;
        }

        public void FetchOnThread(object state)
        {
            lock (provider)
            {
                //获取可见范围内的所有要素
                IEnvelope visibleBox = trans.VisibleMapBounds;
                ISpatialFilter filter = new SpatialFilter();
                filter.Geometry = visibleBox as IGeometry;
                filter.SpatialRelationship = geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_INTERSECTS;
                IFeatureCursor cursor = provider.Search(filter);
                List<IFeature> features = new List<IFeature>();
                IFeature feature = cursor.Next();
                while (feature != null)
                {
                    features.Add(feature);
                    feature = cursor.Next();
                }             
                // 
                if (dataArrived != null) dataArrived(features, timeOfRequest);
            }
        }
    }
}
