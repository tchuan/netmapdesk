﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Data
{
    /// <summary>
    /// 要素游标接口
    /// </summary>
    public interface IFeatureCursor
    {
        /// <summary>
        /// 获取字段集合
        /// </summary>
        /// <returns></returns>
        IFields GetFields();
        /// <summary>
        /// 获取下一个要素
        /// </summary>
        /// <returns></returns>
        IFeature Next();
        /// <summary>
        /// 游标置于选择集首位
        /// </summary>
        void Reset();
        /// <summary>
        /// 关闭
        /// </summary>
        void Close();
        /// <summary>
        /// 获取游标所指向的要素集
        /// </summary>
        IFeatureClass FeatureClass { get; }
    }
}
