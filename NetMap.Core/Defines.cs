﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap
{
    /// <summary>
    /// 定义
    /// </summary>
    public static class Defines
    {
        /// <summary>
        /// 系统精度
        /// </summary>
        public const double EPSILON = 1e-6;
        /// <summary>
        /// 查找不到
        /// </summary>
        public const int CANNOT_FIND = -1;
        /// <summary>
        /// 无意义的初始值
        /// </summary>
        public const int INITIAL_VALUE = 0;

        #region 符号子类型名字
        public const string SYMBOL_POINT_FONT = "字体点符号";
        public const string SYMBOL_POINT_IMAGE = "图片点符号";
        public const string SYMBOL_LINE = "线符号";
        public const string SYMBOL_FILL_SOLID = "颜色填充符号";
        public const string SYMBOL_FILL_HATCH = "晕线填充符号";
        public const string SYMBOL_FILL_TEXTURE = "纹理填充符号";
        public const string SYMBOL_FILL_LINEARGRADIENT = "渐变填充符号";
        #endregion

        #region Windows消息
        public const int WM_ACTIVATEAPP = 0x001C; 
        public const int WM_NCPAINT = 0x0085;
        #endregion

        #region Windows窗口样式
        public const int GWL_STYLE = -16;

        public const int WS_CAPTION = 0x00C00000;
        public const int WS_BORDER = 0x00800000;        
        public const int WS_VISIBLE = 0x10000000;
        public const int WS_CHILDWINDOW = 0x40000000;
        public const int WS_CLIPSIBLINGS = 0x04000000;
        public const int WS_CLIPCHILDREN = 0x02000000;
        public const long WS_THICKFRAME = 0x00040000;
        public const long WS_OVERLAPPED = 0x00000000;
        #endregion

        #region 序列化

        /// <summary>
        /// 工作空间文件
        /// </summary>
        public const string FILEFLAG_PRJ = "nmprj";
        /// <summary>
        /// 符号库文件
        /// </summary>
        public const string FILEFLAG_SYMBOLLIB = "nmsym";
        /// <summary>
        /// 瓦片包
        /// </summary>
        public const string FILEFLAG_TILEPACKAGE = "nmtp";

        public const string FILENODE_NAME_FLAG = "Flag";
        public const string FILENODE_NAME_VERSION = "Version";
        public const string FILENODE_NAME_TIME = "Time";
        public const string FILENODE_NAME_NAME = "Name";

        #endregion

        public const int NULL_SYMBOL_CODE = -1;
    }
}
