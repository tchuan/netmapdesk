﻿using NetMap.Application.Core;
using NetMap.Application.Gui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetMap.Application.Commands
{
    /// <summary>
    /// 退出
    /// </summary>
    public class ExitWorkbenchCommand : AbstractMenuCommand
    {
        public override void Run()
        {
            WorkbenchSingleton.MainWindow.Close();
        }
    }
}
