﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Application.Core;
using System.Windows.Forms;
using StoneGround.Search.TreeNodes;

namespace StoneGround.Search.Commands
{
    /// <summary>
    /// 料场删除
    /// </summary>
    public class Delete : AbstractMenuCommand
    {
        public override void Run()
        {
            SearchPad search = SearchPad.Instance;
            TreeNode node = search.StoneGroundTreeView.SelectedNode;
            if (node != null)
            {
                if (MessageService.AskQuestion("是否删除此料场？", "提示"))
                {                    
                    search.StoneGroundTreeView.Nodes.Remove(node);
                    StoneGroundNode sgNode = node as StoneGroundNode;
                    sgNode.Delete();
                }
            }
        }
    }
}
