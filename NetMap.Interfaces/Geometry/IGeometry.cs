﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

/*
 * 几何图形类型：点、线、面、多点、多线、多面
 * 2013-10-15 添加有向点为单独的一个类型
 */

namespace NetMap.Interfaces.Geometry
{
    /// <summary>
    /// 几何图形类型
    /// </summary>
    public enum geoGeometryType : short
    {
        /// <summary>
        /// 点
        /// </summary>
        GEO_GEOMETRY_POINT           = 1,
        /// <summary>
        /// 多段线
        /// </summary>
        GEO_GEOMETRY_LINESTRING      = 2,
        /// <summary>
        /// 多边形
        /// </summary>
        GEO_GEOMETRY_POLYGON         = 3,
        /// <summary>
        /// 多点
        /// </summary>
        GEO_GEOMETRY_MULTIPOINT      = 4,
        /// <summary>
        /// 多线
        /// </summary>
        GEO_GEOMETRY_MULTILINESTRING = 5,
        /// <summary>
        /// 多面
        /// </summary>
        GEO_GEOMETRY_MULTIPOLYGON    = 6,
        /// <summary>
        /// 路径
        /// </summary>
        GEO_GEOMETRY_PATH            = 7,
        /// <summary>
        /// 框
        /// </summary>
        GEO_GEOMETRY_RECT            = 8,
        /// <summary>
        /// 有向点
        /// </summary>
        GEO_GEOMETRY_DIRECTIONPOINT  = 9,
        /// <summary>
        /// 路径点
        /// </summary>
        GEO_GEOMETRY_PATHPOINT = 10,
        /// <summary>
        /// 圆
        /// </summary>
        GEO_GEOMETRY_CIRCLE = 11
    }
    /// <summary>
    /// 几何图形
    /// </summary>
    public interface IGeometry
    {
        /// <summary>
        /// 获取和设置几何范围
        /// </summary>
        IEnvelope Envelope { get; set; }
        /// <summary>
        /// 获取几何图形类型
        /// </summary>
        geoGeometryType GeometryType { get; }
        /// <summary>
        /// 更新几何范围
        /// </summary>
        void UpdateEnvelope();
        /// <summary>
        /// 复制图形
        /// </summary>
        /// <returns></returns>
        IGeometry CloneGeometry();
        /// <summary>
        /// 导出为well known byte
        /// </summary>
        /// <returns></returns>
        byte[] ExportToWkb();
        /// <summary>
        /// 导出为well known text
        /// </summary>
        /// <returns></returns>
        string ExportToWkt();
        /// <summary>
        /// 导入well known byte
        /// </summary>
        /// <param name="data"></param>
        void ImportFromWkb(byte[] data);                
        /// <summary>
        /// 导入well known text
        /// </summary>
        /// <param name="data"></param>
        void ImportFromWkt(string data);
        /// <summary>
        /// 导出到二进制流中
        /// </summary>
        /// <param name="writer"></param>
        void Export(BinaryWriter writer);
        /// <summary>
        /// 从二进制流导入
        /// </summary>
        /// <param name="reader"></param>
        void Import(BinaryReader reader);
    }
}
