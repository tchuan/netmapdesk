﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Tile;

namespace NetMap.Interfaces
{
    public interface ITileLayer : ILayer
    {
        /// <summary>
        /// 设置最大重试次数
        /// </summary>
        int MaxRetries
        {
            set;
        }
        /// <summary>
        /// 获取和设置是否补缺
        /// </summary>
        bool RepairEmpty
        {
            get;
            set;
        }
        /// <summary>
        /// 获取瓦片数据源
        /// </summary>
        ITileSource TileSource { get; }
    }
}
