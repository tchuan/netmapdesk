﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Geometry;
using NetMap.Geometry;
using ProjNet.CoordinateSystems;
using ProjNet.CoordinateSystems.Transformations;

namespace NetMap.Render
{
    public class DisplayTransformation : IDisplayTransformation
    {
        private IEnvelope _mapBounds = null;
        private IEnvelope _windowBounds = null;

        public DisplayTransformation()
        {
            _mapBounds = new Envelope(-180, -90, 180, 90);
            _windowBounds = new Envelope(0, 0, 400, 300);
            VisibleMapBounds = new Envelope(-180, -90, 180, 90);
            ScaleRatio = 1;
        }

        public DisplayTransformation(IDisplayTransformation trans)
        {
            _mapBounds = new Envelope(trans.MapBounds);
            VisibleMapBounds = new Envelope(trans.VisibleMapBounds);
            _windowBounds = new Envelope(trans.WindowBounds);
            ScaleRatio = trans.ScaleRatio;
        }

        #region IDisplayTransformation 成员

        public IEnvelope MapBounds
        {
            get { return _mapBounds; }
            set
            {
                _mapBounds.Update(value);
            }
        }

        public IEnvelope VisibleMapBounds { get; private set; }

        public IEnvelope WindowBounds
        {
            get
            {
                return _windowBounds;
            }
            set
            {
                _windowBounds.Update(value);
            }
        }

        public double ScaleRatio { get; private set; }

        public void ZoomToScale(double scale)
        {
            ScaleRatio = scale;
            IPoint center = VisibleMapBounds.Center;
            VisibleMapBounds.XMin = center.X - 0.5 * WindowBounds.Width / ScaleRatio;
            VisibleMapBounds.YMin = center.Y - 0.5 * WindowBounds.Height / ScaleRatio;
            VisibleMapBounds.XMax = center.X + 0.5 * WindowBounds.Width / ScaleRatio;
            VisibleMapBounds.YMax = center.Y + 0.5 * WindowBounds.Height / ScaleRatio;
        }
        
        public void ZoomToWindow(IEnvelope window)
        {
            WindowBounds.Update(window);
            IPoint center = VisibleMapBounds.Center;
            VisibleMapBounds.XMin = center.X - 0.5 * WindowBounds.Width / ScaleRatio;
            VisibleMapBounds.YMin = center.Y - 0.5 * WindowBounds.Height / ScaleRatio;
            VisibleMapBounds.XMax = center.X + 0.5 * WindowBounds.Width / ScaleRatio;
            VisibleMapBounds.YMax = center.Y + 0.5 * WindowBounds.Height / ScaleRatio;
        }

        public void ZoomToDataBounds(IEnvelope bounds)
        {
            IPoint center = bounds.Center;
            ScaleRatio = Math.Min(WindowBounds.Width / bounds.Width, WindowBounds.Height / bounds.Height);
            VisibleMapBounds.XMin = center.X - 0.5 * WindowBounds.Width / ScaleRatio;
            VisibleMapBounds.YMin = center.Y - 0.5 * WindowBounds.Height / ScaleRatio;
            VisibleMapBounds.XMax = center.X + 0.5 * WindowBounds.Width / ScaleRatio;
            VisibleMapBounds.YMax = center.Y + 0.5 * WindowBounds.Height / ScaleRatio;
        }

        public void ZoomToMapBounds()
        {
            IPoint center = MapBounds.Center;
            ScaleRatio = Math.Min(WindowBounds.Width / MapBounds.Width, WindowBounds.Height / MapBounds.Height);
            VisibleMapBounds.XMin = center.X - 0.5 * WindowBounds.Width / ScaleRatio;
            VisibleMapBounds.YMin = center.Y - 0.5 * WindowBounds.Height / ScaleRatio;
            VisibleMapBounds.XMax = center.X + 0.5 * WindowBounds.Width / ScaleRatio;
            VisibleMapBounds.YMax = center.Y + 0.5 * WindowBounds.Height / ScaleRatio;
        }

        public void ZoomToCenter(double x, double y, double scale)
        {
            ScaleRatio = scale;
            VisibleMapBounds.XMin = x - 0.5 * WindowBounds.Width / ScaleRatio;
            VisibleMapBounds.YMin = y - 0.5 * WindowBounds.Height / ScaleRatio;
            VisibleMapBounds.XMax = x + 0.5 * WindowBounds.Width / ScaleRatio;
            VisibleMapBounds.YMax = y + 0.5 * WindowBounds.Height / ScaleRatio;
        }

        public ICoordinateTransformation CoordinateTransformation { get; set; }
        public ICoordinateTransformation ReverseCoordinateTransformation { get; set; }

        public void FromMapPoint(double mapX, double mapY, ref double windowX, ref double windowY)
        {
            if (CoordinateTransformation != null)
            {
                CoordinateTransformation.MathTransform.Transform(mapX, mapY, ref mapX, ref mapY);
            }
            windowX = ScaleRatio * (mapX - VisibleMapBounds.XMin);
            windowY = WindowBounds.Height - ScaleRatio * (mapY - VisibleMapBounds.YMin);
        }

        public void ToMapPoint(double windowX, double windowY, ref double mapX, ref double mapY)
        {
            mapX = windowX / ScaleRatio + VisibleMapBounds.XMin;
            mapY = (WindowBounds.Height - windowY) / ScaleRatio + VisibleMapBounds.YMin;
            if (ReverseCoordinateTransformation != null)
            {
                CoordinateTransformation.MathTransform.Transform(mapX, mapY, ref mapX, ref mapY);
            }            
        }

        public IPoint FromMapPoint(IPoint point)
        {
            //modify 2014-06-13 各种点的信息不丢失
            double x = 0, y = 0;
            FromMapPoint(point.X, point.Y, ref x, ref y);
            if (point is IDirectionPoint)
            {
                IDirectionPoint directionPoint = point as IDirectionPoint;
                return new DirectionPoint(x, y, directionPoint.Angle);
            }
            else if (point is IPathPoint)
            {
                IPathPoint pathPoint = point as IPathPoint;
                IPoint inPoint = FromMapPoint(pathPoint.InPoint);
                IPoint outPoint = FromMapPoint(pathPoint.OutPoint);
                return new PathPoint(x, y, inPoint, outPoint);
            }
            else
            {
                return new Point(x, y);
            }
            //end
        }

        public IList<IPoint> FromMapPoint(IList<IPoint> pts)
        {
            IList<IPoint> ptsRe = new List<IPoint>();
            foreach (IPoint point in pts)
            {
                ptsRe.Add(FromMapPoint(point));
            }
            return ptsRe;
        }

        #endregion
    }
}
