﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace NetMap.Application.Gui
{
    /// <summary>
    /// 比例尺框
    /// </summary>
    public class ScaleComboBox : ComboBox
    {
        public ScaleComboBox()
        {
            List<string> scales = new List<string>()
            {
                "1 : 500",
                "1 : 1000",
                "1 : 2000",
                "1 : 5000",
                "1 : 10000",
                "1 : 25000",
                "1 : 50000",
                "1 : 100000",
                "1 : 200000",
                "1 : 500000",
                "1 : 1000000"
            };
            foreach (string scale in scales)
            {
                ComboBoxItem item = new ComboBoxItem();
                item.Content = scale;
                Items.Add(item);
            }

            //可编辑
            IsEditable = true;
        }
    }
}
