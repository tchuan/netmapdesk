﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NetMap.Interfaces.Data;
using NetMap.Application.Core;
using NetMap.Interfaces.Geometry;
using NetMap.Application;

namespace StoneGround.Dialog
{
    public partial class FrmSGInfo : Form
    {
        private bool _bEditable = false;
        private bool _isDirty = false;
        private IFeature _feature;
        private IFields _fields;

        public FrmSGInfo()
        {
            InitializeComponent();

            textBoxReserves.Validating += new CancelEventHandler(textBoxReserves_Validating);
            textBoxCompressive.Validating += new CancelEventHandler(textBoxCompressive_Validating);
            textBoxDensity.Validating += new CancelEventHandler(textBoxDensity_Validating);
            textBoxWaterAbsorption.Validating += new CancelEventHandler(textBoxWaterAbsorption_Validating);
            textBoxSturdiness.Validating += new CancelEventHandler(textBoxSturdiness_Validating);
            textBoxCrushing.Validating += new CancelEventHandler(textBoxCrushing_Validating);
            textBoxPolish.Validating += new CancelEventHandler(textBoxPolish_Validating);
            textBoxAbrasion.Validating += new CancelEventHandler(textBoxAbrasion_Validating);
            textBoxGill.Validating += new CancelEventHandler(textBoxGill_Validating);
            textBoxPitchAdhere.Validating += new CancelEventHandler(textBoxPitchAdhere_Validating);
            textBoxPositionLon.Validating += new CancelEventHandler(textBoxPositionLon_Validating);
            textBoxPositionLat.Validating += new CancelEventHandler(textBoxPositionLat_Validating);

            textBoxName.Validated += new EventHandler(textBoxName_Validated);
            textBoxTel.Validated += new EventHandler(textBoxTel_Validated);
            textBoxRecordStatus.Validated += new EventHandler(textBoxRecordStatus_Validated);
            textBoxManageStatus.Validated += new EventHandler(textBoxManageStatus_Validated);
            textBoxStrtumNature.Validated += new EventHandler(textBoxStrtumNature_Validated);
            textBoxGeologicage.Validated += new EventHandler(textBoxGeologicage_Validated);
            textBoxReserves.Validated += new EventHandler(textBoxReserves_Validated);
            textBoxMiningConditions.Validated += new EventHandler(textBoxMiningConditions_Validated);
            dateTimePickerRecordTime.Validated += new EventHandler(dateTimePickerRecordTime_Validated);
            textBoxCompressive.Validated += new EventHandler(textBoxCompressive_Validated);
            textBoxDensity.Validated += new EventHandler(textBoxDensity_Validated);
            textBoxWaterAbsorption.Validated += new EventHandler(textBoxWaterAbsorption_Validated);
            textBoxSturdiness.Validated += new EventHandler(textBoxSturdiness_Validated);
            textBoxCrushing.Validated += new EventHandler(textBoxCrushing_Validated);
            textBoxPolish.Validated += new EventHandler(textBoxPolish_Validated);
            textBoxAbrasion.Validated += new EventHandler(textBoxAbrasion_Validated);
            textBoxGill.Validated += new EventHandler(textBoxGill_Validated);
            textBoxPitchAdhere.Validated += new EventHandler(textBoxPitchAdhere_Validated);
            textBoxPositionLon.Validated += new EventHandler(textBoxPositionLon_Validated);
            textBoxPositionLat.Validated += new EventHandler(textBoxPositionLat_Validated);

            FormClosing += new FormClosingEventHandler(FrmSGInfo_FormClosing);
            this.Activated += new EventHandler(FrmSGInfo_Activated);
        }

        void FrmSGInfo_Activated(object sender, EventArgs e)
        {
            textBoxName.Focus(); //获得焦点
        }              

        void FrmSGInfo_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_bEditable && _isDirty)
            {
                DialogResult result = MessageBox.Show("料场信息已改变，是否保存？", "提示", MessageBoxButtons.YesNoCancel);
                if (result == System.Windows.Forms.DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
                else if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    _feature.Store();
                    //刷新地图
                    WorkspaceSingleton.ActiveMapView.Invalidate();
                }
            }
        }

        #region 数据验证

        void textBoxPositionLat_Validating(object sender, CancelEventArgs e)
        {
            double result;
            if (!double.TryParse(textBoxPositionLat.Text, out result))
            {
                MessageBox.Show(this, "请输入有效的：纬度", "提示");
                e.Cancel = true;
            }
        }

        void textBoxPositionLon_Validating(object sender, CancelEventArgs e)
        {
            double result;
            if (!double.TryParse(textBoxPositionLon.Text, out result))
            {
                MessageBox.Show(this, "请输入有效的：经度", "提示");
                e.Cancel = true;
            }
        }

        void textBoxPitchAdhere_Validating(object sender, CancelEventArgs e)
        {
            int result;
            if (!int.TryParse(textBoxPitchAdhere.Text, out result))
            {
                MessageBox.Show(this, "请输入有效的：与沥青粘附等级（级）", "提示");
                e.Cancel = true;
            }
        }

        void textBoxGill_Validating(object sender, CancelEventArgs e)
        {
            double result;
            if (!double.TryParse(textBoxGill.Text, out result))
            {
                MessageBox.Show(this, "请输入有效的：针片状（%）", "提示");
                e.Cancel = true;
            }
        }

        void textBoxAbrasion_Validating(object sender, CancelEventArgs e)
        {
            double result;
            if (!double.TryParse(textBoxAbrasion.Text, out result))
            {
                MessageBox.Show(this, "请输入有效的：磨耗值（%）", "提示");
                e.Cancel = true;
            }
        }

        void textBoxPolish_Validating(object sender, CancelEventArgs e)
        {
            double result;
            if (!double.TryParse(textBoxPolish.Text, out result))
            {
                MessageBox.Show(this, "请输入有效的：磨光值（PSV）", "提示");
                e.Cancel = true;
            }
        }

        void textBoxCrushing_Validating(object sender, CancelEventArgs e)
        {
            double result;
            if (!double.TryParse(textBoxCrushing.Text, out result))
            {
                MessageBox.Show(this, "请输入有效的：压碎值（%）", "提示");
                e.Cancel = true;
            }
        }

        void textBoxSturdiness_Validating(object sender, CancelEventArgs e)
        {
            double result;
            if (!double.TryParse(textBoxSturdiness.Text, out result))
            {
                MessageBox.Show(this, "请输入有效的：坚固性（%）", "提示");
                e.Cancel = true;
            }
        }

        void textBoxWaterAbsorption_Validating(object sender, CancelEventArgs e)
        {
            double result;
            if (!double.TryParse(textBoxWaterAbsorption.Text, out result))
            {
                MessageBox.Show(this, "请输入有效的：吸水率（%）", "提示");
                e.Cancel = true;
            }
        }

        void textBoxDensity_Validating(object sender, CancelEventArgs e)
        {
            double result;
            if (!double.TryParse(textBoxDensity.Text, out result))
            {
                MessageBox.Show(this, "请输入有效的：表观相对密度", "提示");
                e.Cancel = true;
            }
        }

        void textBoxCompressive_Validating(object sender, CancelEventArgs e)
        {
            double result;
            if (!double.TryParse(textBoxCompressive.Text, out result))
            {
                MessageBox.Show(this, "请输入有效的：单轴抗压强度（Mpa）", "提示");
                e.Cancel = true;
            }
        }

        void textBoxReserves_Validating(object sender, CancelEventArgs e)
        {
            double result;
            if (!double.TryParse(textBoxReserves.Text, out result))
            {
                MessageBox.Show(this, "请输入有效的：储量（万m³）", "提示");
                e.Cancel = true;
            }
        }
        
        #endregion

        #region 数据验证完毕

        void textBoxPositionLat_Validated(object sender, EventArgs e)
        {
            double value = 0;
            double.TryParse(textBoxPositionLat.Text, out value);
            IPoint point = _feature.Geometry as IPoint;
            if (point.Y != value)
            {
                _feature.Geometry = new NetMap.Geometry.Point(point.X, value);
                _isDirty = true;
            }
        }

        void textBoxPositionLon_Validated(object sender, EventArgs e)
        {
            double value = 0;
            double.TryParse(textBoxPositionLon.Text, out value);
            IPoint point = _feature.Geometry as IPoint;
            if (point.X != value)
            {
                _feature.Geometry = new NetMap.Geometry.Point(value, point.Y);
                _isDirty = true;
            }
        }  

        void textBoxPitchAdhere_Validated(object sender, EventArgs e)
        {
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_PITCHADHERE);
            int value = 0;
            int.TryParse(textBoxPitchAdhere.Text, out value);
            int oldValue = (int)_feature.GetValue(index);
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxGill_Validated(object sender, EventArgs e)
        {
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_GILL);
            double value = 0;
            double.TryParse(textBoxGill.Text, out value);
            double oldValue = (double)_feature.GetValue(index);
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxAbrasion_Validated(object sender, EventArgs e)
        {
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_ABRASION);
            double value = 0;
            double.TryParse(textBoxAbrasion.Text, out value);
            double oldValue = (double)_feature.GetValue(index);
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxPolish_Validated(object sender, EventArgs e)
        {
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_POLISH);
            double value = 0;
            double.TryParse(textBoxPolish.Text, out value);
            double oldValue = (double)_feature.GetValue(index);
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxCrushing_Validated(object sender, EventArgs e)
        {
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_CRUSHING);
            double value = 0;
            double.TryParse(textBoxCrushing.Text, out value);
            double oldValue = (double)_feature.GetValue(index);
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxSturdiness_Validated(object sender, EventArgs e)
        {
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_STURDINESS);
            double value = 0;
            double.TryParse(textBoxSturdiness.Text, out value);
            double oldValue = (double)_feature.GetValue(index);
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxWaterAbsorption_Validated(object sender, EventArgs e)
        {
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_WATERABSORPTION);
            double value = 0;
            double.TryParse(textBoxWaterAbsorption.Text, out value);
            double oldValue = (double)_feature.GetValue(index);
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxDensity_Validated(object sender, EventArgs e)
        {
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_DENSITY);
            double value = 0;
            double.TryParse(textBoxDensity.Text, out value);
            double oldValue = (double)_feature.GetValue(index);
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxCompressive_Validated(object sender, EventArgs e)
        {
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_COMPRESSIVE);
            double value = 0;
            double.TryParse(textBoxCompressive.Text, out value);
            double oldValue = (double)_feature.GetValue(index);
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void dateTimePickerRecordTime_Validated(object sender, EventArgs e)
        {
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_RECORDTIME);
            DateTime value = dateTimePickerRecordTime.Value;
            DateTime oldValue = (DateTime)_feature.GetValue(index);
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxMiningConditions_Validated(object sender, EventArgs e)
        {
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_MININGCONDITIONS);
            string value = textBoxMiningConditions.Text;
            string oldValue = _feature.GetValue(index).ToString();
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxReserves_Validated(object sender, EventArgs e)
        {
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_RESERVES);
            double value = 0;
            double.TryParse(textBoxReserves.Text, out value);
            double oldValue = (double)_feature.GetValue(index);
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxGeologicage_Validated(object sender, EventArgs e)
        {
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_GEOLOGICAGE);
            string value = textBoxGeologicage.Text;
            string oldValue = _feature.GetValue(index).ToString();
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxStrtumNature_Validated(object sender, EventArgs e)
        {
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_STRTUMNATURE);
            string value = textBoxStrtumNature.Text;
            string oldValue = _feature.GetValue(index).ToString();
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxManageStatus_Validated(object sender, EventArgs e)
        {
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_MANAGESTATUS);
            string value = textBoxManageStatus.Text;
            string oldValue = _feature.GetValue(index).ToString();
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxRecordStatus_Validated(object sender, EventArgs e)
        {
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_RECORDSTATUS);
            string value = textBoxRecordStatus.Text;
            string oldValue = _feature.GetValue(index).ToString();
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxTel_Validated(object sender, EventArgs e)
        {
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_TEL);
            string value = textBoxTel.Text;
            _feature.SetValue(index, value);
            _isDirty = true;
        }

        void textBoxName_Validated(object sender, EventArgs e)
        {
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_NAME);
            string value = textBoxName.Text;
            string oldValue = _feature.GetValue(index).ToString();
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        #endregion

        /// <summary>
        /// 初始化要素
        /// </summary>
        /// <param name="feature">料场要素</param>
        /// <param name="bEditable">是否能编辑料场属性</param>
        public void Init(IFeature feature, bool bEditable)
        {
            _bEditable = bEditable;
            if (!_bEditable)
            {
                textBoxName.Enabled = false;
                textBoxTel.Enabled = false;
                textBoxManageStatus.Enabled = false;
                textBoxRecordStatus.Enabled = false;
                textBoxStrtumNature.Enabled = false;
                textBoxGeologicage.Enabled = false;
                textBoxReserves.Enabled = false;
                textBoxMiningConditions.Enabled = false;
                dateTimePickerRecordTime.Enabled = false;
                textBoxPositionLon.Enabled = false;
                textBoxPositionLat.Enabled = false;
                textBoxCompressive.Enabled = false;
                textBoxDensity.Enabled = false;
                textBoxWaterAbsorption.Enabled = false;
                textBoxSturdiness.Enabled = false;
                textBoxCrushing.Enabled = false;
                textBoxPolish.Enabled = false;
                textBoxAbrasion.Enabled = false;
                textBoxGill.Enabled = false;
                textBoxPitchAdhere.Enabled = false;
                textBoxTrafficConditions.Enabled = false;
            }
            _isDirty = false;
            _feature = feature;
            _fields = _feature.Fields;

            #region UI赋值
            IFields fields = feature.Fields;
            int index = fields.FindField(StoneGroundDatabaseService.FIELD_NAME);
            object value = feature.GetValue(index);
            textBoxName.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_TEL);
            value = feature.GetValue(index);
            textBoxTel.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_RECORDSTATUS);
            value = feature.GetValue(index);
            textBoxRecordStatus.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_MANAGESTATUS);
            value = feature.GetValue(index);
            textBoxManageStatus.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_STRTUMNATURE);
            value = feature.GetValue(index);
            textBoxStrtumNature.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_GEOLOGICAGE);
            value = feature.GetValue(index);
            textBoxGeologicage.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_RESERVES);
            value = feature.GetValue(index);
            textBoxReserves.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_MININGCONDITIONS);
            value = feature.GetValue(index);
            textBoxMiningConditions.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_TRAFFICCONDITIONS);
            value = feature.GetValue(index);
            textBoxTrafficConditions.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_RECORDTIME);
            value = feature.GetValue(index);
            dateTimePickerRecordTime.Value = (DateTime)value;

            index = fields.FindField(StoneGroundDatabaseService.FIELD_COMPRESSIVE);
            value = feature.GetValue(index);
            textBoxCompressive.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_DENSITY);
            value = feature.GetValue(index);
            textBoxDensity.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_WATERABSORPTION);
            value = feature.GetValue(index);
            textBoxWaterAbsorption.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_STURDINESS);
            value = feature.GetValue(index);
            textBoxSturdiness.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_CRUSHING);
            value = feature.GetValue(index);
            textBoxCrushing.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_POLISH);
            value = feature.GetValue(index);
            textBoxPolish.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_ABRASION);
            value = feature.GetValue(index);
            textBoxAbrasion.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_GILL);
            value = feature.GetValue(index);
            textBoxGill.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_PITCHADHERE);
            value = feature.GetValue(index);
            textBoxPitchAdhere.Text = value.ToString();

            IPoint point = feature.Geometry as IPoint;
            textBoxPositionLon.Text = point.X.ToString();
            textBoxPositionLat.Text = point.Y.ToString();

            #endregion

            JustifyUI();

            this.buttonModify.Click += new System.EventHandler(this.buttonModify_Click);
        }

        /// <summary>
        /// 初始化创建
        /// </summary>
        public void InitCreate(IFeature newFeature)
        {
            buttonModify.Text = "创建";

            _bEditable = true;
            _isDirty = false;
            _feature = newFeature;
            _fields = _feature.Fields;

            this.buttonModify.Click += new EventHandler(buttonCreate_Click);
        }

        private void JustifyUI()
        {
            if (_bEditable)
            {
                buttonModify.Enabled = true;
            }
            else
            {
                buttonModify.Enabled = false;
                this.Height = buttonModify.Top + 25;
            }
        }

        private void buttonModify_Click(object sender, EventArgs e)
        {
            if (!_bEditable) //检查是否可以编辑
            {
                return;
            }
            if (_isDirty)
            {
                _feature.Store();
                _isDirty = false;
                //刷新地图
                WorkspaceSingleton.ActiveMapView.Invalidate();
                MessageBox.Show(this, "修改成功", "提示");                
            }
            else
            {
                MessageBox.Show(this, "信息未变更，无需修改", "提示");
            }
            this.Close();
        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            if (!_bEditable) //检查是否可以编辑
            {
                return;
            }
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_NAME);
            object value = _feature.GetValue(index);
            if (value == null || string.IsNullOrEmpty(value.ToString()))
            {
                MessageBox.Show(this, "料场信息不全，不能创建", "提示");
            }
            else
            {
                _feature.Store();
                _isDirty = false;
                //刷新地图
                WorkspaceSingleton.ActiveMapView.Invalidate();
                this.Close();
            }
        }
    }
}
