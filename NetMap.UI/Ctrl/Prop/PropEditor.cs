﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NetMap.Interfaces.Data;

namespace NetMap.UI.Ctrl.Prop
{
    public partial class PropEditor : UserControl
    {
        public const string PROP_CATEGORY_BASIC = "基本";
        public const string PROP_CATEGORY_PROP = "属性";

        private IFeature _feature;
        private bool _isPropModify = false;

        public PropEditor()
        {
            InitializeComponent();

            propertyGridMain.PropertyValueChanged += new PropertyValueChangedEventHandler(propertyGridMain_PropertyValueChanged);
        }

        //属性值变化
        void propertyGridMain_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            if(_feature != null)
            {
                PropItemDescriptor descriptor = e.ChangedItem.PropertyDescriptor as PropItemDescriptor;
                string fieldName = descriptor.Name;
                int index = _feature.Fields.FindField(fieldName);
                _feature.SetValue(index, e.ChangedItem.Value);
                _isPropModify = true;
            }
        }

        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="feature">目标要素</param>
        public void Init(IFeature feature)
        {
            _feature = feature;
            _isPropModify = false;

            Props props = new Props();
            
            //基本
            PropItem oidProp = new PropItem();
            oidProp.Name = "OID";
            oidProp.Category = PROP_CATEGORY_BASIC;
            oidProp.Value = feature.OID;
            oidProp.Description = "OID";
            oidProp.Type = typeof(Int64);
            oidProp.IsReadOnly = true;
            oidProp.Browsable = true;
            props.Add(oidProp);
            //TODO 其他基本的要素属性

            //属性
            IFields fields = feature.Fields;
            IField field = null;
            for (int i = 0; i < fields.Count; i++)
            {
                field = fields.GetField(i);
                PropItem item = new PropItem();
                item.Name = field.Name;
                item.Category = PROP_CATEGORY_PROP;
                item.Value = feature.GetValue(i);
                item.Description = field.Name; //TODO 添加描述
                item.IsReadOnly = false;
                #region Type
                switch (field.Type)
                {
                    case geoFieldType.GEO_SMALLINT:
                        {
                            item.Type = typeof(Int16);
                            break;
                        }
                    case geoFieldType.GEO_INT:
                        {
                            item.Type = typeof(Int32);
                            break;
                        }
                    case geoFieldType.GEO_BIGINT:
                        {
                            item.Type = typeof(Int64);
                            break;
                        }
                    case geoFieldType.GEO_SINGLE:
                        {
                            item.Type = typeof(float);
                            break;
                        }
                    case geoFieldType.GEO_DOUBLE:
                        {
                            item.Type = typeof(double);
                            break;
                        }
                    case geoFieldType.GEO_STRING:
                        {
                            item.Type = typeof(string);
                            break;
                        }
                    case geoFieldType.GEO_DATE:
                        {
                            item.Type = typeof(DateTime);
                            break;
                        }
                    case geoFieldType.GEO_GEOMETRY:
                    case geoFieldType.GEO_BLOB:
                        {
                            item.Type = typeof(byte[]);
                            item.IsReadOnly = true;
                            break;
                        }
                }
                #endregion
                item.Browsable = true;
                props.Add(item);
            }

            propertyGridMain.SelectedObject = props;
        }

        /// <summary>
        /// 获取属性是否改变
        /// </summary>
        public bool IsPropModify
        {
            get
            {
                return _isPropModify;
            }
        }
    }
}
