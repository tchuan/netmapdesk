﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;
using System.Drawing;

namespace NetMap.Interfaces.Symbol
{
    /// <summary>
    /// Box点符号接口
    /// </summary>
    public interface IBoxPointSymbol : IPointSymbol
    {
        /// <summary>
        /// 获取和设置是否有边线
        /// </summary>
        bool Outline { get; set; }
        /// <summary>
        /// 获取和设置是否有填充
        /// </summary>
        bool Fill { get; set; }
        /// <summary>
        /// 获取和设置线宽
        /// </summary>
        float Width { get; set; }
        /// <summary>
        /// 获取和设置填充色
        /// </summary>
        Color FillColor { get; set; }
        /// <summary>
        /// 获取和设置Box
        /// </summary>
        IEnvelope Box { get; set; }
    }
}
