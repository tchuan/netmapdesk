﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Symbol;
using System.IO;
using NetMap.Interfaces.Persist;
using System.Xml;
using NetMap.Interfaces;

namespace NetMap.Symbol
{
    /// <summary>
    /// 符号库，可以是单独文件，也可以作为其他文件的一部分
    /// </summary>
    public class SymbolLibrary : ISymbolLibrary, IFile, IPersistResource
    {
        private List<ISymbol> _symbolList = new List<ISymbol>();
        private int _count = 0;
        private string _name = "默认符号库";        

        public SymbolLibrary()
        { 
        }

        #region ISymbolLibrary成员

        public string Name { get { return _name; } set { _name = value; } }     

        public ISymbol FindSymbol(int code)
        {
                foreach (ISymbol symbol in _symbolList)
                {
                    if (symbol.Code == code) 
                    {
                        return symbol; 
                    }
                }
            return null;
        }

        public void AddSymbol(ISymbol symbol)
        {
            if (_symbolList == null) _symbolList = new List<ISymbol>();
            bool tem = false;
            foreach (ISymbol s in _symbolList)
            {
                if (symbol.Code == s.Code)
                {
                    tem = true;
                    break;
                }
            }
            if (tem)
            {
                symbol.Code = GetAvailable();
                _symbolList.Add(symbol);
            }
            else
            {
                _symbolList.Add(symbol);
            }
        }

        public void UpdateSymbol(int code, ISymbol newSymbol)
        {
            if (_symbolList != null)
            {
                ISymbol symbol = _symbolList.First(m => m.Code == code);
                if (symbol != null)
                {
                    _symbolList.Remove(symbol);
                }
                _symbolList.Add(newSymbol);
            }
        }

        public void RemoveSymbol(int code)
        {
            _symbolList.Remove(_symbolList.FirstOrDefault(m => m.Code == code));
        }

        public bool IsCodeExist(int code)
        {
            bool tem=false;
            if (_symbolList == null) tem = false;
            else
            {
                tem = (_symbolList.FirstOrDefault(m => m.Code == code) == null ? false : true);
            }
            return tem;
        }

        public int GetAvailable()
        {
            if (_symbolList == null) return 0;
            else
            {
                int max = int.MinValue;
                foreach (ISymbol symbol in _symbolList)
                {
                    if (max < symbol.Code) max = symbol.Code;
                }
                return (max + 1);
            }
        }

        public void Recode(int startCode)
        {
            for (int i = 0; i < _symbolList.Count(); i++)
            {
                _symbolList[i].Code = i + startCode;
            }
        }

        public List<ISymbol> GetAllSymbol()
        {
            if (_symbolList != null) return _symbolList;
            else return new List<ISymbol>();
        }

        public int Count 
        {
            get 
            {
                if (_symbolList == null) return -1;
                _count = _symbolList.Count;
                return _count;
            } 
        }

        #endregion

        #region IFile成员

        private int _version = 0;
        private DateTime _time = DateTime.Now;
        private string _path = string.Empty;

        private const string NAME_LIB = "SymbolLibrary";
        private const string NAME_VERSION = "Version";
        private const string NAME_TIME = "Time";
        private const string NAME_FLAG = "Flag";
        private const string NAME_NAME = "Name";
        private const string NAME_SYMBOLS = "Symbols";

        public string Flag
        {
            get
            {
                return Defines.FILEFLAG_SYMBOLLIB;
            }
        }

        public int Version { get { return _version; } set { _version = value; } }

        public DateTime Time { get { return _time; } set { _time = value; } }

        public void Save()
        {
            if (string.IsNullOrEmpty(_path)
                || (!File.Exists(_path)))
            {
                throw new FileNotFoundException("符号库文件未找到，不能保存");
            }
        }

        public void Load(string path)
        {
            using (TextReader reader = File.OpenText(path))
            {
                using (XmlTextReader xmlReader = new XmlTextReader(reader, null))
                {
                    while (xmlReader.Read())
                    {
                        if (xmlReader.IsStartElement())
                        {
                            switch (xmlReader.Name)
                            {
                                case NAME_LIB:
                                    {
                                        string flag = xmlReader.GetAttribute(NAME_FLAG);
                                        if (flag != Defines.FILEFLAG_SYMBOLLIB)
                                        {
                                            throw new FileIOException("未知符号文件");
                                        }
                                        Version = int.Parse(xmlReader.GetAttribute(NAME_VERSION));
                                        Time = DateTime.Parse(xmlReader.GetAttribute(NAME_TIME));

                                        break;
                                    }
                                default:
                                    {
                                        throw new FileIOException("未知符号文件");
                                    }
                            }
                        }
                    }
                }
            }
        }

        public void SaveAs(string path)
        {
            XmlDocument document = new XmlDocument();
            XmlElement lib = document.CreateElement(NAME_LIB);
            lib.SetAttribute(NAME_VERSION, Version.ToString());
            lib.SetAttribute(NAME_TIME, Time.ToString("yy-MM-dd hh:mm:ss"));
            lib.SetAttribute(NAME_FLAG, Defines.FILEFLAG_SYMBOLLIB);
            lib.SetAttribute(NAME_NAME, Name);
            XmlElement symbolsNode = document.CreateElement(NAME_SYMBOLS);
            foreach (ISymbol symbol in _symbolList)
            {
                if (symbol is IPersistResource)
                {
                    IPersistResource pr = symbol as IPersistResource;
                    pr.Save(symbolsNode, document);
                }
            }
            lib.AppendChild(symbolsNode);
            document.AppendChild(lib);
            document.Save(path);
        }

        #endregion

        //读取符号集
        private void ReadSymbols(XmlReader reader)
        {
        }

        #region IPersistResource 成员

        void IPersistResource.Save(XmlElement node, XmlDocument document)
        {
            throw new NotImplementedException();
        }

        void IPersistResource.Load(XmlReader reader, object context)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
