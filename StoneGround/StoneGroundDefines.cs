﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StoneGround
{
    /// <summary>
    /// 系统定义
    /// </summary>
    public static class StoneGroundDefines
    {
        #region 图层名字

        public const string LAYER_NAME_TILE = "tile";
        public const string LAYER_NAME_PROVINCE = "province";
        public const string LAYER_NAME_ZONE = "zone";
        public const string LAYER_NAME_ROAD = "road";
        public const string LAYER_NAME_STONEGROUND = "stoneground";
        public const string LAYER_NAME_DZONE = "dzone";

        #endregion
    }
}
