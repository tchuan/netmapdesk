﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;

namespace NetMap.Interfaces.Data
{
    /// <summary>
    /// 空间关系
    /// </summary>
    public enum geoSpatialRelationship
    {
        /// <summary>
        /// 相离
        /// </summary>
        GEO_SPATIALRELATIONSHIP_DISJOINT,
        /// <summary>
        /// 相接
        /// </summary>
        GEO_SPATIALRELATIONSHIP_TOUCHES,
        /// <summary>
        /// 相交
        /// </summary>
        GEO_SPATIALRELATIONSHIP_CROSSES,
        /// <summary>
        /// 真包含
        /// </summary>
        GEO_SPATIALRELATIONSHIP_WITHINS,
        /// <summary>
        /// 叠置
        /// </summary>
        GEO_SPATIALRELATIONSHIP_OVERLAPS,
        /// <summary>
        /// 包含
        /// </summary>
        GEO_SPATIALRELATIONSHIP_CONTAINS,
        /// <summary>
        /// 相交
        /// </summary>
        GEO_SPATIALRELATIONSHIP_INTERSECTS,
        /// <summary>
        /// 相等
        /// </summary>
        GEO_SPATIALRELATIONSHIP_EQUALS,
        /// <summary>
        /// 范围相交
        /// </summary>
        GEO_SPATIALRELATIONSHIP_ENVELOPEINTERSECTS
    }

    /// <summary>
    /// 空间查询接口
    /// </summary>
    public interface ISpatialFilter : IQueryFilter
    {
        /// <summary>
        /// 获取和设置空间关系
        /// </summary>
        geoSpatialRelationship SpatialRelationship { get; set; }
        /// <summary>
        /// 获取和设置几何图形
        /// </summary>
        IGeometry Geometry { get; set; }
    }
}
