﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjNet.CoordinateSystems.Transformations;
using NetMap.Geometry;
using NetMap.Interfaces.Geometry;

namespace NetMap.Utilities
{
    /// <summary>
    /// 图形坐标转换
    /// </summary>
    public class GeometryCoordinateTransformation
    {
        public static IGeometry Transformation(ICoordinateTransformation coordinateTransformation, IGeometry geometry)
        {
            if (geometry is IPoint)
            {
               return Transformation(coordinateTransformation, geometry as IPoint);
            }
            else if (geometry is IMultiPoint)
            {
                return Transformation(coordinateTransformation,geometry as IMultiPoint);
            }
            else if (geometry is IDirectionPoint)
            {
                return  Transformation(coordinateTransformation, geometry as IDirectionPoint);
            }
            else if (geometry is ILineString)
            {
                return Transformation(coordinateTransformation,geometry as ILineString);
            }
            else if (geometry is IMultiLineString)
            {
                return Transformation(coordinateTransformation,geometry as IMultiLineString);
            }
            else if (geometry is IPolygon)
            {
                return Transformation(coordinateTransformation, geometry as IPolygon);
            }
            else if (geometry is IMultiPolygon)
            {
                return Transformation(coordinateTransformation,geometry as IMultiPolygon);
            }
            return null;
        }

        private static IGeometry Transformation(ICoordinateTransformation coordia, IMultiPolygon pt)
        {
            List<IPolygon> plys = new List<IPolygon>();
            foreach (IPolygon p in pt)
            {
                plys.Add(Transformation(coordia,p) as IPolygon);
            }
            return new MultiPolygon(plys,plys.ToEnvelope());
        }

        private static IGeometry Transformation(ICoordinateTransformation coordia, IPolygon pt)
        {
            List<ILineString> rings = new List<ILineString>();
            foreach (ILineString line in pt.InteriorRings)
            {
                rings.Add(Transformation(coordia,line) as ILineString);
            }
            foreach (ILineString line in pt.ExteriorRing)
            {
                rings.Add(Transformation(coordia, line) as ILineString);
            }
            return new Polygon(rings, rings.ToEnvelope());
        }

        private static IGeometry Transformation(ICoordinateTransformation coordia, IMultiLineString pt)
        {
            List<ILineString> lines = new List<ILineString>();
            foreach (ILineString line in pt)
            {
                lines.Add(Transformation(coordia,line) as ILineString);
            }
            return new MultiLineString(lines,lines.ToEnvelope());
        }
        private static IGeometry Transformation(ICoordinateTransformation coordia, ILineString pt)
        {
            List<IPoint> pts=new List<IPoint>();
            foreach (IPoint temPt in pt)
            {
                IPoint tem = Transformation(coordia, temPt) as IPoint;
                pts.Add(tem);
            }
            return  new LineString(pts, pts.ToEnvelope());
        }
        private static IGeometry Transformation(ICoordinateTransformation coordia, IDirectionPoint pt)
        {
            double x = 0;
            double y = 0;
            coordia.MathTransform.Transform(pt.X, pt.Y, ref x, ref y);
            return new DirectionPoint(x, y, pt.Angle);
        }
        private static IGeometry Transformation(ICoordinateTransformation coordia, IMultiPoint pt)
        {
            List<IPoint> pts = new List<IPoint>();
            foreach (IPoint temPt in pt)
            {
                pts.Add(Transformation(coordia,temPt) as IPoint);
            }
            return new MultiPoint(pts, pts.ToEnvelope());
        }
        private static IGeometry Transformation(ICoordinateTransformation coordia, IPoint pt)
        {
            double x = 0;
            double y = 0;
            coordia.MathTransform.Transform(pt.X, pt.Y, ref x, ref y); 
            return new Point(x, y);
        }
        
    }
    public static class Helper
    {
        public static IEnvelope ToEnvelope(this List<IPoint> pts)
        {
            double xmin = double.MaxValue;
            double ymin = double.MaxValue;
            double xmax = double.MinValue;
            double ymax = double.MinValue;
            foreach (IPoint pt in pts)
            {
                if (xmin > pt.X) xmin = pt.X;
                if (ymin > pt.Y) ymin = pt.Y;
                if (xmax < pt.X) xmax = pt.X;
                if (ymax < pt.Y) ymax = pt.Y;
            }
            return new Envelope(xmin, ymin, xmax, ymax);
        }

        public static IEnvelope ToEnvelope(this List<IPolygon> plys)
        {
            List<IEnvelope> envs = new List<IEnvelope>();
            foreach (IPolygon p in plys)
            {
                envs.Add(p.ToEnvelope());
            }
            return envs.ToEnvelope();
        }

        public static IEnvelope ToEnvelope(this IPolygon plys)
        {
            List<IEnvelope> envs = new List<IEnvelope>();
            foreach (ILineString ll in plys.ExteriorRing)
            {
                envs.Add(ll.ToEnvelope());
            }
            foreach (ILineString ll in plys.InteriorRings)
            {
                envs.Add(ll.ToEnvelope());
            }
            return envs.ToEnvelope();
        }

        public static IEnvelope ToEnvelope(this List<ILineString> lines)
        {
            List<IEnvelope> envs = new List<IEnvelope>();
            foreach (ILineString line in lines)
            {
                envs.Add(line.ToEnvelope());
            }
            return envs.ToEnvelope();
        }

        public static IEnvelope ToEnvelope(this ILineString line)
        {
            return (line as List<IPoint>).ToEnvelope();
        }

        public static IEnvelope ToEnvelope(this List<IEnvelope> envs)
        {
            double xmin = envs[0].XMin;
            double ymin = envs[0].YMin;
            double xmax = envs[0].XMax;
            double ymax = envs[0].YMax;
            foreach (IEnvelope en in envs)
            {
                if (xmin > en.XMin) xmin = en.XMin;
                if (ymin > en.YMin) ymin = en.YMin;
                if (xmax < en.XMax) xmax = en.XMax;
                if (ymax < en.YMax) ymax = en.YMax;
            }
            return new Envelope(xmin,ymin,xmax,ymax);
        }
       
   }
}
