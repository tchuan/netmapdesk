using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using NetMap.UI.Properties;

namespace Aga.Controls.Tree.NodeControls
{
	public class NodeStateIcon: NodeIcon
	{
		protected Image _leaf;
        protected Image _opened;
        protected Image _closed;

		public NodeStateIcon()
		{
			_leaf = MakeTransparent(Resources.leaf);
			_opened = MakeTransparent(Resources.Folder);
			_closed = MakeTransparent(Resources.FolderClosed);
		}

		private static Image MakeTransparent(Bitmap bitmap)
		{
			bitmap.MakeTransparent(bitmap.GetPixel(0,0));
			return bitmap;
		}

		protected override Image GetIcon(TreeNodeAdv node)
		{
			Image icon = base.GetIcon(node);
            if (icon != null)
                return icon;
            else if (node.CanExpand)
            {
                if (node.IsExpanded)
                    return _opened;
                else
                    return _closed;
            }
            else if (node.IsLeaf||node.Nodes.Count==0)
                return _leaf;
            else 
                return null;
			 
		}
	}
}
