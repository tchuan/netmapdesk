﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Application.Core;
using NetMap.Interfaces;
using NetMap;
using NetMap.Geometry;
using NetMap.Interfaces.Data;
using NetMap.Data;
using NetMap.Sqlite;
using NetMap.Tile.Cache;
using NetMap.Tile;
using NetMap.Interfaces.Tile;
using NetMap.Tile.Wellknown;
using NetMap.Application;
using NetMap.Symbol;
using NetMap.Render;
using System.Drawing;
using System.Windows.Forms;
using NetMap.Interfaces.Symbol;
using NetMap.Application.Gui;
using NetMap.Interfaces.Geometry;
using System.IO;
using NetMap.Util;
using User;

namespace StoneGround
{
    public class AutoStart : AbstractMenuCommand
    {
        public override void Run()
        {
            IMapDocument map = BuildMap();
            IMapView view = WorkspaceSingleton.ActiveMapView;
            view.MapDocument = map;
            //绑定工作台关闭事件
            WorkbenchSingleton.WorkbenchUnloaded += new EventHandler(WorkbenchSingleton_WorkbenchUnloaded);
        }

        //关闭事件处理
        void WorkbenchSingleton_WorkbenchUnloaded(object sender, EventArgs e)
        {
            SpatialDatabaseService.Instance.Close();
            StoneGroundDatabaseService.Instance.Close();
        }

        private IMapDocument BuildMap()
        {
            IMapDocument map = new MapDocument(new Envelope(97.0646437994724, 20.8970976253298, 106.682058047493, 29.9208443271768));
            
            //瓦片图层
            ILayer tileLayer = BuildTileLayer();
            map.AddLayer(tileLayer);

            //地理底图矢量图层
            SpatialDatabaseService.Instance.Init();
            ILayer proviceLayer = BuildProvinceLayer();
            map.AddLayer(proviceLayer);
            ILayer dzoneLayer = BuildDzoneLayer();
            map.AddLayer(dzoneLayer);
            ILayer zoneLayer = BuildZoneLayer();
            map.AddLayer(zoneLayer);
            //加入道路图层
            ILayer roadLayer = BuildRoadLayer();
            roadLayer.Name = StoneGroundDefines.LAYER_NAME_ROAD;
            map.AddLayer(roadLayer);

            //料场
            StoneGroundDatabaseService.Instance.Init();
            ILayer stoneGroundLayer = BuildStoneGroundLayer();
            map.AddLayer(stoneGroundLayer);

            SetDocumentScale(tileLayer as TileLayer, map);

            return map;
        }

        #region 图层

        //构建瓦片图层
        private ILayer BuildTileLayer()
        { 
            //瓦片矢量背景
            List<string> nmtps = FindNmtp();
            ITileProvider provider = CreateTileProvider(nmtps);
            ITileSchema schema = new TiandituTileSchema();
            ITileSource source = new TileSource(provider, schema);
            ILayer layer = new TileLayer(source, true);
            layer.Name = StoneGroundDefines.LAYER_NAME_TILE;
            return layer;
        }

        //构建省图层
        private ILayer BuildProvinceLayer()
        {
            //线符号
            StrokeSymbol symbol = new StrokeSymbol();
            symbol.Name = "省界";
            symbol.NodeAvailable = false;
            symbol.Pen.Width = 3;
            symbol.Pen.Color = Color.FromArgb(128, 0, 0, 255);
            //图层方案
            SimpleRender render = new SimpleRender();
            render.Symbol = symbol;
            //图层
            FeatureLayer layer = new FeatureLayer();
            layer.Name = StoneGroundDefines.LAYER_NAME_PROVINCE;
            layer.Render = render;
            layer.FeatureClass = SpatialDatabaseService.Instance.ProviceFeatureClass;
            return layer;
        }

        //构建地级行政区图层
        private ILayer BuildDzoneLayer()
        {
            FeatureLayer layer = new FeatureLayer();
            layer.Name = StoneGroundDefines.LAYER_NAME_DZONE;
            layer.FeatureClass = SpatialDatabaseService.Instance.DzoneFeatureClass;
            layer.SelectionSymbol = SelectionSymbolManager.Instance.GetSymbol(layer.FeatureClass.GeometryType);
            return layer;
        }

        //构建行政区划图层
        private ILayer BuildZoneLayer()
        {
            FeatureLayer layer = new FeatureLayer();
            layer.Name = StoneGroundDefines.LAYER_NAME_ZONE;
            layer.FeatureClass = SpatialDatabaseService.Instance.ZoneFeatureClass;
            IStrokeSymbol strokeSymbol = new StrokeSymbol();
            strokeSymbol.Pen.Color = Color.Red; //红色
            strokeSymbol.Pen.Width = 2;
            layer.SelectionSymbol = strokeSymbol;
            return layer;
        }

        //构建道路图层
        private ILayer BuildRoadLayer()
        {
            FeatureLayer layer = new FeatureLayer();
            layer.Name = StoneGroundDefines.LAYER_NAME_ROAD;
            layer.FeatureClass = SpatialDatabaseService.Instance.RoadFeatureClass;
            layer.SelectionSymbol = SelectionSymbolManager.Instance.GetSymbol(layer.FeatureClass.GeometryType);

            if (UserService.Instance.CurrentUser.UserType == SystemUserType.Support)
            {
                //线符号
                StrokeSymbol symbol = new StrokeSymbol();
                symbol.Name = "道路";
                symbol.NodeAvailable = false;
                symbol.Pen.Width = 10;
                symbol.Pen.Color = Color.FromArgb(128, 44, 163, 50);
                //图层方案
                SimpleRender render = new SimpleRender();
                render.Symbol = symbol;
                layer.Render = render;
            }

            return layer;
        }

        //构建料场图层
        private ILayer BuildStoneGroundLayer()
        {
            //图层方案
            SimpleRender render = new SimpleRender();
            render.Symbol = StoneGroundDatabaseService.Instance.Symbol;            
            //图层
            FeatureLayer layer = new FeatureLayer();
            layer.Name = StoneGroundDefines.LAYER_NAME_STONEGROUND;
            layer.Render = render;
            layer.SelectionSymbol = SelectionSymbolManager.Instance.GetSymbol(geoGeometryType.GEO_GEOMETRY_POINT);
            layer.FeatureClass = StoneGroundDatabaseService.Instance.StoneGroundFeatureClass;
            //图层标注
            layer.IsLabel = true;
            layer.LabelField = StoneGroundDatabaseService.FIELD_NAME;
            layer.LabelSymbol = StoneGroundDatabaseService.Instance.LabelSymbol;
            return layer;
        }

        #endregion

        //查找所有的地图瓦片文件
        private List<string> FindNmtp()
        {
            List<string> nmtps = new List<string>();
            string dir = System.IO.Path.Combine(System.Windows.Forms.Application.StartupPath, "map");
            string[] files = Directory.GetFiles(dir);
            foreach (string file in files)
            {
                if (file.EndsWith(".nmtp", StringComparison.OrdinalIgnoreCase))
                {
                    nmtps.Add(file);
                }
            }
            return nmtps;
        }

        private ITileProvider CreateTileProvider(List<string> nmtps)
        {
            ICombinationTileProvider combinationTileProvider = new CombinationTileProvider();
            foreach (string nmtp in nmtps)
            {
                string name = System.IO.Path.GetFileNameWithoutExtension(nmtp);
                string leveltotal = name.Substring(6);
                string[] levels = leveltotal.Split('-');
                if (levels.Count() == 1)
                {
                    ITileProvider provider = OpenNmtp(nmtp);
                    combinationTileProvider.AddProvider(levels[0], provider);
                }
                else if (levels.Count() == 2)
                {
                    int levelFrom = 0, levelTo = 0;
                    if (!int.TryParse(levels[0], out levelFrom))
                    {
                        LoggingService.Info("瓦片文件名字错误：" + name);
                        continue;
                    }
                    if (!int.TryParse(levels[1], out levelTo))
                    {
                        LoggingService.Info("瓦片文件名字错误：" + name);
                        continue;
                    }
                    ITileProvider provider = OpenNmtp(nmtp);
                    for (int i = levelFrom; i <= levelTo; i++)
                    {
                        combinationTileProvider.AddProvider(i.ToString(), provider);
                    }
                }                
            }
            return combinationTileProvider;
        }

        //打开瓦片
        private ITileProvider OpenNmtp(string file)
        {
            IConnectProperties connectProperties = new ConnectProperties();
            connectProperties.Database = file;
            ISpatialDatabaseFactory factory = new SqliteSpatialDatabaseFactory();
            ISpatialDatabase sdb = factory.Open(connectProperties);
            //底图
            ITable vecTable = sdb.OpenTable("tile");
            SampleDatabaseCache _vecCache = new SampleDatabaseCache(vecTable);
            Bitmap defaultBmp = new Bitmap(256, 256);
            using (Graphics g = Graphics.FromImage(defaultBmp))
            {
                g.Clear(TiandituTileSchema.GetBackColor("1")); //TODO 区分各个级别的瓦片
            }
            MemoryStream stream = new MemoryStream();
            defaultBmp.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
            byte[] defaultBmpData = StreamUtil.StreamToBytes(stream);
            stream.Dispose();
            defaultBmp.Dispose();
            //瓦片矢量背景
            ITileProvider provider = new FakeTileProvider(_vecCache, defaultBmpData); //给定了默认的背景图片
            return provider;
        }

        //设置瓦片图层的缩放比例
        private void SetDocumentScale(TileLayer layer, IMapDocument map)
        {
            ICombinationTileProvider provider = layer.TileSource.Provider as ICombinationTileProvider;
            string min = "2", max = "19";
            provider.GetMinAndMaxLevel(out min, out max);
            min = "6"; //最小限定为6级
            ITileSchema schema = layer.TileSource.Schema;
            map.MinScale = 1.0 / schema.Resolutions[min].UnitsPerPixel;
            map.MaxScale = 1.0 / schema.Resolutions[max].UnitsPerPixel;
        }
    }
}
