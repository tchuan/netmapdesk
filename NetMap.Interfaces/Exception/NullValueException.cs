﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace NetMap.Interfaces
{
    /// <summary>
    /// 空值错误
    /// </summary>
    [Serializable]
    public class NullValueException : ApplicationException
    {
        public NullValueException()
            : base()
        {
        }

        public NullValueException(string message)
            : base(message)
        {
        }

        public NullValueException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected NullValueException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
