﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using NetMap.Interfaces;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Render;
using NetMap.Render;
using NetMap.Geometry;
using System.Drawing.Drawing2D;

namespace NetMap.Utilities
{
    /// <summary>
    /// 将地图文档指定范围绘制在图片上
    /// </summary>
    public class ImageDrawHelper
    {
        private Image _board = null;

        public ImageDrawHelper(int width = 256, int height = 256)
        {
            _board = new Bitmap(width, height);
        }

        /// <summary>
        /// 重新设置图片大小
        /// </summary>
        public void ResetSize(int width, int height)
        {
            if (_board.Width == width && _board.Height == height)
            {
                return;
            }
            else
            {
                _board.Dispose();
                _board = new Bitmap(width, height);
            }
        }

        /// <summary>
        /// 绘制
        /// </summary>
        /// <param name="map">地图文档</param>
        /// <param name="box">空间范围</param>
        /// <returns>画板图片引用</returns>
        public Image Draw(IMapDocument map, IEnvelope box)
        {
            IDisplayTransformation trans = new DisplayTransformation();
            trans.MapBounds = map.Extent;
            trans.WindowBounds = new Envelope(0, 0, _board.Width, _board.Height);
            trans.ZoomToDataBounds(box);
            IDisplay display = new Display(Graphics.FromImage(_board), new SymbolRender(),
                null, trans);

            // 高质量的呈现地图图像
            display.G.SmoothingMode = SmoothingMode.AntiAlias;

            display.G.Clear(Color.White);

            ILayer layer = null;
            IDisplayTransformation displayTransformation = display.DisplayTransformation;
            for (int i = 0; i < map.LayerCount; i++)
            {
                layer = map.GetLayer(i);
                if (layer.Visible
                    && displayTransformation.ScaleRatio >= layer.MinScale
                    && displayTransformation.ScaleRatio <= layer.MaxScale)
                {
                    if (trans.ScaleRatio >= layer.MinScale
                        && trans.ScaleRatio <= layer.MaxScale)
                    {
                        layer.Draw(display);
                    }
                }
            }
            //绘制选择集
            for (int i = 0; i < map.LayerCount; i++)
            {
                layer = map.GetLayer(i);
                if (layer.Visible
                    && displayTransformation.ScaleRatio >= layer.MinScale
                    && displayTransformation.ScaleRatio <= layer.MaxScale
                    && (layer is IFeatureSelection))
                {
                    IFeatureSelection featureSelection = layer as IFeatureSelection;
                    featureSelection.DrawSelection(display);
                }
            }

            return _board;
        }
    }
}
