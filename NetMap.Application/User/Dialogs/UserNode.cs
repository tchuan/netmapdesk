﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Data;

namespace User.Dialogs
{
    internal class UserNode : Aga.Controls.Tree.Node
    {
        private IRow _row;

        public UserNode(IRow row)
        {
            _row = row;
        }

        public IRow Row
        {
            get { return _row; }
        }

        public string User
        {
            get { return _row.GetValue(0).ToString(); }
        }

        public string Pwd
        {
            get { return _row.GetValue(1).ToString(); }
        }

        public string UserType
        {
            get
            {
                string result = "未知";
                SystemUserType userType = (SystemUserType)(int)_row.GetValue(2);
                switch (userType)
                {
                    case SystemUserType.Support:
                        {
                            result = "技术支持";
                            break;
                        }
                    case SystemUserType.Admin:
                        {
                            result = "管理员";
                            break;
                        }
                    case SystemUserType.User:
                        {
                            result = "用户";
                            break;
                        }
                }
                return result;
            }
        }

        public string Information
        {
            get
            {
                return _row.GetValue(3).ToString();
            }
        }
    }

    internal class PermissionNode : Aga.Controls.Tree.Node
    {
        private IRow _row;

        public PermissionNode(IRow row)
        {
            _row = row;
        }

        public IRow Row { get { return _row; } }

        public string Cls { get { return _row.GetValue(0).ToString(); } }

        public string Label { get { return _row.GetValue(1).ToString(); } }

        public string UserType
        {
            get
            {
                string result = "未知";
                SystemUserType userType = (SystemUserType)(int)_row.GetValue(2);
                switch (userType)
                {
                    case SystemUserType.Support:
                        {
                            result = "技术支持";
                            break;
                        }
                    case SystemUserType.Admin:
                        {
                            result = "管理员";
                            break;
                        }
                    case SystemUserType.User:
                        {
                            result = "用户";
                            break;
                        }
                }
                return result;
            }
        }
    }
}
