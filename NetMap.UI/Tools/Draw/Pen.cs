﻿using NetMap.Interfaces;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Symbol;
using NetMap.Interfaces.Tracker;
using NetMap.Render;
using NetMap.Symbol;
using NetMap.Tracker;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetMap.Tools.Draw
{
    /// <summary>
    /// 钢笔工具
    /// </summary>
    public class Pen : Tool
    {
        private IDisplayTransformation _disTrans = null;
        private bool _isDown = false;
        private CreatePathTracker _tracker = null;

        public Pen()
            : base("钢笔")
        {
        }

        public Pen(IMapEditView editView)
            : base("钢笔", editView)
        {
        }

        /// <summary>
        /// 获取轨迹记录器
        /// </summary>
        public ITracker Tracker
        {
            get { return _tracker; }
        }

        public override void Init()
        {
            base.Init();
            _tracker = new CreatePathTracker();    
            //默认的画笔符号
            BoxPointSymbol pointSymbol = new BoxPointSymbol();
            pointSymbol.Fill = true;
            pointSymbol.FillColor = Color.White;
            pointSymbol.Size = 10;
            StrokeSymbol symbol = new StrokeSymbol();
            symbol.Pen.Width = 2;
            symbol.StartNodeSymbol = pointSymbol;
            symbol.EndNodeSymbol = pointSymbol;
            symbol.NodeAvailable = true;
            symbol.NodeSymbol = pointSymbol;
            symbol.IsAssistLine = true;            
            SolidFillSymbol finalSymbol = new SolidFillSymbol();
            finalSymbol.Color = Color.FromArgb(128, 128, 128, 128);
            finalSymbol.OutLine = symbol;
            _tracker.Symbol = finalSymbol;
        }

        public override void GainFocus()
        {
            _disTrans = View.DisplayTransformation;
            _isDown = false;
            base.GainFocus();
        }

        public override void LostFocus()
        {
            _disTrans = null;
            _isDown = false;
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                _tracker.Add(e.X, e.Y);
                _isDown = true;
                View.Refresh();
            }
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            if (_isDown)
            {
                _tracker.SetControlPoint(e.X, e.Y);
                View.Refresh();
            }
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (!_isDown) return;
                _isDown = false;
                _tracker.SetControlPoint(e.X, e.Y);
                View.Refresh();
            }
            else if (e.Button == MouseButtons.Right)
            {
                _tracker.Finish();
            }
        }

        //delete、backspace键退一个点
        //c键封闭
        public override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete ||
                e.KeyCode == Keys.Back)
            {
                _tracker.Back();
                View.Invalidate();
            }
            else if (e.KeyCode == Keys.C)
            {
                if (_tracker.IsClose)
                {
                    _tracker.Open();
                }
                else
                {
                    _tracker.Close();
                }
                View.Invalidate();
            }
        }

        public override void Draw(Graphics g)
        {
            _tracker.Draw(g);
        }
    }
}
