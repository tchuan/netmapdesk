﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Symbol;
using System.Drawing;

/*
 *   2014-04-23 仅提供在窗口坐标系下的绘图接口
 */

namespace NetMap.Interfaces.Tracker
{
    /// <summary>
    /// 橡皮条接口
    /// </summary>
    public interface ITracker
    {
        /// <summary>
        /// 表现橡皮条的符号
        /// </summary>
        ISymbol Symbol { get; set; }
        /// <summary>
        /// 重置
        /// </summary>
        void Reset();
        /// <summary>
        /// 刷新
        /// </summary>
        void Draw(Graphics g);
        /// <summary>
        /// 刷新
        /// </summary>
        /// <param name="display"></param>
        void Draw(IDisplay display);
        /// <summary>
        /// 获取是否已经开始创建图形
        /// </summary>
        /// <returns></returns>
        bool IsStarted { get; }
        /// <summary>
        /// 获取绘制的图形
        /// </summary>
        IGeometry GetGeometry();
        /// <summary>
        /// 橡皮条从起点
        /// </summary>
        /// <param name="point"></param>
        void Start(IPoint point);        
        /// <summary>
        /// 橡皮条到目标点
        /// </summary>
        /// <param name="point"></param>
        void MoveTo(IPoint point);
        /// <summary>
        /// 结束绘制
        /// </summary>
        void Finish();
    }
}
