﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Format;
using NetMap.Interfaces.Symbol;
using System.Drawing;
using NetMap.Interfaces.Persist;
using System.Xml;

namespace NetMap.Symbol
{
    /// <summary>
    /// 文本符号
    /// </summary>
    public class TextSymbol : Symbol, ITextSymbol, IBinaryFormat, IPersistResource
    {
        /// <summary>
        /// 默认的注记符号
        /// </summary>
        public TextSymbol()
        {
            Color = Color.FromArgb(255, 255, 255, 255); // 白色
            Height = 10;
            Width = 10;
            Font = "微软雅黑";
            Angle = 0;
            Rotate = 0;
            OffsetX = 0;
            OffsetY = 0;
        }

        public TextSymbol(TextSymbol symbol)
        {
            //TODO
        }

        public override ISymbol Clone()
        {
            return new TextSymbol(this);
        }

        public override SymbolType SymbolType
        {
            get { return SymbolType.GEO_SYMBOL_TEXT; }
        }

        public override string KindAbstract
        {
            get { return SymbolFactory.S_TEXT; }
        }

        public Color Color
        {
            get;
            set;
        }

        public double Height
        {
            get;
            set;
        }

        public double Width
        {
            get;
            set;
        }

        public string Font
        {
            get;
            set;
        }

        public double Angle { get; set; }

        public double Rotate { get; set; }

        public double OffsetX { get; set; }

        public double OffsetY { get; set; }

        #region IBinaryFormat 成员

        byte[] IBinaryFormat.ToWkb()
        {
            MemoryStream stream = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(stream);
            writer.Write(Name);
            writer.Write(Code);
            writer.Write(Color.A);
            writer.Write(Color.R);
            writer.Write(Color.G);
            writer.Write(Color.B);
            writer.Write(Height);
            writer.Write(Width);
            writer.Write(Font);
            return stream.ToArray();
        }

        void IBinaryFormat.FromWkb(byte[] wkb)
        {
            MemoryStream stream = new MemoryStream(wkb);
            BinaryReader reader = new BinaryReader(stream);
            Name = reader.ReadString();
            Code = reader.ReadInt32();
            byte a = reader.ReadByte();
            byte r = reader.ReadByte();
            byte g = reader.ReadByte();
            byte b = reader.ReadByte();
            Color = Color.FromArgb(a, r, g, b);
            Height = reader.ReadDouble();
            Width = reader.ReadDouble();
            Font = reader.ReadString();
        }

        #endregion

        #region IPersistResource 成员

        void IPersistResource.Save(XmlElement node, XmlDocument document)
        {
            XmlElement symbol = document.CreateElement("TextSymbol");
            symbol.SetAttribute("Name", Name);
            symbol.SetAttribute("Code", Code.ToString());
            symbol.SetAttribute("Color", string.Format("#{0:X2}{1:X2}{2:X2}", Color.R, Color.G, Color.B, Color.A));
            symbol.SetAttribute("Height", Height.ToString("%.2f"));
            symbol.SetAttribute("Width", Width.ToString("%.2f"));
            symbol.SetAttribute("Font", Font);
            node.AppendChild(symbol);
        }

        void IPersistResource.Load(XmlReader reader, object context)
        {
            Name = reader.GetAttribute("Name");
            Code = int.Parse(reader.GetAttribute("Code"));
            Color = ColorTranslator.FromHtml(reader.GetAttribute("Color"));
            Height = double.Parse(reader.GetAttribute("Height"));
            Width = double.Parse(reader.GetAttribute("Width"));
            Font = reader.GetAttribute("Font");
        }

        #endregion
    }
}
