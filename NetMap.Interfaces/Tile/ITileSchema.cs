﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjNet.CoordinateSystems;
using NetMap.Interfaces.Geometry;
using NetMap.Tile;

namespace NetMap.Interfaces.Tile
{
    /// <summary>
    /// Enumeration of possible axis directions
    /// </summary>
    /// <remarks>
    /// Direction is relative to the coordinate system in which the map is presented.
    /// <para/>
    /// InvertedX and InvertedXY do not exist yet, and may never.
    /// </remarks>
    public enum AxisDirection
    {
        /// <summary>
        /// The axis direction of the tiles match that of the map. This is used by TMS.
        /// </summary>
        Normal,

        /// <summary>
        /// The y-axis direction is inverted compared to that of the map. This is used by OpenStreetMap
        /// </summary>
        InvertedY
    }

    /// <summary>
    /// 切片定义接口
    /// </summary>
    public interface ITileSchema
    {
        string Name { get; }
        string Srs { get; }
        IEnvelope Extent { get; }
        int GetTileWidth(string levelId);
        int GetTileHeight(string levelId);
        double GetOriginX(string levelId);
        double GetOriginY(string levelId);
        int GetMatrixWidth(string levelId);
        int GetMatrixHeight(string levelId);
        IDictionary<string, Resolution> Resolutions { get; }
        string Format { get; }
        AxisDirection Axis { get; }
        /// <summary>
        /// 获取视图下包括的所有瓦片信息
        /// </summary>
        /// <param name="extent"></param>
        /// <param name="levelId"></param>
        /// <returns></returns>
        IEnumerable<TileInfo> GetTilesInView(IEnvelope extent, string levelId);
        IEnumerable<TileInfo> GetTilesInView(IEnvelope extent, double resolution);
        /// <summary>
        /// 获取视图下的瓦片范围
        /// </summary>
        /// <param name="extent"></param>
        /// <param name="levelId"></param>
        /// <returns></returns>
        TileRange GetTileRangeInView(IEnvelope extent, string levelId);
        TileRange GetTileRangeInView(IEnvelope extent, double resolution);
        IEnvelope GetExtentOfTilesInView(IEnvelope extent, string levelId);
        int GetMatrixFirstCol(string levelId);
        int GetMatrixFirstRow(string levelId);
    }
}
