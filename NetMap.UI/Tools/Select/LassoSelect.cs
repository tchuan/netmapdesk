﻿using NetMap.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Tools.Select
{
    /// <summary>
    /// 按套索选择
    /// </summary>
    public class LassoSelect : Tool
    {
        public LassoSelect()
            : base("按套索选择")
        {
        }

        public LassoSelect(IMapEditView view)
            : base("按套索选择", view)
        {
        }
    }
}
