﻿using NetMap.Interfaces.Tile;
using System;

namespace NetMap.Tile
{
    public class WebTileProvider : ITileProvider
    {
        private readonly ITileCache<byte[]> _tileCache;
        private readonly IRequest _request;
        private readonly Func<Uri, byte[]> _fetchTile;

        public WebTileProvider(IRequest request = null, ITileCache<byte[]> tileCache = null,
            Func<Uri, byte[]> fetchTile = null)
        {
            _request = request ?? new NullRequest();
            _tileCache = tileCache ?? new NullCache();
            _fetchTile = fetchTile ?? (RequestHelper.FetchImage);
        }

        public virtual byte[] GetTile(TileInfo tileInfo)
        {
            byte[] bytes = _tileCache.Find(tileInfo.Index);
            if (bytes == null)
            {
                bytes = _fetchTile(_request.GetUri(tileInfo));
                if (bytes != null) _tileCache.Add(tileInfo.Index, bytes);
            }
            return bytes;
        }

        public IRequest Request
        {
            get { return _request; }
        }
    }
}
