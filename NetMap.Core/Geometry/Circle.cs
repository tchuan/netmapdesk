﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;
using System.IO;

namespace NetMap.Geometry
{
    public class Circle : Geometry, ICircle
    {
        public Circle()
            : this(0, 0, 0)
        {
        }

        public Circle(double x, double y, double radius)
        {
            X = x;
            Y = y;
            Radius = radius;
            _box = new Envelope(x - radius, y - radius,
                x + radius, y + radius);
        }

        public Circle(ICircle other)
            : this(other.X, other.Y, other.Radius)
        {
        }

        #region ICircle 成员

        public double X
        {
            get;
            set;
        }

        public double Y
        {
            get;
            set;
        }

        public double Radius
        {
            get;
            set;
        }

        #endregion

        public override geoGeometryType GeometryType
        {
            get { return geoGeometryType.GEO_GEOMETRY_CIRCLE; }
        }

        public override void UpdateEnvelope()
        {
            _box.Update(X - Radius, Y - Radius,
                X + Radius, Y + Radius);
        }

        public override IGeometry CloneGeometry()
        {
            Circle circle = new Circle(this);
            return circle;
        }

        public override byte[] ExportToWkb()
        {
            Stream stream = new MemoryStream(24);
            BinaryWriter writer = new BinaryWriter(stream);
            writer.Write(X);
            writer.Write(Y);
            writer.Write(Radius);
            byte[] bytes = new byte[stream.Length];
            stream.Seek(0, SeekOrigin.Begin);
            stream.Read(bytes, 0, bytes.Length);
            stream.Close();
            return bytes;
        }

        public override string ExportToWkt()
        {
            throw new NotImplementedException();
        }

        public override void ImportFromWkb(byte[] data)
        {
            Stream stream = new MemoryStream(data);
            BinaryReader reader = new BinaryReader(stream);
            X = reader.ReadDouble();
            Y = reader.ReadDouble();
            Radius = reader.ReadDouble();
            reader.Close();
            stream.Close();
        }

        public override void ImportFromWkt(string data)
        {
            //TODO
        }

        public override void Export(BinaryWriter writer)
        {
            writer.Write(X);
            writer.Write(Y);
            writer.Write(Radius);
        }

        public override void Import(BinaryReader reader)
        {
            X = reader.ReadDouble();
            Y = reader.ReadDouble();
            Radius = reader.ReadDouble();
        }

        public override string ToString()
        {
            return string.Format("Circle:{0},{1},{2}", X, Y, Radius);
        }
    }
}
