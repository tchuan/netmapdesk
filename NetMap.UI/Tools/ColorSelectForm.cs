﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetMap.Tools
{
    public partial class ColorSelectForm : Form
    {
        public ColorSelectForm()
        {
            InitializeComponent();
            FormClosing += new FormClosingEventHandler(ColorSelectForm_FormClosing);
        }

        void ColorSelectForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Hide();
            e.Cancel = true;
        }
    }
}
