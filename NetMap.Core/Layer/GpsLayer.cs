﻿using NetMap.Geometry;
using NetMap.Interfaces;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Symbol;
using NetMap.Render;
using NetMap.Symbol;
using ProjNet.CoordinateSystems;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace NetMap
{
    public class GpsLayer : Layer, IGpsLayer
    {
        private IStrokeSymbol _symbol;
        private ILineString _lineString;
        private ISymbolDrawer _drawer;

        public GpsLayer()
        {
            _lineString = new LineString();

            //符号
            StrokeSymbol symbol = new StrokeSymbol();
            symbol.Pen.Color = Color.Green;
            symbol.Pen.Width = 2;
            PolygonPointSymbol endSymbol = PolygonPointSymbol.NewWellknownArrow;
            endSymbol.Fill = true;
            endSymbol.FillColor = Color.Blue;
            endSymbol.Size = 20;
            endSymbol.Outline = false;
            symbol.EndNodeSymbol = endSymbol;
            _symbol = symbol;

            _drawer = SymbolDrawerFactory.Instance.GetDrawer(_symbol);
        }

        #region IGpsLayer 成员

        public IStrokeSymbol Symbol
        {
            get
            {
                return _symbol;
            }
            set
            {
                _symbol = value;
            }
        }

        public void AddGpsPoint(IDirectionPoint point)
        {
            _lineString.Add(point);
        }

        public void ClearGpsPoint()
        {
            _lineString.Clear();
        }

        #endregion

        public override void Draw(IDisplay display)
        {
            if (_lineString.Count > 1)
            {
                _drawer.Draw(display, _symbol, _lineString, "");
            }
        }

        public override IEnvelope Extent
        {
            get
            {
                _lineString.UpdateEnvelope();
                return _lineString.Envelope;
            }
        }

        //TODO wgs84坐标系
        public override ICoordinateSystem CoordinateSystem
        {
            get
            {
                return null;
            }
        }
    }
}
