﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Format;
using NetMap.Interfaces.Symbol;
using System.Drawing;
using System.Drawing.Drawing2D;
using NetMap.Interfaces.Persist;
using System.Xml;

namespace NetMap.Symbol
{
    /// <summary>
    /// 线划线符号
    /// </summary>
    public class StrokeSymbol : LineSymbol, IStrokeSymbol, IBinaryFormat, IPersistResource
    {
        public StrokeSymbol()
        {
            Pen = new Pen(Color.Black, 1);
        }

        public StrokeSymbol(IStrokeSymbol symbol)
        {
            Pen = symbol.Pen;
        }       

        public override ISymbol Clone()
        {
            return new StrokeSymbol(this);
        }

        public override string KindAbstract
        {
            get { return SymbolFactory.S_LINE_STROKE; }
        }

        public Pen Pen
        {
            get;
            set;
        }

        #region IBinaryFormat 成员

        public byte[] ToWkb()
        {
            MemoryStream stream = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(stream);

            writer.Write(Name);
            writer.Write(Code);

            //首节点
            if (StartNodeSymbol != null)
            {
                writer.Write(StartNodeSymbol.KindAbstract);
                IBinaryFormat snsFormat = StartNodeSymbol as IBinaryFormat;
                byte[] snsWkb = snsFormat.ToWkb();
                writer.Write(snsWkb.Count());
                writer.Write(snsWkb);
            }
            else
            {
                writer.Write(SymbolFactory.S_NULL);
            }
            //尾节点
            if (EndNodeSymbol != null)
            {
                writer.Write(EndNodeSymbol.KindAbstract);
                IBinaryFormat ensFormat = EndNodeSymbol as IBinaryFormat;
                byte[] ensWkb = ensFormat.ToWkb();
                writer.Write(ensWkb.Count());
                writer.Write(ensWkb);
            }
            else
            {
                writer.Write(SymbolFactory.S_NULL);
            }
            //节点
            if (NodeSymbol != null)
            {
                writer.Write(NodeSymbol.KindAbstract);
                IBinaryFormat nsFormat = NodeSymbol as IBinaryFormat;
                byte[] nsWkb = nsFormat.ToWkb();
                writer.Write(nsWkb.Count());
                writer.Write(nsWkb);
            }
            else
            {
                writer.Write(SymbolFactory.S_NULL);
            }
            //附加节点
            if (AdditionalNodeSymbol != null)
            {
                writer.Write(AdditionalNodeSymbol.KindAbstract);
                IBinaryFormat ansFormat = AdditionalNodeSymbol as IBinaryFormat;
                byte[] ansWkb = ansFormat.ToWkb();
                writer.Write(ansWkb.Count());
                writer.Write(ansWkb);
            }
            else
            {
                writer.Write(SymbolFactory.S_NULL);
            }
            //边上节点
            if (OnEdgeNodeSymbol != null)
            {
                writer.Write((OnEdgeNodeSymbol as ISymbol).KindAbstract);
                IBinaryFormat onsFormat = OnEdgeNodeSymbol as IBinaryFormat;
                byte[] onsWkb = onsFormat.ToWkb();
                writer.Write(onsWkb.Count());
                writer.Write(onsWkb);
            }
            else
            {
                writer.Write(SymbolFactory.S_NULL);
            }

            writer.Write(NodeAvailable);
            writer.Write(AdditionalNodeAvailable);
            writer.Write(OnEdgeNodeAvailable);

            writer.Write(Pen.Width);
            writer.Write(Pen.Color.ToArgb());
            //writer.Write((int)Pen.LineCap);
            writer.Write((int)Pen.LineJoin);
            if (Pen.DashPattern != null)
            {
                writer.Write(Pen.DashPattern.Count());
                foreach (float item in Pen.DashPattern)
                {
                    writer.Write(item);
                }
            }
            else
            {
                writer.Write(0);
            }
            writer.Write(Pen.DashOffset);
            writer.Write((int)Pen.DashCap);

            return stream.ToArray();
        }

        public void FromWkb(byte[] wkb)
        {
            MemoryStream stream = new MemoryStream(wkb);
            BinaryReader reader = new BinaryReader(stream);

            Name = reader.ReadString();
            Code = reader.ReadInt32();

            string sk = reader.ReadString();
            ISymbol snsSymbol = SymbolFactory.CreateSymbol(sk);
            if (snsSymbol == null)
            {
                StartNodeSymbol = null;
            }
            else
            {
                StartNodeSymbol = snsSymbol as IPointSymbol;
                IBinaryFormat snsFormat = StartNodeSymbol as IBinaryFormat;
                int count = reader.ReadInt32();
                byte[] snsWkb = reader.ReadBytes(count);
                snsFormat.FromWkb(snsWkb);
            }

            sk = reader.ReadString();
            ISymbol ensSymbol = SymbolFactory.CreateSymbol(sk);
            if (ensSymbol == null)
            {
                EndNodeSymbol = null;
            }
            else
            {
                EndNodeSymbol = ensSymbol as IPointSymbol;
                IBinaryFormat ensFormat = EndNodeSymbol as IBinaryFormat;
                int count = reader.ReadInt32();
                byte[] ensWkb = reader.ReadBytes(count);
                ensFormat.FromWkb(ensWkb);
            }

            sk = reader.ReadString();
            ISymbol nsSymbol = SymbolFactory.CreateSymbol(sk);
            if (nsSymbol == null)
            {
                NodeSymbol = null;
            }
            else
            {
                NodeSymbol = nsSymbol as IPointSymbol;
                IBinaryFormat nsFormat = NodeSymbol as IBinaryFormat;
                int count = reader.ReadInt32();
                byte[] nsWkb = reader.ReadBytes(count);
                nsFormat.FromWkb(nsWkb);
            }

            sk = reader.ReadString();
            ISymbol ansSymbol = SymbolFactory.CreateSymbol(sk);
            if (ansSymbol == null)
            {
                AdditionalNodeSymbol = null;
            }
            else
            {
                AdditionalNodeSymbol = ansSymbol as IPointSymbol;
                IBinaryFormat ansFormat = AdditionalNodeSymbol as IBinaryFormat;
                int count = reader.ReadInt32();
                byte[] ansWkb = reader.ReadBytes(count);
                ansFormat.FromWkb(ansWkb);
            }

            sk = reader.ReadString();
            ISymbol onsSymbol = SymbolFactory.CreateSymbol(sk);
            if (onsSymbol == null)
            {
                OnEdgeNodeSymbol = null;
            }
            else
            {
                OnEdgeNodeSymbol = onsSymbol as IOnEdgePointSymbol;
                IBinaryFormat onsFormat = OnEdgeNodeSymbol as IBinaryFormat;
                int count = reader.ReadInt32();
                byte[] onsWkb = reader.ReadBytes(count);
                onsFormat.FromWkb(onsWkb);
            }

            NodeAvailable = reader.ReadBoolean();
            AdditionalNodeAvailable = reader.ReadBoolean();
            OnEdgeNodeAvailable = reader.ReadBoolean();

            Pen.Width = reader.ReadSingle();
            int color = reader.ReadInt32();
            Pen.Color = Color.FromArgb(color);
            Pen.SetLineCap((LineCap)reader.ReadInt32(),
                (LineCap)reader.ReadInt32(),
                (DashCap)reader.ReadInt32());
            Pen.LineJoin = (LineJoin)reader.ReadInt32();
            int dashArrayCount = reader.ReadInt32();
            if (dashArrayCount > 0)
            {
                Pen.DashPattern = new float[dashArrayCount];
                for (int i = 0; i < dashArrayCount; i++)
                {
                    Pen.DashPattern[i] = reader.ReadSingle();
                }
            }
            else
            {
                Pen.DashPattern = null;
            }
            Pen.DashOffset = reader.ReadSingle();
            Pen.DashCap = (DashCap)reader.ReadInt32();
        }

        #endregion

        #region IPersistResource 接口

        public void Save(XmlElement node, XmlDocument document)
        {
            XmlElement symbol = document.CreateElement("StrokeSymbol");
            symbol.SetAttribute("Name", Name);
            symbol.SetAttribute("Code", Code.ToString());
            //首节点符号
            if (StartNodeSymbol != null)
            {
                symbol.SetAttribute("StartNodeSymbol", StartNodeSymbol.Code.ToString());
            }
            //尾节点符号
            if (EndNodeSymbol != null)
            {
                symbol.SetAttribute("EndNodeSymbol", EndNodeSymbol.Code.ToString());
            }
            //节点
            if (NodeSymbol != null)
            {
                symbol.SetAttribute("NodeSymbol", NodeSymbol.Code.ToString());
            }
            //附加节点
            if (AdditionalNodeSymbol != null)
            {
                symbol.SetAttribute("AdditionalNodeSymbol", AdditionalNodeSymbol.Code.ToString());
            }
            //边上节点
            if (OnEdgeNodeSymbol != null)
            {
                //TODO
            }

            symbol.SetAttribute("NodeAvailable", NodeAvailable.ToString());
            symbol.SetAttribute("AdditionalNodeAvailable", AdditionalNodeAvailable.ToString());
            symbol.SetAttribute("OnEdgeNodeAvailable", OnEdgeNodeAvailable.ToString());

            XmlElement penNode = document.CreateElement("Pen");
            SymbolXmlUtil.XmlPen(penNode, Pen);
            symbol.AppendChild(penNode);
            node.AppendChild(symbol);
        }

        public void Load(XmlReader reader, object context)
        {
            Name = reader.GetAttribute("Name");
            Code = int.Parse(reader.GetAttribute("Code"));

            ISymbolLibrary library = context as ISymbolLibrary;
            StartNodeSymbol = library.FindSymbol(int.Parse(reader.GetAttribute("StartNodeSymbol"))) as IPointSymbol;
            EndNodeSymbol = library.FindSymbol(int.Parse(reader.GetAttribute("EndNodeSymbol"))) as IPointSymbol;
            NodeSymbol = library.FindSymbol(int.Parse(reader.GetAttribute("NodeSymbol"))) as IPointSymbol;
            AdditionalNodeSymbol = library.FindSymbol(int.Parse(reader.GetAttribute("AdditionalNodeSymbol"))) as IPointSymbol;
            //TODO OnEdgeNode

            NodeAvailable = bool.Parse(reader.GetAttribute("NodeAvailable"));
            AdditionalNodeAvailable = bool.Parse(reader.GetAttribute("AdditionalNodeAvailable"));
            OnEdgeNodeAvailable = bool.Parse(reader.GetAttribute("OnEdgeNodeAvailable"));

            while (reader.Read())
            {
                if(reader.IsStartElement())
                {
                    if (reader.Name == "Pen")
                    {
                        Pen = SymbolXmlUtil.ParsePen(reader);
                    }
                }
                else if (reader.NodeType == XmlNodeType.EndElement
                    && reader.Name == "StrokeSymbol")
                {
                    break;
                }
            }
        }

        #endregion

        /// <summary>
        /// 默认新实例
        /// </summary>
        public static StrokeSymbol NewDefault
        {
            get
            {
                return new StrokeSymbol();
            }
        }
    }
}
