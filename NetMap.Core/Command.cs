﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces;
using System.Drawing;

namespace NetMap.Interfaces
{
    /// <summary>
    /// 命令基类
    /// </summary>
    public abstract class Command : ICommand
    {
        protected string _name = string.Empty;
        protected bool _enable = true;
        protected IMapView _view = null;
        protected Image _icon = null;
        protected string _groupName = "NetMap工具集";
        protected string _description = string.Empty;

        public Command(string name)
        {
            _name = name;
            SetIcon();
        }

        public Command(string name, IMapView view)
        {
            _view = view;
            _name = name;
            SetIcon();
        }

        protected virtual void SetIcon()
        {
        }

        #region ICommond 成员

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public string GroupName
        {
            get
            {
                return _groupName;
            }
            set
            {
                _groupName = value;
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }

        public bool IsEnable
        {
            get
            {
                return _enable;
            }
            set
            {
                _enable = value;
            }
        }

        public virtual void Init()
        {
            //do nothing
        }

        public virtual void Execute()
        {
            //默认为空
        }      

        public IMapView View
        {
            get 
            {
                return _view;
            }
            set
            {
                _view = value;
            }
        }

        public Image Icon
        {
            get { return _icon; }
        }

        public byte[] UserData
        {
            get;
            set;
        }

        #endregion
    }
}
