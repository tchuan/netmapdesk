﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjNet.CoordinateSystems.Transformations;
using NetMap.Geometry;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Render;

namespace NetMap.Render
{
    public class YDisplayTransformation : IDisplayTransformation, IYDisplayTransformation
    {
        private IEnvelope _mapBounds = null;
        private IEnvelope _windowBounds = null;

        public YDisplayTransformation()
        {
            _mapBounds = new Envelope(-180, -90, 180, 90);
            _windowBounds = new Envelope(0, 0, 400, 300);
            VisibleMapBounds = new Envelope(-180, -90, 180, 90);
            ScaleRatio = 1;

            YZoom = 0.5;
            YAngle = 45;
        }

        #region IDisplayTransformation 接口

        public IEnvelope MapBounds
        {
            get { return _mapBounds; }
            set
            {
                _mapBounds.Update(value);
            }
        }

        public IEnvelope VisibleMapBounds
        {
            get;
            private set;
        }

        public IEnvelope WindowBounds
        {
            get
            {
                return _windowBounds;
            }
            set
            {
                _windowBounds.Update(value);
            }
        }

        public double ScaleRatio
        {
            get;
            private set;
        }

        public void ZoomToScale(double scale)
        {
            ScaleRatio = scale;
            IPoint center = VisibleMapBounds.Center;
            VisibleMapBounds.XMin = center.X - 0.5 * WindowBounds.Width / ScaleRatio;
            VisibleMapBounds.YMin = center.Y - 0.5 * WindowBounds.Height / ScaleRatio;
            VisibleMapBounds.XMax = center.X + 0.5 * WindowBounds.Width / ScaleRatio;
            VisibleMapBounds.YMax = center.Y + 0.5 * WindowBounds.Height / ScaleRatio;

            RecalCenter();
        }

        public void ZoomToWindow(IEnvelope window)
        {
            WindowBounds.Update(window);
            IPoint center = VisibleMapBounds.Center;
            VisibleMapBounds.XMin = center.X - 0.5 * WindowBounds.Width / ScaleRatio;
            VisibleMapBounds.YMin = center.Y - 0.5 * WindowBounds.Height / ScaleRatio;
            VisibleMapBounds.XMax = center.X + 0.5 * WindowBounds.Width / ScaleRatio;
            VisibleMapBounds.YMax = center.Y + 0.5 * WindowBounds.Height / ScaleRatio;

            RecalCenter();
        }

        public void ZoomToDataBounds(IEnvelope bounds)
        {
            IPoint center = bounds.Center;
            ScaleRatio = Math.Min(WindowBounds.Width / bounds.Width, WindowBounds.Height / bounds.Height);
            VisibleMapBounds.XMin = center.X - 0.5 * WindowBounds.Width / ScaleRatio;
            VisibleMapBounds.YMin = center.Y - 0.5 * WindowBounds.Height / ScaleRatio;
            VisibleMapBounds.XMax = center.X + 0.5 * WindowBounds.Width / ScaleRatio;
            VisibleMapBounds.YMax = center.Y + 0.5 * WindowBounds.Height / ScaleRatio;

            RecalCenter();
        }

        public void ZoomToMapBounds()
        {
            IPoint center = MapBounds.Center;
            ScaleRatio = Math.Min(WindowBounds.Width / MapBounds.Width, WindowBounds.Height / MapBounds.Height);
            VisibleMapBounds.XMin = center.X - 0.5 * WindowBounds.Width / ScaleRatio;
            VisibleMapBounds.YMin = center.Y - 0.5 * WindowBounds.Height / ScaleRatio;
            VisibleMapBounds.XMax = center.X + 0.5 * WindowBounds.Width / ScaleRatio;
            VisibleMapBounds.YMax = center.Y + 0.5 * WindowBounds.Height / ScaleRatio;

            RecalCenter();
        }

        public void ZoomToCenter(double x, double y, double scale)
        {
            ScaleRatio = scale;
            VisibleMapBounds.XMin = x - 0.5 * WindowBounds.Width / ScaleRatio;
            VisibleMapBounds.YMin = y - 0.5 * WindowBounds.Height / ScaleRatio;
            VisibleMapBounds.XMax = x + 0.5 * WindowBounds.Width / ScaleRatio;
            VisibleMapBounds.YMax = y + 0.5 * WindowBounds.Height / ScaleRatio;

            RecalCenter();
        }

        public ICoordinateTransformation CoordinateTransformation
        {
            get;
            set;
        }

        public ICoordinateTransformation ReverseCoordinateTransformation
        {
            get;
            set;
        }

        private double cx = 0;
        private double cy = 0;
        //重新计算中心点
        private void RecalCenter()
        {         
            if (CoordinateTransformation != null)
            {
                CoordinateTransformation.MathTransform.Transform(MapBounds.Center.X, MapBounds.Center.Y, ref cx, ref cy);
            }
            cx = ScaleRatio * (cx - VisibleMapBounds.XMin);
            cy = WindowBounds.Height - ScaleRatio * (cy - VisibleMapBounds.YMin);
        }

        public void FromMapPoint(double mapX, double mapY, ref double windowX, ref double windowY)
        {            
            //目标点
            if (CoordinateTransformation != null)
            {
                CoordinateTransformation.MathTransform.Transform(mapX, mapY, ref mapX, ref mapY);
            }
            windowX = ScaleRatio * (mapX - VisibleMapBounds.XMin);
            windowY = WindowBounds.Height - ScaleRatio * (mapY - VisibleMapBounds.YMin);
            //Y转换
            windowX += (cy - windowY) * Math.Tan(YAngle);
            windowY += (cy - windowY) * YZoom;
        }

        public void ToMapPoint(double windowX, double windowY, ref double mapX, ref double mapY)
        {
            //Y转换
            windowY = (windowY - cy * YZoom) / (1 - YZoom);
            windowX -= (cy - windowY) * Math.Tan(YAngle);
            //目标点
            mapX = windowX / ScaleRatio + VisibleMapBounds.XMin;
            mapY = (WindowBounds.Height - windowY) / ScaleRatio + VisibleMapBounds.YMin;
            if (ReverseCoordinateTransformation != null)
            {
                CoordinateTransformation.MathTransform.Transform(mapX, mapY, ref mapX, ref mapY);
            }  
        }

        public IPoint FromMapPoint(IPoint point)
        {
            double x = 0, y = 0;
            FromMapPoint(point.X, point.Y, ref x, ref y);
            return new Point(x, y);
        }

        public IList<IPoint> FromMapPoint(IList<IPoint> pts)
        {
            IList<IPoint> ptsRe = new List<IPoint>();
            foreach (IPoint point in pts)
            {
                ptsRe.Add(FromMapPoint(point));
            }
            return ptsRe;
        }

        #endregion

        #region IYDisplayTransformation 接口
        
        public double YZoom
        {
            get;
            set;
        }

        public double YAngle
        {
            get;
            set;
        }

        #endregion
    }
}
