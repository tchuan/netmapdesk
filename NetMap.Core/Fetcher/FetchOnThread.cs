﻿// Copyright 2010 - Paul den Dulk (Geodan)
//
// This file is part of Mapsui.
// Mapsui is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// Mapsui is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Mapsui; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 

using System;
using NetMap.Tile;
using NetMap.Interfaces.Tile;
using System.Drawing;

/*
 * modify: 2013-12-26 修改完成信息的参数
 */

namespace NetMap.Fetcher
{
    class FetchOnThread
        //This class is needed because in CF one can only pass arguments to a thread using a class constructor.
        //Once support for CF is dropped (replaced by SL on WinMo?) this class should be removed.
    {
        readonly ITileProvider tileProvider;
        readonly TileInfo tileInfo;
        /// <summary>
        /// 瓦片获取完成事件
        /// </summary>
        readonly FetchTileCompletedEventHandler fetchTileCompleted;

        public FetchOnThread(ITileProvider tileProvider, TileInfo tileInfo, FetchTileCompletedEventHandler fetchTileCompleted)
        {
            this.tileProvider = tileProvider;
            this.tileInfo = tileInfo;
            this.fetchTileCompleted = fetchTileCompleted;
        }

        public void FetchTile(object state)
        {
            byte[] image = null;
            if (tileProvider != null)
            {
                image = tileProvider.GetTile(tileInfo);
            }
            fetchTileCompleted(this, new FetchTileCompletedEventArgs(false, tileInfo, image));
        }
    }

    public delegate void FetchTileCompletedEventHandler(object sender, FetchTileCompletedEventArgs e);

    public class FetchTileCompletedEventArgs
    {
        public FetchTileCompletedEventArgs(bool cancelled, TileInfo tileInfo, byte[] image)
        {
            Cancelled = cancelled;
            TileInfo = tileInfo;
            Image = image;
        }

        public readonly bool Cancelled;
        public readonly TileInfo TileInfo;
        public readonly byte[] Image;
    }
}
