﻿using System;
using System.Windows.Forms;
using NetMap.Application.Core;

namespace StoneGround.Analyze.Dialog
{
    /// <summary>
    /// 经纬度输入窗口
    /// </summary>
    public partial class RadiusInput : Form
    {
        public RadiusInput()
        {
            InitializeComponent();
            Radius = 50;
        }

        /// <summary>
        /// 获取服务半径
        /// </summary>
        public double Radius
        {
            get;
            private set;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            double value = 0;
            if (double.TryParse(textBoxRadius.Text, out value))
            {
                Radius = value;
            }
            else
            {
                MessageService.ShowMessage("请输入有效半径");
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
