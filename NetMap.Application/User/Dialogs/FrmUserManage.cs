﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NetMap.Data;
using NetMap.Interfaces.Data;

namespace User.Dialogs
{
    public partial class FrmUserManage : Form
    {
        private Aga.Controls.Tree.NodeControls.NodeTextBox _nodeUser;
        private Aga.Controls.Tree.NodeControls.NodeTextBox _nodePwd;
        private Aga.Controls.Tree.NodeControls.NodeTextBox _nodeUserType;
        private Aga.Controls.Tree.NodeControls.NodeTextBox _nodeInfo;
        private NodeButton _nodeEdit;
        private NodeButton _nodeDelete;
        private Aga.Controls.Tree.TreeModel _model;

        public FrmUserManage()
        {
            InitializeComponent();

            _nodeUser = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            _nodePwd = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            _nodeUserType = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            _nodeInfo = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            _nodeEdit = new NodeButton("修改");
            _nodeDelete = new NodeButton("删除");
            treeViewAdv1.NodeControls.Add(_nodeUser);
            treeViewAdv1.NodeControls.Add(_nodePwd);
            treeViewAdv1.NodeControls.Add(_nodeUserType);
            treeViewAdv1.NodeControls.Add(_nodeInfo);
            treeViewAdv1.NodeControls.Add(_nodeEdit);
            treeViewAdv1.NodeControls.Add(_nodeDelete);

            int leftMargin = 30;

            _nodeUser.DataPropertyName = "User";
            _nodeUser.EditEnabled = false;
            _nodeUser.ParentColumn = treeColumn1;

            _nodePwd.DataPropertyName = "Pwd";
            _nodePwd.EditEnabled = false;
            _nodePwd.LeftMargin = leftMargin;
            _nodePwd.ParentColumn = treeColumn2;

            _nodeUserType.DataPropertyName = "UserType";
            _nodeUserType.EditEnabled = false;
            _nodeUserType.LeftMargin = leftMargin;
            _nodeUserType.ParentColumn = treeColumn3;

            _nodeInfo.DataPropertyName = "Information";
            _nodeInfo.EditEnabled = false;
            _nodeInfo.LeftMargin = leftMargin;
            _nodeInfo.ParentColumn = treeColumn4;

            _nodeEdit.ParentColumn = treeColumn5;            
            _nodeDelete.ParentColumn = treeColumn5;

            _nodeEdit.BtnClick += new EventHandler(_nodeEdit_BtnClick);
            _nodeDelete.BtnClick += new EventHandler(_nodeDelete_BtnClick);

            this.treeViewAdv1.ContextMenuStrip = new System.Windows.Forms.ContextMenuStrip();
            ToolStripMenuItem menu = new ToolStripMenuItem("新增");
            this.treeViewAdv1.ContextMenuStrip.Items.Add(menu);
            menu.Click += new EventHandler(menu_Click);

            LoadData();
        }        

        #region 编辑

        void menu_Click(object sender, EventArgs e)
        {
            FrmUser dialog = new FrmUser();
            dialog.StartPosition = FormStartPosition.CenterParent;
            dialog.InitNew();
            dialog.ShowDialog(this);
        }

        void _nodeDelete_BtnClick(object sender, EventArgs e)
        {
            Aga.Controls.Tree.TreeNodeAdv node = treeViewAdv1.SelectedNode;
            if (node == null)
            {
                return;
            }
            UserNode userNode = node.Tag as UserNode;
            _model.Nodes.Remove(userNode);
            userNode.Row.Delete();
        }

        void _nodeEdit_BtnClick(object sender, EventArgs e)
        {
            Aga.Controls.Tree.TreeNodeAdv node = treeViewAdv1.SelectedNode;
            if (node == null)
            {
                return;
            }
            UserNode userNode = node.Tag as UserNode;
            FrmUser dialog = new FrmUser();
            dialog.StartPosition = FormStartPosition.CenterParent;
            dialog.InitEdit(userNode.Row);
            dialog.ShowDialog(this);
        }

        #endregion

        private void LoadData()
        {
            _model = new Aga.Controls.Tree.TreeModel();

            IQueryFilter filter = new QueryFilter();
            filter.Fields.Add(UserService.FIELD_USER_ID);
            filter.Fields.Add(UserService.FIELD_USER_PWD);
            filter.Fields.Add(UserService.FIELD_USER_TYPE);
            filter.Fields.Add(UserService.FIELD_USER_INFO);
            ICursor cursor = UserService.Instance.UserTable.Search(filter);
            IRow row = cursor.Next();
            int index = row.Fields.FindField(UserService.FIELD_USER_TYPE);
            while (row != null)
            {
                //只加载小于当前用户权限的用户
                int userType = (int)row.GetValue(index);
                if (userType > (int)UserService.Instance.CurrentUser.UserType)
                {
                    UserNode node = new UserNode(row);
                    _model.Nodes.Add(node);
                }
                row = cursor.Next();
            }
            cursor.Close();

            this.treeViewAdv1.Model = _model;
        }
    }
}
