﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Symbol;
using NetMap.Interfaces.Data;
using NetMap.Interfaces;
using System.IO;
using NetMap.Interfaces.Format;
using NetMap.Symbol;
using NetMap.Interfaces.Persist;
using System.Xml;

namespace NetMap.Render
{
    public class SimpleRender : IFeatureRender, ISimpleRender, IDisposable, IBinaryFormat, IPersistResource
    {
        private ISymbol _symbol = null;
        private ITextSymbol _textSymbol = null;

        public SimpleRender()
        {
        }

        public SimpleRender(ISymbol symbol)
        {
            Symbol = symbol;
        }

        #region IFeatureRender 成员

        public void DrawCursor(IDisplay display, IFeatureCursor cursor, IRenderCancel cancel)
        {
            if (_symbol == null)
            {
                return;
            }
            ISymbolRender symbolRender = display.SymbolRender;
            symbolRender.Setup(display);
            IFeatureClass featureClass = cursor.FeatureClass;
            geoFeatureType fType = featureClass.FeatureType;
            if(fType == geoFeatureType.GEO_FEATURE_ANNOTATION)
            {
                if(_symbol.SymbolType != SymbolType.GEO_SYMBOL_TEXT)
                {
                    return; //注记要素要求文本符号
                }
                else
                {
                    _textSymbol = _symbol as ITextSymbol;
                }
            }
            IFeature feature = cursor.Next();
            while (feature != null)
            {
                switch(fType)
                {
                    case geoFeatureType.GEO_FEATURE_SIMPLE:
                        {
                            symbolRender.Draw(_symbol, feature.Geometry);
                            break;
                        }
                    case geoFeatureType.GEO_FEATURE_ANNOTATION:
                        {
                            symbolRender.DrawText(_textSymbol, feature.Geometry, (feature as ITextFeature).Text);
                            break;
                        }
                }                
                feature = cursor.Next();
            }
        }

        #endregion

        #region ISimpleRender 成员

        public string Name
        {
            get;
            set;
        }

        public ISymbol Symbol
        {
            get
            {
                return _symbol;
            }
            set
            {
                //符号资源清理
                if (_symbol != null)
                {
                    if (_symbol is IDisposable)
                    {
                        (_symbol as IDisposable).Dispose();
                    }
                }
                //更新符号
                _symbol = value;
                if (_symbol is ITextSymbol)
                {
                    _textSymbol = _symbol as ITextSymbol;
                }
            }
        }

        #endregion

        #region IDisposable 成员

        public void Dispose()
        {
            if (_symbol != null)
            {
                //_symbol.Clear(); //TODO 修改
                _symbol = null;
            }
        }

        #endregion        
    
        #region IBinaryFormat 成员

        public byte[] ToWkb()
        {
            MemoryStream stream = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(stream);

            if (_symbol == null)
            {
                writer.Write(SymbolFactory.S_NULL);
            }
            else
            {
                writer.Write(_symbol.KindAbstract);
                IBinaryFormat format = _symbol as IBinaryFormat;
                byte[] wkb = format.ToWkb();
                writer.Write(wkb.Count());
                writer.Write(wkb);                
            }
            writer.Dispose();

            return stream.ToArray();
        }

        public void FromWkb(byte[] wkb)
        {
            MemoryStream stream = new MemoryStream(wkb);
            BinaryReader reader = new BinaryReader(stream);

            string sk = reader.ReadString();
            _symbol = SymbolFactory.CreateSymbol(sk);
            if (_symbol != null)
            {
                int count = reader.ReadInt32();
                byte[] iwkb = reader.ReadBytes(count);
                IBinaryFormat format = _symbol as IBinaryFormat;
                format.FromWkb(iwkb);
            }
            reader.Dispose();
            stream.Dispose();
        }

        #endregion

        #region IPersistResource 成员

        public void Save(XmlElement node, XmlDocument document)
        {
            XmlElement render = document.CreateElement("SimpleRender");
            render.SetAttribute("Name", Name);
            render.SetAttribute("Symbol", Symbol.Code.ToString());
            node.AppendChild(render);
        }

        public void Load(XmlReader reader, object context)
        {
            ISymbolLibrary library = context as ISymbolLibrary;
            Name = reader.GetAttribute("Name");
            int code = int.Parse(reader.GetAttribute("Symbol"));
            Symbol = library.FindSymbol(code);
        }

        #endregion
    }
    
}
