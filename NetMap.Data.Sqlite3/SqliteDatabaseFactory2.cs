﻿using Community.CsharpSqlite;
using NetMap.Interfaces.Data;
using NetMap.Sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*
 * create: 2014-06-17 加密的Sqlite3数据库
 */

namespace NetMap.Sqlite
{
    public class SqliteDatabaseFactory2 : IDatabaseFactory
    {
        const string WellknownName = "cryptographic sqlite";

        string IDatabaseFactory.Name
        {
            get
            {
                return WellknownName;
            }
        }

        IDatabase IDatabaseFactory.Create(IConnectProperties pro)
        {
            Sqlite3.sqlite3 db = null;
            int err = Sqlite3.SQLITE_OK;
            if (string.IsNullOrEmpty(pro.Database))
            {
                //内存数据库
                err = Sqlite3.sqlite3_open(":memory:", out db);
                if (err != Sqlite3.SQLITE_OK)
                {
                    throw new Exception("空间数据库创建失败");
                }
                SqliteSpatialDatabase database = new SqliteSpatialDatabase();
                database.FactoryName = WellknownName;
                database.open(db, true);
                return database;
            }
            else
            {
                err = Sqlite3.sqlite3_open_v2(pro.Database, out db,
                    Sqlite3.SQLITE_OPEN_NOMUTEX | Sqlite3.SQLITE_OPEN_READWRITE | Sqlite3.SQLITE_OPEN_CREATE, null);
                if (err != Sqlite3.SQLITE_OK)
                {
                    throw new Exception("数据库创建失败");
                }
                string encrySql = "pragma hexkey='" + SpatialDbDefine.ENCRYPTION_CODE + "'";
                err = Sqlite3.sqlite3_exec(db, encrySql, 0, 0, 0);
                if (err != Sqlite3.SQLITE_OK)
                {
                    string errMsg = Sqlite3.sqlite3_errmsg(db);
                    throw new ApplicationException(errMsg);
                }
                SqliteDatabase database = new SqliteDatabase();
                database.FactoryName = WellknownName;
                database.open(db, true);
                return database;
            }
        }

        IDatabase IDatabaseFactory.Open(IConnectProperties pro)
        {
            Sqlite3.sqlite3 db = null;
            int err = Sqlite3.sqlite3_open_v2(pro.Database, out db,
                Sqlite3.SQLITE_OPEN_NOMUTEX | Sqlite3.SQLITE_OPEN_READWRITE | Sqlite3.SQLITE_OPEN_CREATE, null);
            if (err != Sqlite3.SQLITE_OK)
            {
                throw new ApplicationException("数据库打开失败");
            }
            string encrySql = "pragma hexkey='" + SpatialDbDefine.ENCRYPTION_CODE + "'";
            err = Sqlite3.sqlite3_exec(db, encrySql, 0, 0, 0);
            if (err != Sqlite3.SQLITE_OK)
            {
                string errMsg = Sqlite3.sqlite3_errmsg(db);
                throw new ApplicationException(errMsg);
            }
            SqliteDatabase database = new SqliteDatabase();
            database.FactoryName = WellknownName;
            database.open(db, false);
            return database;
        }
    }

    public class SqliteSpatialDatabaseFactory2 : ISpatialDatabaseFactory
    {
        const string WellknownName = "cryptographic sqlite spatial";

        string ISpatialDatabaseFactory.Name
        {
            get
            {
                return WellknownName;
            }
        }

        ISpatialDatabase ISpatialDatabaseFactory.Create(IConnectProperties pro)
        {
            Sqlite3.sqlite3 db = null;
            int err = Sqlite3.SQLITE_OK;
            if (string.IsNullOrEmpty(pro.Database))
            {
                //内存数据库
                err = Sqlite3.sqlite3_open(":memory:", out db);
                if (err != Sqlite3.SQLITE_OK)
                {
                    throw new Exception("空间数据库创建失败");
                }
                SqliteSpatialDatabase database = new SqliteSpatialDatabase();
                database.FactoryName = WellknownName;
                database.open(db, true);
                return database;
            }
            else
            {
                err = Sqlite3.sqlite3_open_v2(pro.Database, out db,
                    Sqlite3.SQLITE_OPEN_NOMUTEX | Sqlite3.SQLITE_OPEN_READWRITE | Sqlite3.SQLITE_OPEN_CREATE, null);
                if (err != Sqlite3.SQLITE_OK)
                {
                    throw new Exception("空间数据库创建失败");
                }
                string encrySql = "pragma hexkey='" + SpatialDbDefine.ENCRYPTION_CODE_SPATIAL + "'";
                err = Sqlite3.sqlite3_exec(db, encrySql, 0, 0, 0);
                if (err != Sqlite3.SQLITE_OK)
                {
                    string errMsg = Sqlite3.sqlite3_errmsg(db);
                    throw new ApplicationException(errMsg);
                }
                SqliteSpatialDatabase database = new SqliteSpatialDatabase();
                database.FactoryName = WellknownName;
                database.open(db, true);
                return database;
            }
        }

        ISpatialDatabase ISpatialDatabaseFactory.Open(IConnectProperties pro)
        {
            Sqlite3.sqlite3 db = null;
            int err = Sqlite3.sqlite3_open_v2(pro.Database, out db,
                Sqlite3.SQLITE_OPEN_NOMUTEX | Sqlite3.SQLITE_OPEN_READWRITE | Sqlite3.SQLITE_OPEN_CREATE, null);
            if (err != Sqlite3.SQLITE_OK)
            {
                string errMsg = "空间数据库打开失败:" + Sqlite3.sqlite3_errmsg(db);
                throw new ApplicationException(errMsg);
            }
            else
            {
                //打开加密数据库
                string encrySql = "pragma hexkey='" + SpatialDbDefine.ENCRYPTION_CODE_SPATIAL + "'";
                err = Sqlite3.sqlite3_exec(db, encrySql, 0, 0, 0);
                if (err != Sqlite3.SQLITE_OK)
                {
                    string errMsg = Sqlite3.sqlite3_errmsg(db);
                    throw new ApplicationException(errMsg);
                }
                SqliteSpatialDatabase database = new SqliteSpatialDatabase();
                database.FactoryName = WellknownName;
                database.open(db, false);
                return database;
            }
        }
    }
}
