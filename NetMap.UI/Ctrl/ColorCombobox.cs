﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetMap.UI.Ctrl
{
    public partial class ColorCombobox : UserControl
    {
        private static string[] _colorList = { "#000000","#973302","#343200","#013300","#003466","#000083","#313398","#333333","#810004",
                                              "#FD6802","#858200","#008002","#008081","#0201FF","#69669D","#80807E","#FE0002","#FE9B00",
                                             "#9ACB00","#339A65","#33CBCC","#3C62FF","#780179","#99999B","#FF00FE","#FFCB03","#FFFE01",
                                             "#00FF01","#01FFFF","#00CCFF","#993365","#C0C0C0","#FF99CB","#FFCA9B","#FFFE99","#CDFFCC", 
                                             "#CDFFFF","#99CDFD","#C89CFB","#FFFFFF"};
        private Color _selectedColor = Color.Empty;
        public delegate void ColorChangedHandle(Color color);
        public event ColorChangedHandle ColorChangedEvent;
        public Color SelectedColor
        {
            get { return _selectedColor; }
        }
        public ColorCombobox()
        {
            InitializeComponent();
            InitItems();
            
            comboBox1.SelectedIndexChanged += new EventHandler(comboBox1_SelectedIndexChanged);
            comboBox1.DrawItem += new DrawItemEventHandler(comboBox1_DrawItem);
            
        }
        # region 十六进制和Color的转换
        private System.Drawing.Color colorHx16toRGB(string strHxColor)
        {
            try
            {
                if (strHxColor.Length == 0)
                {//如果为空
                    return System.Drawing.Color.FromArgb(0,0,0,0);//设为黑色
                }
                else
                {//转换颜色
                     return  System.Drawing.Color.FromArgb(System.Int32.Parse(strHxColor.Substring(1,  2), System.Globalization.NumberStyles.AllowHexSpecifier),  System.Int32.Parse(strHxColor.Substring(3, 2),  System.Globalization.NumberStyles.AllowHexSpecifier),  System.Int32.Parse(strHxColor.Substring(5, 2),  System.Globalization.NumberStyles.AllowHexSpecifier));
                }
            }
            catch
            {//设为黑色
                return System.Drawing.Color.FromArgb(0,0,0,0);
            }
        }
        /// <summary>
        /// [颜色：RGB转成16进制]
        /// </summary>
        /// <param name="R">红 int</param>
        /// <param name="G">绿 int</param>
        /// <param name="B">蓝 int</param>
        /// <returns></returns>
        private string colorRGBtoHx16(int R, int G, int B)
        {
            return System.Drawing.ColorTranslator.ToHtml(System.Drawing.Color.FromArgb(R, G, B));
        }
        #endregion
        private void InitItems()
        {
            comboBox1.DrawMode = DrawMode.OwnerDrawFixed;
            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox1.ItemHeight = 20;
            comboBox1.BeginUpdate();
            comboBox1.Items.Clear();
            foreach (string color in _colorList)
            {
                Color temColor = colorHx16toRGB(color);
                comboBox1.Items.Add(temColor);
            }
            comboBox1.Items.Add(new Button());
            comboBox1.SelectedIndex = 0;
           
            
            comboBox1.EndUpdate();
        }
        void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == comboBox1.Items.Count - 1)
            {
                //_selectedColor = Color.Red;
                DIYColorFrm temDIYColorFrm = new DIYColorFrm();
                temDIYColorFrm.DefaultColor = _selectedColor;
                temDIYColorFrm.SelectedColorEnvnt += new DIYColorFrm.CurrentColorHandle(temDIYColorFrm_SelectedColorEnvnt);
                temDIYColorFrm.Show();
            }
            else
            {
                _selectedColor = (Color)comboBox1.SelectedItem;
                if (ColorChangedEvent != null) { ColorChangedEvent(_selectedColor); }
            }
        }

        void temDIYColorFrm_SelectedColorEnvnt(Color color)
        {
            _selectedColor = color;
            if (ColorChangedEvent != null) { ColorChangedEvent(_selectedColor); }
            
        }

        void comboBox1_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index < 0) return;
            Rectangle rect = e.Bounds;
            rect.Inflate(-1, -1);
            if (e.Index == comboBox1.Items.Count-1)
            {
                e.Graphics.FillRectangle(new SolidBrush(Color.Gray), rect);
                e.Graphics.DrawRectangle(new Pen(new SolidBrush(Color.Green)), rect);
                Font font = new Font("微软雅黑", 9);//定义字体
                e.Graphics.DrawString("自定义", font, Brushes.Blue,rect.X+27,rect.Y+3);
            }
            else
            {
                Color temColor = (Color)comboBox1.Items[e.Index];
                e.Graphics.FillRectangle(new SolidBrush(temColor), rect);
                e.Graphics.DrawRectangle(new Pen(new SolidBrush(Color.Gray)), rect);
                //e.Graphics.DrawString("自定义", Font, Brushes.Blue, 10, 10);
            }
        }
    }
}
