﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Geometry
{
    /// <summary>
    /// 有向点接口
    /// 备注：2013-4-10 规定正东角度为0，顺时针增大
    /// </summary>
    public interface IDirectionPoint : IPoint
    {
        /// <summary>
        /// 获取和设置角度
        /// </summary>
        double Angle { get; set; }
    }
}
