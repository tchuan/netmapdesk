﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NetMap.Core;
using NetMap.Interfaces.Geometry;

namespace NetMap.Geometry
{
    public class MultiLineString : Geometry, IMultiLineString
    {
        private List<ILineString> _lines = null;

        public MultiLineString()
        {
            _lines = new List<ILineString>();
        }

        public MultiLineString(List<ILineString> lines, IEnvelope box)
        {
            _lines = lines;
            _box = box;
        }

        public override geoGeometryType GeometryType
        {
            get { return geoGeometryType.GEO_GEOMETRY_MULTILINESTRING; }
        }

        public override void UpdateEnvelope()
        {
            if (_lines.Count == 0)
            {
                _box = null;
                return;
            }
            foreach (ILineString line in _lines)
            {
                line.UpdateEnvelope();
            }
            if (_box == null)
            {
                _box = new Envelope(_lines[0].Envelope);
            }
            else
            {
                _box.Update(_lines[0].Envelope);
            }
            foreach (ILineString line in _lines)
            {
                _box.Union(line.Envelope);
            }
        }

        #region IList<ILineString> 成员

        public int IndexOf(ILineString item)
        {
            return _lines.IndexOf(item);
        }

        public void Insert(int index, ILineString item)
        {
            _lines.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            _lines.RemoveAt(index);
        }

        public ILineString this[int index]
        {
            get
            {
                return _lines[index];
            }
            set
            {
                _lines[index] = value;
            }
        }

        #endregion

        #region ICollection<ILineString> 成员

        public void Add(ILineString item)
        {
            _lines.Add(item);
        }

        public void Clear()
        {
            _lines.Clear();
        }

        public bool Contains(ILineString item)
        {
            return _lines.Contains(item);
        }

        public void CopyTo(ILineString[] array, int arrayIndex)
        {
            _lines.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _lines.Count; }
        }

        public bool IsReadOnly
        {
            get { return true; }
        }

        public bool Remove(ILineString item)
        {
            return _lines.Remove(item);
        }

        #endregion

        #region IEnumerable<ILineString> 成员

        public IEnumerator<ILineString> GetEnumerator()
        {
            foreach (ILineString line in _lines)
            {
                yield return line;
            }
        }

        #endregion

        #region IEnumerable 成员

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            foreach (ILineString line in _lines)
            {
                yield return line;
            }
        }

        #endregion

        public override IGeometry CloneGeometry()
        {
            IMultiLineString geometry = new MultiLineString(_lines, this.Envelope);
            return geometry;
        }

        // 总数 点数 x y x y ...点数 x y x y ...
        public override byte[] ExportToWkb()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    writer.Write(_lines.Count);
                    foreach (ILineString line in _lines)
                    {
                        line.Export(writer);
                    }                       
                }
                return stream.ToArray();
            }
        }

        public override string ExportToWkt()
        {
            throw new NotImplementedException();
        }

        public override void ImportFromWkb(byte[] data)
        {
            using (MemoryStream stream = new MemoryStream(data))
            {
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    int count = reader.ReadInt32();
                    for (int i = 0; i < count; i++)
                    {
                        ILineString line = new LineString();
                        line.Import(reader);
                        _lines.Add(line);
                    }
                }
            }
        }

        public override void ImportFromWkt(string data)
        {
            throw new NotImplementedException();
        }

        public override void Export(BinaryWriter writer)
        {
            writer.Write(_lines.Count);
            foreach (ILineString line in _lines)
            {
                line.Export(writer);
            }
        }

        public override void Import(BinaryReader reader)
        {
            int count = reader.ReadInt32();
            for (int i = 0; i < count; i++)
            {
                ILineString line = new LineString();
                line.Import(reader);
            }
        }
    }
}
