﻿namespace NetMap.UI.Ctrl
{
    partial class DIYColorFrm
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.CancelBT = new System.Windows.Forms.Button();
            this.OKBT = new System.Windows.Forms.Button();
            this.ColorBT = new System.Windows.Forms.Button();
            this.BtextBox = new System.Windows.Forms.TextBox();
            this.GtextBox = new System.Windows.Forms.TextBox();
            this.RtextBox = new System.Windows.Forms.TextBox();
            this.BtrackBar = new System.Windows.Forms.TrackBar();
            this.GtrackBar = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.R = new System.Windows.Forms.Label();
            this.RtrackBar = new System.Windows.Forms.TrackBar();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BtrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GtrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RtrackBar)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(280, 169);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "属性";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.CancelBT);
            this.tabPage1.Controls.Add(this.OKBT);
            this.tabPage1.Controls.Add(this.ColorBT);
            this.tabPage1.Controls.Add(this.BtextBox);
            this.tabPage1.Controls.Add(this.GtextBox);
            this.tabPage1.Controls.Add(this.RtextBox);
            this.tabPage1.Controls.Add(this.BtrackBar);
            this.tabPage1.Controls.Add(this.GtrackBar);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.R);
            this.tabPage1.Controls.Add(this.RtrackBar);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(281, 164);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "颜色";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // CancelBT
            // 
            this.CancelBT.Location = new System.Drawing.Point(204, 134);
            this.CancelBT.Name = "CancelBT";
            this.CancelBT.Size = new System.Drawing.Size(59, 24);
            this.CancelBT.TabIndex = 13;
            this.CancelBT.Text = "Cancel";
            this.CancelBT.UseVisualStyleBackColor = true;
            this.CancelBT.Click += new System.EventHandler(this.CancelBT_Click);
            // 
            // OKBT
            // 
            this.OKBT.Location = new System.Drawing.Point(129, 134);
            this.OKBT.Name = "OKBT";
            this.OKBT.Size = new System.Drawing.Size(59, 24);
            this.OKBT.TabIndex = 13;
            this.OKBT.Text = "OK";
            this.OKBT.UseVisualStyleBackColor = true;
            this.OKBT.Click += new System.EventHandler(this.OKBT_Click);
            // 
            // ColorBT
            // 
            this.ColorBT.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ColorBT.Location = new System.Drawing.Point(31, 134);
            this.ColorBT.Name = "ColorBT";
            this.ColorBT.Size = new System.Drawing.Size(59, 24);
            this.ColorBT.TabIndex = 13;
            this.ColorBT.UseVisualStyleBackColor = true;
            // 
            // BtextBox
            // 
            this.BtextBox.Location = new System.Drawing.Point(226, 86);
            this.BtextBox.Name = "BtextBox";
            this.BtextBox.Size = new System.Drawing.Size(48, 21);
            this.BtextBox.TabIndex = 12;
            this.BtextBox.TextChanged += new System.EventHandler(this.BtextBox_TextChanged);
            // 
            // GtextBox
            // 
            this.GtextBox.Location = new System.Drawing.Point(226, 47);
            this.GtextBox.Name = "GtextBox";
            this.GtextBox.Size = new System.Drawing.Size(48, 21);
            this.GtextBox.TabIndex = 12;
            this.GtextBox.TextChanged += new System.EventHandler(this.GtextBox_TextChanged);
            // 
            // RtextBox
            // 
            this.RtextBox.Location = new System.Drawing.Point(226, 11);
            this.RtextBox.Name = "RtextBox";
            this.RtextBox.Size = new System.Drawing.Size(48, 21);
            this.RtextBox.TabIndex = 12;
            this.RtextBox.TextChanged += new System.EventHandler(this.RtextBox_TextChanged);
            // 
            // BtrackBar
            // 
            this.BtrackBar.BackColor = System.Drawing.Color.White;
            this.BtrackBar.Location = new System.Drawing.Point(45, 86);
            this.BtrackBar.Maximum = 255;
            this.BtrackBar.Name = "BtrackBar";
            this.BtrackBar.Size = new System.Drawing.Size(175, 45);
            this.BtrackBar.TabIndex = 11;
            this.BtrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.BtrackBar.ValueChanged += new System.EventHandler(this.BtrackBar_ValueChanged);
            // 
            // GtrackBar
            // 
            this.GtrackBar.BackColor = System.Drawing.Color.White;
            this.GtrackBar.Location = new System.Drawing.Point(45, 47);
            this.GtrackBar.Maximum = 255;
            this.GtrackBar.Name = "GtrackBar";
            this.GtrackBar.Size = new System.Drawing.Size(175, 45);
            this.GtrackBar.TabIndex = 10;
            this.GtrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.GtrackBar.ValueChanged += new System.EventHandler(this.GtrackBar_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 12);
            this.label2.TabIndex = 9;
            this.label2.Text = "B";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 12);
            this.label1.TabIndex = 8;
            this.label1.Text = "G";
            // 
            // R
            // 
            this.R.AutoSize = true;
            this.R.Location = new System.Drawing.Point(28, 11);
            this.R.Name = "R";
            this.R.Size = new System.Drawing.Size(11, 12);
            this.R.TabIndex = 7;
            this.R.Text = "R";
            // 
            // RtrackBar
            // 
            this.RtrackBar.BackColor = System.Drawing.Color.White;
            this.RtrackBar.Location = new System.Drawing.Point(45, 11);
            this.RtrackBar.Maximum = 255;
            this.RtrackBar.Name = "RtrackBar";
            this.RtrackBar.Size = new System.Drawing.Size(175, 45);
            this.RtrackBar.TabIndex = 6;
            this.RtrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.RtrackBar.ValueChanged += new System.EventHandler(this.RtrackBar_ValueChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(289, 190);
            this.tabControl1.TabIndex = 0;
            // 
            // DIYColorFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(289, 190);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "DIYColorFrm";
            this.ShowIcon = false;
            this.Text = "Color Selector";
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BtrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GtrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RtrackBar)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox RtextBox;
        private System.Windows.Forms.TrackBar BtrackBar;
        private System.Windows.Forms.TrackBar GtrackBar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label R;
        private System.Windows.Forms.TrackBar RtrackBar;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Button CancelBT;
        private System.Windows.Forms.Button OKBT;
        private System.Windows.Forms.Button ColorBT;
        private System.Windows.Forms.TextBox BtextBox;
        private System.Windows.Forms.TextBox GtextBox;

    }
}
