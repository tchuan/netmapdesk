﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetMap.UI
{
    /// <summary>
    /// 获取文本对话框
    /// </summary>
    public partial class FrmGetText : Form
    {
        public FrmGetText()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 获取结果文本
        /// </summary>
        public string ResultText
        {
            get;
            private set;
        }

        private void buttonApply_Click(object sender, EventArgs e)
        {
            ResultText = textBoxGetText.Text;
        }
    }
}
