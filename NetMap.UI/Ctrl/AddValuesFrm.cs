﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetMap.UI.Ctrl
{
    public partial class AddValuesFrm : Form
    {
        public delegate void SelectedHandle(List<object> objs);
        public event SelectedHandle SelectedEvent;
        private List<object> mayValues;//可以添加的值
        private List<object> addedValues;//已经添加的值
        public AddValuesFrm(List<object> values)
        {
            InitializeComponent();
            mayValues = values;
            if (mayValues == null) return;
            if (mayValues.Count <= 0) return;
            addingValuesLv.MultiSelect = true;
            addingValuesLv.View = View.SmallIcon;
            addingValuesLv.CheckBoxes = true;
            foreach (object obj in mayValues)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Name = obj.ToString();
                lvi.Text = obj.ToString();
                addingValuesLv.Items.Add(lvi);
            }
        }

        private void OkBt_Click(object sender, EventArgs e)
        {
            ListView.CheckedIndexCollection tem = addingValuesLv.CheckedIndices;
            if (tem == null) return;
            if (tem.Count <= 0) return;
            addedValues = new List<object>();
            foreach(int i in tem)
            {
                addedValues.Add(mayValues[i]);
            }
            if (SelectedEvent!=null)
            SelectedEvent(addedValues);
        }

        private void CancelBt_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
