﻿using NetMap.Geometry;
using NetMap.Interfaces.Geometry;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace NetMap.Utilities
{
    public class XmlUtil
    {
        public static string ToXml(IEnvelope box)
        {
            return string.Format("{0},{1},{2},{3}", box.XMin, box.YMin, box.XMax, box.YMax);
        }

        public static IEnvelope ToEnvelope(string xml)
        {
            IEnvelope box = new Envelope();
            string[] items = xml.Split(',');
            box.XMin = double.Parse(items[0]);
            box.YMin = double.Parse(items[1]);
            box.XMax = double.Parse(items[2]);
            box.YMax = double.Parse(items[3]);
            return box;
        }

        public static string ToXml(IPoint point)
        {
            return string.Format("{0},{1}", point.X, point.Y);
        }

        public static IPoint ToPoint(string xml)
        {
            IPoint point = new NetMap.Geometry.Point();
            string[] items = xml.Split(',');
            point.X = double.Parse(items[0]);
            point.Y = double.Parse(items[1]);
            return point;
        }

        public static string ToXml(Color color)
        {
            return string.Format("#{0:X2}{1:X2}{2:X2}{3:X2}", color.A, color.R, color.G, color.B);
        }
    }
}
