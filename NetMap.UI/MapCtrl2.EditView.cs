﻿using System.Windows.Forms;
using NetMap.Interfaces;

namespace NetMap.UI
{
    partial class MapCtrl2
    {        
        #region Fields

        private ITool _currentTool = null;

        #endregion

        void IMapEditView.SetCurrentTool(ITool tool)
        {
            if (_currentTool != null)
            {
                _currentTool.LostFocus();
            }
            _currentTool = tool;
            if (_status != null)
            {
                _status.ShowInfo(_currentTool.Name);
            }
            _currentTool.GainFocus();
            Cursor = _currentTool.Cursor; // 设置光标
        }

        ITool IMapEditView.GetCurrentTool()
        {
            return _currentTool;
        }
    }
}
