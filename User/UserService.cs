﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Data;
using System.IO;
using System.Reflection;
using NetMap.Data;
using NetMap.Sqlite;

/*
 * create: 2014-06-17 用户管理服务
 * 2014-07-23 加入权限控制
 */

namespace User
{
    public class UserService
    {
        private static UserService _instance;
        public static UserService Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new UserService();
                }
                return _instance;
            }
        }

        private IDatabase _db;
        private ITable _table;
        private ITable _permissionTable;
        public const string TABLE_USER = "gis_user";
        public const string FIELD_USER_ID = "user";
        public const string FIELD_USER_PWD = "pwd";
        public const string FIELD_USER_TYPE = "usertype";
        public const string FIELD_USER_INFO = "info";
        public const string TABLE_PERMISSION = "permission";
        public const string FIELD_PERMISSION_CLASS = "cls";
        public const string FIELD_PERMISSION_LABEL = "label";
        public const string FIELD_PERMISSION_USERTYPE = "usertype";

        private UserService()
        {
        }

        public void Init()
        {
            Assembly dll = typeof(UserService).Assembly;
            string path = Path.GetDirectoryName(dll.Location);
            string dbFile = Path.Combine(path, "map\\user.db");
            IConnectProperties connectProperties = new ConnectProperties();
            connectProperties.Database = dbFile;
            IDatabaseFactory factory = new SqliteDatabaseFactory2();
            if (File.Exists(dbFile))
            {
                //打开
                _db = factory.Open(connectProperties);
                _table = _db.OpenTable(TABLE_USER);
                _permissionTable = _db.OpenTable(TABLE_PERMISSION);
            }
            else
            {
                //创建
                _db = factory.Create(connectProperties);
                //创建用户表
                IFields fields = new Fields();
                IField field = new Field(FIELD_USER_ID, geoFieldType.GEO_STRING);
                fields.SetField(0, field);
                field = new Field(FIELD_USER_PWD, geoFieldType.GEO_STRING);
                fields.SetField(1, field);
                field = new Field(FIELD_USER_TYPE, geoFieldType.GEO_INT);
                fields.SetField(2, field);
                field = new Field(FIELD_USER_INFO, geoFieldType.GEO_STRING);
                fields.SetField(3, field);
                _table = _db.CreateTable(TABLE_USER, fields);                
                //创建权限表
                IFields permissionFields = new Fields();
                IField permissionField = new Field(FIELD_PERMISSION_CLASS, geoFieldType.GEO_STRING);
                permissionFields.SetField(0, permissionField);
                permissionField = new Field(FIELD_PERMISSION_LABEL, geoFieldType.GEO_STRING);
                permissionFields.SetField(1, permissionField);
                permissionField = new Field(FIELD_PERMISSION_USERTYPE, geoFieldType.GEO_INT);
                permissionFields.SetField(2, permissionField);
                _permissionTable = _db.CreateTable(TABLE_PERMISSION, permissionFields);
                //添加默认数据
                AddDefaultUser();
                AddDefaultPermission();
            }
        }

        #region 用户

        /// <summary>
        /// 登陆
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        public SystemUser Login(string id, string pwd)
        {
            string whereClause = FIELD_USER_ID + "=\'" + id + "\' and "
                + FIELD_USER_PWD + "=\'" + pwd + "\'";
            IQueryFilter filter = new QueryFilter();
            filter.WhereClause = whereClause;
            filter.Fields.Add(FIELD_USER_TYPE);
            filter.Fields.Add(FIELD_USER_INFO);
            ICursor cursor = _table.Search(filter);
            IRow row = cursor.Next();
            cursor.Close();
            if (row != null)
            {
                SystemUser user = new SystemUser();
                user.Id = id;
                user.Pwd = pwd;
                user.UserType = (SystemUserType)(int)row.GetValue(0);
                user.Info = row.GetValue(1).ToString();
                return user;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 新增用户
        /// </summary>
        /// <param name="user"></param>
        public void NewUser(SystemUser user)
        {
            IRow row = _table.CreateRow();
            row.SetValue(0, user.Id);
            row.SetValue(1, user.Pwd);
            row.SetValue(2, (int)user.UserType);
            row.SetValue(3, user.Info);
            row.Store();
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="id"></param>
        public void DeleteUser(string id)
        {
            string whereClause = FIELD_USER_ID + "=" + id;
            IQueryFilter filter = new QueryFilter();
            filter.WhereClause = whereClause;
            ICursor cursor = _table.Search(filter);
            IRow row = cursor.Next();
            while (row != null)
            {
                row.Delete();
                row = cursor.Next();
            }
            cursor.Close();
        }

        /// <summary>
        /// 获取和设置当前用户
        /// </summary>
        public SystemUser CurrentUser
        {
            get;
            set;
        }

        /// <summary>
        /// 获取用户表
        /// </summary>
        public ITable UserTable
        {
            get { return _table; }
        }

        #endregion

        #region 权限

        /// <summary>
        /// 查找类的权限
        /// </summary>
        /// <param name="cls"></param>
        /// <returns></returns>
        public SystemUserType FindPermission(string cls)
        {
            string sql = "select " + FIELD_PERMISSION_USERTYPE + " from "
                + TABLE_PERMISSION + " where " + FIELD_PERMISSION_CLASS + "=\'"
                + cls + "\'";
            int userType = _db.ExecuteAndGetInt(sql);
            return (SystemUserType)userType;
        }

        /// <summary>
        /// 获取权限表
        /// </summary>
        public ITable PermissionTable
        {
            get { return _permissionTable; }
        }

        #endregion

        private void AddDefaultUser()
        {
            //插入默认的管理员
            IRow row = _table.CreateRow();
            row.SetValue(0, "gis");
            row.SetValue(1, "gisman");
            row.SetValue(2, (int)SystemUserType.Admin);
            row.SetValue(3, "默认系统管理员");
            row.Store();
            //插入系统维护
            row = _table.CreateRow();
            row.SetValue(0, "facemap");
            row.SetValue(1, "facemap");
            row.SetValue(2, (int)SystemUserType.Support);
            row.SetValue(3, "系统维护与技术支持");
            row.Store();
        }

        private void AddDefaultPermission()
        {
            IRow row = _table.CreateRow();
            row.SetValue(0, "User.Commands.UserManageCommand");
            row.SetValue(1, "用户管理");
            row.SetValue(2, 1);
            row.Store();

            row = _table.CreateRow();
            row.SetValue(0, "User.Commands.PermissionManageCommand");
            row.SetValue(1, "权限管理");
            row.SetValue(2, 1);
            row.Store();
        }
    }
}
