﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Windows.Forms;
using ProjNet.CoordinateSystems;
using NetMap.Data;
using NetMap.Geometry;
using NetMap.Interfaces;
using NetMap.Interfaces.Data;
using NetMap.Interfaces.Format;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Symbol;
using NetMap.Render;
using NetMap.Utilities;
using NetMap.Symbol;
using NetMap.Interfaces.Persist;
using System.Xml;
using System.Drawing;

namespace NetMap
{
    /// <summary>
    /// 要素图层
    /// </summary>
    public class FeatureLayer : Layer, IFeatureLayer, IDisplayLayer, ILabelLayer, IFeatureSelection, IPersistResource
    {
        private IFeatureClass _featureClass;

        public FeatureLayer()
        {
            //标注特性，默认不标注
            IsLabel = false;
            LabelField = string.Empty;
            LabelSymbol = null;
        }
        
        #region IFeatureLayer 成员

        public bool Editable
        {
            get;
            set;
        }

        public bool Selectable
        {
            get;
            set;
        }

        public geoGeometryType GeometryType
        {
            get { return _featureClass.GeometryType; }
        }

        public geoFeatureType FeatureType
        {
            get { return _featureClass.FeatureType; }
        }

        public IFeatureClass FeatureClass
        {
            get
            {
                return _featureClass;
            }
            set
            {
                _featureClass = value;
            }
        }

        #endregion

        #region IDisplayLayer 成员

        public IFeatureRender Render
        {
            get;
            set;
        }

        #endregion

        #region ILabelLayer 成员

        public bool IsLabel
        {
            get;
            set;
        }

        public string LabelField
        {
            get;
            set;
        }

        public ITextSymbol LabelSymbol
        {
            get;
            set;
        }

        //绘制标注
        private void DrawLabel(IDisplay display, IFeatureCursor featureCursor)
        {
        }

        #endregion

        #region IFeatureSelection 成员

        private ISelectionSet _selection = null;
        private ISymbol _selectionSymbol = null;
        private IFeatureRender _selectionRender = null;

        public ISymbol SelectionSymbol
        {
            get
            {
                return _selectionSymbol;
            }
            set
            {
                _selectionSymbol = value;
                _selectionRender = new SimpleRender(_selectionSymbol);
            }
        }

        public ISelectionSet SelectionSet
        {
            get { return _selection; }
        }

        public void Select(IQueryFilter filter, geoSelectionResultEnum method)
        {
            if (_selection == null)
            {
                switch (method)
                {
                    case geoSelectionResultEnum.GEO_SELECTIONRESULT_NEW:
                        {
                            _selection = _featureClass.Select(filter);
                            break;
                        }
                    case geoSelectionResultEnum.GEO_SELECTIONRESULT_ADD:
                        {
                            _selection = _featureClass.Select(filter);
                            break;
                        }
                    case geoSelectionResultEnum.GEO_SELECTIONRESULT_SUBTRACT:
                        {
                            break;
                        }
                    case geoSelectionResultEnum.GEO_SELECTIONRESULT_AND:
                        {
                            break;
                        }
                    case geoSelectionResultEnum.GEO_SELECTIONRESULT_XOR:
                        {
                            _selection = _featureClass.Select(filter);
                            break;
                        }
                }
            }
            else
            {
                ISelectionSet newSelection = _featureClass.Select(filter);
                switch (method)
                {
                    case geoSelectionResultEnum.GEO_SELECTIONRESULT_NEW:
                        {
                            _selection = newSelection;
                            break;
                        }
                    case geoSelectionResultEnum.GEO_SELECTIONRESULT_ADD:
                        {
                            _selection.AddList(newSelection);
                            break;
                        }
                    case geoSelectionResultEnum.GEO_SELECTIONRESULT_SUBTRACT:
                        {
                            _selection.RemoveList(newSelection);
                            break;
                        }
                    case geoSelectionResultEnum.GEO_SELECTIONRESULT_AND:
                        {
                            List<long> newSet = new List<long>();
                            foreach (long oid in _selection)
                            {
                                if (newSelection.IsIn(oid))
                                {
                                    newSet.Add(oid);
                                }
                            }
                            _selection.RemoveAll();
                            _selection.AddList(newSet);
                            break;
                        }
                    case geoSelectionResultEnum.GEO_SELECTIONRESULT_XOR:
                        {
                            List<long> newSet = new List<long>();
                            foreach (long oid in _selection)
                            {
                                if (!newSelection.IsIn(oid))
                                {
                                    newSet.Add(oid);
                                }
                            }
                            foreach (long oid in newSelection)
                            {
                                if (!_selection.IsIn(oid))
                                {
                                    newSet.Add(oid);
                                }
                            }
                            _selection.RemoveAll();
                            _selection.AddList(newSet);
                            break;
                        }
                }
            }
        }

        public void Clear()
        {
            if (_selection != null)
            {
                _selection.RemoveAll();
            }
            _selection = null;
        }

        public void DrawSelection(IDisplay display)
        {
            //检查无选择
            if(_selection == null)
            {
                return;
            }
            //检查选择符号是否有效
            if(_selectionRender == null)
            {
                return;
            }
            //获取选择集要素游标
            IQueryFilter filter = new SpatialFilter();
            IFeatureCursor featureCursor = _selection.Search(filter);
            _selectionRender.DrawCursor(display, featureCursor, null);
        }

        #endregion

        public override void Draw(IDisplay display)
        {
            //Modify 2014-07-01 无图层方案时不渲染
            if (Render == null)
            {
                return;
            }
            // 查询范围内的要素
            ISpatialFilter filter = new SpatialFilter();
            //Modify 2013-10-15 查询时要查询属性字段 
            IFields fields = _featureClass.Fields;
            int count = fields.Count;
            for (int i = 0; i < count; i++)
            {
                filter.Fields.Add(fields.GetField(i).Name);
            }
            //end
            if (ReverseCoordinateTransformation != null)
            {
                filter.Geometry = GeometryCoordinateTransformation.Transformation(ReverseCoordinateTransformation,
                    display.DisplayTransformation.VisibleMapBounds as IGeometry);
                filter.Geometry.UpdateEnvelope();
            }
            else
            {
                filter.Geometry = display.DisplayTransformation.VisibleMapBounds as IGeometry;
                filter.Geometry.UpdateEnvelope();
            }
            filter.SpatialRelationship = geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_ENVELOPEINTERSECTS;
            IFeatureCursor cursor = _featureClass.Search(filter);
            //执行具体绘制
            display.DisplayTransformation.CoordinateTransformation = CoordinateTransformation;
            Render.DrawCursor(display, cursor, null);
            //绘制注记
            cursor.Reset();
            DrawLabel(display, cursor);
            //Modify 2014-07-12 关闭cursor
            cursor.Close();
            //end
        }

        public override IEnvelope Extent
        {
            get 
            {
                if (CoordinateTransformation != null)
                {
                    double x1 = 0, y1 = 0, x2 = 0, y2 = 0;
                    CoordinateTransformation.MathTransform.Transform(_featureClass.Extent.XMin, _featureClass.Extent.YMin,
                        ref x1, ref y1);
                    CoordinateTransformation.MathTransform.Transform(_featureClass.Extent.XMax, _featureClass.Extent.YMax,
                        ref x2, ref y2);
                    return new Envelope(Math.Min(x1, x2), Math.Min(y1, y2), Math.Max(x1, x2), Math.Max(y1, y2));
                }
                return _featureClass.Extent; 
            }
        }

        public override ICoordinateSystem CoordinateSystem
        {
            get
            {
                return _featureClass.CoordinateSystem;
            }
        }

        #region IPersistResource 成员

        public void Save(XmlElement node, XmlDocument document)
        {
            XmlElement layer = document.CreateElement("FeatureLayer");
            layer.SetAttribute("Name", Name);
            layer.SetAttribute("Visible", Visible.ToString());
            layer.SetAttribute("MaxScale", MaxScale.ToString());
            layer.SetAttribute("MinScale", MinScale.ToString());
            //feature
            layer.SetAttribute("Selectable", Selectable.ToString());
            layer.SetAttribute("Editable", Editable.ToString());
            layer.SetAttribute("SpatialDatabase", FeatureClass.SpatialDatabase.Name);
            layer.SetAttribute("FeatureClass", FeatureClass.Name);
            //render
            XmlElement render = document.CreateElement("Render");
            IPersistResource renderPR = Render as IPersistResource;
            renderPR.Save(layer, document);
            //label
            if (IsLabel)
            {
                XmlElement label = document.CreateElement("Label");
                label.SetAttribute("Field", LabelField);
                label.SetAttribute("Symbol", LabelSymbol.Code.ToString());
                layer.AppendChild(label);
            }
            node.AppendChild(layer);
        }

        public void Load(XmlReader reader, object context)
        {
            Name = reader.GetAttribute("Name");
            Visible = bool.Parse(reader.GetAttribute("Visible"));
            MaxScale = double.Parse(reader.GetAttribute("MaxScale"));
            MinScale = double.Parse(reader.GetAttribute("MinScale"));
            Selectable = bool.Parse(reader.GetAttribute("Selectable"));
            Editable = bool.Parse(reader.GetAttribute("Editable"));
            string sdbName = reader.GetAttribute("SpatialDatabase");
            string featureclass = reader.GetAttribute("FeatureClass");

            IWorkspace workspace = context as IWorkspace;
            ISpatialDatabase sdb = workspace.DataSource.Find(sdbName);
            if (sdb == null)
            {
                throw new NotSupportedException("数据集定义无效");
            }
            IFeatureClass fc = sdb.OpenFeatureClass(featureclass);
            if (fc == null)
            {
                throw new NotSupportedException("数据集定义无效");
            }
            FeatureClass = fc;
            //render,label
            IsLabel = false; //先设置为false
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    switch (reader.Name)
                    {
                        case "Render":
                            {
                                Render = RenderXmlUtil.LoadFeatureRender(reader, "Render", context);
                                break;
                            }
                        case "Label":
                            {
                                IsLabel = true;
                                LabelField = reader.GetAttribute("Field");
                                int code = int.Parse(reader.GetAttribute("Symbol"));
                                LabelSymbol = workspace.SymbolLibrary.FindSymbol(code) as ITextSymbol;
                                break;
                            }
                    }
                }
                else if (reader.NodeType == XmlNodeType.EndElement
                    && reader.Name == "FeatureLayer")
                {
                    return;
                }
            }
        }

        #endregion
    }
}
