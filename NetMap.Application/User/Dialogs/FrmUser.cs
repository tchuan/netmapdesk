﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NetMap.Interfaces.Data;
using NetMap.Application.Core;

namespace User.Dialogs
{
    public partial class FrmUser : Form
    {
        private IRow _row;

        public FrmUser()
        {
            InitializeComponent();

            comboBoxUserType.Items.Add("用户");
            comboBoxUserType.SelectedIndex = 0;
        }

        public void InitEdit(IRow row)
        {
            _row = row;
            buttonOperate.Text = "修改";
            textBoxUser.Enabled = false;

            textBoxUser.Text = row.GetValue(0).ToString();
            textBoxPwd.Text = row.GetValue(1).ToString();
            textBoxInformation.Text = row.GetValue(3).ToString();
        }

        public void InitNew()
        {
            buttonOperate.Text = "新增";
        }

        private void buttonOperate_Click(object sender, EventArgs e)
        {
            string text = buttonOperate.Text;
            string user = textBoxUser.Text;
            string pwd = textBoxPwd.Text;
            string information = textBoxInformation.Text;

            if (string.IsNullOrEmpty(user) ||
                string.IsNullOrEmpty(pwd))
            {
                MessageService.ShowMessage("请输入完整的用户信息", "提示");
            }

            switch (text)
            {
                case "修改":
                    {
                        _row.SetValue(1, pwd);
                        _row.SetValue(3, information);
                        _row.Store();
                        break;
                    }
                case "新增":
                    {
                        IRow row = UserService.Instance.UserTable.CreateRow();
                        row.SetValue(0, user);
                        row.SetValue(1, pwd);
                        row.SetValue(2, (int)SystemUserType.User);
                        row.SetValue(3, information);
                        row.Store();
                        break;
                    }
                default:
                    {
                        break;
                    }
            }

            MessageService.ShowMessage(text + "成功", "提示");
            this.Close();
        }        
    }
}
