﻿using NetMap.Interfaces.Tile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Utilities
{
    /// <summary>
    /// Web地图比例尺变化工具
    /// </summary>
    public static class ZoomHelper
    {
        /// <summary>
        /// 放大比例尺
        /// </summary>
        /// <param name="scaleRatios">比例尺从小到大的集合</param>
        /// <param name="scaleRationRef"></param>
        /// <returns></returns>
        public static double ZoomIn(IList<double> scaleRatios, double scaleRatioRef)
        {
            // 默认比例尺变化
            if (scaleRatios.Count == 0)
            {
                return scaleRatioRef * 2;
            }
            // 查找比例尺
            foreach(double scaleRatio in scaleRatios)
            {
                if (scaleRatio > scaleRatioRef)
                    return scaleRatio;
            }
            // 最大比例尺
            return scaleRatios.Last();
        }

        /// <summary>
        /// 缩小比例尺
        /// </summary>
        /// <param name="scaleRatios"></param>
        /// <param name="scaleRatioRef"></param>
        /// <returns></returns>
        public static double ZoomOut(IList<double> scaleRatios, double scaleRatioRef)
        {
            if (scaleRatios.Count == 0)
            {
                return scaleRatioRef * 0.5;
            }
            // 查找比例尺
            for (int i = scaleRatios.Count - 1; i >= 0; i--)
            {
                if (scaleRatios[i] < scaleRatioRef)
                    return scaleRatios[i];
            }
            // 最小比例尺
            return scaleRatios.First();
        }

        /// <summary>
        /// 获取最近比例尺
        /// </summary>
        /// <param name="resolutions"></param>
        /// <param name="resolution"></param>
        /// <returns></returns>
        public static string GetNearestLevel(IDictionary<string, Resolution> resolutions, double resolution)
        {
            if (resolutions.Count == 0)
            {
                throw new ArgumentException("No tile resolutions");
            }

            var localResolutions = resolutions.OrderByDescending(r => r.Value.UnitsPerPixel);

            //smaller than smallest
            if (localResolutions.Last().Value.UnitsPerPixel > resolution) return localResolutions.Last().Key;

            //bigger than biggest
            if (localResolutions.First().Value.UnitsPerPixel < resolution) return localResolutions.First().Key;

            string result = null;
            double resultDistance = double.MaxValue;
            foreach (var current in localResolutions)
            {
                double distance = Math.Abs(current.Value.UnitsPerPixel - resolution);
                if (distance < resultDistance)
                {
                    result = current.Key;
                    resultDistance = distance;
                }
            }
            if (result == null) throw new Exception("Unexpected error when calculating nearest level");
            return result;
        }

        /// <summary>
        /// 获取较小比例尺的图片
        /// </summary>
        /// <param name="resolutions"></param>
        /// <param name="resolution"></param>
        /// <returns></returns>
        public static bool GetLowerLevel(IDictionary<string, Resolution> resolutions, double resolution, out string level)
        {
            if (resolutions.Count == 0)
            {
                level = "";
                return false;
            }

            var localResolutions = resolutions.OrderBy(r => r.Value.UnitsPerPixel);

            foreach (var current in localResolutions)
            {
                if (current.Value.UnitsPerPixel > resolution)
                {
                    level = current.Key;
                    return true;
                }
            }
            level = "";
            return false;
        }
    }
}
