﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Tile;

namespace NetMap.Tile
{
    public class CombinationTileProvider : ICombinationTileProvider
    {
        private Dictionary<string, ITileProvider> _innerProviderDic
             = new Dictionary<string, ITileProvider>();

        #region ICombinationTileProvider 成员

        public void AddProvider(string level, ITileProvider provider)
        {
            if (_innerProviderDic.ContainsKey(level))
            {
                _innerProviderDic[level] = provider;
            }
            else
            {
                _innerProviderDic.Add(level, provider);
            }
        }
                
        public void AddProvider(List<string> levels, ITileProvider provider)
        {
            foreach (string level in levels)
            {
                AddProvider(level, provider);
            }
        }

        public void Clear()
        {
            _innerProviderDic.Clear();
        }

        public void GetMinAndMaxLevel(out string min, out string max)
        {
            int minLevel = int.MaxValue;
            int maxLevel = int.MinValue;
            foreach (string level in _innerProviderDic.Keys)
            {
                int value = int.Parse(level);
                if (value < minLevel)
                {
                    minLevel = value;
                }
                if (value > maxLevel)
                {
                    maxLevel = value;
                }
            }
            min = minLevel.ToString();
            max = maxLevel.ToString();
        }

        #endregion

        #region ITileProvider 成员

        public byte[] GetTile(TileInfo tileInfo)
        {
            string level = tileInfo.Index.Level;
            if (_innerProviderDic.ContainsKey(level))
            {
                ITileProvider provider = _innerProviderDic[level];
                return provider.GetTile(tileInfo);
            }
            else
            {
                return null;
            }
        }

        #endregion
    }
}
