﻿// Copyright (c) AlphaSierraPapa for the SharpDevelop Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

using System;
using System.Windows.Forms;
using NetMap.Application.Core;

namespace NetMap.Application.Gui.XmlForms
{
	public abstract class BaseIDEForm : XmlForm
	{
//		public BaseSharpDevelopForm(string fileName) : base(fileName)
//		{
//		}

        public BaseIDEForm()
		{
		}
		
		protected override void SetupXmlLoader()
		{
			xmlLoader.StringValueFilter    = new IDEStringValueFilter();
			xmlLoader.PropertyValueCreator = new IDEPropertyValueCreator();
		}
		
		public void SetEnabledStatus(bool enabled, params string[] controlNames)
		{
			foreach (string controlName in controlNames) {
				Control control = ControlDictionary[controlName];
				if (control == null) {
					MessageService.ShowError(controlName + " not found!");
				} else {
					control.Enabled = enabled;
				}
			}
		}
		
		
	}
}
