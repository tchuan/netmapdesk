﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Symbol;

namespace NetMap.Interfaces.Render
{
    /// <summary>
    /// 简单图层方案接口
    /// 备注：使用唯一的符号来绘制图层内部要素
    /// </summary>
    public interface ISimpleRender : ILayerRender
    {
        /// <summary>
        /// 获取和设置符号
        /// </summary>
        ISymbol Symbol { get; set; }
    }
}
