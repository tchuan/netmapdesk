﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetMap.UI.Ctrl
{
   public  class ToolStripColorCtrlButton : ToolStripControlHost
    {
        public ToolStripColorCtrlButton()
            : base(new ColorCtrlButton())
        { }
        public ColorCtrlButton GetColorCtrlButton()
        {
            
            return (Control as ColorCtrlButton);
        }
    }
}
