﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetMap.UI.Ctrl
{
    /// <summary>
    /// 线型变换事件处理
    /// </summary>
    /// <param name="style">变更后的线型</param>
    public delegate void LineStyleChangedEventHandler(bool[] style);
    
    /// <summary>
    /// 线型设置对话框
    /// </summary>
    internal partial class LineStyleForm : Form
    {
        /// <summary>
        /// 线型变换事件
        /// </summary>
        public event LineStyleChangedEventHandler LineStyleChanged;

        public LineStyleForm()
        {
            InitializeComponent();
            this.Deactivate += new EventHandler(LineStyleForm_Deactivate);
        }

        /// <summary>
        /// 获取线型编辑器
        /// </summary>
        /// <returns></returns>
        public LineStyleEditor GetEditor()
        {
            return lineStyleCtrlEditor;
        }

        /// <summary>
        /// 在控件周围展示出本窗口
        /// </summary>
        /// <param name="ctrl"></param>
        public void Show(Control ctrl)
        {
            Rectangle rect = ctrl.RectangleToScreen(new Rectangle(0, 0, ctrl.Width, ctrl.Height));
            this.Left = rect.Left;
            this.Top = rect.Top + ctrl.Height;
            Rectangle ScreenRect = Screen.PrimaryScreen.WorkingArea;
            if (this.Right > ScreenRect.Width || this.Bottom > ScreenRect.Height)
            {
                this.Left = rect.Left - this.Width + ctrl.Width;
                this.Top = rect.Top - this.Height;
            }
            this.Show();
        }

        #region 事件处理

        void LineStyleForm_Deactivate(object sender, EventArgs e)
        {
            Hide();
        }

        #endregion

        private void btnApply_Click(object sender, EventArgs e)
        {
            bool[] style = lineStyleCtrlEditor.Grids;
            if (LineStyleChanged != null)
            {
                LineStyleChanged(style);
            }
            Hide();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Hide();
        }

        // 给Form加边框
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.Style &= ~Defines.WS_CAPTION | Defines.WS_BORDER;
                return cp;
            }
        }
    }
}
