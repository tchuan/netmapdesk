﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace NetMap.UI.Ctrl.Prop
{
    class PropItemDescriptor : PropertyDescriptor
    {
        private PropItem _prop;

        public PropItemDescriptor(PropItem prop, Attribute[] attrs)
            : base(prop.Name, attrs)
        {
            _prop = prop;
        }

        public override bool CanResetValue(object component)
        {
            return false;
        }

        public override string Category
        {
            get
            {
                return _prop.Category;
            }
        }

        public override string Description
        {
            get
            {
                return _prop.Description;
            }
        }

        public override TypeConverter Converter
        {
            get
            {
                return _prop.Converter;
            }
        }

        public override Type ComponentType
        {
            get { return this.GetType(); }
        }

        public override object GetValue(object component)
        {
            return _prop.Value;
        }

        public override bool IsReadOnly
        {
            get { return _prop.IsReadOnly; }
        }

        public override Type PropertyType
        {
            get { return _prop.Type; }
        }

        public override void ResetValue(object component)
        {
            //do nothing
        }

        public override void SetValue(object component, object value)
        {
            _prop.Value = value;
        }

        public override bool ShouldSerializeValue(object component)
        {
            return false;
        }
    }
}
