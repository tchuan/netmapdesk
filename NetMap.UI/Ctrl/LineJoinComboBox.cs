﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace NetMap.UI.Ctrl
{
    /// <summary>
    /// 线接变化事件处理
    /// </summary>
    /// <param name="linejoin"></param>
    public delegate void LineJoinChangedEventHandler(LineJoin linejoin);
    /// <summary>
    /// 线接样式选择控件
    /// </summary>
    public partial class LineJoinComboBox : UserControl
    {
        private LineJoin _selectedLineJoin = LineJoin.Bevel;
        private Pen _pen = new Pen(Color.Black, 5);
        private PointF[] _pts = new PointF[] { new PointF(0, 0), new Point(0, 0), new Point(0, 0)};
        private StringFormat _sf = null;
        /// <summary>
        /// 线接变化事件
        /// </summary>
        public event LineJoinChangedEventHandler LineJoinChanged;

        public LineJoinComboBox()
        {
            _sf = new StringFormat();
            _sf.Alignment = StringAlignment.Center;
            _sf.LineAlignment = StringAlignment.Center;
            InitializeComponent();
            InitItems();
            comboBoxInner.DrawItem += new DrawItemEventHandler(comboBoxInner_DrawItem);
            comboBoxInner.SelectedIndexChanged += new EventHandler(comboBoxInner_SelectedIndexChanged);
        }          

        /// <summary>
        /// 选择的线接
        /// </summary>
        public LineJoin SelectedLineJoin
        {
            get { return _selectedLineJoin; }
            set 
            { 
                _selectedLineJoin = value;
                comboBoxInner.SelectedItem = _selectedLineJoin;
            }
        }

        private void InitItems()
        {            
            comboBoxInner.DrawMode = DrawMode.OwnerDrawFixed;
            comboBoxInner.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxInner.ItemHeight = 20;
            comboBoxInner.BeginUpdate();
            comboBoxInner.Items.Clear();
            Array linejoins = Enum.GetValues(typeof(LineJoin));
            foreach (LineJoin linejoin in linejoins)
            {
                comboBoxInner.Items.Add(linejoin);
            }
            comboBoxInner.SelectedIndex = 0;
            comboBoxInner.EndUpdate();
        }

        private void comboBoxInner_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index < 0) return;
            Rectangle rect = e.Bounds;
            LineJoin linejoin = (LineJoin)comboBoxInner.Items[e.Index];
            rect.Inflate(-1, -1);
            if (linejoin == _selectedLineJoin)
            {
                e.Graphics.FillRectangle(SystemBrushes.Highlight, rect);
            }
            else
            {
                e.Graphics.FillRectangle(SystemBrushes.Window, rect);
            }
            _pen.LineJoin = linejoin;
            _pts[0].X = 0; _pts[0].Y = rect.Top;
            _pts[1].X = 0.2f * rect.Width; _pts[1].Y = rect.Top + 0.5f * rect.Height;
            _pts[2].X = 0; _pts[2].Y = rect.Bottom;
            e.Graphics.DrawLines(_pen, _pts);
            e.Graphics.DrawString(linejoin.ToString(), Font, Brushes.Black, 0.6f * rect.Width, rect.Top + 0.5f * rect.Height, _sf);
        }

        private void comboBoxInner_SelectedIndexChanged(object sender, EventArgs e)
        {
            _selectedLineJoin = (LineJoin)comboBoxInner.SelectedItem;
            // 触发事件
            if (LineJoinChanged != null)
            {
                LineJoinChanged(_selectedLineJoin);
            }
        }
    }
}
