﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace NetMap.UI
{
    /// <summary>
    /// 比例尺条
    /// </summary>
    public class ScaleRatioBar
    {
        private IList<double> _scaleRatios = new List<double>();
        private int GRIDSIZE = 6;
        private Pen _pen = Pens.Gray;
        private Brush _brush = Brushes.Blue;

        /// <summary>
        /// 构造
        /// </summary>
        /// <param name="scaleRatios">比例尺集合</param>
        public ScaleRatioBar(IList<double> scaleRatios)
        {
            foreach (double item in scaleRatios)
            {
                _scaleRatios.Add(item);
            }
        }

        /// <summary>
        /// 绘制
        /// </summary>
        /// <param name="g"></param>
        /// <param name="anchor">锚点</param>
        public void Draw(Graphics g, Point anchor, double scaleRatio)
        {
            int top = anchor.Y;
            int count = _scaleRatios.Count;
            double item = 0;
            for (int i = 1; i < count; i++)
            {
                item = _scaleRatios[i];
                Rectangle rect = new Rectangle(anchor.X, top, GRIDSIZE, GRIDSIZE);
                if (item > scaleRatio)
                {                    
                    g.FillRectangle(_brush, rect);                    
                }
                g.DrawRectangle(_pen, rect);
                top += GRIDSIZE;
            }
        }
    }
}
