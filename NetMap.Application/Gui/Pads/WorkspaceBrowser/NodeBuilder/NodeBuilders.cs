﻿using System;
using System.Windows.Forms;
using NetMap.Application.Core;
using NetMap.Interfaces;

namespace NetMap.Application.WorkspaceBrowser
{
	public static class NodeBuilders
	{
		/// <summary>
		/// This method builds a ProjectBrowserNode Tree out of a given combine.
		/// </summary>
		public static TreeNode AddProjectNode(TreeNode motherNode, IWorkspace workspace)
		{
			IWorkspaceNodeBuilder   workspaceNodeBuilder = null;
			foreach (IWorkspaceNodeBuilder nodeBuilder in AddInTree.BuildItems<IWorkspaceNodeBuilder>("/Application/Views/WorkspaceBrowser/NodeBuilders", null, true)) 
            {
				if (nodeBuilder.CanBuildWorkspaceTree(workspace)) 
                {
                    workspaceNodeBuilder = nodeBuilder;
					break;
				}
			}
            if (workspaceNodeBuilder != null)
            {
                return workspaceNodeBuilder.AddWorkspaceNode(motherNode, workspace);
			}

			throw new NotImplementedException("can't create node builder for workspace");
		}
	}
}
