﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Data
{
    /// <summary>
    /// 数据接口
    /// </summary>
    public interface IData
    {
        /// <summary>
        /// 获取名字
        /// 文件名或表名
        /// </summary>
        string Name { get; }
        /// <summary>
        /// 获取和设置别名
        /// </summary>
        string Alias { get; set; }
        /// <summary>
        /// 获取字段集合
        /// </summary>
        IFields Fields { get; }
        /// <summary>
        /// 获取所属的数据库
        /// </summary>
        IDatabase Database { get; }
    }
}
