﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Fetcher;

namespace NetMap.Interfaces
{
    public delegate void MapDocumentChangedHandler(IMapDocument newDocument);
    public delegate void MapViewDrawHandler(IDisplay display);

    /// <summary>
    /// 图层改变类型
    /// </summary>
    public enum LayerChangeType
    {
        /// <summary>
        /// 新增
        /// </summary>
        Add = 1,
        /// <summary>
        /// 移除
        /// </summary>
        Remove = 2,
        /// <summary>
        /// 移动
        /// </summary>
        Move = 3,
        /// <summary>
        /// 移除所有图层
        /// </summary>
        AllRemove = 4,
        /// <summary>
        /// 当前图层变化
        /// </summary>
        CurrentChange = 5
    }

    public delegate void BeforeLayerChangedHandler(LayerChangeType type);
    public delegate void LayerChangedHandler(ILayer layer, LayerChangeType type);
    public delegate void FileDragDropHandler(string file);
    public delegate void MapViewChangedHandler(bool bChangeEnd, IDisplayTransformation trans);
    public delegate void MapViewScaleChangedHandler(double scale);

    /// <summary>
    /// 地图视图的事件接口
    /// </summary>
    public interface IMapViewEvent
    {
        /// <summary>
        /// 地图文档变化事件
        /// </summary>
        event MapDocumentChangedHandler MapDocumentChanged;
        /// <summary>
        /// 地图绘制开始事件
        /// </summary>
        event MapViewDrawHandler BeforeDraw;
        /// <summary>
        /// 地图绘制完成事件
        /// </summary>
        event MapViewDrawHandler AfterDraw;
        /// <summary>
        /// 文件拖入地图事件
        /// </summary>
        event FileDragDropHandler FileDragDrop;
        /// <summary>
        /// 地图视图改变事件
        /// </summary>
        event MapViewChangedHandler MapViewChanged;
        /// <summary>
        /// 地图视图比例尺改变事件
        /// </summary>
        event MapViewScaleChangedHandler MapViewScaleChanged;
    }

    /// <summary>
    /// 地图文档的事件接口
    /// </summary>
    public interface IMapDocumentEvent
    {
        /// <summary>
        /// 图层变化前事件
        /// </summary>
        event BeforeLayerChangedHandler BeforeLayerChanged;
        /// <summary>
        /// 图层变化事件
        /// </summary>
        event LayerChangedHandler LayerChanged;
        /// <summary>
        /// 数据变化事件
        /// </summary>
        event DataChangedEventHandler DataChanged;
    }
}
