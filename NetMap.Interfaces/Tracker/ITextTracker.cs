﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;

namespace NetMap.Interfaces.Tracker
{
    /// <summary>
    /// 文本橡皮条接口
    /// </summary>
    public interface ITextTracker
    {
        /// <summary>
        /// 设置文本内容
        /// </summary>
        string Text { get; set; }
        /// <summary>
        /// 设置文本的点位
        /// </summary>
        /// <param name="point"></param>
        void SetPoint(IPoint point);
        /// <summary>
        /// 设置注记的角度
        /// </summary>
        /// <param name="angle"></param>
        void SetAngle(double angle);
    }
}
