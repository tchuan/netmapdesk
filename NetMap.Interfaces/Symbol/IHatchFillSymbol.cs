﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace NetMap.Interfaces.Symbol
{
    /// <summary>
    /// 阴影面符号接口
    /// </summary>
    public interface IHatchFillSymbol : IFillSymbol
    {
        /// <summary>
        /// 阴影线条间空间的颜色
        /// </summary>
        Color BackgroundColor { get; set; }
        /// <summary>
        /// 阴影线条的颜色
        /// </summary>
        Color ForegroundColor { get; set; }
        /// <summary>
        /// 阴影样式
        /// </summary>
        HatchStyle HatchStyle { get; set; }
    }
}
