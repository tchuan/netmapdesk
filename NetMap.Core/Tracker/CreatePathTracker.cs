﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Tracker;
using NetMap.Interfaces.Geometry;
using NetMap.Geometry;
using NetMap.Interfaces.Symbol;
using System.Drawing;
using NetMap.Render;
using NetMap.Interfaces.Render;

namespace NetMap.Tracker
{
    /// <summary>
    /// 创建路径橡皮条
    /// </summary>
    public class CreatePathTracker : Tracker, ICreatePathTracker
    {
        private IPath _path = new Path();

        public CreatePathTracker()
            : base()
        {
        }

        public CreatePathTracker(ISymbol symbol)
            : base(symbol)
        {
        }

        public override void Reset()
        {
            _path = new Path();
            base.Reset();
        }

        public override IGeometry GetGeometry()
        {
            return _path;
        }

        public override void Start(IPoint point)
        {
            Add(point.X, point.Y);
        }

        public override void Draw(Graphics g)
        {
            if (_path.Count > 0)
            {
                ISymbolDrawer drawer = SymbolDrawerFactory.Instance.GetDrawer(Symbol);
                if (drawer != null)
                {
                    drawer.Draw(g, Symbol, _path, null);
                }
            }
        }

        public override void Draw(IDisplay display)
        {
            if (_path.Count > 0)
            {
                ISymbolDrawer drawer = SymbolDrawerFactory.Instance.GetDrawer(Symbol);
                if (drawer != null)
                {
                    drawer.Draw(display, Symbol, _path, null);
                }
            }
        }

        #region ICreatePathTracker 成员

        public void Add(double x, double y)
        {
            IPathPoint point = new PathPoint(x, y);
            point.InPoint = new NetMap.Geometry.Point(x, y);
            point.OutPoint = new NetMap.Geometry.Point(x, y);
            _path.Add(point);
        }

        //都是设置出点
        public void SetControlPoint(double x, double y)
        {
            IPathPoint endPoint = _path.EndPoint;
            endPoint.OutPoint.X = x;
            endPoint.OutPoint.Y = y;
            endPoint.InPoint.X = 2 * endPoint.X - x;
            endPoint.InPoint.Y = 2 * endPoint.Y - y;
        }

        public void Back()
        {
            _path.RemoveAt(_path.Count - 1);
        }

        public bool IsClose
        {
            get
            {
                return _path.IsClose;
            }
        }

        public void Close()
        {
            _path.IsClose = true;
        }

        public void Open()
        {
            _path.IsClose = false;
        }

        #endregion
    }
}
