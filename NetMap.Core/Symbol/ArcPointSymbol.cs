﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Geometry;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Symbol;
using System.Drawing;
using NetMap.Interfaces.Persist;
using System.Xml;

namespace NetMap.Symbol
{
    public class ArcPointSymbol : PointSymbol, IArcPointSymbol, IPersistResource
    {
        public ArcPointSymbol()
        {
            Width = 1;
            Fill = true;
            Outline = true;
            Color = Color.Black;
            FillColor = Color.White;
            Envelope = new Envelope(-3, -3, 3, 3);
            StartAngle = 0f;
            SweepAngle = 360f;
        }

        public ArcPointSymbol(ArcPointSymbol symbol)
        {
            //TODO
        }

        #region ISymbol 成员

        public override ISymbol Clone()
        {
            return new ArcPointSymbol(this);
        }

        public override string KindAbstract
        {
            get { return SymbolFactory.S_POINT_ARC; }
        }

        #endregion

        #region IArcPointSymbol 成员

        public float Width
        {
            get;
            set;
        }

        public bool Outline
        {
            get;
            set;
        }

        public bool Fill
        {
            get;
            set;
        }

        public Color FillColor
        {
            get;
            set;
        }

        public IEnvelope Envelope
        {
            get;
            set;
        }

        public float StartAngle
        {
            get;
            set;
        }

        public float SweepAngle
        {
            get;
            set;
        }

        #endregion

        #region IPersistResource 成员

        public void Save(XmlElement node, XmlDocument document)
        {
            XmlElement symbol = document.CreateElement("ArcPointSymbol");
            symbol.SetAttribute("Color", string.Format("#{0:X2}{1:X2}{2:X2}", Color.R, Color.G, Color.B, Color.A));
            symbol.SetAttribute("XOffset", XOffset.ToString("%.2f"));
            symbol.SetAttribute("YOffset", YOffset.ToString("%.2f"));
            symbol.SetAttribute("Angle", Angle.ToString("%.2f"));
            symbol.SetAttribute("Size", Angle.ToString("%.2f"));
            throw new NotImplementedException();
        }

        public void Load(XmlReader reader, object context)
        {
            throw new NotImplementedException();
        }

        #endregion

        /// <summary>
        /// 默认新实例
        /// </summary>
        public static ArcPointSymbol NewDefault
        {
            get
            {
                return new ArcPointSymbol();
            }
        }
    }
}
