﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Symbol
{
    /// <summary>
    /// 符号库接口，负责管理一组符号
    /// </summary>
    public interface ISymbolLibrary
    {
        /// <summary>
        /// 获取和设置符号库名称
        /// </summary>
        string Name { get; set; }        
        /// <summary>
        /// 查找符号
        /// </summary>
        /// <param name="code">符号编码</param>
        ISymbol FindSymbol(int code);
        /// <summary>
        /// 添加符号
        /// </summary>
        /// <param name="symbol"></param>
        void AddSymbol(ISymbol symbol);
        /// <summary>
        /// 更新符号
        /// </summary>
        /// <param name="code">符号编码</param>
        /// <param name="symbol">新符号</param>
        void UpdateSymbol(int code, ISymbol newSymbol);
        /// <summary>
        /// 移除符号
        /// </summary>
        /// <param name="code">符号编码</param>
        void RemoveSymbol(int code);        
        /// <summary>
        /// 编码是否已存在
        /// </summary>
        /// <param name="code">符号编码</param>
        /// <returns></returns>
        bool IsCodeExist(int code);
        /// <summary>
        /// 获取一个可用的编码
        /// </summary>
        /// <returns></returns>
        int GetAvailable();
        /// <summary>
        /// 重新编码
        /// 备注：符号将按照顺序重新编码（从startCode开始）
        /// </summary>
        /// <param name="startCode">起始编码</param>
        void Recode(int startCode);
        /// <summary>
        /// 获取当前所有符号
        /// </summary>
        /// <returns></returns>
        List<ISymbol> GetAllSymbol();
        /// <summary>
        /// 库中的符号数
        /// </summary>
        int Count { get; }
    }
}
