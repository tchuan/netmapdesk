﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.SpatialAnalysis;
using NetMap.Interfaces.Geometry;

namespace NetMap.SpatialAnalysis
{
    /// <summary>
    /// 与点的范围相交空间分析
    /// </summary>
    internal class PointRectIntersectsSA : ISpatialAnalysis
    {
        private IPoint _point;
        internal PointRectIntersectsSA(IPoint point)
        {
            _point = point;
        }

        public IGeometry Geometry
        {
            get
            {
                return _point;
            }
            set
            {
                _point = value as IPoint;
            }
        }

        public bool Evaluate(IGeometry geometry)
        {
            IEnvelope pointRect = _point.Envelope;
            return pointRect.IsIntersect(geometry.Envelope);
        }
    }
}
