﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;

namespace NetMap.Interfaces.SpatialAnalysis
{
    /// <summary>
    /// 空间分析接口
    /// </summary>
    public interface ISpatialAnalysis
    {
        /// <summary>
        /// 参考图形
        /// </summary>
        IGeometry Geometry { get; set; }
        /// <summary>
        /// 分析
        /// </summary>
        /// <param name="geometry"></param>
        /// <returns></returns>
        bool Evaluate(IGeometry geometry);
    }
}
