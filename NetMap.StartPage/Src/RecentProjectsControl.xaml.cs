﻿// Copyright (c) AlphaSierraPapa for the KJSuite Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using NetMap.Application.Core;
using NetMap.Application.Gui;

using Core = NetMap.Application.Core;

namespace NetMap.StartPage
{
	/// <summary>
	/// Interaction logic for RecentProjectsControl.xaml
	/// </summary>
	public partial class RecentProjectsControl : UserControl
	{
		public RecentProjectsControl()
		{
			InitializeComponent();
		}
		
		public static readonly DependencyProperty HeaderProperty = HeaderedContentControl.HeaderProperty.AddOwner(typeof(RecentProjectsControl));
		
		public object Header {
			get { return GetValue(HeaderProperty); }
			set { SetValue(HeaderProperty, value); }
		}
		
		void AsyncBuildRecentProjectList(object state)
		{
			List<RecentOpenItem> items = new List<RecentOpenItem>();
			foreach (string path in (string[])state) {
				Core.LoggingService.Debug("RecentProjectsControl: Looking up path '" + path + "'");
				FileInfo file = new FileInfo(path);
				if (file.Exists) {
					items.Add(
						new RecentOpenItem {
							Name = Path.GetFileNameWithoutExtension(path),
							LastModification = file.LastWriteTime.ToShortDateString(),
							Path = path
						});
				}
			}
			if (items.Count > 0) {
				WorkbenchSingleton.SafeThreadAsyncCall(new Action(
					delegate {
						lastProjectsListView.ItemsSource = items;
						lastProjectsListView.Visibility = Visibility.Visible;
					}));
			}
		}
		
		class RecentOpenItem : INotifyPropertyChanged
		{
			public string Name { get; set; }
			public string LastModification { get; set; }
			public string Path { get; set; }
			
			event System.ComponentModel.PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged { add { } remove { } }
			
			public override string ToString()
			{
				return this.Name;
			}
		}

        void openSolutionClick(object sender, RoutedEventArgs e)
        {
            //new NetMap.Application.Commands.OpenFile().Run();            
        }

        void newSolutionClick(object sender, RoutedEventArgs e)
        {
            //new NetMap.Application.Commands.CreateNewFile().Run();
        }
	}
}
