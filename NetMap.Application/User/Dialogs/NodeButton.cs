﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace User.Dialogs
{
    class NodeButton : Aga.Controls.Tree.NodeControls.NodeTextBox
    {
        private string _btnText;
        /// <summary>
        /// 点击事件
        /// </summary>
        public event EventHandler BtnClick;

        public NodeButton(string btnText)
        {
            _btnText = btnText;
            _font = new Font("微软雅黑", 10, FontStyle.Underline);
        }

        public override object GetValue(Aga.Controls.Tree.TreeNodeAdv node)
        {
            //return base.GetValue(node);
            return _btnText;
        }

        public override void MouseUp(Aga.Controls.Tree.TreeNodeAdvMouseEventArgs args)
        {
            base.MouseUp(args);
            if (BtnClick != null)
            {
                BtnClick(this, null);
            }
        }

        private Font _font;
        public override void Draw(Aga.Controls.Tree.TreeNodeAdv node, Aga.Controls.Tree.DrawContext context)
        {
            //base.Draw(node, context);

            context.Graphics.DrawString(_btnText, _font,
                Brushes.Magenta, context.Bounds.Left, context.Bounds.Top + 3);
        }
    }
}
