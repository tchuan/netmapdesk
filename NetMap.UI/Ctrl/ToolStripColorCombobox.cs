﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetMap.UI.Ctrl
{
    public class ToolStripColorComboBox:ToolStripControlHost
    {
        public ToolStripColorComboBox()
            : base(new ColorCombobox())
        { }
        public ColorCombobox GetColorComboBox()
        {
            return Control as ColorCombobox;
        }
    }
}
