﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetMap.UI.Ctrl
{
    /// <summary>
    /// 线型编辑控件
    /// 备注：限制了大小
    /// </summary>
    public partial class LineStyleEditor : UserControl
    {
        private int _curPos = 0; // 默认位置为开始处
        private const int GRID_SIZE = 10; // 格网大小
        private const int GRID_NUM = 50;
        private const int GAP = 10; // 上下空隙宽度
        private bool[] _grids = null;
        private Pen _gridPen = new Pen(Color.Black, 1);
        // 表示网格意义的画刷
        private Brush _gridTrueBrush = Brushes.Black;
        private Brush _gridFalseBrush = Brushes.White;
        private Brush _gridUnknownBrush = Brushes.Gray;
        // 控制杆控制
        private bool _bPole = false;

        public LineStyleEditor()
        {
            InitializeComponent();
            _grids = new bool[GRID_NUM];
            for (int i = 0; i < GRID_NUM; i++)
            {
                _grids[i] = false;
            }
            Width = GRID_SIZE * GRID_NUM;
            Height = GAP + GRID_SIZE + GAP;
        }

        /// <summary>
        /// 线型的设置和获取
        /// </summary>
        public bool[] Grids
        {
            get
            {
                // 获取线型
                if (_curPos == 0)
                {
                    return null; // 表示实线
                }
                bool[] outer = new bool[_curPos];
                for (int i = 0; i < _curPos; i++)
                {
                    outer[i] = _grids[i];
                }
                return outer;
            }
            set
            {
                // 设置线型（超出部分将丢失）
                bool[] outer = value;
                if (outer == null)
                {
                    _curPos = 0;
                    for (int i = _curPos; i < GRID_NUM; i++)
                    {
                        _grids[i] = false;
                    }
                    return;
                }
                for (int i = 0; i < outer.Length; i++)
                {
                    if (i >= GRID_NUM)
                    {
                        break;
                    }
                    _grids[i] = outer[i];
                }
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            using (Image img = new Bitmap(Width, Height))
            {
                using (Graphics g = Graphics.FromImage(img))
                {
                    // 画控制杆
                    g.FillRectangle(_gridTrueBrush, _curPos * GRID_SIZE - 0.5f * GRID_SIZE, 0,
                        GRID_SIZE, GRID_SIZE + 2 * GAP);
                    // 画格网
                    for (int i = 0; i < 50; i++)
                    {
                        if (i < _curPos) // 有效区域
                        {
                            if (_grids[i])
                            {
                                g.FillRectangle(_gridTrueBrush, i * GRID_SIZE, GAP, GRID_SIZE, GRID_SIZE);
                            }
                            else
                            {
                                g.FillRectangle(_gridFalseBrush, i * GRID_SIZE, GAP, GRID_SIZE, GRID_SIZE);
                            }
                        }
                        else             // 无效区域
                        {
                            g.FillRectangle(_gridUnknownBrush, i * GRID_SIZE, GAP, GRID_SIZE, GRID_SIZE);
                        }
                        g.DrawRectangle(_gridPen, i * GRID_SIZE, GAP, GRID_SIZE, GRID_SIZE);
                    }
                }
                e.Graphics.DrawImage(img, 0, 0);
            }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            if (e.X >= _curPos * GRID_SIZE - 0.5f * GRID_SIZE && e.X < (_curPos + 1) * GRID_SIZE - 0.5f * GRID_SIZE)
            {
                _bPole = true;
            }
            else
            {
                //控制格子点的真假
                int pos = (int)Math.Ceiling(((double)e.X) / GRID_SIZE) - 1;
                if (pos < _curPos)
                {
                    _grids[pos] = !_grids[pos];
                    Refresh();
                }
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (_bPole)
            {
                _curPos = (int)Math.Round(((double)e.X) / GRID_SIZE, 0);
                if (_curPos < 0)
                {
                    _curPos = 0;
                }
                else if (_curPos > GRID_NUM)
                {
                    _curPos = GRID_NUM;
                }
                // 掩盖部分置为假
                for (int i = _curPos; i < GRID_NUM; i++)
                {
                    _grids[i] = false;
                }
                Refresh();
            }
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            if (_bPole)
            {
                _bPole = false;
            }
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            Width = GRID_SIZE * GRID_NUM;
            Height = GAP + GRID_SIZE + GAP;
        }
    }
}
