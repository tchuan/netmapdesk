﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Application.Core.Services
{
    /// <summary>
    /// 权限服务接口
    /// </summary>
    public interface IPermissionService
    {
        /// <summary>
        /// 类是否可用
        /// </summary>
        /// <param name="cls">类名</param>
        /// <returns></returns>
        bool IsEnable(string cls);
    }

    /// <summary>
    /// 权限服务
    /// </summary>
    public static class PermissionService
    {
        /// <summary>
        /// 获取和设置许可服务
        /// </summary>
        public static IPermissionService PermissionServiceInstance
        {
            get;
            set;
        }
    }
}
