﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Geometry;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Symbol;
using System.Drawing;

namespace NetMap.Symbol
{
    public class BoxPointSymbol : PointSymbol, IBoxPointSymbol
    {
        public BoxPointSymbol()
        {
            Color = Color.Black;

            Outline = true;
            Fill = true;
            Width = 1.0f;
            FillColor = Color.White;
            Box = new Envelope(-2, -2, 2, 2);
        }

        public BoxPointSymbol(BoxPointSymbol other)
        {
            //TODO
        }

        #region ISymbol 成员

        public override ISymbol Clone()
        {
            return new BoxPointSymbol(this);
        }

        public override string KindAbstract
        {
            get { return SymbolFactory.S_POINT_BOX; }
        }

        #endregion

        #region IBoxPointSymbol 成员

        public bool Outline
        {
            get;
            set;
        }

        public bool Fill
        {
            get;
            set;
        }

        public float Width
        {
            get;
            set;
        }

        public Color FillColor
        {
            get;
            set;
        }

        public IEnvelope Box
        {
            get;
            set;
        }

        #endregion
    }
}
