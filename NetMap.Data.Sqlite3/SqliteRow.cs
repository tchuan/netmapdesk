﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Data;
using NetMap.Interfaces.Data;
using Community.CsharpSqlite;
using NetMap.Interfaces;

namespace NetMap.Sqlite
{
    /// <summary>
    /// Sqlite数据库记录
    /// </summary>
    internal class SqliteRow : IRow
    {
        /// <summary>
        /// 未分配的OID
        /// </summary>
        internal const int NEW_OID = -1;

        protected IFields _fields = new Fields();
        protected long _oid = 0;
        protected SqliteTable _table = null;
        protected List<object> _values = new List<object>(); //字段值
        protected List<bool> _modifies = new List<bool>(); //字段值修改标志
        protected RowState _state = RowState.geoRSNew; //记录状态

        internal SqliteRow()
        {
        }

        #region IRow 成员

        public IFields Fields
        {
            get { return _fields; }
            set { _fields = value; }
        }

        public long OID
        {
            get { return _oid; }
            internal set { _oid = value; }
        }

        public ITable Table
        {
            get { return _table; }
        }

        public object GetValue(int index)
        {
            return _values[index];
        }

        public void SetValue(int index, object value)
        {
            _values[index] = value;
            _modifies[index] = true;
        }

        public void Delete()
        {
            if (_oid == SqliteRow.NEW_OID || _state != RowState.geoRSExist)
            {
                return;
            }
            StringBuilder builder = new StringBuilder();
            builder.Append("delete from ");
            builder.Append(_table._name);
            builder.Append(" where ");
            builder.Append(SpatialDbDefine.FIELD_OID).Append("=").Append(_oid);
            string sql = builder.ToString();
            int err = Sqlite3.sqlite3_exec(_table._db, sql, 0, 0, 0);
            if (err != Sqlite3.SQLITE_OK)
            {
                throw new Exception(Sqlite3.sqlite3_errmsg(_table._db));
            }
            _state = RowState.geoRSDelete; //标识状态为删除状态
        }

        public void Store()
        {
            switch (_state)
            {
                case RowState.geoRSNew:
                    {
                        insert();
                        break;
                    }
                case RowState.geoRSExist:
                    {
                        update();
                        break;
                    }
                case RowState.geoRSDelete:
                    {
                        //已删除对象无操作
                        break;
                    }
            }
        }

        #endregion

        //初始化字段值
        internal virtual void InitValue(Sqlite3.Vdbe stmt)
        {            
            int count = _fields.Count;
            _oid = Sqlite3.sqlite3_column_int(stmt, count);
            for (int i = 0; i < count; i++)
            {
                switch (_fields.GetField(i).Type)
                {
                    case geoFieldType.GEO_SMALLINT:
                        {
                            _values[i] = (short)Sqlite3.sqlite3_column_int(stmt, i);
                            break;
                        }
                    case geoFieldType.GEO_INT:
                        {
                            _values[i] = Sqlite3.sqlite3_column_int(stmt, i);
                            break;
                        }
                    case geoFieldType.GEO_SINGLE:
                        {
                            _values[i] = (float)Sqlite3.sqlite3_column_double(stmt, i);
                            break;
                        }
                    case geoFieldType.GEO_DOUBLE:
                        {
                            _values[i] = Sqlite3.sqlite3_column_double(stmt, i);
                            break;
                        }
                    case geoFieldType.GEO_STRING:
                        {
                            _values[i] = Sqlite3.sqlite3_column_text(stmt, i);
                            break;
                        }
                    case geoFieldType.GEO_BLOB:
                        {
                            _values[i] = Sqlite3.sqlite3_column_blob(stmt, i);
                            break;
                        }
                    case geoFieldType.GEO_DATE:
                        {
                            //datetime字段的处理
                            string value = Sqlite3.sqlite3_column_text(stmt, i);
                            _values[i] = DateTime.Parse(value);
                            break;
                        }
                    case geoFieldType.GEO_GEOMETRY:
                        {
                            //在关系表中，GEOMETRY按BLOB处理
                            _values[i] = Sqlite3.sqlite3_column_blob(stmt, i);
                            break;
                        }
                }
            }
            _state = RowState.geoRSExist;
        }

        //初始化字段信息
        internal virtual void InitFields(SqliteTable table, IFields fields)
        {
            _table = table;
            //拷贝字段信息
            int count = fields.Count;
            for (int i = 0; i < count; i++)
            {
                _fields.SetField(i, new Field(fields.GetField(i)));
            }
            //初始化
            _values.AddRange(new object[count]); //TODO 设置默认值
            _modifies.AddRange(new bool[count]);
            //设置修改标志为假
            for (int i = 0; i < count; i++)
            {
                _modifies[i] = false;
            }
            _state = RowState.geoRSNew; //新行
        }

        #region 插入

        //插入新记录
        protected virtual void insert()
        {
            insert_check();

            Sqlite3.Vdbe stmt = null;
            int err = insert_sql(ref stmt);
            if (err != Sqlite3.SQLITE_OK)
            {
                return;
            }

            err = Sqlite3.sqlite3_step(stmt);
            if (err != Sqlite3.SQLITE_DONE)
            {
                Sqlite3.sqlite3_finalize(stmt);
                Error.Error.Record(err, Sqlite3.sqlite3_errmsg(_table._db));
                return;
            }

            _oid = Sqlite3.sqlite3_last_insert_rowid(_table._db);
            Sqlite3.sqlite3_finalize(stmt);

            _state = RowState.geoRSExist;
        }

        //插入检查
        protected virtual void insert_check()
        {
            foreach (object obj in _values)
            {
                if (obj == null)
                {
                    throw new NullValueException("字段值为空，不能插入表中");
                }
            }            
        }

        //构建插入的stmt
        protected virtual int insert_sql(ref Sqlite3.Vdbe stmt)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("insert into ");
            builder.Append(_table._name).Append("(");
            int count = _fields.Count;
            for (int i = 0; i < count; i++)
            {
                if (i != 0)
                {
                    builder.Append(",");                   
                }
                builder.Append(_fields.GetField(i).Name);
            }
            builder.Append(") values(");
            for (int i = 0; i < count; i++)
            {
                builder.Append(":").Append(_fields.GetField(i).Name);
                if (i != count - 1)
                {
                    builder.Append(",");
                }
            }
            builder.Append(")");
            string sql = builder.ToString();
            string tail = "";
            int err = Sqlite3.sqlite3_prepare_v2(_table._db, sql, sql.Length, ref stmt, ref tail);
            if (err != Sqlite3.SQLITE_OK)
            {
                Error.Error.Record(err, tail);
                return err;
            }
            IField field = null;
            for (int i = 0; i < count; i++)
            {
                field = _fields.GetField(i);
                bind_value(stmt, field.Type, ":" + field.Name, _values[i]);
            }
            return err;
        }
        
        //插入完成
        protected virtual void insert_over()
        {
            //什么都不做，提供给子类重载
        }

        #endregion

        protected void bind_value(Sqlite3.Vdbe stmt, geoFieldType type, string name, object value)
        {
            int index = Sqlite3.sqlite3_bind_parameter_index(stmt, name);
            switch (type)
            {
                case geoFieldType.GEO_SMALLINT:
                    {
                        Sqlite3.sqlite3_bind_int(stmt, index, (short)value);
                        break;
                    }
                case geoFieldType.GEO_INT:
                    {
                        Sqlite3.sqlite3_bind_int(stmt, index, (int)value);
                        break;
                    }
                case geoFieldType.GEO_BIGINT:
                    {
                        Sqlite3.sqlite3_bind_int64(stmt, index, (long)value);
                        break;
                    }
                case geoFieldType.GEO_SINGLE:
                    {
                        Sqlite3.sqlite3_bind_double(stmt, index, (float)value);
                        break;
                    }
                case geoFieldType.GEO_DOUBLE:
                    {
                        Sqlite3.sqlite3_bind_double(stmt, index, (double)value);
                        break;
                    }
                case geoFieldType.GEO_STRING:
                    {
                        Sqlite3.sqlite3_bind_text(stmt, index, value.ToString(), -1, null); 
                        break;
                    }                
                case geoFieldType.GEO_DATE:
                    {
                        DateTime time = (DateTime)value;
                        Sqlite3.sqlite3_bind_text(stmt, index, time.ToString("s"), -1, null);
                        break;
                    }
                case geoFieldType.GEO_GEOMETRY:
                    {
                        byte[] bts = (value as NetMap.Interfaces.Geometry.IGeometry).ExportToWkb();
                        Sqlite3.sqlite3_bind_blob(stmt, index, bts, bts.Length, null);                            
                        break;
                    }
                case geoFieldType.GEO_BLOB:
                    {
                        byte[] bts = (byte[])value;
                        Sqlite3.sqlite3_bind_blob(stmt, index, bts, bts.Length, null);
                        break;
                    }
            }
        }
                
        #region 更新

        //更新记录
        protected virtual void update()
        {
            int err = update_check();
            if (err != Sqlite3.SQLITE_OK)
            {
                return;
            }

            Sqlite3.Vdbe stmt = null;
            err = update_sql(ref stmt);
            if (err != Sqlite3.SQLITE_OK)
            {
                return;
            }

            err = Sqlite3.sqlite3_step(stmt);
            if (err != Sqlite3.SQLITE_DONE)
            {
                Sqlite3.sqlite3_finalize(stmt);
                Error.Error.Record(err, Sqlite3.sqlite3_errmsg(_table._db));
                return;
            }

            Sqlite3.sqlite3_finalize(stmt);

            update_over();
        }

        //更新时，检查字段值
        protected virtual int update_check()
        {
            if (_modifies.IndexOf(true) < 0)
            {
                Error.Error.Record(SpatialDbDefine.GIS_DBERR_NOMODIFY, "图形值未修改");
                return SpatialDbDefine.GIS_DBERR_NOMODIFY;
            }
            return Sqlite3.SQLITE_OK;
        }

        //构建更新的stmt
        protected virtual int update_sql(ref Sqlite3.Vdbe stmt)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("update ").Append(_table._name).Append(" set ");
            int count = _fields.Count;
            IField field = null;
            bool first = true;
            for (int i = 0; i < count; i++)
            {
                field = _fields.GetField(i);
                if (_modifies[i])
                {
                    if (first)
                    {
                        first = false;
                    }
                    else
                    {
                        builder.Append(",");
                    }
                    builder.Append(field.Name).Append("=:").Append(field.Name);
                }
            }
            builder.Append(" where ").Append(SpatialDbDefine.FIELD_OID);
            builder.Append("=").Append(_oid);

            string sql = builder.ToString();
            string tail = "";
            int err = Sqlite3.sqlite3_prepare_v2(_table._db, sql, sql.Length, ref stmt, ref tail);
            if (err != Sqlite3.SQLITE_OK)
            {
                Error.Error.Record(err, tail);
                return err;
            }
            for (int i = 0; i < count; i++)
            {
                if (_modifies[i])
                {
                    field = _fields.GetField(i);
                    bind_value(stmt, field.Type, ":" + field.Name, _values[i]);
                }
            }
            return err;
        }

        //更新完成
        protected virtual void update_over()
        {
            int count = _modifies.Count;
            for (int i = 0; i < count; i++)
            {
                _modifies[i] = false;
            }
        }

        #endregion
    }
}
