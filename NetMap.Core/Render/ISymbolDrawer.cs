﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Symbol;
using NetMap.Core.Render;
using System.Drawing;

namespace NetMap.Render
{
    /// <summary>
    /// 符号绘制接口
    /// </summary>
    public interface ISymbolDrawer
    {
        /// <summary>
        /// 绘制地图对象（需要坐标转换）
        /// </summary>
        /// <param name="display"></param>
        /// <param name="symbol"></param>
        /// <param name="geometry"></param>
        /// <param name="text"></param>
        void Draw(IDisplay display, ISymbol symbol, IGeometry geometry, string text);
        /// <summary>
        /// 绘制普通对象
        /// </summary>
        /// <param name="g"></param>
        /// <param name="symbol"></param>
        /// <param name="geometry"></param>
        /// <param name="text"></param>
        void Draw(Graphics g, ISymbol symbol, IGeometry geometry, string text);
    }
}
