﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Application.Commands
{
    public class ToolsViewMenuBuilder : ViewMenuBuilder
    {
        protected override string Category
        {
            get
            {
                return "Tools";
            }
        }
    }

    public class MainViewMenuBuilder : ViewMenuBuilder
    {
        protected override string Category
        {
            get
            {
                return "Main";
            }
        }
    }
}
