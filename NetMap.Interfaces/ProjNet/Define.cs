﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.ProjNet
{
    /// <summary>
    /// SKT
    /// 2013年7月5号添加
    /// </summary>
    public class SKTDefine
    {
        /// <summary>
        /// google地图投影的SKT
        /// Web Mercator 投影系统使用
        /// </summary>
        public const string Web_Mercator = "PROJCS[\"Web Mercator\",GEOGCS[\"GCS_WGS_1984\",DATUM[\"D_WGS_1984\",SPHEROID[\"WGS_1984\",6378137.0,298.257223563]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Mercator\"],PARAMETER[\"False_Easting\",0.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",0.0],PARAMETER[\"Standard_Parallel_1\",0.0],PARAMETER[\"Latitude_Of_Origin\",0],UNIT[\"Meter\",1.0]]";
        /// <summary>
        /// China2000地理坐标的SKT
        /// </summary>
        public const string China_2000 = "GEOGCS[\"GCS_China_Geodetic_Coordinate_System_2000\",DATUM[\"D_China_2000\",SPHEROID[\"CGCS2000\",6378137.0,298.257222101]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]]";
    }
}
