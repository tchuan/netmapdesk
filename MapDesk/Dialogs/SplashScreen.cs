﻿// Copyright (c) AlphaSierraPapa for the SharpDevelop Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace MapDesk
{
    public class SplashScreenForm : Form
    {
        static SplashScreenForm splashScreen;
        static List<string> requestedFileList = new List<string>();
        static List<string> parameterList = new List<string>();
        Bitmap bitmap;
        
        public static SplashScreenForm SplashScreen {
            get {
                return splashScreen;
            }
            set {
                splashScreen = value;
            }
        }
        
        public SplashScreenForm()
        {
            const string text1 = "云南省高速公路沥青路面碎石场管理系统";
            const string text2 = " 云南省公路开发投资有限责任公司\n" +
                                 "云南云岭高速公路交通科技有限公司\n" +
                                 "  云南龙瑞高速公路建设指挥部\n" +
                                 "招商局重庆交通科研设计院有限公司\n" +
                                 "         2015年12月";
            
            FormBorderStyle = FormBorderStyle.None;
            StartPosition   = FormStartPosition.CenterScreen;
            ShowInTaskbar   = false;
            // Stream must be kept open for the lifetime of the bitmap
            Assembly assembly = typeof(SplashScreenForm).Assembly;
            string assemblyName = assembly.GetName().Name;
            bitmap = new Bitmap(assembly.GetManifestResourceStream(assemblyName + ".Resources.SplashScreen.png"));
            this.ClientSize = bitmap.Size;
            using (Font font = new Font("黑体", 28)) {
                using (Graphics g = Graphics.FromImage(bitmap)) {
                    g.DrawString(text1, font, Brushes.Yellow, 166 - 3*44, 58);
                }
            }
            using (Font font = new Font("黑体", 20)) {
                using (Graphics g = Graphics.FromImage(bitmap)) {
                    g.DrawString(text2, font, Brushes.Yellow, 166 - 3*12, 98);
                }
            }
            BackgroundImage = bitmap;
        }
        
        public static void ShowSplashScreen()
        {
            splashScreen = new SplashScreenForm();
            splashScreen.Show();
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                if (bitmap != null) {
                    bitmap.Dispose();
                    bitmap = null;
                }
            }
            base.Dispose(disposing);
        }
        
        public static string[] GetParameterList()
        {
            return parameterList.ToArray();
        }
        
        public static string[] GetRequestedFileList()
        {
            return requestedFileList.ToArray();
        }
        
        public static void SetCommandLineArgs(string[] args)
        {
            requestedFileList.Clear();
            parameterList.Clear();
            
            foreach (string arg in args) {
                if (arg.Length == 0) continue;
                if (arg[0] == '-' || arg[0] == '/') {
                    int markerLength = 1;
                    
                    if (arg.Length >= 2 && arg[0] == '-' && arg[1] == '-') {
                        markerLength = 2;
                    }
                    
                    string param = arg.Substring(markerLength);
                    // work around .NET "feature" that causes trouble with /addindir:"c:\temp\"
                    // http://www.mobzystems.com/code/bugingetcommandlineargs.aspx
                    if (param.EndsWith("\"", StringComparison.Ordinal))
                        param = param.Substring(0, param.Length - 1) + "\\";
                    parameterList.Add(param);
                } else {
                    requestedFileList.Add(arg);
                }
            }
        }
    }
}
