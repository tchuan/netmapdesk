﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjNet.CoordinateSystems;
using NetMap.Interfaces.Geometry;

namespace NetMap.Interfaces.Data
{
    /// <summary>
    /// 要素集接口
    /// </summary>
    public interface IFeatureClass : IData
    {
        /// <summary>
        /// 获取所属的空间数据库
        /// </summary>
        ISpatialDatabase SpatialDatabase { get; }
        /// <summary>
        /// 创建新要素
        /// </summary>
        /// <returns></returns>
        IFeature CreateFeature();
        /// <summary>
        /// 获取要素
        /// </summary>
        /// <param name="oid">要素OID</param>
        /// <returns></returns>
        IFeature GetFeature(long oid);
        /// <summary>
        /// 创建空间索引
        /// </summary>
        void CreateSpatialIndex();
        /// <summary>
        /// 执行查询
        /// </summary>
        /// <param name="filter">查询条件</param>
        IFeatureCursor Search(IQueryFilter filter);
        /// <summary>
        /// 执行选择
        /// </summary>
        /// <param name="filter">查询条件</param>
        /// <returns></returns>
        ISelectionSet Select(IQueryFilter filter);
        /// <summary>
        /// 获取图形类型
        /// </summary>
        /// <returns></returns>
        geoGeometryType GeometryType
        {
            get;
        }
        /// <summary>
        /// 获取要素类型
        /// </summary>
        /// <returns></returns>
        geoFeatureType FeatureType
        {
            get;
        }
        /// <summary>
        /// 获取数据范围
        /// </summary>
        /// <returns></returns>
        IEnvelope Extent
        {
            get;
        }
        /// <summary>
        /// 获取坐标系统
        /// </summary>
        ICoordinateSystem CoordinateSystem { get; }
        /// <summary>
        /// 清空要素
        /// </summary>
        void ClearFeature();
    }
}
