﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Application.WorkspaceBrowser.TreeNodes
{
    public class MapFolderNode : AbstractWorkspaceBrowserTreeNode
    {
        public override object AcceptVisitor(WorkspaceBrowserTreeNodeVisitor visitor, object data)
        {
            return visitor.Visit(this, data);
        }
    }
}
