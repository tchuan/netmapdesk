﻿// Copyright 2010 - Paul den Dulk (Geodan)
//
// This file is part of Mapsui.
// Mapsui is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// Mapsui is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Mapsui; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 

using System;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Data;
using NetMap.Interfaces.Geometry;

/*
 * 2013-5-28: 修改加入NetMap
 */

namespace NetMap.Interfaces.Fetcher
{
    /// <summary>
    /// 异步数据获取接口
    /// </summary>
    public interface IAsyncDataFetcher
    {
        /// <summary>
        /// 停止获取
        /// </summary>
        void AbortFetch();
        /// <summary>
        /// 视图变化
        /// </summary>
        /// <param name="changeEnd">变化是否结束</param>
        /// <param name="trans">变化后的视图信息</param>
        void ViewChanged(bool changeEnd, IDisplayTransformation trans);
        /// <summary>
        /// 数据已变化事件
        /// </summary>
        event DataChangedEventHandler DataChanged;
        /// <summary>
        /// 清理缓存
        /// </summary>
        void ClearCache();
    }

    public delegate void DataChangedEventHandler(DataChangedEventArgs e);

    /// <summary>
    /// 数据改变信息
    /// </summary>
    public class DataChangedEventArgs
    {
        public DataChangedEventArgs(bool canceled, int layerTag, IEnvelope box)
        {
            Canceled = canceled;
            LayerTag = layerTag;
            Box = box;
        }

        /// <summary>
        /// 获取是否取消
        /// </summary>
        public bool Canceled { get; private set; }
        /// <summary>
        /// 获取和设置图层id
        /// </summary>
        public int LayerTag { get; set; }
        /// <summary>
        /// 获取范围
        /// </summary>
        public IEnvelope Box { get; private set; }
    }
}
