﻿// Copyright (c) AlphaSierraPapa for the KJSuite Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

using System;
using System.Windows.Forms;
using NetMap.Application.Core;
using NetMap.Application.Gui;
using NetMap.Application.WorkspaceBrowser.TreeNodes;

namespace NetMap.Application.WorkspaceBrowser
{
	public class WorkspaceBrowserPad : AbstractPadContent
	{
		static WorkspaceBrowserPad instance;
		public static WorkspaceBrowserPad Instance {
			get {
				if (instance == null) {
					PadDescriptor pad = WorkbenchSingleton.Workbench.GetPad(typeof(WorkspaceBrowserPad));
					if (pad != null) {
						pad.CreatePad();
					} else {
						// Pad is not used (stripped-down SD version, e.g. SharpReport)
						// Create dummy pad to prevent NullReferenceExceptions
						instance = new WorkspaceBrowserPad();
					}
				}
				return instance;
			}
		}
		WorkspaceBrowserPanel workspaceBrowserPanel = new WorkspaceBrowserPanel();
		
		public AbstractWorkspaceBrowserTreeNode SelectedNode {
			get {
				return workspaceBrowserPanel.SelectedNode;
			}
		}
		
		public WorkspaceBrowserControl WorkspaceBrowserControl {
			get {
				return workspaceBrowserPanel.ProjectBrowserControl;
			}
		}
		
		public override object Control {
			get {
				return workspaceBrowserPanel;
			}
		}
		
		public WorkspaceBrowserPad()
		{
			instance = this;
		}
		
		public void StartLabelEdit(ExtTreeNode node)
		{
			WorkspaceBrowserControl.TreeView.StartLabelEdit(node);
		}
	}
}
