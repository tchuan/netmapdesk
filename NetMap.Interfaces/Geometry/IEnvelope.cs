﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Geometry
{
    /// <summary>
    /// 范围
    /// </summary>
    public interface IEnvelope
    {
        #region 属性

        /// <summary>
        /// X方向最大值
        /// </summary>
        double XMax { get; set; }
        /// <summary>
        /// Y方向最大值
        /// </summary>
        double YMax { get; set; }
        /// <summary>
        /// X方向最小值
        /// </summary>
        double XMin { get; set; }
        /// <summary>
        /// Y方向最小值
        /// </summary>
        double YMin { get; set; }
        /// <summary>
        /// 高度
        /// </summary>
        double Height { get; }
        /// <summary>
        /// 宽度
        /// </summary>
        double Width { get; }
        /// <summary>
        /// 中心点
        /// </summary>
        IPoint Center { get; }
        /// <summary>
        /// 获取（笛卡尔坐标系下的左下角）
        /// </summary>
        IPoint MinxMiny { get; }
        /// <summary>
        /// 获取（笛卡尔坐标系下的左上角）
        /// </summary>
        IPoint MinxMaxy { get; }
        /// <summary>
        /// 获取（笛卡尔坐标系下的右下角）
        /// </summary>
        IPoint MaxxMiny { get; }
        /// <summary>
        /// 获取（笛卡尔坐标系下的右上角）
        /// </summary>
        IPoint MaxyMaxy { get; }
        /// <summary>
        /// 获取（笛卡尔坐标系下的左点）
        /// </summary>
        IPoint MinxCenter { get; }
        /// <summary>
        /// 获取（笛卡尔坐标系下的下点）
        /// </summary>
        IPoint MinyCenter { get; }
        /// <summary>
        /// 获取（笛卡尔坐标系下的右点）
        /// </summary>
        IPoint MaxxCenter { get; }
        /// <summary>
        /// 获取（笛卡尔坐标系下的上点）
        /// </summary>
        IPoint MaxyCenter { get; }
        #endregion

        #region 关系

        /// <summary>
        /// 是否包含
        /// </summary>
        /// <param name="other">范围</param>
        /// <returns>关系判断结果</returns>
        bool IsContain(IEnvelope other);
        /// <summary>
        /// 是否包含
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        bool IsContain(IPoint point);
        /// <summary>
        /// 是否相交
        /// </summary>
        /// <param name="other">范围</param>
        /// <returns>关系判断结果</returns>
        bool IsIntersect(IEnvelope other);

        #endregion

        #region 操作

        /// <summary>
        /// 并操作
        /// </summary>
        /// <param name="other">范围</param>
        void Union(IEnvelope other);
        /// <summary>
        /// 并操作
        /// </summary>
        /// <param name="point"></param>
        void Union(IPoint point);
        /// <summary>
        /// 更新范围
        /// </summary>
        /// <param name="other"></param>
        void Update(IEnvelope other);
        /// <summary>
        /// 更新范围
        /// </summary>
        /// <param name="minx"></param>
        /// <param name="miny"></param>
        /// <param name="maxx"></param>
        /// <param name="maxy"></param>
        void Update(double minx, double miny, double maxx, double maxy);
        /// <summary>
        /// 改变中心点
        /// </summary>
        void ChangeCenter(double x, double y);
      
        #endregion
    }
}
