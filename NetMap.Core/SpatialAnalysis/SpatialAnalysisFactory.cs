﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Data;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.SpatialAnalysis;

/*
 * 2014-06-22: 不特殊处理DirectionPoint和PathPoint
 */

namespace NetMap.SpatialAnalysis
{
    /// <summary>
    /// 空间分析工厂
    /// </summary>
    public class SpatialAnalysisFactory
    {
        /// <summary>
        /// 创建空间分析对象
        /// </summary>
        /// <param name="filter">空间过滤条件</param>
        /// <returns></returns>
        public static ISpatialAnalysis Create(ISpatialFilter filter)
        {
            if (filter.Geometry == null)
            {
                return null;
            }
            IGeometry geometry = filter.Geometry;
            switch (geometry.GeometryType)
            {
                case geoGeometryType.GEO_GEOMETRY_POINT:
                case geoGeometryType.GEO_GEOMETRY_DIRECTIONPOINT:
                    {
                        return CreatePoint(filter, geometry as IPoint);
                    }
                case geoGeometryType.GEO_GEOMETRY_LINESTRING:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_POLYGON:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTIPOINT:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTILINESTRING:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTIPOLYGON:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_PATH:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_RECT:
                    {
                        return CreateRect(filter, geometry as IEnvelope);
                    }
                case geoGeometryType.GEO_GEOMETRY_CIRCLE:
                    {
                        return CreateCircle(filter, geometry as ICircle);
                    }
                default:
                    {
                        throw new NotSupportedException("不支持图形类型");
                    }
            }
            return null;
        }

        #region 分析类型

        private static ISpatialAnalysis CreatePoint(ISpatialFilter filter, IPoint point)
        {
            switch (filter.SpatialRelationship)
            {
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_DISJOINT:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_TOUCHES:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_CROSSES:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_WITHINS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_OVERLAPS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_CONTAINS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_INTERSECTS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_EQUALS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_ENVELOPEINTERSECTS:
                    {
                        return new PointRectIntersectsSA(point);
                    }
            }
            return null;
        }

        private static ISpatialAnalysis CreateLineString(ISpatialFilter filter, ILineString line)
        {
            switch (filter.SpatialRelationship)
            {
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_DISJOINT:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_TOUCHES:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_CROSSES:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_WITHINS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_OVERLAPS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_CONTAINS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_INTERSECTS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_EQUALS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_ENVELOPEINTERSECTS:
                    {
                        break;
                    }
            }
            return null;
        }

        private static ISpatialAnalysis CreatePolygon(ISpatialFilter filter, IPolygon polygon)
        {
            switch (filter.SpatialRelationship)
            {
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_DISJOINT:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_TOUCHES:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_CROSSES:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_WITHINS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_OVERLAPS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_CONTAINS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_INTERSECTS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_EQUALS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_ENVELOPEINTERSECTS:
                    {
                        break;
                    }
            }
            return null;
        }

        private static ISpatialAnalysis CreateMultiPoint(ISpatialFilter filter, IMultiPoint multiPoint)
        {
            switch (filter.SpatialRelationship)
            {
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_DISJOINT:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_TOUCHES:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_CROSSES:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_WITHINS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_OVERLAPS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_CONTAINS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_INTERSECTS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_EQUALS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_ENVELOPEINTERSECTS:
                    {
                        break;
                    }
            }
            return null;
        }

        private static ISpatialAnalysis CreateMultiLineString(ISpatialFilter filter, IMultiLineString multiLineString)
        {
            switch (filter.SpatialRelationship)
            {
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_DISJOINT:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_TOUCHES:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_CROSSES:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_WITHINS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_OVERLAPS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_CONTAINS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_INTERSECTS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_EQUALS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_ENVELOPEINTERSECTS:
                    {
                        break;
                    }
            }
            return null;
        }

        private static ISpatialAnalysis CreateMultiPolygon(ISpatialFilter filter, IMultiPolygon multiPolygon)
        {
            switch (filter.SpatialRelationship)
            {
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_DISJOINT:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_TOUCHES:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_CROSSES:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_WITHINS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_OVERLAPS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_CONTAINS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_INTERSECTS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_EQUALS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_ENVELOPEINTERSECTS:
                    {
                        break;
                    }
            }
            return null;
        }

        private static ISpatialAnalysis CreatePath(ISpatialFilter filter, IPath path)
        {
            switch (filter.SpatialRelationship)
            {
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_DISJOINT:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_TOUCHES:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_CROSSES:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_WITHINS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_OVERLAPS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_CONTAINS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_INTERSECTS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_EQUALS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_ENVELOPEINTERSECTS:
                    {
                        break;
                    }
            }
            return null;
        }

        private static ISpatialAnalysis CreateRect(ISpatialFilter filter, IEnvelope rect)
        {
            switch (filter.SpatialRelationship)
            {
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_DISJOINT:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_TOUCHES:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_CROSSES:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_WITHINS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_OVERLAPS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_CONTAINS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_INTERSECTS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_ENVELOPEINTERSECTS:
                    {
                        return new RectIntersectsSA(rect);
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_EQUALS:
                    {
                        break;
                    }
                default:
                    {
                        throw new NotSupportedException("不支持的空间关系");
                    }
            }
            return null;
        }

        private static ISpatialAnalysis CreateCircle(ISpatialFilter filter, ICircle circle)
        {
            switch (filter.SpatialRelationship)
            {
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_DISJOINT:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_TOUCHES:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_CROSSES:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_WITHINS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_OVERLAPS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_CONTAINS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_INTERSECTS:
                    {
                        return new CircleIntersectsSA(circle);
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_ENVELOPEINTERSECTS:
                    {
                        break;
                    }
                case geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_EQUALS:
                    {
                        break;
                    }
                default:
                    {
                        throw new NotSupportedException("不支持的空间关系");
                    }
            }
            return null;
        }

        #endregion
    }
}
