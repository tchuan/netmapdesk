﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Application.Gui.Pads;
using NetMap.UI.Tools;
using NetMap.Interfaces;
using NetMap.Application;

namespace StoneGround.Edit.Commands
{
    /// <summary>
    /// 道路属性编辑
    /// </summary>
    public class RoadAttributeEditCommand : MapCommand
    {
        public RoadAttributeEditCommand()
        {
            InnerCmd = new AttributeEdit(StoneGroundDefines.LAYER_NAME_ROAD);
        }
    }
}
