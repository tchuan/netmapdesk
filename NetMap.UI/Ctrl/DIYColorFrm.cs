﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetMap.UI.Ctrl
{
    public partial class DIYColorFrm : System.Windows.Forms.Form
    {
        private int r = 0;
        private int g = 0;
        private int b = 0;
        private Color currentColor = Color.Empty;
        private Color defauntColor;
        public delegate void CurrentColorHandle(Color color);
        public event CurrentColorHandle SelectedColorEnvnt;
        protected void OnSelectedColorEnvetn(Color color)
        {
            if (SelectedColorEnvnt != null)
            {
                SelectedColorEnvnt(color);
            }
        }
        public DIYColorFrm()
        {
            InitializeComponent();
        }
        public Color DefaultColor 
        { 
            set 
            {
                if (value != Color.Empty)
                {
                    defauntColor = value;
                    RtextBox.Text = value.R.ToString();
                    GtextBox.Text = value.G.ToString();
                    BtextBox.Text = value.B.ToString();
                    RtrackBar.Value = value.R;
                    GtrackBar.Value = value.G;
                    BtrackBar.Value = value.B;
                    r = RtrackBar.Value;
                    g = GtrackBar.Value;
                    b = BtrackBar.Value;
                    currentColor = value;
                    ColorBT.BackColor = currentColor;
                }
                else
                {
                    defauntColor = Color.FromArgb(0,0,0);
                    RtextBox.Text = "0";
                    GtextBox.Text = "0";
                    BtextBox.Text = "0";
                    RtrackBar.Value = 0;
                    GtrackBar.Value = 0;
                    BtrackBar.Value = 0;
                    r = 0; g = 0; b = 0;
                    currentColor = Color.FromArgb(0, 0, 0);
                    ColorBT.BackColor = currentColor;
                }
            } 
        }

        private void RtextBox_TextChanged(object sender, EventArgs e)
        {
            if (int.TryParse(RtextBox.Text, out r))
            {
                if (r >= 0 && r <= 255)
                {
                    RtrackBar.Value = r;
                    currentColor = Color.FromArgb(r,g,b);
                    ColorBT.BackColor = currentColor;
                }
            }
        }

        private void GtextBox_TextChanged(object sender, EventArgs e)
        {
            if (int.TryParse(GtextBox.Text, out g))
            {
                if (g >= 0 && g <= 255)
                {
                    GtrackBar.Value = g;
                    currentColor = Color.FromArgb(r, g, b);
                    ColorBT.BackColor = currentColor;
                }
            }
        }

        private void BtextBox_TextChanged(object sender, EventArgs e)
        {
            if (int.TryParse(BtextBox.Text, out b))
            {
                if (b >= 0 && b <= 255)
                {
                    BtrackBar.Value = b;
                    currentColor = Color.FromArgb(r, g, b);
                    ColorBT.BackColor = currentColor;
                }
            }
        }

        private void OKBT_Click(object sender, EventArgs e)
        {
            currentColor = Color.FromArgb(r,g,b);
            ColorBT.BackColor = currentColor;
            OnSelectedColorEnvetn(currentColor);
            this.Close();
        }

        private void RtrackBar_ValueChanged(object sender, EventArgs e)
        {
            r = RtrackBar.Value;
            RtextBox.Text = r.ToString();
            currentColor = Color.FromArgb(r, g, b);
            ColorBT.BackColor = currentColor;
        }

        private void GtrackBar_ValueChanged(object sender, EventArgs e)
        {
            g = GtrackBar.Value;
            GtextBox.Text = g.ToString();
            currentColor = Color.FromArgb(r, g, b);
            ColorBT.BackColor = currentColor;
        }

        private void BtrackBar_ValueChanged(object sender, EventArgs e)
        {
            b = BtrackBar.Value;
            BtextBox.Text = b.ToString();
            currentColor = Color.FromArgb(r, g, b);
            ColorBT.BackColor = currentColor;
        }

        private void CancelBT_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
