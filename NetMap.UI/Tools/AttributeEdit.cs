﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces;
using NetMap.Tools.Select;
using System.Windows.Forms;
using NetMap.Interfaces.Data;
using NetMap.Data;

namespace NetMap.UI.Tools
{
    /// <summary>
    /// 属性编辑工具
    /// 说明：先选择
    ///      选择后，弹出属性编辑窗口
    /// </summary>
    public class AttributeEdit : Tool
    {
        private CircleSelect _circleSelect;
        private IFeatureLayer _targetLayer;
        private string _targetLayerName;

        public AttributeEdit(string targetLayerName)
            : base("属性编辑")
        {
            _targetLayerName = targetLayerName;
        }

        public AttributeEdit(IMapEditView view, string targetLayerName)
            : base("属性编辑", view)
        {
            _targetLayerName = targetLayerName;
        }

        public override void Init()
        {            
            base.Init();

            IMapDocument map = EditView.MapDocument;
            ILayer layer = map.FindLayer(_targetLayerName);
            if (layer is IFeatureLayer)
            {
                _targetLayer = layer as IFeatureLayer;
            }
            else
            {
                throw new NotSupportedException("属性编辑工具不支持的图层类型");
            }

            _circleSelect = new CircleSelect(EditView);
            _circleSelect.Init();
            _circleSelect.TargetLayer = _targetLayer;
        }

        public override void GainFocus()
        {
            base.GainFocus();
            _circleSelect.GainFocus();
        }

        public override void LostFocus()
        {
            //base.LostFocus();
            _circleSelect.LostFocus();
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            //base.OnMouseDown(e);
            _circleSelect.OnMouseDown(e);
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            //base.OnMouseMove(e);
            _circleSelect.OnMouseMove(e);
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            //base.OnMouseUp(e);
            _circleSelect.OnMouseUp(e);
            if (_targetLayer is IFeatureSelection)
            {
                IFeatureSelection featureSelection = _targetLayer as IFeatureSelection;
                ISelectionSet selectionSet = featureSelection.SelectionSet;
                if (selectionSet.Count > 0)
                {
                    IQueryFilter filter = new QueryFilter();
                    IFields fields = selectionSet.GetTarget().Fields;
                    for (int j = 0; j < fields.Count; j++)
                    {
                        IField field = fields.GetField(j);
                        filter.Fields.Add(field.Name);
                    }
                    IFeatureCursor cursor = selectionSet.Search(filter);
                    IFeature feature = cursor.Next();
                    cursor.Close();
                    if (feature != null)
                    {
                        //AttributeForm att = new AttributeForm();
                        //att.Init(feature);
                        //att.ShowDialog();
                        AttributeEditForm attEdit = new AttributeEditForm();
                        attEdit.Init(feature);
                        attEdit.Show();
                    }
                }
            }
        }

        public override void Draw(System.Drawing.Graphics g)
        {
            //base.Draw(g);
            _circleSelect.Draw(g);
        }
    }
}
