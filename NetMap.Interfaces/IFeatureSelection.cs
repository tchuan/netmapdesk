﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Data;
using NetMap.Interfaces.Symbol;
using NetMap.Interfaces.Render;

namespace NetMap.Interfaces
{
    /// <summary>
    /// 选择集逻辑运算类型
    /// </summary>
    public enum geoSelectionResultEnum
    {
        /// <summary>
        /// 替换原选择集
        /// </summary>
        GEO_SELECTIONRESULT_NEW = 1,
        /// <summary>
        /// 在原有结果集上增加（并集）
        /// </summary>
        GEO_SELECTIONRESULT_ADD = 2,
        /// <summary>
        /// 在原有结果集上减少（差集）
        /// </summary>
        GEO_SELECTIONRESULT_SUBTRACT = 3,
        /// <summary>
        /// 取与原有结果集的交集
        /// </summary>
        GEO_SELECTIONRESULT_AND =4,
        /// <summary>
        /// 取与原有结果集的异或集（两者不相同对象的集合）
        /// </summary>
        GEO_SELECTIONRESULT_XOR = 5
    }

    /// <summary>
    /// 对要素图层的选择结果管理接口
    /// </summary>
    public interface IFeatureSelection
    {
        /// <summary>
        /// 获取和设置选择集符号
        /// </summary>
        ISymbol SelectionSymbol { get; set; }
        /// <summary>
        /// 获取当前选择集
        /// </summary>
        ISelectionSet SelectionSet { get; }
        /// <summary>
        /// 选择
        /// </summary>
        /// <param name="filter">查询条件</param>
        /// <param name="method">结果集的逻辑运算类型</param>
        void Select(IQueryFilter filter, geoSelectionResultEnum method);
        /// <summary>
        /// 清空当前选择集
        /// </summary>
        void Clear();
        /// <summary>
        /// 绘制选择集
        /// </summary>
        /// <param name="display"></param>
        void DrawSelection(IDisplay display);
    }
}
