﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Geometry
{
    /// <summary>
    /// Path接口
    /// </summary>
    public interface IPath : IGeometry, IList<IPathPoint>
    {
        /// <summary>
        /// 获取起点
        /// </summary>
        IPathPoint StartPoint { get; }
        /// <summary>
        /// 获取终点
        /// </summary>
        IPathPoint EndPoint { get; }
        /// <summary>
        /// 获取和设置是否封闭
        /// </summary>
        bool IsClose { get; set; }
    }
}
