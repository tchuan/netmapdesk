﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NetMap.Core;
using NetMap.Interfaces.Geometry;

namespace NetMap.Geometry
{
    public class DirectionPoint : Point, IDirectionPoint
    {
        private double _angle = 0.0;

        public DirectionPoint()
            : base()
        {
        }

        public DirectionPoint(double x, double y, double angle)
            : base(x, y)
        {
            _angle = angle;
        }

        #region IDirectionPoint 成员

        public double Angle
        {
            get
            {
                return _angle;
            }
            set
            {
                _angle = value;
            }
        }

        #endregion

        public override geoGeometryType GeometryType
        {
            get
            {
                return geoGeometryType.GEO_GEOMETRY_DIRECTIONPOINT;
            }
        }
        
        public override IGeometry CloneGeometry()
        {
            IPoint pt=base.CloneGeometry() as IPoint;
            return new DirectionPoint(pt.X, pt.Y, _angle);
        }

        public override byte[] ExportToWkb()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryWriter writer = new BinaryWriter(stream);
                writer.Write(X);
                writer.Write(Y);
                writer.Write(Angle);
                return stream.ToArray();
            }
        }

        public override string ExportToWkt()
        {
            throw new NotImplementedException();
        }

        public override void ImportFromWkb(byte[] data)
        {
            using (MemoryStream stream = new MemoryStream(data))
            {
                BinaryReader reader = new BinaryReader(stream);
                X = reader.ReadDouble();
                Y = reader.ReadDouble();
                Angle = reader.ReadDouble();
                reader.Close();
            }
        }

        public override void ImportFromWkt(string data)
        {
            throw new NotImplementedException();
        }

        // x y angle
        public override void Export(BinaryWriter writer)
        {
            writer.Write(X);
            writer.Write(Y);
            writer.Write(Angle);
        }

        public override void Import(BinaryReader reader)
        {
            X = reader.ReadDouble();
            Y = reader.ReadDouble();
            Angle = reader.ReadDouble();
        }
    }
}
