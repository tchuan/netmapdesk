﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NetMap.Core;
using NetMap.Interfaces.Geometry;

namespace NetMap.Geometry
{
    /// <summary>
    /// 列表第一个图形默认为外圈
    /// </summary>
    public class Polygon : Geometry, IPolygon
    {
        private List<ILineString> _rings = null;

        public Polygon()
        {
            _rings = new List<ILineString>();
        }

        public Polygon(ILineString ring)
        {
            _rings = new List<ILineString>();
            _rings.Add(ring);
        }

        public Polygon(List<ILineString> lines, IEnvelope box)
        {
            _rings = lines;
            _box = box;
        }

        public override geoGeometryType GeometryType
        {
            get { return geoGeometryType.GEO_GEOMETRY_POLYGON; }
        }

        public override void UpdateEnvelope()
        {
            if (_rings.Count == 0)
            {
                _box = null;
                return;
            }
            foreach (ILineString ring in _rings)
            {
                ring.UpdateEnvelope();
            }
            if (_box == null)
            {
                _box = new Envelope(_rings[0].Envelope);
            }
            else
            {
                _box.Update(_rings[0].Envelope);
            }
            foreach (ILineString ring in _rings)
            {
                _box.Update(ring.Envelope);
            }
        }

        #region IPolygon 成员

        public ILineString ExteriorRing
        {
            get
            {
                if (_rings.Count > 0)
                {
                    return _rings[0];
                }
                else
                {
                    return null;
                }
            }
        }

        public List<ILineString> InteriorRings 
        { 
            get
            {
                if (_rings.Count > 1)
                {
                    return _rings.GetRange(1, _rings.Count - 1);
                }
                else
                {
                    return new List<ILineString>(); // 返回空集合
                }
            } 
        }

        public ILineString GetInteriorRingN(int n)
        {
            if (n < 0 || n >= _rings.Count - 1)
            {
                return null;
            }
            return _rings[n + 1];
        }

        #endregion

        public override IGeometry CloneGeometry()
        {
            IPolygon geometry = new Polygon(_rings,this.Envelope);
            return geometry;
        }

        // 总数 点数 x y x y ... 点数 x y x y ...
        public override byte[] ExportToWkb()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    writer.Write(_rings.Count);
                    foreach (ILineString ring in _rings)
                    {
                        writer.Write(ring.Count);
                        foreach (IPoint point in ring)
                        {
                            writer.Write(point.X);
                            writer.Write(point.Y);
                        }
                    }
                }
                return stream.ToArray();
            }
        }

        public override string ExportToWkt()
        {
            throw new NotImplementedException();
        }

        public override void ImportFromWkb(byte[] data)
        {
            using (MemoryStream stream = new MemoryStream(data))
            {
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    int count = reader.ReadInt32();
                    for (int i = 0; i < count; i++)
                    {
                        int ptCount = reader.ReadInt32();
                        ILineString ring = new LineString();
                        for (int j = 0; j < ptCount; j++)
                        {
                            IPoint point = new Point();
                            point.X = reader.ReadDouble();
                            point.Y = reader.ReadDouble();
                            ring.Add(point);
                        }
                        _rings.Add(ring);
                    }
                }
            }
        }

        public override void ImportFromWkt(string data)
        {
            throw new NotImplementedException();
        }

        public override void Export(BinaryWriter writer)
        {
            writer.Write(_rings.Count);
            foreach (ILineString ring in _rings)
            {
                ring.Export(writer);
            }
        }

        //认为多边形内部全部为LineString
        public override void Import(BinaryReader reader)
        {
            int count = reader.ReadInt32();
            for (int i = 0; i < count; i++)
            {
                ILineString ring = new LineString();
                ring.Import(reader);
            }
        }
    }
}
