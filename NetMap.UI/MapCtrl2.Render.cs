﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using NetMap.Render;
using NetMap.Interfaces;
using NetMap.Interfaces.Fetcher;
using NetMap.Interfaces.Render;
using NetMap.Geometry;
using System.Drawing.Drawing2D;
using System.Diagnostics;
using System.Windows.Forms;
using NetMap.UI.Properties;

namespace NetMap.UI
{
    partial class MapCtrl2
    {
        #region Fields

        //[双缓存]
        private Bitmap _bufferBitmap;
        private IDisplay _display = null;
        private IRenderCancel _renderCancel = null;
        private IDisplayTransformation _displayTF = new DisplayTransformation();
        private Bitmap _effectBitmap;
        private Graphics _effectGraphics;
        private bool _isInvalidated;
        private bool _viewInitialized;
        private long _startTicks;
        private bool _isManipulated;             //是否是操作
        private bool _isNeedUpdateBuffer = true; //是否需要更新buffer图片
        //Ticks are in units of 100 nanoseconds. I prefer to use milliseconds myself
        //1 tick = 100 nanoseconds (from documentation)
        //1 millisecond = 1000000 nanoseconds
        //1 millisecond = (1000000 / 100) = 10000 ticks.
        private const int MaxMiliseconds = 40;
        private const long MaxTicks = (long)MaxMiliseconds * 10000;
        private bool _isCallingDoEvents;
        //logo绘制
        private Font _logoFont = new Font("Arial", 8, FontStyle.Italic);
        private Brush _logoBrush = new SolidBrush(Color.Purple);

        #endregion

        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            if (_isCallingDoEvents) return;
            if (!_viewInitialized) InitializeView();
            if (!_viewInitialized) return; //上面的初始化失败了

            _isManipulated = false;
            _isInvalidated = false;

            //set startTicks for use in AbortRender
            _startTicks = DateTime.Now.Ticks;
            base.OnPaint(e);            
            if (_isNeedUpdateBuffer)
            {
                _isNeedUpdateBuffer = false;
                //重置背景
                _display.G.Clear(BackColor);                
                //绘制地图
                DrawMap();
                //重置效果
                _effectGraphics.Clear(Color.Transparent);
                //工具效果
                if (_currentTool != null)
                {
                    _currentTool.Draw(_effectGraphics);
                }
                //画地图上的效果
                _display.G.DrawImage(_effectBitmap, 0, 0);
                //画Logo
                _display.G.DrawString("NetMap 2.0", _logoFont, _logoBrush, 10, Height - 30);                
            }
            //Render the buffer to the control
            e.Graphics.DrawImage(_bufferBitmap, 0, 0);

            if (_isInvalidated)
            {
                Invalidate();
            }
        }

        protected override void OnPaintBackground(System.Windows.Forms.PaintEventArgs e)
        {
            //by overriding this method and not calling the base class implementation
            //we prevent flickering.
        }

        #region Privates

        //绘制地图
        private void DrawMap()
        {
            OnBeforeDraw(_display);
#if DEBUG
            Stopwatch watch = new Stopwatch();
            Debug.Print("开始地图渲染");
            watch.Start();
#endif

            //绘制图层
            ILayer layer = null;
            for (int i = 0; i < _map.LayerCount; i++)
            {
                layer = _map.GetLayer(i);
                if (layer.Visible
                    && _displayTF.ScaleRatio >= layer.MinScale
                    && _displayTF.ScaleRatio <= layer.MaxScale)
                {
                    layer.Draw(_display);
                }
                if (_renderCancel.Cancel()) //判断是否退出绘图
                {
                    break;
                }
            }
            //绘制选择集
            for (int i = 0; i < _map.LayerCount; i++)
            {
                layer = _map.GetLayer(i);
                if (layer.Visible
                    && _displayTF.ScaleRatio >= layer.MinScale
                    && _displayTF.ScaleRatio <= layer.MaxScale
                    && (layer is IFeatureSelection))
                {
                    IFeatureSelection featureSelection = layer as IFeatureSelection;
                    featureSelection.DrawSelection(_display);
                }
                if (_renderCancel.Cancel()) //判断是否退出绘图
                {
                    break;
                }
            }

#if DEBUG
            watch.Stop();
            Debug.Print("地图渲染结束，耗时(毫秒)：" + watch.ElapsedMilliseconds);
#endif
            OnAfterDraw(_display);
        }        

        //绘制效果
        private void DrawEffect()
        {
        }

        //更新渲染环境
        private void UpdateRenderContext()
        {
            _displayTF.WindowBounds = new Envelope(0, 0, Width, Height);
            if (_bufferBitmap == null ||
                _bufferBitmap.Width != Width || _bufferBitmap.Height != Height)
            {
                _bufferBitmap = new Bitmap(Width, Height);
                ISymbolRender symbolRender = new SymbolRender();
                _display = new Display(Graphics.FromImage(_bufferBitmap), symbolRender, null, _displayTF);
                symbolRender.Setup(_display);
                _display.G.SmoothingMode = SmoothingMode.AntiAlias;
            }
            if (_effectBitmap == null ||
                _effectBitmap.Width != Width || _effectBitmap.Height != Height)
            {
                _effectBitmap = new Bitmap(Width, Height);
                _effectGraphics = Graphics.FromImage(_effectBitmap);
            }
        }

        private void InvalidateMap(bool isManipulated)
        {
            _isManipulated = isManipulated;
            _isNeedUpdateBuffer = true;
            _isInvalidated = true;
            Invalidate();
        }

        private void InitializeView()
        {
            if (double.IsNaN(Width) || Width == 0)
            {
                return;
            }
            if (_map == null || _map.Extent == null
            || double.IsNaN(_map.Extent.Width) || _map.Extent.Width <= 0)
            {
                return;
            }
            
            _displayTF.ZoomToMapBounds();
            _viewInitialized = true;
            ViewChanged(true);
        }

        private void ViewChanged(bool changeEnd)
        {
            if (!_viewInitialized)
            {
                return;
            }
            if (_map != null)
            {
                IDisplayTransformation displayTF = new DisplayTransformation(_displayTF);
                _map.ViewChanged(changeEnd, displayTF);
            }
        }

        private bool AbortRender()
        {
            // When calling DoEvents we want all events to be called except
            // OnPaint. This is prevented by checking on isCallingDoEvents.
            _isCallingDoEvents = true;
            Application.DoEvents();
            _isCallingDoEvents = false;
            if (_isManipulated && (DateTime.Now.Ticks - _startTicks) > MaxTicks)
            {
                return true;
            }
            return false;
        }

        private void RefreshEffect()
        {
            using (Graphics g = CreateGraphics())
            {
                //绘制底图
                g.DrawImage(_bufferBitmap, 0, 0);
                //重置效果
                _effectGraphics.Clear(Color.Transparent);
                //工具效果
                if (_currentTool != null)
                {
                    _currentTool.Draw(_effectGraphics);
                }
                //画地图上的效果
                g.DrawImage(_effectBitmap, 0, 0);
                //画Logo
                g.DrawString("NetMap 2.0", _logoFont, _logoBrush, 10, Height - 30);
            }
        }

        #endregion
    }
}
