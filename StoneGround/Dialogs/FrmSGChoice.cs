﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace StoneGround.Dialogs
{
    /// <summary>
    /// 料场选择对话框
    /// </summary>
    public partial class FrmSGChoice : Form
    {
        private List<string> _selectedSgLst = new List<string>();

        public FrmSGChoice()
        {
            InitializeComponent();

            button1.Enabled = false;
            listBox1.SelectedIndexChanged += listBox1_SelectedIndexChanged;
        }

        void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedItems.Count > 0)
            {
                button1.Enabled = true;
            }
            else
            {
                button1.Enabled = false;
            }
        }

        /// <summary>
        /// 设置源料场集合
        /// </summary>
        /// <param name="sgLst"></param>
        public void SetSGCollection(List<string> sgLst)
        {
            listBox1.Items.Clear();
            foreach (string item in sgLst)
            {
                listBox1.Items.Add(item);
            }
        }

        /// <summary>
        /// 获取选择目标
        /// </summary>
        public List<string> SelectedSGCollection => _selectedSgLst;

        private void button1_Click(object sender, EventArgs e)
        {
            //获取选择目标
            _selectedSgLst.Clear();
            foreach (object item in listBox1.SelectedItems)
            {
                _selectedSgLst.Add(item.ToString());
            }
            //设置对话框结果
            this.DialogResult = DialogResult.OK;
            this.Close();            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            for (var i = 0; i < listBox1.Items.Count; i++)
            {
                listBox1.SetSelected(i, checkBox1.Checked);
            }
        }
    }
}
