﻿using NetMap.Data;
using NetMap.Interfaces;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Tile;
using NetMap.Tile;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace NetMap
{
    /// <summary>
    /// 静态瓦片图层
    /// </summary>
    public class StaticTileLayer : Layer, ITileLayer
    {
        private ITileSource _tileSource;
        private int _maxRetries = 1;
        private bool _repairEmpty = false;

        public StaticTileLayer()
        {
        }

        public StaticTileLayer(ITileSource source)
        {
            _tileSource = source;
        }

        #region ITileLayer 成员

        public int MaxRetries
        {
            set { _maxRetries = value; }
        }

        public bool RepairEmpty
        {
            get { return _repairEmpty; }
            set { _repairEmpty = value; }
        }

        public ITileSource TileSource
        {
            get { return _tileSource; }
        }

        #endregion

        #region ILayer 成员

        public override void Draw(IDisplay display)
        {
            if (CoordinateTransformation != null)
            {
                //不支持影像的投影变换
                return;
            }
            ISymbolRender render = display.SymbolRender;
            render.Setup(display);
            IDisplayTransformation displayTransformation = display.DisplayTransformation;
            ITileSchema schema = _tileSource.Schema;
            ITileProvider provider = _tileSource.Provider;
            string level = Utilities.ZoomHelper.GetNearestLevel(schema.Resolutions, 1.0 / displayTransformation.ScaleRatio);
            IEnumerable<TileInfo> tiles = schema.GetTilesInView(displayTransformation.VisibleMapBounds, level);
            foreach (TileInfo tileInfo in tiles)
            {
                byte[] data = provider.GetTile(tileInfo);
                if (data != null)
                {
                    using(Image image = Image.FromStream(new MemoryStream(data)))
                    {
                        render.Draw(tileInfo.Extent, image);
                    }
                }
            }
        }

        public override ProjNet.CoordinateSystems.ICoordinateSystem CoordinateSystem
        {
            get
            {
                //TODO 支持坐标系统
                return null;
            }
        }

        public override IEnvelope Extent
        {
            get { return _tileSource.Schema.Extent; }
        }

        #endregion
    }
}
