﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;
using System.Drawing;

namespace NetMap.Interfaces.Symbol
{
    /// <summary>
    /// 圆弧点符号接口
    /// </summary>
    public interface IArcPointSymbol : IPointSymbol
    {
        /// <summary>
        /// 获取和设置线宽
        /// </summary>
        float Width { get; set; }
        /// <summary>
        /// 获取和设置是否有边线
        /// </summary>
        bool Outline { get; set; }
        /// <summary>
        /// 获取和设置是否填充
        /// </summary>
        bool Fill { get; set; }
        /// <summary>
        /// 获取和设置填充色
        /// </summary>
        Color FillColor { get; set; }
        /// <summary>
        /// 获取和设置椭圆范围
        /// </summary>
        IEnvelope Envelope { get; set; }
        /// <summary>
        /// 获取和设置从 x 轴到弧线的起始点沿顺时针方向度量的角（以度为单位）
        /// </summary>
        float StartAngle { get; set; }
        /// <summary>
        /// 获取和设置从 StartAngle 参数到弧线的结束点沿顺时针方向度量的角（以度为单位）
        /// </summary>
        float SweepAngle { get; set; }
    }
}
