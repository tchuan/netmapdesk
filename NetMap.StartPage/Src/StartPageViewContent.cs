﻿// Copyright (c) AlphaSierraPapa for the KJSuite Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

using System;
using NetMap.Application.Gui;

namespace NetMap.StartPage
{
    public class StartPageViewContent : AbstractPadContent
	{
		private StartPageControl content = new StartPageControl();
		
		public override object Control {
			get {
				return content;
			}
		}
		
		public StartPageViewContent()
		{
		}
	}
}
