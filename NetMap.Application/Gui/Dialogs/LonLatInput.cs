﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NetMap.Application.Core;

namespace NetMap.Application.Gui.Dialogs
{
    /// <summary>
    /// 经纬度输入窗口
    /// </summary>
    public partial class LonLatInput : Form
    {
        public LonLatInput()
        {
            InitializeComponent();
            Longitude = 0;
            Latitude = 0;
        }

        /// <summary>
        /// 获取经度
        /// </summary>
        public double Longitude
        {
            get;
            private set;
        }

        /// <summary>
        /// 获取纬度
        /// </summary>
        public double Latitude
        {
            get;
            private set;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            double value = 0;
            if (double.TryParse(textBoxLongitude.Text, out value))
            {
                Longitude = value;
            }
            else
            {
                MessageService.ShowMessage("请输入有效的经纬度");
                return;
            }
            if (double.TryParse(textBoxLatitude.Text, out value))
            {
                Latitude = value;
            }
            else
            {
                MessageService.ShowMessage("请输入有效的经纬度");
                return;
            }
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }
    }
}
