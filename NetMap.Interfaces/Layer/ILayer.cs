﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Geometry;
using ProjNet.CoordinateSystems;
using ProjNet.CoordinateSystems.Transformations;

namespace NetMap.Interfaces
{
    /// <summary>
    /// 图层接口
    /// </summary>
    public interface ILayer
    {
        /// <summary>
        /// 层名
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// 获取或设置是否可见
        /// </summary>
        bool Visible { get; set; }
        /// <summary>
        /// 获取或设置最大显示比例
        /// </summary>
        double MaxScale { get; set; }
        /// <summary>
        /// 获取和设置最小显示比例
        /// </summary>
        double MinScale { get; set; }
        /// <summary>
        /// 图层的标签
        /// 备注：
        ///     原因：
        ///     1.使用标签比使用名称判断效率高
        ///     2.图层名可重复，标签不重复
        /// </summary>
        int Tag { get; set; }
        /// <summary>
        /// 绘制
        /// </summary>
        /// <param name="display">渲染环境</param>
        void Draw(IDisplay display);
        /// <summary>
        /// 图层范围
        /// </summary>
        IEnvelope Extent { get; }
        /// <summary>
        /// 获取坐标参考系统
        /// </summary>
        ICoordinateSystem CoordinateSystem { get; }
        /// <summary>
        /// 获取和设置坐标转换
        /// </summary>
        ICoordinateTransformation CoordinateTransformation { get; set; }
        /// <summary>
        /// 获取和设置坐标反转
        /// </summary>
        ICoordinateTransformation ReverseCoordinateTransformation { get; set; }
    }
}
