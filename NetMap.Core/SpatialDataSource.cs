﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces;
using NetMap.Interfaces.Data;
using NetMap.Interfaces.Persist;
using System.Xml;
using NetMap.Data;

namespace NetMap
{
    public class SpatialDataSource : ISpatialDataSource, IPersistResource
    {
        private List<ISpatialDatabase> _sdbLst = new List<ISpatialDatabase>();

        public void Add(ISpatialDatabase db)
        {
            if (_sdbLst.IndexOf(db) < 0)
            {
                _sdbLst.Add(db);
            }
        }

        public ISpatialDatabase Find(string name)
        {
            foreach (ISpatialDatabase sdb in _sdbLst)
            {
                if (sdb.Name == name)
                {
                    return sdb;
                }
            }
            return null;
        }

        public void Remove(ISpatialDatabase db)
        {
            _sdbLst.Remove(db);
        }

        public void Clear()
        {
            _sdbLst.Clear();
        }

        public IEnumerator<ISpatialDatabase> GetEnumerator()
        {
            return _sdbLst.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _sdbLst.GetEnumerator();
        }

        #region IPersistResource 接口

        public void Save(XmlElement node, XmlDocument document)
        {
            foreach (ISpatialDatabase db in _sdbLst)
            {
                XmlElement element = document.CreateElement(db.FactoryName);
                element.SetAttribute("Name", db.Name);
                XmlElement child = document.CreateElement("ConnectProperties");
                child.SetAttribute("Database", db.ConnectProperties.Database);
                child.SetAttribute("Server", db.ConnectProperties.Server);
                child.SetAttribute("User", db.ConnectProperties.User);
                child.SetAttribute("Password", db.ConnectProperties.Password);
                element.AppendChild(child);
                node.AppendChild(element);
            }
        }

        public void Load(XmlReader reader, object context)
        {
            while (reader.Read())
            {
                if (reader.IsStartElement())
                {
                    ISpatialDatabaseFactory sdbFactory = DatabaseFactory.Instance.FindSpatialFactory(reader.Name);
                    if (sdbFactory == null)
                    {
                        throw new NotSupportedException("不支持的空间数据库");
                    }
                    string name = reader.GetAttribute("Name");
                    IConnectProperties connectProperties = new ConnectProperties();
                    while (reader.Read())
                    {
                        if (reader.IsStartElement() && reader.Name == "ConnectProperties")
                        {
                            connectProperties.Database = reader.GetAttribute("Database");
                            connectProperties.Server = reader.GetAttribute("Server");
                            connectProperties.User = reader.GetAttribute("User");
                            connectProperties.Password = reader.GetAttribute("Password");
                            break;
                        }
                    }
                    break;
                }
            }
        }

        #endregion
    }
}
