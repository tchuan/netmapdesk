﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NetMap.Data;
using NetMap.Interfaces.Data;

namespace NetMap.UI
{
    /// <summary>
    /// 数据连接窗口
    /// </summary>
    public partial class FrmConnectDatabase : Form
    {
        public FrmConnectDatabase()
        {
            InitializeComponent();
        }

        private IConnectProperties _properties = new ConnectProperties();
        /// <summary>
        /// 获取空间数据库连接
        /// </summary>
        public IConnectProperties ConnectionProperties
        {
            get
            {
                return _properties;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            _properties.Database = textBoxDatabase.Text;
            _properties.Server = textBoxDatabase.Text;
            _properties.User = textBoxDatabase.Text;
            _properties.Password = textBoxDatabase.Text;
        }
    }
}
