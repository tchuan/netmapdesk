﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;

namespace NetMap.Interfaces.Tracker
{
    /// <summary>
    /// 点橡皮条接口
    /// </summary>
    public interface IPointTracker
    {
        /// <summary>
        /// 设置点的点位
        /// </summary>
        /// <param name="point"></param>
        void SetPoint(IPoint point);
    }
}
