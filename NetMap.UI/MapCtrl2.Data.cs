﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Fetcher;
using NetMap.Interfaces;

namespace NetMap.UI
{
    partial class MapCtrl2
    {
        #region Fields

        private IMapDocument _map = null;

        #endregion

        private void MapDataChange(DataChangedEventArgs e)
        {
            if (IsHandleCreated)
            {
                //ViewChanged should not be called here, This would cause a loop
                BeginInvoke((Action)(() => DataChanged(e)));
            }
        }

        private void DataChanged(DataChangedEventArgs e)
        {
            if (e.Canceled == false)
            {
                InvalidateMap(false);
            }
            //TODO 处理错误
        }
    }
}
