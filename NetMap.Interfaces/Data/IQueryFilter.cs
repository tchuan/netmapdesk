﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Data
{
    /// <summary>
    /// 查询接口
    /// </summary>
    public interface IQueryFilter
    {
        /// <summary>
        /// 获取目标字段集合
        /// </summary>
        List<string> Fields { get; }
        /// <summary>
        /// 查询条件
        /// </summary>
        string WhereClause { get; set; }
    }
}
