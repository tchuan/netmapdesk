﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Symbol;

namespace NetMap.Interfaces.Render
{
    /// <summary>
    /// 质底法图层方案接口
    /// </summary>
    public interface IUniqueValueRender : ILayerRender
    {
        /// <summary>
        /// 参考属性字段
        /// </summary>
        string Field { get; set; }
        /// <summary>
        /// 缺省符号
        /// </summary>
        ISymbol DefaultSymbol { get; set; }
        /// <summary>
        /// 是否表示缺省符号
        /// </summary>
        bool IsUseDefaultSymbol { get; set; }
        /// <summary>
        /// 获取唯一值集合
        /// </summary>
        List<string> UniqueValues { get; }
        /// <summary>
        /// 设置符号
        /// </summary>
        /// <param name="value"></param>
        /// <param name="symbol"></param>
        void SetSymbol(string value, ISymbol symbol);
        /// <summary>
        /// 获取符号
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        ISymbol GetSymbol(string value);
        /// <summary>
        /// 替换对应value的symbol
        /// </summary>
        /// <param name="value"></param>
        /// <param name="symbol"></param>
        void ChangeSymbol(string value, ISymbol symbol);
        /// <summary>
        /// 移除符号
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        bool Remove(string value);
    }
}
