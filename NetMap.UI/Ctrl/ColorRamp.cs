﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetMap.UI.Ctrl
{
    public partial class ColorRamp : UserControl
    {
        private List<int> _arrColor=null;//存储颜色的数组
        private int _count;//产生的颜色数目
        private int _startColor,_endColor;//起止颜色
        
        public ColorRamp()
        {
            InitializeComponent(); 
        }
        public void Set(Color startColor, Color endColor, int count)
        {
            _startColor = RGBToInteger(startColor);
            _endColor = RGBToInteger(endColor);
            _count = count;
            if (_count == 1) _arrColor = new List<int>() { _startColor };
            else if (_count == 2) _arrColor = new List<int>() { _startColor, _endColor };
            else if (_count > 2)
            {
                _arrColor = new List<int>();
                _arrColor.Add(_startColor);
                float dis = (float)Math.Abs(_endColor-_startColor) / (_count - 1);
                float temdis = dis;
                for (int i = 0; i < _count - 2; i++)
                {
                    if (_startColor < _endColor)
                        _arrColor.Add((int)Math.Floor(_startColor + temdis + 0.5));
                    else _arrColor.Add((int)Math.Floor(_startColor - temdis + 0.5));
                    temdis += dis;
                }
                _arrColor.Add(_endColor);
            }
            else return;
            //Random t = new Random();
            //_arrColor = new List<int>();
            //List<int> temArrColor = new List<int>();//临时存储 用来排序
            //for (int i = 0; i < _count; i++)
            //{
            //    temArrColor.Add(t.Next(_startColor < _endColor ? _startColor : _endColor, _startColor > _endColor ? _startColor : _endColor));
            //}
            //if (_startColor > _endColor) _arrColor = OrderMaxToMin(temArrColor);
            //else _arrColor = OrderMinToMax(temArrColor) ;
        }
        public List<Color> Colors
        {
            get
            {
                if (_arrColor == null) return null;
                List<Color> result = new List<Color>();
                foreach (int i in _arrColor)
                {
                    result.Add(IntegerToRGB(i));
                }
                return result;
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
                //base.OnPaint(e);
                if (_arrColor == null) return;
                Graphics g = this.CreateGraphics();
                g.Clear(this.BackColor);
                float tem = (float)this.Width / _count;
                float jishu = 0f;
                int index=0;
                while (jishu < this.Width&&index<(_arrColor.Count<20?_arrColor.Count:20))
                {
                    Color temColor = IntegerToRGB(_arrColor[index++]);
                    g.FillRectangle(new SolidBrush(temColor), jishu,0, tem, this.Height);
                    jishu += tem;
                }
                g.Dispose();
        }
        /// <summary>
        /// 从小到大排序
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        private List<int> OrderMinToMax(List<int> arr)
        {
            if (arr == null) return null;
            if (arr.Count == 0) return null;
            List<int> result = new List<int>();
            while (arr.Count > 0)
            {
                result.Add(arr.Min());
                arr.Remove(arr.Min());
            }
            return result;
        }
        /// <summary>
        /// 从大到小排序
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        private List<int> OrderMaxToMin(List<int> arr)
        {
            if (arr == null) return null;
            if (arr.Count == 0) return null;
            List<int> result = new List<int>();
            while (arr.Count > 0)
            {
                result.Add(arr.Max());
                arr.Remove(arr.Max());
            }
            return result;
        }
        /// <summary>
        /// RGB颜色转化为对应的整数值
        /// </summary>
        /// <param name="color">待转换颜色</param>
        /// <returns></returns>
        private int RGBToInteger(Color color)
        {
            try
            {
                return (color.B + color.G * 256 + color.R * 65536);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 整数转化为对应的RGB值
        /// 整数值小于等于16777215，大于等于0
        /// </summary>
        /// <param name="integer">整数</param>
        /// <returns>返回RGB</returns>
        private Color IntegerToRGB(int integer)
        {
            /* 整数转RGB 
            rgb顾名思义红绿兰. 
            将颜色值除以65536,得到整数就是r 
            然后将余数除以256,得到整数就是g 
            最后的余数就是b 
            */
            try
            {
                if (integer > 16777215 || integer < 0) return Color.Empty;
                int R, G, B;
                R = (int)Math.Truncate((double)integer / 65536);
                integer = integer % 65536;
                G = (int)Math.Truncate((double)integer / 256);
                B = integer % 256;
                Color result = Color.FromArgb(R, G, B);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
    
}
