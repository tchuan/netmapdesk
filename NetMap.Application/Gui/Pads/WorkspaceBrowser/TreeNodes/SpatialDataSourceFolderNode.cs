﻿using NetMap.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Application.WorkspaceBrowser.TreeNodes
{
    public class SpatialDataSourceFolderNode : CustomFolderNode
    {
        ISpatialDataSource _sds;

        /// <summary>
        /// 获取空间数据源
        /// </summary>
        public ISpatialDataSource SpatialDataSource
        {
            get
            {
                return _sds;
            }
        }

        public SpatialDataSourceFolderNode(ISpatialDataSource sds)
        {
            _sds = sds;

            ContextmenuAddinTreePath = "/Application/Pads/WorkspaceBrowser/ContextMenu/SpatialDataSourceFolderNode";

            this.Tag = sds;
            Text = "空间数据";

            OpenedImage = "WorkspaceBrowser.SolutionFolder.Open";
            ClosedImage = "WorkspaceBrowser.SolutionFolder.Closed";
        }
    }
}
