﻿// Copyright (c) AlphaSierraPapa for the KJSuite Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

using System;
using System.Windows.Forms;
using NetMap.Application.Core;
using NetMap.Application.Core.WinForms;
using NetMap.Application.Gui;
using NetMap.Application.WorkspaceBrowser.TreeNodes;

namespace NetMap.Application.WorkspaceBrowser
{
	/// <summary>
	/// Description of ProjectBrowserPanel.
	/// </summary>
	public class WorkspaceBrowserPanel : UserControl
	{
		ToolStrip             toolStrip;
		WorkspaceBrowserControl workspaceBrowserControl;
		ToolStripItem[]       standardItems;
		
		public AbstractWorkspaceBrowserTreeNode SelectedNode {
			get {
                return workspaceBrowserControl.SelectedNode;
			}
		}
		
		public WorkspaceBrowserControl ProjectBrowserControl {
			get {
                return workspaceBrowserControl;
			}
		}

        public WorkspaceBrowserPanel()
		{
            workspaceBrowserControl = new WorkspaceBrowserControl();
            workspaceBrowserControl.Dock = DockStyle.Fill;
            Controls.Add(workspaceBrowserControl);

            string path = "/Application/Pads/ProjectBrowser/ToolBar/Standard";
			if (AddInTree.ExistsTreeNode(path)) {
				toolStrip = ToolbarService.CreateToolStrip(this, path);
				toolStrip.ShowItemToolTips  = true;
				toolStrip.Dock = DockStyle.Top;
				toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
				toolStrip.Stretch   = true;
				standardItems = new ToolStripItem[toolStrip.Items.Count];
				toolStrip.Items.CopyTo(standardItems, 0);
				Controls.Add(toolStrip);
			}
            workspaceBrowserControl.TreeView.BeforeSelect += TreeViewBeforeSelect;
		}
		
		void TreeViewBeforeSelect(object sender, TreeViewCancelEventArgs e)
		{
			UpdateToolStrip(e.Node as AbstractWorkspaceBrowserTreeNode);
		}

        void UpdateToolStrip(AbstractWorkspaceBrowserTreeNode node)
		{
			if (toolStrip == null) return;
			toolStrip.Items.Clear();
			toolStrip.Items.AddRange(standardItems);
			ToolbarService.UpdateToolbar(toolStrip);
			if (node != null && node.ToolbarAddinTreePath != null) {
				toolStrip.Items.Add(new ToolStripSeparator());
				toolStrip.Items.AddRange(ToolbarService.CreateToolStripItems(node.ToolbarAddinTreePath, node, false));
			}
		}
	}
}
