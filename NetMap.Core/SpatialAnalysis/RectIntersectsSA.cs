﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.SpatialAnalysis;

namespace NetMap.SpatialAnalysis
{
    /// <summary>
    /// 范围相交分析
    /// </summary>
    internal class RectIntersectsSA : ISpatialAnalysis
    {
        private IEnvelope _rect = null;

        internal RectIntersectsSA(IEnvelope rect)
        {
            _rect = rect;
        }

        public IGeometry Geometry
        {
            get
            {
                return _rect as IGeometry;
            }
            set
            {
                _rect = value as IEnvelope;
            }
        }

        public bool Evaluate(IGeometry geometry)
        {
            return _rect.IsIntersect(geometry.Envelope);
        }
    }
}
