﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetMap.UI.Ctrl
{
    /// <summary>
    /// 线型控件
    /// </summary>
    public partial class LineStyleCtrl : UserControl
    {
        private LineStyleForm _frm = new LineStyleForm();
        private bool[] _style = null;
        /// <summary>
        /// 线型变化事件
        /// </summary>
        public event LineStyleChangedEventHandler LineStyleChanged;

        public LineStyleCtrl()
        {
            InitializeComponent();
            _frm.LineStyleChanged += new LineStyleChangedEventHandler(UpdateLineStyleView);
        }

        /// <summary>
        /// 线型
        /// </summary>
        public bool[] Style
        {
            get
            {
                return _style;
            }
            set
            {
                _style = value;
                _frm.GetEditor().Grids = _style;
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            using (Pen pen = new Pen(Color.Black, 3))
            {
                List<float> linearray = new List<float>();
                if (_style != null)
                {
                    bool cur = _style[0];
                    int num = 0;
                    for (int i = 0; i < _style.Length; i++)
                    {
                        if (_style[i] == cur)
                        {
                            num++;
                        }
                        else // 虚实切换
                        {
                            linearray.Add(num);
                            num = 1;
                            cur = !cur;
                        }
                    }
                    linearray.Add(num);
                }
                if (linearray.Count > 0)
                {
                    pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Custom;
                    if (!_style[0]) // 以虚线开头
                    {
                        pen.DashOffset = linearray[0];
                        linearray.RemoveAt(0);
                    }
                    if (linearray.Count > 0)
                    {
                        pen.DashPattern = linearray.ToArray();                        
                    }
                }
                e.Graphics.DrawLine(pen, 0, 0.5f * Height, Width, 0.5f * Height);
            }
        }

        void UpdateLineStyleView(bool[] style)
        {
            _style = style;
            Refresh();
            if (LineStyleChanged != null)
            {
                LineStyleChanged(style);
            }
        }

        protected override void OnClick(EventArgs e)
        {
            base.OnClick(e);
            _frm.Show(this);
        }
    }
}
