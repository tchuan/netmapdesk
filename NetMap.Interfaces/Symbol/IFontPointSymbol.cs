﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Symbol
{
    /// <summary>
    /// 字体点符号接口
    /// </summary>
    public interface IFontPointSymbol : IPointSymbol
    {
        /// <summary>
        /// 获取和设置字体样式
        /// </summary>
        ITextSymbol Font { get; set; }
        /// <summary>
        /// 获取和设置内容
        /// </summary>
        string Text { get; set; }
    }
}
