﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Format;
using NetMap.Interfaces.Symbol;
using System.Drawing;
using NetMap.Interfaces.Persist;
using System.Xml;

namespace NetMap.Symbol
{
    /// <summary>
    /// 字体点符号
    /// </summary>
    public class FontPointSymbol : PointSymbol, IFontPointSymbol, IBinaryFormat, IPersistResource
    {
        /// <summary>
        /// 默认的字体点符号
        /// </summary>
        public FontPointSymbol()
        {
            Font = new TextSymbol();
            Text = "A";
        }

        public FontPointSymbol(FontPointSymbol symbol)
        {
            //TODO
        }

        public override ISymbol Clone()
        {
            return new FontPointSymbol(this);
        }

        public override string KindAbstract
        {
            get { return SymbolFactory.S_POINT_FONT; }
        }

        public ITextSymbol Font
        {
            get;
            set;
        }

        public string Text
        {
            get;
            set;
        }

        #region IBinaryFormat 成员

        public byte[] ToWkb()
        {
            MemoryStream stream = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(stream);

            writer.Write(Name);
            writer.Write(Code);

            writer.Write(Color.A);
            writer.Write(Color.R);
            writer.Write(Color.G);
            writer.Write(Color.B);
            writer.Write(XOffset);
            writer.Write(YOffset);
            writer.Write(Angle);
            writer.Write(Size);

            writer.Write(Font.KindAbstract);
            IBinaryFormat format = Font as IBinaryFormat;
            byte[] wkb = format.ToWkb();
            writer.Write(wkb.Count());
            writer.Write(wkb);
            writer.Write(Text);

            writer.Dispose();

            return stream.ToArray();
        }

        public void FromWkb(byte[] wkb)
        {
            MemoryStream stream = new MemoryStream(wkb);
            BinaryReader reader = new BinaryReader(stream);
            Name = reader.ReadString();
            Code = reader.ReadInt32();

            Color = Color.FromArgb(reader.ReadInt32());
            XOffset = reader.ReadDouble();
            YOffset = reader.ReadDouble();
            Angle = reader.ReadDouble();
            Size = reader.ReadDouble();

            string sk = reader.ReadString();
            ISymbol fontSymbol = SymbolFactory.CreateSymbol(sk);
            if (fontSymbol == null)
            {
                Font = null;
            }
            else
            {
                Font = fontSymbol as ITextSymbol;
                int count = reader.ReadInt32();
                byte[] fontwkb = reader.ReadBytes(count);
                IBinaryFormat format = Font as IBinaryFormat;
                format.FromWkb(fontwkb);
            }
            
            Text = reader.ReadString();
        }

        #endregion

        #region IPersistResource 成员

        public void Save(XmlElement node, XmlDocument document)
        {
            XmlElement symbol = document.CreateElement("ArcPointSymbol");
            symbol.SetAttribute("Name", Name);
            symbol.SetAttribute("Code", Code.ToString());
            symbol.SetAttribute("Color", string.Format("#{0:X2}{1:X2}{2:X2}", Color.R, Color.G, Color.B, Color.A));
            symbol.SetAttribute("XOffset", XOffset.ToString("%.2f"));
            symbol.SetAttribute("YOffset", YOffset.ToString("%.2f"));
            symbol.SetAttribute("Angle", Angle.ToString("%.2f"));
            symbol.SetAttribute("Size", Angle.ToString("%.2f"));
            symbol.SetAttribute("FontSymbolCode", Font.Code.ToString());
            node.AppendChild(symbol);
        }

        public void Load(XmlReader reader, object context)
        {
            Name = reader.GetAttribute("Name");
            Code = int.Parse(reader.GetAttribute("Code"));
            Color = ColorTranslator.FromHtml(reader.GetAttribute("Color"));
            XOffset = double.Parse(reader.GetAttribute("XOffset"));
            YOffset = double.Parse(reader.GetAttribute("YOffset"));
            Angle = double.Parse(reader.GetAttribute("Angle"));
            Size = double.Parse(reader.GetAttribute("Size"));
            int fontCode = int.Parse(reader.GetAttribute("FontSymbolCode"));
            ISymbolLibrary lib = context as ISymbolLibrary;
            ISymbol symbol = lib.FindSymbol(fontCode);
            if (symbol == null)
            {
                throw new ApplicationException("对文本符号的引用错误");
            }
            Font = symbol as ITextSymbol;
        }

        #endregion
    }
}
