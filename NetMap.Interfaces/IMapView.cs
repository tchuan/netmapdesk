﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Render;
using System.Drawing;

namespace NetMap.Interfaces
{  
    /// <summary>
    /// 地图视图接口
    /// </summary>
    public interface IMapView
    {
        /// <summary>
        /// 背景色
        /// </summary>
        Color BackColor { get; set; }
        /// <summary>
        /// 获取渲染环境
        /// </summary>
        IDisplay Display { get; }
        /// <summary>
        /// 获取坐标转换
        /// </summary>
        /// <returns></returns>
        IDisplayTransformation DisplayTransformation { get; }
        /// <summary>
        /// 获取渲染控制
        /// </summary>
        /// <returns></returns>
        IRenderCancel GetCancel();
        /// <summary>
        /// 清空视图
        /// </summary>
        void Clear();
        /// <summary>
        /// 立即展示
        /// </summary>
        void Refresh();
        /// <summary>
        /// 视图无效
        /// </summary>
        void Invalidate();
        /// <summary>
        /// 地图文档
        /// </summary>
        IMapDocument MapDocument { get; set; }
        /// <summary>
        /// 缩放到比例尺
        /// </summary>
        /// <param name="scale">比例尺</param>
        void ZoomToScale(double scale);
        /// <summary>
        /// 缩放到窗口
        /// </summary>
        /// <param name="window"></param>
        void ZoomToWindow(IEnvelope window);
        /// <summary>
        /// 缩放到数据范围
        /// </summary>
        /// <param name="box"></param>
        void ZoomToData(IEnvelope box);
        /// <summary>
        /// 缩放到全图
        /// </summary>
        void ZoomToMap();
        /// <summary>
        /// 缩放到scale比例尺下，并以x,y为中心
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="scale"></param>
        void ZoomToCenter(double x, double y, double scale);
        /// <summary>
        /// 展示tip
        /// </summary>
        /// <param name="tip">tip内容</param>
        /// <param name="active">是否激活</param>
        void ShowTip(string tip, bool active);
        /// <summary>
        /// 状态展示
        /// </summary>
        IStatus Status { get; set; } 
    }
}
