﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Persist
{
    /// <summary>
    /// 文件接口
    /// </summary>
    public interface IFile
    {
        /// <summary>
        /// 获取文件标识，用于文件验证
        /// </summary>
        string Flag { get; }
        /// <summary>
        /// 获取和设置版本
        /// </summary>
        int Version { get; set; }
        /// <summary>
        /// 获取和设置更新时间
        /// </summary>
        DateTime Time { get; set; }
        /// <summary>
        /// 保存文档
        /// </summary>
        void Save();
        /// <summary>
        /// 加载文档
        /// </summary>
        /// <param name="path"></param>
        void Load(string path);
        /// <summary>
        /// 另存文档
        /// </summary>
        /// <param name="path"></param>
        void SaveAs(string path);
    }
}
