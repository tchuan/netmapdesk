﻿using NetMap.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Tools.Select
{
    /// <summary>
    /// 按线选择
    /// </summary>
    public class LineSelect : Tool
    {
        public LineSelect()
            : base("按线选择")
        {
        }

        public LineSelect(IMapEditView view)
            : base("按线选择", view)
        {
        }
    }
}
