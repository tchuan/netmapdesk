﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Format
{
    /// <summary>
    /// 二进制格式化接口
    /// </summary>
    public interface IBinaryFormat
    {
        /// <summary>
        /// 导出成字节流
        /// </summary>
        /// <returns></returns>
        byte[] ToWkb();
        /// <summary>
        /// 从字节流导入
        /// </summary>
        void FromWkb(byte[] wkb);
    }
}
