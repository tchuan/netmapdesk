﻿using System;
using NetMap.Interfaces.Data;
using Community.CsharpSqlite;
using NetMap.Interfaces.Geometry;
using NetMap.Data;
using ProjNet.CoordinateSystems;
using System.Collections.Generic;

namespace NetMap.Sqlite
{
    /// <summary>
    /// Sqlite空间数据库
    /// </summary>
    internal class SqliteSpatialDatabase : SqliteDatabase, ISpatialDatabase
    {
        internal ITable _metaTable = null; //元数据表

        internal SqliteSpatialDatabase()
        {
        }

        #region ISpatialDatabase 成员

        public IFeatureClass CreateFeatureClass(string name, IFields fields,
            geoFeatureType fType, geoGeometryType gType, IEnvelope extent, ICoordinateSystem coordinateSystem)
        {
            SqliteFeatureClass featureClass = new SqliteFeatureClass();
            int err = featureClass.create(this, name, fields, fType, gType, extent, coordinateSystem);
            if (err != Sqlite3.SQLITE_OK)
            {
                throw new Exception("创建要素集失败");
            }
            return featureClass;
        }

        public IFeatureClass OpenFeatureClass(string name)
        {
            SqliteFeatureClass featureClass = new SqliteFeatureClass();
            int err = featureClass.open(this, name);
            if (err != Sqlite3.SQLITE_OK)
            {
                throw new Exception("打开要素集失败");
            }
            return featureClass;
        }

        public void StartEdit()
        {
            throw new NotImplementedException();
        }

        public void EndEdit()
        {
            throw new NotImplementedException();
        }

        public bool IsEditing
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// 得到数据库中所有表名
        /// </summary>
        public System.Collections.Generic.List<string> TableNames
        {
            get
            {
                string sql = "select name from sqlite_master where type=\"table\" order by name";
                Sqlite3.Vdbe stmt = null;
                int err1 = Sqlite3.sqlite3_prepare_v2(_db, sql, sql.Length, ref stmt, 0);
                if (err1 != Sqlite3.SQLITE_OK)
                {
                    return null;
                }
                err1 = Sqlite3.sqlite3_step(stmt);
                List<string> result = new List<string>();
                while (err1 == Sqlite3.SQLITE_ROW)
                {
                    string name = Sqlite3.sqlite3_column_text(stmt, 0);
                    err1 = Sqlite3.sqlite3_step(stmt);
                    result.Add(name);
                }
                return result;
            }
        }

        #endregion

        /// <summary>
        /// 打开空间数据库
        /// </summary>
        /// <param name="db"></param>
        /// <param name="isNew">是否是新建的空间数据库</param>
        internal override void open(Sqlite3.sqlite3 db, bool isNew)
        {
            base.open(db, isNew);
          
            if (isNew)
            {
                //构建元数据表
                IFields fields = new Fields();
                IField tnField = new Field();
                tnField.Name = SpatialDbDefine.FIELD_TABLENAME;
                tnField.Type = geoFieldType.GEO_STRING;
                fields.SetField(0, tnField);
                IField ftField = new Field();
                ftField.Name = SpatialDbDefine.FIELD_FEATURETYPE;
                ftField.Type = geoFieldType.GEO_SMALLINT;
                fields.SetField(1, ftField);
                IField gtField = new Field();
                gtField.Name = SpatialDbDefine.FIELD_GEOMETRYTYPE;
                gtField.Type = geoFieldType.GEO_SMALLINT;
                fields.SetField(2, gtField);
                IField boxField = new Field();
                boxField.Name = SpatialDbDefine.FIELD_ENVELOPE;
                boxField.Type = geoFieldType.GEO_BLOB;
                fields.SetField(3, boxField);
                IField csField = new Field();
                csField.Name = SpatialDbDefine.FIELD_COORDINATESSYSTEM;
                csField.Type = geoFieldType.GEO_STRING;
                fields.SetField(4,csField);
                _metaTable = CreateTable(SpatialDbDefine.TABLE_METADATA, fields);
            }
            else
            {
                _metaTable = OpenTable(SpatialDbDefine.TABLE_METADATA);
            }
        }        
    }
}
