﻿using NetMap.Fetcher;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Tile;
using NetMap.Tile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*
 * create: 2014-05-18 只加载一次的加载策略
 */

namespace NetMap.Fetcher
{
    public class OnceFetchStrategy : IFetchStrategy
    {
        public OnceFetchStrategy()
        {
        }

        /// <summary>
        /// 获取目标瓦片集合
        /// </summary>
        /// <param name="schema"></param>
        /// <param name="extent"></param>
        /// <param name="levelId"></param>
        /// <returns></returns>
        public IList<TileInfo> GetTilesWanted(ITileSchema schema, IEnvelope extent, 
            string levelId)
        {
            IList<TileInfo> infos = new List<TileInfo>();
            var tileInfos = schema.GetTilesInView(extent, levelId);
            tileInfos = SortByPriority(tileInfos, extent.Center.X, extent.Center.Y);
            foreach (var info in tileInfos)
            {
                infos.Add(info);
            }
            return infos;
        }

        private IEnumerable<TileInfo> SortByPriority(IEnumerable<TileInfo> tiles, double centerX, double centerY)
        {
            return tiles.OrderBy(t => FastDistance(centerX, centerY, t.Extent.Center.X, t.Extent.Center.Y));
        }

        public double FastDistance(double x1, double y1, double x2, double y2)
        {
            return Math.Pow(x1 - x2, 2.0) + Math.Pow(y1 - y2, 2.0);
        } 
    }
}
