﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Format;
using NetMap.Interfaces.Symbol;
using System.Drawing;

namespace NetMap.Symbol
{
    /// <summary>
    /// 图片点符号
    /// </summary>
    public class ImagePointSymbol : PointSymbol, IImagePointSymbol, IBinaryFormat
    {
        public ImagePointSymbol()
        {
            AnchorX = 0.5;
            AnchorY = 0.5;
        }

        public ImagePointSymbol(Image img)
        {
            Image = img;
            AnchorX = 0.5;
            AnchorY = 0.5;
        }

        public ImagePointSymbol(ImagePointSymbol symbol)
        {
            Image = symbol.Image.Clone() as Image;
            AnchorX = symbol.AnchorX;
            AnchorY = symbol.AnchorY;

            Color = symbol.Color;
            XOffset = symbol.XOffset;
            YOffset = symbol.YOffset;
            Angle = symbol.Angle;
            Size = symbol.Size;
            Rotate = symbol.Rotate;
        }

        public override ISymbol Clone()
        {
            return new ImagePointSymbol(this);
        }

        public override string KindAbstract
        {
            get { return SymbolFactory.S_POINT_IMAGE; }
        }

        #region IImagePointSymbol 成员

        public Image Image { get; set; }

        public double AnchorX { get; set; }

        public double AnchorY { get; set; }

        #endregion

        #region IBinaryFormat 成员

        public byte[] ToWkb()
        {
            //TODO
            throw new NotImplementedException();
        }

        public void FromWkb(byte[] wkb)
        {
            //TODO
            throw new NotImplementedException();
        }

        #endregion        
    }
}
