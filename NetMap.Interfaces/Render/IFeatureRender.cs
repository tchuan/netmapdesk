﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Data;

namespace NetMap.Interfaces.Render
{
    /// <summary>
    /// 图层方案接口
    /// </summary>
    public interface IFeatureRender
    {
        /// <summary>
        /// 绘制要素
        /// </summary>
        /// <param name="display">渲染环境</param>
        /// <param name="cursor">渲染目标要素</param>
        /// <param name="cancel">控制绘图过程的对象</param>
        void DrawCursor(IDisplay display, IFeatureCursor cursor, IRenderCancel cancel);
    }
}
