﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Symbol;
using NetMap.Interfaces.Geometry;
using System.Drawing;

namespace NetMap.Render
{
    /// <summary>
    /// 弧段点符号绘制
    /// </summary>
    public class ArcPointSymbolDrawer : ISymbolDrawer
    {
        public void Draw(IDisplay display,
            ISymbol symbol, IGeometry geometry, 
            string text)
        {
            if (geometry is IPoint)
            {
                TrueDraw(display, symbol as IArcPointSymbol, geometry as IPoint);
            }
            else if (geometry is IMultiPoint)
            {
                IMultiPoint points = geometry as IMultiPoint;
                foreach (IPoint point in points)
                {
                    TrueDraw(display, symbol as IArcPointSymbol, point);
                }
            }
        }

        private void TrueDraw(IDisplay display,
            IArcPointSymbol symbol,
            IPoint point)
        {
            double x = 0, y = 0;
            display.DisplayTransformation.FromMapPoint(point.X, point.Y, ref x, ref y);
            if (symbol.Fill)
            {
                System.Drawing.Brush brush = new SolidBrush(symbol.FillColor);
                display.G.FillEllipse(brush, (float)(x - 0.5 * symbol.Size),
                    (float)(y - 0.5 * symbol.Size), (float)symbol.Size, (float)symbol.Size);
            }
            if (symbol.Outline)
            {
                System.Drawing.Pen pen = new Pen(symbol.Color, symbol.Width);
                display.G.DrawEllipse(pen, (float)(x - 0.5 * symbol.Size),
                    (float)(y - 0.5 * symbol.Size), (float)symbol.Size, (float)symbol.Size);
            }
        }

        public void Draw(Graphics g, ISymbol symbol, IGeometry geometry, string text)
        {
            if (geometry is IPoint)
            {
                TrueDirectlyDraw(g, symbol as IArcPointSymbol, geometry as IPoint);
            }
            else if (geometry is IMultiPoint)
            {
                IMultiPoint points = geometry as IMultiPoint;
                foreach (IPoint point in points)
                {
                    TrueDirectlyDraw(g, symbol as IArcPointSymbol, point);
                }
            }
        }
        
        //直接绘制
        private void TrueDirectlyDraw(Graphics g, IArcPointSymbol symbol, IPoint point)
        {
            if (symbol.Fill)
            {
                System.Drawing.Brush brush = new SolidBrush(symbol.FillColor);
                g.FillEllipse(brush, (float)(point.X - 0.5 * symbol.Size),
                    (float)(point.Y - 0.5 * symbol.Size), (float)symbol.Size, (float)symbol.Size);
            }
            if (symbol.Outline)
            {
                System.Drawing.Pen pen = new Pen(symbol.Color, symbol.Width);
                g.DrawEllipse(pen, (float)(point.X - 0.5 * symbol.Size),
                    (float)(point.Y - 0.5 * symbol.Size), (float)symbol.Size, (float)symbol.Size);
            }
        }
    }
}
