﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;

namespace NetMap.Interfaces.Data
{
    /// <summary>
    /// 数据库接口
    /// </summary>
    public interface IDatabase
    {
        /// <summary>
        /// 获取和设置名字
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// 获取和设置工厂名字
        /// </summary>
        string FactoryName { get; }
        /// <summary>
        /// 数据源连接属性
        /// </summary>
        IConnectProperties ConnectProperties { get; }
        /// <summary>
        /// 新建表
        /// </summary>
        /// <param name="name">表名</param>
        /// <param name="fields">表的字段信息</param>
        /// <returns></returns>
        ITable CreateTable(string name, IFields fields);        
        /// <summary>
        /// 打开表
        /// </summary>
        /// <param name="name">表名</param>
        /// <returns></returns>
        ITable OpenTable(string name);        
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sql">sql语句</param>
        /// <returns></returns>
        IRecordset OpenRecordset(string sql);
        /// <summary>
        /// 执行sql语句
        /// </summary>
        /// <param name="sql"></param>
        void ExecuteSQL(string sql);
        /// <summary>
        /// 执行SQL语句获取bool值
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        bool ExecuteAndGetBool(string sql);
        /// <summary>
        /// 执行SQl语句获取int值
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        int ExecuteAndGetInt(string sql);
        /// <summary>
        /// 执行SQL获取行id
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        int ExecuteAndGetId(string sql);
        /// <summary>
        /// 执行SQL语句获取string值
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        string ExecuteAndGetString(string sql);
        /// <summary>
        /// 执行SQL语句获取blob字段值
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        byte[] ExecuteAndGetBytes(string sql);
        /// <summary>
        /// 执行SQL语句获取double字段值
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        double ExecuteAndGetDouble(string sql);
        /// <summary>
        /// 执行SQL语句获取datetime字段值
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        DateTime ExecuteAndGetDatetime(string sql);
        /// <summary>
        /// 执行数据库更新操作
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        bool ExecuteUpdate(string sql);
        /// <summary>
        /// 启动事务
        /// </summary>
        void StartTransaction();
        /// <summary>
        /// 提交事务
        /// </summary>
        void CommitTransaction();
        /// <summary>
        /// 回滚事务
        /// </summary>
        void RollTransaction();
        /// <summary>
        /// 获取是否在事务进行中
        /// </summary>
        bool IsTransactioning { get; }
        /// <summary>
        /// 重做
        /// </summary>
        void Redo();
        /// <summary>
        /// 撤销
        /// </summary>
        void Undo();
        /// <summary>
        /// 获取是否可以重做
        /// </summary>
        bool Redoable { get; }
        /// <summary>
        /// 获取是否可以撤销
        /// </summary>
        bool Undoable { get; }
        /// <summary>
        /// 关闭
        /// </summary>
        void Close();
    }
}
