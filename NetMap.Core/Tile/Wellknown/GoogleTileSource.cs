// Copyright (c) BruTile developers team. All rights reserved. See License.txt in the project root for license information.

// This file was created by Felix Obermaier (www.ivv-aachen.de) 2010.

using NetMap.Interfaces.Tile;
using System;
using System.Diagnostics;
using System.Net;

namespace NetMap.Tile.Wellknown
{
    [Serializable]
    public class GoogleTileSource : ITileSource
    {
        private readonly SphericalMercatorInvertedWorldSchema _tileSchema;
        private readonly WebTileProvider _tileProvider;
        public const string UserAgent = @"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.7) Gecko/20091221 Firefox/3.5.7";
        public const string Referer = "http://maps.google.com/";

        public GoogleTileSource(GoogleMapType mapType)
            : this(new GoogleRequest(mapType))
        {
        }

        public GoogleTileSource(GoogleRequest request, ITileCache<byte[]> tileCache = null)
        {
            _tileSchema = new SphericalMercatorInvertedWorldSchema();
            _tileProvider = new WebTileProvider(request, tileCache, 
                // The Google requests needs to fake the UserAgent en Referer.
                uri =>
                    {
                        var httpWebRequest = (HttpWebRequest) WebRequest.Create(uri);
                        httpWebRequest.UserAgent = UserAgent;
                        httpWebRequest.Referer = Referer;
                        try
                        {
                            byte[] value = RequestHelper.FetchImage(httpWebRequest);
                            return value;
                        }
                        catch (System.Exception ex)
                        {
                            Debug.Write(ex);
                            return null;
                        }                        
                    });
        }

        public ITileProvider Provider
        {
            get { return _tileProvider; }
        }

        public ITileSchema Schema
        {
            get { return _tileSchema; }
        }

        public string Title { get; private set; }
    }
}
