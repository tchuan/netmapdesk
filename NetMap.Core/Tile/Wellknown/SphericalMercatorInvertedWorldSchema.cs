﻿// Copyright (c) BruTile developers team. All rights reserved. See License.txt in the project root for license information.

using NetMap.Interfaces.Tile;
namespace NetMap.Tile.Wellknown
{
    public class SphericalMercatorInvertedWorldSchema : SphericalMercatorWorldSchema
    {
        public SphericalMercatorInvertedWorldSchema() 
        {
            Axis = AxisDirection.InvertedY;
            OriginY = -OriginY; 
            Name = "WorldSphericalMercatorInverted";
        }
    }
}
