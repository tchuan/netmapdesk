﻿using NetMap.Data;
using NetMap.Interfaces.Data;
using NetMap.Interfaces.Tile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Tile.Cache
{
    /// <summary>
    /// 样例瓦片缓存
    /// 字段：level string 层级
    ///       row   int 行
    ///       col   int 列
    ///       image blob 瓦片
    ///       addtime datetime 加入时间
    /// </summary>
    public class SampleDatabaseCache : DatabaseCache
    {
        public SampleDatabaseCache(ITable table)
            : base(table, "level", "row", "col", "image", "addtime")
        {
        }

        /// <summary>
        /// 创建瓦片表
        /// </summary>
        /// <param name="database">数据库</param>
        /// <param name="tileTableName">瓦片表名</param>
        /// <returns></returns>
        public static ITable CreateTable(IDatabase database, string tileTableName)
        {
            IFields fields = new Fields();
            IField levelField = new Field("level", geoFieldType.GEO_STRING);
            IField rowField = new Field("row", geoFieldType.GEO_INT);
            IField colField = new Field("col", geoFieldType.GEO_INT);
            IField imageField = new Field("image", geoFieldType.GEO_BLOB);
            IField addTimeField = new Field("addtime", geoFieldType.GEO_DATE);
            fields.SetField(0, levelField);
            fields.SetField(1, rowField);
            fields.SetField(2, colField);
            fields.SetField(3, imageField);
            fields.SetField(4, addTimeField);
            return database.CreateTable(tileTableName, fields);
        }
    }
}
