﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Geometry
{
    /// <summary>
    /// 圆
    /// </summary>
    public interface ICircle
    {
        /// <summary>
        /// 获取和设置x坐标
        /// </summary>
        double X { get; set; }
        /// <summary>
        /// 获取和设置y坐标
        /// </summary>
        double Y { get; set; }
        /// <summary>
        /// 获取和设置半径
        /// </summary>
        double Radius { get; set; }
    }
}
