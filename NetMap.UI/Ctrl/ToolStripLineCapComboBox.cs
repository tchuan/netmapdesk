﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetMap.UI.Ctrl
{
    /// <summary>
    /// 在工具栏上使用的LineComboBox
    /// </summary>
    public class ToolStripLineCapComboBox : ToolStripControlHost
    {
        public ToolStripLineCapComboBox()
            : base(new LineCapComboBox())
        {
        }

        /// <summary>
        /// 获取LineCapComboBox
        /// </summary>
        /// <returns></returns>
        public LineCapComboBox GetLineCapComboBox()
        {
            return Control as LineCapComboBox;
        }
    }
}
