﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetMap.Interfaces
{
    public delegate void MapToolChangeHandler(ITool command);

    /// <summary>
    /// 地图编辑视图事件接口
    /// </summary>
    public interface IMapEditViewEvent
    {
        /// <summary>
        /// 地图工具变化事件
        /// </summary>
        event MapToolChangeHandler MapToolChange;        
    }

    /// <summary>
    /// 地图编辑视图接口
    /// </summary>
    public interface IMapEditView : IMapView
    {    
        /// <summary>
        /// 设置地图当前工具
        /// </summary>
        /// <param name="tool">工具</param>
        void SetCurrentTool(ITool tool);
        /// <summary>
        /// 获取地图当前工具
        /// </summary>
        /// <returns></returns>
        ITool GetCurrentTool();
    }
}
