﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Application.Gui;
using NetMap.Application.Core.WinForms;
using System.Windows.Forms;
using NetMap.Interfaces.Data;
using StoneGround.Search.TreeNodes;
using StoneGround.Search.Commands;

namespace StoneGround.Search
{
    /// <summary>
    /// 查询面板
    /// </summary>
    public class SearchPad : AbstractPadContent
    {
        public static SearchPad Instance;
        private Panel _contentPanel = new Panel();
        private ExtTreeView _stoneGroundTreeView = new ExtTreeView();
        private SearchCondition _condition = new SearchCondition();

        public SearchPad()
        {
            Instance = this;

            _stoneGroundTreeView.Dock = DockStyle.Fill;
            _stoneGroundTreeView.ShowLines = false;
            _stoneGroundTreeView.ShowRootLines = false;
            _stoneGroundTreeView.ShowPlusMinus = false;
            _contentPanel.Controls.Add(_stoneGroundTreeView);

            string path = "/Application/Pads/SearchPad/Toolbars";
            ToolStrip[] toolStrips = ToolbarService.CreateToolbars(this, path);
            _contentPanel.Controls.AddRange(toolStrips);

            //绑定事件
            DzoneSearch.Instance.DzoneChange += ZoneSearch.Instance.DzoneChange;
            _stoneGroundTreeView.NodeMouseDoubleClick += new TreeNodeMouseClickEventHandler(_stoneGroundTreeView_NodeMouseDoubleClick);
        }

        #region 节点事件处理

        void _stoneGroundTreeView_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                StoneGroundNode node = e.Node as StoneGroundNode;
                node.Position();
            }
        }

        #endregion

        public override object Control
        {
            get { return _contentPanel; }
        }

        /// <summary>
        /// 获取料场树
        /// </summary>
        public ExtTreeView StoneGroundTreeView
        {
            get
            {
                return _stoneGroundTreeView;
            }
        }

        /// <summary>
        /// 获取查询条件
        /// </summary>
        public SearchCondition Condition
        {
            get
            {
                return _condition;
            }
        }

        /// <summary>
        /// 构建树节点
        /// </summary>
        /// <param name="featureCursor"></param>
        public void BuildSearchTreeNodes(IFeatureCursor featureCursor)
        {
            _stoneGroundTreeView.BeginUpdate();

            _stoneGroundTreeView.Nodes.Clear(); //先清理

            IFeature feature = featureCursor.Next();
            while (feature != null)
            {
                TreeNode node = new StoneGroundNode(feature);
                _stoneGroundTreeView.Nodes.Add(node);
                feature = featureCursor.Next();
            }            
            _stoneGroundTreeView.EndUpdate();
        }

        /// <summary>
        /// 构建树节点
        /// </summary>
        /// <param name="features"></param>
        public void BuildSearchTreeNodes(List<IFeature> features)
        {
            _stoneGroundTreeView.BeginUpdate();

            _stoneGroundTreeView.Nodes.Clear(); //先清理

            foreach(IFeature feature in features)
            {
                TreeNode node = new StoneGroundNode(feature);
                _stoneGroundTreeView.Nodes.Add(node);
            }
            _stoneGroundTreeView.EndUpdate();
        }

        /// <summary>
        /// 清空树节点
        /// </summary>
        public void ClearSearchTreeNodes()
        {
            _stoneGroundTreeView.Nodes.Clear();
        }
    }
}
