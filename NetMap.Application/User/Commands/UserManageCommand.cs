﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Application.Core;
using User.Dialogs;

namespace User.Commands
{
    /// <summary>
    /// 用户管理
    /// </summary>
    public class UserManageCommand : AbstractCommand
    {
        public override void Run()
        {
            FrmUserManage dialog = new FrmUserManage();
            dialog.ShowDialog();
        }
    }
}
