﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Data;

namespace NetMap.Data
{
    public class QueryFilter : IQueryFilter
    {
        protected List<string> _fields = new List<string>();
        protected string _whereClause = string.Empty;

        public List<string> Fields
        {
            get { return _fields; }
        }

        public string WhereClause
        {
            get
            {
                return _whereClause;
            }
            set
            {
                _whereClause = value;
            }
        }
    }
}
