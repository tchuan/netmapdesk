﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Data;

namespace NetMap.Interfaces.Data
{
    /// <summary>
    /// 影像数据接口
    /// </summary>
    public interface IImageData : IData
    {
        /// <summary>
        /// 获取影像
        /// </summary>
        /// <param name="level">层</param>
        /// <param name="row">行</param>
        /// <param name="col">列</param>
        /// <returns></returns>
        SpaceImage GetImage(int level, int row, int col);
    }
}
