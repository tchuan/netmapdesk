﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjNet.CoordinateSystems;
using NetMap.Interfaces;
using NetMap.Interfaces.Data;
using NetMap.Interfaces.Element;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Symbol;

namespace NetMap
{
    public class ElementLayer : Layer, IElementLayer
    {
        protected IEnvelope _extent = null;
        protected ICoordinateSystem _coord = null;

        public ElementLayer(IEnvelope bounds, ICoordinateSystem coord)
        {
            Elements = new List<IElement>();
            _extent = bounds;
            _coord = coord;
        }

        #region IElementLayer 成员

        public List<IElement> Elements
        {
            get;
            set;
        }

        public ISymbol Symbol { get; set; }

        #endregion
    
        public override void Draw(IDisplay display)
        {            
            ISymbolRender symbolRender = display.SymbolRender;
            symbolRender.Setup(display);
            foreach (IElement element in Elements)
            {
                switch (element.ElementType)
                {
                    case geoElementType.GEO_ELEMENT_SIMPLE:
                        {
                            symbolRender.Draw(Symbol, element.Geometry);
                            break;
                        }
                    case geoElementType.GEO_ELEMENT_TEXT:
                        {
                            ITextElement textElement = element as ITextElement;
                            symbolRender.DrawText(Symbol as ITextSymbol, textElement.Point, textElement.Text);
                            break;
                        }
                    case geoElementType.GEO_ELEMENT_RASTER:
                        {
                            IRasterElement raterElement = element as IRasterElement;
                            symbolRender.Draw(raterElement.Image.Bounds, raterElement.Image.Image);
                            break;
                        }
                }
            }
        }

        public override IEnvelope Extent
        {
            get { return _extent; }
        }

        public override ICoordinateSystem CoordinateSystem
        {
            get { return _coord; }
        }
    }
}
