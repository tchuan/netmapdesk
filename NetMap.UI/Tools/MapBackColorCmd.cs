﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces;
using NetMap.UI.Properties;
using System.Drawing;
using NetMap.UI.Ctrl;

namespace NetMap.Tools
{
    /// <summary>
    /// 地图背景色设置工具
    /// </summary>
    public class MapBackColorCmd : Command
    {
        private ColorSelectForm _frm = new ColorSelectForm();

        public MapBackColorCmd()
            : base("背景色设置")
        {
            Init();
        }

        public MapBackColorCmd(IMapView view)
            : base("背景色设置", view)
        {            
            Init();
        }

        protected override void SetIcon()
        {
            _icon = Resources.green_chalkboard;
        }

        private void Init()
        {
            _frm.colorComboboxCtrl.ColorChangedEvent += new ColorCombobox.ColorChangedHandle(colorComboboxCtrl_ColorChangedEvent);
        }

        void colorComboboxCtrl_ColorChangedEvent(Color color)
        {
            View.BackColor = color;
            View.Invalidate();
        }

        public override void Execute()
        {
            _frm.Show();
        }
    }
}
