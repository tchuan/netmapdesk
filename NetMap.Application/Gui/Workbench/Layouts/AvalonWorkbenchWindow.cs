﻿// Copyright (c) AlphaSierraPapa for the SharpDevelop Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

using AvalonDock;
using NetMap.Application.Core;
using NetMap.Application.Core.Presentation;
using System.Windows.Media;

namespace NetMap.Application.Gui
{
	sealed class AvalonWorkbenchWindow : DocumentContent, IWorkbenchWindow, IOwnerState
	{
		readonly static string contextMenuPath = "/Application/Workbench/OpenFileTab/ContextMenu";
		
		AvalonDockLayout dockLayout;
		
		public AvalonWorkbenchWindow(AvalonDockLayout dockLayout)
		{
			if (dockLayout == null)
				throw new ArgumentNullException("dockLayout");
			
			CustomFocusManager.SetRememberFocusedChild(this, true);
			this.IsFloatingAllowed = true;
			this.dockLayout = dockLayout;
		}
		
		protected override void FocusContent()
		{
			WpfWorkbench.FocusDebug("{0}.FocusContent() IsActiveContent={1} IsKeyboardFocusWithin={2} Keyboard.FocusedElement={3}",
			                        Title, IsActiveContent, IsKeyboardFocusWithin, Keyboard.FocusedElement);
			if (!(IsActiveContent && !IsKeyboardFocusWithin)) {
				return;
			}
			IInputElement activeChild = CustomFocusManager.GetFocusedChild(this);
			WpfWorkbench.FocusDebug("{0}.FocusContent() - Will move focus (activeChild={1})", this.Title, activeChild);
		}
		
		internal static void SetFocus(ManagedContent m, Func<object> activeChildFunc, bool forceSetFocus = false)
		{
			m.Dispatcher.BeginInvoke(
				DispatcherPriority.Background,
				new Action(
					delegate {
						// ensure that condition for FocusContent() is still fulfilled
						// (necessary to avoid focus switching loops when changing layouts)
						if (!forceSetFocus && !(m.IsActiveContent && !m.IsKeyboardFocusWithin)) {
							WpfWorkbench.FocusDebug("{0} - not moving focus (IsActiveContent={1}, IsKeyboardFocusWithin={2})",
							                        m.Title, m.IsActiveContent, m.IsKeyboardFocusWithin);
							return;
						}
                        object activeChild = activeChildFunc();
                        if (activeChild is IInputElement)
                        {
                            WpfWorkbench.FocusDebug("{0} - moving focus to: {1}", m.Title, activeChild != null ? activeChild.ToString() : "<null>");
                            if (activeChild != null)
                            {
                                Keyboard.Focus(activeChild as IInputElement);
                            }
                        }
                        else if (activeChild is System.Windows.Forms.Control)
                        {
                            (activeChild as System.Windows.Forms.Control).Focus();
                        }
					}));
		}
		
		public bool IsDisposed { get { return false; } }
		
		#region IOwnerState
		[Flags]
		public enum OpenFileTabStates {
			Nothing             = 0,
			FileDirty           = 1,
			FileReadOnly        = 2,
			FileUntitled        = 4,
			ViewContentWithoutFile = 8
		}
		
		public System.Enum InternalState {
			get {
				OpenFileTabStates state = OpenFileTabStates.Nothing;
				//todo 
				return state;
			}
		}
		#endregion
		
		TabControl viewTabControl;
		
		SDWindowsFormsHost GetActiveWinFormsHost()
		{
            return this.Content as SDWindowsFormsHost;
		}
		
		public event EventHandler ActiveViewContentChanged;			
		
		
		public void SelectWindow()
		{
			Activate();//this.SetAsActive();
		}
		
		void Dispose()
		{
		}

        sealed class TabControlWithModifiedShortcuts : TabControl
        {
            readonly AvalonWorkbenchWindow parentWindow;

            public TabControlWithModifiedShortcuts(AvalonWorkbenchWindow parentWindow)
            {
                this.parentWindow = parentWindow;
            }

            protected override void OnKeyDown(KeyEventArgs e)
            {
                // We don't call base.KeyDown to prevent the TabControl from handling Ctrl+Tab.
                // Instead, we let the key press bubble up to the DocumentPane.
            }
        }
		
		bool forceClose;
		
		public bool CloseWindow(bool force)
		{
			WorkbenchSingleton.AssertMainThread();
			
			forceClose = force;
			Close();
            return true;
		}
		
		protected override void OnClosed()
		{
			base.OnClosed();
			Dispose();
			CommandManager.InvalidateRequerySuggested();
		}

		void OnTitleChanged()
		{
			if (TitleChanged != null)
			{
				TitleChanged(this, EventArgs.Empty);
			}
		}

		public event EventHandler TitleChanged;

		void OnInfoTipChanged()
		{
			if (InfoTipChanged != null)
			{
				InfoTipChanged(this, EventArgs.Empty);
			}
		}

		public event EventHandler InfoTipChanged;

		public override string ToString()
		{
			return "[AvalonWorkbenchWindow: " + this.Title + "]";
		}
		
		/// <summary>
		/// Gets the target for re-routing commands to this window.
		/// </summary>
		internal IInputElement GetCommandTarget()
		{
			return CustomFocusManager.GetFocusedChild(this) ?? GetActiveWinFormsHost();
		}
	}
}
