﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;

namespace NetMap.Interfaces.Data
{
    /// <summary>
    /// 要素类型
    /// </summary>
    public enum geoFeatureType : short
    {
        GEO_FEATURE_SIMPLE     = 1,     //普通要素
        GEO_FEATURE_ANNOTATION = 2,     //注记要素
        GEO_FEATURE_RASTER     = 3      //影像要素
    }

    /// <summary>
    /// 要素接口
    /// </summary>
    public interface IFeature : IRow
    {        
        /// <summary>
        /// 获取和设置几何图形
        /// 注意：设置几何图形时，使用引用
        ///       获取几何图形时，使用拷贝
        /// </summary>
        IGeometry Geometry { get; set; }
        /// <summary>
        /// 获取要素类型
        /// </summary>
        geoFeatureType FeatureType { get; }
        /// <summary>
        /// 获取要素所属的地物类
        /// </summary>
        IFeatureClass FeatureClass { get; }
    }
}
