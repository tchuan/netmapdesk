﻿using System;
using NetMap.Interfaces.Data;
using Community.CsharpSqlite;
using NetMap.Data;

namespace NetMap.Sqlite
{
    /// <summary>
    /// Sqlite数据库游标
    /// </summary>
    internal class SqliteCursor : ICursor
    {
        protected Sqlite3.Vdbe _stmt = null;
        protected IFields _fields = null;
        protected SqliteTable _table = null;
        private bool _last = false;

        internal SqliteCursor()
        {
        }

        #region ICursor 成员

        public IFields Fields
        {
            get { return _fields; }
        }

        public IRow Next()
        {
            if (_last)
                return null;
            SqliteRow row = new SqliteRow();
            row.InitFields(_table, _fields);
            row.InitValue(_stmt);
            int err = Sqlite3.sqlite3_step(_stmt);
            if (err != Sqlite3.SQLITE_ROW)
            {
                _last = true;
            }
            return row;
        }

        public void Reset()
        {
            Sqlite3.sqlite3_reset(_stmt);
            int err = Sqlite3.sqlite3_step(_stmt);
            if (err != Sqlite3.SQLITE_ROW)
            {
                _last = true;
            }
            else
            {
                _last = false;
            }
        }

        public void Close()
        {
            if (_stmt != null)
            {
                Sqlite3.sqlite3_finalize(_stmt);
                _stmt = null;
            }
        }

        public ITable Table
        {
            get { return _table; }
        }

        #endregion

        internal void open(SqliteTable table, IFields fields, Sqlite3.Vdbe stmt)
        {
            _table = table;
            _fields = fields;
            _stmt = stmt;

            Sqlite3.sqlite3_reset(stmt);
            int err = Sqlite3.sqlite3_step(stmt);
            if (err != Sqlite3.SQLITE_ROW)
            {
                _last = true;
            }
            else
            {
                _last = false;
            }
        }
    }
}
