﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.SpatialAnalysis;
using NetMap.Interfaces.Geometry;
using NetMap.Geometry.Algorithms;

namespace NetMap.SpatialAnalysis
{
    /// <summary>
    /// 与圆相交分析
    /// 备注：可用于表示图形是否在点的指定距离内
    /// </summary>
    public class CircleIntersectsSA : ISpatialAnalysis
    {
        private ICircle _circle;

        internal CircleIntersectsSA(ICircle circle)
        {
            _circle = circle;
        }

        #region ISpatialAnalysis 成员

        public IGeometry Geometry
        {
            get { return _circle as IGeometry; }
            set { _circle = value as ICircle; }
        }

        public bool Evaluate(IGeometry geometry)
        {
            bool result = false;
            switch (geometry.GeometryType)
            {
                case geoGeometryType.GEO_GEOMETRY_POINT:
                case geoGeometryType.GEO_GEOMETRY_DIRECTIONPOINT:
                    {
                        //用距离的平方做比较
                        IPoint point = geometry as IPoint;
                        double dis2 = (point.X - _circle.X) * (point.X - _circle.X)
                             + (point.Y - _circle.Y) * (point.Y - _circle.Y);
                        result = (dis2 <= _circle.Radius * _circle.Radius);
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_LINESTRING:
                    {                        
                        ILineString lineString = geometry as ILineString;
                        double dis2Min = GetMinDisToLineString(lineString); 
                        result = (dis2Min <= _circle.Radius * _circle.Radius);
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_POLYGON:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTIPOINT:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTILINESTRING:
                    {
                        IMultiLineString multiLineString = geometry as IMultiLineString;
                        double dis2Min = double.MaxValue; //注意多段线数量为0时会有bug
                        double dis2Temp = 0;
                        foreach (ILineString lineString in multiLineString)
                        {
                            dis2Temp = GetMinDisToLineString(lineString);
                            if (dis2Temp < dis2Min)
                            {
                                dis2Min = dis2Temp;
                            }
                        }
                        result = (dis2Min <= _circle.Radius * _circle.Radius);
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTIPOLYGON:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_PATH:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_RECT:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_CIRCLE:
                    {
                        //圆心距离与半径和比较
                        ICircle circle = geometry as ICircle;
                        double dis2 = Algorithms.PointToPointDistance2(_circle.X, _circle.Y, circle.X, circle.Y);
                        double dis2Temp = (_circle.Radius + circle.Radius) * (_circle.Radius + circle.Radius);
                        break;
                    }
            }
            return result;
        }

        /*
         * 计算圆心到线段的最短距离
         * 备注：取每条线段进行到圆心的距离运算，得到最小的距离
         */
        private double GetMinDisToLineString(ILineString lineString)
        {
            IPoint p1, p2;
            double dis2Min = double.MaxValue; //注意线段点数为0时会有bug
            double dis2Temp = 0;
            int count = lineString.Count;
            for (int i = 0; i < count - 2; i++)
            {
                p1 = lineString[i];
                p2 = lineString[i + 1];
                dis2Temp = Algorithms.PointToLinesegmentDistance2(_circle.X, _circle.Y,
                    p1.X, p1.Y, p2.X, p2.Y);
                if (dis2Temp < dis2Min)
                {
                    dis2Min = dis2Temp;
                }
            }
            return dis2Min;
        }

        #endregion
    }
}
