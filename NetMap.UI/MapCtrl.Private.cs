﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using NetMap.Geometry;
using NetMap.Render;
using System.Drawing.Drawing2D;
using NetMap.Interfaces;
using NetMap.Interfaces.Render;
using System.Drawing.Text;
using NetMap.Interfaces.Geometry;
using NetMap.Render;
using NetMap.UI.Properties;

namespace NetMap.UI
{
    partial class MapCtrl
    {
        #region 私有变量
        private IMapDocument _map = null;
        private ITool _tool = null; // 当前工具
        //[地图]
        private IDisplay _display = null;
        private IDisplayTransformation _TF = null;
        //[地图效果]
        private IDisplay _effectDisplay = null;
        //[缓存]
        private Image _image = null;       //地图图片缓存
        private Image _effectImage = null; //地图效果缓存
        private Image _bufferImage = null; //控件双缓存
        private Rectangle _bufferImgBound;
        private ScaleRatioBar _scaleRatioBar = null;
        private System.Drawing.Point _scaleRatioBarAnchor = new System.Drawing.Point(10, 10);
        #endregion

        private void Init()
        {
            _TF = new DisplayTransformation();
            if (_map != null)
            {
                _TF.MapBounds = _map.Extent;
                _TF.WindowBounds = new Envelope(0, 0, Width, Height);
                _TF.ZoomToMapBounds();
            }
            else
            {
                //默认和地图范围和窗口一致，当地图文档重新设置时就变化了
                _TF.MapBounds = new Envelope(0, 0, Width, Height);
                _TF.WindowBounds = new Envelope(0, 0, Width, Height);
                _TF.ZoomToMapBounds();
            }
            UpdateImage();
            _bufferImgBound = new Rectangle(0, 0, Width, Height);
            _bufferImage = new Bitmap(Width, Height);
        }

        /// <summary>
        /// 绘制地图
        /// </summary>
        private void DrawMap()
        {
            OnBeforeDraw(_display);
            //Stopwatch watch = new Stopwatch();
            //Log.GetInstance().Debug("开始地图渲染");
            //watch.Start();

            _display.G.Clear(BackColor);
            ILayer layer = null;
            //for ( int i = _map.LayerCount - 1; i >= 0; i--)
            for (int i = 0; i < _map.LayerCount; i++) //Modify 2013-10-15 图层的添加（AddLayer）永远是在最上层进行添加
            {
                layer = _map.GetLayer(i);
                if (layer.Visible)
                {
                    if (_display.DisplayTransformation.ScaleRatio >= layer.MinScale
                        && _display.DisplayTransformation.ScaleRatio <= layer.MaxScale)
                    {
                        layer.Draw(_display);
                    }
                }
            }

            // 绘制比例尺条
            if (_scaleRatioBar != null)
            {
                _scaleRatioBar.Draw(_display.G, _scaleRatioBarAnchor,
                    _display.DisplayTransformation.ScaleRatio);
            }

            //watch.Stop();
            //Log.GetInstance().Debug("地图渲染结束，耗时(毫秒)：" + watch.ElapsedMilliseconds);
            OnAfterDraw(_display);

            _bRefreshMap = false; //地图刷新完成
        }

        private void UpdateUI(IEnvelope bufferImgBound)
        {
            _bufferImgBound = new Rectangle((int)bufferImgBound.XMin, (int)bufferImgBound.YMin, (int)bufferImgBound.Width, (int)bufferImgBound.Height);

            using (Graphics g = Graphics.FromImage(_bufferImage))
            {
                // 设置高质量的呈现文本
                g.TextRenderingHint = TextRenderingHint.AntiAlias;
                // 绘制
                g.Clear(BackColor);
                g.DrawImage(_image, _bufferImgBound);
                g.DrawImage(_effectImage, 0, 0);
                if (_tool != null)
                {
                    _tool.Draw(g);
                }
            }
        }

        //更新坐标转换
        private void UpdateTF()
        {
            _TF.MapBounds = _map.Extent;
            _TF.WindowBounds = new Envelope(0, 0, Width, Height);
            _TF.ZoomToMapBounds();          
        }

        //更新图片缓存
        private void UpdateImage()
        {
            if (_image != null) 
            {
                _image.Dispose();
            }
            _image = new Bitmap(Width, Height);
            if (_effectImage != null)
            {
                _effectImage.Dispose();
            }
            _effectImage = new Bitmap(Width, Height);
            if (_bufferImage != null)
            {
                _bufferImage.Dispose();
            }
            _bufferImage = new Bitmap(Width, Height);

            //创建新的绘图环境
            ISymbolRender symbolRender = new SymbolRender();
            _display = new Display(Graphics.FromImage(_image), symbolRender, null, _TF);
            symbolRender.Setup(_display);
            ISymbolRender effectSymbolRender = new SymbolRender();
            _effectDisplay = new Display(Graphics.FromImage(_effectImage), effectSymbolRender, null, _TF);
            effectSymbolRender.Setup(_effectDisplay);
            
            // 设置高质量的呈现地图图像
            _display.G.SmoothingMode = SmoothingMode.AntiAlias;
        }

        //取消数据获取
        private void CancelFetcher()
        {
        }
    }
}
