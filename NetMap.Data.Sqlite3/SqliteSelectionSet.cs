﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Community.CsharpSqlite;
using NetMap.Interfaces.Data;
using NetMap.Interfaces.SpatialAnalysis;
using NetMap.Interfaces.Geometry;
using NetMap.Geometry;

namespace NetMap.Sqlite
{
    /// <summary>
    /// Sqlite要素选择集
    /// </summary>
    internal class SqliteSelectionSet : ISelectionSet
    {
        private Dictionary<long, long> _dic; //不能会用HashSet，这里用Dictionary代替
        private SqliteFeatureClass _featureClass = null;
        private IEnvelope _box = null;

        internal SqliteSelectionSet()
        {
            _dic = new Dictionary<long, long>();
        }

        #region ISelectionSet 成员

        public bool IsIn(long oid)
        {
            return _dic.ContainsKey(oid);
        }

        public int Count
        {
            get { return _dic.Count; }
        }

        public void Add(long oid)
        {
            if(!_dic.ContainsKey(oid))
            {
                _dic.Add(oid, oid);
            }
        }

        public void Remove(long oid)
        {
            _dic.Remove(oid);
        }

        public void RemoveAll()
        {
            _dic.Clear();
        }

        public void AddList(IEnumerable<long> oids)
        {
            foreach(long oid in oids)
            {
                Add(oid);
            }
        }

        public void RemoveList(IEnumerable<long> oids)
        {
            foreach(long oid in oids)
            {
                Remove(oid);
            }
        }
        
        public IFeatureClass GetTarget()
        {
            return _featureClass;
        }

        public IFeatureCursor Search(IQueryFilter filter)
        {
            return _featureClass.search(filter, this);
        }

        public ISelectionSet Select(IQueryFilter filter)
        {
            return _featureClass.select(filter, this);
        }

        public IEnvelope Envelope { get { return _box; } }
        
        public IEnumerator<long> GetEnumerator()
        {
            return _dic.Keys.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _dic.Keys.GetEnumerator();
        }        

        #endregion

        //记录OID和范围
        internal void open(SqliteFeatureClass featureClass, IFeatureCursor cursor)
        {
            _featureClass = featureClass;
            IFeature feature = cursor.Next();
            if (feature != null)
            {
                _box = new Envelope(feature.Geometry.Envelope);
            }
            else
            {
                _box = null;
            }
            while(feature != null)
            {
                _dic.Add(feature.OID, feature.OID);
                _box.Union(feature.Geometry.Envelope);
                feature = cursor.Next();
            }
            cursor.Close();
        }        
    }
}
