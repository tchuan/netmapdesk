﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using NetMap.Interfaces.Geometry;
using NetMap.Data;

namespace NetMap.Interfaces.Data
{
    /// <summary>
    /// 影像要素接口
    /// </summary>
    public interface IRasterFeature : IFeature
    {
        /// <summary>
        /// 设置和获取空间影像
        /// </summary>
        SpaceImage Image { get; set; }
    }
}
