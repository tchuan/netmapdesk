﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using NetMap.Application;
using NetMap.Interfaces.Data;
using NetMap.Data;
using NetMap.Application.Util;
using NetMap.Interfaces;
using NetMap.Interfaces.Geometry;
using StoneGround.Search;

namespace StoneGround.Analyze.Dialog
{
    public partial class FrmRoad : Form
    {
        public FrmRoad()
        {
            InitializeComponent();

            Load += FrmRoad_Load;
            listViewMain.ItemSelectionChanged += listViewMain_ItemSelectionChanged;
        }

        void FrmRoad_Load(object sender, EventArgs e)
        {
            //由于道路存在多个要素组成一个道路的情况，因此要记录多个要素
            Dictionary<string, List<IFeature>> featureDic = new Dictionary<string, List<IFeature>>();
            IQueryFilter filter = new QueryFilter();
            filter.Fields.Add(SpatialDatabaseService.FIELD_ROAD_CODE);
            filter.Fields.Add(SpatialDatabaseService.FIELD_ROAD_LEVEL);
            filter.Fields.Add(SpatialDatabaseService.FIELD_ROAD_NAME);
            IFeatureClass zoneFeatureClass = SpatialDatabaseService.Instance.RoadFeatureClass;
            IFeatureCursor cursor = zoneFeatureClass.Search(filter);
            IFeature feature = cursor.Next();
            while (feature != null)
            {
                string name = feature.GetValue(0).ToString();
                if (!string.IsNullOrEmpty(name))
                {
                    if (featureDic.Keys.Contains(name))
                    {
                        featureDic[name].Add(feature);
                    }
                    else
                    {
                        List<IFeature> featureLst = new List<IFeature>();
                        featureLst.Add(feature);
                        featureDic.Add(name, featureLst);
                    }
                }
                feature = cursor.Next();
            }
            cursor.Close();

            //构建界面
            foreach(KeyValuePair<string, List<IFeature>> item in featureDic)
            {
                ListViewItem lvItem = new ListViewItem(item.Key);
                lvItem.Tag = item.Value;
                listViewMain.Items.Add(lvItem);
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            Close();
        }

        void listViewMain_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (listViewMain.SelectedItems.Count <= 0)
                return;

            var featureLst = new List<IFeature>();
            foreach (var item in listViewMain.SelectedItems)
            {
                var i = item as ListViewItem;
                featureLst.AddRange(i.Tag as List<IFeature>);
            }

            var sgFeatures = new List<IFeature>();
            var sgFC = StoneGroundDatabaseService.Instance.StoneGroundFeatureClass;
            var sgFields = sgFC.Fields;
            IQueryFilter fFilter = new QueryFilter();
            for (int i = 0; i < sgFields.Count; i++)
            {
                var field = sgFields.GetField(i);
                fFilter.Fields.Add(field.Name);
            }
            IFeatureCursor fCursor = sgFC.Search(fFilter);
            IFeature fFeature = fCursor.Next();
            while (fFeature != null)
            {
                IPoint point = fFeature.Geometry as IPoint;
                IPoint minDisPoint = GetMinDisPointFromMutiLineString(point, featureLst);
                double dis = WebMercatorUtil.Instance.GetDistance(point, minDisPoint);
                if (dis <= StoneGroundAnalyzeConfig.ServiceRadius)
                {
                    sgFeatures.Add(fFeature);
                }
                fFeature = fCursor.Next();
            }
            //重新构建树
            if (sgFeatures.Count == 0)
            {
                SearchPad.Instance.ClearSearchTreeNodes();
                MessageBox.Show("该公路周围没有料场", "提示");
            }
            else
            {
                SearchPad.Instance.BuildSearchTreeNodes(sgFeatures);
            }

            // 高亮当前公路
            HighlightRoad();
        }

        private void HighlightRoad()
        {
            IMapView view = WorkspaceSingleton.ActiveMapView;
            ILayer layer = view.MapDocument.FindLayer(StoneGroundDefines.LAYER_NAME_ROAD);
            IFeatureSelection selection = layer as IFeatureSelection;
            IQueryFilter filter = new QueryFilter();

            var roads = "";
            foreach (var item in listViewMain.SelectedItems)
            {
                var i = item as ListViewItem;
                roads += ("'" + i.Text + "',"); 
            }
            roads = roads.Remove(roads.Length - 1);
            filter.WhereClause = SpatialDatabaseService.FIELD_ROAD_CODE + " in (" + roads + ")";

            selection.Select(filter, geoSelectionResultEnum.GEO_SELECTIONRESULT_NEW);
            IEnvelope box = selection.SelectionSet?.Envelope;
            if (box != null)
            {
                //外扩1/2后，缩放
                IPoint center = box.Center;
                IEnvelope boxZoom = new NetMap.Geometry.Envelope(center.X - box.Width, center.Y - box.Height,
                    center.X + box.Width, center.Y + box.Height);
                view.ZoomToData(boxZoom);
            }
            view.Invalidate();
        }

        private IPoint GetMinDisPointFromMutiLineString(IPoint point, List<IFeature> featureLst)
        {
            double dis = double.MaxValue;
            IPoint tempPoint = new NetMap.Geometry.Point(0, 0);
            IPoint bestPoint = new NetMap.Geometry.Point(0, 0);
            foreach (IFeature feature in featureLst)
            {
                IMultiLineString mline = feature.Geometry as IMultiLineString;
                foreach (ILineString line in mline)
                {
                    double temp = GetMinDisPointFromLineString(point, line, tempPoint);
                    if (temp < dis)
                    {
                        bestPoint.X = tempPoint.X; bestPoint.Y = tempPoint.Y;
                        dis = temp;
                    }
                }
            }
            return bestPoint;
        }

        private double GetMinDisPointFromLineString(IPoint point, ILineString line, IPoint pointBest)
        {
            IPoint p1, p2;
            double dis2Min = double.MaxValue; //注意线段点数为0时会有bug
            double dis2Temp = 0;
            int count = line.Count;
            IPoint pointTemp = new NetMap.Geometry.Point();
            for (int i = 0; i < count - 2; i++)
            {
                p1 = line[i];
                p2 = line[i + 1];
                dis2Temp = NetMap.Geometry.Algorithms.Algorithms.PointToLinesegmentMinDisPoint(point, p1, p2, pointTemp);
                if (dis2Temp < dis2Min)
                {
                    pointBest.X = pointTemp.X; pointBest.Y = pointTemp.Y;
                    dis2Min = dis2Temp;
                }
            }
            return dis2Min;
        }
    }
}
