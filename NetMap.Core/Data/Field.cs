﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Data;

namespace NetMap.Data
{
    /// <summary>
    /// 字段
    /// </summary>
    public class Field : IField
    {        
        private string _name = string.Empty;
        private geoFieldType _type = geoFieldType.GEO_INT;
        private bool _isNullable = false;

        /// <summary>
        /// 构建默认字段
        /// </summary>
        public Field()
            : this("Default", geoFieldType.GEO_INT)
        {            
        }

        /// <summary>
        /// 构建字段
        /// </summary>
        /// <param name="name">名字</param>
        /// <param name="type">类型</param>
        public Field(string name, geoFieldType type)
        {
            _name = name;
            _type = type;
        }

        /// <summary>
        /// 构建字段
        /// </summary>
        /// <param name="field">参考字段</param>
        public Field(IField field)
        {
            _name = field.Name;
            _type = field.Type;
            _isNullable = field.IsNullable;
        }

        #region IField 成员

        public bool IsNullable
        {
            get
            {
                return _isNullable;
            }
            set
            {
                _isNullable = value;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public geoFieldType Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
            }
        }

        #endregion
    }
}
