﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Common
{
    /// <summary>
    /// 信息处理
    /// </summary>
    /// <param name="message"></param>
    public delegate void MessageHandler(string message);
}
