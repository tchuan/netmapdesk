﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Data;
using NetMap.Fetcher;
using NetMap.Interfaces;
using NetMap.Interfaces.Fetcher;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Tile;
using NetMap.Tile;
using NetMap.Interfaces.Persist;
using System.Xml;
using NetMap.Interfaces.Data;
using NetMap.Geometry;
using NetMap.Utilities;

namespace NetMap
{
    /// <summary>
    /// 动态瓦片图层
    /// </summary>
    public class TileLayer : Layer, ITileLayer, IAsyncDataFetcher, IPersistResource
    {
        private ITileFetcher _tileFetcher;
        private ITileSource _tileSource;
        private readonly MemoryCache<SpaceImage> _cache = new MemoryCache<SpaceImage>(200, 300);
        private int _maxRetries;
        private bool _repairEmpty = false;

        public TileLayer()
        {
        }

        /// <summary>
        /// 构建瓦片图层
        /// </summary>
        /// <param name="source">瓦片数据源</param>
        /// <param name="localMode">是否为本地模式</param>
        public TileLayer(ITileSource source, bool localMode = false)
        {
            _tileSource = source;
            if (localMode)
            {
                _tileFetcher = new OnceTileFetcher(source, _cache);
                _tileFetcher.DataChanged += _tileFetcher_DataChanged;
            }
            else
            {
                _tileFetcher = new TileFetcher(source, _cache, _maxRetries);
                _tileFetcher.DataChanged += _tileFetcher_DataChanged;
            }
        }

        private void _tileFetcher_DataChanged(DataChangedEventArgs e)
        {
            if (DataChanged != null)
            {
                e.LayerTag = Tag; //设置图层tag
                DataChanged.Invoke(e);
            }
        }        

        #region ITileLayer 成员

        public int MaxRetries
        {
            set { _maxRetries = value; }
        }

        public bool RepairEmpty
        {
            get { return _repairEmpty; }
            set { _repairEmpty = value; }
        }

        public ITileSource TileSource
        {
            get { return _tileSource; }
        }

        #endregion

        #region IAsyncDataFetcher 成员

        public void AbortFetch()
        {
            if (_tileFetcher != null)
            {
                _tileFetcher.AbortFetch();
            }
        }

        public void ViewChanged(bool changeEnd, IDisplayTransformation trans)
        {
            if (_tileFetcher != null && changeEnd)
            {
                _tileFetcher.ViewChanged(trans);
            }
        }

        public event DataChangedEventHandler DataChanged;

        public void ClearCache()
        {
            _cache.Clear();
        }

        #endregion

        #region ILayer 成员

        public override void Draw(IDisplay display)
        {
            if (CoordinateTransformation != null)
            {
                //不支持影像的投影变换
                return;
            }
            ISymbolRender render = display.SymbolRender;
            render.Setup(display);
            IEnumerable<SpaceImage> imgs = GetFeaturesInView(display.DisplayTransformation);
            foreach (SpaceImage img in imgs)
            {
                render.Draw(img.Bounds, img.Image);
            }
        }

        public override ProjNet.CoordinateSystems.ICoordinateSystem CoordinateSystem
        {
            get
            {
                return null; //modify 2014-04-16
            }
        }

        public override IEnvelope Extent
        {
            get { return _tileSource.Schema.Extent; }
        }

        #endregion

        private IEnumerable<SpaceImage> GetFeaturesInView(IDisplayTransformation trans)
        {
            var dictionary = new Dictionary<TileIndex, SpaceImage>();
            ITileSchema schema = _tileSource.Schema;
            string level = Utilities.ZoomHelper.GetNearestLevel(schema.Resolutions, 1.0 / trans.ScaleRatio);
            GetRecursive(dictionary, schema, _cache, trans.VisibleMapBounds, level);

            //对瓦片加载顺序进行排序
            var sortedDictionary = (from entry in dictionary orderby entry.Key ascending select entry).ToDictionary(pair => pair.Key, pair => pair.Value);

            return sortedDictionary.Values;
        }

        private void GetRecursive(IDictionary<TileIndex, SpaceImage> resultTiles, ITileSchema schema,
            MemoryCache<SpaceImage> cache, IEnvelope extent, string level)
        {
            var tiles = schema.GetTilesInView(extent, level);

            foreach (TileInfo tileInfo in tiles)
            {
                var feature = cache.Find(tileInfo.Index);
                if (feature == null)
                {
                    if (_repairEmpty)
                    {
                        string lowerLevel;
                        if (ZoomHelper.GetLowerLevel(schema.Resolutions, schema.Resolutions[level].UnitsPerPixel, out lowerLevel))
                        {
                            GetRecursive(resultTiles, schema, cache, Envelope.Interect(tileInfo.Extent, extent), lowerLevel);
                        }
                    }
                }
                else
                {
                    resultTiles[tileInfo.Index] = feature;                    
                    //string lowerLevel;
                    //if (ZoomHelper.GetLowerLevel(schema.Resolutions, schema.Resolutions[level].UnitsPerPixel, out lowerLevel))
                    //{
                    //    GetRecursive(resultTiles, schema, cache, Envelope.Interect(tileInfo.Extent, extent), lowerLevel);
                    //}
                }
            }
        }

        public static bool IsFullyShown(SpaceImage feature)
        {
            var currentTile = DateTime.Now.Ticks;
            const long second = 10000000;
            return ((currentTile - feature.TickFetched) > second);
        }

        #region IPersistResource 成员

        public void Save(XmlElement node, XmlDocument document)
        {
            throw new NotImplementedException();
        }

        public void Load(XmlReader reader, object context)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
