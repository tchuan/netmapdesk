﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Data
{
    /// <summary>
    /// 属性表行对象接口
    /// </summary>
    public interface IRow
    {
        /// <summary>
        /// 字段集合
        /// </summary>
        IFields Fields { get; }
        /// <summary>
        /// 获取本行的OID
        /// </summary>
        long OID { get; }
        /// <summary>
        /// 本行所属的属性表
        /// </summary>
        ITable Table { get; }
        /// <summary>
        /// 获取字段值
        /// </summary>
        /// <param name="index">字段索引</param>
        /// <returns></returns>
        object GetValue(int index);
        /// <summary>
        /// 设置字段值
        /// </summary>
        /// <param name="index">字段索引</param>
        /// <param name="value">字段值</param>
        /// <returns></returns>
        void SetValue(int index, object value);
        /// <summary>
        /// 从属性表中删除本行
        /// </summary>
        void Delete();
        /// <summary>
        /// 向属性表中存储本行
        /// </summary>
        void Store();
    }
}
