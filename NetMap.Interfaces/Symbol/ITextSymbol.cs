﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;
using System.Drawing;

namespace NetMap.Interfaces.Symbol
{
    /// <summary>
    /// 注记符号接口
    /// </summary>
    public interface ITextSymbol : ISymbol
    {
        /// <summary>
        /// 文本显示的颜色
        /// </summary>
        Color Color { get; set; }
        /// <summary>
        /// 文本显示的高度
        /// </summary>
        double Height { get; set; }
        /// <summary>
        /// 文本显示的宽度
        /// </summary>
        double Width { get; set; }
        /// <summary>
        /// 获取和设置文本显示的字体
        /// </summary>
        string Font { get; set; }
        /// <summary>
        /// 获取和设置符号本身的旋转角度（以°为单位）
        /// </summary>
        double Angle { get; set; }
        /// <summary>
        /// 获取和设置旋转角度（以°为单位）
        /// </summary>
        double Rotate { get; set; }
        /// <summary>
        /// x偏移（单位为像素，坐标系为窗口坐标系）
        /// </summary>
        double OffsetX { get; set; }
        /// <summary>
        /// y偏移（单位为像素，坐标系为窗口坐标系）
        /// </summary>
        double OffsetY { get; set; }
    }
}
