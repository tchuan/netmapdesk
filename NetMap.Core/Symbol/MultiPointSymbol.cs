﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Format;
using NetMap.Interfaces.Symbol;

namespace NetMap.Symbol
{
    /// <summary>
    /// 复合点符号
    /// </summary>
    public class MultiPointSymbol : PointSymbol, IMultiPointSymbol, IBinaryFormat
    {
        private List<IPointSymbol> _items = new List<IPointSymbol>();

        public MultiPointSymbol()
        {
        }

        public MultiPointSymbol(MultiPointSymbol symbol)
        {
            // TODO
        }

        public override ISymbol Clone()
        {
            return new MultiPointSymbol(this);
        }

        public override string KindAbstract
        {
            get { return "复合点符号"; }
        }

        #region IMultiPointSymbol 成员

        public int IndexOf(IPointSymbol item)
        {
            return _items.IndexOf(item);
        }

        public void Insert(int index, IPointSymbol item)
        {
            _items.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            _items.RemoveAt(index);
        }

        public IPointSymbol this[int index]
        {
            get
            {
                return _items[index];
            }
            set
            {
                _items[index] = value;
            }
        }

        public void Add(IPointSymbol item)
        {
            _items.Add(item);
        }

        public void Clear()
        {
            _items.Clear();
        }

        public bool Contains(IPointSymbol item)
        {
            return _items.Contains(item);
        }

        public void CopyTo(IPointSymbol[] array, int arrayIndex)
        {
            _items.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _items.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(IPointSymbol item)
        {
            return _items.Remove(item);
        }

        public IEnumerator<IPointSymbol> GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        #endregion

        #region IBinaryFormat 成员

        public byte[] ToWkb()
        {
            MemoryStream stream = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(stream);

            writer.Write(Name);
            writer.Write(Code);

            writer.Write(Count);
            foreach (IPointSymbol item in this)
            {
                writer.Write(item.KindAbstract);
                IBinaryFormat format = item as IBinaryFormat;
                byte[] wkb = format.ToWkb();
                writer.Write(wkb.Count());
                writer.Write(wkb);
            }

            return stream.ToArray();
        }

        public void FromWkb(byte[] wkb)
        {
            MemoryStream stream = new MemoryStream(wkb);
            BinaryReader reader = new BinaryReader(stream);

            Name = reader.ReadString();
            Code = reader.ReadInt32();

            int count = reader.ReadInt32();
            for (int i = 0; i < count; i++)
            {
                string sk = reader.ReadString();
                ISymbol symbol = SymbolFactory.CreateSymbol(sk);
                int icount = reader.ReadInt32();
                byte[] iwkb = reader.ReadBytes(icount);
                IBinaryFormat format = symbol as IBinaryFormat;
                format.FromWkb(iwkb);
                Add(symbol as IPointSymbol);
            }
        }

        #endregion
    }
}
