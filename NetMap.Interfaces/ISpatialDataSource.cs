﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Data;

namespace NetMap.Interfaces
{
    /// <summary>
    /// 数据源，管理多个空间数据库Database
    /// </summary>
    public interface ISpatialDataSource : IEnumerable<ISpatialDatabase>
    {
        /// <summary>
        /// 添加数据库
        /// </summary>
        /// <param name="db">数据库</param>
        void Add(ISpatialDatabase db);
        /// <summary>
        /// 查找
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        ISpatialDatabase Find(string name);
        /// <summary>
        /// 移除数据库
        /// </summary>
        void Remove(ISpatialDatabase db);
        /// <summary>
        /// 清空数据库
        /// </summary>
        void Clear();
    }
}
