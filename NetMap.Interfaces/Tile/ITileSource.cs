﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Tile
{
    /// <summary>
    /// 切片数据源接口
    /// </summary>
    public interface ITileSource
    {
        /// <summary>
        /// 获取切片Provider
        /// </summary>
        ITileProvider Provider { get; }
        /// <summary>
        /// 获取切片定义
        /// </summary>
        ITileSchema Schema { get; }
    }
}
