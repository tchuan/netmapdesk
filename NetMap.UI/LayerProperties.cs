﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NetMap.Interfaces;

namespace NetMap.UI
{
    /// <summary>
    /// 图层属性编辑
    /// </summary>
    public partial class LayerProperties : Form
    {
        private ILayer _layer = null;
        private IFeatureLayer _featureLayer = null;

        public LayerProperties()
        {
            InitializeComponent();
            this.FormClosing += new FormClosingEventHandler(LayerProperties_FormClosing);
        }

        void LayerProperties_FormClosing(object sender, FormClosingEventArgs e)
        {
            Hide();
            e.Cancel = true;
        }

        /// <summary>
        /// 设置目标图层
        /// </summary>
        /// <param name="layer"></param>
        public void SetLayer(ILayer layer)
        {
            _layer = layer;
            textBoxName.Text = layer.Name;
            checkBoxVisible.Checked = layer.Visible;
            textBoxMaxScale.Text = layer.MaxScale.ToString("0.000000");
            textBoxMinScale.Text = layer.MinScale.ToString("0.000000");
            _featureLayer = layer as IFeatureLayer;
            if (_featureLayer == null)
            {
                checkBoxSelectable.Checked = false;
                checkBoxSelectable.Enabled = false;
                checkBoxEditable.Checked = false;
                checkBoxEditable.Enabled = false;
            }
        }

        // 更新图层属性
        private void btnApply_Click(object sender, EventArgs e)
        {
            _layer.Name = textBoxName.Text;
            _layer.Visible = checkBoxVisible.Checked;
            _layer.MaxScale = double.Parse(textBoxMaxScale.Text);
             _layer.MinScale = double.Parse(textBoxMinScale.Text);
            if (_featureLayer != null)
            {
                _featureLayer.Selectable = checkBoxSelectable.Checked;
                _featureLayer.Editable = checkBoxEditable.Checked;
            }
            Hide();         
        }
    }
}
