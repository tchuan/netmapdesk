﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Tracker;
using NetMap.Interfaces.Symbol;
using NetMap.Interfaces.Geometry;
using NetMap.Geometry;
using NetMap.Interfaces.Render;

namespace NetMap.Tracker
{
    public class PolygonTracker : Tracker, ICreatePolygonTracker
    {
        private ILineString _line = new LineString();
        private IPoint _moveP = new Point(0, 0);

        public PolygonTracker(ISymbol symbol, ISymbolRender symbolRender)
            : base(symbol, symbolRender)
        {
        }

        public override void Reset()
        {
            _line.Clear();
            _bstart = false;
        }

        public override void Refresh(bool bClear)
        {
            if (_line.Count > 0)
            {
                _line.Add(_moveP);
                _line.Add(_line[0]);
                //_symbol.Draw(new Polygon(_line)); //TODO 修改
                _line.RemoveAt(_line.Count - 1);
                _line.RemoveAt(_line.Count - 1);
            }
        }

        public override IGeometry GetGeometry()
        {
            if (_line.Count > 2)
            {
                return _line;
            }
            else
            {
                return null;
            }
        }

        public override void Start(IPoint point)
        {
            _line.Add(point);
            _bstart = true;
        }

        public override void MoveTo(IPoint point)
        {
            _moveP.X = point.X;
            _moveP.Y = point.Y;
        }

        public override void Finish()
        {
            _bstart = false;
        }

        #region IPolygonTracker 成员

        public void AddPoint(IPoint point)
        {
            _line.Add(point);
        }

        public void BackPoint()
        {
            if (_line.Count > 0)
            {
                _line.RemoveAt(_line.Count - 1);
            }
        }

        #endregion
    }
}
