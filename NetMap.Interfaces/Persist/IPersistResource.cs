﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using NetMap.Interfaces.Symbol;
using NetMap.Interfaces.Data;
using System.Xml;

namespace NetMap.Interfaces.Persist
{
    /// <summary>
    /// 可序列化的资源接口
    /// </summary>
    public interface IPersistResource
    {
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="stream"></param>
        void Save(XmlElement node, XmlDocument document);
        /// <summary>
        /// 加载
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="context">资源加载的上下文</param>
        void Load(XmlReader reader, object context);
    }
}
