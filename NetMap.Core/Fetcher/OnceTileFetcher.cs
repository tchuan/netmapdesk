﻿using NetMap.Data;
using NetMap.Geometry;
using NetMap.Interfaces.Fetcher;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Tile;
using NetMap.Render;
using NetMap.Tile;
using NetMap.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

/*
 * create: 2014-05-18 单次瓦片获取，
 *  对于瓦片规则下应该存在的瓦片获取失败后，将不进行下次获取。
 */

namespace NetMap.Fetcher
{
    /// <summary>
    /// 单次瓦片获取，适合本地模式
    /// </summary>
    class OnceTileFetcher : ITileFetcher
    {
        private const int ThreadMax = 2;
        private volatile bool _isThreadRunning;
        private volatile bool _isViewChanged;
        private readonly MemoryCache<SpaceImage> _memoryCache;
        private readonly ITileSource _tileSource;
        private IList<TileIndex> _tilesInProgress = new List<TileIndex>();
        private IList<TileInfo> _missingTiles = new List<TileInfo>();
        private IList<TileIndex> _cacheMissingTiles = new List<TileIndex>();
        private IDisplayTransformation _displayTransformation;
        private readonly AutoResetEvent _waitHandle = new AutoResetEvent(true);
        private readonly IFetchStrategy _strategy;
        private int _threadCount;

        #region 事件

        /// <summary>
        /// 数据改变事件
        /// </summary>
        public event DataChangedEventHandler DataChanged;

        #endregion

        public OnceTileFetcher(ITileSource tileSource, MemoryCache<SpaceImage> memoryCache)
        {
            _tileSource = tileSource;
            _memoryCache = memoryCache;
            _strategy = new OnceFetchStrategy();
        }

        public void ViewChanged(IDisplayTransformation trans)
        {
            _displayTransformation = new DisplayTransformation(trans);
            _isViewChanged = true;
            _waitHandle.Set();
            if (!_isThreadRunning) // 启动TileFetchLoop
            {
                _isThreadRunning = true;
                ThreadPool.QueueUserWorkItem(TileFetchLoop);
            }
        }

        public void AbortFetch()
        {
            _isThreadRunning = false;
            _waitHandle.Set();
        }

        #region Private Methods

        private void TileFetchLoop(object state)
        {
            try
            {
                while (_isThreadRunning)
                {
                    _waitHandle.WaitOne();

                    if (_isViewChanged)
                    {
                        ITileSchema schema = _tileSource.Schema;
                        string level = ZoomHelper.GetNearestLevel(schema.Resolutions, 1.0 / _displayTransformation.ScaleRatio);
                        _missingTiles = _strategy.GetTilesWanted(_tileSource.Schema, _displayTransformation.VisibleMapBounds, level);
                        _isViewChanged = false;
                    }

                    _missingTiles = GetTilesMissing(_missingTiles, _memoryCache, _cacheMissingTiles);

                    if (_missingTiles.Count == 0)
                    {
                        _waitHandle.Reset();
                        continue;
                    }

                    FetchTiles();

                    //Thread.Sleep(10); //为界面线程留一点CPU时间

                    if (_threadCount >= ThreadMax) { _waitHandle.Reset(); }
                }
            }
            finally
            {
                _isThreadRunning = false;
            }
        }

        private IList<TileInfo> GetTilesMissing(IEnumerable<TileInfo> infosIn, MemoryCache<SpaceImage> memoryCache,
            IList<TileIndex> cacheMissingTiles)
        {
            IList<TileInfo> tilesOut = new List<TileInfo>();
            foreach (TileInfo info in infosIn)
            {
                if (memoryCache.Find(info.Index) == null) //不包含
                {
                    lock (_cacheMissingTiles)
                    {
                        if (!_cacheMissingTiles.Contains(info.Index)) //不缺失
                        {
                            tilesOut.Add(info);
                        }
                    }
                }
            }
            return tilesOut;
        }

        private void FetchTiles()
        {
            foreach (TileInfo info in _missingTiles)
            {
                if (_threadCount >= ThreadMax)
                {
                    return;
                }
                FetchTile(info);
            }
        }

        private void FetchTile(TileInfo info)
        {
            //检查
            if (_tilesInProgress.Contains(info.Index)) return;
            if (_memoryCache.Find(info.Index) != null) return;

            //请求瓦片
            lock (_tilesInProgress)
            {
                _tilesInProgress.Add(info.Index);
            }
            _threadCount++;
            // 在线程内下载瓦片
            var fetchOnThread = new FetchOnThread(_tileSource.Provider, info, LocalFetchCompleted);
            ThreadPool.QueueUserWorkItem(fetchOnThread.FetchTile);
        }

        private void LocalFetchCompleted(object sender, FetchTileCompletedEventArgs e)
        {
            SpaceImage image = null;
            if (e.Cancelled == false
                && _isThreadRunning && e.Image != null)
            {
                image = new SpaceImage(e.Image, new Envelope(e.TileInfo.Extent));
                _memoryCache.Add(e.TileInfo.Index, image);
            }
            _threadCount--;
            lock (_tilesInProgress)
            {
                if (_tilesInProgress.Contains(e.TileInfo.Index))
                {
                    _tilesInProgress.Remove(e.TileInfo.Index);
                }
            }
            if (e.Image == null) //无瓦片
            {
                lock (_cacheMissingTiles)
                {
                    _cacheMissingTiles.Add(e.TileInfo.Index);
                }
            }
            _waitHandle.Set();
            if (DataChanged != null)
            {
                DataChanged(new DataChangedEventArgs(e.Cancelled, -1, e.TileInfo.Extent));
            }
        }

        #endregion
    }
}
