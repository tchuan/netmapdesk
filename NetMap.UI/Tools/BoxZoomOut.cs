﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using NetMap.UI.Properties;
using NetMap.Interfaces.Render;
using NetMap.Interfaces;

namespace NetMap.Tools
{
    /// <summary>
    /// 拉框缩小
    /// </summary>
    public class BoxZoomOut : Tool
    {
        private IDisplayTransformation _disTrans = null;
        private int _downx, _downy;
        private int _movex, _movey;
        private bool _down = false;

        public BoxZoomOut()
            : base("拉框缩小")
        { }

        public BoxZoomOut(IMapEditView view)
            : base("拉框缩小", view)
        { }

        protected override void SetIcon()
        {
            _icon = Resources.中心缩小_Big;
        }

        public override void GainFocus()
        {
            _disTrans = View.DisplayTransformation;
            base.GainFocus();
        }

        public override void LostFocus()
        {
            _disTrans = null;
            base.LostFocus();
        }

        public override void OnMouseDown(System.Windows.Forms.MouseEventArgs e)
        {
            _down = true;
            _downx = e.X;
            _downy = e.Y;
        }

        public override void OnMouseUp(System.Windows.Forms.MouseEventArgs e)
        {
            _down = false;
            if (e.X == _downx || e.Y == _downy)
            {
                return;
            }
            double centerX = 0, centerY = 0;
            _disTrans.ToMapPoint(0.5d * (e.X + _downx), 0.5d * (e.Y + _downy), ref centerX, ref centerY);
            double scale = _disTrans.ScaleRatio 
                / Math.Min(_disTrans.WindowBounds.Width / (double)Math.Abs(e.X - _downx), _disTrans.WindowBounds.Height / (double)Math.Abs(e.Y - _downy));
            View.ZoomToCenter(centerX, centerY, scale);
            View.Refresh();
        }

        public override void OnMouseMove(System.Windows.Forms.MouseEventArgs e)
        {
            if (_down)
            {
                _movex = e.X; _movey = e.Y;
                View.Refresh();
            }
        }

        public override void Draw(System.Drawing.Graphics g)
        {
            if (_down)
            {
                g.DrawRectangle(Pens.Green,Math.Min(_downx,_movex),
                    Math.Min(_downy, _movey), Math.Abs(_downx - _movex),
                    Math.Abs(_downy - _movey));
            }
        }
    }
}
