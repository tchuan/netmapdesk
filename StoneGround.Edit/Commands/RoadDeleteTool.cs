﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Application.Gui.Pads;
using NetMap.UI.Tools;

namespace StoneGround.Edit.Commands
{
    /// <summary>
    /// 道路要素删除
    /// </summary>
    public class RoadDeleteTool : MapCommand
    {
        public RoadDeleteTool()
        {
            InnerCmd = new FeatureDelete(StoneGroundDefines.LAYER_NAME_ROAD);
        }
    }
}
