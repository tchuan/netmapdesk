﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*
 * 2014-07-01 用组合的瓦片Provider来提供瓦片
 */

namespace NetMap.Interfaces.Tile
{
    /// <summary>
    /// 组合瓦片Provider
    /// </summary>
    public interface ICombinationTileProvider : ITileProvider
    {
        /// <summary>
        /// 添加单个层级的瓦片provider
        /// </summary>
        /// <param name="level">层级</param>
        /// <param name="provider">瓦片Provider</param>
        void AddProvider(string level, ITileProvider provider);
        /// <summary>
        /// 添加多个层级的瓦片provider
        /// </summary>
        /// <param name="levels">层级集合</param>
        /// <param name="provider">瓦片Provider</param>
        void AddProvider(List<string> levels, ITileProvider provider);
        /// <summary>
        /// 清空
        /// </summary>
        void Clear();
        /// <summary>
        /// 获取层级范围
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        void GetMinAndMaxLevel(out string min, out string max);
    }
}
