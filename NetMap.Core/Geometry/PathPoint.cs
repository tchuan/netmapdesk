﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NetMap.Core;
using NetMap.Interfaces.Geometry;

namespace NetMap.Geometry
{
    public class PathPoint : Point, IPathPoint
    {
        private IPoint _in = null;
        private IPoint _out = null;

        /// <summary>
        /// 构建路径点
        /// </summary>
        public PathPoint()
            : base()
        {
            _in = new Point(this);
            _out = new Point(this);
        }

        /// <summary>
        /// 构建路径点
        /// </summary>
        /// <param name="x">坐标x</param>
        /// <param name="y">坐标y</param>
        public PathPoint(double x, double y)
            : base(x, y)
        {
            _in = new Point(this);
            _out = new Point(this);
        }

        /// <summary>
        /// 构建路径点
        /// </summary>
        /// <param name="x">坐标x</param>
        /// <param name="y">坐标y</param>
        /// <param name="inPoint">进点</param>
        /// <param name="outPoint">出点</param>
        public PathPoint(double x, double y, IPoint inPoint, IPoint outPoint)
            : base(x, y)
        {
            _in = inPoint;
            _out = outPoint;
        }
        
        public IPoint InPoint
        {
            get
            {
                return _in;
            }
            set
            {
                _in.X = value.X;
                _in.Y = value.Y;
            }
        }

        public IPoint OutPoint
        {
            get
            {
                return _out;
            }
            set
            {
                _out.X = value.X;
                _out.Y = value.Y;
            }
        }

        public override geoGeometryType GeometryType
        {
            get
            {
                return geoGeometryType.GEO_GEOMETRY_PATHPOINT;
            }
        }

        public override IGeometry CloneGeometry()
        {
            IPathPoint geometry = new PathPoint(_x, _y);
            return geometry;
        }

        // inx,iny,x,y,outx,outy
        public override byte[] ExportToWkb()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    writer.Write(InPoint.X);
                    writer.Write(InPoint.Y);
                    writer.Write(X);
                    writer.Write(Y);
                    writer.Write(OutPoint.X);
                    writer.Write(OutPoint.Y);
                }
                return stream.ToArray();
            }
        }

        public override string ExportToWkt()
        {
            throw new NotImplementedException();
        }

        public override void ImportFromWkb(byte[] data)
        {
            using (MemoryStream stream = new MemoryStream(data))
            {
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    InPoint.X = reader.ReadDouble();
                    InPoint.Y = reader.ReadDouble();
                    X = reader.ReadDouble();
                    Y = reader.ReadDouble();
                    OutPoint.X = reader.ReadDouble();
                    OutPoint.Y = reader.ReadDouble();
                }
            }
        }

        // inx,iny,x,y,outx,outy
        public override void Export(BinaryWriter writer)
        {
            writer.Write(InPoint.X);
            writer.Write(InPoint.Y);
            writer.Write(X);
            writer.Write(Y);
            writer.Write(OutPoint.X);
            writer.Write(OutPoint.Y);
        }

        public override void Import(BinaryReader reader)
        {
            InPoint.X = reader.ReadDouble();
            InPoint.Y = reader.ReadDouble();
            X = reader.ReadDouble();
            Y = reader.ReadDouble();
            OutPoint.X = reader.ReadDouble();
            OutPoint.Y = reader.ReadDouble();
        }
    }
}
