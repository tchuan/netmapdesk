﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Application.Gui.Pads;
using NetMap;
using NetMap.Interfaces;
using System.Windows.Forms;
using NetMap.Interfaces.Data;
using NetMap.Data;
using StoneGround.Analyze.Dialog;

namespace StoneGround.Analyze.Commands
{
    public class RoadAnalyzeTool : Tool
    {
        public RoadAnalyzeTool()
            : base("道路周围料场分析")
        {
        }

        public RoadAnalyzeTool(IMapEditView view)
            : base("道路周围料场分析", view)
        {
        }

        private FrmRoad roadDialog;

        public override void GainFocus()
        {
            base.GainFocus();
            if (roadDialog == null)
            {
                roadDialog = new FrmRoad();
                roadDialog.StartPosition = FormStartPosition.CenterParent;
            }
            roadDialog.ShowDialog(NetMap.Application.Gui.WorkbenchSingleton.MainWin32Window);
        }

        public override void LostFocus()
        {
            //base.LostFocus();
            roadDialog.Hide();
        }
    }

    /// <summary>
    /// 高速路维修周围料场分析
    /// </summary>
    public class RoadAnalyzeCommand : MapCommand
    {
        public RoadAnalyzeCommand()
        {
            InnerCmd = new RoadAnalyzeTool();
        }
    }
}
