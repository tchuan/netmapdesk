﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Tracker;
using NetMap.Interfaces.Geometry;
using NetMap.Geometry;
using NetMap.Interfaces.Symbol;
using NetMap.Interfaces.Render;
using System.Drawing;

using Point = NetMap.Geometry.Point;
using NetMap.Render;

namespace NetMap.Tracker
{
    /// <summary>
    /// 创建多线段橡皮条
    /// </summary>
    public class CreateLineStringTracker : Tracker, ICreateLineStringTracker
    {
        private ILineString _line = new LineString();
        private IPoint _moveP = new Point(0, 0);
        private bool _bClose = false;

        public CreateLineStringTracker()
            : base()
        {
        }

        public CreateLineStringTracker(ISymbol symbol)
            : base(symbol)
        {
        }

        public override void Reset()
        {
            _line = new LineString();
            base.Reset();
        }

        public override IGeometry GetGeometry()
        {
            if (_line.Count > 1)
            {
                return _line;
            }
            else
            {
                return null;
            }
        }

        public override void Start(IPoint point)
        {
            _line.Add(point);
            _bstart = true;
        }

        public override void MoveTo(IPoint point)
        {
            _moveP.X = point.X;
            _moveP.Y = point.Y;
        }

        public override void Finish()
        {
            _bstart = false;
        }

        public override void Draw(Graphics g)
        {
            if (_line.Count > 0)
            {
                ISymbolDrawer drawer = SymbolDrawerFactory.Instance.GetDrawer(Symbol);
                if (drawer != null)
                {
                    drawer.Draw(g, Symbol, _line, null);
                }
            }
        }

        public override void Draw(IDisplay display)
        {
            if (_line.Count > 0)
            {
                ISymbolDrawer drawer = SymbolDrawerFactory.Instance.GetDrawer(Symbol);
                if (drawer != null)
                {
                    drawer.Draw(display, Symbol, _line, null);
                }
            }
        }

        #region ILineStringTracker 成员

        public bool IsClose
        {
            get
            {
                return _bClose;
            }
            set
            {
                _bClose = value;
            }
        }

        public void AddPoint(double x, double y)
        {
            _line.Add(new NetMap.Geometry.Point(x, y));
        }

        public void ChangeEndPoint(double x, double y)
        {
            IPoint endPoint = _line.EndPoint;
            endPoint.X = x;
            endPoint.Y = y;
        }

        public void BackPoint()
        {
            if (_line.Count > 0)
            {
                _line.RemoveAt(_line.Count - 1);
            }
        }

        #endregion
    }
}
