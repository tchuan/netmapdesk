﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace NetMap.Interfaces
{
    /// <summary>
    /// 文件IO异常
    /// </summary>
    [Serializable]
    public class FileIOException : ApplicationException
    {
        public FileIOException()
            : base()
        {
        }

        public FileIOException(string message)
            : base(message)
        {
        }

        public FileIOException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected FileIOException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
