﻿// Copyright (c) AlphaSierraPapa for the KJSuite Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

using NetMap.Interfaces;
using System;
using System.Windows.Forms;

namespace NetMap.Application.WorkspaceBrowser
{
	public interface IWorkspaceNodeBuilder
	{
        bool CanBuildWorkspaceTree(IWorkspace workspace);
        TreeNode AddWorkspaceNode(TreeNode motherNode, IWorkspace project);
	}
}
