﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Data;
using System.IO;
using System.Windows.Forms;
using NetMap.Data;
using NetMap.Geometry;
using NetMap.Interfaces.Symbol;
using NetMap.Symbol;
using NetMap.Application.Core;
using System.Drawing;
using NetMap.Sqlite;

namespace StoneGround
{
    /// <summary>
    /// 料场的数据服务
    /// </summary>
    public class StoneGroundDatabaseService
    {
        private static StoneGroundDatabaseService _instance;
        public static StoneGroundDatabaseService Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StoneGroundDatabaseService();
                }
                return _instance;
            }
        }

        private StoneGroundDatabaseService()
        {
        }

        private ISpatialDatabase _sdb;
        private IFeatureClass _stoneGroundFeatureClass;
        private ITable _stoneGroundPicTable;  
        private ITable _stoneGroundMachinePicTable; 
        private ISymbol _symbol;
        private ITextSymbol _labelSymbol;

        public const string DB_STONEGROUND = "sg.db";
        public const string TABLE_STONEGROUND = "stoneground";   //料场空间表
        public const string TABLE_SGPIC = "sgpic";               //料场现场情况表
        public const string TABLE_SGMACHINEPIC = "sgmachinepic"; //料场碎石加工设备表
        //料场信息
        public const string FIELD_NAME = "name";
        public const string FIELD_RECORDSTATUS = "recordstatus";
        public const string FIELD_MANAGESTATUS = "managestatus";
        public const string FIELD_ADDRESS = "address";
        public const string FIELD_TEL = "tel";
        public const string FIELD_STRTUMNATURE = "strtumnature";
        public const string FIELD_GEOLOGICAGE = "geologicage";
        public const string FIELD_RESERVES = "reserves";
        public const string FIELD_MININGCONDITIONS = "miningconditions";
        public const string FIELD_TRAFFICCONDITIONS = "trafficconditions";
        public const string FIELD_RECORDTIME = "recordtime";
        //碎石技术指标
        public const string FIELD_DENSITY = "density";
        public const string FIELD_WATERABSORPTION = "waterabsorption";
        public const string FIELD_STURDINESS = "sturdiness";
        public const string FIELD_CRUSHING = "curshing";
        public const string FIELD_POLISH = "polish";
        public const string FIELD_ABRASION = "abrasion";
        public const string FIELD_GILL = "gill";
        public const string FIELD_PITCHADHERE = "pitchadhere";
        public const string FIELD_COMPRESSIVE = "compressive";
        //现场情况，碎石加工设备
        public const string FIELD_SGOID = "sgoid"; //料场oid
        public const string FIELD_PIC = "pic";     //图片
        public const string FIELD_PICNAME = "picname"; //图片名字

        /// <summary>
        /// 初始化
        /// </summary>
        public void Init()
        {
            string dbFile = System.IO.Path.Combine(Application.StartupPath, "map", DB_STONEGROUND);
            IConnectProperties connectProperties = new ConnectProperties();
            connectProperties.Database = dbFile;
            ISpatialDatabaseFactory factory = new SqliteSpatialDatabaseFactory2();
            if (File.Exists(dbFile))
            {
                //打开
                _sdb = factory.Open(connectProperties);
                _stoneGroundFeatureClass = _sdb.OpenFeatureClass(TABLE_STONEGROUND);
                _stoneGroundPicTable = _sdb.OpenTable(TABLE_SGPIC);
                _stoneGroundMachinePicTable = _sdb.OpenTable(TABLE_SGMACHINEPIC);
            }
            else
            {
                //创建
                _sdb = factory.Create(connectProperties);
                CreateSGFeatureClass();
                CreateSGPicTable();
                CreateSGMachinePicTable();
            }
        }

        /// <summary>
        /// 获取料场空间数据库
        /// </summary>
        public ISpatialDatabase StoneGroundSdb
        {
            get
            {
                return _sdb;
            }
        }

        /// <summary>
        /// 获取料场要素集
        /// </summary>
        public IFeatureClass StoneGroundFeatureClass
        {
            get
            {
                return _stoneGroundFeatureClass;
            }
        }

        /// <summary>
        /// 获取料场现场情况表
        /// </summary>
        public ITable StoneGroundPicTable
        {
            get
            {
                return _stoneGroundPicTable;
            }
        }

        /// <summary>
        /// 获取料场碎石加工设备表
        /// </summary>
        public ITable StoneGroundMachinePicTable
        {
            get { return _stoneGroundMachinePicTable; }
        }

        /// <summary>
        /// 获取料场符号
        /// </summary>
        public ISymbol Symbol
        {
            get
            {
                if (_symbol == null)
                {
                    System.Drawing.Bitmap img = (System.Drawing.Bitmap)ResourceService.GetImageResource("mark_red_32");
                    ImagePointSymbol imgSymbol = new ImagePointSymbol(img);
                    imgSymbol.AnchorX = 0.5;
                    imgSymbol.AnchorY = 1;
                    imgSymbol.Size = 32;
                    _symbol = imgSymbol;
                }
                return _symbol;
            }
        }

        /// <summary>
        /// 获取料场标注符号
        /// </summary>
        public ITextSymbol LabelSymbol
        {
            get
            {
                if (_labelSymbol == null)
                {
                    TextSymbol labelSymbol = new TextSymbol();
                    labelSymbol.OffsetX = 0;
                    labelSymbol.OffsetY = 10;
                    labelSymbol.Color = Color.FromArgb(255, 0, 0);
                    _labelSymbol = labelSymbol;
                }
                return _labelSymbol;
            }
        }

        /// <summary>
        /// 关闭
        /// </summary>
        public void Close()
        {
            if (_sdb != null)
            {
                _sdb.Close();
            }
        }

        #region Privates

        //创建料场空间表
        private void CreateSGFeatureClass()
        {
            IFields fields = new Fields();
            int i = 0;
            IField field = new Field(FIELD_NAME, geoFieldType.GEO_STRING);
            fields.SetField(i++, field);
            field = new Field(FIELD_RECORDSTATUS, geoFieldType.GEO_STRING);
            fields.SetField(i++, field);
            field = new Field(FIELD_MANAGESTATUS, geoFieldType.GEO_STRING);
            fields.SetField(i++, field);
            field = new Field(FIELD_ADDRESS, geoFieldType.GEO_STRING);
            fields.SetField(i++, field);
            field = new Field(FIELD_TEL, geoFieldType.GEO_STRING);
            fields.SetField(i++, field);
            field = new Field(FIELD_STRTUMNATURE, geoFieldType.GEO_STRING);
            fields.SetField(i++, field);
            field = new Field(FIELD_GEOLOGICAGE, geoFieldType.GEO_STRING);
            fields.SetField(i++, field);
            field = new Field(FIELD_RESERVES, geoFieldType.GEO_DOUBLE);
            fields.SetField(i++, field);
            field = new Field(FIELD_MININGCONDITIONS, geoFieldType.GEO_STRING);
            fields.SetField(i++, field);
            field = new Field(FIELD_TRAFFICCONDITIONS, geoFieldType.GEO_STRING);
            fields.SetField(i++, field);
            field = new Field(FIELD_RECORDTIME, geoFieldType.GEO_DATE);
            fields.SetField(i++, field);
            field = new Field(FIELD_DENSITY, geoFieldType.GEO_DOUBLE);
            fields.SetField(i++, field);
            field = new Field(FIELD_WATERABSORPTION, geoFieldType.GEO_DOUBLE);
            fields.SetField(i++, field);
            field = new Field(FIELD_STURDINESS, geoFieldType.GEO_DOUBLE);
            fields.SetField(i++, field);
            field = new Field(FIELD_CRUSHING, geoFieldType.GEO_DOUBLE);
            fields.SetField(i++, field);
            field = new Field(FIELD_POLISH, geoFieldType.GEO_DOUBLE);
            fields.SetField(i++, field);
            field = new Field(FIELD_ABRASION, geoFieldType.GEO_DOUBLE);
            fields.SetField(i++, field);
            field = new Field(FIELD_GILL, geoFieldType.GEO_DOUBLE);
            fields.SetField(i++, field);
            field = new Field(FIELD_PITCHADHERE, geoFieldType.GEO_INT);
            fields.SetField(i++, field);
            field = new Field(FIELD_COMPRESSIVE, geoFieldType.GEO_DOUBLE);
            fields.SetField(i++, field);
            _stoneGroundFeatureClass = _sdb.CreateFeatureClass(TABLE_STONEGROUND,
                fields, geoFeatureType.GEO_FEATURE_SIMPLE, NetMap.Interfaces.Geometry.geoGeometryType.GEO_GEOMETRY_POINT,
                new Envelope(97.0646437994724, 20.8970976253298, 106.682058047493, 29.9208443271768),
                null);
        }

        //创建料场现场情况表
        private void CreateSGPicTable()
        {
            IFields fields = new Fields();
            int i = 0;
            IField field = new Field(FIELD_SGOID, geoFieldType.GEO_BIGINT);
            fields.SetField(i++, field);
            field = new Field(FIELD_PIC, geoFieldType.GEO_BLOB);
            fields.SetField(i++, field);
            field = new Field(FIELD_PICNAME, geoFieldType.GEO_STRING);
            fields.SetField(i++, field);
            _stoneGroundPicTable = _sdb.CreateTable(TABLE_SGPIC, fields);
        }

        //创建料场碎石加工设备
        private void CreateSGMachinePicTable()
        {
            IFields fields = new Fields();
            int i = 0;
            IField field = new Field(FIELD_SGOID, geoFieldType.GEO_BIGINT);
            fields.SetField(i++, field);
            field = new Field(FIELD_PIC, geoFieldType.GEO_BLOB);
            fields.SetField(i++, field);
            field = new Field(FIELD_PICNAME, geoFieldType.GEO_STRING);
            fields.SetField(i++, field);
            _stoneGroundMachinePicTable = _sdb.CreateTable(TABLE_SGMACHINEPIC, fields);
        }

        #endregion
    }
}
