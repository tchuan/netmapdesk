﻿using NetMap.Interfaces;
using NetMap.Interfaces.Geometry;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace NetMap
{
    /// <summary>
    /// 地图视图数据
    /// </summary>
    public class MapViewData
    {
        /// <summary>
        /// 获取和设置名字
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 获取和设置背景色
        /// </summary>
        public Color BackColor { get; set; }
        /// <summary>
        /// 获取和设置中心点
        /// </summary>
        public IPoint Center { get; set; }
        /// <summary>
        /// 获取和设置比例尺
        /// </summary>
        public double Scale { get; set; }
        /// <summary>
        /// 获取和设置地图
        /// </summary>
        public IMapDocument Map { get; set; }
    }
}
