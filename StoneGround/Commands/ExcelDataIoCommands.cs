﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Application.Core;
using System.Windows.Forms;
using System.Diagnostics;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Data;
using NetMap.Data;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;

namespace StoneGround.Commands
{
    /// <summary>
    /// 数据导入（Excel）
    /// </summary>
    public class ExcelDataImportCommands : AbstractCommand
    {
        /// <summary>
        /// 料场项
        /// </summary>
        class SGItem
        {
            /// <summary>
            /// 名字
            /// </summary>
            public string Name;
            /// <summary>
            /// 数据
            /// </summary>
            public Dictionary<string, string> Data;
        }
        private List<SGItem> _itemLst = new List<SGItem>();
        private StringBuilder _importLog = new StringBuilder();
        /// <summary>
        /// 导入结果
        /// </summary>
        enum ImportResult
        {
            Cancel,
            Ok,
            Fail
        }

        public override void Run()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "料场数据 (*.xls)|*.xls";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                _itemLst.Clear();
                string file = dialog.FileName;
                try
                {
                    using (System.IO.FileStream fs = System.IO.File.OpenRead(file))
                    {
                        HSSFWorkbook wk = new HSSFWorkbook(fs);
                        for (int i = 0; i < wk.NumberOfSheets; i++)
                        {
                            ISheet sheet = wk.GetSheetAt(i);
                            ImportOne(sheet);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageService.ShowWarning("料场数据导入失败，错误信息：" + ex.Message);
                    return;
                }
                ImportResult result = Import();
                if (result == ImportResult.Ok)
                {
                    NetMap.Application.WorkspaceSingleton.ActiveMapView.Invalidate(); //刷新地图
                    MessageService.ShowMessage(_importLog.ToString(), "提示");
                }
                else if (result == ImportResult.Fail)
                {
                    MessageService.ShowMessage("没有料场被导入，请确认数据格式是否正确或该组料场是否已存在", "提示");
                }
            }
        }

        private void ImportOne(ISheet sheet)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            int count = 11;
            int startRow = 1;
            for (int i = startRow; i < startRow + count; i++)
            {
                NPOI.SS.UserModel.IRow row = sheet.GetRow(i);
                string row1 = row.GetCell(0).ToString();
                string row2 = row.GetCell(1).ToString();
                if (string.IsNullOrEmpty(row1))
                {
                    return;
                }
                data.Add(row1, row2);
            }
            count = 9;
            startRow = 2;
            for (int i = startRow; i < startRow + count; i++)
            {
                NPOI.SS.UserModel.IRow row = sheet.GetRow(i);
                string row1 = row.GetCell(2).ToString();
                string row2 = row.GetCell(3).ToString();
                if (string.IsNullOrEmpty(row1))
                {
                    return;
                }
                data.Add(row1, row2);
            }
            SGItem item = CheckItem(data);
            if(item != null && CheckName(item)) //对象有效和非重复
            {
                _itemLst.Add(item);
            }
        }

        private SGItem CheckItem(Dictionary<string, string> data)
        {
            string key = "料场名称";
            if (data.ContainsKey(key))
            {
                string name = data[key];
                if (string.IsNullOrEmpty(name))
                {
                    return null; //无效料场
                }
                IQueryFilter filter = new QueryFilter();
                filter.WhereClause = StoneGroundDatabaseService.FIELD_NAME + "=\'" + name + "\'";
                IFeatureCursor cursor = StoneGroundDatabaseService.Instance.StoneGroundFeatureClass.Search(filter);
                IFeature feature = cursor.Next();
                cursor.Close();
                if (feature != null)
                {
                    return null; //已存在此料场
                }
                return new SGItem() { Name = name, Data = data };
            }
            else
            {
                return null;
            }
        }

        private ImportResult Import()
        {
            if (_itemLst.Count == 0)
            {
                return ImportResult.Fail;
            }

            List<string> items = new List<string>();
            foreach (SGItem item in _itemLst)
            {
                items.Add(item.Name);
            }
            StoneGround.Dialogs.FrmSGChoice dialog = new Dialogs.FrmSGChoice();
            dialog.SetSGCollection(items);
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                _importLog.Clear();
                List<string> importItemLst = dialog.SelectedSGCollection;
                foreach (SGItem item in _itemLst)
                {
                    if (importItemLst.IndexOf(item.Name) >= 0)
                    {
                        //导入
                        if (CreateFeature(item.Data))
                        {
                            _importLog.Append(item.Name).Append(" 导入成功\r\n");
                        }
                        else
                        {
                            _importLog.Append(item.Name).Append(" 导入失败\r\n");
                        }
                    }
                }
                return ImportResult.Ok;
            }
            else
            {
                return ImportResult.Cancel;
            }
        }

        //创建要素
        //注意：对应的文字是根据excel中来的
        private bool CreateFeature(Dictionary<string, string> data)
        {
            IFeatureClass fc = StoneGroundDatabaseService.Instance.StoneGroundFeatureClass;
            IFeature feature = fc.CreateFeature();
            int index = 0;
            IFields fields = fc.Fields;
            try
            {
                foreach (KeyValuePair<string, string> dataItem in data)
                {
                    switch (dataItem.Key)
                    {
                        #region 料场信息
                        case "料场名称":
                            {
                                index = fields.FindField(StoneGroundDatabaseService.FIELD_NAME);
                                feature.SetValue(index, dataItem.Value);
                                break;
                            }
                        case "联系方式":
                            {
                                index = fields.FindField(StoneGroundDatabaseService.FIELD_TEL);
                                feature.SetValue(index, dataItem.Value);
                                break;
                            }
                        case "料场备案情况":
                            {
                                index = fields.FindField(StoneGroundDatabaseService.FIELD_RECORDSTATUS);
                                feature.SetValue(index, dataItem.Value);
                                break;
                            }
                        case "料场经营状态":
                            {
                                index = fields.FindField(StoneGroundDatabaseService.FIELD_MANAGESTATUS);
                                feature.SetValue(index, dataItem.Value);
                                break;
                            }
                        case "经纬度":
                            {
                                string[] nums = dataItem.Value.Split('；');
                                double lon = double.Parse(nums[0]);
                                double lat = double.Parse(nums[1]);
                                IPoint point = new NetMap.Geometry.Point(lon, lat);
                                feature.Geometry = point;
                                break;
                            }
                        case "岩层性质":
                            {
                                index = fields.FindField(StoneGroundDatabaseService.FIELD_STRTUMNATURE);
                                feature.SetValue(index, dataItem.Value);
                                break;
                            }
                        case "所属地质年代":
                            {
                                index = fields.FindField(StoneGroundDatabaseService.FIELD_GEOLOGICAGE);
                                feature.SetValue(index, dataItem.Value);
                                break;
                            }
                        case "储量（万m³）":
                            {
                                index = fields.FindField(StoneGroundDatabaseService.FIELD_RESERVES);
                                double num = double.Parse(dataItem.Value);
                                feature.SetValue(index, num);
                                break;
                            }
                        case "开采条件":
                            {
                                index = fields.FindField(StoneGroundDatabaseService.FIELD_MININGCONDITIONS);
                                feature.SetValue(index, dataItem.Value);
                                break;
                            }
                        case "交通条件":
                            {
                                index = fields.FindField(StoneGroundDatabaseService.FIELD_TRAFFICCONDITIONS);
                                feature.SetValue(index, dataItem.Value);
                                break;
                            }
                        case "料场信息录入日期":
                            {
                                DateTime value = DateTime.Parse(dataItem.Value);
                                index = fields.FindField(StoneGroundDatabaseService.FIELD_RECORDTIME);
                                feature.SetValue(index, value);
                                break;
                            }
                        #endregion
                        #region 碎石指标
                        case "单轴抗压强度（Mpa）":
                            {
                                double value = double.Parse(dataItem.Value);
                                index = fields.FindField(StoneGroundDatabaseService.FIELD_COMPRESSIVE);
                                feature.SetValue(index, value);
                                break;
                            }
                        case "表观相对密度":
                            {
                                double value = double.Parse(dataItem.Value);
                                index = fields.FindField(StoneGroundDatabaseService.FIELD_DENSITY);
                                feature.SetValue(index, value);
                                break;
                            }
                        case "吸水率（%）":
                            {
                                double value = double.Parse(dataItem.Value);
                                index = fields.FindField(StoneGroundDatabaseService.FIELD_WATERABSORPTION);
                                feature.SetValue(index, value);
                                break;
                            }
                        case "坚固性（%）":
                            {
                                double value = double.Parse(dataItem.Value);
                                index = fields.FindField(StoneGroundDatabaseService.FIELD_STURDINESS);
                                feature.SetValue(index, value);
                                break;
                            }
                        case "压碎值（%）":
                            {
                                double value = double.Parse(dataItem.Value);
                                index = fields.FindField(StoneGroundDatabaseService.FIELD_CRUSHING);
                                feature.SetValue(index, value);
                                break;
                            }
                        case "磨光值（PSV）":
                            {
                                double value = double.Parse(dataItem.Value);
                                index = fields.FindField(StoneGroundDatabaseService.FIELD_POLISH);
                                feature.SetValue(index, value);
                                break;
                            }
                        case "磨耗值（%）":
                            {
                                double value = double.Parse(dataItem.Value);
                                index = fields.FindField(StoneGroundDatabaseService.FIELD_ABRASION);
                                feature.SetValue(index, value);
                                break;
                            }
                        case "针片状（%）":
                            {
                                double value = double.Parse(dataItem.Value);
                                index = fields.FindField(StoneGroundDatabaseService.FIELD_GILL);
                                feature.SetValue(index, value);
                                break;
                            }
                        case "与沥青粘附性等级（级）":
                            {
                                int value = int.Parse(dataItem.Value);
                                index = fields.FindField(StoneGroundDatabaseService.FIELD_PITCHADHERE);
                                feature.SetValue(index, value);
                                break;
                            }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Write(ex);
                return false;
            }
            //TODO 这里先写死地址
            index = fields.FindField(StoneGroundDatabaseService.FIELD_ADDRESS);
            feature.SetValue(index, "");
            //END
            feature.Store();
            return true;
        }

        //检查名字
        private bool CheckName(SGItem item)
        {
            foreach (SGItem sgItem in _itemLst)
            {
                if (sgItem.Name == item.Name)
                {
                    return false;
                }
            }
            return true;
        }
    }

    /// <summary>
    /// 数据导出(Excel)
    /// </summary>
    public class ExcelDataExportCommands : AbstractCommand
    {
        /// <summary>
        /// 料场项
        /// </summary>
        class SGItem
        {
            /// <summary>
            /// 名字
            /// </summary>
            public string Name;
            /// <summary>
            /// 要素
            /// </summary>
            public IFeature Feature;
        }
        private List<SGItem> _itemLst = new List<SGItem>();

        public override void Run()
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "料场数据 (*.xls)|*.xls";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string file = dialog.FileName;
                    UpdateSGCollection();
                    Export(file);
                }
                catch (Exception ex)
                {
                    MessageService.ShowWarning("料场数据导出失败，错误信息：" + ex.Message);
                    return;
                }
            }
        }

        private void UpdateSGCollection()
        {
            //清理
            _itemLst.Clear();
            //查询
            IQueryFilter filter = new QueryFilter();
            GetAllFields(filter);
            IFeatureCursor cursor = StoneGroundDatabaseService.Instance.StoneGroundFeatureClass.Search(filter);
            IFeature feature = cursor.Next();
            while (feature != null)
            {
                SGItem item = new SGItem();
                item.Name = feature.GetValue(0).ToString();
                item.Feature = feature;
                _itemLst.Add(item);
                feature = cursor.Next();
            }
            cursor.Close();
        }

        private void Export(string file)
        {
            List<string> sgItemLst = new List<string>();
            foreach (SGItem item in _itemLst)
            {
                sgItemLst.Add(item.Name);
            }
            StoneGround.Dialogs.FrmSGChoice dialog = new Dialogs.FrmSGChoice();
            dialog.SetSGCollection(sgItemLst);
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                List<string> exportItemLst = dialog.SelectedSGCollection;
                _itemLst.RemoveAll(sgItemTemp =>
                    {
                        return exportItemLst.IndexOf(sgItemTemp.Name) < 0;
                    });
                string templateFile = System.IO.Path.Combine(Application.StartupPath, "template\\料场信息模板.xls");
                using (System.IO.FileStream fs = System.IO.File.OpenRead(templateFile))
                {
                    HSSFWorkbook wk = new HSSFWorkbook(fs);
                    //复制n-1的sheet页
                    for (int i = 0; i < _itemLst.Count - 1; i++)
                    {
                        wk.CloneSheet(0);
                    }                    
                    //导出
                    Dictionary<string, int> nameDic = new Dictionary<string, int>();
                    for (int i = 0; i < _itemLst.Count; i++)
                    {
                        SGItem item = _itemLst[i];
                        int itemIndex = 0;
                        if(nameDic.Keys.Contains(item.Name))
                        {
                            itemIndex++;
                            nameDic[item.Name] = itemIndex;
                        }
                        else
                        {
                            nameDic.Add(item.Name, itemIndex);
                        }
                        if (itemIndex == 0) //没有重名
                        {
                            wk.SetSheetName(i, item.Name);
                        }
                        else //重名
                        {
                            wk.SetSheetName(i, item.Name + "-" + itemIndex);
                        }
                        ExportItem(_itemLst[i], wk.GetSheetAt(i));
                    }
                    using (System.IO.FileStream fs1 = System.IO.File.OpenWrite(file))
                    {
                        wk.Write(fs1);
                    }                    
                }
                MessageService.ShowMessage("导出成功", "提示");
            }
        }

        private void ExportItem(SGItem item, ISheet sheet)
        {
            IFields fields = item.Feature.Fields;
            int index = fields.FindField(StoneGroundDatabaseService.FIELD_NAME);
            SetCellValue(sheet, 1, 1, item.Feature.GetValue(index).ToString());
            index = fields.FindField(StoneGroundDatabaseService.FIELD_TEL);
            SetCellValue(sheet, 2, 1, item.Feature.GetValue(index).ToString());
            index = fields.FindField(StoneGroundDatabaseService.FIELD_RECORDSTATUS);
            SetCellValue(sheet, 3, 1, item.Feature.GetValue(index).ToString());
            index = fields.FindField(StoneGroundDatabaseService.FIELD_MANAGESTATUS);
            SetCellValue(sheet, 4, 1, item.Feature.GetValue(index).ToString());
            //经纬度
            IPoint point = item.Feature.Geometry as IPoint;
            string xy = string.Format("{0:f}；{1:f}", point.X, point.Y);
            SetCellValue(sheet, 5, 1, xy);
            //岩层性质
            index = fields.FindField(StoneGroundDatabaseService.FIELD_STRTUMNATURE);
            SetCellValue(sheet, 6, 1, item.Feature.GetValue(index).ToString());
            //所属地质年代
            index = fields.FindField(StoneGroundDatabaseService.FIELD_GEOLOGICAGE);
            SetCellValue(sheet, 7, 1, item.Feature.GetValue(index).ToString());
            //储量
            index = fields.FindField(StoneGroundDatabaseService.FIELD_RESERVES);
            SetCellValue(sheet, 8, 1, item.Feature.GetValue(index).ToString());
            //开采条件
            index = fields.FindField(StoneGroundDatabaseService.FIELD_MININGCONDITIONS);
            SetCellValue(sheet, 9, 1, item.Feature.GetValue(index).ToString());
            //交通条件
            index = fields.FindField(StoneGroundDatabaseService.FIELD_TRAFFICCONDITIONS);
            SetCellValue(sheet, 10, 1, item.Feature.GetValue(index).ToString());
            //料场信息录入日期
            index = fields.FindField(StoneGroundDatabaseService.FIELD_RECORDTIME);
            SetCellValue(sheet, 11, 1, ((DateTime)item.Feature.GetValue(index)).ToString("yyyy-MM-dd HH:mm:ss"));

            //单轴抗压强度
            index = fields.FindField(StoneGroundDatabaseService.FIELD_COMPRESSIVE);
            SetCellValue(sheet, 2, 3, item.Feature.GetValue(index).ToString());
            //表观相对密度
            index = fields.FindField(StoneGroundDatabaseService.FIELD_DENSITY);
            SetCellValue(sheet, 3, 3, item.Feature.GetValue(index).ToString());
            //吸水率
            index = fields.FindField(StoneGroundDatabaseService.FIELD_WATERABSORPTION);
            SetCellValue(sheet, 4, 3, item.Feature.GetValue(index).ToString());
            //坚固性
            index = fields.FindField(StoneGroundDatabaseService.FIELD_STURDINESS);
            SetCellValue(sheet, 5, 3, item.Feature.GetValue(index).ToString());
            //压碎值
            index = fields.FindField(StoneGroundDatabaseService.FIELD_CRUSHING);
            SetCellValue(sheet, 6, 3, item.Feature.GetValue(index).ToString());
            //磨光值
            index = fields.FindField(StoneGroundDatabaseService.FIELD_POLISH);
            SetCellValue(sheet, 7, 3, item.Feature.GetValue(index).ToString());
            //磨耗值
            index = fields.FindField(StoneGroundDatabaseService.FIELD_ABRASION);
            SetCellValue(sheet, 8, 3, item.Feature.GetValue(index).ToString());
            //针片状
            index = fields.FindField(StoneGroundDatabaseService.FIELD_GILL);
            SetCellValue(sheet, 9, 3, item.Feature.GetValue(index).ToString());
            //与沥青粘附性等级
            index = fields.FindField(StoneGroundDatabaseService.FIELD_PITCHADHERE);
            SetCellValue(sheet, 10, 3, item.Feature.GetValue(index).ToString());
        }

        //设置单元格的值
        private void SetCellValue(ISheet sheet, int row, int col, string value)
        {
            NPOI.SS.UserModel.IRow sheetRow = sheet.GetRow(row);
            ICell cell = sheetRow.GetCell(col);
            cell.SetCellValue(value);
        }

        private void GetAllFields(IQueryFilter filter)
        {
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_NAME);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_RECORDSTATUS);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_MANAGESTATUS);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_ADDRESS);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_TEL);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_STRTUMNATURE);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_GEOLOGICAGE);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_RESERVES);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_MININGCONDITIONS);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_TRAFFICCONDITIONS);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_RECORDTIME);

            filter.Fields.Add(StoneGroundDatabaseService.FIELD_DENSITY);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_WATERABSORPTION);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_STURDINESS);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_CRUSHING);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_POLISH);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_ABRASION);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_GILL);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_PITCHADHERE);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_COMPRESSIVE);
        }
    }
}
