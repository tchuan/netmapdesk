﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Forms;

namespace NetMap.UI
{
    partial class MapCtrl
    {
        /// <summary>
        /// 是否在设计器中
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public bool IsDesignerHosted
        {
            get
            {
                return IsControlDesignerHosted(this);
            }
        }

        /// <summary>
        /// 是否在设计器中
        /// </summary>
        /// <param name="ctrl"></param>
        /// <returns></returns>
        public bool IsControlDesignerHosted(Control ctrl)
        {
            if (ctrl != null)
            {
                if (ctrl.Site != null)
                {

                    if (ctrl.Site.DesignMode == true)
                        return true;

                    else
                    {
                        if (IsControlDesignerHosted(ctrl.Parent))
                            return true;

                        else
                            return false;
                    }
                }
                else
                {
                    if (IsControlDesignerHosted(ctrl.Parent))
                        return true;
                    else
                        return false;
                }
            }
            else
                return false;
        }
    }
}
