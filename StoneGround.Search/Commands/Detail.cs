﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Application.Core;
using StoneGround.Search.TreeNodes;
using System.Windows.Forms;

namespace StoneGround.Search.Commands
{
    /// <summary>
    /// 详细信息
    /// </summary>
    public class Detail : AbstractMenuCommand
    {
        public override void Run()
        {
            SearchPad search = SearchPad.Instance;
            TreeNode node = search.StoneGroundTreeView.SelectedNode;
            if (node != null)
            {
                StoneGroundNode sgNode = node as StoneGroundNode;
                sgNode.Detail();
            }
        }
    }
}
