﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;
using System.Drawing;
using System.IO;

namespace NetMap.Data
{
    /// <summary>
    /// 带空间信息的影像
    /// </summary>
    public class SpaceImage : IDisposable
    {
        /// <summary>
        /// 获取和设置影像数据
        /// </summary>
        public Image Image { get; set; }
        /// <summary>
        /// 获取和设置影像范围
        /// </summary>
        public IEnvelope Bounds { get; set; }
        /// <summary>
        /// 获取和设置获取时间
        /// </summary>
        public long TickFetched { get; private set; }

        public SpaceImage(byte[] data, IEnvelope box)
        {
            using (MemoryStream stream = new MemoryStream(data))
            {
                Image = Image.FromStream(stream);
            }
            Bounds = box;
            TickFetched = DateTime.Now.Ticks;
        }

        public void Dispose()
        {
            Image.Dispose();
        }
    }
}
