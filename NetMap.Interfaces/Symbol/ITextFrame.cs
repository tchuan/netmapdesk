﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Symbol
{
    /// <summary>
    /// 段落文本符号接口
    /// </summary>
    public interface ITextFrame : ITextSymbol
    {
        /// <summary>
        /// 文本显示的水平间距
        /// </summary>
        double HorizonExtra { get; set; }
        /// <summary>
        /// 文本显示的垂直间距
        /// </summary>
        double VerticalExtra { get; set; }
    }
}
