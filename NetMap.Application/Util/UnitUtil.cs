﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Application.Util
{
    /// <summary>
    /// 单位工具
    /// </summary>
    public static class UnitUtil
    {
        /// <summary>
        /// 1英寸=25.4mm
        /// </summary>
        public const double INCH_PER_MM = 25.4;
    }
}
