﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Tracker;
using NetMap.Interfaces;
using NetMap.Interfaces.Tracker;
using System.Windows.Forms;
using System.Drawing;
using NetMap.Interfaces.Data;
using NetMap.Symbol;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Geometry;
using NetMap.Geometry;
using NetMap.Render;
using NetMap.Interfaces.Symbol;

/*
 * 2014-07-10:  支持绘制多段线
 */

namespace NetMap.UI.Tools.Draw
{
    /// <summary>
    /// 直线绘制工具
    /// 备注：左键绘制、右键平移
    /// </summary>
    public class LinePen : Tool
    {
        private bool _isStart = false;
        private bool _isFirst = true;
        private CreateLineStringTracker _tracker;
        private int _panX = 0, _panY = 0;
        private bool _isPan = false;
        private IMultiLineString _mline = new MultiLineString();

        public LinePen()
            : base("绘线")
        {
        }

        public LinePen(IMapEditView editView)
            : base("绘线", editView)
        {
        }

        protected override void SetCursor()
        {
            //base.SetCursor();
            _cursor = Cursors.Arrow;
        }

        /// <summary>
        /// 获取绘制的多线段
        /// </summary>
        public IMultiLineString MultiLineString
        {
            get
            {
                return _mline;
            }
        }

        public override void Init()
        {
            base.Init();
            StrokeSymbol symbol = new StrokeSymbol();
            symbol.Pen.Width = 2;
            symbol.Pen.Color = Color.FromArgb(255, 0, 255);
            _tracker = new CreateLineStringTracker(symbol);
        }

        public override void GainFocus()
        {
            base.GainFocus();
            _isFirst = true;
            _isStart = false;
            _panX = 0;
            _panY = 0;
            _isPan = false;
            _mline = new MultiLineString();
            _tracker.Reset();
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            //base.OnMouseDown(e);
            switch (e.Button)
            {
                case MouseButtons.Left:
                    {
                        IDisplayTransformation trans = View.DisplayTransformation;
                        double x = 0, y = 0;
                        trans.ToMapPoint(e.X, e.Y, ref x, ref y);
                        _tracker.AddPoint(x, y);
                        if (_isFirst)
                        {
                            _tracker.AddPoint(x, y);
                            _isFirst = false;
                            _isStart = true;
                        }
                        View.Invalidate();
                        break;
                    }
                case MouseButtons.Middle:
                    {
                        if (_isPan) 
                            return;
                        _isPan = true;
                        _panX = e.X;
                        _panY = e.Y;
                        break;
                    }
                case MouseButtons.Right:
                    {
                        //保存
                        _tracker.BackPoint();
                        _tracker.Finish();
                        ILineString line = _tracker.GetGeometry() as ILineString;
                        if (line.Count > 1)
                        {
                            _mline.Add(line);
                        }
                        _tracker.Reset();
                        _isFirst = true;
                        _isStart = false;
                        break;
                    }
            }
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            if (_isPan)
            {
                IDisplayTransformation trans = View.DisplayTransformation;
                double xMapOff = (e.X - _panX) / trans.ScaleRatio;
                double yMapOff = (e.Y - _panY) / trans.ScaleRatio;
                IPoint center = trans.VisibleMapBounds.Center;
                View.ZoomToCenter(center.X - xMapOff, center.Y + yMapOff, trans.ScaleRatio);
                _panX = e.X;
                _panY = e.Y;
                View.Invalidate();
            }
            else
            {
                if (_isStart)
                {
                    IDisplayTransformation trans = View.DisplayTransformation;
                    double x = 0, y = 0;
                    trans.ToMapPoint(e.X, e.Y, ref x, ref y);
                    _tracker.ChangeEndPoint(x, y);
                    View.Invalidate();
                }
            }
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    {
                        break;
                    }
                case MouseButtons.Middle:
                    {
                        if (!_isPan)
                            return;
                        _isPan = false;
                        IDisplayTransformation trans = View.DisplayTransformation;                        
                        double xMapOff = (e.X - _panX) / trans.ScaleRatio;
                        double yMapOff = (e.Y - _panY) / trans.ScaleRatio;
                        IPoint center = trans.VisibleMapBounds.Center;
                        View.ZoomToCenter(center.X - xMapOff, center.Y + yMapOff, trans.ScaleRatio);
                        View.Invalidate();
                        break;
                    }
                case MouseButtons.Right:
                    {
                        break;
                    }
            }
        }

        //delete、backspace键后退一点
        //esc重置
        //n键新多段线
        public override void OnKeyDown(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Back:
                case Keys.Delete:
                    {
                        _tracker.BackPoint();
                        View.Invalidate();
                        break;
                    }
                case Keys.Escape:
                    {
                        _tracker.Reset();
                        View.Invalidate();
                        break;
                    }
                case Keys.N:
                    {
                        _tracker.BackPoint();
                        _tracker.Finish();
                        ILineString line = _tracker.GetGeometry() as ILineString;
                        if (line.Count > 1)
                        {
                            _mline.Add(line);
                        }
                        _tracker.Reset();
                        _isFirst = true;
                        _isStart = false;
                        break;
                    }
            }
        }

        public override void Draw(Graphics g)
        {
            //base.Draw(g);
            IDisplay display = View.Display;
            //绘制MultiLineString
            if (_mline.Count > 0)
            {
                ISymbol symbol = _tracker.Symbol;
                ISymbolDrawer drawer = SymbolDrawerFactory.Instance.GetDrawer(symbol);
                if (drawer != null)
                {
                    drawer.Draw(display, symbol, _mline, null);
                }
            }
            //绘制Tracker
            _tracker.Draw(display);
        }
    }
}
