﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjNet.CoordinateSystems;
using NetMap.Interfaces.Geometry;

namespace NetMap.Interfaces.Data
{
    public interface ISpatialDatabase : IDatabase
    {
        /// <summary>
        /// 新建空间表
        /// </summary>
        /// <param name="name">表名</param>
        /// <param name="fields">字段信息</param>
        /// <param name="fType">要素类型</param>
        /// <param name="gType">图形类型</param>
        /// <param name="extent">范围</param>
        /// <param name="coordinateSystem">坐标参考</param>
        /// <returns></returns>
        IFeatureClass CreateFeatureClass(string name, IFields fields,
            geoFeatureType fType, geoGeometryType gType, IEnvelope extent, ICoordinateSystem coordinateSystem);
        /// <summary>
        /// 打开空间表
        /// </summary>
        /// <param name="name">表名</param>
        /// <returns></returns>
        IFeatureClass OpenFeatureClass(string name);
        /// <summary>
        /// 开始编辑
        /// </summary>
        void StartEdit();
        /// <summary>
        /// 结束编辑
        /// </summary>
        void EndEdit();
        /// <summary>
        /// 获取是否在编辑中
        /// </summary>
        /// <returns></returns>
        bool IsEditing { get; }
    }
}
