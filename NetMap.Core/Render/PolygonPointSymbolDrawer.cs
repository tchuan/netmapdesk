﻿using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Symbol;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;

namespace NetMap.Render
{
    /// <summary>
    /// 多边形点符号绘制
    /// </summary>
    public class PolygonPointSymbolDrawer : ISymbolDrawer
    {
        #region ISymbolDrawer 成员

        public void Draw(IDisplay display, ISymbol symbol, IGeometry geometry, string text)
        {
            switch (geometry.GeometryType)
            {
                case geoGeometryType.GEO_GEOMETRY_POINT:
                case geoGeometryType.GEO_GEOMETRY_PATHPOINT:
                    {
                        InnerDraw(display, symbol as IPolygonPointSymbol, geometry as IPoint);
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_DIRECTIONPOINT:
                    {
                        IDirectionPoint point = geometry as IDirectionPoint;
                        InnerDrawDirectionPoint(display, symbol as IPolygonPointSymbol, point);
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTIPOINT:
                    {
                        IPolygonPointSymbol pointSymbol = symbol as IPolygonPointSymbol;
                        IMultiPoint pts = geometry as IMultiPoint;
                        foreach (IPoint point in pts)
                        {
                            InnerDraw(display, pointSymbol, point);
                        }
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_LINESTRING:
                case geoGeometryType.GEO_GEOMETRY_POLYGON:
                case geoGeometryType.GEO_GEOMETRY_MULTILINESTRING:
                case geoGeometryType.GEO_GEOMETRY_MULTIPOLYGON:
                case geoGeometryType.GEO_GEOMETRY_PATH:
                case geoGeometryType.GEO_GEOMETRY_RECT:
                    {
                        //点符号不支持直接表现这些图形
                        //如果希望点符号关联到这些图形，先利用这些图形运算到点后，再进行点符号的绘制
                        break;
                    }                
            }
        }

        public void Draw(Graphics g, ISymbol symbol, IGeometry geometry, string text)
        {
            switch (geometry.GeometryType)
            {
                case geoGeometryType.GEO_GEOMETRY_POINT:
                case geoGeometryType.GEO_GEOMETRY_PATHPOINT:
                    {
                        IPoint point = geometry as IPoint;
                        DirectlyDraw(g, symbol as IPolygonPointSymbol, point.X, point.Y, 0);
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTIPOINT:
                    {
                        IPolygonPointSymbol pointSymbol = symbol as IPolygonPointSymbol;
                        IMultiPoint pts = geometry as IMultiPoint;
                        foreach (IPoint point in pts)
                        {
                            DirectlyDraw(g, pointSymbol, point.X, point.Y, 0);
                        }
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_DIRECTIONPOINT:
                    {
                        IDirectionPoint point = geometry as IDirectionPoint;
                        DirectlyDraw(g, symbol as IPolygonPointSymbol, point.X, point.Y, point.Angle);
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_LINESTRING:
                case geoGeometryType.GEO_GEOMETRY_POLYGON:
                case geoGeometryType.GEO_GEOMETRY_MULTILINESTRING:
                case geoGeometryType.GEO_GEOMETRY_MULTIPOLYGON:
                case geoGeometryType.GEO_GEOMETRY_PATH:
                case geoGeometryType.GEO_GEOMETRY_RECT:
                    {
                        //点符号不支持直接表现这些图形
                        //如果希望点符号关联到这些图形，先利用这些图形运算到点后，再进行点符号的绘制
                        break;
                    }                
            }
        }

        #endregion

        //绘制点
        private void InnerDraw(IDisplay display, IPolygonPointSymbol symbol, IPoint point)
        {
            double x = 0, y = 0;
            display.DisplayTransformation.FromMapPoint(point.X, point.Y, ref x, ref y);
            DirectlyDraw(display.G, symbol, x, y, 0);
        }

        //绘制有向点
        private void InnerDrawDirectionPoint(IDisplay display, IPolygonPointSymbol symbol, IDirectionPoint point)
        {
            double x = 0, y = 0;
            display.DisplayTransformation.FromMapPoint(point.X, point.Y, ref x, ref y);
            DirectlyDraw(display.G, symbol, x, y, point.Angle);
        }

        //绘制
        private void DirectlyDraw(Graphics g, IPolygonPointSymbol symbol, double x, double y, double angle)
        {
            IPolygon polygon = symbol.Polygon;
            GraphicsPath gpath = new GraphicsPath();
            gpath.AddPolygon(Convert(polygon.ExteriorRing));
            foreach (ILineString line in polygon.InteriorRings)
            {
                gpath.AddPolygon(Convert(line));
            }

            Matrix oldMatrix = g.Transform;
            Matrix matrix = new Matrix();
            matrix.Translate((float)x, (float)y);
            matrix.Rotate((float)(360 - symbol.Rotate - angle));
            matrix.Scale((float)symbol.Size, (float)symbol.Size);
            g.Transform = matrix;
            if (symbol.Fill)
            {
                Brush brush = new SolidBrush(symbol.FillColor);
                g.FillPath(brush, gpath);
                brush.Dispose();
            }
            if (symbol.Outline)
            {
                Pen pen = new Pen(symbol.Color, symbol.Width);
                g.DrawPath(pen, gpath);
                pen.Dispose();
            }
            g.Transform = oldMatrix;
            gpath.Dispose();
        }

        private PointF[] Convert(ILineString line)
        {
            List<PointF> pts = new List<PointF>();
            foreach (IPoint point in line)
            {
                pts.Add(new PointF((float)(point.X), (float)(point.Y)));
            }
            return pts.ToArray();
        }
    }
}
