﻿// Copyright 2008 - Paul den Dulk (Geodan)
// 
// This file is part of Mapsui.
// Mapsui is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// Mapsui is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Lesser General Public License
// along with Mapsui; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 

using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Data;
using NetMap.Data;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Tile;
using NetMap.Tile;
using NetMap.Render;
using NetMap.Interfaces.Fetcher;
using System.Drawing;
using NetMap.Geometry;
using NetMap.Utilities;

/*
 * 2013-6-9：修改加入NetMap.Core
 */

namespace NetMap.Fetcher
{
    /// <summary>
    /// 瓦片获取，适合Web和混合模式
    /// </summary>
    class TileFetcher : ITileFetcher
    {
        #region Fields

        private readonly MemoryCache<SpaceImage> memoryCache;
        private readonly ITileSource tileSource;
        private IDisplayTransformation trans;
        private readonly IList<TileIndex> tilesInProgress = new List<TileIndex>();
        private IList<TileInfo> missingTiles = new List<TileInfo>();
        private readonly IDictionary<TileIndex, int> retries = new Dictionary<TileIndex, int>();
        private const int ThreadMax = 2;
        private int threadCount;
        private readonly AutoResetEvent waitHandle = new AutoResetEvent(true);
        private readonly IFetchStrategy strategy = new FetchStrategy();
        private readonly int maxRetries = 2;
        private volatile bool isThreadRunning;
        private volatile bool isViewChanged;
        
        #endregion

        #region EventHandlers

        public event DataChangedEventHandler DataChanged;

        #endregion

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="tileSource">瓦片数据源</param>
        /// <param name="memoryCache">瓦片要素缓存</param>
        /// <param name="maxRetries">最大重试次数</param>
        public TileFetcher(ITileSource tileSource, MemoryCache<SpaceImage> memoryCache,            
            int maxRetries = 2)
        {
            if (tileSource == null) 
                throw new ArgumentException("TileProvider不能为空");
            this.tileSource = tileSource;

            if (memoryCache == null) 
                throw new ArgumentException("MemoryCache不能为空");
            this.memoryCache = memoryCache;

            this.maxRetries = maxRetries;
        }

        #region Public Methods

        public void ViewChanged(IDisplayTransformation trans)
        {
            this.trans = new DisplayTransformation(trans);    
            isViewChanged = true;
            waitHandle.Set();
            if (!isThreadRunning) // 启动TileFetchLoop
            {
                isThreadRunning = true;
                ThreadPool.QueueUserWorkItem(TileFetchLoop);
            }
        }

        public void AbortFetch()
        {
            isThreadRunning = false;
            waitHandle.Set();
        }

        #endregion
        
        #region Private Methods

        private void TileFetchLoop(object state)
        {
            try
            {
                while (isThreadRunning)
                {
                    waitHandle.WaitOne();

                    if (isViewChanged)
                    {
                        ITileSchema schema = tileSource.Schema;
                        string level = ZoomHelper.GetNearestLevel(schema.Resolutions, 1.0 / trans.ScaleRatio);
                        missingTiles = strategy.GetTilesWanted(tileSource.Schema, trans.VisibleMapBounds, level);
                        retries.Clear();
                        isViewChanged = false;
                    }

                    missingTiles = GetTilesMissing(missingTiles, memoryCache, retries, maxRetries);

                    if (missingTiles.Count == 0) 
                    { 
                        waitHandle.Reset();
                        continue;
                    }                    

                    FetchTiles();                    

                    if (threadCount >= ThreadMax) { waitHandle.Reset(); }
                }
            }
            finally
            {
                isThreadRunning = false;
            }
        }

        private IList<TileInfo> GetTilesMissing(IEnumerable<TileInfo> infosIn, MemoryCache<SpaceImage> memoryCache, 
            IDictionary<TileIndex, int> retries, int maxRetries)
        {
            IList<TileInfo> tilesOut = new List<TileInfo>();
            foreach (TileInfo info in infosIn)
            {
                if ((memoryCache.Find(info.Index) == null) &&
                    ((!retries.Keys.Contains(info.Index)) || retries[info.Index] < maxRetries))

                    tilesOut.Add(info);
            }
            return tilesOut;
        }

        private void FetchTiles()
        {
            foreach (TileInfo info in missingTiles)
            {
                if (threadCount >= ThreadMax) return;
                FetchTile(info);
            }
        }

        private void FetchTile(TileInfo info)
        {
            //first a number of checks
            if (tilesInProgress.Contains(info.Index)) return;
            if (retries.Keys.Contains(info.Index) && retries[info.Index] >= maxRetries) return;
            if (memoryCache.Find(info.Index) != null) return;

            //now we can go for the request.
            lock (tilesInProgress) 
            { 
                tilesInProgress.Add(info.Index); 
            }
            if (!retries.Keys.Contains(info.Index))
            {
                retries.Add(info.Index, 0);
            }
            else
            {
                retries[info.Index]++;
            }
            threadCount++;
            // 在线程内下载瓦片
            var fetchOnThread = new FetchOnThread(tileSource.Provider, info, LocalFetchCompleted);
            ThreadPool.QueueUserWorkItem(fetchOnThread.FetchTile);
        }

        private void LocalFetchCompleted(object sender, FetchTileCompletedEventArgs e)
        {
            SpaceImage image = null;
            if (e.Cancelled == false && isThreadRunning && e.Image != null)
            {
                image = new SpaceImage(e.Image, new Envelope(e.TileInfo.Extent));
                memoryCache.Add(e.TileInfo.Index, image);
            }

            threadCount--;
            lock (tilesInProgress)
            {
                if (tilesInProgress.Contains(e.TileInfo.Index))
                    tilesInProgress.Remove(e.TileInfo.Index);
            }
            waitHandle.Set();
            if (DataChanged != null)
            {
                DataChanged(new DataChangedEventArgs(e.Cancelled, -1, e.TileInfo.Extent));
            }
        }

        #endregion
    }
}
