﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Data;
using NetMap.Interfaces.Render;

namespace NetMap.Interfaces
{
    /// <summary>
    /// 要素图层的表现接口
    /// </summary>
    public interface IDisplayLayer
    {
        /// <summary>
        /// 要素渲染对象
        /// </summary>
        IFeatureRender Render { get; set; }
    }
}
