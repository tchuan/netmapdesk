﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StoneGround.Search
{
    /// <summary>
    /// 查询条件
    /// </summary>
    public class SearchCondition
    {
        //是否有地级区划限制
        public bool IsDzone { get; set; }
        public string Dzone { get; set; }

        /// <summary>
        /// 是否有县级区划限制
        /// </summary>
        public bool IsZone { get; set; }
        public string Zone { get; set; }

        /// <summary>
        /// 是否有道路限制
        /// </summary>
        public bool IsRoad { get; set; }
        public string Road { get; set; }

        /// <summary>
        /// 是否有名称限制
        /// </summary>
        public bool IsName { get; set; }
        public string Name { get; set; }

        public void Clear()
        {
            IsDzone = false;
            IsZone = false;
            IsRoad = false;
            IsName = false;
            Dzone = "";
            Zone = "";
            Road = "";
            Name = "";
        }

        public SearchCondition()
        {
            IsDzone = false;
            IsZone = false;
            IsRoad = false;
            IsName = false;
            Dzone = "";
            Zone = "";
            Road = "";
            Name = "";
        }
    }
}
