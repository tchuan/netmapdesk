﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;

namespace NetMap.Interfaces.Tracker
{
    /// <summary>
    /// 创建多段线的橡皮条接口
    /// </summary>
    public interface ICreateLineStringTracker
    {
        /// <summary>
        /// 是否封闭
        /// </summary>
        /// <returns></returns>
        bool IsClose { get; set; }
        /// <summary>
        /// 加一个点
        /// </summary>
        /// <param name="point"></param>
        void AddPoint(double x, double y);
        /// <summary>
        /// 更改结束点
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        void ChangeEndPoint(double x, double y);
        /// <summary>
        /// 后退一点
        /// </summary>
        void BackPoint();        
    }
}
