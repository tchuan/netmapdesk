﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Symbol;
using NetMap.Symbol;
using System.Drawing;

namespace NetMap.Symbol
{
    public class OnEdgePointSymbol : PointSymbol, IOnEdgePointSymbol
    {
        private geoOnEdgeType _onEdgeType;
        public geoOnEdgeType OnEdgeType
        {
            get
            {
                return _onEdgeType;
            }
            set
            {
                _onEdgeType=value;
            }
        }
        private int _referenceValue;
        public int ReferenceValue
        {
            get
            {
                return _referenceValue;
            }
            set
            {
                _referenceValue=value;
            }
        }

        public override ISymbol Clone()
        {
            throw new NotImplementedException();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override string KindAbstract
        {
            get { throw new NotImplementedException(); }
        }

        public override SymbolType SymbolType
        {
            get
            {
                return base.SymbolType;
            }
        }
        private Pen _pen = new Pen(Color.Black, 1);
        public Pen Pen
        {
            get
            {
                return _pen;
            }
            set
            {
                _pen=value;
            }
        }
        private Color _fill = Color.FromArgb(255, 0, 0, 0);
        public Color Fill
        {
            get
            {
                return _fill;
            }
            set
            {
                _fill=value;
            }
        }
    }
}
