﻿// Copyright (c) AlphaSierraPapa for the KJSuite Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

using System;
using System.IO;
using System.Windows.Forms;

using NetMap.Application.Core;
using NetMap.Interfaces;

namespace NetMap.Application.WorkspaceBrowser
{
	public class DefaultWorkspaceNodeBuilder : IWorkspaceNodeBuilder
	{
        public bool CanBuildWorkspaceTree(IWorkspace workspace)
		{
			return true;
		}

        public TreeNode AddWorkspaceNode(TreeNode motherNode, IWorkspace workspace)
		{
            //TDDO
            return null;
		}
	}
}
