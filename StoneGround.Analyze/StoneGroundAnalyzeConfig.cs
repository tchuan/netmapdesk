﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StoneGround.Analyze
{
    /// <summary>
    /// 料场分析设置
    /// </summary>
    public class StoneGroundAnalyzeConfig
    {
        /// <summary>
        /// 服务半径默认为50km(只是大概的常规值，因为考虑运费问题，50公里外运费就不划算了)
        /// </summary>
        public static double ServiceRadius = 50 * 1000;
    }
}
