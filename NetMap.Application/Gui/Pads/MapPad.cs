﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.UI;
using System.Windows.Forms;
using NetMap.Interfaces;
using NetMap.Application.Core.WinForms;
using NetMap.Application.Util;
using NetMap.Application.Core;

namespace NetMap.Application.Gui.Pads
{
    /// <summary>
    /// 地图面板
    /// </summary>
    public class MapPad : AbstractPadContent
    {
        private MapCtrl2 _mapCtrl;
        private IMapView _view;

        public MapPad()
        {
            _mapCtrl = new MapCtrl2();
            _view = _mapCtrl;
            //2014-10-30 地图中加入右键菜单
            string path = "/Application/Pads/MapPad/ContextMenu";
            _mapCtrl.ContextMenuStrip = MenuService.CreateContextMenu(this, path);
            //end
            _view.Status = WorkbenchSingleton.StatusBar as IStatus;
            //2014-5-30 当前就一个地图,设置为当前地图
            WorkspaceSingleton.ActiveMapView = _view;
            //end
            IMapViewEvent viewEvent = _view as IMapViewEvent;
            viewEvent.MapViewScaleChanged += new MapViewScaleChangedHandler(viewEvent_MapViewScaleChanged);
            _mapCtrl.MouseClick += new MouseEventHandler(_mapCtrl_MouseClick);
            Autostart();
        }

        public override object Control => _mapCtrl;

        public override object InitiallyFocusedControl => _mapCtrl;

        void viewEvent_MapViewScaleChanged(double scale)
        {
            IStatus status = WorkbenchSingleton.StatusBar as IStatus;
            double wmScaleDenominator = WebMercatorUtil.Instance.GetWebMercatorScaleDenominator(scale);
            status.ShowMapScale(1 / wmScaleDenominator);
        }

        void _mapCtrl_MouseClick(object sender, MouseEventArgs e)
        {
            _mapCtrl.Focus();
        }

        private void Autostart()
        {
            LoggingService.Info("Running map autostart commands...");
            string path = "/Application/Pads/MapPad/Autostart";
            foreach (NetMap.Application.Core.ICommand command in AddInTree.BuildItems<NetMap.Application.Core.ICommand>(path, null, false))
            {
                try
                {
                    command.Run();
                }
                catch (Exception ex)
                {
                    // allow startup to continue if some commands fail
                    MessageService.ShowException(ex);
                }
            }
        }
    }
}
