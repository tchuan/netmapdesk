﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Text;
using System.Diagnostics;

namespace NetMap.UI.Ctrl
{
    internal delegate void FontSelectedHandler(FontFamily ff, char chr);
    // 字体和字符选择窗体
    internal partial class FontSelectForm : Form
    {
        private StringFormat _sf = null;
        private Brush _fontBrush = Brushes.Black;
        private FontFamily _selectedFF = null;
        private Image _chrsImg = new Bitmap(246, 246);
        private Color _chrsImgBackColor = Color.White;
        private char _selectedChr = 'a';                      // 选择到的字符
        public event FontSelectedHandler FontSelected;        // 选中完成事件

        public FontSelectForm()
        {
            _sf = new StringFormat();
            _sf.Alignment = StringAlignment.Center;
            _sf.LineAlignment = StringAlignment.Center;
            InitializeComponent();
            // 设置文本居中
            DataGridViewCellStyle cellstyle = new DataGridViewCellStyle();
            cellstyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            foreach (DataGridViewColumn column in dataGridViewChrs.Columns)
            {
                column.DefaultCellStyle = cellstyle;
            }
            dataGridViewChrs.RowTemplate.DefaultCellStyle = cellstyle;
            // 加载数据
            InitFonts();
            InitChrs();
            // 事件处理
            comboBoxFonts.DrawItem += new DrawItemEventHandler(comboBoxFonts_DrawItem);
            comboBoxFonts.SelectedIndexChanged += new EventHandler(comboBoxFonts_SelectedIndexChanged);
            this.Deactivate += new EventHandler(FontSelectForm_Deactivate);
            dataGridViewChrs.CellClick += new DataGridViewCellEventHandler(dataGridViewChrs_CellClick);
        }

        void dataGridViewChrs_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            _selectedChr = dataGridViewChrs[e.ColumnIndex, e.RowIndex].Value.ToString()[0];
            Hide();
            if (FontSelected != null)
            {
                FontSelected(_selectedFF, _selectedChr);
            }
        }       

        /// <summary>
        /// 更新FontFamily
        /// </summary>
        /// <param name="ff"></param>
        public void UpdateFontFamily(FontFamily ff)
        {
            _selectedFF = ff;
            comboBoxFonts.SelectedItem = ff;
            // 更新字符预览
            UpdateChrsView();
        }

        /// <summary>
        /// 在控件周围展示出本窗口
        /// </summary>
        /// <param name="ctrl"></param>
        public void Show(Control ctrl)
        {
            Rectangle rect = ctrl.RectangleToScreen(new Rectangle(0, 0, ctrl.Width, ctrl.Height));
            this.Left = rect.Left;
            this.Top = rect.Top + ctrl.Height;
            Rectangle ScreenRect = Screen.PrimaryScreen.WorkingArea;
            if (this.Right > ScreenRect.Width || this.Bottom > ScreenRect.Height)
            {
                this.Left = rect.Left - this.Width + ctrl.Width;
                this.Top = rect.Top - this.Height;
            }
            this.Show();
        }

        private void InitFonts()
        {
            comboBoxFonts.DrawMode = DrawMode.OwnerDrawFixed;
            comboBoxFonts.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxFonts.ItemHeight = 20;
            comboBoxFonts.BeginUpdate();
            comboBoxFonts.Items.Clear();
            InstalledFontCollection fonts = new InstalledFontCollection();
            foreach (FontFamily ff in fonts.Families)
            {
                comboBoxFonts.Items.Add(ff);
            }
            comboBoxFonts.SelectedItem = _selectedFF;
            comboBoxFonts.EndUpdate();
        }

        #region ComboBox事件处理

        void comboBoxFonts_SelectedIndexChanged(object sender, EventArgs e)
        {
            _selectedFF = (FontFamily)comboBoxFonts.SelectedItem;
            // 更新字符预览
            UpdateChrsView();
        }

        void comboBoxFonts_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index < 0) return;
            FontFamily ff = (FontFamily)comboBoxFonts.Items[e.Index];            
            // 修改2013-4-18 以黑体显示字体列表
            e.Graphics.DrawString(ff.Name, Font, _fontBrush,
                0.5f * e.Bounds.Width, e.Bounds.Top + 0.5f * e.Bounds.Height, _sf);
            // end
        }

        #endregion

        #region 窗体事件处理

        void FontSelectForm_Deactivate(object sender, EventArgs e)
        {
            this.Hide();
        }

        #endregion

        // 给Form加边框
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.Style &= ~Defines.WS_CAPTION | Defines.WS_BORDER;
                return cp;
            }
        }

        private void InitChrs()
        {
            int row = 13108;
            int col = 5;
            dataGridViewChrs.Rows.Add(6554);
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    int number = i * 10 + j;
                    if (number >= ushort.MaxValue)
                    {
                        return;
                    }
                    dataGridViewChrs[j, i].Value = new string(Convert.ToChar(number), 1);
                }
            }
        }

        private void UpdateChrsView()
        {
            dataGridViewChrs.Font = new Font(_selectedFF, 15);
        }
    }
}
