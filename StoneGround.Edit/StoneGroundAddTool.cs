﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap;
using NetMap.Interfaces;
using NetMap.Interfaces.Symbol;
using System.Windows.Forms;
using System.Drawing;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Data;
using NetMap.Interfaces.Geometry;
using NetMap.Application.Gui.Dialogs;
using NetMap.Application.Core;
using StoneGround.Dialog;

namespace StoneGround.Edit
{
    /// <summary>
    /// 添加料场
    /// </summary>
    public class StoneGroundAddTool : Tool
    {
        private ISymbol _symbol;

        public StoneGroundAddTool()
            : base("添加料场")
        {
        }

        public StoneGroundAddTool(IMapEditView view)
            : base("添加料场", view)
        {
        }

        public override void GainFocus()
        {
            //base.GainFocus();
            if (_view.Status != null)
            {
                _view.Status.ShowInfo("新增 左键直接绘制料场，右键精确绘制料场");
            }
        }

        /// <summary>
        /// 设置料场符号
        /// </summary>
        /// <param name="symbol"></param>
        public void SetSymbol(ISymbol symbol)
        {
            _symbol = symbol;
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            //base.OnMouseDown(e);
            if (e.Button == MouseButtons.Left)
            {
                double x = 0, y = 0;
                IDisplayTransformation displayTranformation = View.DisplayTransformation;
                displayTranformation.ToMapPoint(e.X, e.Y, ref x, ref y);
                AddFeature(x, y);
                MessageService.ShowMessage("添加料场成功", "提示");
            }
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            //base.OnMouseMove(e);
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            //base.OnMouseUp(e);
            if (e.Button == MouseButtons.Right)
            {
                LonLatInput input = new LonLatInput();
                if (input.ShowDialog() == DialogResult.OK)
                {
                    double x = 0, y = 0;
                    IDisplayTransformation displayTranformation = View.DisplayTransformation;
                    displayTranformation.ToMapPoint(e.X, e.Y, ref x, ref y);
                    AddFeature(x, y);
                    MessageService.ShowMessage("添加料场成功", "提示");
                }
            }
        }

        public override void Draw(Graphics g)
        {
            //base.Draw(g);
        }

        //添加料场，x=经度, y=纬度
        private void AddFeature(double x, double y)
        {
            IFeatureClass featureClass = StoneGroundDatabaseService.Instance.StoneGroundFeatureClass;
            IFeature feature = featureClass.CreateFeature();
            feature.Geometry = new NetMap.Geometry.Point(x, y);
            feature.SetValue(0, "测试料场");
            feature.SetValue(1, "");
            feature.SetValue(2, "");
            feature.SetValue(3, "**市**镇");
            feature.SetValue(4, "000-00000000");
            feature.SetValue(5, "");
            feature.SetValue(6, "");
            feature.SetValue(7, 0.0);
            feature.SetValue(8, "");
            feature.SetValue(9, "");
            feature.SetValue(10, DateTime.Now);
            feature.SetValue(11, 0.0); //density
            feature.SetValue(12, 0.0);
            feature.SetValue(13, 0.0);
            feature.SetValue(14, 0.0);
            feature.SetValue(15, 0.0);
            feature.SetValue(16, 0.0);
            feature.SetValue(17, 0.0);
            feature.SetValue(18, 0);
            feature.SetValue(19, 0.0);
            feature.Store();
            View.Invalidate();
        }
    }

    /// <summary>
    /// 添加料场命令
    /// </summary>
    public class StoneGroundAddCommand : Command
    {
        public StoneGroundAddCommand()
            : base("添加料场2")
        {
        }

        public StoneGroundAddCommand(IMapView view)
            : base("添加料场2", view)
        {
        }

        public override void Execute()
        {
            /*FrmSGInfo dialog = new FrmSGInfo();
            dialog.StartPosition = FormStartPosition.CenterParent;
            IFeatureClass featureClass = StoneGroundDatabaseService.Instance.StoneGroundFeatureClass;
            IFeature newFeature = featureClass.CreateFeature();
            DefaultValue(newFeature);
            dialog.InitCreate(newFeature);
            dialog.ShowDialog(NetMap.Application.Gui.WorkbenchSingleton.MainWin32Window);*/
            FrmSGInfo2 dialog = new FrmSGInfo2();
            dialog.StartPosition = FormStartPosition.CenterParent;
            IFeatureClass featureClass = StoneGroundDatabaseService.Instance.StoneGroundFeatureClass;
            IFeature newFeature = featureClass.CreateFeature();
            DefaultValue(newFeature);
            dialog.InitCreate(newFeature);
            dialog.ShowDialog(NetMap.Application.Gui.WorkbenchSingleton.MainWin32Window);
        }

        private void DefaultValue(IFeature feature)
        {
            feature.Geometry = new NetMap.Geometry.Point(101.888, 24.518);
            feature.SetValue(0, "");
            feature.SetValue(1, "");
            feature.SetValue(2, "");
            feature.SetValue(3, "");
            feature.SetValue(4, "");
            feature.SetValue(5, "");
            feature.SetValue(6, "");
            feature.SetValue(7, 0.0);
            feature.SetValue(8, "");
            feature.SetValue(9, "");
            feature.SetValue(10, DateTime.Now);
            feature.SetValue(11, 0.0); 
            feature.SetValue(12, 0.0);
            feature.SetValue(13, 0.0);
            feature.SetValue(14, 0.0);
            feature.SetValue(15, 0.0);
            feature.SetValue(16, 0.0);
            feature.SetValue(17, 0.0);
            feature.SetValue(18, 0);
            feature.SetValue(19, 0.0);
        }
    }
}
