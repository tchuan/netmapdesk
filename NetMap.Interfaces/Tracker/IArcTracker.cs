﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;

namespace NetMap.Interfaces.Tracker
{
    /// <summary>
    /// 圆弧橡皮条接口
    /// </summary>
    public interface IArcTracker
    {
        /// <summary>
        /// 设置圆心(舍弃，Start时设置圆心)
        /// </summary>
        /// <param name="point"></param>
        //void SetCenter(IPoint point);
        /// <summary>
        /// 设置圆弧的半径
        /// </summary>
        /// <param name="point"></param>
        void SetRadius(IPoint point);
        /// <summary>
        /// 设置圆弧起始角度
        /// </summary>
        /// <param name="point"></param>
        void SetStartAngle(IPoint point);
        /// <summary>
        /// 设置圆弧弧度
        /// </summary>
        /// <param name="point"></param>
        void SetSweepAngle(IPoint point);
    }
}
