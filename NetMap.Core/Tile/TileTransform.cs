﻿﻿// Copyright (c) BruTile developers team. All rights reserved. See License.txt in the project root for license information.

using NetMap.Geometry;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Tile;
using System;

namespace NetMap.Tile
{
    public static class TileTransform
    {
        public static TileRange WorldToTile(IEnvelope extent, string levelId, ITileSchema schema)
        {
            switch (schema.Axis)
            {
                case AxisDirection.Normal:
                    return WorldToTileNormal(extent, levelId, schema);
                case AxisDirection.InvertedY:
                    return WorldToTileInvertedY(extent, levelId, schema);
                default:
                    throw new Exception("Axis type was not found");
            }
        }

        public static IEnvelope TileToWorld(TileRange range, string levelId, ITileSchema schema)
        {
            switch (schema.Axis)
            {
                case AxisDirection.Normal:
                    return TileToWorldNormal(range, levelId, schema);
                case AxisDirection.InvertedY:
                    return TileToWorldInvertedY(range, levelId, schema);
                default:
                    throw new Exception("Axis type was not found");
            }
        }

        private static TileRange WorldToTileNormal(IEnvelope extent, string levelId, ITileSchema schema)
        {
            var resolution = schema.Resolutions[levelId];
            var tileWorldUnits = resolution.UnitsPerPixel * schema.GetTileWidth(levelId);
            var firstCol = (int)Math.Floor((extent.XMin - schema.GetOriginX(levelId)) / tileWorldUnits);
            var firstRow = (int)Math.Floor((extent.YMin - schema.GetOriginY(levelId)) / tileWorldUnits);
            var lastCol = (int)Math.Ceiling((extent.XMax - schema.GetOriginX(levelId)) / tileWorldUnits);
            var lastRow = (int)Math.Ceiling((extent.YMax - schema.GetOriginY(levelId)) / tileWorldUnits);
            return new TileRange(firstCol, firstRow, lastCol - firstCol, lastRow - firstRow);
        }

        private static IEnvelope TileToWorldNormal(TileRange range, string levelId, ITileSchema schema)
        {
            var resolution = schema.Resolutions[levelId];
            var tileWorldUnits = resolution.UnitsPerPixel * schema.GetTileWidth(levelId);
            var minX = range.FirstCol * tileWorldUnits + schema.GetOriginX(levelId);
            var minY = range.FirstRow * tileWorldUnits + schema.GetOriginY(levelId);
            var maxX = (range.FirstCol + range.ColCount) * tileWorldUnits + schema.GetOriginX(levelId);
            var maxY = (range.FirstRow + range.RowCount) * tileWorldUnits + schema.GetOriginY(levelId);
            return new Envelope(minX, minY, maxX, maxY);
        }

        private static TileRange WorldToTileInvertedY(IEnvelope extent, string levelId, ITileSchema schema)
        {
            var resolution = schema.Resolutions[levelId];
            var tileWorldUnits = resolution.UnitsPerPixel * schema.GetTileWidth(levelId);
            var firstCol = (int)Math.Floor((extent.XMin - schema.GetOriginX(levelId)) / tileWorldUnits);
            var firstRow = (int)Math.Floor((-extent.YMax + schema.GetOriginY(levelId)) / tileWorldUnits);
            var lastCol = (int)Math.Ceiling((extent.XMax - schema.GetOriginX(levelId)) / tileWorldUnits);
            var lastRow = (int)Math.Ceiling((-extent.YMin + schema.GetOriginY(levelId)) / tileWorldUnits);
            return new TileRange(firstCol, firstRow, lastCol - firstCol, lastRow - firstRow);
        }

        private static IEnvelope TileToWorldInvertedY(TileRange range, string levelId, ITileSchema schema)
        {
            var resolution = schema.Resolutions[levelId];
            var tileWorldUnits = resolution.UnitsPerPixel * schema.GetTileWidth(levelId);
            var minX = range.FirstCol * tileWorldUnits + schema.GetOriginX(levelId);
            var minY = -(range.FirstRow + range.RowCount) * tileWorldUnits + schema.GetOriginY(levelId);
            var maxX = (range.FirstCol + range.ColCount) * tileWorldUnits + schema.GetOriginX(levelId);
            var maxY = -(range.FirstRow) * tileWorldUnits + schema.GetOriginY(levelId);
            return new Envelope(minX, minY, maxX, maxY);
        }
    }
}
