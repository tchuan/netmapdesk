﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Application.Core;
using NetMap.Application;
using NetMap.Interfaces;

namespace StoneGround.Search.Commands
{
    /// <summary>
    /// 清空搜索条件
    /// </summary>
    public class ClearSearchCondition : AbstractCommand
    {
        public override void Run()
        {
            //清理搜索条件
            SearchPad.Instance.Condition.Clear();

            IMapView view = WorkspaceSingleton.ActiveMapView;
            IMapDocument map = view.MapDocument;
            //清空区划条件
            ILayer dzoneLayer = map.FindLayer(StoneGroundDefines.LAYER_NAME_DZONE);
            IFeatureSelection dzoneFeatureSelection = dzoneLayer as IFeatureSelection;
            dzoneFeatureSelection.Clear();
            ILayer layer = map.FindLayer(StoneGroundDefines.LAYER_NAME_ZONE);
            IFeatureSelection selection = layer as IFeatureSelection;
            selection.Clear();
            //清空道路条件
            layer = map.FindLayer(StoneGroundDefines.LAYER_NAME_ROAD);
            selection = layer as IFeatureSelection;
            selection.Clear();

            // 清空查询列表
            SearchPad.Instance.ClearSearchTreeNodes();
            SearchPad.Instance.Condition.Clear();

            view.Invalidate();
        }
    }
}
