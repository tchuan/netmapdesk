﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap;
using NetMap.Interfaces;
using System.Windows.Forms;
using System.Drawing;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Data;
using NetMap.UI.Tools.Draw;
using NetMap.Application.Gui.Pads;

namespace StoneGround.Edit.Commands
{
    class RoadAddTool : Tool
    {
        private LinePen _linePen;

        public RoadAddTool()
            : base("添加道路")
        {
        }

        public RoadAddTool(IMapEditView view)
            : base("添加道路", view)
        {
        }

        public override void Init()
        {
            base.Init();
            _linePen = new LinePen(EditView);
            _linePen.Init();
        }

        public override void GainFocus()
        {
            base.GainFocus();
            _linePen.GainFocus();
        }

        public override void LostFocus()
        {
            //base.LostFocus();
            _linePen.LostFocus();
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            //base.OnMouseDown(e);
            _linePen.OnMouseDown(e);
            if (e.Button == MouseButtons.Right)
            {
                IMultiLineString geometry = _linePen.MultiLineString;
                AddFeature(geometry);
                _linePen.GainFocus(); //重置画线工具
                View.Invalidate();
            }
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            //base.OnMouseMove(e);            
            _linePen.OnMouseMove(e);
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            //base.OnMouseUp(e);
            _linePen.OnMouseUp(e);
        }

        public override void Draw(Graphics g)
        {
            //base.Draw(g);
            _linePen.Draw(g);
        }

        private void AddFeature(IMultiLineString geometry)
        {
            IFeatureClass featureClass = SpatialDatabaseService.Instance.RoadFeatureClass;
            IFeature feature = featureClass.CreateFeature();
            feature.Geometry = geometry;
            feature.SetValue(0, 1);
            feature.SetValue(1, "道路");
            feature.SetValue(2, "G11");
            feature.Store();
        }
    }

    /// <summary>
    /// 添加道路
    /// </summary>
    public class RoadAddCommand : MapCommand
    {
        public RoadAddCommand()
        {
            InnerCmd = new RoadAddTool();
        }
    }
}
