﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetMap.UI.Ctrl
{
    public partial class ColorForm : System.Windows.Forms.Form
    {
        private Array allColors=null;//获取系统颜色名存入列表
        private AllRect allRec = null;
        private Color selectedColor = Color.Black;
        private Color defaultColor = Color.Black;
        public ColorForm(Color defaultColor)
        {
            InitializeComponent();
            this.defaultColor = defaultColor;
            allColors = Enum.GetValues(typeof(KnownColor));
            allRec = new AllRect(allColors);
            //this.BackColor = Color.Gray;
            this.Width = 226; this.Height = 286;
            
            
        }
        #region 事件定义
        public delegate void ActionEventHandler(Color color);
        public event ActionEventHandler SelectColorChanged;
        protected virtual void OnSelectColorChanged(Color color)
        {
            if (SelectColorChanged != null)
            {
                SelectColorChanged(color);
            }
        }
        #endregion
        public Color SelectedColor { get { return selectedColor; } }
        /// <summary>
        /// 自定义的Show
        /// </summary>
        /// <param name="Ctl">要在那个控件附近Show出本窗口</param>
        public virtual void Show(Control Ctl)
        {
            Rectangle rect = Ctl.RectangleToScreen(new Rectangle(0,0,Ctl.Width,Ctl.Height));
            this.Left = rect.Left; this.Top = rect.Top + Ctl.Height;
            Rectangle ScreenRect = Screen.PrimaryScreen.WorkingArea;
            if (this.Right > ScreenRect.Width || this.Bottom > ScreenRect.Height)
            {
                this.Left = rect.Left - this.Width + Ctl.Width; this.Top = rect.Top - this.Height;
            }
            this.Show();
        }
        /// <summary>
        /// 重写WndProc
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m)
        {
            //如果整个程序失去就隐藏
            if (m.Msg ==NetMap.Defines.WM_ACTIVATEAPP && m.WParam == IntPtr.Zero)
            { this.Hide(); }
            base.WndProc(ref m);
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(new Pen(Color.Black, 2), new Rectangle(0, 0, this.Width, this.Height));
            int x = 5, y = 5, count = 1;
            using (SolidBrush MyBrush1 = new SolidBrush(Color.Black))
            {
                #region 默认区域
                Rectangle r1 = new Rectangle(x, y, 130, 12);
                e.Graphics.DrawRectangle(new Pen(Color.FromArgb(172, 168, 153)), x, y, 12, 12);
                e.Graphics.FillRectangle(new SolidBrush(this.defaultColor), x, y, 12, 12);
                e.Graphics.DrawRectangle(new Pen(Color.Gray, 1),new Rectangle(5,5,216,12));
                Point p = new Point(x + 100, y - 1);
                Font drawFont = new Font("Arial", 8);
                e.Graphics.DrawString("默认", drawFont, MyBrush1, p);
                #endregion
                y += 20;
                for (int i = 0; i < allColors.Length; i++)
                {
                    if (count < 13)
                    {
                        SolidBrush MyBrush = new SolidBrush(Color.FromName(allColors.GetValue(i).ToString()));
                        e.Graphics.DrawRectangle(new Pen(Color.FromArgb(172, 168, 153)), x, y, 12, 12);
                        Rectangle r = new Rectangle(x, y, 12, 12);
                        count = i + 1;
                        allRec.SetValue(r, i);
                        e.Graphics.FillRectangle(MyBrush, x, y, 12, 12);
                        e.Graphics.DrawRectangle(new Pen(Color.Gray, 1), r);
                        x += 17;
                    }
                    else
                    {
                        int row = count / 13 + 1;   //
                        if (count % 13 == 0)
                        {
                            x = 5;
                            if (row == 0)
                            {
                                y = 43;
                            }
                            else
                            {
                                y = 17 * row + 8;
                            }
                        }
                        SolidBrush MyBrush = new SolidBrush(Color.FromName(allColors.GetValue(i).ToString()));
                        Rectangle r = new Rectangle(x, y, 12, 12);
                        e.Graphics.DrawRectangle(new Pen(Color.FromArgb(172, 168, 153)), r);
                        count = i + 1;
                        allRec.SetValue(r, i);
                        e.Graphics.FillRectangle(MyBrush, x, y, 12, 12);
                        e.Graphics.DrawRectangle(new Pen(Color.Gray,1),r);
                        x += 17;
                    }
                }
                #region 绘制自定义区域
                r1 = new Rectangle(x, y, 130, 12);
                allRec.SetValue(r1, count);//其他颜色
                e.Graphics.DrawRectangle(new Pen(Color.Gray), r1);
                e.Graphics.DrawString("其他颜色", drawFont, MyBrush1, x+40,y);
                #endregion 
                #region 加入默认颜色区域
                allRec.SetValue(new Rectangle(5, 5, 216, 12), count + 1);
                #endregion
                #region 无颜色区域
                allRec.SetValue(new Rectangle(5, this.Height - 22, this.Width - 10, 15), count + 2);
                e.Graphics.DrawRectangle(new Pen(Color.Gray), new Rectangle(5, this.Height-22, this.Width-10, 15));
                e.Graphics.DrawString("No Color", drawFont, MyBrush1, this.Width/2-14, y+18);
                #endregion
            }
        
        }
        protected override void OnMouseMove(MouseEventArgs e)
        {
            Point pt = Control.MousePosition;
            Color cl = Color.FromArgb(247,246,241);
            if (IsPtInRec(pt,this.Bounds))
            {
                using (Graphics g = this.CreateGraphics())
                {
                    Point p = new Point(e.X,e.Y);
                    Rectangle temRec = new Rectangle();
                    int n = 0;
                    for (int i = 0; i < allRec.Count; i++)
                    {
                        if (i != allRec.Count - 1 && i != allRec.Count - 2 && i != allRec.Count - 3)
                        {
                            g.FillRectangle(new SolidBrush(Color.FromName(allColors.GetValue(i).ToString())), allRec.GetArrRect[i]);
                            g.DrawRectangle(new Pen(Color.Gray, 1), allRec.GetArrRect[i]);
                        }
                        else if (i == allRec.Count - 1)//无颜色区
                        {
                            g.DrawRectangle(new Pen(Color.Gray, 1), allRec.GetArrRect[i]);
                        }
                        else if (i == allRec.Count - 2)//默认颜色区
                        {
                            g.DrawRectangle(new Pen(Color.Gray, 1), allRec.GetArrRect[i]);
                        }
                        else if (i == allRec.Count - 3)//自定义区
                        {
                            g.DrawRectangle(new Pen(Color.Gray, 1), allRec.GetArrRect[i]);
                        }
                    }
                    for (int i = 0; i < allRec.Count; i++)
                    {
                        if (IsPtInRec(p, allRec.GetArrRect[i]))
                        {
                            temRec = allRec.GetArrRect[i]; 
                            n = i; 
                        }
                    }
                    g.DrawRectangle(new Pen(Color.Black,1f), temRec);
                    
                }
            }
            base.OnMouseMove(e);
        }
        protected override void OnMouseDown(MouseEventArgs e)
        {
            
            this.Capture = false;//释放鼠标
            Point pt = Control.MousePosition;
            Color cl = Color.Empty;
            if (IsPtInRec(pt, this.Bounds))
            {
                Rectangle temRec = new Rectangle();
                int n = 0;
                for (int i = 0; i < allRec.Count; i++)
                {
                    Point p = new Point(e.X,e.Y);
                    if (IsPtInRec(p, allRec.GetArrRect[i]))
                    { 
                        temRec = allRec.GetArrRect[i]; 
                        n = i;
                    }
                }
                if (n != allRec.Count - 1&&n!=allRec.Count-2&&n!=allRec.Count-3)
                {
                    selectedColor = Color.FromName(allColors.GetValue(n).ToString());
                }
                else if (n == allRec.Count - 1)//无颜色区
                {
                    selectedColor = Color.Empty;
                }
                else if (n == allRec.Count - 2)//默认颜色区
                {
                    selectedColor = defaultColor;
                }
                else if (n == allRec.Count - 3)//自定义颜色区
                {
                    DIYColorFrm temDIYColorFrm = new DIYColorFrm();
                    temDIYColorFrm.DefaultColor = defaultColor;
                    temDIYColorFrm.SelectedColorEnvnt += new DIYColorFrm.CurrentColorHandle(temDIYColorFrm_SelectedColorEnvnt);
                    temDIYColorFrm.Show();
                    this.Hide();
                }
            }
            
            base.OnMouseDown(e);
        }
        void temDIYColorFrm_SelectedColorEnvnt(Color color)
        {
            selectedColor = color;
            OnSelectColorChanged(selectedColor);
        }
        protected override void OnMouseUp(MouseEventArgs e)
        {
            Point pt = Control.MousePosition;
            if (IsPtInRec(pt, this.Bounds))
            {
                Point p = new Point(e.X,e.Y);
                bool temBool=false;
                for (int i = 0; i < allRec.Count; i++)
                {
                    if (IsPtInRec(p, allRec.GetArrRect[i])) temBool = true;
                }
                if (temBool == true) 
                {
                    OnSelectColorChanged(selectedColor);
                    this.Hide(); 
                }
                temBool = false;
            }
            base.OnMouseUp(e);
        }
        private bool IsPtInRec(Point pt, Rectangle rect)
        {
            if (pt.X > rect.X && pt.X < rect.Right && pt.Y > rect.Y && pt.Y < rect.Bottom) return true;
            else return false;
        }
    }
    internal class AllRect
    {
        private List<Rectangle> arrRect;
        public AllRect(Array colors)
        {
            arrRect = new List<Rectangle>();
            arrRect.Clear();
            for (int i = 0; i <= colors.Length+2; i++)
            {
                arrRect.Add(new Rectangle());
            }
        }
        public int Count { get { return arrRect.Count(); } }
        public List<Rectangle> GetArrRect { get { return arrRect; } }
        public void SetValue(Rectangle rect, int i)
        {
            if(i>=0&&i<arrRect.Count)
            arrRect[i] = rect;
        }
    }
}
