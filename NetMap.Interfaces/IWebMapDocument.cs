﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces
{
    /// <summary>
    /// Web地图文档接口
    /// </summary>
    public interface IWebMapDocument : IMapDocument
    {
        /// <summary>
        /// 所有比例尺
        /// </summary>
        IList<double> ScaleRatios { get; set; }
    }
}
