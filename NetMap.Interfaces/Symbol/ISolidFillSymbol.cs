﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace NetMap.Interfaces.Symbol
{
    /// <summary>
    /// 实心面符号接口
    /// </summary>
    public interface ISolidFillSymbol : IFillSymbol
    {
        /// <summary>
        /// 颜色
        /// </summary>
        Color Color { get; set; }
    }
}
