﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;
using System.Drawing;

namespace NetMap.Interfaces.Render
{
    /// <summary>
    /// 渲染环境接口
    /// </summary>
    public interface IDisplay
    {
        Graphics G { get; }
        /// <summary>
        /// 获取符号渲染器
        /// </summary>
        ISymbolRender SymbolRender { get; }
        /// <summary>
        /// 获取裁剪范围
        /// </summary>
        IEnvelope ClipEnvelope { get; }
        /// <summary>
        /// 获取显示用的空间转换对象
        /// </summary>
        IDisplayTransformation DisplayTransformation { get; }
    }
}
