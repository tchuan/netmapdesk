﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Application.Core;
using System.Windows.Forms;

namespace StoneGround.Commands
{
    /// <summary>
    /// 查看帮助
    /// </summary>
    public class ViewHelpCommand : AbstractCommand
    {
        public override void Run()
        {
            string file = System.IO.Path.Combine(Application.StartupPath, "doc\\用户手册.pdf");
            System.Diagnostics.Process.Start(file);
        }
    }

    /// <summary>
    /// 关于
    /// </summary>
    public class AboutCommand : AbstractCommand
    {
        public override void Run()
        {
            MessageService.ShowMessage("  云南省公路开发投资有限责任公司\n云南云岭高速公路交通科技有限公司\n    云南龙瑞高速公路建设指挥部\n" +
                                       "招商局重庆交通科研设计院有限公司\n", "关于");
        }
    }
}
