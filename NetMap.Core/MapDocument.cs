﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Common;
using NetMap.Geometry;
using System.IO;
using NetMap.Interfaces.Symbol;
using NetMap.Interfaces.Render;
using NetMap.Render;
using ProjNet.CoordinateSystems;
using NetMap.Interfaces.Fetcher;
using NetMap.Interfaces.Persist;
using System.Xml;
using NetMap.Utilities;

namespace NetMap
{
    public class MapDocument : IMapDocument, IMapDocumentEvent, IPersistResource
    {
        private List<ILayer> _layers = new List<ILayer>();
        private IEnvelope _extent = null;
        private ILayer _currentLayer = null;
        private int _tag = 0; // add 2013-4-18 图层的tag

        /// <summary>
        /// 创建空地图文档
        /// </summary>
        public MapDocument()
            : this(null)
        {            
        }

        /// <summary>
        /// 创建地图文档
        /// </summary>
        /// <param name="extent">地图范围</param>
        public MapDocument(IEnvelope extent)
        {
            Name = "未命名地图";
            Description = "";
            Scale = 1;
            _extent = extent;
            MinScale = double.MinValue;
            MaxScale = double.MaxValue;
        }

        #region IMapDocument 成员

        public string Name
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public double MaxScale
        {
            get;
            set;
        }

        public double MinScale
        {
            get;
            set;
        }

        public int LayerCount
        {
            get 
            {
                return _layers.Count;
            }
        }

        public double Scale
        {
            get;
            set;
        }

        /// <summary>
        /// 获取和设置空间参考坐标系
        /// </summary>
        public ICoordinateSystem CoordinateSystem
        {
            get;
            set;
        }

        public ILayer GetLayer(int index)
        {
            return _layers[index];
        }

        public void InsertLayer(int index, ILayer layer)
        {
            //触发事件
            OnBeforeLayerChanged(LayerChangeType.Add);

            _layers.Insert(index, layer);
            layer.Tag = _tag++;
            if (layer is IAsyncDataFetcher)
            {
                IAsyncDataFetcher fetcher = layer as IAsyncDataFetcher;
                fetcher.DataChanged += AsyncLayerDataChange;
            }

            // 触发事件
            OnLayerChanged(layer, LayerChangeType.Add);
        }        

        public void AddLayer(ILayer layer)
        {
            //触发事件
            OnBeforeLayerChanged(LayerChangeType.Add);

            _layers.Add(layer);
            layer.Tag = _tag++;
            if (layer is IAsyncDataFetcher)
            {
                IAsyncDataFetcher fetcher = layer as IAsyncDataFetcher;
                fetcher.DataChanged += AsyncLayerDataChange;
            }

            // 触发事件
            OnLayerChanged(layer, LayerChangeType.Add);
        }

        public void DeleteLayer(ILayer layer)
        {
            //触发事件
            OnBeforeLayerChanged(LayerChangeType.Remove);

            _layers.Remove(layer);
            // 如果删除的是当前图层，当前图层置为null
            if (_currentLayer != null && layer.Tag == _currentLayer.Tag)
            {
                _currentLayer = null;
            }
            if (layer is IAsyncDataFetcher)
            {
                IAsyncDataFetcher fetcher = layer as IAsyncDataFetcher;
                fetcher.DataChanged -= AsyncLayerDataChange;
            }

            // 触发事件
            OnLayerChanged(layer, LayerChangeType.Remove);
        }

        public void ClearLayer()
        {
            //触发事件
            OnBeforeLayerChanged(LayerChangeType.AllRemove);

            IDisposable disposableLayer = null;
            foreach (ILayer layer in _layers)
            {
                if (layer is IAsyncDataFetcher)
                {
                    IAsyncDataFetcher fetcher = layer as IAsyncDataFetcher;
                    fetcher.DataChanged -= AsyncLayerDataChange;
                }
                disposableLayer = layer as IDisposable;
                if (disposableLayer != null)
                {
                    disposableLayer.Dispose();
                }
            }
            _layers.Clear();

            // 触发事件
            OnLayerChanged(null, LayerChangeType.AllRemove);
        }

        public void MoveLayer(ILayer targetLayer, ILayer refLayer, MutualRelationship relation)
        {
            //触发事件
            OnBeforeLayerChanged(LayerChangeType.Move);
            _layers.Remove(targetLayer);
            for (int i = 0; i < _layers.Count; i++)
            {
                if (_layers[i].Tag == refLayer.Tag)
                {
                    switch (relation)
                    {
                        case MutualRelationship.Before:
                            {
                                _layers.Insert(i, targetLayer);
                                break;
                            }                        
                        case MutualRelationship.Inner:
                            {
                                //Log.GetInstance().Debug("不支持组合图层");
                                break;
                            }
                        case MutualRelationship.After:
                            {
                                _layers.Insert(i + 1, targetLayer);
                                break;
                            }
                    }
                    break;
                }
            }
            // 触发事件
            OnLayerChanged(targetLayer, LayerChangeType.Move);
        }

        public ILayer FindLayer(string name)
        {
            foreach (ILayer layer in _layers)
            {
                if (layer.Name == name)
                {
                    return layer;
                }
            }
            return null;
        }

        //TODO 改进内部逻辑
        public void RecalExtent()
        {
            if (_layers.Count > 0)
            {
                if (_extent == null)
                {
                    if (_layers[0].Extent != null)
                    {
                        _extent = new Envelope(_layers[0].Extent);
                    }
                    else
                    {
                        _extent = new Envelope(-180, -90, 180, 90); //默认范围
                    }
                }
                else
                {
                    _extent.Update(_layers[0].Extent);
                }
                foreach (ILayer layer in _layers)
                {
                    if (layer.Extent != null) //检查不带范围的图层
                    {
                        _extent.Union(layer.Extent);
                    }
                }
            }
        }

        public IEnvelope Extent 
        {
            get
            {
                return _extent;
            }
            set
            {
                _extent = value;
            }
        }

        public ILayer CurrentLayer
        {
            get
            {
                return _currentLayer;
            }
            set
            {
                if (_currentLayer != null)
                {
                    if (value == null)
                    {
                        OnBeforeLayerChanged(LayerChangeType.CurrentChange);
                        _currentLayer = null;
                        OnLayerChanged(_currentLayer, LayerChangeType.CurrentChange);
                    }
                    else
                    {
                        if (_currentLayer.Tag != value.Tag)
                        {
                            OnBeforeLayerChanged(LayerChangeType.CurrentChange);
                            _currentLayer = value;
                            OnLayerChanged(_currentLayer, LayerChangeType.CurrentChange);
                        }
                    }       
                }
                else
                {
                    if (value != null)
                    {
                        OnBeforeLayerChanged(LayerChangeType.CurrentChange);
                        _currentLayer = value;
                        OnLayerChanged(_currentLayer, LayerChangeType.CurrentChange);
                    }
                }
            }
        }

        public void ViewChanged(bool changeEnd, IDisplayTransformation trans)
        {
            foreach (ILayer layer in _layers)
            {
                if (layer is IAsyncDataFetcher)
                {
                    IAsyncDataFetcher fetcher = layer as IAsyncDataFetcher;
                    fetcher.ViewChanged(changeEnd, trans);
                }
            }
        }

        public void AbortFetch()
        {
            foreach (ILayer layer in _layers)
            {
                if (layer is IAsyncDataFetcher)
                {
                    IAsyncDataFetcher fetcher = layer as IAsyncDataFetcher;
                    fetcher.AbortFetch();
                }
            }
        }

        #endregion

        #region IMapDocumentEvent 成员

        public event BeforeLayerChangedHandler BeforeLayerChanged;

        public event LayerChangedHandler LayerChanged;

        public event DataChangedEventHandler DataChanged;

        #endregion

        #region 事件处理

        protected virtual void OnBeforeLayerChanged(LayerChangeType type)
        {
            if (BeforeLayerChanged != null)
            {
                BeforeLayerChanged.Invoke(type);
            }
        }

        protected virtual void OnLayerChanged(ILayer layer, LayerChangeType type)
        {
            if (LayerChanged != null)
            {
                LayerChanged.Invoke(layer, type);                
            }
        }

        private void AsyncLayerDataChange(DataChangedEventArgs e)
        {
            OnDataChanged(e);
        }

        private void OnDataChanged(DataChangedEventArgs e)
        {
            if (DataChanged != null)
            {
                DataChanged(e);
            }
        }

        #endregion        

        public IEnumerable<ILayer> GetLayers()
        {
            foreach (ILayer layer in _layers)
            {
                yield return layer;
            }
        }

        #region IPersistResource 接口

        public void Save(XmlElement node, XmlDocument document)
        {
            XmlElement map = document.CreateElement("Map");
            map.SetAttribute("Name", Name);
            map.SetAttribute("Description", Description);
            map.SetAttribute("Scale", Scale.ToString());
            map.SetAttribute("Extent", XmlUtil.ToXml(Extent));
            foreach (ILayer layer in _layers)
            {
                IPersistResource layerPR = layer as IPersistResource;
                layerPR.Save(map, document);
            }
            node.AppendChild(map);
        }

        public void Load(XmlReader reader, object context)
        {
            Name = reader.GetAttribute("Name");
            Description = reader.GetAttribute("Description");
            Scale = double.Parse(reader.GetAttribute("Scale"));
            Extent = XmlUtil.ToEnvelope(reader.GetAttribute("Extent"));
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    switch (reader.Name)
                    {
                        case "FeatureLayer":
                            {
                                FeatureLayer layer = new FeatureLayer();
                                layer.Load(reader, context);
                                _layers.Add(layer);
                                break;
                            }
                        case "ImageBlockLayer":
                            {
                                ImageBlockLayer layer = new ImageBlockLayer();
                                layer.Load(reader, context);
                                _layers.Add(layer);
                                break;
                            }
                        case "TileLayer":
                            {
                                TileLayer layer = new TileLayer();
                                layer.Load(reader, context);
                                _layers.Add(layer);
                                break;
                            }
                        default:
                            {
                                throw new NotSupportedException("不支持的图层类型");
                            }
                    }
                }
                else if (reader.NodeType == XmlNodeType.EndElement
                    && reader.Name == "Map")
                {
                    return;
                }
            }
        }

        #endregion
    }
}
