﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces;
using NetMap.Interfaces.Geometry;

namespace NetMap
{
    public class WebMapDocument : MapDocument, IWebMapDocument
    {
        private IList<double> _scaleRatios = new List<double>();

        public WebMapDocument()
            : base()
        {
        }

        public WebMapDocument(IEnvelope bound)
            : base(bound)
        {
        }

        public IList<double> ScaleRatios
        {
            get
            {
                return _scaleRatios;
            }
            set
            {
                _scaleRatios.Clear();
                foreach(double scaleRatio in value)
                {
                    _scaleRatios.Add(scaleRatio);
                }
            }
        }
    }
}
