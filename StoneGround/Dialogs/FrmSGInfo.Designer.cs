﻿namespace StoneGround.Dialog
{
    partial class FrmSGInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxPitchAdhere = new System.Windows.Forms.TextBox();
            this.textBoxGill = new System.Windows.Forms.TextBox();
            this.textBoxAbrasion = new System.Windows.Forms.TextBox();
            this.textBoxPolish = new System.Windows.Forms.TextBox();
            this.textBoxCrushing = new System.Windows.Forms.TextBox();
            this.textBoxSturdiness = new System.Windows.Forms.TextBox();
            this.textBoxWaterAbsorption = new System.Windows.Forms.TextBox();
            this.textBoxDensity = new System.Windows.Forms.TextBox();
            this.textBoxCompressive = new System.Windows.Forms.TextBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxTel = new System.Windows.Forms.TextBox();
            this.textBoxRecordStatus = new System.Windows.Forms.TextBox();
            this.textBoxManageStatus = new System.Windows.Forms.TextBox();
            this.textBoxStrtumNature = new System.Windows.Forms.TextBox();
            this.textBoxGeologicage = new System.Windows.Forms.TextBox();
            this.textBoxReserves = new System.Windows.Forms.TextBox();
            this.textBoxMiningConditions = new System.Windows.Forms.TextBox();
            this.textBoxTrafficConditions = new System.Windows.Forms.TextBox();
            this.dateTimePickerRecordTime = new System.Windows.Forms.DateTimePicker();
            this.buttonModify = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxPositionLon = new System.Windows.Forms.TextBox();
            this.textBoxPositionLat = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(57, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "料场名称：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(57, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "联系方式：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(33, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "料场备案情况：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(33, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "料场经营状态：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(57, 215);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "岩层性质：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(33, 249);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 12);
            this.label6.TabIndex = 0;
            this.label6.Text = "所属地质年代：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Blue;
            this.label7.Location = new System.Drawing.Point(33, 283);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 12);
            this.label7.TabIndex = 0;
            this.label7.Text = "储量（万m³）：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(57, 317);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 12);
            this.label8.TabIndex = 0;
            this.label8.Text = "开采条件：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Blue;
            this.label9.Location = new System.Drawing.Point(309, 379);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 12);
            this.label9.TabIndex = 0;
            this.label9.Text = "交通条件：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Blue;
            this.label10.Location = new System.Drawing.Point(9, 404);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(113, 12);
            this.label10.TabIndex = 0;
            this.label10.Text = "料场信息录入日期：";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Blue;
            this.label12.Location = new System.Drawing.Point(24, 33);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(131, 12);
            this.label12.TabIndex = 0;
            this.label12.Text = "单轴抗压强度（Mpa）：";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.Blue;
            this.label13.Location = new System.Drawing.Point(66, 67);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 12);
            this.label13.TabIndex = 0;
            this.label13.Text = "表观相对密度：";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.Blue;
            this.label14.Location = new System.Drawing.Point(72, 101);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(83, 12);
            this.label14.TabIndex = 0;
            this.label14.Text = "吸水率（%）：";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.Blue;
            this.label15.Location = new System.Drawing.Point(72, 135);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(83, 12);
            this.label15.TabIndex = 0;
            this.label15.Text = "坚固性（%）：";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.Blue;
            this.label16.Location = new System.Drawing.Point(72, 169);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(83, 12);
            this.label16.TabIndex = 0;
            this.label16.Text = "压碎值（%）：";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.Color.Blue;
            this.label17.Location = new System.Drawing.Point(60, 203);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(95, 12);
            this.label17.TabIndex = 0;
            this.label17.Text = "磨光值（PSV）：";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.Color.Blue;
            this.label18.Location = new System.Drawing.Point(72, 237);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(83, 12);
            this.label18.TabIndex = 0;
            this.label18.Text = "磨耗值（%）：";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.Color.Blue;
            this.label19.Location = new System.Drawing.Point(72, 271);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(83, 12);
            this.label19.TabIndex = 0;
            this.label19.Text = "针片状（%）：";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.Blue;
            this.label20.Location = new System.Drawing.Point(6, 305);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(149, 12);
            this.label20.TabIndex = 0;
            this.label20.Text = "与沥青粘附性等级（级）：";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxPitchAdhere);
            this.groupBox1.Controls.Add(this.textBoxGill);
            this.groupBox1.Controls.Add(this.textBoxAbrasion);
            this.groupBox1.Controls.Add(this.textBoxPolish);
            this.groupBox1.Controls.Add(this.textBoxCrushing);
            this.groupBox1.Controls.Add(this.textBoxSturdiness);
            this.groupBox1.Controls.Add(this.textBoxWaterAbsorption);
            this.groupBox1.Controls.Add(this.textBoxDensity);
            this.groupBox1.Controls.Add(this.textBoxCompressive);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.groupBox1.Location = new System.Drawing.Point(305, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(295, 344);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "碎石各项指标";
            // 
            // textBoxPitchAdhere
            // 
            this.textBoxPitchAdhere.Location = new System.Drawing.Point(157, 304);
            this.textBoxPitchAdhere.Name = "textBoxPitchAdhere";
            this.textBoxPitchAdhere.Size = new System.Drawing.Size(128, 21);
            this.textBoxPitchAdhere.TabIndex = 2;
            // 
            // textBoxGill
            // 
            this.textBoxGill.Location = new System.Drawing.Point(157, 269);
            this.textBoxGill.Name = "textBoxGill";
            this.textBoxGill.Size = new System.Drawing.Size(128, 21);
            this.textBoxGill.TabIndex = 2;
            // 
            // textBoxAbrasion
            // 
            this.textBoxAbrasion.Location = new System.Drawing.Point(157, 234);
            this.textBoxAbrasion.Name = "textBoxAbrasion";
            this.textBoxAbrasion.Size = new System.Drawing.Size(128, 21);
            this.textBoxAbrasion.TabIndex = 2;
            // 
            // textBoxPolish
            // 
            this.textBoxPolish.Location = new System.Drawing.Point(157, 199);
            this.textBoxPolish.Name = "textBoxPolish";
            this.textBoxPolish.Size = new System.Drawing.Size(128, 21);
            this.textBoxPolish.TabIndex = 2;
            // 
            // textBoxCrushing
            // 
            this.textBoxCrushing.Location = new System.Drawing.Point(157, 164);
            this.textBoxCrushing.Name = "textBoxCrushing";
            this.textBoxCrushing.Size = new System.Drawing.Size(128, 21);
            this.textBoxCrushing.TabIndex = 2;
            // 
            // textBoxSturdiness
            // 
            this.textBoxSturdiness.Location = new System.Drawing.Point(157, 129);
            this.textBoxSturdiness.Name = "textBoxSturdiness";
            this.textBoxSturdiness.Size = new System.Drawing.Size(128, 21);
            this.textBoxSturdiness.TabIndex = 2;
            // 
            // textBoxWaterAbsorption
            // 
            this.textBoxWaterAbsorption.Location = new System.Drawing.Point(157, 94);
            this.textBoxWaterAbsorption.Name = "textBoxWaterAbsorption";
            this.textBoxWaterAbsorption.Size = new System.Drawing.Size(128, 21);
            this.textBoxWaterAbsorption.TabIndex = 2;
            // 
            // textBoxDensity
            // 
            this.textBoxDensity.Location = new System.Drawing.Point(157, 59);
            this.textBoxDensity.Name = "textBoxDensity";
            this.textBoxDensity.Size = new System.Drawing.Size(128, 21);
            this.textBoxDensity.TabIndex = 2;
            // 
            // textBoxCompressive
            // 
            this.textBoxCompressive.Location = new System.Drawing.Point(157, 24);
            this.textBoxCompressive.Name = "textBoxCompressive";
            this.textBoxCompressive.Size = new System.Drawing.Size(128, 21);
            this.textBoxCompressive.TabIndex = 2;
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(127, 17);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(162, 21);
            this.textBoxName.TabIndex = 0;
            // 
            // textBoxTel
            // 
            this.textBoxTel.Location = new System.Drawing.Point(127, 51);
            this.textBoxTel.Name = "textBoxTel";
            this.textBoxTel.Size = new System.Drawing.Size(162, 21);
            this.textBoxTel.TabIndex = 2;
            // 
            // textBoxRecordStatus
            // 
            this.textBoxRecordStatus.Location = new System.Drawing.Point(127, 85);
            this.textBoxRecordStatus.Name = "textBoxRecordStatus";
            this.textBoxRecordStatus.Size = new System.Drawing.Size(162, 21);
            this.textBoxRecordStatus.TabIndex = 2;
            // 
            // textBoxManageStatus
            // 
            this.textBoxManageStatus.Location = new System.Drawing.Point(127, 119);
            this.textBoxManageStatus.Multiline = true;
            this.textBoxManageStatus.Name = "textBoxManageStatus";
            this.textBoxManageStatus.Size = new System.Drawing.Size(162, 77);
            this.textBoxManageStatus.TabIndex = 2;
            // 
            // textBoxStrtumNature
            // 
            this.textBoxStrtumNature.Location = new System.Drawing.Point(127, 208);
            this.textBoxStrtumNature.Name = "textBoxStrtumNature";
            this.textBoxStrtumNature.Size = new System.Drawing.Size(162, 21);
            this.textBoxStrtumNature.TabIndex = 2;
            // 
            // textBoxGeologicage
            // 
            this.textBoxGeologicage.Location = new System.Drawing.Point(127, 242);
            this.textBoxGeologicage.Name = "textBoxGeologicage";
            this.textBoxGeologicage.Size = new System.Drawing.Size(162, 21);
            this.textBoxGeologicage.TabIndex = 2;
            // 
            // textBoxReserves
            // 
            this.textBoxReserves.Location = new System.Drawing.Point(127, 276);
            this.textBoxReserves.Name = "textBoxReserves";
            this.textBoxReserves.Size = new System.Drawing.Size(162, 21);
            this.textBoxReserves.TabIndex = 2;
            // 
            // textBoxMiningConditions
            // 
            this.textBoxMiningConditions.Location = new System.Drawing.Point(127, 310);
            this.textBoxMiningConditions.Multiline = true;
            this.textBoxMiningConditions.Name = "textBoxMiningConditions";
            this.textBoxMiningConditions.Size = new System.Drawing.Size(162, 77);
            this.textBoxMiningConditions.TabIndex = 2;
            // 
            // textBoxTrafficConditions
            // 
            this.textBoxTrafficConditions.Location = new System.Drawing.Point(379, 373);
            this.textBoxTrafficConditions.Multiline = true;
            this.textBoxTrafficConditions.Name = "textBoxTrafficConditions";
            this.textBoxTrafficConditions.Size = new System.Drawing.Size(219, 77);
            this.textBoxTrafficConditions.TabIndex = 2;
            // 
            // dateTimePickerRecordTime
            // 
            this.dateTimePickerRecordTime.Location = new System.Drawing.Point(126, 401);
            this.dateTimePickerRecordTime.Name = "dateTimePickerRecordTime";
            this.dateTimePickerRecordTime.Size = new System.Drawing.Size(165, 21);
            this.dateTimePickerRecordTime.TabIndex = 3;
            // 
            // buttonModify
            // 
            this.buttonModify.Location = new System.Drawing.Point(494, 463);
            this.buttonModify.Name = "buttonModify";
            this.buttonModify.Size = new System.Drawing.Size(84, 33);
            this.buttonModify.TabIndex = 4;
            this.buttonModify.Text = "更改";
            this.buttonModify.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.Blue;
            this.label11.Location = new System.Drawing.Point(9, 435);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(143, 12);
            this.label11.TabIndex = 0;
            this.label11.Text = "料场位置（经度 纬度）：";
            // 
            // textBoxPositionLon
            // 
            this.textBoxPositionLon.Location = new System.Drawing.Point(158, 430);
            this.textBoxPositionLon.Name = "textBoxPositionLon";
            this.textBoxPositionLon.Size = new System.Drawing.Size(102, 21);
            this.textBoxPositionLon.TabIndex = 2;
            // 
            // textBoxPositionLat
            // 
            this.textBoxPositionLat.Location = new System.Drawing.Point(266, 430);
            this.textBoxPositionLat.Name = "textBoxPositionLat";
            this.textBoxPositionLat.Size = new System.Drawing.Size(102, 21);
            this.textBoxPositionLat.TabIndex = 2;
            // 
            // FrmSGInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(610, 500);
            this.Controls.Add(this.buttonModify);
            this.Controls.Add(this.dateTimePickerRecordTime);
            this.Controls.Add(this.textBoxTrafficConditions);
            this.Controls.Add(this.textBoxMiningConditions);
            this.Controls.Add(this.textBoxReserves);
            this.Controls.Add(this.textBoxGeologicage);
            this.Controls.Add(this.textBoxStrtumNature);
            this.Controls.Add(this.textBoxManageStatus);
            this.Controls.Add(this.textBoxRecordStatus);
            this.Controls.Add(this.textBoxPositionLat);
            this.Controls.Add(this.textBoxPositionLon);
            this.Controls.Add(this.textBoxTel);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "FrmSGInfo";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "料场信息";
            this.TopMost = true;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxPitchAdhere;
        private System.Windows.Forms.TextBox textBoxGill;
        private System.Windows.Forms.TextBox textBoxAbrasion;
        private System.Windows.Forms.TextBox textBoxPolish;
        private System.Windows.Forms.TextBox textBoxCrushing;
        private System.Windows.Forms.TextBox textBoxSturdiness;
        private System.Windows.Forms.TextBox textBoxWaterAbsorption;
        private System.Windows.Forms.TextBox textBoxDensity;
        private System.Windows.Forms.TextBox textBoxCompressive;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxTel;
        private System.Windows.Forms.TextBox textBoxRecordStatus;
        private System.Windows.Forms.TextBox textBoxManageStatus;
        private System.Windows.Forms.TextBox textBoxStrtumNature;
        private System.Windows.Forms.TextBox textBoxGeologicage;
        private System.Windows.Forms.TextBox textBoxReserves;
        private System.Windows.Forms.TextBox textBoxMiningConditions;
        private System.Windows.Forms.TextBox textBoxTrafficConditions;
        private System.Windows.Forms.DateTimePicker dateTimePickerRecordTime;
        private System.Windows.Forms.Button buttonModify;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxPositionLon;
        private System.Windows.Forms.TextBox textBoxPositionLat;
    }
}