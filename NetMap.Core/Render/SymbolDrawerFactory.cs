﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetMap.Interfaces.Symbol;
using NetMap.Symbol;

namespace NetMap.Render
{
    /// <summary>
    /// 窗体符号绘制工厂
    /// </summary>
    public class SymbolDrawerFactory
    {
        private Dictionary<string, ISymbolDrawer> _registeredDrawer = new Dictionary<string, ISymbolDrawer>();

        private SymbolDrawerFactory()
        {
            //禁止在外部创建工厂的新实例
        }

        private static SymbolDrawerFactory _instance = null;
        /// <summary>
        /// 窗体符号绘制工厂实例
        /// </summary>
        public static SymbolDrawerFactory Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SymbolDrawerFactory();
                    _instance.RegisterAll(); //2013-8-7 全部注册
                }
                return _instance;
            }
        }

        /// <summary>
        /// 注册所有符号绘制
        /// </summary>
        public void RegisterAll()
        {
            _registeredDrawer.Clear();
            _registeredDrawer.Add(SymbolFactory.S_POINT_FONT, new FontPointSymbolDrawer());
            _registeredDrawer.Add(SymbolFactory.S_POINT_IMAGE, new ImagePointSymbolDrawer());
            _registeredDrawer.Add(SymbolFactory.S_POINT_BOX, new BoxPointSymbolDrawer());
            _registeredDrawer.Add(SymbolFactory.S_POINT_ARC, new ArcPointSymbolDrawer());
            _registeredDrawer.Add(SymbolFactory.S_POINT_POLYGON, new PolygonPointSymbolDrawer());
            _registeredDrawer.Add(SymbolFactory.S_LINE_STROKE, new StrokeSymbolDrawer());
            _registeredDrawer.Add(SymbolFactory.S_FILL_SOLID, new SolidFillSymbolDrawer());
            _registeredDrawer.Add(SymbolFactory.S_TEXT, new TextSymbolDrawer());
        }

        /// <summary>
        /// 注册运行时支持的符号绘制
        /// </summary>
        /// <param name="names"></param>
        public void RegisterDrawer(List<string> names)
        {
            _registeredDrawer.Clear();
            //TODO
        }

        /// <summary>
        /// 创建具体的符号绘制对象
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        public ISymbolDrawer GetDrawer(ISymbol symbol)
        {
            if (symbol == null) return null;
            return _registeredDrawer[symbol.KindAbstract];
        }
    }
}
