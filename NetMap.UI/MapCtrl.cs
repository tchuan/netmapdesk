﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NetMap.Interfaces;
using NetMap.Interfaces.Render;
using NetMap.Render;
using NetMap.Interfaces.Geometry;
using NetMap.Geometry;
using System.Diagnostics;
using NetMap.Interfaces.Fetcher;

namespace NetMap.UI
{
    [DesignerAttribute(typeof(MapCtrlDesigner))]
    public partial class MapCtrl : UserControl, IMapEditView, IMapViewEvent, IMapEditViewEvent
    {         
        #region  构造函数

        public MapCtrl()
            : this(400, 300)
        {
        }

        public MapCtrl(int width, int height)
            : this(width, height, null)
        {
        }

        public MapCtrl(int width, int height, IMapDocument document)
        {
            InitializeComponent();
            _map = document;
            Init();
            AllowDrop = true;
        }

        #endregion

        #region 重写

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!IsDesignerHosted)
            {
                InitRender().ProgressChanged += new ProgressChangedEventHandler(invalidatorEngage);
                
            }
        }

        //尺寸变化后，尽量保持数据范围不变
        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            if (this.Width == 0 || this.Height == 0)
            {
                return;
            }
            if (_map == null)
            {
                Invalidate();
                return;
            }
            _TF.WindowBounds = new Envelope(0, 0, Width, Height);
            _TF.ZoomToDataBounds(_TF.VisibleMapBounds);
            UpdateImage();
            _bufferImage.Dispose();
            _bufferImage = new Bitmap(Width, Height);
            DrawMap();
            UpdateUI(_TF.WindowBounds);
            Invalidate();
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if (_tool != null)
            {
                _tool.OnKeyDown(e);
            }     
        }

        protected override void OnKeyUp(KeyEventArgs e)
        {
            base.OnKeyUp(e);
            if (_tool != null)
            {
                _tool.OnKeyUp(e);
            }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            if (_tool != null)
            {
                _tool.OnMouseDown(e);
            }
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            if (_tool != null)
            {
                _tool.OnMouseUp(e);
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (_tool != null)
            {
                _tool.OnMouseMove(e);
            }
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            base.OnMouseEnter(e);
            if (_tool != null)
            {
                _tool.OnMouseEnter(e);
            }
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            if (_tool != null)
            {
                _tool.OnMouseLeave(e);
            }
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            base.OnMouseWheel(e);
            if (_tool != null)
            {
                _tool.OnMouseWheel(e);
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            if (_map != null)
            {
                if (_bRefreshMap)
                {
                    DrawMap();
                }
                e.Graphics.Clear(BackColor);
                e.Graphics.DrawImage(_bufferImage, 0, 0);
            }
        }

        protected override void OnDragEnter(DragEventArgs drgevent)
        {
            base.OnDragEnter(drgevent);
            if (drgevent.Data.GetDataPresent(DataFormats.FileDrop))
            {
                drgevent.Effect = DragDropEffects.Link;
            }
        }

        protected override void OnDragDrop(DragEventArgs drgevent)
        {
            base.OnDragDrop(drgevent);
            if (drgevent.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string file = ((System.Array)drgevent.Data.GetData(DataFormats.FileDrop)).GetValue(0).ToString();
                if (FileDragDrop != null)
                {
                    FileDragDrop.Invoke(file);
                }
            }
        }
        
        #endregion

        #region IMapEditView
                
        void IMapEditView.SetCurrentTool(ITool tool)
        {
            if (_tool != null)
            {
                _tool.LostFocus();
            }
            _tool = tool;
            _tool.GainFocus();
            Cursor = _tool.Cursor; // 设置光标
        }

        ITool IMapEditView.GetCurrentTool()
        {
            return _tool;
        }

        #endregion

        #region IMapView

        Color IMapView.BackColor
        {
            get
            {
                return base.BackColor;
            }
            set
            {
                base.BackColor = value;
            }
        }

        IDisplay IMapView.GetDisplay()
        {
            //Modify 2013-10-18 对视图的额外操作在效果层上进行
            //return _display;
            return _effectDisplay;
            //end
        }

        IRenderCancel IMapView.GetCancel()
        {
            throw new NotImplementedException("不支持渲染控制");
        }

        void IMapView.Clear()
        {
            _display.G.Clear(Color.Transparent);       //清空地图
            _effectDisplay.G.Clear(Color.Transparent); //清空地图效果
            UpdateUI(_TF.WindowBounds);
            this.CreateGraphics().DrawImage(_bufferImage, 0, 0);
        }        

        void IMapView.Draw()
        {
            DrawMap();
        }

        void IMapView.Refresh()
        {
            DrawMap();
            UpdateUI(_TF.WindowBounds);
            this.CreateGraphics().DrawImage(_bufferImage, 0, 0);
        }

        void IMapView.Invalidate()
        {
            Invalidate();
        }

        void IMapView.ViewChange()
        {
            OnMapViewChanged(true, _TF);
        }

        IMapDocument IMapView.MapDocument
        {
            get { return _map; }
            set
            {
                UnbindMapDataChange(_map);
                _map = value;
                BindMapDataChange(_map);
                if (_map is IWebMapDocument)
                {
                    _scaleRatioBar = new ScaleRatioBar((_map as IWebMapDocument).ScaleRatios);
                }
                else
                {
                    _scaleRatioBar = null;
                }
                UpdateTF();
                UpdateImage();
                OnMapDocumentChanged(_map);
            }
        }

        void IMapView.ZoomToScale(double scale)
        {
            _TF.ZoomToScale(scale);
            OnMapViewChanged(true, _TF);
        }

        void IMapView.ZoomToWindow(IEnvelope window)
        {
            _TF.ZoomToWindow(window);
            OnMapViewChanged(true, _TF);
        }

        void IMapView.ZoomToData(IEnvelope box)
        {
            _TF.ZoomToDataBounds(box);
            OnMapViewChanged(true, _TF);
        }

        void IMapView.ZoomToMap()
        {
            UpdateTF();
            OnMapViewChanged(true, _TF);
        }

        void IMapView.ZoomToCenter(double x, double y, double scale)
        {
            _TF.ZoomToCenter(x, y, scale);
            OnMapViewChanged(true, _TF);
        }

        #endregion

        #region virtual

        protected virtual void OnMapDocumentChanged(IMapDocument newDocument)
        {
            if (MapDocumentChanged != null)
            {
                MapDocumentChanged.Invoke(newDocument);
            }
        }

        protected virtual void OnBeforeDraw(IDisplay display)
        {
            if (BeforeDraw != null)
            {
                BeforeDraw.Invoke(display);
            }
        }

        protected virtual void OnAfterDraw(IDisplay display)
        {
            if (AfterDraw != null)
            {
                AfterDraw.Invoke(display);
            }
        }

        protected virtual void OnMapToolChange()
        {
            if (MapToolChange != null)
            {
                MapToolChange.Invoke(_tool);
            }      
        }

        #endregion

        #region IMapViewEvent

        public event MapDocumentChangedHandler MapDocumentChanged;

        public event MapViewDrawHandler BeforeDraw;

        public event MapViewDrawHandler AfterDraw;

        public event FileDragDropHandler FileDragDrop;

        public event MapViewChangedHandler MapViewChanged;

        protected virtual void OnMapViewChanged(bool bChangeEnd, IDisplayTransformation trans)
        {
            if (_map != null)
            {
                int count = _map.LayerCount;
                ILayer layer = null;
                IAsyncDataFetcher fetcher = null;
                for (int i = 0; i < count; i++)
                {
                    layer = _map.GetLayer(i);
                    if (layer is IAsyncDataFetcher)
                    {
                        fetcher = layer as IAsyncDataFetcher;
                        fetcher.ViewChanged(bChangeEnd, trans);
                    }
                }
            }
            if (MapViewChanged != null)
            {
                MapViewChanged.Invoke(bChangeEnd, trans);
            }
        }

        #endregion

        #region IMapEditViewEvent

        public event MapToolChangeHandler MapToolChange;

        #endregion
    }
}
