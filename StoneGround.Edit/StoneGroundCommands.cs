﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Application.Gui.Pads;

namespace StoneGround.Edit
{
    /// <summary>
    /// 添加料场
    /// </summary>
    public class AddCommand : MapCommand
    {
        public AddCommand()
        {
            InnerCmd = new StoneGroundAddTool();
        }
    }

    /// <summary>
    /// 添加料场2
    /// </summary>
    public class AddCommand2 : MapCommand
    {
        public AddCommand2()
        {
            InnerCmd = new StoneGroundAddCommand();
        }
    }

    /// <summary>
    /// 移动料场
    /// </summary>
    public class MoveCommand : MapCommand
    {
        public MoveCommand()
        {
            InnerCmd = new StoneGroundMoveTool();
        }
    }

    /// <summary>
    /// 删除料场
    /// </summary>
    public class DeleteCommand : MapCommand
    {
        public DeleteCommand()
        {
            InnerCmd = new StoneGroundDeleteTool();
        }
    }

    /// <summary>
    /// 查询料场信息
    /// </summary>
    public class InfoCommand : MapCommand
    {
        public InfoCommand()
        {
            InnerCmd = new StoneGroundInfoTool();
        }
    }
}
