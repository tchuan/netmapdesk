﻿namespace NetMap.UI
{
    partial class TocCtrl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TocCtrl));
            this.toolStripToc = new System.Windows.Forms.ToolStrip();
            this.toolStripUpdate = new System.Windows.Forms.ToolStripButton();
            this.toolStripApply = new System.Windows.Forms.ToolStripButton();
            this.treeViewAdvLayers = new Aga.Controls.Tree.TreeViewAdv();
            this.toolStripToc.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripToc
            // 
            this.toolStripToc.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripUpdate,
            this.toolStripApply});
            this.toolStripToc.Location = new System.Drawing.Point(0, 0);
            this.toolStripToc.Name = "toolStripToc";
            this.toolStripToc.Size = new System.Drawing.Size(204, 25);
            this.toolStripToc.TabIndex = 0;
            this.toolStripToc.Text = "toolStrip1";
            // 
            // toolStripUpdate
            // 
            this.toolStripUpdate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripUpdate.Image = ((System.Drawing.Image)(resources.GetObject("toolStripUpdate.Image")));
            this.toolStripUpdate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripUpdate.Name = "toolStripUpdate";
            this.toolStripUpdate.Size = new System.Drawing.Size(36, 22);
            this.toolStripUpdate.Text = "更新";
            this.toolStripUpdate.Click += new System.EventHandler(this.toolStripUpdate_Click);
            // 
            // toolStripApply
            // 
            this.toolStripApply.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripApply.Image = ((System.Drawing.Image)(resources.GetObject("toolStripApply.Image")));
            this.toolStripApply.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripApply.Name = "toolStripApply";
            this.toolStripApply.Size = new System.Drawing.Size(36, 22);
            this.toolStripApply.Text = "应用";
            this.toolStripApply.Click += new System.EventHandler(this.toolStripApply_Click);
            // 
            // treeViewAdvLayers
            // 
            this.treeViewAdvLayers.BackColor = System.Drawing.SystemColors.Window;
            this.treeViewAdvLayers.DefaultToolTipProvider = null;
            this.treeViewAdvLayers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewAdvLayers.DragDropMarkColor = System.Drawing.Color.Black;
            this.treeViewAdvLayers.FirstVisibleRow = 0;
            this.treeViewAdvLayers.FullRowSelect = true;
            this.treeViewAdvLayers.GridLineStyle = Aga.Controls.Tree.GridLineStyle.Horizontal;
            this.treeViewAdvLayers.LineColor = System.Drawing.SystemColors.ControlDark;
            this.treeViewAdvLayers.Location = new System.Drawing.Point(0, 25);
            this.treeViewAdvLayers.Model = null;
            this.treeViewAdvLayers.Name = "treeViewAdvLayers";
            this.treeViewAdvLayers.RowHeight = 30;
            this.treeViewAdvLayers.SelectedNode = null;
            this.treeViewAdvLayers.ShowLines = false;
            this.treeViewAdvLayers.ShowPlusMinus = false;
            this.treeViewAdvLayers.Size = new System.Drawing.Size(204, 302);
            this.treeViewAdvLayers.TabIndex = 1;
            this.treeViewAdvLayers.Text = "图层树";
            this.treeViewAdvLayers.UseColumns = false;
            // 
            // TocCtrl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.treeViewAdvLayers);
            this.Controls.Add(this.toolStripToc);
            this.Name = "TocCtrl";
            this.Size = new System.Drawing.Size(204, 327);
            this.toolStripToc.ResumeLayout(false);
            this.toolStripToc.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripToc;
        private System.Windows.Forms.ToolStripButton toolStripUpdate;
        private System.Windows.Forms.ToolStripButton toolStripApply;
        private Aga.Controls.Tree.TreeViewAdv treeViewAdvLayers;     
    }
}
