﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetMap.UI.Ctrl
{
    public class ToolStripColorCtrlByName : ToolStripControlHost
    {
        public ToolStripColorCtrlByName()
            : base(new ColorCtrlByName())
        { }
        public ColorCtrlByName GetColorCtrlByName()
        {
            return Control as ColorCtrlByName;
        }
    }
}
