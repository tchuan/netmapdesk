﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Data;

namespace NetMap.Data
{
    public class Fields : IFields
    {        
        private Dictionary<int, IField> _dic = new Dictionary<int, IField>();

        public Fields()
        {
        }

        #region IFields 成员

        public int Count
        {
            get { return _dic.Count; }
        }

        public IField GetField(int index)
        {
            if (index >= _dic.Count)
            {
                throw new ArgumentException("index越界");
            }
            return _dic[index];
        }

        public void SetField(int index, IField field)
        {
            if (index > _dic.Count) //保证字段索引是从0开始,并顺序增加
            {
                throw new ArgumentException("index越界");
            }
            IField dump = null;
            if (_dic.TryGetValue(index, out dump))
            {
                _dic[index] = field;    //更新字段
            }
            else
            {
                _dic.Add(index, field); //新增字段
            }
        }

        public int FindField(string name)
        {
          return _dic.FirstOrDefault(m => m.Value.Name == name).Key;
        }

        #endregion        
    }
}
