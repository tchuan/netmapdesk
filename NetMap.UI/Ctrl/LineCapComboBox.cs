﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace NetMap.UI.Ctrl
{
    /// <summary>
    /// 线帽变化事件处理
    /// </summary>
    /// <param name="linecap"></param>
    public delegate void LineCapChangedEventHandler(LineCap linecap);
    /// <summary>
    /// 线帽样式选择控件
    /// </summary>
    public partial class LineCapComboBox : UserControl
    {
        private LineCap _selectedLineCap = LineCap.Flat;
        private Pen _pen = new Pen(Color.Black, 5);
        private StringFormat _sf = null;
        /// <summary>
        /// 线帽变化事件
        /// </summary>
        public event LineCapChangedEventHandler LineCapChanged;

        public LineCapComboBox()
        {
            _sf = new StringFormat();
            _sf.Alignment = StringAlignment.Center;
            _sf.LineAlignment = StringAlignment.Center;
            InitializeComponent();
            InitItems();
            comboBoxInner.DrawItem += new DrawItemEventHandler(comboBoxInner_DrawItem);
            comboBoxInner.SelectedIndexChanged += new EventHandler(comboBoxInner_SelectedIndexChanged);
        }              

        /// <summary>
        /// 选择的线帽
        /// </summary>
        public LineCap SelectedLineCap
        {
            get { return _selectedLineCap; }
            set 
            { 
                _selectedLineCap = value;
                comboBoxInner.SelectedItem = _selectedLineCap;
            }
        }

        private void InitItems()
        {            
            comboBoxInner.DrawMode = DrawMode.OwnerDrawFixed;
            comboBoxInner.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxInner.ItemHeight = 20;
            comboBoxInner.BeginUpdate();
            comboBoxInner.Items.Clear();
            Array linecaps = Enum.GetValues(typeof(LineCap));
            foreach (LineCap linecap in linecaps)
            {
                comboBoxInner.Items.Add(linecap);
            }
            comboBoxInner.SelectedIndex = 0;
            comboBoxInner.EndUpdate();
        }

        private void comboBoxInner_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index < 0) return;
            Rectangle rect = e.Bounds;
            LineCap linecap = (LineCap)comboBoxInner.Items[e.Index];
            rect.Inflate(-1, -1);
            if (linecap == _selectedLineCap)
            {
                e.Graphics.FillRectangle(SystemBrushes.Highlight, rect);
            }
            else
            {
                e.Graphics.FillRectangle(SystemBrushes.Window, rect);
            }
            _pen.StartCap = linecap;
            e.Graphics.DrawLine(_pen, 10, rect.Top + 0.5f * rect.Height, 0.2f * rect.Width, rect.Top + 0.5f * rect.Height);
            e.Graphics.DrawString(linecap.ToString(), Font, Brushes.Black, 0.6f * rect.Width, rect.Top + 0.5f * rect.Height, _sf);
        }

        private void comboBoxInner_SelectedIndexChanged(object sender, EventArgs e)
        {
            _selectedLineCap = (LineCap)comboBoxInner.SelectedItem;
            // 触发事件
            if (LineCapChanged != null)
            {
                LineCapChanged(_selectedLineCap);
            }
        } 
    }
}
