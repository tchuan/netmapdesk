﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Symbol;
using System.Drawing;

namespace NetMap.Symbol
{
    /// <summary>
    /// 符号工厂
    /// </summary>
    public class SymbolFactory
    {
        #region 类别

        public const string S_NULL = "空符号";
        public const string S_POINT_FONT = "字体点符号";
        public const string S_POINT_IMAGE = "图片点符号";
        public const string S_POINT_ARC = "弧段点符号";
        public const string S_POINT_BOX = "Box点符号";
        public const string S_POINT_POLYGON = "多边形点符号";
        public const string S_LINE_STROKE = "线划线符号";
        public const string S_FILL_SOLID = "单色填充符号";
        public const string S_TEXT = "文本符号";

        #endregion
        /// <summary>
        /// 通过类别描述获取符号默认新实例
        /// </summary>
        /// <param name="kind"></param>
        /// <returns></returns>
        public static ISymbol CreateSymbol(string kind)
        {
            switch (kind)
            {
                case S_POINT_FONT:
                    {
                        return new FontPointSymbol();
                    }
                case S_LINE_STROKE:
                    {
                        return new StrokeSymbol();
                    }
                case S_FILL_SOLID:
                    {
                        return new SolidFillSymbol();
                    }
                case S_TEXT:
                    {
                        return new TextSymbol();
                    }
                default:
                    {
                        return null;
                    }
            }
        }

        /// <summary>
        /// 通过符号类型获取默认符号实例
        /// </summary>
        /// <param name="sType"></param>
        /// <returns></returns>
        public static ISymbol GetDefaultSymbol(SymbolType sType)
        {
            switch (sType)
            {
                case SymbolType.GEO_SYMBOL_POINT:
                    {
                        FontPointSymbol symbol = new FontPointSymbol();
                        symbol.Size = 20;
                        symbol.Name = "默认点符号";
                        symbol.Font.Color = Color.Black;
                        return symbol;
                    }
                case SymbolType.GEO_SYMBOL_LINE:
                    {
                        StrokeSymbol symbol = new StrokeSymbol();
                        symbol.Name = "默认线符号";
                        symbol.Pen.Color = Color.Black;
                        return symbol;
                    }
                case SymbolType.GEO_SYMBOL_FILL:
                    {
                        SolidFillSymbol symbol = new SolidFillSymbol();
                        symbol.Name = "默认面符号";
                        symbol.Color = Color.White;
                        symbol.OutLine = new StrokeSymbol();
                        return symbol;
                    }
                case SymbolType.GEO_SYMBOL_TEXT:
                    {
                        TextSymbol symbol = new TextSymbol();
                        symbol.Name = "默认文本符号";
                        symbol.Width = 40;
                        symbol.Height = 40;
                        symbol.Color = Color.Black;
                        return symbol;
                    }
                case SymbolType.GEO_SYMBOL_NULL:
                    {
                        return null;
                    }
                default:
                    {
                        throw new NotSupportedException();
                    }
            }
        }
    }
}
