﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;
using System.Drawing;

namespace NetMap.Interfaces.Symbol
{
    /// <summary>
    /// 多边形点符号接口
    /// </summary>
    public interface IPolygonPointSymbol : IPointSymbol
    {
        /// <summary>
        /// 获取和设置线宽
        /// </summary>
        float Width { get; set; }
        /// <summary>
        /// 获取和设置是否有边线
        /// </summary>
        bool Outline { get; set; }
        /// <summary>
        /// 获取和设置是否有填充
        /// </summary>
        bool Fill { get; set; }
        /// <summary>
        /// 获取和设置填充色
        /// </summary>
        Color FillColor { get; set; }
        /// <summary>
        /// 获取和设置多边形
        /// </summary>
        IPolygon Polygon { get; set; }
    }
}
