﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Symbol
{
    /// <summary>
    /// 渐变面符号接口
    /// 备注：参考GDI+的LinearGradientBrush属性，较复杂，暂未定义完成
    ///       类似的PathGradientBrush未定义
    /// </summary>
    public interface ILinearGradientFillSymbol : IFillSymbol
    {
    }
}
