﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NetMap.Core;
using NetMap.Interfaces.Geometry;

namespace NetMap.Geometry
{
    public class Path : Geometry, IPath
    {
        private List<IPathPoint> _pts = null;

        /// <summary>
        /// 空构造
        /// </summary>
        public Path()
        {
            _pts = new List<IPathPoint>();
            IsClose = false;
        }

        /// <summary>
        /// 完整数据的构造
        /// </summary>
        /// <param name="pts"></param>
        /// <param name="box"></param>
        public Path(List<IPathPoint> pts, IEnvelope box)
        {
            _pts = pts;
            _box = new Envelope(box);
            IsClose = false;
        }

        public override geoGeometryType GeometryType
        {
            get { return geoGeometryType.GEO_GEOMETRY_PATH; }
        }

        public override void UpdateEnvelope()
        {
            if (_pts.Count == 0)
            {
                _box = null;
                return;
            }
            foreach (IPathPoint point in _pts)
            {
                point.UpdateEnvelope();
            }
            if (_box == null)
            {
                _box = new Envelope(_pts[0]);
            }
            else
            {
                _box.Update(_pts[0].Envelope);
            }
            foreach(IPathPoint point in _pts)
            {
                _box.Union(point);
            }
        }

        #region IList<IPathPoint> 成员

        public int IndexOf(IPathPoint item)
        {
            return _pts.IndexOf(item);
        }

        public void Insert(int index, IPathPoint item)
        {
            _pts.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            _pts.RemoveAt(index);
        }

        public IPathPoint this[int index]
        {
            get
            {
                return _pts[index];
            }
            set
            {
                _pts[index] = value;
            }
        }

        #endregion

        #region ICollection<IPathPoint> 成员

        public void Add(IPathPoint item)
        {
            _pts.Add(item);
        }

        public void Clear()
        {
            _pts.Clear();
        }

        public bool Contains(IPathPoint item)
        {
            return _pts.Contains(item);
        }

        public void CopyTo(IPathPoint[] array, int arrayIndex)
        {
            _pts.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _pts.Count; }
        }

        public bool IsReadOnly
        {
            get { return true; }
        }

        public bool Remove(IPathPoint item)
        {
            return _pts.Remove(item);
        }

        #endregion

        #region IEnumerable<IPoint> 成员

        public IEnumerator<IPathPoint> GetEnumerator()
        {
            foreach (IPathPoint point in _pts)
            {
                yield return point;
            }
        }

        #endregion

        #region IEnumerable 成员

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            foreach (IPathPoint point in _pts)
            {
                yield return point;
            }
        }

        #endregion

        #region IPath 成员

        public IPathPoint StartPoint
        {
            get { return _pts.First(); }
        }

        public IPathPoint EndPoint
        {
            get 
            {
                if (IsClose)
                {
                    return StartPoint;
                }
                else
                {
                    return _pts.Last();
                }
            }
        }

        public bool IsClose
        {
            get;
            set;
        }

        #endregion

        public override IGeometry CloneGeometry()
        {
            IPath geometry = new Path(_pts, this.Envelope);
            return geometry;
        }

        public override byte[] ExportToWkb()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    writer.Write(_pts.Count);
                    foreach (IPathPoint point in _pts)
                    {
                        point.Export(writer);
                    }
                }
                return stream.ToArray();
            }
        }

        public override string ExportToWkt()
        {
            throw new NotImplementedException();
        }

        public override void ImportFromWkb(byte[] data)
        {
            using (MemoryStream stream = new MemoryStream(data))
            {
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    int nCount = reader.ReadInt32();
                    for (int i = 0; i < nCount; i++)
                    {
                        PathPoint point = new PathPoint();
                        point.Import(reader);
                        _pts.Add(point);
                    }
                }
            }
        }

        public override void ImportFromWkt(string data)
        {
            throw new NotImplementedException();
        }

        public override void Export(BinaryWriter writer)
        {
            writer.Write(_pts.Count);
            foreach (IPathPoint point in _pts)
            {
                point.Export(writer);
            }
        }

        //读取时认为点全部是PathPoint
        public override void Import(BinaryReader reader)
        {
            int nCount = reader.ReadInt32();
            for (int i = 0; i < nCount; i++)
            {
                IPathPoint point = new PathPoint();
                point.Import(reader);
                _pts.Add(point);
            }
        }
    }
}
