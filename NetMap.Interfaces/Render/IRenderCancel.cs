﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Render
{
    public delegate bool RenderCancelDelegate();

    /// <summary>
    /// 绘图控制接口
    /// </summary>
    public interface IRenderCancel
    {
        RenderCancelDelegate Cancel { get; set; }
    }
}
