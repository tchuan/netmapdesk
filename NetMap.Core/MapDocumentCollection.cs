﻿using NetMap.Interfaces;
using NetMap.Interfaces.Persist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace NetMap.Core
{
    public class MapDocumentCollection : IMapDocumentCollection, IPersistResource
    {
        private List<IMapDocument> _mapLst = new List<IMapDocument>();

        public void Add(IMapDocument map)
        {
            if (_mapLst.IndexOf(map) < 0)
            {
                _mapLst.Add(map);
            }
        }

        public IMapDocument Find(string name)
        {
            foreach (IMapDocument map in _mapLst)
            {
                if (map.Name == name)
                {
                    return map;
                }
            }
            return null;
        }

        public void Remove(IMapDocument map)
        {
            _mapLst.Remove(map);
        }

        public void Clear()
        {
            _mapLst.Clear();
        }

        public IEnumerator<IMapDocument> GetEnumerator()
        {
            return _mapLst.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _mapLst.GetEnumerator();
        }

        #region IPersistResource 接口

        public void Save(XmlElement node, XmlDocument document)
        {
            foreach (IMapDocument map in _mapLst)
            {
                IPersistResource mapPR = map as IPersistResource;
                mapPR.Save(node, document);
            }
        }

        public void Load(XmlReader reader, object context)
        {
            while (reader.Read())
            {
                if(reader.NodeType == XmlNodeType.Element)
                {
                    switch (reader.Name)
                    {
                        case "Map":
                            {
                                IMapDocument map = new MapDocument();
                                IPersistResource mapPR = map as IPersistResource;
                                mapPR.Load(reader, null);
                                _mapLst.Add(map);
                                break;
                            }
                        case "WebMap":
                            {
                                throw new NotImplementedException();
                            }
                    }
                    break;
                }
                else if (reader.NodeType == XmlNodeType.EndElement
                    && reader.Name == "Maps")
                {
                    return;
                }
            }
        }

        #endregion
    }
}
