﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces;
using NetMap.Data;
using NetMap.Interfaces.Geometry;
using NetMap.Application.Core.WinForms;
using NetMap.Application.Core;
using NetMap.Interfaces.Data;
using NetMap.Application;

namespace StoneGround.Search.Commands
{
    public class ZoneSearch : AbstractComboBoxCommand
    {
        public static ZoneSearch Instance;

        public ZoneSearch()
        {
            Instance = this;
        }

        public override void Run()
        {
            //do nothing
        }

        public void DzoneChange(string text)
        {
            SearchPad.Instance.Condition.IsZone = false;
            SearchPad.Instance.Condition.Zone = "";

            var comboBox = (ToolBarComboBox)base.ComboBox;
            comboBox.Items.Clear();
            comboBox.Items.Add("无");
            if (text != "无")
            {
                //查找地级市
                IQueryFilter filter = new QueryFilter();
                filter.Fields.Add(SpatialDatabaseService.FIELD_DZONE_ADCODE);
                filter.WhereClause = SpatialDatabaseService.FIELD_DZONE_NAME + "='" + text + "'";
                IFeatureCursor cursor = SpatialDatabaseService.Instance.DzoneFeatureClass.Search(filter);
                IFeature feature = cursor.Next();
                cursor.Close();
                //查找县级市
                if (feature != null)
                {
                    string adcode = feature.GetValue(0).ToString();
                    IQueryFilter zoneFilter = new QueryFilter();
                    zoneFilter.Fields.Add(SpatialDatabaseService.FIELD_ZONE_NAME);
                    zoneFilter.WhereClause = SpatialDatabaseService.FIELD_ZONE_ADCODE + " like '" + adcode + "%'";
                    IFeatureCursor zoneCursor = SpatialDatabaseService.Instance.ZoneFeatureClass.Search(zoneFilter);
                    IFeature zoneFeature = zoneCursor.Next();
                    while (zoneFeature != null)
                    {
                        string name = zoneFeature.GetValue(0).ToString();
                        comboBox.Items.Add(name);
                        zoneFeature = zoneCursor.Next();
                    }
                    zoneCursor.Close();
                }
            }
            comboBox.SelectedIndex = 0;

            IMapView view = WorkspaceSingleton.ActiveMapView;
            ILayer layer = view.MapDocument.FindLayer(StoneGroundDefines.LAYER_NAME_ZONE);
            IFeatureSelection selection = layer as IFeatureSelection;
            selection.Clear();
        }

        protected override void OnOwnerChanged(EventArgs e)
        {
            base.OnOwnerChanged(e);

            var comboBox = (ToolBarComboBox)base.ComboBox;
            comboBox.AutoSize = false; //调整宽度
            comboBox.Width = 150;
            comboBox.Items.Add("无");
            comboBox.SelectedIndex = 0;
            comboBox.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);
        }

        void comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ToolBarComboBox comboBox = (ToolBarComboBox)base.ComboBox;
            string text = comboBox.Text;
            IMapView view = WorkspaceSingleton.ActiveMapView;
            ILayer layer = view.MapDocument.FindLayer(StoneGroundDefines.LAYER_NAME_ZONE);
            IFeatureSelection selection = layer as IFeatureSelection;
            if (text == "无")
            {
                selection.Clear();
                SearchPad.Instance.Condition.IsZone = false;
                SearchPad.Instance.Condition.Zone = "";
            }
            else
            {
                IQueryFilter filter = new QueryFilter();
                filter.WhereClause = SpatialDatabaseService.FIELD_ZONE_NAME + "='" + text + "'";
                selection.Select(filter, geoSelectionResultEnum.GEO_SELECTIONRESULT_NEW);
                SearchPad.Instance.Condition.IsZone = true;
                SearchPad.Instance.Condition.Zone = text;
            }
            if (selection.SelectionSet != null)
            {
                IEnvelope box = selection.SelectionSet.Envelope;
                if (box != null)
                {
                    //外扩1/2后，缩放
                    IPoint center = box.Center;
                    IEnvelope boxZoom = new NetMap.Geometry.Envelope(center.X - box.Width, center.Y - box.Height,
                        center.X + box.Width, center.Y + box.Height);
                    view.ZoomToData(boxZoom);
                }
            }
            view.Invalidate();
        }
    }
}
