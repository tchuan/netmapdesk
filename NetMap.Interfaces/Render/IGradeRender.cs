﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Symbol;
using NetMap.Interfaces.Data;

namespace NetMap.Interfaces.Render
{
    /// <summary>
    /// 分级图层方案接口
    /// </summary>
    public interface IGradeRender : ILayerRender
    {
        /// <summary>
        /// 获取和设置参考属性字段
        /// </summary>
        string Field { get; set; }
        /// <summary>
        /// 获取和设置缺省符号
        /// </summary>
        ISymbol DefaultSymbol { get; set; }
        /// <summary>
        /// 获取和设置是否表示缺省符号
        /// </summary>
        bool IsUseDefaultSymbol { get; set; }
        /// <summary>
        /// 获取值范围的集合
        /// </summary>
        IEnumerable<IValueRange> ValueRanges { get; }
        /// <summary>
        /// 设置值范围对应的符号
        /// 备注：不存在此值范围时，新增
        ///       已存在此值范围时，更新
        /// </summary>
        /// <param name="vr"></param>
        /// <param name="symbol"></param>
        void SetSymbol(IValueRange vr, ISymbol symbol);
        /// <summary>
        /// 获取值对应的符号
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        ISymbol GetSymbol(object value);
    }
}
