﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using NetMap.Interfaces.Render;
using NetMap.UI.Properties;
using NetMap.Interfaces;

namespace NetMap.Tools
{
    /// <summary>
    /// 放大工具
    /// </summary>
    public class ZoomIn : Tool
    {
        private IDisplayTransformation _disTrans = null;

        public ZoomIn()
            : base("放大")
        {
            _icon = Resources.放大;
        }

        public ZoomIn(IMapEditView view)
            : base("放大", view)
        {
            _icon = Resources.放大;
        }

        public override void GainFocus()
        {
            _disTrans = View.DisplayTransformation;
            base.GainFocus();
        }

        public override void LostFocus()
        {
            _disTrans = null;
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            double x = 0, y = 0;
            _disTrans.ToMapPoint(e.X, e.Y, ref x, ref y);
            double scale =_disTrans.ScaleRatio * 2;
            View.ZoomToCenter(x, y, scale);
            View.Refresh();
        }
    }
}
