﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap;
using System.Windows.Forms;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Data;
using NetMap.Data;
using NetMap.Geometry;
using NetMap.UI.Tools;
using NetMap.Interfaces;
using NetMap.Application.Gui.Pads;
using StoneGround.Dialog;

namespace StoneGround.Edit
{
    /// <summary>
    /// 料场信息编辑
    /// </summary>
    public class StoneGroundEditTool : Tool
    {
        public StoneGroundEditTool()
            : base("料场信息编辑")
        {
        }

        public StoneGroundEditTool(IMapEditView view)
            : base("料场信息编辑", view)
        {
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                const int tolerance = 10;
                double x = 0, y = 0;
                IDisplayTransformation displayTranformation = View.DisplayTransformation;
                displayTranformation.ToMapPoint(e.X, e.Y, ref x, ref y);

                IFeatureClass featureClass = StoneGroundDatabaseService.Instance.StoneGroundFeatureClass;
                ISpatialFilter filter = new SpatialFilter();
                filter.Geometry = new Circle(x, y, tolerance / displayTranformation.ScaleRatio);
                filter.SpatialRelationship = geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_INTERSECTS;
                IFields fields = featureClass.Fields;
                for (int i = 0; i < fields.Count; i++)
                {
                    IField field = fields.GetField(i);
                    filter.Fields.Add(field.Name);
                }

                IFeatureCursor cursor = featureClass.Search(filter);
                IFeature feature = cursor.Next();
                cursor.Close();
                if (feature != null)
                {
                    //通用的GIS数据属性编辑
                    //AttributeEditForm attEdit = new AttributeEditForm();
                    //attEdit.Init(feature);
                    //attEdit.Show();

                    /*FrmSGInfo dialog = new FrmSGInfo();
                    dialog.StartPosition = FormStartPosition.CenterParent;
                    dialog.Init(feature, true);
                    dialog.ShowDialog(NetMap.Application.Gui.WorkbenchSingleton.MainWin32Window);*/
                    FrmSGInfo2 dialog = new FrmSGInfo2();
                    dialog.StartPosition = FormStartPosition.CenterParent;
                    dialog.InitModify(feature);
                    dialog.ShowDialog(NetMap.Application.Gui.WorkbenchSingleton.MainWin32Window);
                }                
            }
        }
    }

    /// <summary>
    /// 编辑料场属性
    /// </summary>
    public class EditCommand : MapCommand
    {
        public EditCommand()
        {
            InnerCmd = new StoneGroundEditTool();
        }
    }
}
