﻿using System;
using System.Collections.Generic;
using System.Windows.Interop;

namespace NetMap.Application.Gui
{
    /// <summary>
    /// Runs workbench initialization.
    /// </summary>
    public class WorkbenchStartup
    {
        const string workbenchMemento = "WorkbenchMemento";
        App app;

        public void InitializeWorkbench()
        {
            app = new App();
            System.Windows.Forms.Integration.WindowsFormsHost.EnableWindowsFormsInterop();
            ComponentDispatcher.ThreadIdle -= ComponentDispatcher_ThreadIdle; // ensure we don't register twice
            ComponentDispatcher.ThreadIdle += ComponentDispatcher_ThreadIdle;
            WorkbenchSingleton.InitializeWorkbench(new WpfWorkbench(), new AvalonDockLayout());
        }

        static void ComponentDispatcher_ThreadIdle(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.RaiseIdle(e);
        }

        public void Run()
        {
            app.Run(WorkbenchSingleton.MainWindow);
        }
    }
}
