﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Format;
using NetMap.Interfaces.Symbol;

namespace NetMap.Symbol
{
    /// <summary>
    /// 复合填充符号
    /// </summary>
    public class MultiFillSymbol : FillSymbol, IMultiFillSymbol, IBinaryFormat
    {
        private List<IFillSymbol> _items = new List<IFillSymbol>();

        public MultiFillSymbol()
        {
        }

        public MultiFillSymbol(MultiFillSymbol symbol)
        {
            //TODO
        }

        public override ISymbol Clone()
        {
            return new MultiFillSymbol(this);
        }

        public override string KindAbstract
        {
            get { return "复合填充符号"; }
        }

        #region IMultiFillSymbol 成员

        public int IndexOf(IFillSymbol item)
        {
            return _items.IndexOf(item);
        }

        public void Insert(int index, IFillSymbol item)
        {
            _items.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            _items.RemoveAt(index);
        }

        public IFillSymbol this[int index]
        {
            get
            {
                return _items[index];
            }
            set
            {
                _items[index] = value;
            }
        }

        public void Add(IFillSymbol item)
        {
            _items.Add(item);
        }

        public void Clear()
        {
            _items.Clear();
        }

        public bool Contains(IFillSymbol item)
        {
            return _items.Contains(item);
        }

        public void CopyTo(IFillSymbol[] array, int arrayIndex)
        {
            _items.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _items.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(IFillSymbol item)
        {
            return _items.Remove(item);
        }

        public IEnumerator<IFillSymbol> GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        #endregion

        #region IBinaryFormat 成员

        public byte[] ToWkb()
        {
            MemoryStream stream = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(stream);

            writer.Write(Name);
            writer.Write(Code);

            writer.Write(Count);
            foreach (IFillSymbol item in this)
            {
                writer.Write(item.KindAbstract);
                IBinaryFormat format = item as IBinaryFormat;
                byte[] wkb = format.ToWkb();
                writer.Write(wkb.Count());
                writer.Write(wkb);
            }

            return stream.ToArray();
        }

        public void FromWkb(byte[] wkb)
        {
            MemoryStream stream = new MemoryStream(wkb);
            BinaryReader reader = new BinaryReader(stream);

            Name = reader.ReadString();
            Code = reader.ReadInt32();

            int count = reader.ReadInt32();
            for (int i = 0; i < count; i++)
            {
                string sk = reader.ReadString();
                ISymbol symbol = SymbolFactory.CreateSymbol(sk);
                int icount = reader.ReadInt32();
                byte[] iwkb = reader.ReadBytes(icount);
                IBinaryFormat format = symbol as IBinaryFormat;
                format.FromWkb(iwkb);
                Add(symbol as IFillSymbol);
            }
        }

        #endregion
    }
}
