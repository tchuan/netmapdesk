﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NetMap.Core;
using NetMap.Interfaces.Geometry;

namespace NetMap.Geometry
{
    public class MultiPolygon : Geometry, IMultiPolygon
    {
        private List<IPolygon> _polygons = null;

        public MultiPolygon()
        {
            _polygons = new List<IPolygon>();
        }

        public MultiPolygon(List<IPolygon> polygons, IEnvelope box)
        {
            _polygons = polygons;
            _box = box;
        }

        public override geoGeometryType GeometryType
        {
            get { return geoGeometryType.GEO_GEOMETRY_MULTIPOLYGON; }
        }

        public override void UpdateEnvelope()
        {
            if (_polygons.Count == 0)
            {
                _box = null;
                return;
            }
            foreach (IPolygon polygon in _polygons)
            {
                polygon.UpdateEnvelope();
            }
            if (_box == null)
            {
                _box = new Envelope(_polygons[0].Envelope);
            }
            else
            {
                _box.Update(_polygons[0].Envelope);
            }
            foreach (IPolygon polygon in _polygons)
            {
                _box.Union(polygon.Envelope);
            }
        }

        #region IList<IPolygon> 成员

        public int IndexOf(IPolygon item)
        {
            return _polygons.IndexOf(item);
        }

        public void Insert(int index, IPolygon item)
        {
            _polygons.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            _polygons.RemoveAt(index);
        }

        public IPolygon this[int index]
        {
            get
            {
                return _polygons[index];
            }
            set
            {
                _polygons[index] = value;
            }
        }

        #endregion

        #region ICollection<IPolygon> 成员

        public void Add(IPolygon item)
        {
            _polygons.Add(item);
        }

        public void Clear()
        {
            _polygons.Clear();
        }

        public bool Contains(IPolygon item)
        {
            return _polygons.Contains(item);
        }

        public void CopyTo(IPolygon[] array, int arrayIndex)
        {
            _polygons.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _polygons.Count; }
        }

        public bool IsReadOnly
        {
            get { return true; }
        }

        public bool Remove(IPolygon item)
        {
            return _polygons.Remove(item);
        }

        #endregion

        #region IEnumerable<IPolygon> 成员

        public IEnumerator<IPolygon> GetEnumerator()
        {
            foreach (IPolygon polygon in _polygons)
            {
                yield return polygon;
            }
        }

        #endregion

        #region IEnumerable 成员

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            foreach (IPolygon polygon in _polygons)
            {
                yield return polygon;
            }
        }

        #endregion

        public override IGeometry CloneGeometry()
        {
            IMultiPolygon geometry = new MultiPolygon(_polygons, this.Envelope);
            return geometry;
        }

        public override byte[] ExportToWkb()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    writer.Write(_polygons.Count);
                    foreach (IPolygon polygon in _polygons)
                    {
                        polygon.Export(writer);
                    }
                }
                return stream.ToArray();
            }
        }

        public override string ExportToWkt()
        {
            throw new NotImplementedException();
        }

        public override void ImportFromWkb(byte[] data)
        {
            using (MemoryStream stream = new MemoryStream(data))
            {
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    int count = reader.ReadInt32();
                    for (int i = 0; i < count; i++)
                    {
                        IPolygon polygon = new Polygon();
                        polygon.Import(reader);
                        _polygons.Add(polygon);
                    }
                }
            }
        }

        public override void ImportFromWkt(string data)
        {
            throw new NotImplementedException();
        }

        public override void Export(BinaryWriter writer)
        {
            writer.Write(_polygons.Count);
            foreach (IPolygon polygon in _polygons)
            {
                polygon.Export(writer);
            }
        }

        public override void Import(BinaryReader reader)
        {
            int count = reader.ReadInt32();
            for (int i = 0; i < count; i++)
            {
                IPolygon polygon = new Polygon();
                polygon.Import(reader);
            }
        }
    }
}
