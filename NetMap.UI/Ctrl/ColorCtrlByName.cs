﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetMap.UI.Ctrl
{
    public partial class ColorCtrlByName : UserControl
    {
        public delegate void ColorChangedHandle(Color color);
        public event ColorChangedHandle ColorChangedEvent;
        public ColorCtrlByName()
        {
            InitializeComponent();
            InitItems();
            comboBox1.DrawItem += new DrawItemEventHandler(comboBox1_DrawItem);
        }
        public string SelectColorName
        {
            get { return comboBox1.Text; }
        }
        public Color SelectColor
        {
            get { return Color.FromName(comboBox1.Text); }
        }
        void comboBox1_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index >= 0)//判断是否需要重绘
            {
                string colorName = comboBox1.Items[e.Index].ToString();//获取颜色名
                SolidBrush brush = new SolidBrush(Color.FromName(colorName));//定义画刷
                Font font = new Font("微软雅黑", 9);//定义字体
                Rectangle rect = e.Bounds;
                rect.Inflate(-2, -2);
                Rectangle rectColor = new Rectangle(rect.Location, new Size(20, rect.Height));
                e.Graphics.FillRectangle(brush, rectColor);//填充颜色
                e.Graphics.DrawRectangle(Pens.Black, rectColor);//绘制边框
                e.Graphics.DrawString(colorName, font, Brushes.Black, (rect.X + 22), rect.Y);//绘制文字
            }

        }
        private void InitItems()
        {
            comboBox1.DrawMode = DrawMode.OwnerDrawFixed;//手动绘制所有元素
            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;//下拉框样式设置为不能编辑
            comboBox1.Items.Clear();//清空原有项
            Array allColors = Enum.GetValues(typeof(KnownColor));//获取系统颜色名存入列表
            foreach (KnownColor var in allColors)
            {
                comboBox1.Items.Add(var.ToString()); //加载该选项框的子项
            }
            comboBox1.SelectedIndex = 0;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = comboBox1.SelectedIndex;
            if (index >= 0)
            {
                if (ColorChangedEvent != null) { ColorChangedEvent(Color.FromName(Enum.GetValues(typeof(KnownColor)).GetValue(index).ToString())); }
            }
        }
        
    }
}
