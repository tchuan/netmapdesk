﻿namespace NetMap.UI
{
    partial class LayerProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnApply = new System.Windows.Forms.Button();
            this.labelMinScale = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.checkBoxVisible = new System.Windows.Forms.CheckBox();
            this.checkBoxSelectable = new System.Windows.Forms.CheckBox();
            this.checkBoxEditable = new System.Windows.Forms.CheckBox();
            this.labelMaxScale = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxMaxScale = new System.Windows.Forms.TextBox();
            this.textBoxMinScale = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(186, 102);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(92, 41);
            this.btnApply.TabIndex = 0;
            this.btnApply.Text = "应用";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // labelMinScale
            // 
            this.labelMinScale.AutoSize = true;
            this.labelMinScale.Location = new System.Drawing.Point(20, 125);
            this.labelMinScale.Name = "labelMinScale";
            this.labelMinScale.Size = new System.Drawing.Size(65, 12);
            this.labelMinScale.TabIndex = 1;
            this.labelMinScale.Text = "最小比例尺";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(64, 15);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(208, 21);
            this.textBoxName.TabIndex = 2;
            // 
            // checkBoxVisible
            // 
            this.checkBoxVisible.AutoSize = true;
            this.checkBoxVisible.Location = new System.Drawing.Point(22, 54);
            this.checkBoxVisible.Name = "checkBoxVisible";
            this.checkBoxVisible.Size = new System.Drawing.Size(48, 16);
            this.checkBoxVisible.TabIndex = 3;
            this.checkBoxVisible.Text = "可见";
            this.checkBoxVisible.UseVisualStyleBackColor = true;
            // 
            // checkBoxSelectable
            // 
            this.checkBoxSelectable.AutoSize = true;
            this.checkBoxSelectable.Location = new System.Drawing.Point(117, 54);
            this.checkBoxSelectable.Name = "checkBoxSelectable";
            this.checkBoxSelectable.Size = new System.Drawing.Size(48, 16);
            this.checkBoxSelectable.TabIndex = 3;
            this.checkBoxSelectable.Text = "可选";
            this.checkBoxSelectable.UseVisualStyleBackColor = true;
            // 
            // checkBoxEditable
            // 
            this.checkBoxEditable.AutoSize = true;
            this.checkBoxEditable.Enabled = false;
            this.checkBoxEditable.Location = new System.Drawing.Point(212, 54);
            this.checkBoxEditable.Name = "checkBoxEditable";
            this.checkBoxEditable.Size = new System.Drawing.Size(60, 16);
            this.checkBoxEditable.TabIndex = 3;
            this.checkBoxEditable.Text = "可编辑";
            this.checkBoxEditable.UseVisualStyleBackColor = true;
            // 
            // labelMaxScale
            // 
            this.labelMaxScale.AutoSize = true;
            this.labelMaxScale.Location = new System.Drawing.Point(20, 93);
            this.labelMaxScale.Name = "labelMaxScale";
            this.labelMaxScale.Size = new System.Drawing.Size(65, 12);
            this.labelMaxScale.TabIndex = 1;
            this.labelMaxScale.Text = "最大比例尺";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "层名";
            // 
            // textBoxMaxScale
            // 
            this.textBoxMaxScale.Location = new System.Drawing.Point(95, 90);
            this.textBoxMaxScale.Name = "textBoxMaxScale";
            this.textBoxMaxScale.Size = new System.Drawing.Size(85, 21);
            this.textBoxMaxScale.TabIndex = 4;
            // 
            // textBoxMinScale
            // 
            this.textBoxMinScale.Location = new System.Drawing.Point(94, 122);
            this.textBoxMinScale.Name = "textBoxMinScale";
            this.textBoxMinScale.Size = new System.Drawing.Size(85, 21);
            this.textBoxMinScale.TabIndex = 4;
            // 
            // LayerProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 160);
            this.Controls.Add(this.textBoxMinScale);
            this.Controls.Add(this.textBoxMaxScale);
            this.Controls.Add(this.checkBoxEditable);
            this.Controls.Add(this.checkBoxSelectable);
            this.Controls.Add(this.checkBoxVisible);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelMaxScale);
            this.Controls.Add(this.labelMinScale);
            this.Controls.Add(this.btnApply);
            this.Name = "LayerProperties";
            this.Text = "图层属性";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Label labelMinScale;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.CheckBox checkBoxVisible;
        private System.Windows.Forms.CheckBox checkBoxSelectable;
        private System.Windows.Forms.CheckBox checkBoxEditable;
        private System.Windows.Forms.Label labelMaxScale;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxMaxScale;
        private System.Windows.Forms.TextBox textBoxMinScale;
    }
}