﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using User;
using NetMap.Application.Core;
using System.IO;

namespace MapDesk
{
    public partial class FrmLogin : Form
    {
        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();
        [DllImport("user32.dll")]
        public static extern bool SendMessage(IntPtr hwnd, int wMsg, int wParam, int lParam);

        public const int WM_SYSCOMMAND = 0x0112;
        public const int SC_MOVE = 0xF010;
        public const int HTCAPTION = 0x0002;

        private bool _isUserInputPwd = false; //是否是用户输入的密码

        public FrmLogin()
        {
            InitializeComponent();

            //启动用户服务
            UserService.Instance.Init();

            // 加载登录配置
            LoginConfig cfg = LoadLoginConfig();
            if (cfg != null)
            {
                if (cfg.IsKeepLogin)
                {
                    checkBox1.Checked = true;
                    textBoxId.Text = cfg.UserId;
                    textBoxPwd.Text = cfg.UserPwd;
                }
            }

            panel1.MouseDown +=new MouseEventHandler(panel1_MouseDown);
            textBoxPwd.KeyDown += new KeyEventHandler(textBoxPwd_KeyDown);
            textBoxPwd.KeyPress += new KeyPressEventHandler(textBoxPwd_KeyPress);
            textBoxPwd.KeyUp += new KeyEventHandler(textBoxPwd_KeyUp);
            this.Activated += new EventHandler(FrmLogin_Activated);
        }

        #region 密码框

        void textBoxPwd_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Return)
            {
                base.OnKeyUp(e);
            }
            else
            {
                e.Handled = true;
            }
        }

        void textBoxPwd_KeyPress(object sender, KeyPressEventArgs e)
        {            
            if (e.KeyChar != '\r')
            {
                _isUserInputPwd = true;
                base.OnKeyPress(e);
            }
            else
            {                
                e.Handled = true;
            }        
        }

        void textBoxPwd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Login(textBoxId.Text, textBoxPwd.Text);
                e.Handled = true;
            }
            else
            {
                base.OnKeyDown(e);
            }
        }

        #endregion

        #region 事件处理

        void FrmLogin_Activated(object sender, EventArgs e)
        {
            textBoxId.Focus(); //让账号具有焦点
        }

        /// <summary>
        /// 是否登陆
        /// </summary>
        public bool IsLogin
        {
            get;
            private set;
        }

        void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, WM_SYSCOMMAND, SC_MOVE + HTCAPTION, 0); 
        }

        private void Login(string user, string pwd)
        {
            if (string.IsNullOrEmpty(user)
            || string.IsNullOrEmpty(pwd))
            {
                MessageBox.Show("用户名或密码错误，请输入正确的账号密码进行登录", "提示");
                return;
            }
            if (_isUserInputPwd)
            {
                pwd = UserService.Instance.PwdEncrypt(pwd);
            }
            SystemUser sysUser = UserService.Instance.Login(user, pwd);
            if (sysUser == null)
            {
                MessageBox.Show("用户名或密码错误，请输入正确的账号密码进行登录", "提示");
                return;
            }
            else
            {
                UserService.Instance.CurrentUser = sysUser;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
                //保持登录配置
                LoginConfig cfg = new LoginConfig();
                cfg.IsKeepLogin = checkBox1.Checked;
                cfg.UserId = user;
                cfg.UserPwd = pwd;
                SaveLoginConfig(cfg);
            }
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            Login(textBoxId.Text, textBoxPwd.Text);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        #endregion

        #region 配置

        private const string LOGIN_CONFIG = "login.cfg";

        // 加载登录配置
        private LoginConfig LoadLoginConfig()
        {
            string file = Path.Combine(Application.StartupPath, LOGIN_CONFIG);
            if (File.Exists(file))
            {
                FileStream stream = new FileStream(file, FileMode.Open,
                    FileAccess.Read, FileShare.None);
                System.Xml.Serialization.XmlSerializer deserializer = new System.Xml.Serialization.XmlSerializer(typeof(LoginConfig));
                LoginConfig cfg = deserializer.Deserialize(stream) as LoginConfig;
                stream.Close();
                return cfg;
            }
            else
            {
                return null;
            }
        }

        // 保持登录配置
        private void SaveLoginConfig(LoginConfig cfg)
        {
            string file = Path.Combine(Application.StartupPath, LOGIN_CONFIG);
            FileStream stream = new FileStream(file, FileMode.Create,
                FileAccess.Write, FileShare.None);
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(LoginConfig));
            serializer.Serialize(stream, cfg);
            stream.Close();
        }

        #endregion

        //系统信息
        private void linkLabelSystemInfo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("云南省高速公路沥青路面碎石场管理系统").Append("\r\n");
            builder.Append("程序编制：谭巍").Append("\r\n");
            builder.Append("联系电话：023-62653475").Append("\r\n");
            builder.Append("电子邮件：tanwei@cmhk.com");
            string info = builder.ToString();
            MessageBox.Show(info, "系统信息");
        }
    }
}
