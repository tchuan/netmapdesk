﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Data;
using Community.CsharpSqlite;

namespace NetMap.Sqlite
{
    /// <summary>
    /// Sqlite数据库
    /// </summary>
    internal class SqliteDatabase : IDatabase
    {
        internal Sqlite3.sqlite3 _db;
        protected bool _open = false; //事务标志        

        internal SqliteDatabase()
        {
        }

        #region IDatabase 成员

        public string Name
        {
            get;
            set;
        }

        public string FactoryName
        {
            get;
            internal set;
        }

        public IConnectProperties ConnectProperties
        {
            get;
            internal set;
        }

        public ITable CreateTable(string name, IFields fields)
        {
            SqliteTable table = new SqliteTable();
            int err = table.create(_db, name, fields);
            if (err != Sqlite3.SQLITE_OK)
            {
                return null;
            }
            table.Database = this;
            return table;
        }

        public ITable OpenTable(string name)
        {
            SqliteTable table = new SqliteTable();
            table.open(_db, name);
            table.Database = this;
            return table;
        }

        public IRecordset OpenRecordset(string sql)
        {
            Sqlite3.Vdbe stmt = null;
            string tail = "";
            int err = Sqlite3.sqlite3_prepare_v2(_db, sql, sql.Length, ref stmt, ref tail);
            if (err != Sqlite3.SQLITE_OK)
            {
                return null;
            }
            SqliteRecordset recordset = new SqliteRecordset();
            recordset.Open(stmt);
            return recordset;
        }

        public void ExecuteSQL(string sql)
        {
            Sqlite3.Vdbe stmt = null;
            string tail = "";
            try
            {
                int err = Sqlite3.sqlite3_prepare_v2(_db, sql, sql.Length, ref stmt, ref tail);
                if (err != Sqlite3.SQLITE_OK)
                {
                    throw new NetMap.Data.SqlSyntaxException("sql语句语法错误");
                }
                else
                {
                    err = Sqlite3.sqlite3_step(stmt);
                    if (err != Sqlite3.SQLITE_DONE
                        && err != Sqlite3.SQLITE_ROW
                        && err != Sqlite3.SQLITE_OK)
                    {
                        throw new ApplicationException("执行sql错误");
                    }
                }
            }
            finally
            {
                Sqlite3.sqlite3_finalize(stmt);
            }
        }

        public bool ExecuteAndGetBool(string sql)
        {
            IRecordset recordset = OpenRecordset(sql);
            short value = (short)recordset.GetValue(0);
            return value == 1;
        }

        public int ExecuteAndGetInt(string sql)
        {
            IRecordset recordset = OpenRecordset(sql);
            return (int)recordset.GetValue(0);
        }

        public string ExecuteAndGetString(string sql)
        {
            IRecordset recordset = OpenRecordset(sql);
            return recordset.GetValue(0).ToString();
        }

        public int ExecuteAndGetId(string sql)
        {
            throw new NotImplementedException();
        }

        public byte[] ExecuteAndGetBytes(string sql)
        {
            IRecordset recordset = OpenRecordset(sql);
            if (recordset == null)
            {
                return null;
            }
            return (byte[])recordset.GetValue(0);
        }

        public double ExecuteAndGetDouble(string sql)
        {
            IRecordset recordset = OpenRecordset(sql);
            return (double)recordset.GetValue(0);
        }

        public DateTime ExecuteAndGetDatetime(string sql)
        {
            IRecordset recordset = OpenRecordset(sql);
            long value = (long)recordset.GetValue(0);
            return new DateTime(value);
        }

        public bool ExecuteUpdate(string sql)
        {
            throw new NotImplementedException();
        }

        public void StartTransaction()
        {
            if (_open) //已经开始事务
            {
                return;
            }
            ExecuteSQL("BEGIN");
            _open = true;
        }

        public void CommitTransaction()
        {
            if (_open)
            {
                ExecuteSQL("COMMIT");
                _open = false;
            }
        }

        public void RollTransaction()
        {
            if (_open)
            {
                ExecuteSQL("ROLLBACK");
                _open = false;
            }
        }

        public bool IsTransactioning
        {
            get { return _open; }
        }

        public void Redo()
        {
            throw new NotImplementedException();
        }

        public void Undo()
        {
            throw new NotImplementedException();
        }

        public bool Redoable
        {
            get { throw new NotImplementedException(); }
        }

        public bool Undoable
        {
            get { throw new NotImplementedException(); }
        }

        public void Close()
        {
            Sqlite3.sqlite3_close(_db);
        }

        #endregion

        /// <summary>
        /// 打开数据库
        /// </summary>
        /// <param name="db"></param>
        /// <param name="isNew">是否是新建的数据库</param>
        internal virtual void open(Sqlite3.sqlite3 db, bool isNew)
        {
            _db = db;
        }        
    }
}
