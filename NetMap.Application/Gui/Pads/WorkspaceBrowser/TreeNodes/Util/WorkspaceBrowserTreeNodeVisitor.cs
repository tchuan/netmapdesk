﻿// Copyright (c) AlphaSierraPapa for the KJSuite Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

using System;
using NetMap.Application.Core;

namespace NetMap.Application.WorkspaceBrowser.TreeNodes
{
	public class WorkspaceBrowserTreeNodeVisitor
	{
        public object Visit(AbstractWorkspaceBrowserTreeNode abstractWorkspaceBrowserTreeNode, object data)
		{
			LoggingService.Warn("Warning visited default Visit() for : " + abstractWorkspaceBrowserTreeNode);
            abstractWorkspaceBrowserTreeNode.AcceptChildren(this, data);
			return data;
		}
		
		public virtual object Visit(CustomFolderNode customFolderNode, object data)
		{
			customFolderNode.AcceptChildren(this, data);
			return data;
		}

        public virtual object Visit(SpatialDataSourceFolderNode spatialDataSourceFolderNode, object data)
        {
            spatialDataSourceFolderNode.AcceptChildren(this, data);
            return data;
        }

        public virtual object Visit(MapFolderNode mapFolderNode, object data)
        {
            mapFolderNode.AcceptChildren(this, data);
            return data;
        }

        public virtual object Visit(MapViewFolderNode mapViewFolderNode, object data)
        {
            mapViewFolderNode.AcceptChildren(this, data);
            return data;
        }
	}
}
