﻿using NetMap.Interfaces.Render;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Render
{
    public class RenderCancel : IRenderCancel
    {
        public RenderCancel()
        {
        }

        public RenderCancelDelegate Cancel
        {
            get;
            set;
        }
    }
}
