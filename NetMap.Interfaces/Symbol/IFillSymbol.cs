﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Symbol
{
    /// <summary>
    /// 面符号接口
    /// </summary>
    public interface IFillSymbol : ISymbol
    {
        /// <summary>
        /// 轮廓线符号
        /// </summary>
        ILineSymbol OutLine { get; set; }
    }
}
