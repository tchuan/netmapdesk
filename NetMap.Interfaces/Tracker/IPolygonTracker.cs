﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;

namespace NetMap.Interfaces.Tracker
{
    /// <summary>
    /// 多边形橡皮条接口
    /// </summary>
    public interface ICreatePolygonTracker
    {
        /// <summary>
        /// 加一个点
        /// </summary>
        /// <param name="point"></param>
        void AddPoint(IPoint point);
        /// <summary>
        /// 后退一点
        /// </summary>
        void BackPoint();
    }
}
