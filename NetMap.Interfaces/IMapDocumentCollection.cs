﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces
{
    /// <summary>
    /// 地图文档集合
    /// </summary>
    public interface IMapDocumentCollection : IEnumerable<IMapDocument>
    {
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="map"></param>
        void Add(IMapDocument map);
        /// <summary>
        /// 查找
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        IMapDocument Find(string name);
        /// <summary>
        /// 移除
        /// </summary>
        /// <param name="map"></param>
        void Remove(IMapDocument map);
        /// <summary>
        /// 清理
        /// </summary>
        void Clear();
    }
}
