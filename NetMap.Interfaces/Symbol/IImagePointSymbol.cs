﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace NetMap.Interfaces.Symbol
{
    /// <summary>
    /// 图片点符号
    /// </summary>
    public interface IImagePointSymbol : IPointSymbol
    {        
        /// <summary>
        /// 图片
        /// </summary>
        Image Image { get; set; }
        /// <summary>
        /// 锚点x，取值范围[0,1]
        /// </summary>
        double AnchorX { get; set; }
        /// <summary>
        /// 描点y，取值范围[0,1]
        /// </summary>
        double AnchorY { get; set; }
    }
}
