﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Symbol;
using System.Drawing.Drawing2D;
using NetMap.Geometry;

namespace NetMap.Render
{
    /// <summary>实体填充符号绘制</summary>
    internal class SolidFillSymbolDrawer : ISymbolDrawer
    {
        public void Draw(IDisplay display, ISymbol symbol, IGeometry geometry, string text)
        {
            ISolidFillSymbol solidFillSymbol = symbol as ISolidFillSymbol;            
            switch (geometry.GeometryType)
            {
                case geoGeometryType.GEO_GEOMETRY_POINT:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_LINESTRING:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_POLYGON:
                    {
                        DrawPolygon(display, symbol as ISolidFillSymbol, geometry as IPolygon);
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTIPOINT:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTILINESTRING:
                    {                      
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTIPOLYGON:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_PATH:
                    {
                        DrawPath(display, symbol as ISolidFillSymbol, geometry as IPath);
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_RECT:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_DIRECTIONPOINT:
                    {
                        break;
                    }
            } 
        }

        public void Draw(Graphics g, ISymbol symbol, IGeometry geometry, string text)
        {
            switch (geometry.GeometryType)
            {
                case geoGeometryType.GEO_GEOMETRY_POINT:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_LINESTRING:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_POLYGON:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTIPOINT:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTILINESTRING:
                    {                      
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTIPOLYGON:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_PATH:
                    {
                        DirectlyDrawPath(g, symbol as ISolidFillSymbol, geometry as IPath);
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_RECT:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_DIRECTIONPOINT:
                    {
                        break;
                    }
            }
        }

        private void draw(IDisplay display, ISolidFillSymbol symbol, IPolygon polygon)
        {
            if (polygon.ExteriorRing == null) return;
            SolidBrush solidBrush = new SolidBrush(Color.FromArgb(symbol.Color.A,symbol.Color.R,symbol.Color.G,symbol.Color.B));
            System.Drawing.Drawing2D.GraphicsPath path = new System.Drawing.Drawing2D.GraphicsPath();
            #region 填充
            
            PointF[] plist=new PointF[polygon.ExteriorRing.Count];
            int i = 0;
            foreach (IPoint pt in polygon.ExteriorRing)
            {
                double dx = 0;
                double dy = 0;
                display.DisplayTransformation.FromMapPoint(pt.X,pt.Y,ref dx,ref dy);
                plist[i++] = new PointF((float)dx,(float)dy);
            }
            if (plist.Count() > 0)
                path.AddPolygon(plist);
            for (int m = 0; m < polygon.InteriorRings.Count; m++)
            {
                List<PointF> pp = new List<PointF>();
                foreach (IPoint ipoint in polygon.InteriorRings[m])
                {
                    double dx = 0;
                    double dy = 0;
                    display.DisplayTransformation.FromMapPoint(ipoint.X,ipoint.Y,ref dx,ref dy);
                    pp.Add(new PointF((float)dx,(float)dy));
                }
                path.AddPolygon(pp.ToArray());
            }
            display.G.FillPath(solidBrush, path);

            #endregion

            #region 绘制边框

            if (symbol.OutLine != null)
            {
                ISymbolDrawer draw = SymbolDrawerFactory.Instance.GetDrawer(symbol.OutLine);

                draw.Draw(display, symbol.OutLine, polygon.ExteriorRing, null);
                for (int k = 0; k < polygon.InteriorRings.Count; k++)
                {
                    draw.Draw(display,symbol.OutLine,polygon.InteriorRings[k],null);
                }
            }

            #endregion
        }
        private void draw(IDisplay display, ISolidFillSymbol symbol, IMultiPolygon polygon)
        {
            foreach (IPolygon p in polygon)
            {
                draw(display,symbol,p);
            }
        }

        //画路径
        private void DrawPath(IDisplay display, ISolidFillSymbol symbol, IPath path)
        {
            IPath transPath = new Path();
            IDisplayTransformation displayTransformation = display.DisplayTransformation;
            int count = path.Count;
            double x = 0, y = 0;
            IPathPoint p = null;
            for (int i = 0; i < count; i++)
            {
                p = path[i];
                IPathPoint point = new PathPoint();
                displayTransformation.FromMapPoint(p.InPoint.X, p.InPoint.Y, ref x, ref y);
                point.InPoint.X = x;
                point.InPoint.Y = y;
                displayTransformation.FromMapPoint(p.X, p.Y, ref x, ref y);
                point.X = x;
                point.Y = y;
                displayTransformation.FromMapPoint(p.OutPoint.X, p.OutPoint.Y, ref x, ref y);
                point.OutPoint.X = x;
                point.OutPoint.Y = y;
                transPath.Add(point);
            }
            DirectlyDrawPath(display.G, symbol, transPath);
        }
        //画多边形
        private void DrawPolygon(IDisplay display, ISolidFillSymbol symbol, IPolygon polygon)
        {
            List<ILineString> rings = new List<ILineString>();
            IDisplayTransformation displayTransformation = display.DisplayTransformation;
            IList<IPoint> pts = displayTransformation.FromMapPoint(polygon.ExteriorRing);
            ILineString ring = new LineString(pts);
            rings.Add(ring);
            foreach (ILineString item in polygon.InteriorRings)
            {
                pts = displayTransformation.FromMapPoint(item);
                ring = new LineString(pts);
                rings.Add(ring);
            }
            IPolygon transPolygon = new Polygon(rings, null);
            DirectlyDrawPolygon(display.G, symbol, transPolygon);
        }

        //直接画路径
        private void DirectlyDrawPath(Graphics g, ISolidFillSymbol symbol, IPath path)
        {
            GraphicsPath gpath = new GraphicsPath();
            IPathPoint p1 = null, p2 = null;
            int count = path.Count;
            for (int i = 0; i < count - 1; i++)
            {
                p1 = path[i];
                p2 = path[i + 1];
                gpath.AddBezier(new PointF((float)p1.X, (float)p1.Y), new PointF((float)p1.OutPoint.X, (float)p1.OutPoint.Y),
                    new PointF((float)p2.InPoint.X, (float)p2.InPoint.Y), new PointF((float)p2.X, (float)p2.Y));
            }
            using (SolidBrush brush = new SolidBrush(symbol.Color))
            {
                g.FillPath(brush, gpath);
            }
            if (symbol.OutLine != null)
            {
                ISymbolDrawer drawer = SymbolDrawerFactory.Instance.GetDrawer(symbol.OutLine);
                drawer.Draw(g, symbol.OutLine, path, "");
            }
        }
        //直接画多边形
        private void DirectlyDrawPolygon(Graphics g, ISolidFillSymbol symbol, IPolygon polygon)
        {
            using (GraphicsPath gpath = new GraphicsPath())
            {
                //外圈
                List<PointF> points = new List<PointF>();
                foreach (IPoint point in polygon.ExteriorRing)
                {
                    points.Add(new PointF((float)point.X, (float)point.Y));
                }
                if (points.Count > 0)
                {
                    gpath.AddPolygon(points.ToArray());
                }
                //内圈
                foreach (ILineString ring in polygon.InteriorRings)
                {
                    points.Clear();
                    foreach (IPoint point in ring)
                    {
                        points.Add(new PointF((float)point.X, (float)point.Y));
                    }
                    if (points.Count > 0)
                    {
                        gpath.AddPolygon(points.ToArray());
                    }
                }
                //绘制
                using (SolidBrush brush = new SolidBrush(symbol.Color))
                {
                    g.FillPath(brush, gpath);
                }
            }
            if (symbol.OutLine != null)
            {
                ISymbolDrawer drawer = SymbolDrawerFactory.Instance.GetDrawer(symbol.OutLine);
                drawer.Draw(g, symbol.OutLine, polygon, "");
            }
        }
       
    }
}
