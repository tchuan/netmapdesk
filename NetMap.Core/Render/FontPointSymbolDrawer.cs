﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Symbol;

namespace NetMap.Render
{
    /// <summary>
    /// 字体点符号绘制
    /// </summary>
    internal class FontPointSymbolDrawer : ISymbolDrawer
    {
        public void Draw(IDisplay display,  
            ISymbol symbol, IGeometry geometry, string text)
        {
            if (symbol == null || geometry == null||display==null) return;
            IFontPointSymbol fontPointSymbol = symbol as IFontPointSymbol;
            if (geometry is IDirectionPoint)
            {
                draw(display,fontPointSymbol,geometry as IDirectionPoint);
            }
            else if (geometry is IPoint)
            {
                draw(display,fontPointSymbol,geometry as IPoint);
            }
            else if (geometry is IMultiPoint)
            {
                draw(display,fontPointSymbol,geometry as IMultiPoint);
            }
            else
            { 
            }
        }

        public void Draw(Graphics g, ISymbol symbol, IGeometry geometry, string text)
        {
            //TODO
        }

        private void draw(IDisplay display, IFontPointSymbol symbol, IMultiPoint pts)
        {
            ISymbolDrawer drawer = SymbolDrawerFactory.Instance.GetDrawer(symbol);
            foreach (IPoint pt in pts)
            {
                drawer.Draw(display,symbol,pt,null);
            }
        }
        private void draw(IDisplay display,IFontPointSymbol symbol,IDirectionPoint pt)
        {
            //
            // 实现
            //
        }
        private void draw(IDisplay display, IFontPointSymbol symbol, IPoint pt)
        {
            double dx = 0;
            double dy = 0;
            display.DisplayTransformation.FromMapPoint(pt.X,pt.Y,ref dx,ref dy);
            System.Drawing.SolidBrush solidBrush = new System.Drawing.SolidBrush(Color.FromArgb(symbol.Font.Color.A, symbol.Font.Color.R, symbol.Font.Color.G, symbol.Font.Color.B));
            Font useFont = new Font(symbol.Font.Font,(float)symbol.Font.Width);
            SizeF usedzie = display.G.MeasureString(symbol.Text, useFont);
            int leftx = Convert.ToInt32(dx-usedzie.Width/2-symbol.XOffset);
            int lefty = Convert.ToInt32(dy-usedzie.Height/2-symbol.YOffset);
            Matrix myMatrix = new Matrix();
            myMatrix.RotateAt((float)symbol.Angle,new PointF(leftx,lefty), MatrixOrder.Append);
            display.G.Transform = myMatrix;
            display.G.DrawString(symbol.Text, useFont, solidBrush, new RectangleF(leftx, lefty, leftx + (int)symbol.Font.Width, leftx + (int)symbol.Font.Height));
            myMatrix.RotateAt(-(float)symbol.Angle, new PointF(leftx, lefty), MatrixOrder.Append);
            display.G.Transform = myMatrix;
        }
    }
}
