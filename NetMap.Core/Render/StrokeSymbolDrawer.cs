﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Symbol;
using NetMap.Interfaces.Render;
using NetMap.Geometry.Algorithms;
using NetMap.Geometry;

namespace NetMap.Render
{
    /// <summary>
    /// 线符号的绘制
    /// </summary>
    internal class StrokeSymbolDrawer : ISymbolDrawer
    {
        public void Draw(IDisplay display, ISymbol symbol, IGeometry geometry, string text)
        {
            switch (geometry.GeometryType)
            {
                case geoGeometryType.GEO_GEOMETRY_POINT:
                case geoGeometryType.GEO_GEOMETRY_MULTIPOINT:
                case geoGeometryType.GEO_GEOMETRY_DIRECTIONPOINT:
                case geoGeometryType.GEO_GEOMETRY_PATHPOINT:
                    {
                        //无法绘制
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_LINESTRING:
                    {
                        InnerDrawLineString(display, symbol as IStrokeSymbol, geometry as ILineString);
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_POLYGON:
                    {
                        InnerDrawPolygon(display, symbol as IStrokeSymbol, geometry as IPolygon);
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTILINESTRING:
                    {
                        IMultiLineString lines = geometry as IMultiLineString;
                        foreach (ILineString line in lines)
                        {
                            InnerDrawLineString(display, symbol as IStrokeSymbol, line);
                        }
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTIPOLYGON:
                    {
                        IMultiPolygon polygons = geometry as IMultiPolygon;
                        foreach (IPolygon polygon in polygons)
                        {
                            InnerDrawPolygon(display, symbol as IStrokeSymbol, polygon);
                        }
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_PATH:
                    {
                        InnerDrawPath(display, symbol as IStrokeSymbol, geometry as IPath);
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_RECT:
                    {
                        IEnvelope rect = geometry as IEnvelope;
                        InnerDrawRect(display, symbol as IStrokeSymbol, rect);
                        break;
                    }
            }
        }

        public void Draw(Graphics g, ISymbol symbol, IGeometry geometry, string text)
        {
            switch(geometry.GeometryType)
            {
                case geoGeometryType.GEO_GEOMETRY_POINT:
                case geoGeometryType.GEO_GEOMETRY_MULTIPOINT:
                case geoGeometryType.GEO_GEOMETRY_DIRECTIONPOINT:
                case geoGeometryType.GEO_GEOMETRY_PATHPOINT:
                    {
                        //无法绘制
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_LINESTRING:
                    {
                        InnerDirectlyDrawLineString(g, symbol as IStrokeSymbol, geometry as ILineString);
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_POLYGON:
                    {
                        InnerDirectlyDrawPolygon(g, symbol as IStrokeSymbol, geometry as IPolygon);
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTILINESTRING:
                    {
                        IMultiLineString lines = geometry as IMultiLineString;
                        foreach (ILineString line in lines)
                        {
                            InnerDirectlyDrawLineString(g, symbol as IStrokeSymbol, line);
                        }
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTIPOLYGON:
                    {
                        IMultiPolygon polygons = geometry as IMultiPolygon;
                        foreach (IPolygon polygon in polygons)
                        {
                            InnerDirectlyDrawPolygon(g, symbol as IStrokeSymbol, polygon);
                        }
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_PATH:
                    {
                        InnerDirectlyDrawPath(g, symbol as IStrokeSymbol, geometry as IPath);
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_RECT:
                    {
                        IEnvelope rect = geometry as IEnvelope;
                        InnerDirectlyDrawRect(g, symbol as IStrokeSymbol, rect);
                        break;
                    }
            }
        }

        //绘制多段线
        private void InnerDrawLineString(IDisplay display, IStrokeSymbol symbol, ILineString line)
        {
            IList<IPoint> pts = display.DisplayTransformation.FromMapPoint(line);
            ILineString newLine = new LineString(pts);
            InnerDirectlyDrawLineString(display.G, symbol, newLine);
        }
        //绘制多段线
        private void InnerDirectlyDrawLineString(Graphics g, IStrokeSymbol symbol, ILineString line)
        {
            //画线
            List<PointF> pts = new List<PointF>();
            foreach (IPoint point in line)
            {
                pts.Add(new PointF((float)point.X, (float)point.Y));
            }
            g.DrawLines(symbol.Pen, pts.ToArray());

            IPoint startPoint = line.StartPoint;
            IPoint endPoint = line.EndPoint;
            //绘制起点
            if (symbol.StartNodeSymbol != null)
            {
                ISymbolDrawer drawer = SymbolDrawerFactory.Instance.GetDrawer(symbol.StartNodeSymbol);
                drawer.Draw(g, symbol.StartNodeSymbol, startPoint, null);
            }
            //绘制节点
            if (symbol.NodeAvailable)
            {
                IPointSymbol nodeSymbol = symbol.NodeSymbol;
                ISymbolDrawer drawer = SymbolDrawerFactory.Instance.GetDrawer(nodeSymbol);
                int count = line.Count;
                IPoint lastPoint = null;
                IPoint currentPoint = null;
                IPoint nextPoint = null;
                for (int i = 1; i < count - 1; i++)
                {
                    lastPoint = line[i - 1];
                    currentPoint = line[i];
                    nextPoint = line[i + 1];
                    switch (symbol.NodeSymbolRotateType)
                    {
                        case LineNodeSymbolRotateType.In:
                            {
                                nodeSymbol.Rotate = Algorithms.RelativeAngle(lastPoint, currentPoint);
                                break;
                            }
                        case LineNodeSymbolRotateType.Out:
                            {
                                nodeSymbol.Rotate = Algorithms.RelativeAngle(nextPoint, lastPoint);
                                break;
                            }
                        default:
                            {
                                //默认是不旋转
                                nodeSymbol.Rotate = 0;
                                break;
                            }
                    }
                    drawer.Draw(g, symbol, currentPoint, null);
                }
            }
            //绘制终点
            if (symbol.EndNodeSymbol != null)
            {
                ISymbolDrawer drawer = SymbolDrawerFactory.Instance.GetDrawer(symbol.EndNodeSymbol);
                drawer.Draw(g, symbol.EndNodeSymbol, endPoint, null);
            }
        }

        //绘制路径
        private void InnerDrawPath(IDisplay display, IStrokeSymbol symbol, IPath path)
        {
            IPath transPath = new Path();            
            IDisplayTransformation displayTransformation = display.DisplayTransformation;
            //int count = path.Count;
            //double x = 0, y = 0;
            //IPathPoint p = null;
            //for (int i = 0; i < count; i++)
            //{
            //    p = path[i];
            //    IPathPoint point = new PathPoint();
            //    displayTransformation.FromMapPoint(p.InPoint.X, p.InPoint.Y, ref x, ref y);
            //    point.InPoint.X = x;
            //    point.InPoint.Y = y;
            //    displayTransformation.FromMapPoint(p.X, p.Y, ref x, ref y);
            //    point.X = x;
            //    point.Y = y;
            //    displayTransformation.FromMapPoint(p.OutPoint.X, p.OutPoint.Y, ref x, ref y);
            //    point.OutPoint.X = x;
            //    point.OutPoint.Y = y;
            //    transPath.Add(point);
            //}
            foreach (IPathPoint point in path)
            {
                IPathPoint transPoint = displayTransformation.FromMapPoint(point) as IPathPoint;
                transPath.Add(transPoint);
            }
            InnerDirectlyDrawPath(display.G, symbol, transPath);
        }
        //绘制路径
        private void InnerDirectlyDrawPath(Graphics g, IStrokeSymbol symbol, IPath path)
        {
            IPathPoint p1 = null, p2 = null;
            int count = path.Count;
            for (int i = 0; i < count - 1; i++)
            {
                p1 = path[i];
                p2 = path[i + 1];
                g.DrawBezier(symbol.Pen, new PointF((float)p1.X, (float)p1.Y), new PointF((float)p1.OutPoint.X, (float)p1.OutPoint.Y),
                    new PointF((float)p2.InPoint.X, (float)p2.InPoint.Y), new PointF((float)p2.X, (float)p2.Y));
                //modify 2014-05-03 辅助线只绘制最末尾的部分
                //if (symbol.IsAssistLine)
                //{
                //    g.DrawLine(symbol.Pen, new PointF((float)p1.InPoint.X, (float)p1.InPoint.Y),
                //        new PointF((float)p1.OutPoint.X, (float)p1.OutPoint.Y));
                //}
                //end
            }
            if (p2 != null && path.IsClose)
            {
                IPathPoint startPoint = path.StartPoint;
                g.DrawLine(symbol.Pen, (float)p2.X, (float)p2.Y, (float)startPoint.X, (float)startPoint.Y);
            }
            //绘制末尾节点辅助线
            if (p2 != null && symbol.IsAssistLine)
            {                
                g.DrawLine(symbol.Pen, new PointF((float)p2.InPoint.X, (float)p2.InPoint.Y),
                        new PointF((float)p2.OutPoint.X, (float)p2.OutPoint.Y));
            }
            //绘制起点
            if (symbol.StartNodeSymbol != null)
            {
                IPathPoint startPoint = path.StartPoint;
                IPointSymbol startNodeSymbol = symbol.StartNodeSymbol;
                if (symbol.IsStartNodeHasDirection)
                {
                    if (startPoint.X == startPoint.InPoint.X
                        && startPoint.Y == startPoint.InPoint.Y)
                    {
                        startNodeSymbol.Rotate = 0;
                    }
                    else
                    {
                        startNodeSymbol.Rotate = Algorithms.RelativeAngle(startPoint, startPoint.InPoint);
                    }
                }
                ISymbolDrawer drawer = SymbolDrawerFactory.Instance.GetDrawer(symbol.StartNodeSymbol);
                drawer.Draw(g, symbol.StartNodeSymbol, startPoint, null);
            }
            //绘制节点
            if (symbol.NodeAvailable)
            {
                ISymbolDrawer drawer = SymbolDrawerFactory.Instance.GetDrawer(symbol.NodeSymbol);
                foreach (IPoint point in path)
                {
                    if (point != path.StartPoint && point != path.EndPoint)
                    {
                        drawer.Draw(g, symbol.NodeSymbol, point, null);
                    }
                }
            }
            //绘制终点
            if (symbol.EndNodeSymbol != null)
            {
                IPathPoint endPoint = path.EndPoint;
                IPointSymbol endNodeSymbol = symbol.EndNodeSymbol;
                if(symbol.IsEndNodeHasDirection)
                {
                    if (endPoint.X == endPoint.OutPoint.X &&
                        endPoint.Y == endPoint.OutPoint.Y)
                    {
                        endNodeSymbol.Rotate = 0;
                    }
                    else
                    {
                        endNodeSymbol.Rotate = Algorithms.RelativeAngle(endPoint, endPoint.OutPoint);
                    }
                }
                ISymbolDrawer drawer = SymbolDrawerFactory.Instance.GetDrawer(symbol.EndNodeSymbol);
                drawer.Draw(g, symbol.EndNodeSymbol, endPoint, null);
//#if DEBUG
//                using (Font font = new Font("黑体", 10))
//                {
//                    g.DrawString(endNodeSymbol.Rotate.ToString(), font, new SolidBrush(Color.Black), (float)endPoint.X, (float)endPoint.Y);
//                }
//#endif
            }
        }

        //绘制框
        private void InnerDrawRect(IDisplay display, IStrokeSymbol symbol, IEnvelope rect)
        {
            double x1 = 0, y1 = 0, x2 = 0, y2 = 0;
            display.DisplayTransformation.FromMapPoint(rect.XMin, rect.YMin, ref x1, ref y1);
            display.DisplayTransformation.FromMapPoint(rect.XMax, rect.YMax, ref x2, ref y2);
            IEnvelope box = new Envelope(x1, y1, x2, y2);
            InnerDirectlyDrawRect(display.G, symbol, box);
        }
        //绘制多边形
        private void InnerDrawPolygon(IDisplay display, IStrokeSymbol symbol, IPolygon polygon)
        {
            List<ILineString> rings = new List<ILineString>();
            IDisplayTransformation displayTransformation = display.DisplayTransformation;
            IList<IPoint> pts = displayTransformation.FromMapPoint(polygon.ExteriorRing);
            ILineString ring = new LineString(pts);
            rings.Add(ring);
            foreach (ILineString item in polygon.InteriorRings)
            {
                pts = displayTransformation.FromMapPoint(item);
                ring = new LineString(pts);
                rings.Add(ring);
            }
            IPolygon transPolygon = new Polygon(rings, null);
            InnerDirectlyDrawPolygon(display.G, symbol, transPolygon);
        }

        //绘制框
        private void InnerDirectlyDrawRect(Graphics g, IStrokeSymbol symbol, IEnvelope rect)
        {   
            //画线
            g.DrawRectangle(symbol.Pen, (float)rect.XMin, (float)rect.YMin, (float)rect.Width, (float)rect.Height);
            //画节点
            if (symbol.NodeAvailable)
            {
                if (symbol.NodeAvailable)
                {
                    IPointSymbol pointSymbol = symbol.NodeSymbol;
                    ISymbolDrawer drawer = SymbolDrawerFactory.Instance.GetDrawer(pointSymbol);
                    drawer.Draw(g, pointSymbol, rect.MaxxMiny , null);
                    drawer.Draw(g, pointSymbol, rect.MinxMaxy, null);
                    drawer.Draw(g, pointSymbol, rect.MaxxMiny, null);
                    drawer.Draw(g, pointSymbol, rect.MaxxMiny, null);
                }
            }
        }
        //绘制多边形
        private void InnerDirectlyDrawPolygon(Graphics g, IStrokeSymbol symbol, IPolygon polygon)
        {
            using (GraphicsPath gpath = new GraphicsPath())
            {
                //外圈
                List<PointF> points = new List<PointF>();
                foreach (IPoint point in polygon.ExteriorRing)
                {
                    points.Add(new PointF((float)point.X, (float)point.Y));
                }
                if (points.Count > 0)
                {
                    gpath.AddPolygon(points.ToArray());
                }
                //内圈
                foreach (ILineString ring in polygon.InteriorRings)
                {
                    points.Clear();
                    foreach (IPoint point in ring)
                    {
                        points.Add(new PointF((float)point.X, (float)point.Y));
                    }
                    if (points.Count > 0)
                    {
                        gpath.AddPolygon(points.ToArray());
                    }
                }
                //绘制
                g.DrawPath(symbol.Pen, gpath);
            }
        }
    }
}

