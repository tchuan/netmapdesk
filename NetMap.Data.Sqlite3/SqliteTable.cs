﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Data;
using Community.CsharpSqlite;
using NetMap.Data;

namespace NetMap.Sqlite
{
    /// <summary>
    /// Sqlite数据库表
    /// </summary>
    internal class SqliteTable : ITable
    {
        protected IFields _fields = null; //字段信息
        internal string _name = string.Empty;
        internal Sqlite3.sqlite3 _db = null;

        internal SqliteTable()
        {            
        }

        #region IData 成员

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public string Alias
        {
            get;
            set;
        }

        public IFields Fields
        {
            get { return _fields; }
        }

        public IDatabase Database
        {
            get;
            internal set;
        }

        #endregion

        #region ITable 成员

        public IRow CreateRow()
        {
            SqliteRow row = new SqliteRow();
            row.InitFields(this, _fields);
            return row;
        }

        public int GetRowCount()
        {
            string sql = "select count(0) from " + _name;
            Sqlite3.Vdbe stmt = null;
            string tail = "";
            int err = Sqlite3.sqlite3_prepare_v2(_db, sql, sql.Length, ref stmt, ref tail);
            if (err != Sqlite3.SQLITE_OK)
            {
                throw new Exception("获取记录条数失败");
            }
            err = Sqlite3.sqlite3_step(stmt);
            if (err != Sqlite3.SQLITE_ROW)
            {
                tail = Sqlite3.sqlite3_errmsg(_db);
                Sqlite3.sqlite3_finalize(stmt);
                throw new Exception(tail);
            }
            int count = Sqlite3.sqlite3_column_int(stmt, 0);
            Sqlite3.sqlite3_finalize(stmt);
            return count;
        }

        public IRow GetRow(long oid)
        {
            SqliteRow row = new SqliteRow();
            row.InitFields(this, _fields);
            row.OID = oid;
            //查询
            int count = _fields.Count;
            StringBuilder builder = new StringBuilder();
            builder.Append("select ");
            bool first = true;
            for (int i = 0; i < count; i++)
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    builder.Append(",");
                }
                builder.Append(_fields.GetField(i).Name);
            }
            builder.Append(" from ");
            builder.Append(_name);
            builder.Append(" where ");
            builder.Append(SpatialDbDefine.FIELD_OID);
            builder.Append("=");
            builder.Append(oid);
            string sql = builder.ToString();
            Sqlite3.Vdbe stmt = null;
            string tail = "";
            int err = Sqlite3.sqlite3_prepare_v2(_db, sql, sql.Length, ref stmt, ref tail);
            if (err != Sqlite3.SQLITE_OK)
            {
                throw new Exception(tail);
            }
            err = Sqlite3.sqlite3_step(stmt);
            if (err == Sqlite3.SQLITE_ROW)
            {
                row.InitValue(stmt);
                Sqlite3.sqlite3_finalize(stmt);
                return row;
            }
            else
            {
                Sqlite3.sqlite3_finalize(stmt);
                return null;
            }
        }

        public ICursor Search(IQueryFilter filter)
        {
            IFields fields = new Fields();
            int err = check_filter(filter, fields);
            if (err != Sqlite3.SQLITE_OK)
            {
                throw new ApplicationException("查询失败");
            }
            //构造sql语句
            string sql = get_search_sql(filter);
            //执行查询
            Sqlite3.Vdbe stmt = null;
            string tail = "";
            err = Sqlite3.sqlite3_prepare_v2(_db, sql, sql.Length, ref stmt, ref tail);
            if (err != Sqlite3.SQLITE_OK)
            {
                throw new ApplicationException("sql错误：" + sql + " 具体错误信息：" + tail);
            }
            SqliteCursor cursor = new SqliteCursor();
            cursor.open(this, fields, stmt);
            return cursor;
        }        

        #endregion

        //创建表
        internal int create(Sqlite3.sqlite3 db, string name, IFields fields)
        {
            _db = db;
            _name = name;
            _fields = fields;
            string sql = build_create_sql(name, fields);
            Sqlite3.Vdbe stmt = null;
            string tail = "";
            int err = Sqlite3.sqlite3_prepare_v2(db, sql, sql.Length, ref stmt, ref tail);
            if (err != Sqlite3.SQLITE_OK)
            {
                return err;
            }
            err = Sqlite3.sqlite3_step(stmt);
            if (err != Sqlite3.SQLITE_DONE)
            {
                Sqlite3.sqlite3_finalize(stmt);
                return err;
            }
            Sqlite3.sqlite3_finalize(stmt);
            return Sqlite3.SQLITE_OK;
        }

        //打开表
        internal virtual int open(Sqlite3.sqlite3 db, string name)
        {
            _db = db;
            _name = name;            

            get_table_field();

            return Sqlite3.SQLITE_OK;
        }

        //获取表的字段信息
        private void get_table_field()
        {
            string sql = "PRAGMA table_info(" + _name + ")";
            Sqlite3.Vdbe stmt = null;
            string tail = "";
            int err = Sqlite3.sqlite3_prepare_v2(_db, sql, sql.Length, ref stmt, ref tail);
            if (err != Sqlite3.SQLITE_OK)
            {
                throw new ApplicationException("获取表的字段信息失败:" + tail);
            }
            int i = 0;
            err = Sqlite3.sqlite3_step(stmt);
            _fields = new Fields();
            while (err == Sqlite3.SQLITE_ROW)
            {
                string name = Sqlite3.sqlite3_column_text(stmt, 1);
                if (name == SpatialDbDefine.FIELD_OID)
                {
                    err = Sqlite3.sqlite3_step(stmt);
                    continue;
                }
                IField field = new Field();
                field.Name = name;
                string fieldString = Sqlite3.sqlite3_column_text(stmt, 2);
                switch (fieldString)
                {
                    case "short":
                        {
                            field.Type = geoFieldType.GEO_SMALLINT;
                            break;
                        }
                    case "int":
                        {
                            field.Type = geoFieldType.GEO_INT;
                            break;
                        }
                    case "long":
                        {
                            field.Type = geoFieldType.GEO_BIGINT;
                            break;
                        }
                    case "float":
                        {
                            field.Type = geoFieldType.GEO_SINGLE;
                            break;
                        }
                    case "double":
                        {
                            field.Type = geoFieldType.GEO_DOUBLE;
                            break;
                        }
                    case "text":
                        {
                            field.Type = geoFieldType.GEO_STRING;
                            break;
                        }
                    case "datetime":
                        {
                            field.Type = geoFieldType.GEO_DATE;
                            break;
                        }
                    case "blob":
                        {
                            field.Type = geoFieldType.GEO_BLOB;
                            break;
                        }
                }
                _fields.SetField(i++, field);
                err = Sqlite3.sqlite3_step(stmt);
            }
            Sqlite3.sqlite3_finalize(stmt);
        }

        #region Protected

        //构建表的创建sql
        protected virtual string build_create_sql(string name, IFields fields)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("create table ");
            builder.Append(name);
            builder.Append("(");
            builder.Append(SpatialDbDefine.FIELD_OID);
            builder.Append(" integer primary key not null");
            int count = fields.Count;
            IField field = null;
            for (int i = 0; i < count; i++)
            {
                field = fields.GetField(i);
                builder.Append(",[").Append(field.Name).Append("]");
                add_one_field(field, builder);
            }
            builder.Append(")");
            return builder.ToString();
        }

        protected void add_one_field(IField field, StringBuilder builder)
        {
            switch (field.Type)
            {
                case geoFieldType.GEO_SMALLINT:
                    {
                        builder.Append(" short");
                        break;
                    }
                case geoFieldType.GEO_INT:
                    {
                        builder.Append(" int");
                        break;
                    }
                case geoFieldType.GEO_BIGINT:
                    {
                        builder.Append(" long");
                        break;
                    }
                case geoFieldType.GEO_SINGLE:
                    {
                        builder.Append(" float");
                        break;
                    }
                case geoFieldType.GEO_DOUBLE:
                    {
                        builder.Append(" double");
                        break;
                    }
                case geoFieldType.GEO_STRING:
                    {
                        builder.Append(" text");
                        break;
                    }
                case geoFieldType.GEO_BLOB:
                    {
                        builder.Append(" blob");
                        break;
                    }
                case geoFieldType.GEO_DATE:
                    {
                        builder.Append(" datetime");
                        break;
                    }
                case geoFieldType.GEO_GEOMETRY:
                    {
                        builder.Append(" blob");
                        break;
                    }
            }
        }        

        //根据所有字段的查询格式
        protected virtual string get_sub_fields()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(SpatialDbDefine.FIELD_OID);
            int count = _fields.Count;
            IField field = null;
            for (int i = 0; i < count; i++)
            {
                field = _fields.GetField(i);
                builder.Append(",").Append(field.Name);
            }
            return builder.ToString();
        }

        protected int getRows(string subFields, ref Sqlite3.Vdbe stmt, string where)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("select ").Append(subFields);
            builder.Append(" from ").Append(_name);
            if (!string.IsNullOrEmpty(where))
            {
                builder.Append(" where ").Append(where);
            }
            string sql = builder.ToString();
            return Sqlite3.sqlite3_prepare_v2(_db, sql, sql.Length, ref stmt, 0);
        }

        #endregion

        //检查filter
        protected int check_filter(IQueryFilter filter, IFields fields)
        {
            int index = -1;
            int i = 0;
            foreach (string field in filter.Fields)
            {
                index = _fields.FindField(field);
                if (index == -1)
                {
                    return SpatialDbDefine.GIS_DBERR_NOTFIND; //未找到目标字段
                }
                else
                {
                    fields.SetField(i++, new Field(_fields.GetField(index)));
                }
            }
            return Sqlite3.SQLITE_OK;
        }

        //构建查询sql
        protected virtual string get_search_sql(IQueryFilter filter)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("select ");
            bool first = true;
            #region 属性字段

            int count = filter.Fields.Count;
            for (int i = 0; i < count; i++)
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    builder.Append(",");
                }
                builder.Append(filter.Fields[i]);
            }
            #endregion
            #region OID
            if (first)
            {
                first = false;
            }
            else
            {
                builder.Append(",");
            }
            builder.Append(SpatialDbDefine.FIELD_OID);
            #endregion
            builder.Append(" from ").Append(_name);
            if (!string.IsNullOrEmpty(filter.WhereClause))
            {
                builder.Append(" where ").Append(filter.WhereClause);
            }
            return builder.ToString();
        }
    }
}
