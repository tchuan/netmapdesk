﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjNet.CoordinateSystems;
using ProjNet.SRID;
using ProjNet.CoordinateSystems.Transformations;
using NetMap.Interfaces.Geometry;
using NetMap.Geometry;
using NetMap.Geometry.Algorithms;

namespace NetMap.Application.Util
{
    /// <summary>
    /// Web墨卡托投影
    /// </summary>
    public class WebMercatorUtil
    {
        private IProjectedCoordinateSystem _cs;
        private ICoordinateTransformation _csTrans;
        private ICoordinateTransformation _csReTrans;

        private static WebMercatorUtil _instance;
        /// <summary>
        /// 获取单例
        /// </summary>
        public static WebMercatorUtil Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new WebMercatorUtil();
                }
                return _instance;
            }
        }

        protected WebMercatorUtil()
        {
            _cs = SRIDReader.GetCSbyID(3785) as IProjectedCoordinateSystem;
            CoordinateTransformationFactory ctFactory = new CoordinateTransformationFactory();
            _csTrans = ctFactory.CreateFromCoordinateSystems(_cs.GeographicCoordinateSystem, _cs);
            _csReTrans = ctFactory.CreateFromCoordinateSystems(_cs, _cs.GeographicCoordinateSystem);
        }

        /// <summary>
        /// 经纬度到投影坐标
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public IPoint LonLatToXY(IPoint point)
        {
            double x = 0, y = 0;
            _csTrans.MathTransform.Transform(point.X, point.Y, ref x, ref y);
            return new NetMap.Geometry.Point(x, y);
        }

        /// <summary>
        /// 获取点到多段线的最短距离
        /// </summary>
        /// <param name="point"></param>
        /// <param name="line"></param>
        /// <returns></returns>
        public double GetPointToLineStringMinDis(IPoint point, ILineString line)
        {
            //转换
            IPoint pointT = LonLatToXY(point);
            ILineString lineT = new LineString();
            foreach (IPoint item in line)
            {
                lineT.Add(LonLatToXY(item));
            }

            //计算距离
            IPoint p1, p2;
            double dis2Min = double.MaxValue; //注意线段点数为0时会有bug
            double dis2Temp = 0;
            int count = lineT.Count;
            for (int i = 0; i < count - 2; i++)
            {
                p1 = lineT[i];
                p2 = lineT[i + 1];
                dis2Temp = Algorithms.PointToLinesegmentDistance(pointT, p1, p2);
                if (dis2Temp < dis2Min)
                {
                    dis2Min = dis2Temp;
                }
            }
            return dis2Min;
        }

        /// <summary>
        /// 获取点到多多段线的最短距离
        /// </summary>
        /// <param name="point"></param>
        /// <param name="mline"></param>
        /// <returns></returns>
        public double GetPointToMultiLineStringMinDis(IPoint point, IMultiLineString mline)
        {
            double dis = double.MaxValue;
            foreach (ILineString line in mline)
            {
                double temp = GetPointToLineStringMinDis(point, line);
                if (temp < dis)
                {
                    dis = temp;
                }
            }
            return dis;
        }

        #region 经纬度距离

        /// <summary>
        /// 计算经纬度点的距离
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        public double GetDistance(IPoint p1, IPoint p2)
        {
            IPoint p1T = LonLatToXY(p1);
            IPoint p2T = LonLatToXY(p2);
            return Algorithms.PointToPointDistance(p1T, p2T);
        }

        /// <summary>
        /// 计算经纬度多段线的长度
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public double GetTotalDistance(ILineString line)
        {
            double sum = 0.0;
            IPoint p1, p2;
            int count = line.Count;
            for (int i = 0; i < count - 1; i++)
            {
                p1 = line[i];
                p2 = line[i + 1];
                sum += GetDistance(p1, p2);
            }
            return sum;
        }

        #endregion

        #region 经纬度比例尺和Web墨卡托比例尺的转换

        /// <summary>
        /// 获取墨卡托比例尺分母
        /// </summary>
        /// <param name="scale"></param>
        /// <returns></returns>
        public double GetWebMercatorScaleDenominator(double scale)
        {
            double wmScaleDenominator = 1;
            double dpi = 96;
            using (System.Drawing.Graphics graphics = System.Drawing.Graphics.FromHwnd(IntPtr.Zero))
            {
                dpi = graphics.DpiX;
            }
            double pixel = 1 / 25.4 * dpi;
            double unit = pixel / scale;
            double x1 = 0, y1 = 0, x2 = 0, y2 = 0;
            _csTrans.MathTransform.Transform(0, 0, ref x1, ref y1);
            _csTrans.MathTransform.Transform(unit, 0, ref x2, ref y2);
            wmScaleDenominator = NetMap.Geometry.Algorithms.Algorithms.PointToPointDistance(x1, y1, x2, y2) * 1000;
            return wmScaleDenominator;
        }

        /// <summary>
        /// 获取经纬度比例尺分母
        /// </summary>
        /// <param name="wmScale"></param>
        /// <returns></returns>
        public double GetScale(double wmScale)
        {
            double scale = 1;
            double dpi = 96;
            using (System.Drawing.Graphics graphics = System.Drawing.Graphics.FromHwnd(IntPtr.Zero))
            {
                dpi = graphics.DpiX;
            }
            double pixel = 1 / 25.4 * dpi;

            double x = 0, y = 0;
            _csTrans.MathTransform.Transform(0, 0, ref x, ref y);
            double dis = 1 * 0.001 / wmScale;
            double x1 = 0, y1 = 0;
            _csReTrans.MathTransform.Transform(x + dis, y, ref x1, ref y1);
            scale = pixel / NetMap.Geometry.Algorithms.Algorithms.PointToPointDistance(0, 0, x1, y1);

            return scale;
        }

        #endregion
    }
}
