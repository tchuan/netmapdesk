﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Element;
using NetMap.Interfaces.Symbol;

namespace NetMap.Interfaces
{
    /// <summary>
    /// 元素图层
    /// </summary>
    public interface IElementLayer
    {
        /// <summary>
        /// 获取和设置元素集合
        /// </summary>
        List<IElement> Elements { get; set; }
        /// <summary>
        /// 获取和设置符号
        /// </summary>
        ISymbol Symbol { get; set; }
    }
}
