﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Render;

namespace NetMap.Interfaces
{
    /// <summary>
    /// 影像图层
    /// </summary>
    public interface IImageLayer : ILayer
    {
        IImageRender Render { get; set; }
    }
}
