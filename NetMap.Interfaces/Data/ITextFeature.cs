﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Data
{
    /// <summary>
    /// 注记要素接口
    /// </summary>
    public interface ITextFeature : IFeature
    {
        /// <summary>
        /// 获取和设置文本内容
        /// </summary>
        string Text { get; set; }
    }
}
