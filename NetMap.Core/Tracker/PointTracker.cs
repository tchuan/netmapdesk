﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Tracker;
using NetMap.Interfaces.Geometry;
using NetMap.Geometry;
using NetMap.Interfaces.Symbol;
using NetMap.Interfaces.Render;

namespace NetMap.Tracker
{
    public class PointTracker : Tracker, IPointTracker
    {
        private bool _bsetPosition = false;
        private IPoint _position = new Point(0, 0);
        private IPoint _moveP = new Point(0, 0);

        public PointTracker(ISymbol symbol, ISymbolRender symbolRender)
            : base(symbol, symbolRender)
        {
        }

        public override void Reset()
        {
            _bstart = false;
            _bsetPosition = false;
        }

        public override void Refresh(bool bClear)
        {
            if (_bsetPosition)
            {
                _symbolRender.Draw(_symbol, _position);
            }
            if (_bstart)
            {
                _symbolRender.Draw(_symbol, _moveP);
            }
        }

        public override IGeometry GetGeometry()
        {
            return _bsetPosition ? _position : null;
        }

        public override void Start(IPoint point)
        {
            _bstart = true;
        }

        public override void MoveTo(IPoint point)
        {
            _moveP.X = point.X;
            _moveP.Y = point.Y;
        }

        public override void Finish()
        {
            _bstart = false;
        }

        #region IPointTracker 成员

        public void SetPoint(IPoint point)
        {
            _position.X = point.X;
            _position.Y = point.Y;
            _bsetPosition = true;
        }

        #endregion
    }
}
