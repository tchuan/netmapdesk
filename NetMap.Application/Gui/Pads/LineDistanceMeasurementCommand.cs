﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces;
using NetMap.Interfaces.Symbol;
using NetMap.Symbol;
using System.Drawing;
using NetMap.Tracker;
using System.Windows.Forms;
using NetMap.Interfaces.Geometry;
using NetMap.Render;
using NetMap.Interfaces.Render;
using NetMap.Geometry;

namespace NetMap.Application.Gui.Pads
{
    /// <summary>
    /// 线测距
    /// 说明：左键连续测量，右键清除
    /// </summary>
    public class LineDistanceMeasurement : Tool
    {
        private List<CreateLineStringTracker> _trackers;
        private ISymbolDrawer _textsDrawer; 
        private bool _isStart = false;
        private bool _isFirst = true;
        private ITextSymbol _textSymbol;
        private double _totalDistance = 0.0;
        private IPoint _endPoint;
        private DateTime _lastClick;
        private StrokeSymbol _symbol;

        public LineDistanceMeasurement()
            : base("线测距")
        {
        }

        public LineDistanceMeasurement(IMapEditView view)
            : base("线测距", view)
        {
        }

        public override void Init()
        {
            base.Init();
            _symbol = new StrokeSymbol
            {
                Pen =
                {
                    Width = 3,
                    Color = Color.FromArgb(200, 0, 200)
                }
            };
            _textSymbol = new TextSymbol
            {
                Color = Color.FromArgb(200, 0, 200)
            };
            _textsDrawer = SymbolDrawerFactory.Instance.GetDrawer(_textSymbol);
            _trackers = new List<CreateLineStringTracker>();

            _lastClick = DateTime.Now;
        }

        public override void GainFocus()
        {
            base.GainFocus();
            _isStart = false;
            _isFirst = true;
        }

        public override void LostFocus()
        {
            base.LostFocus();
            _isStart = false;
            _isFirst = true;
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
                return;

            if ((DateTime.Now - _lastClick).TotalMilliseconds <= 500)
            {
                _isFirst = true;
                _isStart = false;
            }
            else
            {
                if (_isFirst)
                {
                    _isFirst = false;
                    _isStart = true;
                    _trackers.Add(new CreateLineStringTracker(_symbol));
                    _trackers.Last().AddPoint(e.X, e.Y);
                }
                _lastClick = DateTime.Now;
                _trackers.Last().AddPoint(e.X, e.Y);

                View.Refresh();
            }
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            if (_isStart)
            {
                _trackers.Last().ChangeEndPoint(e.X, e.Y);
                View.Refresh();
            }
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right)
                return;
            if (!_isStart)
                return;
            _isStart = false;
            _isFirst = true;
            View.Refresh();
        }

        public override void Draw(Graphics g)
        {
            if (!_isStart)
                return;

            //绘制测距图形
            foreach (var tracker in _trackers)
            {
                tracker.Draw(g);
            }
            //绘制测距的距离
            UpdateDistance(g);
        }

        //更新距离和最后点
        private void UpdateDistance(Graphics g)
        {
            foreach (var tracker in _trackers)
            {
                ILineString lineString = tracker.GetGeometry() as ILineString;
                IDisplayTransformation trans = View.DisplayTransformation;
                List<IPoint> newLine = new List<IPoint>();
                double x = 0, y = 0;
                foreach (IPoint point in lineString)
                {
                    trans.ToMapPoint(point.X, point.Y, ref x, ref y);
                    newLine.Add(new NetMap.Geometry.Point(x, y));
                }
                ILineString line = new LineString(newLine);
                _totalDistance = DistanceCalculator.GetTotalDistance(line);
                _endPoint = lineString.EndPoint;

                var label = _totalDistance.ToString("F4") + "公里"; //保留4位小数
                _textsDrawer.Draw(g, _textSymbol, _endPoint, label); 
            }
        }
    }

    /// <summary>
    /// 线测距命令
    /// </summary>
    public class LineDistanceMeasurementCommand : MapCommand
    {
        public LineDistanceMeasurementCommand()
        {
            InnerCmd = new LineDistanceMeasurement();
        }
    }
}
