﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces
{
    /// <summary>
    /// 地图视图集合
    /// </summary>
    public interface IMapViewCollection : IEnumerable<MapViewData>
    {
        /// <summary>
        /// 添加地图视图
        /// </summary>
        /// <param name="view"></param>
        void Add(MapViewData view);
        /// <summary>
        /// 移除地图视图
        /// </summary>
        /// <param name="view"></param>
        void Remove(MapViewData view);
        /// <summary>
        /// 清理
        /// </summary>
        void Clear();
    }
}
