﻿using NetMap.Interfaces.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Tracker
{
    /// <summary>
    /// 创建路径的橡皮条接口
    /// 说明：1.构建
    /// </summary>
    public interface ICreatePathTracker
    {
        /// <summary>
        /// 增加路径点
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        void Add(double x, double y);
        /// <summary>
        /// 设置最近加入的点的控制点
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        void SetControlPoint(double x, double y);
        /// <summary>
        /// 后退一点
        /// </summary>
        void Back();
        /// <summary>
        /// 获取是否封闭
        /// </summary>
        bool IsClose
        {
            get;
        }
        /// <summary>
        /// 执行自动封闭操作
        /// </summary>
        void Close();
        /// <summary>
        /// 打开结尾的封闭
        /// </summary>
        void Open();
    }
}
