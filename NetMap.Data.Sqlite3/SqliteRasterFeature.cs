﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Data;
using NetMap.Interfaces.Data;
using NetMap.Interfaces.Geometry;

/*
 * 2013-6-28：未实现
 */

namespace NetMap.Sqlite
{
    /// <summary>
    /// Sqlite数据库栅格要素
    /// </summary>
    internal class SqliteRasterFeature : SqliteFeature, IRasterFeature
    {
        protected bool _rasterModify = false; //栅格改变标志
        protected SpaceImage _image = null;

        internal SqliteRasterFeature()
        {
        }

        #region IRasterFeature 成员

        public SpaceImage Image
        {
            get
            {
                return _image;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        public override geoFeatureType FeatureType
        {
            get { return geoFeatureType.GEO_FEATURE_RASTER; }
        }

        protected override void SetGeometry(IGeometry geometry)
        {
            //TODO
            base.SetGeometry(geometry);
        }

        protected override IGeometry GetGeometry()
        {
            //TODO
            return base.GetGeometry();
        }
    }
}
