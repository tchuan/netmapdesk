﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Data;
using NetMap.Interfaces.Geometry;

namespace NetMap.Data
{
    public class SpatialFilter : QueryFilter, ISpatialFilter
    {        
        public SpatialFilter()
        {
            SpatialRelationship = geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_ENVELOPEINTERSECTS;
        }

        public geoSpatialRelationship SpatialRelationship
        {
            get;
            set;
        }

        public IGeometry Geometry
        {
            get;
            set;
        }
    }
}
