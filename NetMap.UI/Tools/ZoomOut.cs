﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Geometry;
using NetMap.UI.Properties;
using NetMap.Interfaces;

namespace NetMap.Tools
{
    /// <summary>
    /// 缩小工具
    /// </summary>
    public class ZoomOut : Tool
    {
        private IDisplayTransformation _disTrans = null;

        public ZoomOut()
            : base("缩小")
        {
            _icon = Resources.缩小;
        }

        public ZoomOut(IMapEditView view)
            : base("缩小", view)
        {
            _icon = Resources.缩小;
        }

        public override void GainFocus()
        {
            _disTrans = View.DisplayTransformation;
            base.GainFocus();
        }

        public override void LostFocus()
        {
            _disTrans = null;
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            double x = 0, y = 0;
            _disTrans.ToMapPoint(e.X, e.Y, ref x, ref y);
            double scale =_disTrans.ScaleRatio * 0.5;
            View.ZoomToCenter(x, y, scale);
            View.Refresh();
        }
    }
}
