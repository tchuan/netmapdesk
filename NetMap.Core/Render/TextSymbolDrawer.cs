﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Symbol;

namespace NetMap.Render
{
    /// <summary> 文本符号绘制</summary>
    internal class TextSymbolDrawer : ISymbolDrawer
    {
        private StringFormat _stringFormat;

        public TextSymbolDrawer()
        {
            _stringFormat = new StringFormat();
            _stringFormat.Alignment = StringAlignment.Center;
            _stringFormat.LineAlignment = StringAlignment.Center;
        }

        public void Draw(IDisplay display, ISymbol symbol, IGeometry geometry, string text)
        {
            if (symbol == null || geometry == null || display == null || text == null) return;
            ITextSymbol textSymbol = symbol as ITextSymbol;
            if (geometry is IMultiPolygon)
            {
                InnerDraw(display,textSymbol,geometry as IMultiPolygon,text);
            }
            else if (geometry is IMultiPoint)
            {
                InnerDraw(display, textSymbol, geometry as IMultiPoint, text);
            }
            else if (geometry is IMultiLineString)
            {
                InnerDraw(display, textSymbol, geometry as IMultiLineString, text);
            }
            else if (geometry is IPolygon)
            {
                InnerDraw(display, textSymbol, geometry as IPolygon, text);
            }
            else if (geometry is ILineString)
            {
                InnerDraw(display, textSymbol, geometry as ILineString, text);
            }
            else if (geometry is IDirectionPoint)
            {
                InnerDraw(display, textSymbol, geometry as IDirectionPoint, text);
            }
            else if (geometry is IPoint)
            {
                InnerDraw(display,textSymbol,geometry as IPoint,text);
            }
        }

        public void Draw(Graphics g, ISymbol symbol, IGeometry geometry, string text)
        {
            switch (geometry.GeometryType)
            {
                case geoGeometryType.GEO_GEOMETRY_POINT:
                    {
                        IPoint point = geometry as IPoint;
                        InnerDirectlyDraw(g, symbol as ITextSymbol, (float)point.X, (float)point.Y, text);
                        break;
                    }
                    //TODO Other
            }
        }

        #region Draw

        private void InnerDraw(IDisplay display, ITextSymbol symbol, IPoint geometry, string text)
        {
            double dx = 0, dy = 0;
            IDisplayTransformation displayTransformation = display.DisplayTransformation;
            displayTransformation.FromMapPoint(geometry.X, geometry.Y, ref dx, ref dy);
            InnerDirectlyDraw(display.G, symbol, (float)dx, (float)dy, text);
        }

        private void InnerDraw(IDisplay display, ITextSymbol symbol, IDirectionPoint geometry, string text)
        {
            double dx = 0; double dy=0;
            display.DisplayTransformation.FromMapPoint(geometry.X,geometry.Y,ref dx,ref dy);
            SizeF size = display.G.MeasureString(text, new Font(symbol.Font, (float)symbol.Width));
            PointF pf = new PointF(Convert.ToInt32(dx - size.Width * 0.5), Convert.ToInt32(dy - size.Height * 0.5));
            Matrix myMatrix = new Matrix();
            myMatrix.RotateAt((float)(geometry.Angle),pf);
            display.G.Transform = myMatrix;
            display.G.DrawString(text, new Font(symbol.Font, (float)symbol.Width), new SolidBrush(Color.FromArgb(symbol.Color.A, symbol.Color.R, symbol.Color.G, symbol.Color.B)), pf);
            myMatrix.RotateAt(-(float)(geometry.Angle), pf);
            display.G.Transform = myMatrix;
        }

        private void InnerDraw(IDisplay display, ITextSymbol symbol, ILineString geometry, string text)
        {
            //
            // 实现
            //
        }

        private void InnerDraw(IDisplay display, ITextSymbol symbol, IPolygon geometry, string text)
        {
            //
            // 实现
            //
        }

        private void InnerDraw(IDisplay display, ITextSymbol symbol, IMultiLineString geometry, string text)
        {
            //
            // 实现
            //
        }

        private void InnerDraw(IDisplay display, ITextSymbol symbol, IMultiPoint geometry, string text)
        {
            //
            // 实现
            //
        }

        private void InnerDraw(IDisplay display, ITextSymbol symbol, IMultiPolygon geometry, string text)
        {
            //
            // 实现
            //
        }

        #endregion

        #region DirectlyDraw

        private void InnerDirectlyDraw(Graphics g, ITextSymbol symbol, float x, float y, string text)
        {
            Matrix oldMatrix = g.Transform;
            Matrix matrix = new Matrix();
            //支持偏移
            x += (float)symbol.OffsetX;
            y += (float)symbol.OffsetY;
            matrix.Translate(x, y);
            if (symbol.Angle + symbol.Rotate != 0)
            {
                matrix.Rotate((float)(360 - (symbol.Angle + symbol.Rotate)));
            }
            g.Transform = matrix;
            Font font = new Font(symbol.Font, (float)symbol.Height);
            Brush brush = new SolidBrush(symbol.Color);            
            g.DrawString(text, font, brush, 0, 0, _stringFormat);
            brush.Dispose();
            font.Dispose();
            g.Transform = oldMatrix;
        }

        #endregion
    }
}
