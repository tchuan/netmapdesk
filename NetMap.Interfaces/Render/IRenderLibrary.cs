﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Render
{
    /// <summary>
    /// 图层样式库接口，负责管理一组图层样式
    /// </summary>
    public interface IRenderLibrary : IEnumerable<ILayerRender>
    {
        /// <summary>
        /// 获取和设置名称
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// 查找
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        ILayerRender Find(string name);
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="render"></param>
        void Add(ILayerRender render);
        /// <summary>
        /// 移除
        /// </summary>
        /// <param name="render"></param>
        void Remove(ILayerRender render);
        /// <summary>
        /// 是否存在指定名称的图层样式
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        bool Exist(string name);
        /// <summary>
        /// 获取图层样式数目
        /// </summary>
        int RenderCount { get; }
    }
}
