﻿// Copyright (c) AlphaSierraPapa for the KJSuite Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

using System;
using System.Linq;
using NetMap.Application.Core;
using NetMap.Application.Gui;
using NetMap.Application;

namespace NetMap.StartPage
{
	public class ShowStartPageCommand : AbstractMenuCommand
	{	
		public override void Run()
		{
            PadDescriptor pad = WorkbenchSingleton.Workbench.GetPad(typeof(StartPageViewContent));
            WorkbenchSingleton.Workbench.ShowPad(pad);
		}
	}
}
