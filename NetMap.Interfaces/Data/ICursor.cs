﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Data
{
    /// <summary>
    /// 游标接口
    /// </summary>
    public interface ICursor
    {
        /// <summary>
        /// 获取字段集合
        /// </summary>
        IFields Fields { get; }
        /// <summary>
        /// 获取下一行
        /// </summary>
        /// <returns></returns>
        IRow Next();
        /// <summary>
        /// 重置
        /// </summary>
        void Reset();
        /// <summary>
        /// 关闭
        /// </summary>
        void Close();
        /// <summary>
        /// 获取游标所指向的表
        /// </summary>
        ITable Table { get; }
    }
}
