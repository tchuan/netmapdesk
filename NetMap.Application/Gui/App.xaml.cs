﻿// Copyright (c) AlphaSierraPapa for the SharpDevelop Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

using System;

namespace NetMap.Application.Gui
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	partial class App : System.Windows.Application
	{
		public App()
		{
			InitializeComponent();
		}
	}
}
