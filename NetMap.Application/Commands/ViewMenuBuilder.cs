﻿using NetMap.Application.Core;
using NetMap.Application.Core.Presentation;
using NetMap.Application.Gui;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace NetMap.Application.Commands
{
    public abstract class ViewMenuBuilder : IMenuItemBuilder
    {
        class BringPadToFrontCommand : System.Windows.Input.ICommand
		{
			PadDescriptor padDescriptor;
			
			public BringPadToFrontCommand(PadDescriptor padDescriptor)
			{
				this.padDescriptor = padDescriptor;
			}
			
			public event EventHandler CanExecuteChanged { add {} remove {} }
			
			public void Execute(object parameter)
			{
				padDescriptor.BringPadToFront();
			}
			
			public bool CanExecute(object parameter)
			{
				return true;
			}
		}
		
		protected abstract string Category {
			get;
		}
		
		public ICollection BuildItems(Codon codon, object owner)
		{
			ArrayList list = new ArrayList();
			foreach (PadDescriptor padContent in WorkbenchSingleton.Workbench.PadContentCollection) {
				if (padContent.Category == Category) {
					var item = new System.Windows.Controls.MenuItem();
					item.Header = NetMap.Application.Core.Presentation.MenuService.ConvertLabel(StringParser.Parse(padContent.Title));
					if (!string.IsNullOrEmpty(padContent.Icon)) {
						item.Icon = PresentationResourceService.GetImage(padContent.Icon);
					}
					item.Command = new BringPadToFrontCommand(padContent);
					if (!string.IsNullOrEmpty(padContent.Shortcut)) {
						var kg = Core.Presentation.MenuService.ParseShortcut(padContent.Shortcut);
						WorkbenchSingleton.MainWindow.InputBindings.Add(
							new System.Windows.Input.InputBinding(item.Command, kg)
						);
						item.InputGestureText = kg.GetDisplayStringForCulture(Thread.CurrentThread.CurrentUICulture);
					}
					
					list.Add(item);
				}
			}
			return list;
		}
    }
}
