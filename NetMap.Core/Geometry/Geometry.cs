﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;
using System.IO;

namespace NetMap.Geometry
{
    public abstract class Geometry : IGeometry
    {
        protected IEnvelope _box = null;

        #region IGeometry 成员

        public IEnvelope Envelope
        {
            get { return _box; }
            set { _box = value; }
        }

        public abstract geoGeometryType GeometryType
        {
            get;
        }

        public abstract void UpdateEnvelope();

        public abstract IGeometry CloneGeometry();

        public abstract byte[] ExportToWkb();

        public abstract string ExportToWkt();

        public abstract void ImportFromWkb(byte[] data);

        public abstract void ImportFromWkt(string data);

        public abstract void Export(BinaryWriter writer);

        public abstract void Import(BinaryReader reader);

        #endregion

        /// <summary>
        /// 从wkb创建图形
        /// </summary>
        /// <param name="wkb"></param>
        /// <returns></returns>
        public static IGeometry CreateFromWkb(byte[] wkb)
        {
            //TODO
            return null;
        }

        /// <summary>
        /// 从wkt创建图形
        /// </summary>
        /// <param name="wkt"></param>
        /// <returns></returns>
        public static IGeometry CreateFromWkt(string wkt)
        {
            //TODO
            return null;
        }

        public static byte[] ExportToWkb(IGeometry geometry)
        {
            return geometry.ExportToWkb();
        }

        public static string ExportToWkt(IGeometry geometry)
        {
            return geometry.ExportToWkt();
        }
    }
}
