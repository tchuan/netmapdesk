﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Data;

namespace NetMap.Data
{
    /// <summary>
    /// 数据库工厂
    /// </summary>
    public class DatabaseFactory
    {
        private Dictionary<string, IDatabaseFactory> _registerDic = new Dictionary<string, IDatabaseFactory>();
        private Dictionary<string, ISpatialDatabaseFactory> _registerSDic = new Dictionary<string, ISpatialDatabaseFactory>();

        private DatabaseFactory()
        {
        }

        private static DatabaseFactory _ins = null;
        /// <summary>
        /// 获取实例
        /// </summary>
        public static DatabaseFactory Instance
        {
            get
            {
                if (_ins == null)
                {
                    _ins = new DatabaseFactory();
                }
                return _ins;
            }
        }

        /// <summary>
        /// 查找数据库工厂
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public IDatabaseFactory FindFactory(string name)
        {
            IDatabaseFactory fac = null;
            _registerDic.TryGetValue(name, out fac);
            return fac;
        }

        /// <summary>
        /// 查找空间数据库工厂
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public ISpatialDatabaseFactory FindSpatialFactory(string name)
        {
            ISpatialDatabaseFactory fac = null;
            _registerSDic.TryGetValue(name, out fac);
            return fac;
        }

        /// <summary>
        /// 注册数据库工厂
        /// </summary>
        /// <param name="name"></param>
        /// <param name="factory"></param>
        public void Register(string name, IDatabaseFactory factory)
        {
            _registerDic.Add(name, factory);
        }

        /// <summary>
        /// 注册空间数据库工厂
        /// </summary>
        /// <param name="name"></param>
        /// <param name="factory"></param>
        public void RegisterSpatial(string name, ISpatialDatabaseFactory factory)
        {
            _registerSDic.Add(name, factory);
        }

        /// <summary>
        /// 清理注册的数据库工厂和空间数据库工厂
        /// </summary>
        public void Clear()
        {
            _registerDic.Clear();
            _registerSDic.Clear();
        }
    }
}
