﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Geometry;
using System.Drawing;

namespace NetMap.UI
{
    partial class MapCtrl2
    {
        private IStatus _status; //状态展示

        #region IMapView 接口

        Color IMapView.BackColor
        {
            get
            {
                return base.BackColor;
            }
            set
            {
                base.BackColor = value;
            }
        }

        IDisplay IMapView.Display
        {
            get
            {
                return _display;
            }
        }

        IDisplayTransformation IMapView.DisplayTransformation
        {
            get
            {
                return _displayTF;
            }
        }

        IRenderCancel IMapView.GetCancel()
        {
            throw new NotImplementedException("不支持渲染控制");
        }

        void IMapView.Clear()
        {
            this.CreateGraphics().Clear(BackColor);
        }

        void IMapView.Refresh()
        {
            RefreshEffect();
        }

        void IMapView.Invalidate()
        {
            ViewChanged(true);
            InvalidateMap(true);
        }

        IMapDocument IMapView.MapDocument
        {
            get
            {
                return _map;
            }
            set
            {
                IMapDocument temp = _map;
                _map = null;

                if (temp != null)
                {
                    IMapDocumentEvent tempEvent = temp as IMapDocumentEvent;
                    tempEvent.DataChanged -= MapDataChange;
                }

                _map = value;
                IMapDocumentEvent mapEvent = _map as IMapDocumentEvent;
                mapEvent.DataChanged += MapDataChange;
                _displayTF.MapBounds.Update(_map.Extent);

                ViewChanged(true);
                InvalidateMap(false);
            }
        }

        void IMapView.ZoomToScale(double scale)
        {
            _displayTF.ZoomToScale(scale);
            if (!_viewInitialized)
            {
                _viewInitialized = true;
                ViewChanged(true);
            }
            //触发视图比例尺改变事件
            OnMapViewScaleChanged(_displayTF.ScaleRatio);
        }

        void IMapView.ZoomToWindow(IEnvelope window)
        {
            _displayTF.ZoomToWindow(window);
            if (!_viewInitialized)
            {
                _viewInitialized = true;
                ViewChanged(true);
            }
            //触发视图比例尺改变事件
            OnMapViewScaleChanged(_displayTF.ScaleRatio);
        }

        void IMapView.ZoomToData(IEnvelope box)
        {
            _displayTF.ZoomToDataBounds(box);
            if (!_viewInitialized)
            {
                _viewInitialized = true;
                ViewChanged(true);
            }
            //触发视图比例尺改变事件
            OnMapViewScaleChanged(_displayTF.ScaleRatio);
        }

        void IMapView.ZoomToMap()
        {
            _displayTF.ZoomToMapBounds();
            if (!_viewInitialized)
            {
                _viewInitialized = true;
                ViewChanged(true);
            }
            //触发视图比例尺改变事件
            OnMapViewScaleChanged(_displayTF.ScaleRatio);
        }

        void IMapView.ZoomToCenter(double x, double y, double scale)
        {
            _displayTF.ZoomToCenter(x, y, scale);
            if (!_viewInitialized)
            {
                _viewInitialized = true;
                ViewChanged(true);
            }
            //触发视图比例尺改变事件
            OnMapViewScaleChanged(_displayTF.ScaleRatio);
        }

        void IMapView.ShowTip(string tip, bool active)
        {
            pointToolTip.SetToolTip(this, tip);
            pointToolTip.Active = active;
        }

        IStatus IMapView.Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        } 

        #endregion
    }
}
