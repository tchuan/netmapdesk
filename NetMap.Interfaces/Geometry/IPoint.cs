﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Geometry
{
    /// <summary>
    ///点
    /// </summary>
    public interface IPoint : IGeometry
    {
        /// <summary>
        /// X坐标
        /// </summary>
        double X { get; set; }
        /// <summary>
        /// Y坐标
        /// </summary>
        double Y { get; set; }
    }
}
