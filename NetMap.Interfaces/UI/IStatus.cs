﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces
{
    /// <summary>
    /// 状态展示接口
    /// </summary>
    public interface IStatus
    {
        /// <summary>
        /// 展示信息
        /// </summary>
        /// <param name="info"></param>
        void ShowInfo(string info);

        /// <summary>
        /// 展示地图比例尺
        /// </summary>
        void ShowMapScale(double scale);

        /// <summary>
        /// 展示鼠标处的地图坐标
        /// </summary>
        /// <param name="mouseX"></param>
        /// <param name="mouseY"></param>
        void ShowMousePosition(double mouseX, double mouseY);
    }
}
