﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Tracker;
using NetMap.Interfaces.Symbol;
using NetMap.Interfaces.Geometry;
using NetMap.Geometry;
using NetMap.Interfaces.Render;

namespace NetMap.Tracker
{
    public class TextTracker : Tracker, ITextTracker
    {
        private ITextSymbol _textSym = null;
        private string _text = string.Empty;
        private IDirectionPoint _position = new DirectionPoint(0, 0, 0);        
        private bool _bsetPosition = false;
        private IPoint _moveP = new NetMap.Geometry.Point(0, 0);

        public TextTracker(ISymbol symbol, ISymbolRender symbolRender)
            : base(symbol, symbolRender)
        {
            _textSym = symbol as ITextSymbol;
        }

        public override void Reset()
        {
            _bstart = false;
            _bsetPosition = false;
        }

        public override void Refresh(bool bClear)
        {
            if (_textSym == null)
            {
                return;
            }
            if (_bsetPosition)
            {
                //_textSym.DrawText(_position, _text); //TODO 修改
            }
            else if (_bstart)
            {
                //_textSym.DrawText(_moveP, _text); //TODO 修改
            }
        }

        public override IGeometry GetGeometry()
        {
            if (_bsetPosition)
            {
                return _position;
            }
            else
            {
                return null;
            }
        }

        public override void Start(IPoint point)
        {
            _bstart = true;
        }

        public override void MoveTo(IPoint point)
        {
            _moveP.X = point.X;
            _moveP.Y = point.Y;
        }

        public override void Finish()
        {
            _bstart = false;
        }

        #region ITextTracker 成员

        public string Text
        {
            get
            {
                return _text;
            }
            set
            {
                _text = value;
            }
        }

        public void SetPoint(Interfaces.Geometry.IPoint point)
        {
            _position.X = point.X;
            _position.Y = point.Y;
            _bsetPosition = true;
        }

        public void SetAngle(double angle)
        {
            _position.Angle = angle;
        }

        #endregion

    }
}
