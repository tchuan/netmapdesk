﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using NetMap.Interfaces;

namespace NetMap
{
    /// <summary>
    /// 工具基类
    /// </summary>
    public abstract class Tool : Command, ITool
    {
        protected IMapEditView _editView = null;
        protected Cursor _cursor = Cursors.Arrow; //默认为箭头图标

        public Tool(string name)
            : base(name)
        {
            SetCursor();
        }

        public Tool(string name, IMapEditView editView)
            : base(name, editView)
        {
            _editView = editView;
        }

        #region Virtuals

        protected virtual void SetCursor()
        {
            _cursor = Cursors.Arrow; //默认为箭头
        }

        #endregion

        #region ITool 成员

        public virtual void GainFocus()
        {
            //默认为空
        }

        public virtual void LostFocus()
        {
            //默认为空
        }

        public virtual void OnMouseDown(MouseEventArgs e)
        {
            //默认为空
        }

        public virtual void OnMouseMove(MouseEventArgs e)
        {
            //默认为空
        }
        public virtual void OnMouseWheel(MouseEventArgs e)
        {
            //默认为空
        }
        public virtual void OnMouseEnter(EventArgs e)
        {
            //默认为空
        }

        public virtual void OnMouseLeave(EventArgs e)
        {
            //默认为空
        }
        public virtual void OnMouseUp(MouseEventArgs e)
        {
            //默认为空
        }

        public virtual void OnKeyDown(KeyEventArgs e)
        {
            //默认为空
        }

        public virtual void OnKeyUp(KeyEventArgs e)
        {
            //默认为空
        }

        public virtual void Draw(Graphics g)
        {
            //默认为空
        }

        public virtual void OnSetCursor(Cursor cursor)
        {
            //默认为空
        }

        public Cursor Cursor
        {
            get
            {
                return _cursor;
            }
            set
            {
                _cursor = value;
            }
        }

        public IMapEditView EditView
        {
            get
            {
                return _editView;
            }
            set
            {
                _editView = value;
                View = _editView;
            }
        }

        #endregion        
    }
}
