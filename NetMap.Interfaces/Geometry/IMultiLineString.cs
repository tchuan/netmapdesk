﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Geometry
{
    /// <summary>
    /// 多多段线接口
    /// </summary>
    public interface IMultiLineString : IGeometry, IList<ILineString>
    {
    }
}
