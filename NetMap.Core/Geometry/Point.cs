﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NetMap.Core;
using NetMap.Interfaces.Geometry;

namespace NetMap.Geometry
{
    public class Point : Geometry, IPoint
    {
        protected double _x = 0;
        protected double _y = 0;

        public Point()
        {
        }

        public Point(double x, double y)
        {
            _x = x;
            _y = y;
            _box = new Envelope(_x, _y, _x, _y);
        }

        public Point(IPoint point)
            : this(point.X, point.Y)
        {
        }

        #region IPoint 成员

        public double X
        {
            get
            {
                return _x;
            }
            set
            {
                _x = value;
            }
        }

        public double Y
        {
            get
            {
                return _y;
            }
            set
            {
                _y = value;
            }
        }

        #endregion

        public override geoGeometryType GeometryType
        {
            get 
            { 
                return geoGeometryType.GEO_GEOMETRY_POINT; 
            }
        }

        public override void UpdateEnvelope()
        {
            if (_box == null)
            {
                _box = new Envelope(_x, _y, _x, _y);
            }
            else
            {
                _box.Update(_x, _y, _x, _y);
            }
        }

        public override IGeometry CloneGeometry()
        {
            IPoint pt = new Point(_x, _y);
            return pt;
        }

        public override byte[] ExportToWkb()
        {
            MemoryStream stream = new MemoryStream(16);
            BinaryWriter writer = new BinaryWriter(stream);
            writer.Write(_x);
            writer.Write(_y);
            return stream.ToArray();
        }

        public override string ExportToWkt()
        {
            throw new NotImplementedException();
        }

        public override void ImportFromWkb(byte[] data)
        {
            using (MemoryStream stream = new MemoryStream(data))
            {
                BinaryReader reader = new BinaryReader(stream);
                _x = reader.ReadDouble();
                _y = reader.ReadDouble();
                reader.Close();
            }
        }

        public override void ImportFromWkt(string data)
        {
            //TODO
        }

        // x y
        public override void Export(BinaryWriter writer)
        {
            writer.Write(X);
            writer.Write(Y);
        }

        public override void Import(BinaryReader reader)
        {
            X = reader.ReadDouble();
            Y = reader.ReadDouble();
        }
    }
}
