﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Common;
using NetMap.Interfaces.Symbol;
using ProjNet.CoordinateSystems;
using NetMap.Interfaces.Render;

namespace NetMap.Interfaces
{
    /// <summary>
    /// 地图文档接口
    /// </summary>
    public interface IMapDocument
    {
        /// <summary>
        /// 获取和设置名字
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// 获取和设置描述信息
        /// </summary>
        string Description { get; set; }
        /// <summary>
        /// 获取或设置最大显示比例
        /// </summary>
        double MaxScale { get; set; }
        /// <summary>
        /// 获取和设置最小显示比例
        /// </summary>
        double MinScale { get; set; }
        /// <summary>
        /// 获取图层数目
        /// </summary>
        int LayerCount { get; }
        /// <summary>
        /// 获取和设置地图默认的显示比例尺
        /// </summary>
        double Scale { get; set; }
        /// <summary>
        /// 获取和设置空间参考坐标系
        /// </summary>
        ICoordinateSystem CoordinateSystem { get; set; }
        /// <summary>
        /// 获取指定索引处的图层
        /// </summary>
        /// <param name="index">图层索引</param>
        /// <returns>图层</returns>
        ILayer GetLayer(int index);
        /// <summary>
        /// 在指定位置插入图层
        /// </summary>
        /// <param name="layer"></param>
        void InsertLayer(int index, ILayer layer);
        /// <summary>
        /// 添加层
        /// </summary>
        /// <param name="layer">图层</param>
        void AddLayer(ILayer layer);
        /// <summary>
        /// 删除层
        /// </summary>
        /// <param name="layer">图层</param>
        void DeleteLayer(ILayer layer);
        /// <summary>
        /// 清楚并释放所有层
        /// </summary>
        void ClearLayer();
        /// <summary>
        /// 移动层
        /// </summary>
        /// <param name="targetLayer">目标图层</param>
        /// <param name="refLayer">参考图层</param>
        /// <param name="relation">移动后与参考图层的关系</param>
        void MoveLayer(ILayer targetLayer, ILayer refLayer, MutualRelationship relation);
        /// <summary>
        /// 通过层名查找图层
        /// </summary>
        /// <param name="name">层名</param>
        /// <returns></returns>
        ILayer FindLayer(string name);
        /// <summary>
        /// 重新计算范围
        /// </summary>
        void RecalExtent();
        /// <summary>
        /// 获取和设置地图范围
        /// </summary>
        IEnvelope Extent { get; set; }
        /// <summary>
        /// 当前图层
        /// </summary>
        ILayer CurrentLayer { get; set; }
        /// <summary>
        /// 获取图层
        /// </summary>
        /// <returns></returns>
        IEnumerable<ILayer> GetLayers();
        /// <summary>
        /// 由视图改变驱动数据加载
        /// </summary>
        /// <param name="changeEnd">是否完成</param>
        /// <param name="trans">视图信息</param>
        void ViewChanged(bool changeEnd, IDisplayTransformation trans);
        /// <summary>
        /// 取消数据加载
        /// </summary>
        void AbortFetch();
    }
}
