﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace NetMap.Interfaces
{
    /// <summary>工具接口</summary>
    public interface ITool : ICommand
    {
        /// <summary>获取焦点</summary>
        void GainFocus();
        /// <summary>失去焦点</summary>
        void LostFocus();
        /// <summary>鼠标按下</summary>
        /// <param name="e"></param>
        void OnMouseDown(MouseEventArgs e);
        /// <summary>
        /// 鼠标拖动</summary>
        /// <param name="e"></param>
        void OnMouseMove(MouseEventArgs e);
        /// <summary>鼠标弹起</summary>
        /// <param name="e"></param>
        void OnMouseUp(MouseEventArgs e);
        /// <summary>鼠标滑轮滚动</summary>
        /// <param name="e"></param>
        void OnMouseWheel(MouseEventArgs e);
        /// <summary>鼠标在控件内</summary>
        /// <param name="e"></param>
        void OnMouseEnter(EventArgs e);
        /// <summary>鼠标离开控件</summary>
        /// <param name="e"></param>
        void OnMouseLeave(EventArgs e);
        /// <summary>键盘按下</summary>
        /// <param name="e"></param>
        void OnKeyDown(KeyEventArgs e);
        /// <summary>键盘弹起</summary>
        /// <param name="e"></param>
        void OnKeyUp(KeyEventArgs e);
        /// <summary>实时绘制</summary>
        /// <param name="g"></param>
        void Draw(Graphics g);
        /// <summary>工具对应的光标</summary>
        Cursor Cursor { get; set; }
        /// <summary>地图编辑视图</summary>
        IMapEditView EditView { get; set; }
    }
}
