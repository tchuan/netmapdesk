﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;
using System.Collections;

namespace NetMap.Tile
{
    /// <summary>
    /// 切片矩阵
    /// </summary>
    public class TileMatrix
    {
        /// <summary>
        /// 获取和设置Identifier，即层级
        /// </summary>
        public int Id
        {
            get;
            set;
        }

        /// <summary>
        /// 获取和设置比例尺分母
        /// </summary>
        public double ScaleDenominator
        {
            get;
            set;
        }

        /// <summary>
        /// 获取和设置左上角的x坐标
        /// </summary>
        public double TopLeftCornerX
        {
            get;
            set;
        }

        /// <summary>
        /// 获取和设置左上角的y坐标
        /// </summary>
        public double TopLeftCornerY
        {
            get;
            set;
        }

        /// <summary>
        /// 获取和设置瓦片宽度
        /// </summary>
        public int TileWidth
        {
            get;
            set;
        }

        /// <summary>
        /// 获取和设置瓦片高度
        /// </summary>
        public int TileHeight
        {
            get;
            set;
        }

        /// <summary>
        /// 获取和设置最大宽度
        /// </summary>
        public int MatrixWidth
        {
            get;
            set;
        }

        /// <summary>
        /// 获取和设置最大高度
        /// </summary>
        public int MatrixHeight
        {
            get;
            set;
        }
    }

    /// <summary>
    /// 切片索引
    /// </summary>
    public class TileIndex : IComparable
    {
        private readonly int _col;
        private readonly int _row;
        private readonly string _level;

        public int Col
        {
            get { return _col; }
        }

        public int Row
        {
            get { return _row; }
        }

        public string Level
        {
            get { return _level; }
        }

        public TileIndex(int col, int row, string level)
        {
            _col = col;
            _row = row;
            _level = level;
        }

        public int CompareTo(object obj)
        {
            if (!(obj is TileIndex))
            {
                throw new ArgumentException("object of type TileIndex was expected");
            }
            return CompareTo((TileIndex)obj);
        }

        public int CompareTo(TileIndex index)
        {
            if (_col < index._col) return -1;
            if (_col > index._col) return 1;
            if (_row < index._row) return -1;
            if (_row > index._row) return 1;
            return String.Compare(_level, index._level, StringComparison.Ordinal);
        }

        #region 重写

        public override bool Equals(object obj)
        {
            if (!(obj is TileIndex))
                return false;

            return Equals((TileIndex)obj);
        }

        public bool Equals(TileIndex index)
        {
            return _col == index._col && _row == index._row && _level == index._level;
        }

        public override int GetHashCode()
        {
            return _col ^ _row ^ _level.GetHashCode();
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0}层{1}行{2}列", _level, _row, _col);
            return builder.ToString();
        }

        #endregion

        #region 操作符重载

        public static bool operator ==(TileIndex key1, TileIndex key2)
        {
            return Equals(key1, key2);
        }

        public static bool operator !=(TileIndex key1, TileIndex key2)
        {
            return !Equals(key1, key2);
        }

        public static bool operator <(TileIndex key1, TileIndex key2)
        {
            return (key1.CompareTo(key2) < 0);
        }

        public static bool operator >(TileIndex key1, TileIndex key2)
        {
            return (key1.CompareTo(key2) > 0);
        }

        #endregion
    }
    
    /// <summary>
    /// 切片信息
    /// </summary>
    public class TileInfo
    {
        /// <summary>
        /// 切片的索引
        /// </summary>
        public TileIndex Index { get; set; }
        /// <summary>
        /// 切片的空间范围
        /// </summary>
        public IEnvelope Extent { get; set; }

        public override string ToString()
        {
            return Index.ToString();
        }
    }

    /// <summary>
    /// 切片范围
    /// </summary>
    public class TileRange
    {
        /// <summary>
        /// 首列
        /// </summary>
        public int FirstCol { get; private set; }
        /// <summary>
        /// 首行
        /// </summary>
        public int FirstRow { get; private set; }
        /// <summary>
        /// 列数
        /// </summary>
        public int ColCount { get; private set; }
        /// <summary>
        /// 行数
        /// </summary>
        public int RowCount { get; private set; }

        public TileRange(int col, int row)
            : this(col, row, 1, 1)
        {
        }

        public TileRange(int firstCol, int firstRow, int colCount, int rowCount)
        {
            FirstCol = firstCol;
            FirstRow = firstRow;
            ColCount = colCount;
            RowCount = rowCount;
        }        

        public bool Equals(TileRange tileRange)
        {
            return FirstCol == tileRange.FirstCol
                && ColCount == tileRange.ColCount
                && FirstRow == tileRange.FirstRow
                && RowCount == tileRange.RowCount;
        }

        #region 重写

        public override bool Equals(object obj)
        {
            if (!(obj is TileRange))
            {
                return false;
            }
            return Equals((TileRange)obj);
        }

        public override int GetHashCode()
        {
            return FirstCol ^ ColCount ^ FirstRow ^ RowCount;
        }

        #endregion

        #region 操作符重载

        public static bool operator ==(TileRange tileRange1, TileRange tileRange2)
        {
            return Equals(tileRange1, tileRange2);
        }

        public static bool operator !=(TileRange tileRange1, TileRange tileRange2)
        {
            return !(tileRange1 == tileRange2);
        }

        #endregion
    }
}
