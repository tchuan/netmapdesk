﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace NetMap.UI.Skin
{
    /// <summary>
    /// 菜单栏皮肤配置
    /// </summary>
    public class MenuStripSkinConfig
    {
        private Color _fontColor = Color.White;
        private Color _marginStartColor = Color.FromArgb(113, 113, 113);
        private Color _marginEndColor = Color.FromArgb(58, 58, 58);
        private Color _dropDownItemBackColor = Color.FromArgb(34, 34, 34);
        private Color _dropDownItemStartColor = Color.Orange;
        private Color _dropDownItemEndColor = Color.FromArgb(160, 100, 20);
        private Color _menuItemStartColor = Color.FromArgb(52, 106, 159);
        private Color _menuItemEndColor = Color.FromArgb(73, 124, 174);
        private Color _separatorColor = Color.Gray;
        private Color _mainMenuBackColor = Color.Black;
        private Color _mainMenuStartColor = Color.FromArgb(93, 93, 93);
        private Color _mainMenuEndColor = Color.FromArgb(34, 34, 34);
        private Color _dropDownBorderColor = Color.FromArgb(40, 96, 151);

        /// <summary>
        /// 菜单字体颜色
        /// </summary>
        public Color FontColor
        {
            get { return _fontColor; }
            set { _fontColor = value; }
        }

        /// <summary>
        /// 下拉菜单图标区域开始颜色
        /// </summary>
        public Color MarginStartColor
        {
            get { return _marginStartColor; }
            set { _marginStartColor = value; }
        }

        /// <summary>
        /// 下拉菜单图标区域结束颜色
        /// </summary>
        public Color MarginEndColor
        {
            get { return _marginEndColor; }
            set { _marginEndColor = value; }
        }

        /// <summary>
        /// 下拉项背景颜色
        /// </summary>
        public Color DropDownItemBackColor
        {
            get { return _dropDownItemBackColor; }
            set { _dropDownItemBackColor = value; }
        }

        /// <summary>
        /// 下拉项选中时开始颜色
        /// </summary>
        public Color DropDownItemStartColor
        {
            get { return _dropDownItemStartColor; }
            set { _dropDownItemStartColor = value; }
        }

        /// <summary>
        /// 下拉项选中时结束颜色
        /// </summary>
        public Color DropDownItemEndColor
        {
            get { return _dropDownItemEndColor; }
            set { _dropDownItemEndColor = value; }
        }

        /// <summary>
        /// 主菜单项选中时的开始颜色
        /// </summary>
        public Color MenuItemStartColor
        {
            get { return _menuItemStartColor; }
            set { _menuItemStartColor = value; }
        }

        /// <summary>
        /// 主菜单选中时的结束颜色
        /// </summary>
        public Color MenuItemEndColor
        {
            get { return _menuItemEndColor; }
            set { _menuItemEndColor = value; }
        }

        /// <summary>
        /// 分割线颜色
        /// </summary>
        public Color SeparatorColor
        {
            get { return _separatorColor; }
            set { _separatorColor = value; }
        }

        /// <summary>
        /// 主菜单背景色
        /// </summary>
        public Color MainMenuBackColor
        {
            get { return _mainMenuBackColor; }
            set { _mainMenuBackColor = value; }
        }

        /// <summary>
        /// 主菜单背景开始颜色
        /// </summary>
        public Color MainMenuStartColor
        {
            get { return _mainMenuStartColor; }
            set { _mainMenuStartColor = value; }
        }

        /// <summary>
        /// 主菜单背景结束颜色
        /// </summary>
        public Color MainMenuEndColor
        {
            get { return _mainMenuEndColor; }
            set { _mainMenuEndColor = value; }
        }

        /// <summary>
        /// 下拉区域边框颜色
        /// </summary>
        public Color DropDownBorderColor
        {
            get { return _dropDownBorderColor; }
            set { _dropDownBorderColor = value; }
        }
    }
}
