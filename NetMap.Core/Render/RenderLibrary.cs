﻿using NetMap.Interfaces.Persist;
using NetMap.Interfaces.Render;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace NetMap.Core.Render
{
    public class RenderLibrary : IRenderLibrary, IPersistResource
    {
        private List<ILayerRender> _renderLst = new List<ILayerRender>();

        #region IRenderLibrary 接口

        public string Name
        {
            get;
            set;
        }

        public ILayerRender Find(string name)
        {
            foreach (ILayerRender render in _renderLst)
            {
                if (render.Name == name)
                {
                    return render;
                }
            }
            return null;
        }

        public void Add(ILayerRender render)
        {
            if (Exist(render.Name))
            {
                throw new NotSupportedException("图层样式库不支持重名的图层样式");
            }
            _renderLst.Add(render);
        }

        public void Remove(ILayerRender render)
        {
            _renderLst.Remove(render);
        }

        public bool Exist(string name)
        {
            foreach (ILayerRender render in _renderLst)
            {
                if (render.Name == name)
                {
                    return true;
                }
            }
            return false;
        }

        public int RenderCount
        {
            get { return _renderLst.Count; }
        }

        public IEnumerator<ILayerRender> GetEnumerator()
        {
            return _renderLst.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _renderLst.GetEnumerator();
        }

        #endregion

        #region IPersistResource 接口

        public void Save(XmlElement node, XmlDocument document)
        {
            throw new NotImplementedException();
        }

        public void Load(XmlReader reader, object context)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
