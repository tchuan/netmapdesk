﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NetMap.UI.Properties;
using NetMap.Interfaces;
using NetMap;

namespace NetMap.Tools
{
    /// <summary>
    /// 打开图片
    /// </summary>
    public class OpenImageCmd : Command
    {
        public OpenImageCmd()
            : base("图片")
        {
            _icon = Resources.全屏显示_Big;
        }

        public OpenImageCmd(IMapView view)
            : base("图片", view)
        {
            _icon = Resources.全屏显示_Big;
        }

        /*
         * 全图工具执行时，更改地图的显示范围，并刷新地图
         */
        public override void Execute()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "jpg (*.jpg)|*.jpg";
            dialog.Multiselect = false;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                Image img = Image.FromFile(dialog.FileName);
                ImageBlockLayer layer = new ImageBlockLayer(img);
                if (View.MapDocument == null)
                {
                    MapDocument document = new MapDocument();
                    document.AddLayer(layer);
                    document.RecalExtent();
                    View.MapDocument = document;
                }
                else
                {
                    View.MapDocument.AddLayer(layer);
                    View.MapDocument.RecalExtent();
                }
                View.Invalidate();
            }
        }
    }
}
