﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using NetMap.Interfaces.Render;
using NetMap.UI.Properties;
using NetMap.Interfaces.Geometry;
using NetMap.Geometry;
using NetMap.Interfaces;

namespace NetMap.Tools
{
    /// <summary>
    /// 平移工具
    /// </summary>
    public class Pan : Tool
    {
        private int _x = 0, _y = 0;
        private bool _isDown = false;
        private IDisplayTransformation _disTrans = null;

        public Pan()
            : base("平移")
        {
        }

        public Pan(IMapEditView view)
            : base("平移", view)
        {          
        }

        protected override void SetIcon()
        {
            _icon = Resources.漫游_Big;
        }

        protected override void SetCursor()
        {
            _cursor = Cursors.Hand;
        }

        public override void GainFocus()
        {
            _disTrans = View.DisplayTransformation;
            _isDown = false;
            base.GainFocus();
        }

        public override void LostFocus()
        {
            _disTrans = null;
            _isDown = false;
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (_isDown) return;
                _x = e.X;
                _y = e.Y;
                _isDown = true;
            }
        }
        
        public override void OnMouseMove(MouseEventArgs e)
        {
            if (_isDown)
            {
                double xMapOff = (e.X - _x) / _disTrans.ScaleRatio;
                double yMapOff = (e.Y - _y) / _disTrans.ScaleRatio;
                IPoint center = _disTrans.VisibleMapBounds.Center;
                View.ZoomToCenter(center.X - xMapOff, center.Y + yMapOff, _disTrans.ScaleRatio);
                _x = e.X;
                _y = e.Y;
                View.Invalidate();
            }
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (!_isDown) return;
                _isDown = false;
                double xMapOff = (e.X - _x) / _disTrans.ScaleRatio;
                double yMapOff = (e.Y - _y) / _disTrans.ScaleRatio;
                IPoint center = _disTrans.VisibleMapBounds.Center;
                View.ZoomToCenter(center.X - xMapOff, center.Y + yMapOff, _disTrans.ScaleRatio);
                View.Invalidate();
            }
        }
    }
}
