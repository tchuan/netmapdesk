﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Data
{
    /// <summary>
    /// sql语句语法异常
    /// </summary>
    public class SqlSyntaxException : Exception
    {
        public SqlSyntaxException()
            : base()
        {
        }

        public SqlSyntaxException(string message)
            : base(message)
        {
        }

        public SqlSyntaxException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
