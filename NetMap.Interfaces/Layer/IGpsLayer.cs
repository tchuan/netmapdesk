﻿using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Symbol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces
{
    /// <summary>
    /// GPS图层
    /// </summary>
    public interface IGpsLayer : ILayer
    {
        /// <summary>
        /// 获取和设置gps轨迹表现的符号
        /// </summary>
        IStrokeSymbol Symbol { get; set; }
        /// <summary>
        /// 添加gps点
        /// </summary>
        /// <param name="point"></param>
        void AddGpsPoint(IDirectionPoint point);
        /// <summary>
        /// 清理gps点
        /// </summary>
        void ClearGpsPoint();
    }
}
