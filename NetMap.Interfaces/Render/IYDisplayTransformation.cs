﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Render
{
    /// <summary>
    /// Y轴变化的坐标转换接口
    /// </summary>
    public interface IYDisplayTransformation
    {
        /// <summary>
        /// 获取和设置Y轴比例
        /// </summary>
        double YZoom { get; set; }
        /// <summary>
        /// 获取和设置Y轴倾斜角度，以度为单位(-90, 90)
        /// </summary>
        double YAngle { get; set; }
    }
}
