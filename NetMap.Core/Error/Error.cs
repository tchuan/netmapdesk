﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*
 * 2013-7-1：统一的错误处理（只能获取最近发生并记录的错误信息）
 */

namespace NetMap.Error
{
    /// <summary>
    /// 错误记录
    /// </summary>
    public static class Error
    {
        #region 错误代码
        public const int GIS_OK = 0;           //正确
        public const int GIS_NOTFIND = 8101;      //未找到
        public const int GIS_WRONGINDEX = 8102;   //错误索引
        #endregion

        private static string _errMessage = string.Empty;
        private static int _errCode = GIS_OK;

        /// <summary>
        /// 记录错误
        /// </summary>
        /// <param name="errCode"></param>
        /// <param name="errMessage"></param>
        public static void Record(int errCode, string errMessage)
        {
            _errCode = errCode;
            _errMessage = errMessage;
        }

        /// <summary>
        /// 获取错误码
        /// </summary>
        public static int ErrCode
        {
            get { return _errCode; }
        }

        /// <summary>
        /// 获取错误详细信息
        /// </summary>
        public static string ErrMessage
        {
            get { return _errMessage; }
        }
    }
}
