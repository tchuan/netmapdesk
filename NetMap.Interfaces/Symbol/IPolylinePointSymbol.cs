﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;

namespace NetMap.Interfaces.Symbol
{
    /// <summary>
    /// 折线点符号接口
    /// </summary>
    public interface IPolylinePointSymbol : IPointSymbol
    {
        /// <summary>
        /// 获取和设置线宽
        /// </summary>
        float Width { get; set; }
        /// <summary>
        /// 获取和设置折线
        /// </summary>
        ILineString Polyline { get; set; }
    }
}
