﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetMap.UI.Ctrl
{
    /// <summary>
    /// 在工具栏上使用的符号字体选择符号工具
    /// </summary>
    public class ToolStripFontSelector : ToolStripControlHost
    {
        public ToolStripFontSelector()
            : base(new FontSelectCtrl())
        {
        }

        /// <summary>
        /// 获取FontSelectCtrl
        /// </summary>
        /// <returns></returns>
        public FontSelectCtrl GetFontSelector()
        {
            return Control as FontSelectCtrl;
        }
    }
}
