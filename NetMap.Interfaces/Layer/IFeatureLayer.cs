﻿using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Data;

namespace NetMap.Interfaces
{
    /// <summary>
    /// 要素图层接口
    /// </summary>
    public interface IFeatureLayer : ILayer
    {
        /// <summary>
        /// 获取和设置是否可编辑
        /// </summary>
        bool Editable { get; set; }
        /// <summary>
        /// 获取和设置是否可选择
        /// </summary>
        bool Selectable { get; set; }        
        /// <summary>
        /// 获取数据的几何类型
        /// </summary>
        geoGeometryType GeometryType { get; }
        /// <summary>
        /// 获取要素类型
        /// </summary>
        geoFeatureType FeatureType { get; }        
        /// <summary>
        /// 获取和设置数据集
        /// </summary>
        IFeatureClass FeatureClass { get; set; }
    }
}
