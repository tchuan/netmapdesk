﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NetMap.Interfaces.Data;

namespace NetMap.UI.Tools
{
    public partial class AttributeEditForm : Form
    {
        private IFeature _feature;

        public AttributeEditForm()
        {
            InitializeComponent();

            this.FormClosing += new FormClosingEventHandler(AttributeEditForm_FormClosing);
        }

        void AttributeEditForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (propEditorMain.IsPropModify)
            {
                if (MessageBox.Show("属性值已改变，是否保存？", "提示", MessageBoxButtons.YesNo)
                    == System.Windows.Forms.DialogResult.Yes)
                {
                    _feature.Store();
                }
            }
        }

        public void Init(IFeature feature)
        {
            _feature = feature;

            propEditorMain.Init(feature);
        }
    }
}
