﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Data
{
    /// <summary>
    /// 数据库工厂接口
    /// </summary>
    public interface IDatabaseFactory
    {
        /// <summary>
        /// 获取名字
        /// </summary>
        string Name
        {
            get;
        }
        /// <summary>
        /// 创建数据库
        /// </summary>
        /// <param name="pro"></param>
        /// <returns></returns>
        IDatabase Create(IConnectProperties pro);
        /// <summary>
        /// 打开数据库
        /// </summary>
        /// <param name="pro"></param>
        /// <returns></returns>
        IDatabase Open(IConnectProperties pro);
    }

    /// <summary>
    /// 空间数据库工厂接口
    /// </summary>
    public interface ISpatialDatabaseFactory
    {
        /// <summary>
        /// 获取名字
        /// </summary>
        string Name
        {
            get;
        }
        /// <summary>
        /// 创建数据库
        /// </summary>
        /// <param name="pro"></param>
        /// <returns></returns>
        ISpatialDatabase Create(IConnectProperties pro);
        /// <summary>
        /// 打开数据库
        /// </summary>
        /// <param name="pro"></param>
        /// <returns></returns>
        ISpatialDatabase Open(IConnectProperties pro);
    }
}
