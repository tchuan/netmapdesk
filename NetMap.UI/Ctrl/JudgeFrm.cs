﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.UI.Ctrl
{
    public class JudgeFrm:System.Windows.Forms.Form
    {
        public delegate void IsTrueHandle(bool Istrue);
        public event IsTrueHandle IsTrueEvent;
        private System.Windows.Forms.Button YesBt;
        private bool isTrue = false;

        private void InitializeComponent()
        {
            this.YesBt = new System.Windows.Forms.Button();
            this.NoBt = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // YesBt
            // 
            this.YesBt.Location = new System.Drawing.Point(37, 98);
            this.YesBt.Name = "YesBt";
            this.YesBt.Size = new System.Drawing.Size(67, 26);
            this.YesBt.TabIndex = 0;
            this.YesBt.Text = "Yes";
            this.YesBt.UseVisualStyleBackColor = true;
            this.YesBt.Click += new System.EventHandler(this.YesBt_Click);
            // 
            // NoBt
            // 
            this.NoBt.Location = new System.Drawing.Point(169, 98);
            this.NoBt.Name = "NoBt";
            this.NoBt.Size = new System.Drawing.Size(67, 26);
            this.NoBt.TabIndex = 0;
            this.NoBt.Text = "No";
            this.NoBt.UseVisualStyleBackColor = true;
            this.NoBt.Click += new System.EventHandler(this.NoBt_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(1, 1);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(271, 73);
            this.textBox1.TabIndex = 1;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // JudgeFrm
            // 
            this.ClientSize = new System.Drawing.Size(271, 134);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.NoBt);
            this.Controls.Add(this.YesBt);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "JudgeFrm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private System.Windows.Forms.Button NoBt;
        private System.Windows.Forms.TextBox textBox1;
        public JudgeFrm(string text)
        {
            InitializeComponent();
            textBox1.Text = text;
        }

        private void YesBt_Click(object sender, EventArgs e)
        {
            isTrue = true;
            if (IsTrueEvent != null) IsTrueEvent(isTrue);
            this.Close();
        }

        private void NoBt_Click(object sender, EventArgs e)
        {
            isTrue = false;
            if (IsTrueEvent != null) IsTrueEvent(isTrue);
            this.Close();
        }
    }
}
