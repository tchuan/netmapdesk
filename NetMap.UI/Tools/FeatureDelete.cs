﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Tools.Select;
using NetMap.Interfaces;
using System.Windows.Forms;
using NetMap.Interfaces.Data;
using NetMap.Data;

namespace NetMap.UI.Tools
{
    /// <summary>
    /// 删除工具
    /// 说明：先选择
    ///      选择后提示是否删除
    /// </summary>
    public class FeatureDelete : Tool
    {
        private CircleSelect _circleSelect;
        private IFeatureLayer _targetLayer;
        private string _targetLayerName;

        public FeatureDelete(string targetLayerName)
            : base("要素删除")
        {
            _targetLayerName = targetLayerName;
        }

        public FeatureDelete(IMapEditView view, string targetLayerName)
            : base("要素删除", view)
        {
            _targetLayerName = targetLayerName;
        }

        public override void Init()
        {
            base.Init();

            IMapDocument map = EditView.MapDocument;
            ILayer layer = map.FindLayer(_targetLayerName);
            if (layer is IFeatureLayer)
            {
                _targetLayer = layer as IFeatureLayer;
            }
            else
            {
                throw new NotSupportedException("要素删除工具不支持的图层类型");
            }

            _circleSelect = new CircleSelect(EditView);
            _circleSelect.Init();
            _circleSelect.TargetLayer = _targetLayer;
        }

        public override void GainFocus()
        {
            base.GainFocus();
            _circleSelect.GainFocus();
        }

        public override void LostFocus()
        {
            //base.LostFocus();
            _circleSelect.LostFocus();
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            //base.OnMouseDown(e);
            _circleSelect.OnMouseDown(e);
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            //base.OnMouseMove(e);
            _circleSelect.OnMouseMove(e);
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            //base.OnMouseUp(e);
            _circleSelect.OnMouseUp(e);
            if (_targetLayer is IFeatureSelection)
            {
                IFeatureSelection featureSelection = _targetLayer as IFeatureSelection;
                ISelectionSet selectionSet = featureSelection.SelectionSet;
                if (selectionSet.Count > 0)
                {
                    IQueryFilter filter = new QueryFilter();
                    IFields fields = selectionSet.GetTarget().Fields;
                    for (int j = 0; j < fields.Count; j++)
                    {
                        IField field = fields.GetField(j);
                        filter.Fields.Add(field.Name);
                    }
                    IFeatureCursor cursor = selectionSet.Search(filter);
                    IFeature feature = cursor.Next();
                    while (feature != null)
                    {
                        feature.Delete();
                        feature = cursor.Next();
                    }
                    cursor.Close();
                    View.Invalidate();
                }
            }
        }
    }
}
