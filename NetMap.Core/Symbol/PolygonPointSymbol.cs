﻿using NetMap.Geometry;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Symbol;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace NetMap.Symbol
{
    public class PolygonPointSymbol : PointSymbol, IPolygonPointSymbol
    {
        public PolygonPointSymbol(IPolygon polygon)
        {
            Width = 1;
            Outline = true;
            Fill = true;
            FillColor = Color.Black;
            Polygon = polygon;
        }

        public PolygonPointSymbol(PolygonPointSymbol symbol)
        {
            //TODO
        }

        #region ISymbol 成员

        public override ISymbol Clone()
        {
            return new PolygonPointSymbol(this);
        }

        public override string KindAbstract
        {
            get { return SymbolFactory.S_POINT_POLYGON; }
        }

        #endregion

        #region IPolygonPointSymbol 成员

        public float Width { get; set; }
        public bool Outline { get; set; }
        public bool Fill { get; set; }
        public Color FillColor { get; set; }
        public IPolygon Polygon { get; set; }

        #endregion

        /// <summary>
        /// 获取箭头的新实例（从目标向外）
        /// </summary>
        public static PolygonPointSymbol NewWellknownArrow
        {
            get
            {
                ILineString line = new LineString();
                line.Add(new NetMap.Geometry.Point(1, 0));
                line.Add(new NetMap.Geometry.Point(-0.7, 0.5));
                line.Add(new NetMap.Geometry.Point(0, 0));
                line.Add(new NetMap.Geometry.Point(-0.7, -0.5));
                IPolygon polygon = new Polygon(line);
                polygon.UpdateEnvelope();
                return new PolygonPointSymbol(polygon);
            }
        }

        /// <summary>
        /// 获取箭头的新实例（指向目标）
        /// </summary>
        public static PolygonPointSymbol NewWellknownArrowTarget
        {
            get
            {
                ILineString line = new LineString();
                line.Add(new NetMap.Geometry.Point(0, 0));
                line.Add(new NetMap.Geometry.Point(-1.7, 0.5));
                line.Add(new NetMap.Geometry.Point(-1, 0));
                line.Add(new NetMap.Geometry.Point(-1.7, -0.5));
                IPolygon polygon = new Polygon(line);
                polygon.UpdateEnvelope();
                return new PolygonPointSymbol(polygon);
            }
        }
    }
}
