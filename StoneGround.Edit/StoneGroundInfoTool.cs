﻿using System;
using NetMap;
using System.Windows.Forms;
using NetMap.Interfaces;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Geometry;
using NetMap.Geometry;
using NetMap.Data;
using NetMap.Interfaces.Data;
using StoneGround.Dialog;

namespace StoneGround.Edit
{
    /// <summary>
    /// 查询料场信息
    /// </summary>
    public class StoneGroundInfoTool : Tool
    {
        //private IFeature _arrowFeature; //指向的目标
        //private ISymbol _arrowFeatureSymbol; 

        public StoneGroundInfoTool()
            : base("查询料场信息")
        {
        }

        public StoneGroundInfoTool(IMapEditView view)
            : base("查询料场信息", view)
        {
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            //base.OnMouseDown(e);
            if (e.Button != MouseButtons.Left)
                return;
            const int tolerance = 10;
            double x = 0, y = 0;
            IDisplayTransformation displayTranformation = View.DisplayTransformation;
            displayTranformation.ToMapPoint(e.X, e.Y, ref x, ref y);

            IFeatureClass featureClass = StoneGroundDatabaseService.Instance.StoneGroundFeatureClass;
            ISpatialFilter filter = new SpatialFilter();
            filter.Geometry = new Circle(x, y, tolerance / displayTranformation.ScaleRatio);
            filter.SpatialRelationship = geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_INTERSECTS;
            IFields fields = featureClass.Fields;
            for (int i = 0; i < fields.Count; i++)
            {
                IField field = fields.GetField(i);
                filter.Fields.Add(field.Name);
            }

            IFeatureCursor cursor = featureClass.Search(filter);
            IFeature feature = cursor.Next();
            cursor.Close();
            if (feature != null)
            {
                FrmSGInfo2 dialog = new FrmSGInfo2();
                dialog.StartPosition = FormStartPosition.CenterParent;
                dialog.InitInfo(feature);
                dialog.ShowDialog(NetMap.Application.Gui.WorkbenchSingleton.MainWin32Window);
            }
        }
    }
}
