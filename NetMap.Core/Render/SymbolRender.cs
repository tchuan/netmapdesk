﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Symbol;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace NetMap.Render
{
    /// <summary>
    /// Winform符号绘制
    /// </summary>
    public class SymbolRender : ISymbolRender
    {
        private IDisplay _display;

        #region ISymbolRender 成员

        public void Setup(IDisplay display)
        {
            _display = display;
        }

        public void Draw(ISymbol symbol, IGeometry geometry)
        {
            ISymbolDrawer drawer = SymbolDrawerFactory.Instance.GetDrawer(symbol);
            if (drawer != null)
            {
                drawer.Draw(_display, symbol, geometry, null);
            }
        }

        public void Draw(IEnvelope bounds, Image image)
        {
            IDisplayTransformation trans = _display.DisplayTransformation;
            double x = 0, y = 0, x1 = 0, y1 = 0;
            trans.FromMapPoint(bounds.XMin, bounds.YMin, ref x, ref y);
            trans.FromMapPoint(bounds.XMax, bounds.YMax, ref x1, ref y1);
            //图片整体向左偏移取整
            int left = (int)Math.Round(Math.Min(x, x1),0, MidpointRounding.AwayFromZero);
            int bottom = (int)Math.Round(Math.Min(y, y1),0, MidpointRounding.AwayFromZero);
            int right = (int)Math.Round(Math.Max(x, x1),0, MidpointRounding.AwayFromZero);
            int top = (int)Math.Round(Math.Max(y, y1),0, MidpointRounding.AwayFromZero);
            Rectangle rect = new Rectangle(left, bottom, right - left, top - bottom);
            ImageAttributes imageAttributes = new ImageAttributes();
            imageAttributes.SetWrapMode(WrapMode.TileFlipXY);
            _display.G.DrawImage(image, rect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, imageAttributes);
        }

        public void DrawText(ITextSymbol symbol, IGeometry geometry, string text)
        {
            ISymbolDrawer drawer = SymbolDrawerFactory.Instance.GetDrawer(symbol);
            if (drawer != null)
            {
                drawer.Draw(_display, symbol, geometry, text);
            }
        }

        public void DirectDraw(IDisplay display, ISymbol symbol, IGeometry geometry)
        {
            throw new NotImplementedException();
        }

        public void DirectDraw(IDisplay display, IEnvelope bounds, Bitmap image)
        {
            throw new NotImplementedException();
        }

        public void DirectDraw(IDisplay display, ITextSymbol symbol, IGeometry geometry, string text)
        {
            ISymbolDrawer drawer = SymbolDrawerFactory.Instance.GetDrawer(symbol);
            if (drawer != null)
            {
                drawer.Draw(display.G, symbol, geometry, text);
            }
        }

        public Interfaces.Geometry.IEnvelope QueryBounds(ISymbol symbol, IGeometry geometry)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
