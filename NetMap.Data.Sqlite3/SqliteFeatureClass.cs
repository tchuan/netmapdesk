﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Data;
using NetMap.Interfaces.Geometry;
using Community.CsharpSqlite;
using NetMap.Data;
using NetMap.SpatialAnalysis;
using NetMap.Interfaces.SpatialAnalysis;
using NetMap.Geometry;
using ProjNet.CoordinateSystems;
using ProjNet.Converters.WellKnownText;

namespace NetMap.Sqlite
{
    /// <summary>
    /// Sqlite数据库要素集
    /// </summary>
    internal class SqliteFeatureClass : SqliteTable, IFeatureClass
    {
        protected geoFeatureType _fType = geoFeatureType.GEO_FEATURE_SIMPLE;
        protected geoGeometryType _gType = geoGeometryType.GEO_GEOMETRY_POINT;
        protected IEnvelope _extent = null;
        protected ICoordinateSystem _cs = null;
        protected SqliteSpatialDatabase _sdb = null;
        protected IRow _metaRow = null; //元数据记录

        internal SqliteFeatureClass()
        {
        }

        #region IFeatureClass 成员

        public ISpatialDatabase SpatialDatabase
        {
            get { return _sdb; }
        }

        public IFeature CreateFeature()
        {
            switch (_fType)
            {
                case geoFeatureType.GEO_FEATURE_SIMPLE:
                    {
                        SqliteFeature feature = new SqliteFeature();
                        feature.InitFeatureFields(this, _fields);
                        return feature;
                    }
                case geoFeatureType.GEO_FEATURE_ANNOTATION:
                    {
                        SqliteTextFeature feature = new SqliteTextFeature();
                        feature.InitFeatureFields(this, _fields);
                        return feature;
                    }
                case geoFeatureType.GEO_FEATURE_RASTER:
                    {
                        SqliteRasterFeature feature = new SqliteRasterFeature();
                        feature.InitFeatureFields(this, _fields);
                        return feature;
                    }
            }
            return null;
        }

        public IFeature GetFeature(long oid)
        {
            switch (_fType)
            {
                case geoFeatureType.GEO_FEATURE_SIMPLE:
                    {
                        SqliteFeature feature = new SqliteFeature();
                        feature.InitFields(this, _fields);
                        feature.OID = oid;
                        Sqlite3.Vdbe stmt = null;
                        int err = getRows(get_sub_fields(), ref stmt, "");
                        if (err != Sqlite3.SQLITE_OK)
                        {
                            throw new Exception("获取要素失败");
                        }
                        return feature;                        
                    }
                case geoFeatureType.GEO_FEATURE_ANNOTATION:
                    {
                        SqliteTextFeature feature = new SqliteTextFeature();
                        feature.InitFields(this, _fields);
                        feature.OID = oid;
                        Sqlite3.Vdbe stmt = null;
                        int err = getRows(get_sub_fields(), ref stmt, "");
                        if (err != Sqlite3.SQLITE_OK)
                        {
                            throw new Exception("获取要素失败");
                        }
                        return feature;
                    }
                case geoFeatureType.GEO_FEATURE_RASTER:
                    {
                        SqliteRasterFeature feature = new SqliteRasterFeature();
                        feature.InitFields(this, _fields);
                        feature.OID = oid;
                        Sqlite3.Vdbe stmt = null;
                        int err = getRows(get_sub_fields(), ref stmt, "");
                        if (err != Sqlite3.SQLITE_OK)
                        {
                            throw new Exception("获取要素失败");
                        }
                        return feature;
                    }
            }
            return null;
        }

        public void CreateSpatialIndex()
        {
            throw new NotImplementedException();
        }

        IFeatureCursor IFeatureClass.Search(IQueryFilter filter)
        {
            return search(filter, null);
        }

        public ISelectionSet Select(IQueryFilter filter)
        {
            return select(filter, null);
        }

        public geoGeometryType GeometryType
        {
            get { return _gType; }
        }

        public geoFeatureType FeatureType
        {
            get { return _fType; }
        }

        public IEnvelope Extent
        {
            get { return _extent; }
        }

        public ICoordinateSystem CoordinateSystem
        {
            get { return _cs; }
        }

        public void ClearFeature()
        {
            string sql = "delete from " + _name;
            _sdb.ExecuteSQL(sql);
        }

        #endregion

        //创建要素集
        internal virtual int create(SqliteSpatialDatabase sdb, string name, IFields fields,
            geoFeatureType fType, geoGeometryType gType, IEnvelope extent, ICoordinateSystem coordinateSystem)
        {
            _sdb = sdb;
            _db = sdb._db;            
            _name = name;
            _fields = fields;
            _fType = fType;
            _gType = gType;
            _extent = extent;
            _cs = coordinateSystem;
            //创建空间表
            int err = create(_db, name, fields);
            if (err != Sqlite3.SQLITE_OK)
            {
                return err;
            }
            //写入元数据
            _metaRow = _sdb._metaTable.CreateRow();
            _metaRow.SetValue(0, name);
            _metaRow.SetValue(1, (short)fType);
            _metaRow.SetValue(2, (short)gType);
            IGeometry geometry = extent as IGeometry;
            _metaRow.SetValue(3, geometry.ExportToWkb());
            if (_cs != null)
            {
                _metaRow.SetValue(4, _cs.ToString());
            }
            else
            {
                _metaRow.SetValue(4, "");
            }
            _metaRow.Store();
            return Sqlite3.SQLITE_OK;
        }

        internal virtual int open(SqliteSpatialDatabase sdb, string name)
        {
            _sdb = sdb;
            _db = sdb._db;
            _name = name;

            //获取元数据表中对应的记录
            IQueryFilter filter = new QueryFilter();
            filter.Fields.Add(SpatialDbDefine.FIELD_TABLENAME);
            filter.Fields.Add(SpatialDbDefine.FIELD_FEATURETYPE);
            filter.Fields.Add(SpatialDbDefine.FIELD_GEOMETRYTYPE);
            filter.Fields.Add(SpatialDbDefine.FIELD_ENVELOPE);
            filter.Fields.Add(SpatialDbDefine.FIELD_COORDINATESSYSTEM);
            filter.WhereClause = SpatialDbDefine.FIELD_TABLENAME + "=\'" + name + "\'";

            ICursor cursor = _sdb._metaTable.Search(filter);
            _metaRow = cursor.Next();
            if (_metaRow == null)
            {
                return SpatialDbDefine.GIS_DBERR_NOTFIND;
            }
            else
            {
                _fType = (geoFeatureType)(short)_metaRow.GetValue(1);
                _gType = (geoGeometryType)(short)_metaRow.GetValue(2);
                _extent = new Envelope();
                IGeometry geoExtent = _extent as IGeometry;
                geoExtent.ImportFromWkb((byte[])_metaRow.GetValue(3));
                string coordinateSystemWkt = (string)_metaRow.GetValue(4);
                if (string.IsNullOrEmpty(coordinateSystemWkt))
                {
                    _cs = null;
                }
                else
                {
                    _cs = CoordinateSystemWktReader.Parse(coordinateSystemWkt) as ICoordinateSystem;
                }
            }

            //获取属性字段
            get_table_field();

            return Sqlite3.SQLITE_OK;
        }

        internal IFeatureCursor search(IQueryFilter filter, ISelectionSet set)
        {
            IFields fields = new Fields();
            int err = check_filter(filter, fields);
            if (err != Sqlite3.SQLITE_OK)
            {
                throw new ApplicationException("查询失败");
            }

            string sql = get_search_sql(filter);
            Sqlite3.Vdbe stmt = null;
            string tail = "";
            err = Sqlite3.sqlite3_prepare_v2(_db, sql, sql.Length, ref stmt, ref tail);
            if (err != Sqlite3.SQLITE_OK)
            {
                throw new Exception(tail);
            }
            SqliteFeatureCursor cursor = new SqliteFeatureCursor();
            ISpatialAnalysis SA = null;
            if (filter is ISpatialFilter)
            {
                SA = SpatialAnalysisFactory.Create(filter as ISpatialFilter);
            }
            cursor.open(this, fields, stmt, SA, set);
            return cursor;
        }

        internal ISelectionSet select(IQueryFilter filter, ISelectionSet set)
        {
            IFeatureCursor cursor = search(filter, set);
            SqliteSelectionSet resultSet = new SqliteSelectionSet();
            resultSet.open(this, cursor);
            return resultSet;
        }

        //创建空间表的创建sql
        protected override string build_create_sql(string name, IFields fields)
        {
            //备注：在属性表的基础上，新增表现要素的字段。
            StringBuilder builder = new StringBuilder();
            builder.Append("create table ");
            builder.Append(name);
            builder.Append("(");
            builder.Append(SpatialDbDefine.FIELD_OID);
            builder.Append(" integer primary key not null,");
            builder.Append(SpatialDbDefine.FIELD_GEOMETRY).Append(" blob not null");
            if (_gType != geoGeometryType.GEO_GEOMETRY_POINT
                && _gType != geoGeometryType.GEO_GEOMETRY_RECT) //Point和Rect图形不存储空间范围
            {
                builder.Append(",").Append(SpatialDbDefine.FIELD_ENVELOPE).Append(" blob not null");
            }
            if (_fType == geoFeatureType.GEO_FEATURE_ANNOTATION)
            {
                builder.Append(",").Append(SpatialDbDefine.FIELD_TEXT).Append(" text");
            }
            else if (_fType == geoFeatureType.GEO_FEATURE_RASTER)
            {
                builder.Append(",").Append(SpatialDbDefine.FIELD_RASTER).Append(" blob");
            }
            int count = fields.Count;
            IField field = null;
            for (int i = 0; i < count; i++)
            {
                field = fields.GetField(i);
                builder.Append(",[").Append(field.Name).Append("]");
                add_one_field(field, builder);
            }
            builder.Append(")");
            return builder.ToString();
        }

        //根据所有字段的查询格式
        protected override string get_sub_fields()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(SpatialDbDefine.FIELD_OID);
            builder.Append(",").Append(SpatialDbDefine.FIELD_GEOMETRY);
            if (_gType != geoGeometryType.GEO_GEOMETRY_POINT
                && _gType != geoGeometryType.GEO_GEOMETRY_RECT 
                && _gType != geoGeometryType.GEO_GEOMETRY_DIRECTIONPOINT) //点、框、有向点不用存储范围
            {
                builder.Append(",").Append(SpatialDbDefine.FIELD_ENVELOPE);
            }
            if (_fType == geoFeatureType.GEO_FEATURE_ANNOTATION)
            {
                builder.Append(",").Append(SpatialDbDefine.FIELD_TEXT);
            }
            else if (_fType == geoFeatureType.GEO_FEATURE_RASTER)
            {
                builder.Append(",").Append(SpatialDbDefine.FIELD_RASTER);
            }
            int count = _fields.Count;
            IField field = null;
            for (int i = 0; i < count; i++)
            {
                field = _fields.GetField(i);
                builder.Append(",").Append(field.Name);
            }
            return builder.ToString();
        }

        //获取属性字段
        private void get_table_field()
        {
            string sql = "PRAGMA table_info(" + _name + ")";
            Sqlite3.Vdbe stmt = null;
            int err = Sqlite3.sqlite3_prepare_v2(_db, sql, sql.Length, ref stmt, 0);
            if (err != Sqlite3.SQLITE_OK)
            {
                //TODO 记录错误
                return;
            }
            err = Sqlite3.sqlite3_step(stmt);
            int i = 0;
            _fields = new Fields();
            while (err == Sqlite3.SQLITE_ROW)
            {
                string name = Sqlite3.sqlite3_column_text(stmt, 1);
                if (name == SpatialDbDefine.FIELD_OID || name == SpatialDbDefine.FIELD_GEOMETRY
                    || name == SpatialDbDefine.FIELD_TEXT || name == SpatialDbDefine.FIELD_RASTER
                    || name == SpatialDbDefine.FIELD_ENVELOPE)
                {
                    err = Sqlite3.sqlite3_step(stmt);
                    continue;
                }
                IField field = new Field();
                field.Name = name;
                switch (Sqlite3.sqlite3_column_text(stmt, 2))
                {
                    case "short":
                        {
                            field.Type = geoFieldType.GEO_SMALLINT;
                            break;
                        }
                    case "int":
                        {
                            field.Type = geoFieldType.GEO_INT;
                            break;
                        }
                    case "float":
                        {
                            field.Type = geoFieldType.GEO_SINGLE;
                            break;
                        }
                    case "double":
                        {
                            field.Type = geoFieldType.GEO_DOUBLE;
                            break;
                        }
                    case "text":
                        {
                            field.Type = geoFieldType.GEO_STRING;
                            break;
                        }
                    case "datetime":
                        {
                            field.Type = geoFieldType.GEO_DATE;
                            break;
                        }
                    case "blob":
                        {
                            field.Type = geoFieldType.GEO_BLOB;
                            break;
                        }
                }
                _fields.SetField(i++, field);
                err = Sqlite3.sqlite3_step(stmt);
            }
            Sqlite3.sqlite3_finalize(stmt);            
        }        

        //构建查询sql
        protected override string get_search_sql(IQueryFilter filter)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("select ");
            #region 属性字段
            int count = filter.Fields.Count;
            bool first = true;
            for (int i = 0; i < count; i++)
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    builder.Append(",");
                }
                builder.Append(filter.Fields[i]);
            }
            #endregion
            #region OID
            if (first)
            {
                first = false;
            }
            else
            {
                builder.Append(",");
            }
            builder.Append(SpatialDbDefine.FIELD_OID);
            #endregion
            #region 特殊字段
            if (first)
            {
                first = false;
            }
            else
            {
                builder.Append(",");
            }
            builder.Append(SpatialDbDefine.FIELD_GEOMETRY);
            if (_gType != geoGeometryType.GEO_GEOMETRY_POINT
                && _gType != geoGeometryType.GEO_GEOMETRY_RECT
                && _gType != geoGeometryType.GEO_GEOMETRY_DIRECTIONPOINT)
            {
                builder.Append(",").Append(SpatialDbDefine.FIELD_ENVELOPE);
            }
            switch (_fType)
            {
                case geoFeatureType.GEO_FEATURE_ANNOTATION:
                    {
                        builder.Append(",").Append(SpatialDbDefine.FIELD_TEXT);
                        break;
                    }
                case geoFeatureType.GEO_FEATURE_RASTER:
                    {
                        builder.Append(",").Append(SpatialDbDefine.FIELD_RASTER);
                        break;
                    }
            }
            #endregion
            builder.Append(" from ").Append(_name);
            if (!string.IsNullOrEmpty(filter.WhereClause))
            {
                builder.Append(" where ").Append(filter.WhereClause);
            }

            return builder.ToString();
        }

        private string get_select_sql(IQueryFilter filter, ISpatialAnalysis SA)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("select ").Append(SpatialDbDefine.FIELD_OID);
            if (SA != null)
            {
                builder.Append(",").Append(SpatialDbDefine.FIELD_GEOMETRY);
                if (_gType != geoGeometryType.GEO_GEOMETRY_POINT
                && _gType != geoGeometryType.GEO_GEOMETRY_RECT
                && _gType != geoGeometryType.GEO_GEOMETRY_DIRECTIONPOINT)
                {
                    builder.Append(",").Append(SpatialDbDefine.FIELD_ENVELOPE);
                }
            }
            builder.Append(" from ").Append(_name);
            if (!string.IsNullOrEmpty(filter.WhereClause))
            {
                builder.Append(" where ").Append(filter.WhereClause);
            }
            return builder.ToString();
        }
    }
}
