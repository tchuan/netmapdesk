﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Format
{
    /// <summary>
    /// 字符串格式化接口
    /// </summary>
    public interface IStringFormat
    {
        /// <summary>
        /// 导出成字符串
        /// </summary>
        /// <returns></returns>
        string ToWkt();
        /// <summary>
        /// 从字符串导入
        /// </summary>
        void FromWkt();
    }
}
