﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace NetMap.UI.Skin
{
    /// <summary>
    /// 菜单栏渲染
    /// </summary>
    public class MenuStripRender : ToolStripProfessionalRenderer
    {
        private MenuStripSkinConfig _skinCfg = new MenuStripSkinConfig();

        // 渲染菜单栏背景
        protected override void OnRenderToolStripBackground(ToolStripRenderEventArgs e)
        {
            e.ToolStrip.ForeColor = _skinCfg.FontColor;
            if (e.ToolStrip is ToolStripDropDown) // 下拉菜单
            { 
                using(Brush brush = new SolidBrush(_skinCfg.DropDownItemBackColor))
                {
                    e.Graphics.FillRectangle(brush, e.AffectedBounds);
                }
            }
            else if (e.ToolStrip is MenuStrip)    // 菜单项
            {
                Blend blend = new Blend();
                float[] fs = new float[5] { 0f, 0.3f, 0.5f, 0.8f, 1f };
                float[] f = new float[5] { 0f, 0.5f, 0.9f, 0.5f, 0f };
                blend.Positions = fs;
                blend.Factors = f;
                FillLineGradient(e.Graphics, e.AffectedBounds, _skinCfg.MainMenuStartColor,
                    _skinCfg.MainMenuEndColor, 90f, blend);
            }
            else
            {
                base.OnRenderToolStripBackground(e);
            }
        }

        // 渲染下拉左侧图标区域
        protected override void OnRenderImageMargin(ToolStripRenderEventArgs e)
        {
            FillLineGradient(e.Graphics, e.AffectedBounds, _skinCfg.MarginStartColor,
                _skinCfg.MarginEndColor, 0f, null);
        }

        // 渲染菜单项的背景
        protected override void OnRenderMenuItemBackground(ToolStripItemRenderEventArgs e)
        {
            if (e.ToolStrip is MenuStrip) // 菜单项
            {
                if (e.Item.Selected || e.Item.Pressed) // 选中或按下
                {
                    Blend blend = new Blend();
                    float[] fs = new float[5] { 0f, 0.3f, 0.5f, 0.8f, 1f };
                    float[] f = new float[5] { 0f, 0.5f, 1f, 0.5f, 0f };
                    blend.Positions = fs;
                    blend.Factors = f;
                    FillLineGradient(e.Graphics, new Rectangle(0, 0, e.Item.Size.Width,
                        e.Item.Size.Height), _skinCfg.MenuItemStartColor, _skinCfg.MenuItemEndColor, 90, blend);
                }
                else
                {
                    base.OnRenderMenuItemBackground(e);
                }
            }
            else if (e.ToolStrip is ToolStripDropDown)
            {
                if (e.Item.Selected)
                {
                    FillLineGradient(e.Graphics, new Rectangle(0, 0, e.Item.Size.Width, e.Item.Size.Height),
                        _skinCfg.DropDownItemStartColor, _skinCfg.DropDownItemEndColor, 90f, null);
                }
            }
            else
            {
                base.OnRenderMenuItemBackground(e);
            }
        }

        // 渲染菜单项的分隔线
        protected override void OnRenderSeparator(ToolStripSeparatorRenderEventArgs e)
        {
            using(Pen pen = new Pen(_skinCfg.SeparatorColor))
            {
                e.Graphics.DrawLine(pen, 0, 2, e.Item.Width, 2);
            }
        }

        // 渲染边框
        protected override void OnRenderToolStripBorder(ToolStripRenderEventArgs e)
        {
            if (e.ToolStrip is ToolStripDropDown)
            {
                using (Pen pen = new Pen(_skinCfg.DropDownBorderColor))
                {
                    e.Graphics.DrawRectangle(pen, new Rectangle(0, 0, e.AffectedBounds.Width - 1, e.AffectedBounds.Height - 1));
                }
            }
            else
            {
                base.OnRenderToolStripBorder(e);
            }            
        }

        // 渲染箭头
        protected override void OnRenderArrow(ToolStripArrowRenderEventArgs e)
        {
            e.ArrowColor = Color.Red; // 设置为红色箭头
            base.OnRenderArrow(e);
        }

        private void FillLineGradient(Graphics g, Rectangle rect, Color startColor,
            Color endColor, float angle, Blend blend)
        {
            using (LinearGradientBrush brush = new LinearGradientBrush(rect, startColor, endColor, angle))
            {
                if (blend != null)
                {
                    brush.Blend = blend;
                }
                GraphicsPath path = new GraphicsPath();
                path.AddRectangle(rect);
                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.FillPath(brush, path);
            }
        }
    }
}
