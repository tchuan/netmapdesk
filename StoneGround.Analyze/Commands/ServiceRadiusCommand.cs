﻿using System.Collections.Generic;
using NetMap.Application.Gui.Pads;
using NetMap;
using NetMap.Interfaces;
using System.Windows.Forms;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Data;
using NetMap.Data;
using NetMap.Interfaces.Geometry;
using StoneGround.Search;
using NetMap.Application.Core;
using StoneGround.Analyze.Dialog;

namespace StoneGround.Analyze.Commands
{
    public class ServiceRadiusTool : Tool
    {
        public ServiceRadiusTool()
            : base("服务半径")
        {
        }

        public ServiceRadiusTool(IMapEditView editView)
            : base("服务半径", editView)
        {
        }

        public override void GainFocus()
        {
            //base.GainFocus();

            if (_view.Status != null)
            {
                _view.Status.ShowInfo("服务半径：鼠标左键选择点位");
            }
        }

        public override void LostFocus()
        {
            //base.LostFocus();
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                double x = 0, y = 0;
                IDisplayTransformation trans = View.DisplayTransformation;
                trans.ToMapPoint(e.X, e.Y, ref x, ref y);
                Search(x, y);
            }
        }

        private void Search(double x, double y)
        {
            IFeatureClass featureClass = StoneGroundDatabaseService.Instance.StoneGroundFeatureClass;
            IQueryFilter filter = new QueryFilter();
            IFields fields = featureClass.Fields;
            for (int i = 0; i < fields.Count; i++)
            {
                IField field = fields.GetField(i);
                filter.Fields.Add(field.Name);
            }
            List<IFeature> features = new List<IFeature>();
            IFeatureCursor cursor = featureClass.Search(filter);
            IFeature feature = cursor.Next();
            while (feature != null)
            {
                IPoint position = feature.Geometry as IPoint;
                double dis = DistanceCalculator.GetDistance(y, x, position.Y, position.X) * 1000; //以m为单位
                if (dis <= StoneGroundAnalyzeConfig.ServiceRadius)
                {
                    features.Add(feature);
                }
                feature = cursor.Next();
            }
            cursor.Close();

            if (features.Count == 0)
            {
                SearchPad.Instance.ClearSearchTreeNodes();
                MessageService.ShowMessage("此服务半径内没有料场", "提示");
            }
            else
            {
                SearchPad.Instance.BuildSearchTreeNodes(features);
            }
        }
    }

    /// <summary>
    /// 服务半径分析
    /// </summary>
    public class ServiceRadiusCommand : MapCommand
    {
        public ServiceRadiusCommand()
        {
            var dialog = new RadiusInput();
            dialog.ShowDialog();
            StoneGroundAnalyzeConfig.ServiceRadius = dialog.Radius * 1000;

            InnerCmd = new ServiceRadiusTool();
        }
    }
}
