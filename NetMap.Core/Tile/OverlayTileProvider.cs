﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Tile;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

namespace NetMap.Tile.Wellknown
{
    /// <summary>
    /// 叠加地图瓦片Provider
    /// </summary>
    public class OverlayTileProvider : ITileProvider
    {
        private List<ITileProvider> _tileProviders = new List<ITileProvider>();
        private ImageFormat _format = ImageFormat.Png;
        private int _height = 256;
        private int _width = 256;
        public delegate Color GetBackColorFunction(string level);
        private GetBackColorFunction _back;

        public OverlayTileProvider(ImageFormat format, GetBackColorFunction back,
            int width = 256, int height = 256)
        {
            _format = format;
            _width = width;
            _height = height;
            _back = back;
        }

        /// <summary>
        /// 添加瓦片项
        /// </summary>
        /// <param name="provider"></param>
        public void AddProviderItem(ITileProvider provider)
        {
            _tileProviders.Add(provider);
        }

        /// <summary>
        /// 清空瓦片项
        /// </summary>
        public void ClearProviders()
        {
            _tileProviders.Clear();
        }

        /// <summary>
        /// 获取瓦片
        /// </summary>
        /// <param name="tileInfo"></param>
        /// <returns></returns>
        public byte[] GetTile(TileInfo tileInfo)
        {
            List<byte[]> layers = new List<byte[]>();
            foreach (ITileProvider provider in _tileProviders)
            {
                byte[] data = provider.GetTile(tileInfo);
                if (data != null)
                {
                    layers.Add(data);                    
                }
            }
            if (layers.Count == 0)
            {
                return null;
            }
            using (Bitmap bitmap = new Bitmap(_width, _height))
            {
                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    Color color = _back(tileInfo.Index.Level);
                    g.Clear(color);
                    foreach (byte[] layer in layers)
                    {
                        using (MemoryStream stream = new MemoryStream(layer))
                        {
                            using (Image img = Image.FromStream(stream))
                            {
                                g.DrawImage(img, 0, 0);
                            }
                        }                        
                    }
                    return ConvertImage(bitmap);
                }
            }
        }

        /// <summary>
        /// 将Image转换为byte[]
        /// </summary>
        /// <param name="image">Image</param>
        /// <returns>byte[]</returns>
        private byte[] ConvertImage(Image image)
        {
            MemoryStream ms = new MemoryStream();
            image.Save(ms, _format);
            ms.Close();
            byte[] bytes = ms.ToArray();
            ms.Dispose();
            return bytes;
        }
    }
}
