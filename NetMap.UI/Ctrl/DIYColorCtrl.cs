﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetMap.UI.Ctrl
{
    public partial class DIYColorCtrl : UserControl
    {
        private int r, g, b;
        private Color currentColor;
        public DIYColorCtrl()
        {
            InitializeComponent();
        }
        public Color SetCurrentColor
        {
            set 
            {
                currentColor = value;
                r = value.R; g = value.G; b = value.B;
                RtextBox.Text = r.ToString(); GtextBox.Text = g.ToString(); BtextBox.Text = b.ToString();
                GtrackBar.Value = g; BtrackBar.Value = b; RtrackBar.Value = r;
            }
        }
        public Color GetCurrentColor { get { return currentColor; } }
        private void RtrackBar_ValueChanged(object sender, EventArgs e)
        {
            r = RtrackBar.Value;
            RtextBox.Text = r.ToString();
            currentColor = Color.FromArgb(r,g,b);
            ColorBT.BackColor = currentColor;
        }

        private void GtrackBar_ValueChanged(object sender, EventArgs e)
        {
            g = GtrackBar.Value;
            GtextBox.Text = g.ToString();
            currentColor = Color.FromArgb(r,g,b);
            ColorBT.BackColor = currentColor;
        }

        private void BtrackBar_ValueChanged(object sender, EventArgs e)
        {
            b = BtrackBar.Value;
            BtextBox.Text = b.ToString();
            currentColor = Color.FromArgb(r, g, b);
            ColorBT.BackColor = currentColor;
        }

        private void RtextBox_TextChanged(object sender, EventArgs e)
        {
            if (int.TryParse(RtextBox.Text, out r))
            {
                if (r >= 0 && r <= 255)
                {
                    RtrackBar.Value = r;
                    currentColor = Color.FromArgb(r, g, b);
                    ColorBT.BackColor = currentColor;
                }
            }
        }

        private void GtextBox_TextChanged(object sender, EventArgs e)
        {
            if (int.TryParse(GtextBox.Text, out g))
            {
                if (g >= 0 && g <= 255)
                {
                    GtrackBar.Value = g;
                    currentColor = Color.FromArgb(r, g, b);
                    ColorBT.BackColor = currentColor;
                }
            }
        }

        private void BtextBox_TextChanged(object sender, EventArgs e)
        {
            if (int.TryParse(BtextBox.Text, out b))
            {
                BtrackBar.Value = b;
                currentColor = Color.FromArgb(r, g, b);
                ColorBT.BackColor = currentColor;
            }
        }

        private void RtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 48 || e.KeyChar > 57) && (e.KeyChar != 8 && e.KeyChar != 13))
            { e.Handled = true; }
        }

        private void GtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 48 || e.KeyChar > 57) && (e.KeyChar != 8 && e.KeyChar != 13))
            { e.Handled = true; }
        }

        private void BtextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 48 || e.KeyChar > 57) && (e.KeyChar != 8 && e.KeyChar != 13))
            { e.Handled = true; }
            if (int.Parse((sender as TextBox).Text) > 255)
            { e.Handled = false; }
        }

    }
}
