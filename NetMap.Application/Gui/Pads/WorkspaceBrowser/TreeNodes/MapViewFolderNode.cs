﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Application.WorkspaceBrowser.TreeNodes
{
    public class MapViewFolderNode : AbstractWorkspaceBrowserTreeNode
    {
        MapViewData _mapViewData;

        public MapViewData MapViewData
        {
            get { return _mapViewData; }
        }

        public MapViewFolderNode(MapViewData mapViewData)
        {
            ContextmenuAddinTreePath = "/Application/Pads/WorkspaceBrowser/ContextMenu/MapViewFolderNode";
        }

        public override object AcceptVisitor(WorkspaceBrowserTreeNodeVisitor visitor, object data)
        {
            throw new NotImplementedException();
        }
    }
}
