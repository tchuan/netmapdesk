﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Common;
using NetMap.Interfaces.Geometry;

namespace NetMap.Geometry.Algorithms
{
    /*
     * 点乘，也叫向量的内积、数量积。顾名思义，求下来的结果是一个数。
     * 向量a·向量b=|a||b|cos<a,b>
     * (a,b)·(c,d)=ac+bd;
     * 注意：在程式的注释中也用dot表示点乘。
     * 
     * 叉乘，也叫向量的外积、向量积。顾名思义，求下来的结果是一个向量，记这个向量为c。
     * |向量c|=|向量a×向量b|=|a||b|sin<a,b>
     * (u1,u2,u3)×(v1,v2,v3)={u2v3-v2u3, u3v1-v3u1, u1v2-u2v1}
     * u3=1,v3=1时{u2-v2,v1-u1,u1v2-u2v1}
     * 向量c的方向与a,b所在的平面垂直，且方向要用“右手法则”判断
     */
    public class Algorithms
    {
        /// <summary>
        /// 两点间的距离
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        /// <returns></returns>
        public static double PointToPointDistance(double x1, double y1, double x2, double y2)
        {
            return Math.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
        }

        /// <summary>
        /// 两点间的距离
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        public static double PointToPointDistance(IPoint p1, IPoint p2)
        {
            return PointToPointDistance(p1.X, p1.Y, p2.X, p2.Y);
        }

        /// <summary>
        /// 两点距离的平方
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        public static double PointToPointDistance2(IPoint p1, IPoint p2)
        {
            return PointToPointDistance2(p1.X, p1.Y, p2.X, p2.Y);
        }

        /// <summary>
        /// 两点距离的平方
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        /// <returns></returns>
        public static double PointToPointDistance2(double x1, double y1, double x2, double y2)
        {
            return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
        }

        /// <summary>
        /// 点到线段的最短距离
        /// </summary>
        /// <param name="p">点</param>
        /// <param name="A">线段端点</param>
        /// <param name="B">线段端点</param>
        /// <returns></returns>
        public static double PointToLinesegmentDistance(IPoint p, IPoint A, IPoint B)
        {
            //线段头尾节点一样时
            if (A.X == B.X && A.Y == B.Y)
            {
                return PointToPointDistance(p, A);
            }
            //
            /*
             *     AC dot AB    |AB||AC|cos<AC,AB>
             * r = --------- = ---------------------
             *       |AB|^2          |AB|^2
             * r的值有以下意义：
             * r = 0时, p = A
             * r = 1时, p = B
             * r < 0时, p在AB延伸的相反反向（p靠A近）
             * r > 1时, p在AB延伸方向（p靠B近）
             * 0 < r < 1时, p在投影在AB上, p点离AB的最短距离为P到AB垂线长
             */
            double segment2 = ((B.X - A.X) * (B.X - A.X) + (B.Y - A.Y) * (B.Y - A.Y));
            double r = ((p.X - A.X) * (B.X - A.X) + (p.Y - A.Y) * (B.Y - A.Y))
                       /
                       segment2;

            if (r <= 0.0) return PointToPointDistance(p, A);
            if (r >= 1.0) return PointToPointDistance(p, B);

            ////这里使用的是面积法
            //double s = ((A.Y - p.Y) * (B.X - A.X) - (A.X - p.X) * (B.Y - A.Y))
            //           /
            //           segment2;
            //return Math.Abs(s) * Math.Sqrt(segment2);

            //这里采用的是投影法
            //投影点
            Point D = new Point(A.X + r * (B.X - A.X), A.Y + r * (B.Y - A.Y));
            return PointToPointDistance(p, D);
        }

        /// <summary>
        /// 点P到线段AB的最近点
        /// </summary>
        /// <param name="p"></param>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        public static double PointToLinesegmentMinDisPoint(IPoint p, IPoint A, IPoint B, IPoint pR)
        {
            //线段头尾节点一样时
            if (A.X == B.X && A.Y == B.Y)
            {
                pR.X = A.X; pR.Y = A.Y;
                return PointToPointDistance(p, A);
            }
            //
            /*
             *     AC dot AB    |AB||AC|cos<AC,AB>
             * r = --------- = ---------------------
             *       |AB|^2          |AB|^2
             * r的值有以下意义：
             * r = 0时, p = A
             * r = 1时, p = B
             * r < 0时, p在AB延伸的相反反向（p靠A近）
             * r > 1时, p在AB延伸方向（p靠B近）
             * 0 < r < 1时, p在投影在AB上, p点离AB的最短距离为P到AB垂线长
             */
            double segment2 = ((B.X - A.X) * (B.X - A.X) + (B.Y - A.Y) * (B.Y - A.Y));
            double r = ((p.X - A.X) * (B.X - A.X) + (p.Y - A.Y) * (B.Y - A.Y))
                       /
                       segment2;

            if (r <= 0.0)
            {
                pR.X = A.X; pR.Y = A.Y;
                return PointToPointDistance(p, A);
            }
            if (r >= 1.0)
            {
                pR.X = B.X; pR.Y = B.Y;
                return PointToPointDistance(p, B);
            }

            ////这里使用的是面积法
            //double s = ((A.Y - p.Y) * (B.X - A.X) - (A.X - p.X) * (B.Y - A.Y))
            //           /
            //           segment2;
            //return Math.Abs(s) * Math.Sqrt(segment2);

            //这里采用的是投影法
            //投影点
            pR.X = A.X + r * (B.X - A.X);
            pR.Y = A.Y + r * (B.Y - A.Y);
            return PointToPointDistance(p, pR);
        }

        /// <summary>
        /// 点到线段的距离的平方
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        /// <returns></returns>
        public static double PointToLinesegmentDistance2(double x, double y,
            double x1, double y1, double x2, double y2)
        {
            if (x1 == x2 && y1 == y2)
            {
                return PointToPointDistance2(x, y, x1, y1);
            }
            double segment2 = PointToPointDistance2(x1, y1, x2, y2);
            double r = ((x - x1) * (x2 - x1) + (y - y1) * (y2 - y1))
                       /
                       segment2;
            if (r <= 0.0) return PointToPointDistance2(x, y, x1, y1);
            if (r >= 1.0) return PointToPointDistance2(x, y, x2, y2);
            return PointToPointDistance2(x, y, x1 + r * (x2 - x1), y1 + r * (y2 - y1));
        }

        /// <summary>
        /// 点到线段的距离的平方
        /// </summary>
        /// <param name="p"></param>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        public static double PointToLinesegmentDistance2(IPoint p, IPoint A, IPoint B)
        {
            return PointToLinesegmentDistance2(p.X, p.Y, A.X, A.Y, B.X, B.Y);
        }

        /// <summary>
        /// 快速计算点到线段的最短距离（取到2个端点距离的较小值）
        /// </summary>
        /// <param name="p"></param>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        public static double FastPointToLinesegmentDistance(IPoint p, IPoint A, IPoint B)
        {
            return Math.Min(PointToPointDistance(p, A), PointToPointDistance(p, B));
        }

        /// <summary>
        /// 判断p点对于AB的方向
        /// </summary>
        public static RelativeDirection RelativeDirectionCal(IPoint p, IPoint A, IPoint B)
        {
            double x1 = B.X - A.X;
            double y1 = B.Y - A.Y;
            double x2 = p.X - A.X;
            double y2 = p.Y - A.Y;
            /*
             * 计算叉乘结果向量的Z坐标
             * AP×AB=(x2,y2)×(x1,y1)
             */
            double d = x2 * y1 - x1 * y2;
            if (d > 0.0)
                return RelativeDirection.Right;
            else if (d < 0.0)
                return RelativeDirection.Left;
            else
            {
                double x3 = x2 - x1;
                double y3 = y2 - y1;
                d = x2 * x3 + y2 * y3;
                if (d > 0.0)
                    return RelativeDirection.Front;
                else if (d < 0.0)
                    return RelativeDirection.Behind;
                else
                    return RelativeDirection.Origin;
            }
        }

        /// <summary>
        /// 计算目标点相对于参考点的角度(笛卡尔坐标系，逆时针[0,360）)
        /// </summary>
        /// <param name="referencePoint"></param>
        /// <param name="targetPoint"></param>
        /// <returns></returns>
        public static double RelativeAngle(IPoint referencePoint, IPoint targetPoint)
        {
            double distance = PointToPointDistance(referencePoint, targetPoint);
            if (targetPoint.Y > referencePoint.Y)
            {
                double rotate = Math.Acos((targetPoint.X - referencePoint.X) / distance);
                return (2 * Math.PI - rotate) * 180 / Math.PI;
            }
            else
            {
                return Math.Acos((targetPoint.X - referencePoint.X) / distance) * 180 / Math.PI;
            }
        }

        #region 点在多边形内判断

        /// <summary>
        /// 点是否在多边形内部(射线法)
        /// </summary>
        /// <param name="pt"></param>
        /// <param name="polygon">简单多边形</param>
        /// <returns></returns>
        public static bool IsPointInPolygonByRay(IPoint p, IPolygon polygon)
        {
            int i;
            int i1;             // point index; i1 = i-1
            int crossings = 0;  // number of segment/ray crossings
            if (polygon.ExteriorRing == null) return false;
            int nPts = polygon.ExteriorRing.Count;

            /*
            *  For each segment l = (i-1, i), see if it crosses ray from test point in positive x direction.
            */
            for (i = 1; i < nPts; i++)
            {
                i1 = i - 1;
                IPoint p1 = polygon.ExteriorRing[i];
                IPoint p2 = polygon.ExteriorRing[i1];

                if (SegmentIntersect(p1, p2, p, new Point(double.MaxValue, p.Y)))
                    crossings++;
            }

            /*
            *  p is inside if number of crossings is odd.
            */
            if ((crossings % 2) == 1)
                return true;
            else return false;

        }

        /// <summary>
        /// 线段是否相交
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <param name="C"></param>
        /// <param name="D"></param>
        /// <returns></returns>
        public static bool SegmentIntersect(IPoint A, IPoint B, IPoint C, IPoint D)
        {
            double delta = Determinant(B.X - A.X, C.X - D.X, B.Y - A.Y, C.Y - D.Y);
            if (delta <= (1e-6) && delta >= -(1e-6))  //两线段重合或平行
            {
                return false;
            }
            double namenda = Determinant(C.X - A.X, C.X - D.X, C.Y - A.Y, C.Y - D.Y) / delta;
            if (namenda > 1 || namenda < 0)
            {
                return false;
            }
            double miu = Determinant(B.X - A.X, C.X - A.X, B.Y - A.Y, C.Y - A.Y) / delta;
            if (miu > 1 || miu < 0)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 行列式
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <param name="v3"></param>
        /// <param name="v4"></param>
        /// <returns></returns>
        public static double Determinant(double v1, double v2, double v3, double v4)
        {
            return (v1 * v3 - v2 * v4);
        }

        #endregion
    }
}
