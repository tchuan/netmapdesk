﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Drawing;

namespace NetMap.Interfaces.Symbol
{
    /// <summary>
    /// 线节点符号角度类型
    /// </summary>
    public enum LineNodeSymbolRotateType
    {
        /// <summary>
        /// 没有限制
        /// </summary>
        None,
        /// <summary>
        /// 按进入的角度来
        /// </summary>
        In,
        /// <summary>
        /// 按出去的角度来
        /// </summary>
        Out
    }

    /// <summary>
    /// 线符号接口
    /// </summary>
    public interface ILineSymbol : ISymbol
    {
        /// <summary>
        /// 获取和设置首节点符号
        /// </summary>
        IPointSymbol StartNodeSymbol { get; set; }
        /// <summary>
        /// 获取和设置尾节点符号
        /// </summary>
        IPointSymbol EndNodeSymbol { get; set; }
        /// <summary>
        /// 获取和设置节点符号
        /// </summary>
        IPointSymbol NodeSymbol { get; set; }
        /// <summary>
        /// 获取和设置附加节点符号
        /// </summary>
        IPointSymbol AdditionalNodeSymbol { get; set; }
        /// <summary>
        /// 获取和设置线上节点符号
        /// </summary>
        IOnEdgePointSymbol OnEdgeNodeSymbol { get; set; }        
        /// <summary>
        /// 获取和设置是否表现节点
        /// </summary>
        bool NodeAvailable { get; set; }
        /// <summary>
        /// 获取和设置是否表现附加节点
        /// </summary>
        bool AdditionalNodeAvailable { get; set; }
        /// <summary>
        /// 获取和设置是否表现线上节点
        /// </summary>
        bool OnEdgeNodeAvailable { get; set; }
        /// <summary>
        /// 获取和设置首节点是否有方向
        /// </summary>
        bool IsStartNodeHasDirection { get; set; }
        /// <summary>
        /// 获取和设置末节点是否有方向
        /// </summary>
        bool IsEndNodeHasDirection { get; set; }
        /// <summary>
        /// 获取和设置是否表示附加线
        /// </summary>
        bool IsAssistLine { get; set; }
        /// <summary>
        /// 获取和设置节点符号的旋转类型
        /// </summary>
        LineNodeSymbolRotateType NodeSymbolRotateType { get; set; }
    }

    /// <summary>
    /// 线划线符号
    /// </summary>
    public interface IStrokeSymbol : ILineSymbol
    {        
        /// <summary>
        /// 获取和设置线划
        /// </summary>
        Pen Pen { get; set; }
    }

    /// <summary>
    /// 复合线符号接口
    /// </summary>
    public interface IMultiLineSymbol : ILineSymbol, IList<IStrokeSymbol>
    {
    }
}
