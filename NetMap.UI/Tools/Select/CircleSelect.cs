﻿using NetMap.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.UI.Properties;
using System.Windows.Forms;
using NetMap.Interfaces.Render;
using NetMap.Geometry;
using NetMap.Interfaces.Geometry;
using NetMap.Data;
using NetMap.Interfaces.Data;
using System.Drawing;

namespace NetMap.Tools.Select
{
    /// <summary>
    /// 按圆选择
    /// </summary>
    public class CircleSelect : Tool
    {
        public CircleSelect()
            : base("按圆选择")
        {
        }

        public CircleSelect(IMapEditView view)
            : base("按圆选择", view)
        {
        }

        /// <summary>
        /// 目标图层
        /// </summary>
        public IFeatureLayer TargetLayer
        {
            get;
            set;
        }

        private int _downx = 0, _downy = 0;
        private int _radius = 3; //半径至少为3
        private bool _isDown = false;

        protected override void SetIcon()
        {
            _icon = Resources.选择;
        }

        public override void GainFocus()
        {
            _isDown = false;
            _radius = 3;
            base.GainFocus();
        }

        public override void LostFocus()
        {
            _isDown = false; 
            _radius = 3;
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (_isDown) return;
                _downx = e.X;
                _downy = e.Y;
                _isDown = true;
            }
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (_isDown)
                {
                    int radius = Math.Max(Math.Abs(e.X - _downx), Math.Abs(e.Y - _downy));
                    if (radius < 3)
                    {
                        _radius = 3;
                    }
                    else
                    {
                        _radius = radius;
                    }                
                    View.Refresh();
                }
            }
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (!_isDown) return;
                _isDown = false;

                IDisplayTransformation trans = View.DisplayTransformation;
                //构建范围
                double x = 0, y = 0;
                trans.ToMapPoint(_downx, _downy, ref x, ref y);
                ICircle circle = new Circle(x, y, _radius / trans.ScaleRatio);
                //构建查询器
                ISpatialFilter filter = new SpatialFilter();
                filter.Geometry = circle as IGeometry;
                filter.SpatialRelationship = geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_INTERSECTS;
                //选择                
                if (TargetLayer.Visible
                && trans.ScaleRatio >= TargetLayer.MinScale
                && trans.ScaleRatio <= TargetLayer.MaxScale
                && (TargetLayer is IFeatureSelection))
                {
                    IFeatureSelection featureSelection = TargetLayer as IFeatureSelection;
                    featureSelection.Select(filter, geoSelectionResultEnum.GEO_SELECTIONRESULT_NEW);
                }
                View.Invalidate();
            }
        }

        public override void Draw(Graphics g)
        {
            if (_isDown)
            {
                g.DrawEllipse(Pens.Green, _downx - _radius,
                    _downy - _radius, 2 * _radius, 2 * _radius);
            }
        }
    }
}
