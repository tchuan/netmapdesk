﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;

namespace NetMap.Application.Gui.Pads
{
    public class DistanceCalculator
    {
        private const double EARTH_RADIUS = 6378.137;//地球半径
        private static double rad(double d)
        {
            return d * Math.PI / 180.0;
        }

        /// <summary>
        /// 计算经纬度点的距离
        /// </summary>
        /// <param name="lat1">纬度</param>
        /// <param name="lng1">经度</param>
        /// <param name="lat2">纬度</param>
        /// <param name="lng2">经度</param>
        /// <returns></returns>
        public static double GetDistance(double lat1, double lng1, double lat2, double lng2)
        {
            double radLat1 = rad(lat1);
            double radLat2 = rad(lat2);
            double a = radLat1 - radLat2;
            double b = rad(lng1) - rad(lng2);

            double s = 2 * Math.Asin(Math.Sqrt(Math.Pow(Math.Sin(a / 2), 2) +
              Math.Cos(radLat1) * Math.Cos(radLat2) * Math.Pow(Math.Sin(b / 2), 2)));
            s = s * EARTH_RADIUS;
            s = Math.Round(s * 10000) / 10000;
            return s;
        }

        /// <summary>
        /// 获取经纬度线的总长
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public static double GetTotalDistance(ILineString line)
        {
            double sum = 0.0;
            IPoint p1, p2;
            int count = line.Count;
            for (int i = 0; i < count - 1; i++)
            {
                p1 = line[i];
                p2 = line[i + 1];
                sum += GetDistance(p1.Y, p1.X, p2.Y, p2.X);
            }
            return sum;
        }
    }
}
