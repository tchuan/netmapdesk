﻿using NetMap.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Tools.Select
{
    /// <summary>
    /// 按多边形选择
    /// </summary>
    public class PolygonSelect : Tool
    {
        public PolygonSelect()
            : base("按多边形选择")
        {
        }

        public PolygonSelect(IMapEditView view)
            : base("按多边形选择", view)
        {
        }
    }
}
