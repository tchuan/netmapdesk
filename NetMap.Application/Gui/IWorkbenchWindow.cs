﻿// Copyright (c) AlphaSierraPapa for the SharpDevelop Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

using System;
using System.Collections.Generic;

namespace NetMap.Application.Gui
{
	/// <summary>
	/// The IWorkbenchWindow is the basic interface to a window which
	/// shows a view (represented by the IViewContent object).
	/// </summary>
	public interface IWorkbenchWindow
	{
		/// <summary>
		/// The window title.
		/// </summary>
		string Title {
			get;
		}
		
		/// <summary>
		/// Gets if the workbench window has been disposed.
		/// </summary>
		[Obsolete("This property always returns false.")]
		bool IsDisposed {
			get;
		}
				
		/// <summary>
		/// Gets/Sets the icon of the view content.
		/// </summary>
		System.Windows.Media.ImageSource Icon {
			get;
			set;
		}
		
		/// <summary>
		/// Closes the window, if force == true it closes the window
		/// without asking, even the content is dirty.
		/// </summary>
		/// <returns>true, if window is closed</returns>
		bool CloseWindow(bool force);
		
		/// <summary>
		/// Brings this window to front and sets the user focus to this
		/// window.
		/// </summary>
		void SelectWindow();
		
		/// <summary>
		/// Is called when the title of this window has changed.
		/// </summary>
		event EventHandler TitleChanged;
	}
}
