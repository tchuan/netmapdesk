﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Symbol
{
    /// <summary>
    /// 符号大类
    /// </summary>
    public enum SymbolType
    {
        /// <summary>
        /// 点符号
        /// </summary>
        GEO_SYMBOL_POINT = 1,
        /// <summary>
        /// 线划符号
        /// </summary>
        GEO_SYMBOL_LINE  = 2,
        /// <summary>
        /// 填充符号
        /// </summary>
        GEO_SYMBOL_FILL  = 3,
        /// <summary>
        /// 文本符号
        /// </summary>
        GEO_SYMBOL_TEXT  = 4,
        /// <summary>
        /// 空符号
        /// </summary>
        GEO_SYMBOL_NULL  = 5
    }

    /// <summary>
    /// 符号接口
    /// </summary>
    public interface ISymbol
    {
        /// <summary>
        /// 获取和设置名称
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// 获取和设置编码，Code用于标识符号
        /// </summary>
        int Code { get; set; } 
        /// <summary>
        /// 复制
        /// </summary>
        /// <returns></returns>
        ISymbol Clone();
        /// <summary>
        /// 获取符号类型
        /// </summary>
        SymbolType SymbolType { get; }
        /// <summary>
        /// 获取符号的类别描述
        /// </summary>
        string KindAbstract { get; }
        /// <summary>
        /// 获取和设置符号是否随地图缩放
        /// </summary>
        bool ZoomWithMap { get; set; }
        /// <summary>
        /// 获取和设置参考比例尺
        /// </summary>
        double ReferenceScale { get; set; }
    }
}
