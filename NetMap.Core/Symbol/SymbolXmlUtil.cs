﻿using NetMap.Interfaces.Persist;
using NetMap.Interfaces.Symbol;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Xml;

namespace NetMap.Symbol
{
    public class SymbolXmlUtil
    {
        /// <summary>
        /// 分析文本符号
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static ITextSymbol ParseTextSymbol(XmlReader reader, string endElement)
        {
            ITextSymbol symbol = null;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    switch (reader.Name)
                    {
                        case "TextSymbol":
                            {
                                symbol = new TextSymbol();                                
                                break;
                            }
                        default:
                            {
                                throw new NotSupportedException();
                            }
                    }
                    IPersistResource pr = symbol as IPersistResource;
                    pr.Load(reader, null);
                    break; //找第一个文本符号
                }
                else if (reader.NodeType == XmlNodeType.EndElement
                    && reader.Name == endElement)
                {
                    break;
                }
            }
            return symbol;
        }

        /// <summary>
        /// 分析点符号
        /// </summary>
        /// <param name="node"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static IPointSymbol ParsePointSymbol(XmlReader reader, object context, string endElement)
        {
            IPointSymbol symbol = null;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    switch (reader.Name)
                    {
                        case "ArcPointSymbol":
                            {
                                symbol = new ArcPointSymbol();
                                break;
                            }
                        case "BoxPointSymbol":
                            {
                                symbol = new BoxPointSymbol();
                                break;
                            }
                        case "FontPointSymbol":
                            {
                                symbol = new FontPointSymbol();
                                break;
                            }
                        case "ImagePointSymbol":
                            {
                                symbol = new ImagePointSymbol();
                                break;
                            }
                        case "OnEdgePointSymbol":
                            {
                                symbol = new OnEdgePointSymbol();
                                break;
                            }
                        case "MultiPointSymbol":
                            {
                                symbol = new MultiPointSymbol();
                                break;
                            }
                        default:
                            {
                                throw new NotSupportedException("不支持的点符号类型");
                            }
                    }
                    IPersistResource pr = symbol as IPersistResource;
                    pr.Load(reader, context);
                    break; //找第一个点符号
                }
                else if (reader.NodeType == XmlNodeType.EndElement
                    && reader.Name == endElement)
                {
                    break;
                }
            }
            return symbol;
        }

        /// <summary>
        /// 分析线符号
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static ILineSymbol ParseLineSymbol(XmlReader reader, object context, string endElement)
        {
            ILineSymbol symbol = null;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    switch (reader.Name)
                    {
                        case "StrokeSymbol":
                            {
                                symbol = new StrokeSymbol();
                                break;
                            }
                        case "MultiLineSymbol":
                            {
                                symbol = new MultiLineSymbol();
                                break;
                            }
                        default:
                            {
                                throw new NotSupportedException("不支持的线符号类型");
                            }
                    }
                    IPersistResource pr = symbol as IPersistResource;
                    pr.Load(reader, context);
                    break; //找第一个线符号
                }
                else if (reader.NodeType == XmlNodeType.EndElement
                    && reader.Name == endElement)
                {
                    break;
                }
            }
            return symbol;
        }

        /// <summary>
        /// 分析填充符号
        /// </summary>
        /// <param name="node"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static IFillSymbol ParseFillSymbol(XmlReader reader, object context, string endElement)
        {
            IFillSymbol symbol = null;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    switch (symbol.Name)
                    {
                        case "SolidFillSymbol":
                            {
                                symbol = new SolidFillSymbol();
                                break;
                            }
                        case "MultiFillSymbol":
                            {
                                symbol = new MultiFillSymbol();
                                break;
                            }
                    }
                    IPersistResource pr = symbol as IPersistResource;
                    pr.Load(reader, context);
                    break;
                }
                else if (reader.NodeType == XmlNodeType.EndElement
                    && reader.Name == endElement)
                {
                    break;
                }
            }
            return symbol;
        }

        /// <summary>
        /// xml化Pen
        /// </summary>
        /// <param name="node"></param>
        /// <param name="pen"></param>
        public static void XmlPen(XmlElement node, Pen pen)
        {
            node.SetAttribute("Width", pen.Width.ToString("%.2f"));            
            node.SetAttribute("Color", string.Format("#{0:X2}{1:X2}{2:X2}", pen.Color.R, pen.Color.G, pen.Color.B, pen.Color.A));
            node.SetAttribute("LineJoin", pen.LineJoin.ToString());
            node.SetAttribute("DashOffset", pen.DashOffset.ToString("%.2f"));
            if (pen.DashPattern != null)
            {
                StringBuilder builder = new StringBuilder();
                bool startFlag = true;
                foreach (float item in pen.DashPattern)
                {
                    builder.Append(item.ToString("%.2f"));
                    if (!startFlag)
                    {
                        builder.Append(' ');
                        startFlag = false;
                    }
                }
                string dashPattern = builder.ToString();
                node.SetAttribute("DashPattern", dashPattern);
            }
        }

        /// <summary>
        /// 分析Pen
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public static Pen ParsePen(XmlReader reader)
        {
            Pen pen = new Pen(Color.Black);
            pen.Width = float.Parse(reader.GetAttribute("Width"));
            pen.Color = ColorTranslator.FromHtml(reader.GetAttribute("Color"));
            pen.LineJoin = (LineJoin)Enum.Parse(typeof(LineJoin), reader.GetAttribute("LineJoin"));
            pen.DashOffset = float.Parse(reader.GetAttribute("DashOffset"));
            string dashPattern = reader.GetAttribute("DashPattern");
            if (!string.IsNullOrEmpty(dashPattern))
            {
                string[] dashArray = dashPattern.Split(' ');
                int length = dashArray.Length;
                if (length > 0)
                {
                    float[] array = new float[length];
                    for (int i = 0; i < length; i++)
                    {
                        array[i] = float.Parse(dashArray[i]);
                    }
                    pen.DashPattern = array;
                }
            }
            return pen;
        }
    }
}
