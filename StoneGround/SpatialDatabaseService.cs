﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using NetMap.Interfaces.Data;
using NetMap.Data;
using NetMap.Sqlite;

namespace StoneGround
{
    /// <summary>
    /// 系统的空间数据服务
    /// </summary>
    public class SpatialDatabaseService
    {
        private static SpatialDatabaseService _instance;
        public static SpatialDatabaseService Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SpatialDatabaseService();
                }
                return _instance;
            }
        }

        private SpatialDatabaseService()
        {
        }

        private ISpatialDatabase _sdb;
        private IFeatureClass _provinceFeatureClass;
        private IFeatureClass _zoneFeatureClass;
        private IFeatureClass _roadFeatureClass;
        private IFeatureClass _dzoneFeatureClass;

        #region 数据库
        private const string STABLE_PROVINCE = "v_province"; //省
        private const string STABLE_ZONE = "v_zone"; //县级行政区
        private const string STABLE_ROAD = "v_road"; //道路
        private const string STABLE_DZONE = "v_dzone"; //地级行政区
        public const string FIELD_ZONE_NAME = "NAME99";
        public const string FIELD_ZONE_ADCODE = "ADCODE99";
        public const string FIELD_ROAD_LEVEL = "roadlevel";
        public const string FIELD_ROAD_NAME = "roadname";
        public const string FIELD_ROAD_CODE = "roadcode";
        public const string FIELD_DZONE_ADCODE = "ADCODE99";
        public const string FIELD_DZONE_NAME = "NAME99";
        #endregion

        /// <summary>
        /// 初始化
        /// </summary>
        public void Init()
        {
            string dbFile = Path.Combine(Application.StartupPath, "map\\yn.db");
            IConnectProperties connectProperties = new ConnectProperties();
            connectProperties.Database = dbFile;
            ISpatialDatabaseFactory factory = new SqliteSpatialDatabaseFactory2();
            _sdb = factory.Open(connectProperties);
            _provinceFeatureClass = _sdb.OpenFeatureClass(STABLE_PROVINCE);
            _zoneFeatureClass = _sdb.OpenFeatureClass(STABLE_ZONE);            
            _roadFeatureClass = _sdb.OpenFeatureClass(STABLE_ROAD);
            _dzoneFeatureClass = _sdb.OpenFeatureClass(STABLE_DZONE);
        }

        #region 获取

        /// <summary>
        /// 获取省要素集
        /// </summary>
        public IFeatureClass ProviceFeatureClass
        {
            get
            {
                return _provinceFeatureClass;
            }
        }

        /// <summary>
        /// 获取区划要素集
        /// </summary>
        public IFeatureClass ZoneFeatureClass
        {
            get
            {
                return _zoneFeatureClass;
            }
        }

        /// <summary>
        /// 获取道路要素集
        /// </summary>
        public IFeatureClass RoadFeatureClass
        {
            get
            {
                return _roadFeatureClass;
            }
        }

        /// <summary>
        /// 获取地级行政区
        /// </summary>
        public IFeatureClass DzoneFeatureClass
        {
            get { return _dzoneFeatureClass; }
        }

        #endregion

        /// <summary>
        /// 关闭
        /// </summary>
        public void Close()
        {
            if (_sdb != null)
            {
                _sdb.Close();
            }
        }
    }
}
