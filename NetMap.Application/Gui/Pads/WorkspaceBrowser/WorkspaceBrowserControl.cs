﻿// Copyright (c) AlphaSierraPapa for the KJSuite Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

using System;
using System.IO;
using System.Drawing;
using System.Windows.Forms;

using NetMap.Application.Core;
using NetMap.Application.Gui;
using NetMap.Application.WorkspaceBrowser.TreeNodes;

namespace NetMap.Application.WorkspaceBrowser
{
	/// <summary>
	/// 工作空间树
	/// </summary>
	public class WorkspaceBrowserControl : System.Windows.Forms.UserControl
	{
		ExtTreeView treeView;
		
		public AbstractWorkspaceBrowserTreeNode SelectedNode {
			get {
                return treeView.SelectedNode as AbstractWorkspaceBrowserTreeNode;
			}
		}
		
		public ExtTreeView TreeView {
			get {
				return treeView;
			}
		}

        public WorkspaceBrowserControl()
		{
			InitializeComponent();
			treeView.CanClearSelection = false;
		}
		
		void CallVisitor(WorkspaceBrowserTreeNodeVisitor visitor)
		{
            foreach (AbstractWorkspaceBrowserTreeNode treeNode in treeView.Nodes)
            {
				treeNode.AcceptVisitor(visitor, null);
			}
		}
		
		/// <summary>
		/// Writes the current view state into the memento.
		/// </summary>
		public void StoreViewState(Properties memento)
		{
			memento.Set("ProjectBrowserState", TreeViewHelper.GetViewStateString(treeView));
		}
		
		/// <summary>
		/// Reads the view state from the memento.
		/// </summary>
		public void ReadViewState(Properties memento)
		{
			TreeViewHelper.ApplyViewStateString(memento.Get("ProjectBrowserState", ""), treeView);
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.treeView = new ExtTreeView();
			this.SuspendLayout();
			// 
			// treeView
			// 
			this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.treeView.ImageIndex = -1;
			this.treeView.Location = new System.Drawing.Point(0, 0);
			this.treeView.Name = "treeView";
			this.treeView.SelectedImageIndex = -1;
			this.treeView.Size = new System.Drawing.Size(292, 266);
			this.treeView.TabIndex = 0;
			
			// 
			// ProjectBrowserControl
			// 
			this.Controls.Add(this.treeView);
			this.Name = "WorkspaceBrowserControl";
			this.Size = new System.Drawing.Size(292, 266);
			this.ResumeLayout(false);
		}
		#endregion
		
		public void ExpandOrCollapseAll(bool expand)
		{
			if (this.treeView == null) return;
			if (this.treeView.Nodes == null || this.treeView.Nodes.Count == 0) return;
			
			if (expand) {
				this.treeView.ExpandAll();
			}
			else {
				this.treeView.CollapseAll();
			}
			
			this.treeView.Nodes[0].Expand();
		}
	}
}
