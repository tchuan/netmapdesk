﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Application.Core;
using StoneGround.Search.TreeNodes;
using System.Windows.Forms;

namespace StoneGround.Search.Commands
{
    /// <summary>
    /// 料场定位
    /// </summary>
    public class Position : AbstractMenuCommand
    {
        public override void Run()
        {
            SearchPad search = SearchPad.Instance;
            TreeNode node = search.StoneGroundTreeView.SelectedNode;
            if (node != null)
            {
                StoneGroundNode sgNode = node as StoneGroundNode;
                sgNode.Position();
            }
        }
    }
}
