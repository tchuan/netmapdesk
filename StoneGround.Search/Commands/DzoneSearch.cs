﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Application.Core;
using NetMap.Application.Core.WinForms;
using NetMap.Data;
using NetMap.Interfaces.Data;
using NetMap.Application;
using NetMap.Interfaces;
using NetMap.Interfaces.Geometry;

namespace StoneGround.Search.Commands
{
    public delegate void DzoneChangeHandler(string dzone);

    /// <summary>
    /// 地级查找
    /// </summary>
    public class DzoneSearch : AbstractComboBoxCommand
    {
        /// <summary>
        /// 实例
        /// </summary>
        public static DzoneSearch Instance;
        /// <summary>
        /// 地级行政区变化事件
        /// </summary>
        public event DzoneChangeHandler DzoneChange;

        public DzoneSearch()
        {
            Instance = this;
        }

        public override void Run()
        {
            //do nothing
        }

        protected override void OnOwnerChanged(EventArgs e)
        {
            base.OnOwnerChanged(e);

            //调整尺寸
            ToolBarComboBox comboBox = (ToolBarComboBox)base.ComboBox;
            comboBox.AutoSize = false;
            comboBox.Width = 150;

            //加载区划
            comboBox.Items.Add("无");
            IQueryFilter filter = new QueryFilter();
            filter.Fields.Add(SpatialDatabaseService.FIELD_DZONE_NAME);
            IFeatureClass dzoneFeatureClass = SpatialDatabaseService.Instance.DzoneFeatureClass;
            IFeatureCursor cursor = dzoneFeatureClass.Search(filter);
            IFeature feature = cursor.Next();
            while (feature != null)
            {
                string name = feature.GetValue(0).ToString();
                comboBox.Items.Add(name);
                feature = cursor.Next();
            }
            cursor.Close();

            comboBox.SelectedIndex = 0;
            comboBox.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged); 
        }

        void comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ToolBarComboBox comboBox = (ToolBarComboBox)base.ComboBox;
            string text = comboBox.Text;
            IMapView view = WorkspaceSingleton.ActiveMapView;
            ILayer layer = view.MapDocument.FindLayer(StoneGroundDefines.LAYER_NAME_DZONE);
            IFeatureSelection selection = layer as IFeatureSelection;
            if (text == "无")
            {
                selection.Clear();
                SearchPad.Instance.Condition.IsDzone = false;
                SearchPad.Instance.Condition.Dzone = "";
            }
            else
            {
                //选择面
                IQueryFilter filter = new QueryFilter();
                filter.WhereClause = SpatialDatabaseService.FIELD_DZONE_NAME + "='" + text + "'";
                selection.Select(filter, geoSelectionResultEnum.GEO_SELECTIONRESULT_NEW);
                SearchPad.Instance.Condition.IsDzone = true;
                SearchPad.Instance.Condition.Dzone = text;
                //触发地级行政区变化事件
                if (DzoneChange != null)
                {
                    DzoneChange.Invoke(text);
                }
            }
            //刷新地图
            if (selection.SelectionSet != null)
            {
                IEnvelope box = selection.SelectionSet.Envelope;
                if (box != null)
                {
                    //外扩1/2后，缩放
                    IPoint center = box.Center;
                    IEnvelope boxZoom = new NetMap.Geometry.Envelope(center.X - box.Width, center.Y - box.Height,
                        center.X + box.Width, center.Y + box.Height);
                    view.ZoomToData(boxZoom);
                }
            }
            view.Invalidate();
        }
    }
}
