﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Data
{
    /// <summary>
    /// 属性表接口
    /// </summary>
    public interface ITable : IData
    {
        /// <summary>
        /// 创建行
        /// </summary>
        /// <returns></returns>
        IRow CreateRow();
        /// <summary>
        /// 获取表的行数
        /// </summary>
        /// <returns></returns>
        int GetRowCount();
        /// <summary>
        /// 获取行
        /// </summary>
        /// <param name="oid">OID</param>
        /// <returns></returns>
        IRow GetRow(long oid);
        /// <summary>
        /// 执行查询
        /// </summary>
        /// <param name="filter">查询条件</param>
        /// <returns></returns>
        ICursor Search(IQueryFilter filter);
    }
}
