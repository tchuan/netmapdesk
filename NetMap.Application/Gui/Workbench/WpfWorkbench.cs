﻿// Copyright (c) AlphaSierraPapa for the SharpDevelop Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Navigation;
using NetMap.Application.Core;
using NetMap.Application.Core.Presentation;

namespace NetMap.Application.Gui
{
	/// <summary>
	/// Workbench implementation using WPF and AvalonDock.
	/// </summary>
    sealed partial class WpfWorkbench : FullScreenEnabledWindow, IWorkbench, System.Windows.Forms.IWin32Window
	{
        const string mainMenuPath = "/Application/Workbench/MainMenu";
        const string viewContentPath = "/Application/Workbench/Pads";
        const string toolbarPath = "/Application/Workbench/ToolBar";
		
		public event EventHandler ActiveWorkbenchWindowChanged;
		public event EventHandler ActiveContentChanged;

		public System.Windows.Forms.IWin32Window MainWin32Window { get { return this; } }
		public ISynchronizeInvoke SynchronizingObject { get; private set; }
		public Window MainWindow { get { return this; } }
		public IStatusBarService StatusBar { get; private set; }
		
		IntPtr System.Windows.Forms.IWin32Window.Handle {
			get {
				var wnd = System.Windows.PresentationSource.FromVisual(this) as System.Windows.Interop.IWin32Window;
				if (wnd != null)
					return wnd.Handle;
				else
					return IntPtr.Zero;
			}
		}
		
		List<PadDescriptor> padDescriptorCollection = new List<PadDescriptor>();
		SdStatusBar statusBar = new SdStatusBar();
		ToolBar[] toolBars;
		
		public WpfWorkbench()
		{
			this.SynchronizingObject = new WpfSynchronizeInvoke(this.Dispatcher);
			this.StatusBar = new SdStatusBarService(statusBar);
			InitializeComponent();
			InitFocusTrackingEvents();

            //最大化
            this.WindowState = System.Windows.WindowState.Maximized;
		}

		void SetBounds(Rect bounds)
		{
			this.Left = bounds.Left;
			this.Top = bounds.Top;
			this.Width = bounds.Width;
			this.Height = bounds.Height;
		}
		
		public void Initialize()
		{
			UpdateFlowDirection();
			
			foreach (PadDescriptor content in AddInTree.BuildItems<PadDescriptor>(viewContentPath, this, false)) {
				if (content != null) {
					ShowPad(content);
				}
			}
			
			mainMenu.ItemsSource = MenuService.CreateMenuItems(this, this, mainMenuPath, activationMethod: "MainMenu", immediatelyExpandMenuBuildersForShortcuts: true);
#if DEBUG
#else
#endif

            toolBars = ToolBarService.CreateToolBars(this, this, toolbarPath);
            if (toolBars != null)
            {
                foreach (ToolBar tb in toolBars)
                {
                    DockPanel.SetDock(tb, Dock.Top);
                    dockPanel.Children.Insert(1, tb);
                }
            }
			DockPanel.SetDock(statusBar, Dock.Bottom);
			dockPanel.Children.Insert(dockPanel.Children.Count - 2, statusBar);
			
			UpdateMenu();
			
			requerySuggestedEventHandler = new EventHandler(CommandManager_RequerySuggested);
			CommandManager.RequerySuggested += requerySuggestedEventHandler;
			ResourceService.LanguageChanged += OnLanguageChanged;
			
			this.StatusBar.SetMessage("${res:MainWindow.StatusBar.ReadyMessage}");
		}
		
		// keep a reference to the event handler to prevent it from being garbage collected
		// (CommandManager.RequerySuggested only keeps weak references to the event handlers)
		EventHandler requerySuggestedEventHandler;

		void CommandManager_RequerySuggested(object sender, EventArgs e)
		{
			UpdateMenu();
		}

		void UpdateMenu()
		{
			MenuService.UpdateStatus(mainMenu.ItemsSource);
            if (toolBars != null)
            {
                foreach (ToolBar tb in toolBars)
                {
                    ToolBarService.UpdateStatus(tb.ItemsSource);
                }
            }
		}
		
		void OnLanguageChanged(object sender, EventArgs e)
		{
			MenuService.UpdateText(mainMenu.ItemsSource);
			UpdateFlowDirection();
		}
		
		void UpdateFlowDirection()
		{
			Language language = LanguageService.GetLanguage(ResourceService.Language);
			Core.WinForms.RightToLeftConverter.IsRightToLeft = language.IsRightToLeft;
			this.FlowDirection = language.IsRightToLeft ? FlowDirection.RightToLeft : FlowDirection.LeftToRight;
			App.Current.Resources[GlobalStyles.FlowDirectionKey] = this.FlowDirection;
		}
		
		public IList<IWorkbenchWindow> WorkbenchWindowCollection {
			get {
				WorkbenchSingleton.AssertMainThread();
				if (workbenchLayout != null)
					return workbenchLayout.WorkbenchWindows;
				else
					return new IWorkbenchWindow[0];
			}
		}
		
		public IList<PadDescriptor> PadContentCollection {
			get {
				WorkbenchSingleton.AssertMainThread();
				return padDescriptorCollection.AsReadOnly();
			}
		}
		
		IWorkbenchWindow activeWorkbenchWindow;
		
		public IWorkbenchWindow ActiveWorkbenchWindow {
			get 
            {
				WorkbenchSingleton.AssertMainThread();
				return activeWorkbenchWindow;
			}
			private set 
            {
				if (activeWorkbenchWindow != value) 
                {
					
					activeWorkbenchWindow = value;
					
					if (ActiveWorkbenchWindowChanged != null) {
						ActiveWorkbenchWindowChanged(this, EventArgs.Empty);
					}
				}
			}
		}
		
		void OnActiveWindowChanged(object sender, EventArgs e)
		{
			if (closeAll)
				return;
			
			if (workbenchLayout != null) {
				this.ActiveContent = workbenchLayout.ActiveContent;
				this.ActiveWorkbenchWindow = workbenchLayout.ActiveWorkbenchWindow;
			} else {
				this.ActiveContent = null;
				this.ActiveWorkbenchWindow = null;
			}
		}
		
		object activeContent;
		
		public object ActiveContent {
			get {
				WorkbenchSingleton.AssertMainThread();
				return activeContent;
			}
			private set {
				if (activeContent != value) {
					activeContent = value;
					
					if (ActiveContentChanged != null) {
						ActiveContentChanged(this, EventArgs.Empty);
					}
				}
			}
		}
		
		IWorkbenchLayout workbenchLayout;
		
		public IWorkbenchLayout WorkbenchLayout {
			get {
				return workbenchLayout;
			}
			set {
				WorkbenchSingleton.AssertMainThread();
				
				if (workbenchLayout != null) {
					workbenchLayout.ActiveContentChanged -= OnActiveWindowChanged;
					workbenchLayout.Detach();
				}
				if (value != null) {
					value.Attach(this);
					value.ActiveContentChanged += OnActiveWindowChanged;
				}
				workbenchLayout = value;
				OnActiveWindowChanged(null, null);
			}
		}
		
		public bool IsActiveWindow {
			get {
				return IsActive;
			}
		}	
		
		public void ShowPad(PadDescriptor content)
		{
			WorkbenchSingleton.AssertMainThread();
			if (content == null)
				throw new ArgumentNullException("content");
			if (padDescriptorCollection.Contains(content))
				throw new ArgumentException("Pad is already loaded");
			
			padDescriptorCollection.Add(content);
			
			if (WorkbenchLayout != null) {
				WorkbenchLayout.ShowPad(content);
			}
		}
		
		public void UnloadPad(PadDescriptor content)
		{
		}
		
		public PadDescriptor GetPad(Type type)
		{
			WorkbenchSingleton.AssertMainThread();
			if (type == null)
				throw new ArgumentNullException("type");
			foreach (PadDescriptor pad in PadContentCollection) {
				if (pad.Class == type.FullName) {
					return pad;
				}
			}
			return null;
		}
		
		/// <summary>
		/// Flag used to prevent repeated ActiveWindowChanged events during CloseAllViews().
		/// </summary>
		bool closeAll;
		
		public void CloseAllViews()
		{
			WorkbenchSingleton.AssertMainThread();
			try {
				closeAll = true;
				foreach (IWorkbenchWindow window in this.WorkbenchWindowCollection.ToArray()) {
					window.CloseWindow(false);
				}
			} finally {
				closeAll = false;
				OnActiveWindowChanged(this, EventArgs.Empty);
			}
		}
		
		System.Windows.WindowState lastNonMinimizedWindowState = System.Windows.WindowState.Normal;
		Rect restoreBoundsBeforeClosing;
		
		protected override void OnStateChanged(EventArgs e)
		{
			base.OnStateChanged(e);
			if (this.WindowState != System.Windows.WindowState.Minimized)
				lastNonMinimizedWindowState = this.WindowState;
		}
		
		public Properties CreateMemento()
		{
			Properties prop = new Properties();
			prop.Set("WindowState", lastNonMinimizedWindowState);
			var bounds = this.RestoreBounds;
			if (bounds.IsEmpty) bounds = restoreBoundsBeforeClosing;
			if (!bounds.IsEmpty) {
				prop.Set("Bounds", bounds);
			}
			return prop;
		}
		
		public void SetMemento(Properties memento)
		{
			Rect bounds = memento.Get("Bounds", new Rect(10, 10, 750, 550));
			// bounds are validated after PresentationSource is initialized (see OnSourceInitialized)
			lastNonMinimizedWindowState = memento.Get("WindowState", System.Windows.WindowState.Maximized);
			SetBounds(bounds);
		}
		
		protected override void OnClosing(CancelEventArgs e)
		{
			base.OnClosing(e);
			if (!e.Cancel) 
            {
				
			}
		}
		
		protected override void OnDragEnter(DragEventArgs e)
		{
			try {
				base.OnDragEnter(e);
				if (!e.Handled) {
					e.Effects = GetEffect(e.Data);
					e.Handled = true;
				}
			} catch (Exception ex) {
				MessageService.ShowException(ex);
			}
		}
		
		protected override void OnDragOver(DragEventArgs e)
		{
			try {
				base.OnDragOver(e);
				if (!e.Handled) {
					e.Effects = GetEffect(e.Data);
					e.Handled = true;
				}
			} catch (Exception ex) {
				MessageService.ShowException(ex);
			}
		}
		
		DragDropEffects GetEffect(IDataObject data)
		{
			try {
				if (data != null && data.GetDataPresent(DataFormats.FileDrop)) {
					string[] files = (string[])data.GetData(DataFormats.FileDrop);
					if (files != null) {
						foreach (string file in files) {
							if (File.Exists(file)) {
								return DragDropEffects.Link;
							}
						}
					}
				}
			} catch (COMException) {
				// Ignore errors getting the data (e.g. happens when dragging attachments out of Thunderbird)
			}
			return DragDropEffects.None;
		}
		
		void InitFocusTrackingEvents()
		{
			#if DEBUG
			this.PreviewLostKeyboardFocus += new KeyboardFocusChangedEventHandler(WpfWorkbench_PreviewLostKeyboardFocus);
			this.PreviewGotKeyboardFocus += new KeyboardFocusChangedEventHandler(WpfWorkbench_PreviewGotKeyboardFocus);
			#endif
		}
		
		[Conditional("DEBUG")]
		internal static void FocusDebug(string format, params object[] args)
		{
			#if DEBUG
			if (enableFocusDebugOutput)
				LoggingService.DebugFormatted(format, args);
			#endif
		}
		
		#if DEBUG
		static bool enableFocusDebugOutput;
		
		void WpfWorkbench_PreviewGotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
		{
			FocusDebug("GotKeyboardFocus: oldFocus={0}, newFocus={1}", e.OldFocus, e.NewFocus);
		}
		
		void WpfWorkbench_PreviewLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
		{
			FocusDebug("LostKeyboardFocus: oldFocus={0}, newFocus={1}", e.OldFocus, e.NewFocus);
		}
		
		protected override void OnPreviewKeyDown(KeyEventArgs e)
		{
			base.OnPreviewKeyDown(e);
			if (!e.Handled && e.Key == Key.D && e.KeyboardDevice.Modifiers == (ModifierKeys.Control | ModifierKeys.Shift | ModifierKeys.Alt)) {
				enableFocusDebugOutput = !enableFocusDebugOutput;
				
				StringWriter output = new StringWriter();
				output.WriteLine("Keyboard.FocusedElement = " + GetElementName(Keyboard.FocusedElement));
				output.WriteLine("ActiveContent = " + GetElementName(this.ActiveContent));
				output.WriteLine("ActiveWorkbenchWindow = " + GetElementName(this.ActiveWorkbenchWindow));
				((AvalonDockLayout)workbenchLayout).WriteState(output);
				LoggingService.Debug(output.ToString());
				e.Handled = true;
			}
			if (!e.Handled && e.Key == Key.F && e.KeyboardDevice.Modifiers == (ModifierKeys.Control | ModifierKeys.Shift | ModifierKeys.Alt)) {
				if (TextOptions.GetTextFormattingMode(this) == TextFormattingMode.Display)
					TextOptions.SetTextFormattingMode(this, TextFormattingMode.Ideal);
				else
					TextOptions.SetTextFormattingMode(this, TextFormattingMode.Display);
				this.StatusBar.SetMessage("TextFormattingMode=" + TextOptions.GetTextFormattingMode(this));
			}
			if (!e.Handled && e.Key == Key.R && e.KeyboardDevice.Modifiers == (ModifierKeys.Control | ModifierKeys.Shift | ModifierKeys.Alt)) {
				switch (TextOptions.GetTextRenderingMode(this)) {
					case TextRenderingMode.Auto:
					case TextRenderingMode.ClearType:
						TextOptions.SetTextRenderingMode(this, TextRenderingMode.Grayscale);
						break;
					case TextRenderingMode.Grayscale:
						TextOptions.SetTextRenderingMode(this, TextRenderingMode.Aliased);
						break;
					default:
						TextOptions.SetTextRenderingMode(this, TextRenderingMode.ClearType);
						break;
				}
				this.StatusBar.SetMessage("TextRenderingMode=" + TextOptions.GetTextRenderingMode(this));
			}
			if (!e.Handled && e.Key == Key.G && e.KeyboardDevice.Modifiers == (ModifierKeys.Control | ModifierKeys.Shift | ModifierKeys.Alt)) {
				GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced);
				this.StatusBar.SetMessage("Total memory = " + (GC.GetTotalMemory(true) / 1024 / 1024f).ToString("f1") + " MB");
			}
		}
		#endif
		
		internal static string GetElementName(object element)
		{
			if (element == null)
				return "<null>";
			else
				return element.GetType().FullName + ": " + element.ToString();
        }
    }
}
