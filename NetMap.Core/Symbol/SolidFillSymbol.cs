﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Symbol;
using NetMap.Interfaces.Format;
using System.IO;
using System.Drawing;
using NetMap.Interfaces.Persist;
using System.Xml;
using NetMap.Utilities;

namespace NetMap.Symbol
{
    /// <summary>
    /// 单色填充符号
    /// </summary>
    public class SolidFillSymbol : FillSymbol, ISolidFillSymbol, IBinaryFormat, IPersistResource
    {
        public SolidFillSymbol()
        {
            Color = Color.FromArgb(255, 255, 255, 255); // 白色
            OutLine = new StrokeSymbol();

        }

        public SolidFillSymbol(SolidFillSymbol symbol)
        {
            //TODO
        }

        public Color Color
        {
            get;
            set;
        }

        public override ISymbol Clone()
        {
            return new SolidFillSymbol(this);
        }

        public override string KindAbstract
        {
            get { return SymbolFactory.S_FILL_SOLID; }
        }

        #region IBinaryFormat

        public byte[] ToWkb()
        {
            MemoryStream stream = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(stream);

            writer.Write(Name);
            writer.Write(Code);

            if (OutLine != null)
            {
                writer.Write(OutLine.KindAbstract);
                IBinaryFormat lFormat = OutLine as IBinaryFormat;
                byte[] lWkb = lFormat.ToWkb();
                writer.Write(lWkb.Count());
                writer.Write(lWkb);
            }
            else
            {
                writer.Write(SymbolFactory.S_NULL);
            }

            writer.Write(Color.A);
            writer.Write(Color.R);
            writer.Write(Color.G);
            writer.Write(Color.B);

            return stream.ToArray();
        }

        public void FromWkb(byte[] wkb)
        {
            MemoryStream stream = new MemoryStream(wkb);
            BinaryReader reader = new BinaryReader(stream);

            Name = reader.ReadString();
            Code = reader.ReadInt32();

            string sk = reader.ReadString();
            ISymbol lSymbol = SymbolFactory.CreateSymbol(sk);
            if (lSymbol == null)
            {
                OutLine = null;
            }
            else
            {
                OutLine = lSymbol as ILineSymbol;
                IBinaryFormat lFormat = OutLine as IBinaryFormat;
                int count = reader.ReadInt32();
                byte[] lWkb = reader.ReadBytes(count);
                lFormat.FromWkb(lWkb);
            }

            Color = Color.FromArgb(reader.ReadInt32());
        }

        #endregion

        #region IPersistResource 接口

        public void Save(XmlElement node, XmlDocument document)
        {
            XmlElement symbol = document.CreateElement("SolidFillSymbol");
            symbol.SetAttribute("Name", Name);
            symbol.SetAttribute("Code", Code.ToString());
            //填充色
            symbol.SetAttribute("Color", XmlUtil.ToXml(Color));
            //外框
            if (OutLine != null)
            {
                XmlElement outlineNode = document.CreateElement("OutLine");
                IPersistResource outlineNodePR = OutLine as IPersistResource;
                outlineNodePR.Save(symbol, document);
                symbol.AppendChild(outlineNode);
            }
            node.AppendChild(symbol);
        }

        public void Load(XmlReader reader, object context)
        {
            Name = reader.GetAttribute("Name");
            Code = int.Parse(reader.GetAttribute("Code"));
            Color = ColorTranslator.FromHtml(reader.GetAttribute("Color"));

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    if (reader.Name == "OutLine")
                    {
                        OutLine = SymbolXmlUtil.ParseLineSymbol(reader, context, "OutLine");
                    }                    
                }
                else if (reader.NodeType == XmlNodeType.EndElement
                    && reader.Name == "SolidFillSymbol")
                {
                    return;
                }
            }
        }

        #endregion

        /// <summary>
        /// 获取默认新实例
        /// </summary>
        public static SolidFillSymbol NewDefault
        {
            get
            {
                return new SolidFillSymbol();
            }
        }
    }
}
