﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Symbol;

namespace NetMap.Render
{
    /// <summary>
    /// 图片点符号绘制
    /// </summary>
    internal class ImagePointSymbolDrawer : ISymbolDrawer
    {
        public void Draw(IDisplay display, ISymbol symbol, IGeometry geometry, string text)
        {
            //不检查错误
            IImagePointSymbol ipSym = symbol as IImagePointSymbol;
            switch (geometry.GeometryType)
            {
                case geoGeometryType.GEO_GEOMETRY_POINT:
                    {
                        InnerDraw(display, ipSym, geometry as IPoint, 0);
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTIPOINT:
                    {
                        IMultiPoint points = geometry as IMultiPoint;
                        foreach (IPoint point in points)
                        {
                            InnerDraw(display, ipSym, geometry as IPoint, 0);
                        }
                        break;
                    }
            }
        }

        public void Draw(Graphics g, ISymbol symbol, IGeometry geometry, string text)
        {
            IImagePointSymbol ipSym = symbol as IImagePointSymbol;
            switch (geometry.GeometryType)
            {
                case geoGeometryType.GEO_GEOMETRY_POINT:
                    {
                        IPoint point = geometry as IPoint;
                        InnerDirectlyDraw(g, ipSym, point.X, point.Y, 0);
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTIPOINT:
                    {
                        IMultiPoint points = geometry as IMultiPoint;
                        foreach (IPoint point in points)
                        {
                            InnerDirectlyDraw(g, ipSym, point.X, point.Y, 0);
                        }
                        break;
                    }
            }
        }

        //具体绘制图片点
        private void InnerDraw(IDisplay display, IImagePointSymbol symbol,
            IPoint point, double angle)
        {
            if (symbol.ZoomWithMap) //实时缩放符号
            {
                IDisplayTransformation trans = display.DisplayTransformation;
                double width = symbol.Size * trans.ScaleRatio / symbol.ReferenceScale;
                double height = symbol.Size * trans.ScaleRatio / symbol.ReferenceScale;
                double x = 0, y = 0;
                trans.FromMapPoint(point.X, point.Y, ref x, ref y);
                x -= symbol.AnchorX * width;
                y -= symbol.AnchorY * height;
                Matrix matrix = new Matrix();
                matrix.Translate((float)x, (float)y, MatrixOrder.Append);
                matrix.Rotate((float)angle, MatrixOrder.Append);
                Matrix oldMatrix = display.G.Transform;
                display.G.Transform = matrix;
                display.G.DrawImage(symbol.Image, 0.0f, 0.0f, (float)width, (float)height);
                display.G.Transform = oldMatrix;
                matrix.Dispose(); 
            }
            else //静态符号
            {         
                IDisplayTransformation trans = display.DisplayTransformation;
                double x = 0, y = 0;
                trans.FromMapPoint(point.X, point.Y, ref x, ref y);
                x -= symbol.AnchorX * symbol.Size;
                y -= symbol.AnchorY * symbol.Size;
                Matrix matrix = new Matrix();
                matrix.Translate((float)x, (float)y, MatrixOrder.Append);
                matrix.Rotate((float)angle, MatrixOrder.Append);
                Matrix oldMatrix = display.G.Transform;
                display.G.Transform = matrix;
                display.G.DrawImage(symbol.Image, 0.0f, 0.0f, (float)symbol.Size, (float)symbol.Size);
                display.G.Transform = oldMatrix;
                matrix.Dispose();  
            }
        }

        private void InnerDirectlyDraw(Graphics g, IImagePointSymbol symbol, double x, double y, double angle)
        {
            x -= symbol.AnchorX * symbol.Size;
            y -= symbol.AnchorY * symbol.Size;
            Matrix matrix = new Matrix();
            matrix.Translate((float)x, (float)y, MatrixOrder.Append);
            matrix.Rotate((float)angle, MatrixOrder.Append);
            Matrix oldMatrix = g.Transform;
            g.Transform = matrix;
            g.DrawImage(symbol.Image, 0.0f, 0.0f, (float)symbol.Size, (float)symbol.Size);
            g.Transform = oldMatrix;
            matrix.Dispose();
        }
    }
}
