﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Tile
{
    public interface IMemoryCache<T> : ITileCache<T>
    {
        int MinTiles { get; set; }
        int MaxTiles { get; set; }
    }
}
