﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Data;
using NetMap.Interfaces.Data;
using Community.CsharpSqlite;
using NetMap.Interfaces.SpatialAnalysis;

namespace NetMap.Sqlite
{
    /// <summary>
    /// Sqlite数据库要素游标
    /// </summary>
    internal class SqliteFeatureCursor : IFeatureCursor
    {
        protected SqliteFeatureClass _featureClass;
        protected IFields _fields = new Fields();
        protected bool _last = false; //末尾标志
        protected Sqlite3.Vdbe _stmt = null;
        protected ISpatialAnalysis _SA = null; //空间过滤条件
        protected ISelectionSet _SS = null;    //选择集容器条件

        internal SqliteFeatureCursor()
        {
        }

        public IFields GetFields()
        {
            return _fields;
        }

        public IFeature Next()
        {
            if (_last)
            {
                return null;
            }

            IFeature fRet = null;
            int err = 0;
            while (true)
            {
                fRet = get_feature();
                if (_SS != null && !_SS.IsIn(fRet.OID)) //不满足选择集容器
                {
                    err = Sqlite3.sqlite3_step(_stmt);
                    if (err != Sqlite3.SQLITE_ROW)
                    {
                        _last = true;
                        break;
                    }
                    continue;
                }
                if (_SA != null && !_SA.Evaluate(fRet.Geometry)) //不满足空间过滤条件
                {
                    err = Sqlite3.sqlite3_step(_stmt);
                    if (err != Sqlite3.SQLITE_ROW)
                    {
                        _last = true;
                        break;
                    }
                    continue;
                }                
                break;
            }
            if (_last == true)
            {
                return null;
            }
            else
            {
                err = Sqlite3.sqlite3_step(_stmt);
                if (err != Sqlite3.SQLITE_ROW)
                {
                    _last = true;
                }
                return fRet;
            }
        }

        public void Reset()
        {
            Sqlite3.sqlite3_reset(_stmt);
            int err = Sqlite3.sqlite3_step(_stmt);
            if (err != Sqlite3.SQLITE_ROW)
            {
                _last = true;
            }
            else
            {
                _last = false;
            }
        }

        public void Close()
        {
            if (_stmt != null)
            {
                Sqlite3.sqlite3_finalize(_stmt);
                _stmt = null;
            }
        }

        public IFeatureClass FeatureClass
        {
            get { return _featureClass; }
        }

        internal void open(SqliteFeatureClass featureClass, IFields fields,
            Sqlite3.Vdbe stmt, ISpatialAnalysis SA, ISelectionSet SS)
        {
            _featureClass = featureClass;
            _fields = fields;
            _stmt = stmt;
            _SA = SA;
            _SS = SS;

            Sqlite3.sqlite3_reset(stmt);
            int err = Sqlite3.sqlite3_step(stmt);
            if (err != Sqlite3.SQLITE_ROW)
            {
                _last = true;
            }
            else
            {
                _last = false;
            }
        }

        private IFeature get_feature()
        {
            IFeature fRet = null;
            switch (_featureClass.FeatureType)
            {
                case geoFeatureType.GEO_FEATURE_SIMPLE:
                    {
                        SqliteFeature feature = new SqliteFeature();
                        feature.InitFeatureFields(_featureClass, _fields);
                        feature.InitValue(_stmt);
                        fRet = feature;
                        break;
                    }
                case geoFeatureType.GEO_FEATURE_ANNOTATION:
                    {
                        SqliteTextFeature feature = new SqliteTextFeature();
                        feature.InitFeatureFields(_featureClass, _fields);
                        feature.InitValue(_stmt);                        
                        fRet = feature;
                        break;
                    }
                case geoFeatureType.GEO_FEATURE_RASTER:
                    {
                        SqliteRasterFeature feature = new SqliteRasterFeature();
                        feature.InitFeatureFields(_featureClass, _fields);
                        feature.InitValue(_stmt);                        
                        fRet = feature;
                        break;
                    }
            }
            return fRet;
        }
    }
}
