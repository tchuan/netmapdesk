﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjNet.CoordinateSystems;
using ProjNet.CoordinateSystems.Transformations;
using NetMap.Interfaces.Geometry;

namespace NetMap.Interfaces.Render
{
    /// <summary>
    /// 适合地图控件的坐标转换接口
    /// </summary>
    public interface IDisplayTransformation
    {
        /// <summary>
        /// 获取和设置地图全图范围
        /// </summary>
        IEnvelope MapBounds { get; set; }
        /// <summary>
        /// 获取和设置地图可见的范围
        /// </summary>
        IEnvelope VisibleMapBounds { get; }
        /// <summary>
        /// 获取和设置窗口范围
        /// </summary>
        IEnvelope WindowBounds { get; set; }
        /// <summary>
        /// 获取和设置显示比例
        /// 它的倒数表示：一个像素表示的地图单位数
        /// </summary>
        double ScaleRatio { get; }
        /// <summary>
        /// 缩放到比例尺
        /// </summary>
        /// <param name="scale"></param>
        void ZoomToScale(double scale);
        /// <summary>
        /// 缩放到窗口
        /// </summary>
        /// <param name="window"></param>
        void ZoomToWindow(IEnvelope window);
        /// <summary>
        /// 缩放到数据范围
        /// </summary>
        /// <param name="bounds"></param>
        void ZoomToDataBounds(IEnvelope bounds);
        /// <summary>
        /// 缩放到地图全图范围
        /// </summary>
        void ZoomToMapBounds();
        /// <summary>
        /// 以某比例尺缩放到某点为中心
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="scale"></param>
        void ZoomToCenter(double x, double y, double scale);
        /// <summary>
        /// 坐标转换
        /// </summary>
        ICoordinateTransformation CoordinateTransformation { get; set; }
        /// <summary>
        /// 坐标反转
        /// </summary>
        ICoordinateTransformation ReverseCoordinateTransformation { get; set; }
        /// <summary>
        /// 从地图坐标到窗口坐标
        /// </summary>
        /// <param name="mapX">地图x坐标</param>
        /// <param name="mapY">地图y坐标</param>
        /// <param name="windowX">窗口x坐标</param>
        /// <param name="windowY">窗口y坐标</param>
        void FromMapPoint(double mapX, double mapY, ref double windowX, ref double windowY);
        /// <summary>
        /// 从窗口坐标到地图坐标
        /// </summary>
        /// <param name="windowX">窗口x坐标</param>
        /// <param name="windowY">窗口y坐标</param>
        /// <param name="mapX">地图x坐标</param>
        /// <param name="mapY">地图y坐标</param>
        void ToMapPoint(double windowX, double windowY, ref double mapX, ref double mapY);
        /// <summary>
        /// 从地图坐标到窗口坐标
        /// </summary>
        /// <param name="point">地图点</param>
        /// <returns></returns>
        IPoint FromMapPoint(IPoint point);
        /// <summary>
        /// 从地图坐标到窗口坐标
        /// </summary>
        /// <param name="pts">地图点集</param>
        /// <returns></returns>
        IList<IPoint> FromMapPoint(IList<IPoint> pts);
    }
}
