﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*
 * 定义一些常用的枚举
 */

namespace NetMap.Interfaces.Common
{
    /// <summary>
    /// 相互关系
    /// </summary>
    public enum MutualRelationship
    {
        /// <summary>
        /// 前面
        /// </summary>
        Before = 1,
        /// <summary>
        /// 内部
        /// </summary>
        Inner = 2,
        /// <summary>
        /// 后面
        /// </summary>
        After = 3,
        /// <summary>
        /// 下面
        /// </summary>
        Bottom = 4,
        /// <summary>
        /// 上面
        /// </summary>
        Top = 5
    }

    /// <summary>
    /// 相对方向
    /// </summary>
    public enum RelativeDirection
    {
        /// <summary>
        /// 原点
        /// </summary>
        Origin,
        /// <summary>
        /// 坐边
        /// </summary>
        Left,
        /// <summary>
        /// 前边
        /// </summary>
        Front,
        /// <summary>
        /// 右边
        /// </summary>
        Right,
        /// <summary>
        /// 后面
        /// </summary>
        Behind
    }
}
