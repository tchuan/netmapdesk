﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Community.CsharpSqlite;
using NetMap.Interfaces.Data;
using NetMap.Interfaces.Geometry;

namespace NetMap.Sqlite
{
    /// <summary>
    /// Sqlite数据库注记要素
    /// </summary>
    internal class SqliteTextFeature : SqliteFeature, ITextFeature
    {
        protected bool _textModify = false;     //注记内容改变标志
        protected string _text = string.Empty;  //注记内容

        internal SqliteTextFeature()
        {
        }

        #region ITextFeature 成员

        public string Text
        {
            get
            {
                return _text;
            }
            set
            {
                if (_text == value)
                {
                    return;
                }
                else
                {
                    _text = value;
                    _textModify = true;
                }
            }
        }

        #endregion

        public override geoFeatureType FeatureType
        {
            get { return geoFeatureType.GEO_FEATURE_ANNOTATION; }
        }

        internal override void InitFeatureFields(SqliteFeatureClass featureClass, IFields fields)
        {
            base.InitFeatureFields(featureClass, fields);
            _textModify = false;
        }

        //附加初始化：初始化注记内容
        protected override void additional_init(Sqlite3.Vdbe stmt, int index)
        {
            byte[] data = Sqlite3.sqlite3_column_blob(stmt, index); //注记位置
            _geometry = new NetMap.Geometry.DirectionPoint();
            _geometry.ImportFromWkb(data);
            _text = Sqlite3.sqlite3_column_text(stmt, index + 1);   //注记内容
        }

        protected override void SetGeometry(IGeometry geometry)
        {
            IDirectionPoint point = geometry as IDirectionPoint;
            if (point == null)
            {
                throw new Exception("错误的注记点");
            }
            _geometry = geometry;
        }

        protected override IGeometry GetGeometry()
        {
            return _geometry.CloneGeometry();
        }        
    }
}
