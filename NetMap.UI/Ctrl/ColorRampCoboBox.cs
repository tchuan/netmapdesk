﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetMap.UI.Ctrl
{
    /// <summary>
    /// 颜色带下拉框控件
    /// </summary>
    public partial class ColorRampCoboBox :ComboBox

    {
        public delegate void SelectedIndexChangedHandle(List<Color> colors);
        public event SelectedIndexChangedHandle SelectedChangedEvent;
        private List<Color> _arrColor = null;//当前选中的颜色带
        private int _count;//颜色个数
        public ColorRampCoboBox()
        {
            InitializeComponent();
            Initi();
        }
        private void Initi()
        {
            this.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.DropDownStyle = ComboBoxStyle.DropDown;
            this.DroppedDown = true;
            //this.Items.Clear();
            ////加入两个item
            //ColorRamp cr = new ColorRamp(); cr.Set(Color.FromArgb(0, 0, 255), Color.FromArgb(0, 0, 0), 20);
            //this.Items.Add(cr);
            //cr = new ColorRamp(); cr.Set(Color.FromArgb(255,0,0),Color.FromArgb(0,0,0),20);
            //this.Items.Add(cr);
            this.SelectedIndexChanged += new EventHandler(ColorRampCoboBox_SelectedIndexChanged);
        }
        public int SetCount 
        { 
            set
            {
                _count = value;
                this.Items.Clear();
                ColorRamp cr = new ColorRamp(); cr.Set(Color.FromArgb(0, 0, 255), Color.FromArgb(0, 0, 0), _count);
                this.Items.Add(cr);
                cr = new ColorRamp(); cr.Set(Color.FromArgb(255, 0, 0), Color.FromArgb(0, 0, 0), _count);
                this.Items.Add(cr);
                cr = new ColorRamp(); cr.Set(Color.FromArgb(255, 255, 0), Color.FromArgb(0, 0, 255), _count);
                this.Items.Add(cr);
                cr = new ColorRamp(); cr.Set(Color.FromArgb(255, 255, 255), Color.FromArgb(0, 255, 0), _count);
                this.Items.Add(cr);
            }
        }
        void ColorRampCoboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.SelectedIndex < 0 || this.SelectedIndex > this.Items.Count) return;
            ColorRamp selected = this.SelectedItem as ColorRamp;
            _arrColor = selected.Colors;
            if (SelectedChangedEvent != null && _arrColor != null) SelectedChangedEvent(_arrColor);
        }
        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            //base.OnDrawItem(e);
            if (e.Index >= 0)
            {
                ColorRamp temColorRamp = this.Items[e.Index] as ColorRamp;
                float tem = (float)this.Width / (temColorRamp.Colors.Count < 20 ? temColorRamp.Colors.Count : 20);
                int mod=(int) Math.Floor( (double)temColorRamp.Colors.Count / 20);
                int temspace = 0;
                if (mod <= 1) temspace = 1;
                else temspace = mod;
                float jishu = 0f;
                int index = 0;
                Rectangle rect = e.Bounds;
                rect.Inflate(-1, -1);
                while (jishu < this.Width && index < temColorRamp.Colors.Count)
                {
                    Color temColor = temColorRamp.Colors[index];
                    e.Graphics.FillRectangle(new SolidBrush(temColor), rect.Left+jishu, rect.Top, tem, this.Height);
                    jishu += tem; 
                    index += temspace;
                }
            }
        }
        /// <summary>
        /// 整数转化为对应的RGB值
        /// 整数值小于等于16777215，大于等于0
        /// </summary>
        /// <param name="integer">整数</param>
        /// <returns>返回RGB</returns>
        private Color IntegerToRGB(int integer)
        {
            /* 整数转RGB 
            rgb顾名思义红绿兰. 
            将颜色值除以65536,得到整数就是r 
            然后将余数除以256,得到整数就是g 
            最后的余数就是b 
            */
            try
            {
                if (integer > 16777215 || integer < 0) return Color.Empty;
                int R, G, B;
                R = (int)Math.Truncate((double)integer / 65536);
                integer = integer % 65536;
                G = (int)Math.Truncate((double)integer / 256);
                B = integer % 256;
                Color result = Color.FromArgb(R, G, B);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
