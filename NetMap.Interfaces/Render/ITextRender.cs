﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Symbol;

namespace NetMap.Interfaces.Render
{
    /// <summary>
    /// 注记渲染方案接口
    /// </summary>
    public interface ITextRender : ILayerRender
    {
        /// <summary>
        /// 注记符号
        /// </summary>
        ITextSymbol Symbol { get; set; }
    }
}
