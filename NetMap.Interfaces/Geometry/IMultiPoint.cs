﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Geometry
{
    /// <summary>
    /// 多点接口
    /// </summary>
    public interface IMultiPoint : IGeometry, IList<IPoint>
    {
    }
}
