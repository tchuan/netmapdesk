﻿using NetMap.Interfaces.Tile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

/*
 * modify: 2014-06-30 默认的瓦片
 */

namespace NetMap.Tile
{
    /// <summary>
    /// 假瓦片Provider，完全依靠cache提供瓦片
    /// </summary>
    public class FakeTileProvider : ITileProvider
    {
        private readonly ITileCache<byte[]> _tileCache;
        private byte[] _defaultTile;

        /// <summary>
        /// 构造假瓦片Provider
        /// </summary>
        /// <param name="tileCache">瓦片缓存</param>
        /// <param name="defaultTile">默认瓦片</param>
        public FakeTileProvider(ITileCache<byte[]> tileCache, byte[] defaultTile = null)
        {
            _tileCache = tileCache;
            _defaultTile = defaultTile;
        }

        public byte[] GetTile(TileInfo tileInfo)
        {
            //Thread.Sleep(10); //为界面线程留一点CPU时间
            byte[] tile = _tileCache.Find(tileInfo.Index);
            if (tile == null)
            {
                return _defaultTile;
            }
            else
            {
                return tile;
            }
        }
    }
}
