﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Data
{
    /// <summary>
    /// 数据连接信息
    /// </summary>
    public interface IConnectProperties
    {
        /// <summary>
        /// 获取和设置连接数据库的名称或文件的名字
        /// </summary>
        string Database { get; set; }
        /// <summary>
        /// 获取和设置数据库服务名
        /// </summary>
        string Server { get; set; }
        /// <summary>
        /// 获取和设置数据库用户名
        /// </summary>
        string User { get; set; }
        /// <summary>
        /// 获取和设置数据库用户密码
        /// </summary>
        string Password { get; set; }
    }
}
