﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Data
{
    /// <summary>
    /// 字段类型
    /// </summary>
    public enum geoFieldType
    {
        /// <summary>
        /// Int16
        /// </summary>
        GEO_SMALLINT = 0,
        /// <summary>
        /// Int32
        /// </summary>
        GEO_INT      = 1,
        /// <summary>
        /// Int64
        /// </summary>
        GEO_BIGINT   = 2,
        /// <summary>
        /// Float
        /// </summary>
        GEO_SINGLE   = 3,
        /// <summary>
        /// Double
        /// </summary>
        GEO_DOUBLE   = 4,
        /// <summary>
        /// Text
        /// </summary>
        GEO_STRING   = 5,
        /// <summary>
        /// Datetime
        /// </summary>
        GEO_DATE     = 6,
        /// <summary>
        /// Geometry
        /// </summary>
        GEO_GEOMETRY = 7,
        /// <summary>
        /// Binary
        /// </summary>
        GEO_BLOB     = 8
    }

    /// <summary>
    /// 字段接口
    /// </summary>
    public interface IField
    {
        /// <summary>
        /// 获取和设置是否可空
        /// </summary>
        bool IsNullable { get; set; }
        /// <summary>
        /// 获取和设置名字
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// 获取和设置类型
        /// </summary>
        geoFieldType Type { get; set; }
    }
}
