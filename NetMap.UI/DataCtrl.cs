﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NetMap.Interfaces.Data;
using NetMap.Interfaces;
using NetMap.Render;
using ProjNet.CoordinateSystems.Transformations;
using NetMap.Symbol;
using NetMap.UI.Properties;
using NetMap.Data;

namespace NetMap.UI
{    
    /// <summary>
    /// 数据控件
    /// </summary>
    public partial class DataCtrl : UserControl
    {
        public DataCtrl()
        {
            InitializeComponent();
        }        

        /// <summary>
        /// 设置地图视图
        /// </summary>
        public IMapView MapView
        {
            private get;
            set;
        }       
    }
}
