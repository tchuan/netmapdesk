﻿using NetMap.Interfaces.Fetcher;
using NetMap.Interfaces.Render;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Fetcher
{
    interface ITileFetcher
    {
        event DataChangedEventHandler DataChanged;
        void ViewChanged(IDisplayTransformation trans);
        void AbortFetch();
    }
}
