﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Data;
using Community.CsharpSqlite;
using NetMap.Data;

namespace NetMap.Sqlite
{
    /// <summary>
    /// Sqlite数据库记录集
    /// </summary>
    internal class SqliteRecordset : IRecordset
    {
        protected IFields _fields = new Fields();
        protected bool _bEof = false;     //是否结束
        protected Sqlite3.Vdbe _stmt;

        public IFields Fields
        {
            get { return _fields; }
        }

        public object GetValue(int iField)
        {
            int type = Sqlite3.sqlite3_column_type(_stmt, iField);
            switch (type)
            {
                case Sqlite3.SQLITE_INTEGER:
                    {
                        return Sqlite3.sqlite3_column_int(_stmt, iField);                        
                    }
                case Sqlite3.SQLITE_FLOAT:
                    {
                        return Sqlite3.sqlite3_column_double(_stmt, iField);
                    }
                case Sqlite3.SQLITE_BLOB:
                    {
                        return Sqlite3.sqlite3_column_blob(_stmt, iField);
                    }
                case Sqlite3.SQLITE_TEXT:
                    {
                        return Sqlite3.sqlite3_column_text(_stmt, iField);
                    }
                default:
                    {
                        //throw new NotSupportedException("不支持的字段类型");
                        break;
                    }
            }
            return null;
        }

        public object GetValue(string field)
        {
            int index = _fields.FindField(field);
            if (index < 0)
            {
                return null;
            }
            return GetValue(index);
        }

        public bool IsEOF()
        {
            return _bEof;
        }

        public void Next()
        {
            int err = Sqlite3.sqlite3_step(_stmt);
            if (err == Sqlite3.SQLITE_DONE)
            {
                _bEof = true;
            }
        }

        public void Close()
        {
            Sqlite3.sqlite3_finalize(_stmt);
        }

        internal void Open(Sqlite3.Vdbe stmt)
        {
            _stmt = stmt;

            //初始化字段信息
            int count = Sqlite3.sqlite3_column_count(stmt);
            for (int i = 0; i < count; i++)
            {
                IField field = new Field();
                field.Name = Sqlite3.sqlite3_column_name(stmt, i);
                int type = Sqlite3.sqlite3_column_type(stmt, i);
                switch (type)
                {
                    case Sqlite3.SQLITE_INTEGER:
                    {
                        field.Type = geoFieldType.GEO_INT;
                        break;
                    }
                    case Sqlite3.SQLITE_FLOAT:
                    {
                        field.Type = geoFieldType.GEO_DOUBLE;
                        break;
                    }
                    case Sqlite3.SQLITE_TEXT:
                    {
                        field.Type = geoFieldType.GEO_STRING;
                        break;
                    }
                    case Sqlite3.SQLITE_BLOB:
                    {
                        field.Type = geoFieldType.GEO_BLOB;
                        break;
                    }
                    default:
                    {
                        field.Type = geoFieldType.GEO_STRING;
                        break;
                    }
                }
                _fields.SetField(i, field);
            }

            Next();
        }
    }
}
