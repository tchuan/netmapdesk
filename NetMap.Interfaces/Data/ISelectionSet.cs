﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;

namespace NetMap.Interfaces.Data
{
    /// <summary>
    /// 选择集接口
    /// </summary>
    public interface ISelectionSet : IEnumerable<long>
    {
        /// <summary>
        /// 是否在要素选择集中
        /// </summary>
        /// <param name="oid"></param>
        /// <returns></returns>
        bool IsIn(long oid);
        /// <summary>
        /// 选择集中OID个数
        /// </summary>
        int Count { get; }
        /// <summary>
        /// 添加指定的OID
        /// </summary>
        /// <param name="oid"></param>
        void Add(long oid);
        /// <summary>
        /// 删除指定的OID
        /// </summary>
        /// <param name="oid"></param>
        void Remove(long oid);
        /// <summary>
        /// 清空
        /// </summary>
        void RemoveAll();
        /// <summary>
        /// 增加指定的一组OID
        /// </summary>
        /// <param name="oids"></param>
        void AddList(IEnumerable<long> oids);
        /// <summary>
        /// 删除指定的一组OID
        /// </summary>
        /// <param name="oids"></param>
        void RemoveList(IEnumerable<long> oids);
        /// <summary>
        /// 获取选择集所属的要素集
        /// </summary>
        /// <returns></returns>
        IFeatureClass GetTarget();
        /// <summary>
        /// 在当前选择集范围里查询要素集获取新地物游标
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        IFeatureCursor Search(IQueryFilter filter);
        /// <summary>
        /// 在当前选择集范围里查询要素集获取新的选择集
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        ISelectionSet Select(IQueryFilter filter);
        /// <summary>
        /// 获取选择集的范围
        /// </summary>
        IEnvelope Envelope { get; }
    }
}
