﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace NetMap.UI.Ctrl.Prop
{
    /// <summary>
    /// 单条属性
    /// </summary>
    public class PropItem
    {
        /// <summary>
        /// 所属类别
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 只读性
        /// </summary>
        public bool IsReadOnly { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 值
        /// </summary>
        public object Value { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        public Type Type { get; set; }
        /// <summary>
        /// 显示或隐藏，true为显示
        /// </summary>
        public bool Browsable { get; set; }
        /// <summary>
        /// 类型转换
        /// </summary>
        public TypeConverter Converter { get; set; }
    }
}
