﻿using NetMap.Application.Gui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NetMap.Application.WorkspaceBrowser.TreeNodes
{
    public abstract class AbstractWorkspaceBrowserTreeNode : ExtTreeNode
    {
        string _toolbarAddinTreePath = null;

        public virtual string ToolbarAddinTreePath
        {
            get
            {
                return _toolbarAddinTreePath;
            }
            set
            {
                _toolbarAddinTreePath = value;
            }
        }

        public abstract object AcceptVisitor(WorkspaceBrowserTreeNodeVisitor visitor, object data);

        public virtual object AcceptChildren(WorkspaceBrowserTreeNodeVisitor visitor, object data)
        {
            foreach (TreeNode node in Nodes)
            {
                if (node is AbstractWorkspaceBrowserTreeNode)
                {
                    ((AbstractWorkspaceBrowserTreeNode)node).AcceptVisitor(visitor, data);
                }
            }
            return data;
        }
    }
}
