﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Data;

namespace NetMap.Data
{
    public class ConnectProperties : IConnectProperties, IEquatable<IConnectProperties>
    {
        public ConnectProperties()
        {
            Database = string.Empty;
            Server = string.Empty;
            User = string.Empty;
            Password = string.Empty;
        }

        public string Database
        {
            get;
            set;
        }

        public string Server
        {
            get;
            set;
        }

        public string User
        {
            get;
            set;
        }

        public string Password
        {
            get;
            set;
        }

        public bool Equals(IConnectProperties other)
        {
            return string.Compare(Database, other.Database) == 0
                && string.Compare(Server, other.Server) == 0
                && string.Compare(User, other.User) == 0
                && string.Compare(Password, other.Password) == 0;
        }
    }
}
