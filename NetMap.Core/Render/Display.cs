﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Geometry;
using System.Drawing;

namespace NetMap.Render
{
    public class Display : IDisplay
    {
        public Display(Graphics g, ISymbolRender symbolRender, IEnvelope clipBox, IDisplayTransformation trans)
        {
            G = g;
            SymbolRender = symbolRender;
            ClipEnvelope = clipBox;
            DisplayTransformation = trans;
        }

        #region IDisplay 成员

        public Graphics G
        {
            get;
            protected set;
        }

        public ISymbolRender SymbolRender
        {
            get;
            protected set;
        }

        public IEnvelope ClipEnvelope
        {
            get;
            protected set;
        }

        public IDisplayTransformation DisplayTransformation
        {
            get;
            protected set;
        }

        #endregion
    }
}
