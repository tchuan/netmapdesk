﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Geometry
{
    /// <summary>
    /// 多边形接口
    /// </summary>
    public interface IPolygon : IGeometry
    {
        /// <summary>
        /// 外圈
        /// </summary>
        ILineString ExteriorRing { get; }
        /// <summary>
        /// 内圈
        /// </summary>
        List<ILineString> InteriorRings { get; }
        /// <summary>
        /// 获取第n个内圈
        /// </summary>
        /// <param name="n">[0, count)</param>
        /// <returns></returns>
        ILineString GetInteriorRingN(int n);
    }
}
