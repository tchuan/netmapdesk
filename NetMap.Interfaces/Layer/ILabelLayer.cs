﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Symbol;

namespace NetMap.Interfaces
{
    /// <summary>
    /// 可标注的图层接口
    /// </summary>
    public interface ILabelLayer
    {
        /// <summary>
        /// 是否标注
        /// </summary>
        bool IsLabel { get; set; }
        /// <summary>
        /// 标注字段
        /// </summary>
        string LabelField { get; set; }
        /// <summary>
        /// 标注的符号
        /// </summary>
        ITextSymbol LabelSymbol { get; set; }
    }
}
