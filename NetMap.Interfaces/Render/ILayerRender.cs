﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Render
{
    /// <summary>
    /// 图层样式
    /// </summary>
    public interface ILayerRender
    {
        /// <summary>
        /// 获取和设置名字
        /// </summary>
        string Name { get; set; }
    }
}
