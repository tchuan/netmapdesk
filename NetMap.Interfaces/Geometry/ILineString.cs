﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Geometry
{
    /// <summary>
    /// 多段线接口
    /// </summary>
    public interface ILineString : IGeometry, IList<IPoint>
    {
        /// <summary>
        /// 起点
        /// </summary>
        IPoint StartPoint { get; }
        /// <summary>
        /// 终点
        /// </summary>
        IPoint EndPoint { get; }
    }
}
