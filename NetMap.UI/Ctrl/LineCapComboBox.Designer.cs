﻿namespace NetMap.UI.Ctrl
{
    partial class LineCapComboBox
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxInner = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // comboBoxInner
            // 
            this.comboBoxInner.FormattingEnabled = true;
            this.comboBoxInner.Location = new System.Drawing.Point(0, 1);
            this.comboBoxInner.Name = "comboBoxInner";
            this.comboBoxInner.Size = new System.Drawing.Size(120, 20);
            this.comboBoxInner.TabIndex = 0;
            // 
            // LineCapComboBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.comboBoxInner);
            this.Name = "LineCapComboBox";
            this.Size = new System.Drawing.Size(120, 24);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxInner;
    }
}
