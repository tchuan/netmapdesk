﻿namespace StoneGround.Search
{
    partial class SearchControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.treeViewAdvGrounds = new Aga.Controls.Tree.TreeViewAdv();
            this.SuspendLayout();
            // 
            // treeViewAdvGrounds
            // 
            this.treeViewAdvGrounds.BackColor = System.Drawing.SystemColors.Window;
            this.treeViewAdvGrounds.DefaultToolTipProvider = null;
            this.treeViewAdvGrounds.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewAdvGrounds.DragDropMarkColor = System.Drawing.Color.Black;
            this.treeViewAdvGrounds.FirstVisibleRow = 0;
            this.treeViewAdvGrounds.LineColor = System.Drawing.SystemColors.ControlDark;
            this.treeViewAdvGrounds.Location = new System.Drawing.Point(0, 0);
            this.treeViewAdvGrounds.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.treeViewAdvGrounds.Model = null;
            this.treeViewAdvGrounds.Name = "treeViewAdvGrounds";
            this.treeViewAdvGrounds.SelectedNode = null;
            this.treeViewAdvGrounds.Size = new System.Drawing.Size(247, 379);
            this.treeViewAdvGrounds.TabIndex = 1;
            this.treeViewAdvGrounds.Text = "treeViewAdv1";
            // 
            // SearchControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.treeViewAdvGrounds);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "SearchControl";
            this.Size = new System.Drawing.Size(247, 379);
            this.ResumeLayout(false);

        }

        #endregion

        private Aga.Controls.Tree.TreeViewAdv treeViewAdvGrounds;

    }
}
