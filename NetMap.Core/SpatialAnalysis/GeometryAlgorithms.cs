﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;

namespace NetMap.SpatialAnalysis
{
    /// <summary>
    /// 图形算法
    /// </summary>
    public class GeometryAlgorithms
    {
        private static GeometryAlgorithms _instance = null;
        /// <summary>
        /// 实例
        /// </summary>
        public static GeometryAlgorithms Instance
        {
            get 
            {
                if (_instance == null)
                {
                    _instance = new GeometryAlgorithms();
                }
                return _instance;
            }
        }

        private GeometryAlgorithms()
        {
        }

        /// <summary>
        /// 测试p1、p2定义的框与q1、q2定义的框相交
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="q1"></param>
        /// <param name="q2"></param>
        /// <returns></returns>
        public bool EnvelopeIntersects(IPoint p1, IPoint p2, IPoint q1, IPoint q2)
        {
            double minq = Math.Min(q1.X, q2.X);
            double maxq = Math.Max(q1.X, q2.X);
            double minp = Math.Min(p1.X, p2.X);
            double maxp = Math.Max(p1.X, p2.X);
            if (minp > maxq) return false;
            if (maxp < minq) return false;

            minq = Math.Min(q1.Y, q2.Y);
            maxq = Math.Max(q1.Y, q2.Y);
            minp = Math.Min(p1.Y, p2.Y);
            maxp = Math.Min(p1.Y, p2.Y);
            if (minp > maxq) return false;
            if (maxp < minq) return false;

            return true;
        }

        /// <summary>
        /// 测试q是否与p1、p2定义的框相交
        /// </summary>
        /// <returns></returns>
        public bool PointIntersectsEnvelope(IPoint p1, IPoint p2, IPoint q)
        {
            return (q.X >= (p1.X < p2.X ? p1.X : p2.X)) && (q.X <= (p1.X > p2.X ? p1.X : p2.X))
                && (q.Y >= (p1.Y < p2.Y ? p1.Y : p2.Y)) && (q.Y <= (p1.Y > p2.Y ? p1.Y : p2.Y));
        }

        ///// <summary>
        ///// 测试点是否在环内
        ///// ring方向任意
        ///// 如果点在线的边上，结果将是未定义的
        ///// 这个算法没有首先检查点是否在Envelope内
        ///// 射线法
        ///// </summary>
        ///// <param name="p">点</param>
        ///// <param name="ring">环</param>
        ///// <returns></returns>
        //public bool IsPointInRing(IPoint p, ILineString ring)
        //{
        //    int i;
        //    int i1;            // point index  i1 = i - 1
        //    double xInt;       // x intersection of segment with ray
        //    int crossings = 0; // 与射线相交的线段数
        //    double x1, y1, x2, y2; // translated coordinates
        //    int nPts = ring.Count;
        //    IPoint p1, p2;

        //    //for each segment l = (i-1, i), see if it crosses ray from test point in positive x direction.
        //    for (i = 1; i < nPts; i++)
        //    {
        //        i1 = i - 1;
        //        p1 = ring[i];
        //        p2 = ring[i1];                
        //        x1 = p1.X - p.X;
        //        y1 = p1.Y - p.Y;
        //        x2 = p2.X - p.X;
        //        y2 = p2.Y - p.Y;

        //        if (((y1 > 0) && (y2 <= 0)) || ((y2 > 0) && (y1 <= 0)))
        //        {
        //            /*
        //            *  segment straddles x axis, so compute intersection.
        //            */
        //            xInt = RobustDeterminant.SignOfDet2x2(x1, y1, x2, y2) / (y2 - y1);

        //            /*
        //            *  crosses ray if strictly positive intersection.
        //            */
        //            if (0.0 < xInt)
        //                crossings++;
        //        }
        //    }
        //}
    }
}
