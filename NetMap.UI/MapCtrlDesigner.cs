﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms.Design;
using System.Windows.Forms;
using System.Drawing;
using NetMap.Interfaces;

namespace NetMap.UI
{
    public class MapCtrlDesigner : ControlDesigner
    {
        protected override void OnPaintAdornments(PaintEventArgs pe)
        {
            //IMapView view = Control as IMapView;
            pe.Graphics.Clear(Color.Black);
            pe.Graphics.DrawString("NetMap MapCtrl 2.0, Copyrights 2013-2015 facemap", Control.Font, Brushes.White, 0, 0);
        }
    }
}
