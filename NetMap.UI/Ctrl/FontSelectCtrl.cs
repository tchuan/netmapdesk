﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace NetMap.UI.Ctrl
{
    /// <summary>
    /// 字体字符选择事件处理
    /// </summary>
    /// <param name="ff">字体</param>
    /// <param name="chr">字符</param>
    public delegate void FontSelectedEventHandler(FontFamily ff, char chr);

    /// <summary>
    /// 符号字体选择符号控件
    /// </summary>
    public class FontSelectCtrl : Control
    {
        private Image _imgChr = new Bitmap(22, 22);
        private StringFormat _sf = new StringFormat();
        private Brush _fontBrush = Brushes.Black;
        private FontSelectForm _frm = new FontSelectForm();
        private FontFamily _ff = new FontFamily("黑体");
        private char _chr = 'a';
        /// <summary>
        /// 字体字符选择事件
        /// </summary>
        public event FontSelectedEventHandler FontSelected;

        public FontSelectCtrl()
        {
            _sf = new StringFormat();
            _sf.Alignment = StringAlignment.Center;
            _sf.LineAlignment = StringAlignment.Center;
            SuspendLayout();
            Size = new System.Drawing.Size(22, 22);
            BackColor = Color.White;
            frm_FontSelected(_ff, _chr);    
            _frm.UpdateFontFamily(_ff);
            ResumeLayout();
            _frm.FontSelected += new FontSelectedHandler(frm_FontSelected);
        }

        public FontFamily SelectedFontFamily
        {
            get
            {
                return _ff;
            }
            set
            {
                _ff = value;
                //更新
                frm_FontSelected(_ff, _chr);
                _frm.UpdateFontFamily(_ff);
            }
        }

        public char SelectedChar
        {
            get
            {
                return _chr;
            }
            set
            {
                _chr = value;
                //更新
                frm_FontSelected(_ff, _chr);
                _frm.UpdateFontFamily(_ff);
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            //base.OnPaint(e);
            e.Graphics.DrawImage(_imgChr, 0, 0);
        }

        protected override void OnClick(EventArgs e)
        {
            _frm.Show(this);
            base.OnClick(e);
        }

        private void DrawChrs(FontFamily ff, char chr)
        {
            using (Graphics g = Graphics.FromImage(_imgChr))
            {
                g.Clear(BackColor);
                using (System.Drawing.Font font = new Font(ff, 15))
                {
                    g.DrawString(new string(chr, 1), font, _fontBrush, 11, 11, _sf);
                }
            }
        }

        void frm_FontSelected(FontFamily ff, char chr)
        {
            // 更新信息
            _ff = ff; 
            _chr = chr;
            // 更新图片
            DrawChrs(ff, chr);
            Refresh();
            // 触发事件
            if (FontSelected != null)
            {
                FontSelected(_ff, chr);
            }
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            Width = 22;
            Height = 22;
        }
    }
}
