﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NetMap.Interfaces.Data;

namespace NetMap.UI.Tools
{
    public partial class AttributeForm : Form
    {
        private IFeature _feature;

        public AttributeForm()
        {
            InitializeComponent();

            int width = listView1.Width;
            int fieldWidth = (int)(0.5 * width);
            this.listView1.Columns.Add("字段", fieldWidth);
            this.listView1.Columns.Add("值", width - fieldWidth);
        }

        public void Init(IFeature feature)
        {
            _feature = feature;

            IFields fields = feature.Fields;
            int count = fields.Count;
            for (int i = 0; i < count; i++)
            {
                IField field = fields.GetField(i);
                object value = feature.GetValue(i);
                string formatValue = (value == null ? "null" : value.ToString());
                ListViewItem item = new ListViewItem(new string[] { field.Name, formatValue });
                listView1.Items.Add(item);
            }
        }
    }
}
