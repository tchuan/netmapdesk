﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StoneGround.Dialog
{
    public partial class FrmPictureView : Form
    {
        private Image _img = null;
        private string _imgname = string.Empty;
        private float _newHeight = 600;
        private float _newWidth = 800;

        public FrmPictureView()
        {
            InitializeComponent();

            this.Click += new EventHandler(FrmPictureView_Click);
        }

        void FrmPictureView_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void InitPictureView(Image img, string name)
        {
            _img = img;
            float width = Width;
            float height = Height;
            float scale = Math.Min(width / img.Width, height / img.Height);
            _newWidth = scale * img.Width;
            _newHeight = scale * img.Height;
            _imgname = name;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            e.Graphics.DrawImage(_img, new RectangleF(0.5f * (Width - _newWidth), 0.5f * (Height - _newHeight), _newWidth, _newHeight),
                new RectangleF(0, 0, _img.Width, _img.Height), GraphicsUnit.Pixel);
            using (Font font = new Font("微软雅黑", 20))
            {
                using (SolidBrush brush = new SolidBrush(Color.DarkBlue))
                {
                    e.Graphics.DrawString(_imgname, font, brush, 20, 20);
                }
            }
        }
    }
}
