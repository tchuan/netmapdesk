﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Tracker;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Symbol;
using NetMap.Interfaces.Geometry;
using System.Drawing;
using NetMap.Render;

namespace NetMap.Tracker
{
    /// <summary>
    /// 橡皮条基类
    /// </summary>
    public abstract class Tracker : ITracker
    {
        protected bool _bstart = false;

        public Tracker()
        {
        }

        public Tracker(ISymbol symbol)
        {
            Symbol = symbol;
        }

        #region ITracker 成员

        public ISymbol Symbol
        {
            get;
            set;
        }

        public virtual void Reset()
        {
            _bstart = false;
        }

        public abstract void Draw(Graphics g);
        public abstract void Draw(IDisplay display);

        public bool IsStarted
        {
            get
            {
                return _bstart;
            }
        }

        public virtual IGeometry GetGeometry()
        {
            return null;
        }

        public virtual void Start(IPoint point)        
        {
            _bstart = true;
        }

        public virtual void MoveTo(IPoint point)
        {
            //do nothing
        }

        public virtual void Finish()
        {
            _bstart = false;
        }

        #endregion
    }
}
