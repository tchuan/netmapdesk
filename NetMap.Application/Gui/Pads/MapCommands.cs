﻿using NetMap.Application.Core;
using NetMap.Interfaces;
using NetMap.Tools;
using StoneGround;

namespace NetMap.Application.Gui.Pads
{
    public abstract class MapCommand : AbstractMenuCommand
    {
        public NetMap.Interfaces.ICommand InnerCmd { get; set; }

        private bool bFirst = true;

        public override void Run()
        {
            if (InnerCmd != null)
            {
                if (bFirst)
                {
                    FirstInit();
                    bFirst = false;
                }
                if (InnerCmd is ITool)
                {
                    ITool tool = InnerCmd as ITool;
                    (WorkspaceSingleton.ActiveMapView as IMapEditView).SetCurrentTool(tool);
                }
                else
                {
                    InnerCmd.Execute();
                }

                if (InnerCmd.Name != "道路周围料场分析")
                    ClearRoad();
            }
        }

        private void ClearRoad()
        {
            IMapView view = WorkspaceSingleton.ActiveMapView;
            ILayer layer = view.MapDocument.FindLayer(StoneGroundDefines.LAYER_NAME_ROAD);
            IFeatureSelection selection = layer as IFeatureSelection;
            selection.Clear();
            view.Invalidate();
        }

        protected virtual void FirstInit()
        {
            //初始化
            InnerCmd.View = WorkspaceSingleton.ActiveMapView;
            if (InnerCmd is ITool)
            {
                ITool tool = InnerCmd as ITool;
                tool.EditView = WorkspaceSingleton.ActiveMapView as IMapEditView;
            }
            InnerCmd.Init();
        }
    }

    /// <summary>
    /// 全图
    /// </summary>
    public class ZoomAllCommand : MapCommand
    {
        public ZoomAllCommand()
        {
            InnerCmd = new ZoomExtent();
        }
    }

    /// <summary>
    /// 自由查看
    /// </summary>
    public class FreeViewCommand : MapCommand
    {
        public FreeViewCommand()
        {
            InnerCmd = new FreeView();
        }
    }
}
