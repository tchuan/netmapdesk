﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Data
{
    /// <summary>
    /// 字段集合
    /// </summary>
    public interface IFields
    {
        /// <summary>
        /// 字段数
        /// </summary>
        int Count { get; }
        /// <summary>
        /// 获取字段
        /// </summary>
        /// <param name="index">字段索引，从0开始</param>
        /// <returns>字段</returns>
        IField GetField(int index);
        /// <summary>
        /// 设置字段
        /// </summary>
        /// <param name="index">字段索引，从0开始</param>
        /// <param name="field">字段信息</param>
        /// <returns>字段</returns>
        void SetField(int index, IField field);
        /// <summary>
        /// 通过字段名查找字段索引号
        /// </summary>
        /// <param name="name">字段名</param>
        /// <returns>字段索引号，-1表示未找到</returns>
        int FindField(string name);
    }
}
