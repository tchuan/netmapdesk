﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Application.Core;

namespace StoneGround.Search.Commands
{
    /// <summary>
    /// 清空料场列表
    /// </summary>
    public class ClearTreeNodes : AbstractMenuCommand
    {
        public override void Run()
        {
            SearchPad search = SearchPad.Instance;
            search.ClearSearchTreeNodes();            
        }
    }
}
