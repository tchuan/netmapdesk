﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap;
using NetMap.Interfaces.Symbol;
using NetMap.Interfaces;
using System.Windows.Forms;
using System.Drawing;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Data;
using NetMap.Data;
using NetMap.Geometry;
using NetMap.Interfaces.Geometry;
using NetMap.Render;

namespace StoneGround.Edit
{
    /// <summary>
    /// 移动料场
    /// </summary>
    public class StoneGroundMoveTool : Tool
    {
        private ISymbol _symbol;
        private IFeature _tempFeature;
        private IPoint _point;

        public StoneGroundMoveTool()
            : base("移动料场")
        {
        }

        public StoneGroundMoveTool(IMapEditView view)
            : base("移动料场", view)
        {
        }

        public override void Init()
        {
            base.Init();
            _symbol = StoneGroundDatabaseService.Instance.Symbol;
            _point = new NetMap.Geometry.Point(0, 0);
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            //base.OnMouseDown(e);
            if (e.Button == MouseButtons.Left)
            {
                double tolerance = 10;
                double x1 = 0, y1 = 0, x2 = 0, y2 = 0;
                IDisplayTransformation displayTranformation = View.DisplayTransformation;
                displayTranformation.ToMapPoint(e.X - tolerance, e.Y - tolerance, ref x1, ref y1);
                displayTranformation.ToMapPoint(e.X + tolerance, e.Y + tolerance, ref x2, ref y2);
                ISpatialFilter filter = new SpatialFilter();
                filter.Geometry = new Envelope(x1, y1, x2, y2);
                filter.SpatialRelationship = geoSpatialRelationship.GEO_SPATIALRELATIONSHIP_ENVELOPEINTERSECTS;
                IFeatureClass featureClass = StoneGroundDatabaseService.Instance.StoneGroundFeatureClass;
                IFeatureCursor cursor = featureClass.Search(filter);
                _tempFeature = cursor.Next();
                if (_tempFeature != null)
                {
                    _point.X = e.X;
                    _point.Y = e.Y;
                }
            }
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            //base.OnMouseMove(e);
            if (e.Button == MouseButtons.Left)
            {
                if (_tempFeature != null)
                {
                    _point.X = e.X;
                    _point.Y = e.Y;
                    View.Refresh();
                }
            }
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            //base.OnMouseUp(e);
            if (e.Button == MouseButtons.Left)
            {
                if (_tempFeature != null)
                {
                    double x = 0, y = 0;
                    IDisplayTransformation displayTranformation = View.DisplayTransformation;
                    displayTranformation.ToMapPoint(e.X, e.Y, ref x, ref y);
                    _tempFeature.Geometry = new NetMap.Geometry.Point(x, y);
                    _tempFeature.Store();
                    _tempFeature = null;
                    View.Invalidate();
                }
            }
        }

        public override void Draw(Graphics g)
        {
            //base.Draw(g);
            if (_tempFeature != null)
            {
                ISymbolDrawer drawer = SymbolDrawerFactory.Instance.GetDrawer(_symbol);
                if (drawer != null)
                {                    
                    drawer.Draw(g, _symbol, _point, null);
                }
            }
        }
    }
}
