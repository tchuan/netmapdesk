﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Symbol;

namespace NetMap.Interfaces
{
    /// <summary>
    /// 工作空间接口
    /// </summary>
    public interface IWorkspace
    {
        /// <summary>
        /// 名字
        /// </summary>
        string Name { get; set; }        
        /// <summary>
        /// 设置和获取数据源
        /// </summary>
        ISpatialDataSource DataSource { get; set; }
        /// <summary>
        /// 符号库
        /// </summary>
        ISymbolLibrary SymbolLibrary { get; set; }
        /// <summary>
        /// 获取和设置地图集
        /// </summary>
        IMapDocumentCollection Maps { get; set; }
        /// <summary>
        /// 获取和设置地图视图集
        /// </summary>
        IMapViewCollection MapViews { get; set; }        
    }
}
