﻿using NetMap.Interfaces.Data;
using NetMap.Interfaces.Tile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Tile.Cache
{
    /// <summary>
    /// 基于NetMap数据模型的瓦片缓存    
    /// </summary>
    public class DatabaseCache : ITileCache<byte[]>
    {
        private IDatabase _database = null;
        private ITable _table = null;
        private IFields _fields = null;
        private string _levelFieldName = "";
        private string _rowFieldName = "";
        private string _colFieldName = "";
        private string _imageFieldName = "";
        private string _timeFieldName = "";
        private readonly object _lockObj = new object();

        public DatabaseCache(ITable table, string levelFieldName,
            string rowFieldName, string colFieldName, 
            string imageFieldName, string timeFieldName)
        {
            _table = table;
            _database = table.Database;
            _fields = table.Fields;
            _levelFieldName = levelFieldName;
            _rowFieldName = rowFieldName;
            _colFieldName = colFieldName;
            _imageFieldName = imageFieldName;
            _timeFieldName = timeFieldName;
        }

        #region ITileCache 成员

        public void Add(TileIndex index, byte[] tile)
        {
            lock (_lockObj)
            {
                IRow row = _table.CreateRow();
                int findex = _fields.FindField(_levelFieldName);
                row.SetValue(findex, index.Level);
                findex = _fields.FindField(_rowFieldName);
                row.SetValue(findex, index.Row);
                findex = _fields.FindField(_colFieldName);
                row.SetValue(findex, index.Col);
                findex = _fields.FindField(_imageFieldName);
                row.SetValue(findex, tile);
                findex = _fields.FindField(_timeFieldName);
                row.SetValue(findex, DateTime.Now);
                row.Store();
            }
        }

        public void Remove(TileIndex index)
        {
            lock (_lockObj)
            {
                string sql = string.Format("delete from {0} where {0}='{1}' and {2}={3} and {4}={5}",
                    _table.Name,
                    _levelFieldName, index.Level,
                    _rowFieldName, index.Row,
                    _colFieldName, index.Col);
                _database.ExecuteSQL(sql);
            }
        }

        public byte[] Find(TileIndex index)
        {
            lock (_lockObj)
            {
                string sql = string.Format("select {0} from {1} where {2}='{3}' and {4}={5} and {6}={7}",
                    _imageFieldName, _table.Name,
                    _levelFieldName, index.Level,
                    _rowFieldName, index.Row,
                    _colFieldName, index.Col);
                return _database.ExecuteAndGetBytes(sql);
            }
        }        

        #endregion

        /// <summary>
        /// 开始事务
        /// </summary>
        public void StartTransaction()
        {
            lock (_lockObj)
            {
                _database.StartTransaction();
            }
        }

        /// <summary>
        /// 提交事务
        /// </summary>
        public void CommitTransaction()
        {
            lock (_lockObj)
            {
                _database.CommitTransaction();
            }
        }

        /// <summary>
        /// 提交上次事务并开始新的事务
        /// </summary>
        public void CommitAndStartTransaction()
        {
            lock (_lockObj)
            {
                _database.StartTransaction();
                _database.CommitTransaction();
            }
        }
    }
}
