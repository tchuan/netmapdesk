﻿using System;

namespace NetMap.Sqlite
{
    /// <summary>
    /// 记录状态
    /// </summary>
    internal enum RowState
    {
        /// <summary>
        /// 新纪录
        /// </summary>
        geoRSNew = 0,
        /// <summary>
        /// 已存在的记录
        /// </summary>
        geoRSExist = 1,
        /// <summary>
        /// 已删除的记录
        /// </summary>
        geoRSDelete = 2
    }

    /// <summary>
    /// 空间数据库的定义
    /// </summary>
    internal class SpatialDbDefine
    {
        #region 特殊字段名字定义

        /// <summary>
        /// 行ID
        /// </summary>
        public const string FIELD_OID = "OID";
        /// <summary>
        /// 图形
        /// </summary>
        public const string FIELD_GEOMETRY = "GEOMETRY";
        /// <summary>
        /// 注记内容
        /// </summary>
        public const string FIELD_TEXT = "TEXT";
        /// <summary>
        /// 栅格
        /// </summary>
        public const string FIELD_RASTER = "RASTER";
        /// <summary>
        /// 范围
        /// </summary>
        public const string FIELD_ENVELOPE = "ENVELOPE";

        #endregion

        #region 元数据表

        /// <summary>
        /// 元数据表名
        /// </summary>
        public const string TABLE_METADATA = "geo_metadata";
        /// <summary>
        /// 表名
        /// </summary>
        public const string FIELD_TABLENAME = "TABLENAME";
        /// <summary>
        /// 要素类型
        /// </summary>
        public const string FIELD_FEATURETYPE = "FEATURETYPE";
        /// <summary>
        /// 图形类型
        /// </summary>
        public const string FIELD_GEOMETRYTYPE = "GEOMETRYTYPE";
        /// <summary>
        /// 空间范围
        /// </summary>
        //public const string FIELD_ENVELOPE = "ENVELOPE";
        /// <summary>
        /// 坐标系统
        /// </summary>
        public const string FIELD_COORDINATESSYSTEM = "COORDINATESSYSTEM";

        #endregion

        #region 错误代码

        public const int GIS_DBERR_SQLSYNTAX = 8200; //SQL语法错误
        public const int GIS_DBERR_NOMODIFY = 8201;  //记录未改动
        public const int GIS_DBERR_NOTFIND = 8202;   //未找到
        public const int GIS_DBERR_EMPTYGEOMETRY = 8203;  //空图形
        public const int GIS_DBERR_EMPTRYENVELOPE = 8204; //空范围
        public const int GIS_DBERR_EMPTRYVALUE = 8205;    //空属性

        #endregion 

        #region 加密Key

        //LocalDatabase的md5 40D55198037235B483CBB7640F2139F7
        //LocalSpatialDatabase的md5 1341F22E78D3DC3E69AEAFBA2942B750
        public const string ENCRYPTION_CODE = "0x40D55198037235B483CBB7640F2139F7";
        public const string ENCRYPTION_CODE_SPATIAL = "0x1341F22E78D3DC3E69AEAFBA2942B750";

        #endregion
    }
}
