﻿using NetMap.Interfaces.Persist;
using NetMap.Interfaces.Render;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace NetMap.Render
{
    public static class RenderXmlUtil
    {
        public static IFeatureRender LoadFeatureRender(XmlReader reader, string endElement, object context)
        {
            IFeatureRender render = null;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    switch (reader.Name)
                    {
                        case "SimpleRender":
                            {
                                render = new SimpleRender();
                                break;
                            }
                        case "UniqueValueRender":
                            {
                                render = new UniqueValueRender();
                                break;
                            }
                        default:
                            {
                                throw new NotSupportedException("不支持的图层方案");
                            }
                    }
                    IPersistResource renderPR = render as IPersistResource;
                    renderPR.Load(reader, context);
                    break;
                }
                else if (reader.NodeType == XmlNodeType.EndElement
                    && reader.Name == endElement)
                {
                    break;
                }
            }
            return render;
        }
    }
}
