﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NetMap.Core;
using NetMap.Interfaces.Geometry;

namespace NetMap.Geometry
{
    /// <summary>
    /// 框
    /// </summary>
    public class Envelope : Geometry, IEnvelope
    {
        private double _minx = 0;
        private double _miny = 0;
        private double _maxx = 0;
        private double _maxy = 0;

        public Envelope()
        {
        }

        public Envelope(IPoint point)
        {
            _minx = point.X;
            _miny = point.Y;
            _maxx = point.X;
            _maxy = point.Y;
        }

        public Envelope(IPoint p1, IPoint p2)
        {
            _minx = Math.Min(p1.X, p2.X);
            _miny = Math.Min(p1.Y, p2.Y);
            _maxx = Math.Max(p1.X, p2.X);
            _maxy = Math.Max(p1.Y, p2.Y);
        }

        public Envelope(double x1, double y1, double x2, double y2)
        {
            _minx = Math.Min(x1, x2);
            _miny = Math.Min(y1, y2);
            _maxx = Math.Max(x1, x2);
            _maxy = Math.Max(y1, y2);
        }

        public Envelope(IEnvelope other)
        {
            _minx = other.XMin;
            _miny = other.YMin;
            _maxx = other.XMax;
            _maxy = other.YMax;
        }

        #region IEnvelope 成员

        public double XMax
        {
            get
            {
                return _maxx;
            }
            set
            {
                _maxx = value;
            }
        }

        public double YMax
        {
            get
            {
                return _maxy;
            }
            set
            {
                _maxy = value;
            }
        }

        public double XMin
        {
            get
            {
                return _minx;
            }
            set
            {
                _minx = value;
            }
        }

        public double YMin
        {
            get
            {
                return _miny;
            }
            set
            {
                _miny = value;
            }
        }

        public double Height
        {
            get { return _maxy - _miny; }
        }

        public double Width
        {
            get { return _maxx - _minx; }
        }

        public IPoint Center
        {
            get
            {
                return new Point(0.5 * (_minx + _maxx), 0.5 * (_miny + _maxy));
            }
        }

        public IPoint MinxMiny         
        {
            get
            {
                return new Point(_minx, _miny);
            }
        }
        public IPoint MinxMaxy 
        {
            get
            {
                return new Point(_minx, _maxy);
            }
        }
        public IPoint MaxxMiny
        {
            get
            {
                return new Point(_maxx, _miny);
            }
        }
        public IPoint MaxyMaxy 
        {
            get
            {
                return new Point(_maxx, _maxy);
            }
        }

        public IPoint MinxCenter 
        {
            get
            {
                return new Point(_minx, 0.5 * (_miny + _maxy));
            }
        }

        public IPoint MinyCenter 
        {
            get
            {
                return new Point(0.5 * (_minx + _maxx), _miny);
            }
        }

        public IPoint MaxxCenter 
        {
            get
            {
                return new Point(_maxx, 0.5 * (_miny + _maxy));
            }
        }

        public IPoint MaxyCenter 
        {
            get
            {
                return new Point(0.5 * (_minx + _miny), _maxy);
            }
        }

        public bool IsContain(IEnvelope other)
        {
            return (_minx <= other.XMin && _miny <= other.YMin && _maxx >= other.XMax && _maxy >= other.YMax);
        }

        public bool IsContain(IPoint point)
        {
            return (point.X >= _minx && point.X <= _maxx && point.Y >= _miny && point.Y <= _maxy);
        }

        public bool IsIntersect(IEnvelope other)
        {
            return !(_minx >= other.XMax || _maxx <= other.XMin || _miny >= other.YMax || _maxy <= other.YMin);
        }

        public void Union(IEnvelope other)
        {
            _minx = Math.Min(_minx, other.XMin);
            _miny = Math.Min(_miny, other.YMin);
            _maxx = Math.Max(_maxx, other.XMax);
            _maxy = Math.Max(_maxy, other.YMax);
        }

        public void Union(IPoint point)
        {
            _minx = Math.Min(_minx, point.X);
            _miny = Math.Min(_miny, point.Y);
            _maxx = Math.Max(_maxx, point.X);
            _maxy = Math.Max(_maxy, point.Y);
        }

        public void Update(IEnvelope other)
        {
            _minx = other.XMin;
            _maxx = other.XMax;
            _miny = other.YMin;
            _maxy = other.YMax;
        }

        public void Update(double minx, double miny, double maxx, double maxy)
        {
            _minx = minx;
            _maxx = maxx;
            _miny = miny;
            _maxy = maxy;
        }

        public void ChangeCenter(double x, double y)
        {
            double offX = x - 0.5 * (_minx + _maxx);
            double offY = y - 0.5 * (_miny + _maxy);
            _minx += offX;
            _maxx += offX;
            _miny += offY;
            _maxy += offY;
        }

        #endregion

        public override geoGeometryType GeometryType
        {
            get { return geoGeometryType.GEO_GEOMETRY_RECT; }
        }

        public override void UpdateEnvelope()
        {
            if (_box == null)
            {
                _box = new Envelope(this);
            }
            else
            {
                _box.Update(this);
            }
        }
        
        public override IGeometry CloneGeometry()
        {
            IEnvelope geometry = new Envelope(_minx, _miny, _maxx, _maxy);
            return geometry as IGeometry;
        }

        public override byte[] ExportToWkb()
        {
            Stream stream = new MemoryStream(32);
            BinaryWriter writer = new BinaryWriter(stream);
            writer.Write(_minx);
            writer.Write(_miny);
            writer.Write(_maxx);
            writer.Write(_maxy);
            byte[] bytes = new byte[stream.Length];
            stream.Seek(0, SeekOrigin.Begin);
            stream.Read(bytes, 0, bytes.Length);
            stream.Close();
            return bytes;
        }

        public override string ExportToWkt()
        {
            throw new NotImplementedException();
        }

        public override void ImportFromWkb(byte[] data)
        {
            Stream stream = new MemoryStream(data);
            BinaryReader reader = new BinaryReader(stream);
            _minx = reader.ReadDouble();
            _miny = reader.ReadDouble();
            _maxx = reader.ReadDouble();
            _maxy = reader.ReadDouble();
            reader.Close();
            stream.Close();
        }

        public override void ImportFromWkt(string data)
        {
            //TODO
        }

        // xmin ymin xmax ymax
        public override void Export(BinaryWriter writer)
        {
            writer.Write(XMin);
            writer.Write(YMin);
            writer.Write(XMax);
            writer.Write(YMax);
        }

        public override void Import(BinaryReader reader)
        {
            XMin = reader.ReadDouble();
            YMin = reader.ReadDouble();
            XMax = reader.ReadDouble();
            YMax = reader.ReadDouble();
        }

        public override string ToString()
        {
            return string.Format("{0},{1},{2},{3}", _minx, _miny, _maxx, _maxy);
        }

        /// <summary>
        /// 求2个范围的交集
        /// </summary>
        /// <param name="box1"></param>
        /// <param name="box2"></param>
        /// <returns></returns>
        public static IEnvelope Interect(IEnvelope box1, IEnvelope box2)
        {
            return new Envelope(Math.Max(box1.XMin, box2.XMin),
                Math.Max(box1.YMin, box2.YMin),
                Math.Max(box1.XMax, box2.XMax),
                Math.Max(box1.YMax, box2.YMax));
        }
    }
}
