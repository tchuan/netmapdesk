﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Application.Gui;
using NetMap.Interfaces.Data;
using NetMap.Application;
using NetMap.Interfaces;
using NetMap.Interfaces.Geometry;
using NetMap.UI.Tools;
using StoneGround.Dialog;
using System.Windows.Forms;

namespace StoneGround.Search.TreeNodes
{
    public class StoneGroundNode : ExtTreeNode
    {
        private IFeature _feature;

        public StoneGroundNode(IFeature feature)
        {
            _feature = feature;

            this.ContextmenuAddinTreePath = "/Application/Pads/SearchPad/ContextMenu/StoneGround";
            int index = _feature.Fields.FindField(StoneGroundDatabaseService.FIELD_NAME);
            Text = _feature.GetValue(index).ToString();
        }

        /// <summary>
        /// 定位
        /// </summary>
        public void Position()
        {
            IMapView view = WorkspaceSingleton.ActiveMapView;
            IPoint point = _feature.Geometry as IPoint;
            view.ZoomToCenter(point.X, point.Y, view.DisplayTransformation.ScaleRatio);
            view.Invalidate();
        }

        /// <summary>
        /// 删除
        /// </summary>
        public void Delete()
        {
            _feature.Delete();
            //删除现场情况
            string sql1 = "delete from " + StoneGroundDatabaseService.TABLE_SGPIC
                        + " where " + StoneGroundDatabaseService.FIELD_SGOID + " = " + _feature.OID;
            StoneGroundDatabaseService.Instance.StoneGroundSdb.ExecuteSQL(sql1);
            //删除碎石加工设备
            string sql2 = "delete from " + StoneGroundDatabaseService.TABLE_SGMACHINEPIC
                        + " where " + StoneGroundDatabaseService.FIELD_SGOID + " = " + _feature.OID;
            StoneGroundDatabaseService.Instance.StoneGroundSdb.ExecuteSQL(sql2);
            //刷新
            WorkspaceSingleton.ActiveMapView.Invalidate();
        }

        /// <summary>
        /// 详细信息
        /// </summary>
        public void Detail()
        {
            //通用的GIS数据属性编辑
            //AttributeEditForm attEdit = new AttributeEditForm();
            //attEdit.Init(_feature);
            //attEdit.Show();

            /*FrmSGInfo dialog = new FrmSGInfo();
            dialog.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            dialog.Init(_feature, false);
            dialog.ShowDialog(NetMap.Application.Gui.WorkbenchSingleton.MainWin32Window);*/
            FrmSGInfo2 dialog = new FrmSGInfo2();
            dialog.StartPosition = FormStartPosition.CenterParent;
            dialog.InitInfo(_feature);
            dialog.ShowDialog(NetMap.Application.Gui.WorkbenchSingleton.MainWin32Window);
        }
    }
}
