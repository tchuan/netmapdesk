﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces;
using NetMap.Interfaces.Persist;
using System.Xml;
using NetMap.Utilities;
using NetMap.Interfaces.Render;
using System.Drawing;

namespace NetMap
{
    public class MapViewCollection : IMapViewCollection, IPersistResource
    {
        private List<MapViewData> _viewLst = new List<MapViewData>();

        public void Add(MapViewData view)
        {
            _viewLst.Add(view);
        }

        public void Remove(MapViewData view)
        {
            _viewLst.Remove(view);
        }

        public void Clear()
        {
            _viewLst.Clear();
        }

        public IEnumerator<MapViewData> GetEnumerator()
        {
            return _viewLst.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _viewLst.GetEnumerator();
        }

        #region IPersistResource 成员

        public void Save(XmlElement node, XmlDocument document)
        {
            foreach (MapViewData view in _viewLst)
            {
                XmlElement viewNode = document.CreateElement("View");
                viewNode.SetAttribute("Name", view.Name);
                viewNode.SetAttribute("BackColor", XmlUtil.ToXml(view.BackColor));
                viewNode.SetAttribute("Center", XmlUtil.ToXml(view.Center));
                viewNode.SetAttribute("Scale", view.Scale.ToString());
                viewNode.SetAttribute("MapName", view.Map.Name);
                node.AppendChild(viewNode);
            }
        }

        public void Load(XmlReader reader, object context)
        {
            IMapDocumentCollection collection = context as IMapDocumentCollection;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    if (reader.Name == "View")
                    {
                        MapViewData view = new MapViewData();
                        view.Name = reader.GetAttribute("Name");
                        view.BackColor = ColorTranslator.FromHtml(reader.GetAttribute("BackColor"));
                        view.Center = XmlUtil.ToPoint(reader.GetAttribute("Center"));
                        view.Scale = double.Parse(reader.GetAttribute("Scale"));
                        view.Map = collection.Find(reader.GetAttribute("MapName"));
                        _viewLst.Add(view);
                    }
                }
                else if (reader.NodeType == XmlNodeType.EndElement
                    && reader.Name == "Views")
                {
                    return;
                }
            }
        }

        #endregion
    }
}
