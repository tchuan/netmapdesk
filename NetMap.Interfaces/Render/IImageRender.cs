﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Data;

namespace NetMap.Interfaces.Render
{
    /// <summary>
    /// 影像方案接口
    /// </summary>
    public interface IImageRender
    {
        /// <summary>
        /// 绘制影像
        /// </summary>
        /// <param name="display">渲染环境</param>
        /// <param name="data">影像信息</param>
        void Draw(IDisplay display, IImageData data);
    }
}
