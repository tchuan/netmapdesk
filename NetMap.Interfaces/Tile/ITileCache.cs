﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Tile;

namespace NetMap.Interfaces.Tile
{
    public interface ITileCache<T>
    {
        void Add(TileIndex index, T tile);
        void Remove(TileIndex index);
        T Find(TileIndex index);
    }
}
