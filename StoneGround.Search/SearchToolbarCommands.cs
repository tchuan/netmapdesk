﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Application.Core;
using NetMap.Interfaces.Data;
using NetMap.Data;
using NetMap.Application.Core.WinForms;
using NetMap.Interfaces;
using NetMap.Application;
using NetMap.Interfaces.Geometry;
using NetMap.Application.Util;

namespace StoneGround.Search
{
    public class NameSearch : AbstractTextBoxCommand
    {
        protected override void OnOwnerChanged(EventArgs e)
        {
            base.OnOwnerChanged(e);

            ToolBarTextBox box = Owner as ToolBarTextBox;
            box.LostFocus += new EventHandler(box_LostFocus);
        }

        void box_LostFocus(object sender, EventArgs e)
        {
            Run();
        }

        public override void Run()
        {
            ToolBarTextBox box = Owner as ToolBarTextBox;
            string text = box.Text;
            if (string.IsNullOrEmpty(text))
            {
                SearchPad.Instance.Condition.IsName = false;
                SearchPad.Instance.Condition.Name = "";
            }
            else
            {
                SearchPad.Instance.Condition.IsName = true;
                SearchPad.Instance.Condition.Name = text;
            }
        }
    }

    public class RoadSearch : AbstractComboBoxCommand
    {
        public override void Run()
        {
            //TODO
            //MessageService.ShowMessage("Road Search");
        }

        protected override void OnOwnerChanged(EventArgs e)
        {
            base.OnOwnerChanged(e);

            var comboBox = (ToolBarComboBox)base.ComboBox;
            comboBox.AutoSize = false; //调整宽度
            comboBox.Width = 150;
            //添加默认
            comboBox.Items.Add("无");
            IQueryFilter filter = new QueryFilter();
            filter.Fields.Add(SpatialDatabaseService.FIELD_ROAD_CODE);
            IFeatureClass zoneFeatureClass = SpatialDatabaseService.Instance.RoadFeatureClass;
            IFeatureCursor cursor = zoneFeatureClass.Search(filter);
            IFeature feature = cursor.Next();
            List<string> names = new List<string>();
            while (feature != null)
            {
                string name = feature.GetValue(0).ToString();
                if (!string.IsNullOrEmpty(name)
                    && names.IndexOf(name) < 0)
                {
                    names.Add(name);
                    comboBox.Items.Add(name);
                }                
                feature = cursor.Next();
            }
            cursor.Close();
            //默认选择
            comboBox.SelectedIndex = 0;
            comboBox.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);
        }

        void comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ToolBarComboBox comboBox = (ToolBarComboBox)base.ComboBox;
            string text = comboBox.Text;
            IMapView view = WorkspaceSingleton.ActiveMapView;
            ILayer layer = view.MapDocument.FindLayer(StoneGroundDefines.LAYER_NAME_ROAD);
            IFeatureSelection selection = layer as IFeatureSelection;
            if (text == "无")
            {
                selection.Clear();
                SearchPad.Instance.Condition.IsRoad = false;
                SearchPad.Instance.Condition.Road = "";
            }
            else
            {
                IQueryFilter filter = new QueryFilter();
                filter.WhereClause = SpatialDatabaseService.FIELD_ROAD_CODE + "='" + text + "'";
                selection.Select(filter, geoSelectionResultEnum.GEO_SELECTIONRESULT_NEW);
                SearchPad.Instance.Condition.IsRoad = true;
                SearchPad.Instance.Condition.Road = text;
            }
            if (selection.SelectionSet != null)
            {
                IEnvelope box = selection.SelectionSet.Envelope;
                if (box != null) 
                {
                    //外扩1/2后，缩放
                    IPoint center = box.Center;
                    IEnvelope boxZoom = new NetMap.Geometry.Envelope(center.X - box.Width, center.Y - box.Height,
                        center.X + box.Width, center.Y + box.Height);
                    view.ZoomToData(boxZoom);
                }
            }
            view.Invalidate();
        }
    }

    public class CommitSearch : AbstractCommand
    {
        /// <summary>
        /// 道路距离为5000m
        /// </summary>
        public const double ROAD_DIS = 5000.0;

        private IPolygon _dzoneGeo = null;
        private IPolygon _zoneGeo = null;
        private IMultiLineString _roadGeo = null;

        public override void Run()
        {
            SearchCondition condition = SearchPad.Instance.Condition;
            IFeatureClass featureClass = StoneGroundDatabaseService.Instance.StoneGroundFeatureClass;

            //没有条件，选择全部料场
            if((!condition.IsName)
                && (!condition.IsRoad)
                && (!condition.IsZone)
                && (!condition.IsDzone))
            {
                IQueryFilter filter = new QueryFilter();
                GetAllFields(filter);
                IFeatureCursor featureCursor = featureClass.Search(filter);
                SearchPad.Instance.BuildSearchTreeNodes(featureCursor);
                featureCursor.Close();
                return;
            }

            //获取过滤条件
            GetZone(condition);
            GetRoad(condition);            
            //名字
            IQueryFilter fFilter = new QueryFilter();
            GetAllFields(fFilter);
            IFeatureCursor fCursor = StoneGroundDatabaseService.Instance.StoneGroundFeatureClass.Search(fFilter);
            List<IFeature> fFeatures = new List<IFeature>();
            IFeature fFeature = fCursor.Next();
            while (fFeature != null)
            {
                //是否满足名字
                string name = fFeature.GetValue(0).ToString();
                if (name.IndexOf(condition.Name, StringComparison.CurrentCultureIgnoreCase) < 0)
                {
                    fFeature = fCursor.Next();
                    continue;
                }
                IPoint point = fFeature.Geometry as IPoint;
                //是否满足区划条件
                if (_zoneGeo != null)
                {
                    if (!NetMap.Geometry.Algorithms.Algorithms.IsPointInPolygonByRay(point, _zoneGeo))
                    {
                        fFeature = fCursor.Next();
                        continue;
                    }
                }
                else if (_dzoneGeo != null)
                {
                    if (!NetMap.Geometry.Algorithms.Algorithms.IsPointInPolygonByRay(point, _dzoneGeo))
                    {
                        fFeature = fCursor.Next();
                        continue;
                    }
                }
                //是否满足道路条件
                if (_roadGeo != null)
                {
                    double dis = WebMercatorUtil.Instance.GetPointToMultiLineStringMinDis(point, _roadGeo);
                    if (dis > ROAD_DIS)
                    {
                        fFeature = fCursor.Next();
                        continue;
                    }
                }                
                fFeatures.Add(fFeature);
                fFeature = fCursor.Next();
            }
            fCursor.Close();

            SearchPad.Instance.BuildSearchTreeNodes(fFeatures);
        }

        //获取区划条件
        private void GetZone(SearchCondition condition)
        {
            if (condition.IsZone) //县级区划
            {
                IQueryFilter filter = new QueryFilter();
                filter.WhereClause = SpatialDatabaseService.FIELD_ZONE_NAME + "='" + condition.Zone + "'";
                IFeatureCursor cursor = SpatialDatabaseService.Instance.ZoneFeatureClass.Search(filter);
                IFeature feature = cursor.Next();
                cursor.Close();
                if (feature != null)
                {
                    _zoneGeo = feature.Geometry as IPolygon;
                }
            }
            else if (condition.IsDzone) //地级区划
            {
                IQueryFilter filter = new QueryFilter();
                filter.WhereClause = SpatialDatabaseService.FIELD_ZONE_NAME + "='" + condition.Dzone + "'";
                IFeatureCursor cursor = SpatialDatabaseService.Instance.DzoneFeatureClass.Search(filter);
                IFeature feature = cursor.Next();
                cursor.Close();
                if (feature != null)
                {
                    _dzoneGeo = feature.Geometry as IPolygon;
                }
            }
        }
        //获取道路条件
        private void GetRoad(SearchCondition condition)
        {            
            if (condition.IsRoad)
            {
                IQueryFilter filter = new QueryFilter();
                filter.WhereClause = SpatialDatabaseService.FIELD_ROAD_CODE + "='" + condition.Road + "'";
                IFeatureCursor cursor = SpatialDatabaseService.Instance.RoadFeatureClass.Search(filter);
                IFeature feature = cursor.Next();
                cursor.Close();
                if (feature != null)
                {
                    _roadGeo = feature.Geometry as IMultiLineString;
                }
            }
        }

        private void GetAllFields(IQueryFilter filter)
        {
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_NAME);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_RECORDSTATUS);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_MANAGESTATUS);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_ADDRESS);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_TEL);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_STRTUMNATURE);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_GEOLOGICAGE);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_RESERVES);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_MININGCONDITIONS);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_TRAFFICCONDITIONS);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_RECORDTIME);

            filter.Fields.Add(StoneGroundDatabaseService.FIELD_DENSITY);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_WATERABSORPTION);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_STURDINESS);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_CRUSHING);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_POLISH);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_ABRASION);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_GILL);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_PITCHADHERE);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_COMPRESSIVE);
        }
    }
}
