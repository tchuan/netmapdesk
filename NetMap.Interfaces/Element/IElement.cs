﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Data;
using NetMap.Interfaces.Geometry;

/*
 * 2013-7-15：添加图面元素模型，包括的模型可能有：UI控件、图框、比例尺条
 */

namespace NetMap.Interfaces.Element
{
    /// <summary>
    /// 元素类型
    /// </summary>
    public enum geoElementType
    {
        /// <summary>
        /// 简单图形
        /// </summary>
        GEO_ELEMENT_SIMPLE = 1,
        /// <summary>
        /// 文本
        /// </summary>
        GEO_ELEMENT_TEXT   = 2,
        /// <summary>
        /// 图片
        /// </summary>
        GEO_ELEMENT_RASTER = 3,
        /// <summary>
        /// 组合
        /// </summary>
        GEO_ELEMENT_GROUP = 4
    }

    /// <summary>
    /// 地图元素接口
    /// </summary>
    public interface IElement
    {
        /// <summary>
        /// 关联的图形
        /// </summary>
        IGeometry Geometry { get; set; }
        /// <summary>
        /// 元素类型
        /// </summary>
        geoElementType ElementType { get; }
    }

    /// <summary>
    /// 地图文本元素接口
    /// </summary>
    public interface ITextElement : IElement
    {
        /// <summary>
        /// 有向点
        /// </summary>
        IDirectionPoint Point { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        string Text { get; set; }
    }

    /// <summary>
    /// 地图栅格元素接口
    /// </summary>
    public interface IRasterElement : IElement
    {
        /// <summary>
        /// 栅格数据
        /// </summary>
        SpaceImage Image { get; set; }
    }

    /// <summary>
    /// 组合地图元素接口
    /// </summary>
    public interface IGroupElement : IElement, IList<IElement>
    {
    }
}
