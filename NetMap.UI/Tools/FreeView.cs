﻿using System;
using System.Drawing;
using System.Windows.Forms;
using NetMap.Interfaces;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Render;
using NetMap.UI.Properties;

namespace NetMap.Tools
{
    /// <summary>
    /// 自由查看工具
    /// </summary>
    public class FreeView : Tool
    {
        private int _x, _y;
        private bool _isDown;
        private IDisplayTransformation _disTrans;

        public FreeView()
            : base("自由查看")
        {
        }

        public FreeView(IMapEditView view)
            : base("自由查看", view)
        {          
        }

        protected override void SetIcon()
        {
            _icon = Resources.漫游_Big;            
        }

        protected override void SetCursor()
        {
            Image cursorImg = Resources.hand_16;
            using (Bitmap bitmap = new Bitmap(cursorImg))
            {
                IntPtr handle = bitmap.GetHicon();
                _cursor = new Cursor(handle);
                //_cursor = Cursors.Hand;
            }
        }

        public override void GainFocus()
        {
            _disTrans = View.DisplayTransformation;
            _isDown = false;
            base.GainFocus();
        }

        public override void LostFocus()
        {
            _disTrans = null;
            _isDown = false;
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (_isDown) return;
                _x = e.X;
                _y = e.Y;
                _isDown = true;
            }
        }
        
        public override void OnMouseMove(MouseEventArgs e)
        {
            if (!_isDown)
                return;
            double xMapOff = (e.X - _x) / _disTrans.ScaleRatio;
            double yMapOff = (e.Y - _y) / _disTrans.ScaleRatio;
            IPoint center = _disTrans.VisibleMapBounds.Center;
            View.ZoomToCenter(center.X - xMapOff, center.Y + yMapOff, _disTrans.ScaleRatio);
            _x = e.X;
            _y = e.Y;
            View.Invalidate();
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (!_isDown)
                    return;
                _isDown = false;
                double xMapOff = (e.X - _x) / _disTrans.ScaleRatio;
                double yMapOff = (e.Y - _y) / _disTrans.ScaleRatio;
                IPoint center = _disTrans.VisibleMapBounds.Center;
                View.ZoomToCenter(center.X - xMapOff, center.Y + yMapOff, _disTrans.ScaleRatio);
                View.Invalidate();
            }
        }

        //以鼠标为中心
        public override void OnMouseWheel(MouseEventArgs e)
        {
            IMapDocument document = View.MapDocument;
            IDisplayTransformation disTrans = View.DisplayTransformation;
            double x = 0, y = 0;
            disTrans.ToMapPoint(e.X, e.Y, ref x, ref y);
            double scale = disTrans.ScaleRatio;
            if (e.Delta > 0)
            {
                scale *= 2;
                if (scale > document.MaxScale)
                {
                    scale = document.MaxScale;
                }
            }
            else
            {
                scale *= 0.5;
                if (scale < document.MinScale)
                {
                    scale = document.MinScale;
                }
            }
            View.ZoomToCenter(x, y, scale);
            View.Invalidate();
        }
    }
}
