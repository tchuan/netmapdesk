﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Symbol;
using NetMap.Interfaces.Data;
using NetMap.Interfaces;
using System.IO;
using NetMap.Interfaces.Format;
using NetMap.Symbol;
using NetMap.Interfaces.Persist;
using System.Xml;

namespace NetMap.Render
{
    /// <summary>
    /// 2013-3-28： 默认对于没有对应符号的要素，在不使用默认符号时，直接不表示
    ///             使用者应该设置正确的字段值、符号对应关系
    /// </summary>
    public class UniqueValueRender : IFeatureRender, IUniqueValueRender, IDisposable, IBinaryFormat, IPersistResource
    {
        private Dictionary<string, ISymbol> _symbolDic = null;

        public UniqueValueRender()
        {
            Field = string.Empty;
            DefaultSymbol = null;
            IsUseDefaultSymbol = true;
            _symbolDic = new Dictionary<string, ISymbol>();
        }

        #region IFeatureRender 成员

        public void DrawCursor(IDisplay display, IFeatureCursor cursor, IRenderCancel cancel)
        {
            //参考字段
            IFields fields = cursor.GetFields();
            int nIndex = fields.FindField(Field);
            if (nIndex < 0)
            {
                return;
            }
            //设置绘图环境
            ISymbolRender symbolRender = display.SymbolRender;
            symbolRender.Setup(display);
            IFeature feature = cursor.Next();
            object value = null;
            ISymbol symbol = null;
            geoFeatureType fType = cursor.FeatureClass.FeatureType;
            while (feature != null)
            {
                value = feature.GetValue(nIndex);
                if (_symbolDic.TryGetValue(value.ToString(), out symbol))
                {
                    if(symbol != null)
                    {
                        DrawFeature(symbol, fType, feature, symbolRender);
                    }
                }
                else
                {
                    if (IsUseDefaultSymbol && DefaultSymbol != null)
                    {
                        DrawFeature(DefaultSymbol, fType, feature, symbolRender);
                    }
                }
                feature = cursor.Next(); //TODO 修改
            }
        }

        #endregion

        #region IUniqueValueRender 成员

        public string Name
        {
            get;
            set;
        }

        public string Field
        {
            get;
            set;
        }

        public ISymbol DefaultSymbol
        {
            get;
            set;
        }

        public bool IsUseDefaultSymbol
        {
            get;
            set;
        }

        public List<string> UniqueValues
        {
            get
            {
                List<string> values = new List<string>();
                foreach (string value in _symbolDic.Keys)
                {
                    values.Add(value);
                }
                return values;
            }
        }

        public void SetSymbol(string value, ISymbol symbol)
        {            
            _symbolDic.Add(value, symbol);
        }

        public ISymbol GetSymbol(string value)
        {
            return _symbolDic[value];
        }

        public void ChangeSymbol(string value,ISymbol symbol)
        {
            if (_symbolDic != null)
            {
                if (_symbolDic.Count > 0)
                {
                    _symbolDic[value] = symbol;
                }
            }
        }

        public bool Remove(string value)
        {
            return _symbolDic.Remove(value);
        }

        #endregion

        #region IDisposable 成员

        public void Dispose()
        {
            _symbolDic.Clear();
        }

        #endregion

        #region IBinaryFormat 成员

        public byte[] ToWkb()
        {
            MemoryStream stream = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(stream);

            writer.Write(Field);
            writer.Write(_symbolDic.Count);
            foreach (KeyValuePair<string, ISymbol> item in _symbolDic)
            {
                writer.Write(item.Key);
                if (item.Value == null)
                {
                    writer.Write(SymbolFactory.S_NULL);
                }
                else
                {
                    writer.Write(item.Value.KindAbstract);
                    IBinaryFormat format = item.Value as IBinaryFormat;
                    byte[] wkb = format.ToWkb();
                    writer.Write(wkb.Count());
                    writer.Write(wkb);
                }
            }
            writer.Write(IsUseDefaultSymbol);
            if (DefaultSymbol == null)
            {
                writer.Write(SymbolFactory.S_NULL);
            }
            else
            {
                writer.Write(DefaultSymbol.KindAbstract);
                IBinaryFormat format = DefaultSymbol as IBinaryFormat;
                byte[] wkb = format.ToWkb();
                writer.Write(wkb.Count());
                writer.Write(wkb);
            }
            writer.Dispose();

            return stream.ToArray();
        }

        public void FromWkb(byte[] wkb)
        {
            MemoryStream stream = new MemoryStream(wkb);
            BinaryReader reader = new BinaryReader(stream);

            Field = reader.ReadString();
            int count = reader.ReadInt32();
            for (int i = 0; i < count; i++)
            {
                string sValue = reader.ReadString();
                string sk = reader.ReadString();
                ISymbol symbol = SymbolFactory.CreateSymbol(sk);
                if (symbol != null)
                {
                    int icount = reader.ReadInt32();
                    byte[] iwkb = reader.ReadBytes(icount);
                    IBinaryFormat format = symbol as IBinaryFormat;
                    format.FromWkb(iwkb);
                }
                _symbolDic.Add(sValue, symbol);
            }
            IsUseDefaultSymbol = reader.ReadBoolean();
            string dsk = reader.ReadString();
            DefaultSymbol = SymbolFactory.CreateSymbol(dsk);
            if (DefaultSymbol != null)
            {
                int dcount = reader.ReadInt32();
                byte[] dWkb = reader.ReadBytes(dcount);
                IBinaryFormat dFormat = DefaultSymbol as IBinaryFormat;
                dFormat.FromWkb(dWkb);
            }
            reader.Dispose();
        }

        #endregion

        private void DrawFeature(ISymbol symbol, geoFeatureType fType, IFeature feature, ISymbolRender symbolRender)
        {            
            switch (fType)
            {
                case geoFeatureType.GEO_FEATURE_SIMPLE:
                    {
                        symbolRender.Draw(symbol, feature.Geometry);
                        break;
                    }
                case geoFeatureType.GEO_FEATURE_ANNOTATION:
                    {
                        ITextFeature tFeature = feature as ITextFeature;
                        ITextSymbol tSymbol = symbol as ITextSymbol;
                        if(tSymbol == null)
                        {
                            return;
                        }
                        symbolRender.DrawText(tSymbol, tFeature.Geometry, tFeature.Text);
                        break;
                    }
                case geoFeatureType.GEO_FEATURE_RASTER:
                    {
                        //不存在的情况
                        break;
                    }
            }
        }

        #region IPersistResource 成员

        public void Save(XmlElement node, XmlDocument document)
        {
            XmlElement render = document.CreateElement("UniqueValueRender");
            render.SetAttribute("Field", Field);
            render.SetAttribute("IsUseDefaultSymbol", IsUseDefaultSymbol.ToString());
            render.SetAttribute("DefaultSymbol", DefaultSymbol.Code.ToString());
            foreach (KeyValuePair<string, ISymbol> item in _symbolDic)
            {
                XmlElement itemNode = document.CreateElement("SymbolItem");
                itemNode.SetAttribute("Value", item.Key);
                itemNode.SetAttribute("Symbol", item.Value.Code.ToString());
                render.AppendChild(itemNode);
            }
            node.AppendChild(render);
        }

        public void Load(XmlReader reader, object context)
        {
            ISymbolLibrary library = context as ISymbolLibrary;
            Field = reader.GetAttribute("Field");
            IsUseDefaultSymbol = bool.Parse(reader.GetAttribute("IsUseDefaultSymbol"));
            int defaultSymbol = int.Parse(reader.GetAttribute("DefaultSymbol"));
            DefaultSymbol = library.FindSymbol(defaultSymbol);
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    if (reader.Name == "SymbolItem")
                    {
                        string key = reader.GetAttribute("Value");
                        int value = int.Parse(reader.GetAttribute("Symbol"));
                        _symbolDic.Add(key, library.FindSymbol(value));
                    }
                }
                else if (reader.NodeType == XmlNodeType.EndElement
                    && reader.Name == "UniqueValueRender")
                {
                    return;
                }
            }
        }

        #endregion
    }
}
