﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NetMap.Interfaces.Data;
using NetMap.Data;

namespace User.Dialogs
{
    public partial class FrmPermissionManage : Form
    {
        private Aga.Controls.Tree.NodeControls.NodeTextBox _nodeCls;
        private Aga.Controls.Tree.NodeControls.NodeTextBox _nodeLabel;
        private Aga.Controls.Tree.NodeControls.NodeTextBox _nodeUserType;
        private NodeButton _nodeEdit;
        private NodeButton _nodeDelete;
        private Aga.Controls.Tree.TreeModel _model;

        public FrmPermissionManage()
        {
            InitializeComponent();

            _nodeCls = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            _nodeLabel = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            _nodeUserType = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            _nodeEdit = new NodeButton("修改");
            _nodeDelete = new NodeButton("删除");
            treeViewAdv1.NodeControls.Add(_nodeCls);
            treeViewAdv1.NodeControls.Add(_nodeLabel);
            treeViewAdv1.NodeControls.Add(_nodeUserType);
            treeViewAdv1.NodeControls.Add(_nodeEdit);
            treeViewAdv1.NodeControls.Add(_nodeDelete);

            _nodeCls.DataPropertyName = "Cls";
            _nodeCls.EditEnabled = false;
            _nodeCls.LeftMargin = 3;
            _nodeCls.ParentColumn = treeColumn1;

            _nodeLabel.DataPropertyName = "Label";
            _nodeLabel.EditEnabled = false;
            _nodeLabel.LeftMargin = 3;
            _nodeLabel.ParentColumn = treeColumn2;

            _nodeUserType.DataPropertyName = "UserType";
            _nodeUserType.EditEnabled = false;
            _nodeUserType.LeftMargin = 3;
            _nodeUserType.ParentColumn = treeColumn3;

            _nodeEdit.ParentColumn = treeColumn4;
            _nodeDelete.ParentColumn = treeColumn4;
            _nodeEdit.BtnClick += new EventHandler(_nodeEdit_BtnClick);
            _nodeDelete.BtnClick += new EventHandler(_nodeDelete_BtnClick);

            LoadData();
        }

        #region 编辑功能

        void _nodeDelete_BtnClick(object sender, EventArgs e)
        {
            MessageBox.Show("删除");
        }

        void _nodeEdit_BtnClick(object sender, EventArgs e)
        {
            MessageBox.Show("修改");
        }

        #endregion

        private void LoadData()
        {
            _model = new Aga.Controls.Tree.TreeModel();

            IQueryFilter filter = new QueryFilter();
            filter.Fields.Add(UserService.FIELD_PERMISSION_CLASS);
            filter.Fields.Add(UserService.FIELD_PERMISSION_LABEL);
            filter.Fields.Add(UserService.FIELD_PERMISSION_USERTYPE);
            ICursor cursor = UserService.Instance.PermissionTable.Search(filter);
            IRow row = cursor.Next();
            while (row != null)
            {
                //TODO 只加载小于当前用户权限的用户
                PermissionNode node = new PermissionNode(row);
                _model.Nodes.Add(node);
                row = cursor.Next();
            }
            cursor.Close();

            this.treeViewAdv1.Model = _model;
        }
    }
}
