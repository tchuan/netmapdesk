﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace User
{
    public class Util
    {
        /// <summary>
        /// md5加密
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public static string Md5Encrypt(string content)
        {
            MD5 md5 = MD5.Create();
            string contentUtf8 = ConvertToUtf8(content);
            byte[] bytes = md5.ComputeHash(Encoding.UTF8.GetBytes(contentUtf8));
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                builder.Append(bytes[i].ToString("X2"));
            }
            return builder.ToString();
        }

        /// <summary>
        /// 转换为utf8编码
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public static string ConvertToUtf8(string content)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(content);
            byte[] utfBytes = Encoding.Convert(Encoding.Unicode, Encoding.UTF8, bytes);
            return Encoding.UTF8.GetString(utfBytes);
        }
    }
}
