﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NetMap.Interfaces.Data;
using NetMap.Interfaces.Geometry;
using NetMap.Application;
using NetMap.Data;

namespace StoneGround.Dialog
{
    public partial class FrmSGInfo2 : Form
    {
        private bool _isDirty = false; //是否编辑过
        private IFeature _feature;
        private IFields _fields;
        private bool _isEdit = false; //是否编辑

        //图片对象状态
        enum PicFeatureState
        {
            /// <summary>
            /// 新建
            /// </summary>
            New,
            /// <summary>
            /// 删除
            /// </summary>
            Delete,
            /// <summary>
            /// 打开的
            /// </summary>
            Open
        }
        //图片要素
        class PicFeature
        {
            public IRow row;              //对应的记录
            public Image img;             //图片
            public string name;           //名字
            public PicFeatureState state; //状态 
        }
        List<PicFeature> _sgPicFeatureList = new List<PicFeature>();
        List<PicFeature> _sgMachinePicFeatureList = new List<PicFeature>();

        public FrmSGInfo2()
        {
            InitializeComponent();

            textBoxReserves.Validating += new CancelEventHandler(textBoxReserves_Validating);
            textBoxCompressive.Validating += new CancelEventHandler(textBoxCompressive_Validating);
            textBoxDensity.Validating += new CancelEventHandler(textBoxDensity_Validating);
            textBoxWaterAbsorption.Validating += new CancelEventHandler(textBoxWaterAbsorption_Validating);
            textBoxSturdiness.Validating += new CancelEventHandler(textBoxSturdiness_Validating);
            textBoxCrushing.Validating += new CancelEventHandler(textBoxCrushing_Validating);
            textBoxPolish.Validating += new CancelEventHandler(textBoxPolish_Validating);
            textBoxAbrasion.Validating += new CancelEventHandler(textBoxAbrasion_Validating);
            textBoxGill.Validating += new CancelEventHandler(textBoxGill_Validating);
            textBoxPitchAdhere.Validating += new CancelEventHandler(textBoxPitchAdhere_Validating);
            textBoxPositionLon.Validating += new CancelEventHandler(textBoxPositionLon_Validating);
            textBoxPositionLat.Validating += new CancelEventHandler(textBoxPositionLat_Validating);

            textBoxName.Validated += new EventHandler(textBoxName_Validated);
            textBoxTel.Validated += new EventHandler(textBoxTel_Validated);
            textBoxRecordStatus.Validated += new EventHandler(textBoxRecordStatus_Validated);
            textBoxManageStatus.Validated += new EventHandler(textBoxManageStatus_Validated);
            textBoxStrtumNature.Validated += new EventHandler(textBoxStrtumNature_Validated);
            textBoxGeologicage.Validated += new EventHandler(textBoxGeologicage_Validated);
            textBoxReserves.Validated += new EventHandler(textBoxReserves_Validated);
            textBoxMiningConditions.Validated += new EventHandler(textBoxMiningConditions_Validated);
            dateTimePickerRecordTime.Validated += new EventHandler(dateTimePickerRecordTime_Validated);
            textBoxCompressive.Validated += new EventHandler(textBoxCompressive_Validated);
            textBoxDensity.Validated += new EventHandler(textBoxDensity_Validated);
            textBoxWaterAbsorption.Validated += new EventHandler(textBoxWaterAbsorption_Validated);
            textBoxSturdiness.Validated += new EventHandler(textBoxSturdiness_Validated);
            textBoxCrushing.Validated += new EventHandler(textBoxCrushing_Validated);
            textBoxPolish.Validated += new EventHandler(textBoxPolish_Validated);
            textBoxAbrasion.Validated += new EventHandler(textBoxAbrasion_Validated);
            textBoxGill.Validated += new EventHandler(textBoxGill_Validated);
            textBoxPitchAdhere.Validated += new EventHandler(textBoxPitchAdhere_Validated);
            textBoxPositionLon.Validated += new EventHandler(textBoxPositionLon_Validated);
            textBoxPositionLat.Validated += new EventHandler(textBoxPositionLat_Validated);
            textBoxTrafficConditions.Validated += new EventHandler(textBoxTrafficConditions_Validated);

            FormClosing += new FormClosingEventHandler(FrmSGInfo_FormClosing);
            this.Activated += new EventHandler(FrmSGInfo_Activated);

            //展示图片
            this.listView1.MouseDoubleClick += new MouseEventHandler(listView1_MouseDoubleClick);
            this.listView2.MouseDoubleClick += new MouseEventHandler(listView2_MouseDoubleClick);
        }              

        /// <summary>
        /// 初始化信息展示
        /// </summary>
        public void InitInfo(IFeature feature)
        {
            _isEdit = false;
            this.Height = tabControl1.Height + 25;
            InfoSetUI();
            InfoSetUIValue(feature);
            //加载图片
            InfoSetSGPic(feature);
            InfoSetSGMachinePic(feature);
        }

        /// <summary>
        /// 初始化创建
        /// </summary>
        /// <param name="newFeature"></param>
        public void InitCreate(IFeature newFeature)
        {
            _isEdit = true;
            _isDirty = false;
            _feature = newFeature;
            _fields = _feature.Fields;
            this.Text = "料场创建";
            CreateSetUI();
            BindPicEditEvent();
        }

        /// <summary>
        /// 初始化修改
        /// </summary>
        /// <param name="feature"></param>
        public void InitModify(IFeature feature)
        {
            _isEdit = true;
            _isDirty = false;
            _feature = feature;
            _fields = feature.Fields;
            this.Text = "料场信息修改";
            InfoSetUIValue(_feature);
            //加载图片
            InfoSetSGPic(feature);
            InfoSetSGMachinePic(feature);
            this.buttonApply.Click += new System.EventHandler(this.buttonModify_Click);
            BindPicEditEvent();
        }

        #region 事件处理

        void FrmSGInfo_Activated(object sender, EventArgs e)
        {
            textBoxName.Focus(); //获得焦点
        }

        void FrmSGInfo_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_isDirty)
            {
                DialogResult result = MessageBox.Show("料场信息已改变，是否保存？", "提示", MessageBoxButtons.YesNoCancel);
                if (result == System.Windows.Forms.DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
                else if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    _feature.Store();
                    //刷新地图
                    WorkspaceSingleton.ActiveMapView.Invalidate();
                }
            }
            //释放图片资源
            foreach (PicFeature pic in _sgPicFeatureList)
            {
                if (pic.img != null)
                {
                    pic.img.Dispose();
                    pic.img = null;
                }
            }
            foreach (PicFeature pic in _sgMachinePicFeatureList)
            {
                if (pic.img != null)
                {
                    pic.img.Dispose();
                    pic.img = null;
                }
            }
        }

        #region 数据验证

        void textBoxPositionLat_Validating(object sender, CancelEventArgs e)
        {
            if(!_isEdit)
            {
                return;
            }
            double result;
            if (!double.TryParse(textBoxPositionLat.Text, out result))
            {
                MessageBox.Show(this, "请输入有效的：纬度", "提示");
                e.Cancel = true;
            }
        }

        void textBoxPositionLon_Validating(object sender, CancelEventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            double result;
            if (!double.TryParse(textBoxPositionLon.Text, out result))
            {
                MessageBox.Show(this, "请输入有效的：经度", "提示");
                e.Cancel = true;
            }
        }

        void textBoxPitchAdhere_Validating(object sender, CancelEventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            int result;
            if (!int.TryParse(textBoxPitchAdhere.Text, out result))
            {
                MessageBox.Show(this, "请输入有效的：与沥青粘附等级（级）", "提示");
                e.Cancel = true;
            }
        }

        void textBoxGill_Validating(object sender, CancelEventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            double result;
            if (!double.TryParse(textBoxGill.Text, out result))
            {
                MessageBox.Show(this, "请输入有效的：针片状（%）", "提示");
                e.Cancel = true;
            }
        }

        void textBoxAbrasion_Validating(object sender, CancelEventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            double result;
            if (!double.TryParse(textBoxAbrasion.Text, out result))
            {
                MessageBox.Show(this, "请输入有效的：磨耗值（%）", "提示");
                e.Cancel = true;
            }
        }

        void textBoxPolish_Validating(object sender, CancelEventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            double result;
            if (!double.TryParse(textBoxPolish.Text, out result))
            {
                MessageBox.Show(this, "请输入有效的：磨光值（PSV）", "提示");
                e.Cancel = true;
            }
        }

        void textBoxCrushing_Validating(object sender, CancelEventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            double result;
            if (!double.TryParse(textBoxCrushing.Text, out result))
            {
                MessageBox.Show(this, "请输入有效的：压碎值（%）", "提示");
                e.Cancel = true;
            }
        }

        void textBoxSturdiness_Validating(object sender, CancelEventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            double result;
            if (!double.TryParse(textBoxSturdiness.Text, out result))
            {
                MessageBox.Show(this, "请输入有效的：坚固性（%）", "提示");
                e.Cancel = true;
            }
        }

        void textBoxWaterAbsorption_Validating(object sender, CancelEventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            double result;
            if (!double.TryParse(textBoxWaterAbsorption.Text, out result))
            {
                MessageBox.Show(this, "请输入有效的：吸水率（%）", "提示");
                e.Cancel = true;
            }
        }

        void textBoxDensity_Validating(object sender, CancelEventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            double result;
            if (!double.TryParse(textBoxDensity.Text, out result))
            {
                MessageBox.Show(this, "请输入有效的：表观相对密度", "提示");
                e.Cancel = true;
            }
        }

        void textBoxCompressive_Validating(object sender, CancelEventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            double result;
            if (!double.TryParse(textBoxCompressive.Text, out result))
            {
                MessageBox.Show(this, "请输入有效的：单轴抗压强度（Mpa）", "提示");
                e.Cancel = true;
            }
        }

        void textBoxReserves_Validating(object sender, CancelEventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            double result;
            if (!double.TryParse(textBoxReserves.Text, out result))
            {
                MessageBox.Show(this, "请输入有效的：储量（万m³）", "提示");
                e.Cancel = true;
            }
        }

        #endregion

        #region 数据验证完毕

        void textBoxTrafficConditions_Validated(object sender, EventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_TRAFFICCONDITIONS);
            string value = textBoxTrafficConditions.Text;
            string oldValue = _feature.GetValue(index).ToString();
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxPositionLat_Validated(object sender, EventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            double value = 0;
            double.TryParse(textBoxPositionLat.Text, out value);
            IPoint point = _feature.Geometry as IPoint;
            if (point.Y != value)
            {
                _feature.Geometry = new NetMap.Geometry.Point(point.X, value);
                _isDirty = true;
            }
        }

        void textBoxPositionLon_Validated(object sender, EventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            double value = 0;
            double.TryParse(textBoxPositionLon.Text, out value);
            IPoint point = _feature.Geometry as IPoint;
            if (point.X != value)
            {
                _feature.Geometry = new NetMap.Geometry.Point(value, point.Y);
                _isDirty = true;
            }
        }

        void textBoxPitchAdhere_Validated(object sender, EventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_PITCHADHERE);
            int value = 0;
            int.TryParse(textBoxPitchAdhere.Text, out value);
            int oldValue = (int)_feature.GetValue(index);
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxGill_Validated(object sender, EventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_GILL);
            double value = 0;
            double.TryParse(textBoxGill.Text, out value);
            double oldValue = (double)_feature.GetValue(index);
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxAbrasion_Validated(object sender, EventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_ABRASION);
            double value = 0;
            double.TryParse(textBoxAbrasion.Text, out value);
            double oldValue = (double)_feature.GetValue(index);
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxPolish_Validated(object sender, EventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_POLISH);
            double value = 0;
            double.TryParse(textBoxPolish.Text, out value);
            double oldValue = (double)_feature.GetValue(index);
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxCrushing_Validated(object sender, EventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_CRUSHING);
            double value = 0;
            double.TryParse(textBoxCrushing.Text, out value);
            double oldValue = (double)_feature.GetValue(index);
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxSturdiness_Validated(object sender, EventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_STURDINESS);
            double value = 0;
            double.TryParse(textBoxSturdiness.Text, out value);
            double oldValue = (double)_feature.GetValue(index);
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxWaterAbsorption_Validated(object sender, EventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_WATERABSORPTION);
            double value = 0;
            double.TryParse(textBoxWaterAbsorption.Text, out value);
            double oldValue = (double)_feature.GetValue(index);
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxDensity_Validated(object sender, EventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_DENSITY);
            double value = 0;
            double.TryParse(textBoxDensity.Text, out value);
            double oldValue = (double)_feature.GetValue(index);
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxCompressive_Validated(object sender, EventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_COMPRESSIVE);
            double value = 0;
            double.TryParse(textBoxCompressive.Text, out value);
            double oldValue = (double)_feature.GetValue(index);
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void dateTimePickerRecordTime_Validated(object sender, EventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_RECORDTIME);
            DateTime value = dateTimePickerRecordTime.Value;
            DateTime oldValue = (DateTime)_feature.GetValue(index);
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxMiningConditions_Validated(object sender, EventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_MININGCONDITIONS);
            string value = textBoxMiningConditions.Text;
            string oldValue = _feature.GetValue(index).ToString();
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxReserves_Validated(object sender, EventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_RESERVES);
            double value = 0;
            double.TryParse(textBoxReserves.Text, out value);
            double oldValue = (double)_feature.GetValue(index);
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxGeologicage_Validated(object sender, EventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_GEOLOGICAGE);
            string value = textBoxGeologicage.Text;
            string oldValue = _feature.GetValue(index).ToString();
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxStrtumNature_Validated(object sender, EventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_STRTUMNATURE);
            string value = textBoxStrtumNature.Text;
            string oldValue = _feature.GetValue(index).ToString();
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxManageStatus_Validated(object sender, EventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_MANAGESTATUS);
            string value = textBoxManageStatus.Text;
            string oldValue = _feature.GetValue(index).ToString();
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxRecordStatus_Validated(object sender, EventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_RECORDSTATUS);
            string value = textBoxRecordStatus.Text;
            string oldValue = _feature.GetValue(index).ToString();
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        void textBoxTel_Validated(object sender, EventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_TEL);
            string value = textBoxTel.Text;
            _feature.SetValue(index, value);
            _isDirty = true;
        }

        void textBoxName_Validated(object sender, EventArgs e)
        {
            if (!_isEdit)
            {
                return;
            }
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_NAME);
            string value = textBoxName.Text;
            string oldValue = _feature.GetValue(index).ToString();
            if (oldValue != value)
            {
                _feature.SetValue(index, value);
                _isDirty = true;
            }
        }

        #endregion

        private void buttonModify_Click(object sender, EventArgs e)
        {
            bool isSGPicChange = IsSGPicChange();
            bool isSGMachinePicChange = IsSGMachinePicChange();
            if (_isDirty || isSGPicChange || isSGMachinePicChange)
            {
                if (_isDirty)
                {
                    _feature.Store();
                    _isDirty = false;
                }
                if (isSGPicChange)
                {
                    SaveSGPic();
                }
                if (isSGMachinePicChange)
                {
                    SaveSGMachinePic();
                }
                //刷新地图
                WorkspaceSingleton.ActiveMapView.Invalidate();
                MessageBox.Show(this, "修改成功", "提示");
            }            
            else
            {
                MessageBox.Show(this, "信息未变更，无需修改", "提示");
            }
            this.Close();
        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            int index = _fields.FindField(StoneGroundDatabaseService.FIELD_NAME);
            object value = _feature.GetValue(index);
            if (value == null || string.IsNullOrEmpty(value.ToString()))
            {
                MessageBox.Show(this, "料场信息不全，不能创建", "提示");
            }
            else
            {

                _feature.Store();   //存储料场属性
                SaveSGPic();        //存储现场情况
                SaveSGMachinePic(); //存储碎石加工设备
                _isDirty = false;
                //刷新地图
                WorkspaceSingleton.ActiveMapView.Invalidate();
                MessageBox.Show(this, "创建成功", "提示");
                this.Close();
            }
        }

        #region 图片编辑

        void buttonPic2Del_Click(object sender, EventArgs e)
        {
            listView2.BeginUpdate();
            ListView.SelectedListViewItemCollection selectedItems = listView2.SelectedItems;
            foreach (ListViewItem item in selectedItems)
            {
                PicFeature picFeature = item.Tag as PicFeature;
                picFeature.state = PicFeatureState.Delete;
                listView2.Items.Remove(item);
            }
            listView2.EndUpdate();
        }

        void buttonPic2Add_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "jpg (*.jpg)|*.jpg";
            dialog.Multiselect = true;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                listView2.BeginUpdate();
                foreach (string file in dialog.FileNames)
                {
                    //创建新图片要素
                    PicFeature newPicFeature = new PicFeature();
                    newPicFeature.img = Image.FromFile(file);
                    newPicFeature.name = System.IO.Path.GetFileNameWithoutExtension(file);
                    newPicFeature.state = PicFeatureState.New;
                    _sgMachinePicFeatureList.Add(newPicFeature);
                    //加入界面
                    Bitmap previewImg = new Bitmap(256, 256);
                    CreatePreviewImage(previewImg, newPicFeature.img);
                    int index = imageList2.Images.Count;
                    imageList2.Images.Add(previewImg);
                    ListViewItem item = new ListViewItem(newPicFeature.name, index);
                    item.Tag = newPicFeature;
                    listView2.Items.Add(item);
                }
                listView2.EndUpdate();
            }
        }

        void buttonPic1Del_Click(object sender, EventArgs e)
        {
            listView1.BeginUpdate();
            ListView.SelectedListViewItemCollection selectedItems = listView1.SelectedItems;
            foreach (ListViewItem item in selectedItems)
            {
                PicFeature picFeature = item.Tag as PicFeature;
                picFeature.state = PicFeatureState.Delete;
                listView1.Items.Remove(item);
            }
            listView1.EndUpdate();
        }

        void buttonPic1Add_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "jpg (*.jpg)|*.jpg";
            dialog.Multiselect = true;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                listView1.BeginUpdate();
                foreach (string file in dialog.FileNames)
                {
                    //创建新图片要素
                    PicFeature newPicFeature = new PicFeature();
                    newPicFeature.img = Image.FromFile(file);
                    newPicFeature.name = System.IO.Path.GetFileNameWithoutExtension(file);
                    newPicFeature.state = PicFeatureState.New;
                    _sgPicFeatureList.Add(newPicFeature);
                    //加入界面
                    Bitmap previewImg = new Bitmap(256, 256);
                    CreatePreviewImage(previewImg, newPicFeature.img);
                    int index = imageList1.Images.Count;
                    imageList1.Images.Add(previewImg);
                    ListViewItem item = new ListViewItem(newPicFeature.name, index);
                    item.Tag = newPicFeature;
                    listView1.Items.Add(item);
                }
                listView1.EndUpdate();
            }
        }

        #endregion

        #region 图片浏览

        void listView2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listView2.SelectedItems.Count > 0)
            {
                FrmPictureView viewer = new FrmPictureView();
                PicFeature pic = listView2.SelectedItems[0].Tag as PicFeature;
                viewer.InitPictureView(pic.img, pic.name);
                viewer.ShowDialog();
            }
        }

        void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                FrmPictureView viewer = new FrmPictureView();
                PicFeature pic = listView1.SelectedItems[0].Tag as PicFeature;
                viewer.InitPictureView(pic.img, pic.name);
                viewer.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Privates

        //信息——设置UI
        private void InfoSetUI()
        {
            textBoxName.Enabled = false;
            textBoxTel.Enabled = false;
            textBoxManageStatus.Enabled = false;
            textBoxRecordStatus.Enabled = false;
            textBoxStrtumNature.Enabled = false;
            textBoxGeologicage.Enabled = false;
            textBoxReserves.Enabled = false;
            textBoxMiningConditions.Enabled = true;
            dateTimePickerRecordTime.Enabled = false;
            textBoxPositionLon.Enabled = false;
            textBoxPositionLat.Enabled = false;
            textBoxCompressive.Enabled = false;
            textBoxDensity.Enabled = false;
            textBoxWaterAbsorption.Enabled = false;
            textBoxSturdiness.Enabled = false;
            textBoxCrushing.Enabled = false;
            textBoxPolish.Enabled = false;
            textBoxAbrasion.Enabled = false;
            textBoxGill.Enabled = false;
            textBoxPitchAdhere.Enabled = false;
            textBoxTrafficConditions.Enabled = true;

            buttonPic1Add.Enabled = false;
            buttonPic1Add.Visible = false;
            buttonPic1Del.Enabled = false;
            buttonPic1Del.Visible = false;
            buttonPic2Add.Enabled = false;
            buttonPic2Add.Visible = false;
            buttonPic2Del.Enabled = false;
            buttonPic2Del.Visible = false;
            buttonApply.Enabled = false;
            buttonApply.Visible = false;
        }

        //信息——UI赋值
        private void InfoSetUIValue(IFeature feature)
        {
            IFields fields = feature.Fields;
            int index = fields.FindField(StoneGroundDatabaseService.FIELD_NAME);
            object value = feature.GetValue(index);
            textBoxName.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_TEL);
            value = feature.GetValue(index);
            textBoxTel.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_RECORDSTATUS);
            value = feature.GetValue(index);
            textBoxRecordStatus.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_MANAGESTATUS);
            value = feature.GetValue(index);
            textBoxManageStatus.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_STRTUMNATURE);
            value = feature.GetValue(index);
            textBoxStrtumNature.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_GEOLOGICAGE);
            value = feature.GetValue(index);
            textBoxGeologicage.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_RESERVES);
            value = feature.GetValue(index);
            textBoxReserves.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_MININGCONDITIONS);
            value = feature.GetValue(index);
            textBoxMiningConditions.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_TRAFFICCONDITIONS);
            value = feature.GetValue(index);
            textBoxTrafficConditions.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_RECORDTIME);
            value = feature.GetValue(index);
            dateTimePickerRecordTime.Value = (DateTime)value;

            index = fields.FindField(StoneGroundDatabaseService.FIELD_COMPRESSIVE);
            value = feature.GetValue(index);
            textBoxCompressive.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_DENSITY);
            value = feature.GetValue(index);
            textBoxDensity.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_WATERABSORPTION);
            value = feature.GetValue(index);
            textBoxWaterAbsorption.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_STURDINESS);
            value = feature.GetValue(index);
            textBoxSturdiness.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_CRUSHING);
            value = feature.GetValue(index);
            textBoxCrushing.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_POLISH);
            value = feature.GetValue(index);
            textBoxPolish.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_ABRASION);
            value = feature.GetValue(index);
            textBoxAbrasion.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_GILL);
            value = feature.GetValue(index);
            textBoxGill.Text = value.ToString();

            index = fields.FindField(StoneGroundDatabaseService.FIELD_PITCHADHERE);
            value = feature.GetValue(index);
            textBoxPitchAdhere.Text = value.ToString();

            IPoint point = feature.Geometry as IPoint;
            textBoxPositionLon.Text = point.X.ToString();
            textBoxPositionLat.Text = point.Y.ToString();
        }

        //信息——现场情况
        private void InfoSetSGPic(IFeature feature)
        { 
            //获取
            ITable table = StoneGroundDatabaseService.Instance.StoneGroundPicTable;
            IQueryFilter filter = new QueryFilter();
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_SGOID);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_PIC);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_PICNAME);
            filter.WhereClause = StoneGroundDatabaseService.FIELD_SGOID + "=" + feature.OID;
            ICursor cursor = table.Search(filter);
            IRow row = cursor.Next();
            while (row != null)
            {
                PicFeature item = new PicFeature();
                item.row = row;
                using (System.IO.MemoryStream stream = new System.IO.MemoryStream((byte[])row.GetValue(1)))
                {
                    item.img = Image.FromStream(stream);
                }
                item.name = row.GetValue(2).ToString();
                item.state = PicFeatureState.Open;
                _sgPicFeatureList.Add(item);
                row = cursor.Next();
            }
            cursor.Close();
            //加入界面
            listView1.BeginUpdate();
            foreach (PicFeature picFeatureItem in _sgPicFeatureList)
            {
                Bitmap previewImg = new Bitmap(256, 256);
                CreatePreviewImage(previewImg, picFeatureItem.img);
                int index = imageList1.Images.Count;
                imageList1.Images.Add(previewImg);
                ListViewItem item = new ListViewItem(picFeatureItem.name, index);
                item.Tag = picFeatureItem;
                listView1.Items.Add(item);
            }
            listView1.EndUpdate();
        }

        //信息——碎石加工设备
        private void InfoSetSGMachinePic(IFeature feature)
        {
            //获取
            ITable table = StoneGroundDatabaseService.Instance.StoneGroundMachinePicTable;
            IQueryFilter filter = new QueryFilter();
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_SGOID);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_PIC);
            filter.Fields.Add(StoneGroundDatabaseService.FIELD_PICNAME);
            filter.WhereClause = StoneGroundDatabaseService.FIELD_SGOID + "=" + feature.OID;
            ICursor cursor = table.Search(filter);
            IRow row = cursor.Next();
            while (row != null)
            {
                PicFeature item = new PicFeature();
                item.row = row;
                using (System.IO.MemoryStream stream = new System.IO.MemoryStream((byte[])row.GetValue(1)))
                {
                    item.img = Image.FromStream(stream);
                }
                item.name = row.GetValue(2).ToString();
                item.state = PicFeatureState.Open;
                _sgMachinePicFeatureList.Add(item);
                row = cursor.Next();
            }
            cursor.Close();
            //加入界面
            listView2.BeginUpdate();
            foreach (PicFeature picFeatureItem in _sgMachinePicFeatureList)
            {
                Bitmap previewImg = new Bitmap(256, 256);
                CreatePreviewImage(previewImg, picFeatureItem.img);
                int index = imageList2.Images.Count;
                imageList2.Images.Add(previewImg);
                ListViewItem item = new ListViewItem(picFeatureItem.name, index);
                item.Tag = picFeatureItem;
                listView2.Items.Add(item);
            }
            listView2.EndUpdate();
        }

        //创建--设置UI
        private void CreateSetUI()
        {            
            this.buttonApply.Text = "创建";
            this.buttonApply.Click += new EventHandler(buttonCreate_Click);
        }

        //绑定图片编辑事件
        private void BindPicEditEvent()
        {
            buttonPic1Add.Click += new EventHandler(buttonPic1Add_Click);
            buttonPic1Del.Click += new EventHandler(buttonPic1Del_Click);
            buttonPic2Add.Click += new EventHandler(buttonPic2Add_Click);
            buttonPic2Del.Click += new EventHandler(buttonPic2Del_Click);
        }

        private void SaveSGPic()
        {
            foreach (PicFeature pic in _sgPicFeatureList)
            {
                switch (pic.state)
                {
                    case PicFeatureState.New:
                        {
                            IRow newRow = StoneGroundDatabaseService.Instance.StoneGroundPicTable.CreateRow();
                            newRow.SetValue(0, _feature.OID);
                            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
                            {
                                pic.img.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                                newRow.SetValue(1, stream.ToArray());
                            }
                            newRow.SetValue(2, pic.name);
                            newRow.Store();
                            pic.row = newRow;
                            pic.state = PicFeatureState.Open;
                            break;
                        }
                    case PicFeatureState.Delete:
                        {
                            if (pic.row != null)
                            {
                                pic.row.Delete();
                                pic.row = null;
                            }
                            break;
                        }
                    case PicFeatureState.Open:
                        {
                            //do nothing
                            break;
                        }
                }
            }
        }

        private void SaveSGMachinePic()
        {
            foreach (PicFeature pic in _sgMachinePicFeatureList)
            {
                switch (pic.state)
                {
                    case PicFeatureState.New:
                        {
                            IRow newRow = StoneGroundDatabaseService.Instance.StoneGroundMachinePicTable.CreateRow();
                            newRow.SetValue(0, _feature.OID);
                            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
                            {
                                pic.img.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                                newRow.SetValue(1, stream.ToArray());
                            }
                            newRow.SetValue(2, pic.name);
                            newRow.Store();
                            pic.row = newRow;
                            pic.state = PicFeatureState.Open;
                            break;
                        }
                    case PicFeatureState.Delete:
                        {
                            if (pic.row != null)
                            {
                                pic.row.Delete();
                                pic.row = null;
                            }
                            break;
                        }
                    case PicFeatureState.Open:
                        {
                            //do nothing
                            break;
                        }
                }
            }
        }

        #endregion

        //创建图片预览
        private void CreatePreviewImage(Bitmap previewImg, Image sourceImg)
        {          
            float width = previewImg.Width;
            float height = previewImg.Height;
            float scale = Math.Min(width / sourceImg.Width, height / sourceImg.Height);
            float newWidth = sourceImg.Width * scale;
            float newHeight = sourceImg.Height * scale;
            //绘制
            //previewImg.SetResolution(sourceImg.HorizontalResolution, sourceImg.VerticalResolution);
            using (Graphics g = Graphics.FromImage(previewImg))
            {
                g.Clear(Color.Transparent);
                g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.DrawImage(sourceImg, new RectangleF(0.5f * (width - newWidth), 0.5f * (height - newHeight), newWidth, newHeight),
                    new RectangleF(0, 0, sourceImg.Width, sourceImg.Height), GraphicsUnit.Pixel);
                g.Dispose();
            }
        }

        private bool IsSGPicChange()
        {
            foreach (PicFeature pic in _sgPicFeatureList)
            {
                if (pic.state != PicFeatureState.Open)
                {
                    return true;
                }
            }
            return false;
        }

        private bool IsSGMachinePicChange()
        {
            foreach (PicFeature pic in _sgMachinePicFeatureList)
            {
                if (pic.state != PicFeatureState.Open)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
