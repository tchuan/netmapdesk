﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Symbol
{
    /// <summary>
    /// 线上类型
    /// </summary>
    public enum geoOnEdgeType
    {
        GEO_ONEDGE_DIVIDE = 1,  //等分
        GEO_ONEDGE_DISTANCE = 2 //等距
    }

    /// <summary>
    /// 线上点符号
    /// </summary>
    public interface IOnEdgePointSymbol 
    {
        /// <summary>
        /// 获取和设置线上类型
        /// </summary>
        geoOnEdgeType OnEdgeType { get; set; }
        /// <summary>
        /// 获取和设置参考值，当为等分线符号时，表示等分数
        ///                   为等距线符号时，表示间距（图上单位）
        /// </summary>
        int ReferenceValue { get; set; }
    }
}
