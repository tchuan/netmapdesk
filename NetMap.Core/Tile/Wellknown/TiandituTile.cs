﻿using NetMap.Geometry;
using NetMap.Interfaces.Tile;
using NetMap.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

/*
 * 备注：天地图地图瓦片分为16级（L=2-18)，其中L=2级比例尺最小。L=2级有8个瓦片文件，向下四分。
 */

namespace NetMap.Tile.Wellknown
{
    /// <summary>
    /// 天地图瓦片类型
    /// </summary>
    public enum TiandituTileType
    {
        /// <summary>
        /// 矢量背景
        /// </summary>
        VecBack,
        /// <summary>
        /// 矢量注记
        /// </summary>
        VecAno,
        /// <summary>
        /// 矢量英文注记
        /// </summary>
        VecEano,
        /// <summary>
        /// 影像背景
        /// </summary>
        ImgBack,
        /// <summary>
        /// 影像注记
        /// </summary>
        ImgAno,
        /// <summary>
        /// 影像英文注记
        /// </summary>
        ImgEano,
        /// <summary>
        /// 地形背景
        /// </summary>
        TerBack,
        /// <summary>
        /// 地形注记
        /// </summary>
        TerAno,
        /// <summary>
        /// 地形英文注记
        /// </summary>
        TerEano
    }

    /// <summary>
    /// 服务通道(t0-t7共8个通道)
    /// </summary>
    public enum TiandituTileServiceChanel
    {
        t0,
        t1,
        t2,
        t3,
        t4,
        t5,
        t6,
        t7
    }

    /// <summary>
    /// 天地图经纬度瓦片Provider
    /// </summary>
    public class TiandituTileProvider : ITileProvider
    {
        #region URL

        internal const string VEC_MAPURL = "http://{0}.tianditu.com/vec_c/wmts?request=GetTile&service=wmts&version=1.0.0&Layer=vec&Style=default&TileMatrixSet=c";
        internal const string VEC_ANOURL = "http://{0}.tianditu.com/cva_c/wmts?request=GetTile&service=wmts&version=1.0.0&Layer=cva&Style=default&TileMatrixSet=c";
        internal const string VEC_EANOURL = "http://{0}.tianditu.com/eva_c/wmts?request=GetTile&service=wmts&version=1.0.0&Layer=cva&Style=default&TileMatrixSet=c";
        internal const string IMG_MAPURL = "http://{0}.tianditu.com/img_c/wmts?request=GetTile&service=wmts&version=1.0.0&Layer=img&Style=default&TileMatrixSet=c";
        internal const string IMG_ANOURL = "http://{0}.tianditu.com/cia_c/wmts?request=GetTile&service=wmts&version=1.0.0&Layer=cia&Style=default&TileMatrixSet=c";
        internal const string IMG_EANOURL = "http://{0}.tianditu.com/eia_c/wmts?request=GetTile&service=wmts&version=1.0.0&Layer=cia&Style=default&TileMatrixSet=c";
        internal const string TER_MAPURL = "http://{0}.tianditu.com/ter_c/wmts?request=GetTile&service=wmts&version=1.0.0&Layer=ter&Style=default&TileMatrixSet=c";
        internal const string TER_ANOURL = "http://{0}.tianditu.com/cta_c/wmts?request=GetTile&service=wmts&version=1.0.0&Layer=cta&Style=default&TileMatrixSet=c";
        internal const string TER_EANOURL = "http://{0}.tianditu.com/eta_c/wmts?request=GetTile&service=wmts&version=1.0.0&Layer=cta&Style=default&TileMatrixSet=c";

        #endregion

        protected TiandituTileType _type = TiandituTileType.VecBack;
        protected string _url = string.Empty;
        protected readonly ITileCache<byte[]> _tileCache;
        private int _timeOut = 10000;
        protected TiandituTileServiceChanel _chanel = TiandituTileServiceChanel.t0;

        /// <summary>
        /// 默认是矢量瓦片
        /// </summary>
        public TiandituTileProvider(TiandituTileType type = TiandituTileType.VecBack, 
            TiandituTileServiceChanel chanel = TiandituTileServiceChanel.t0, 
            ITileCache<byte[]> tileCache = null)
        {
            _type = type;
            _chanel = chanel;
            SetUrl(type);           
        }

        protected virtual void SetUrl(TiandituTileType type)
        {
            switch (type)
            {
                case TiandituTileType.VecBack:
                    {
                        _url = string.Format(VEC_MAPURL, _chanel);                        
                        break;
                    }
                case TiandituTileType.VecAno:
                    {
                        _url = string.Format(VEC_ANOURL, _chanel);
                        break;
                    }
                case TiandituTileType.VecEano:
                    {
                        _url = string.Format(VEC_EANOURL, _chanel);
                        break;
                    }
                case TiandituTileType.ImgBack:
                    {
                        _url = string.Format(IMG_MAPURL, _chanel);
                        break;
                    }
                case TiandituTileType.ImgAno:
                    {
                        _url = string.Format(IMG_ANOURL, _chanel);
                        break;
                    }
                case TiandituTileType.ImgEano:
                    {
                        _url = string.Format(IMG_EANOURL, _chanel);
                        break;
                    }
                case TiandituTileType.TerBack:
                    {
                        _url = string.Format(TER_MAPURL, _chanel);
                        break;
                    }
                case TiandituTileType.TerAno:
                    {
                        _url = string.Format(TER_ANOURL, _chanel);
                        break;
                    }
                case TiandituTileType.TerEano:
                    {
                        _url = string.Format(TER_EANOURL, _chanel);
                        break;
                    }
            }
        }

        public byte[] GetTile(TileInfo tileInfo)
        {
            if (_tileCache != null)
            {
                byte[] bytes = _tileCache.Find(tileInfo.Index);
                if (bytes == null)
                {
                    //获取瓦片
                    bytes = GetSingleTile(tileInfo);
                    //存储瓦片
                    if (bytes != null)
                    {
                        _tileCache.Add(tileInfo.Index, bytes);
                    }
                }
                return bytes;
            }
            else
            {
                return GetSingleTile(tileInfo); 
            }
        }

        //获取单独的瓦片
        private byte[] GetSingleTile(TileInfo tileInfo)
        {
            TileIndex tileIndex = tileInfo.Index;
            string addtionalUrl = BuildUrl(tileIndex.Row, tileIndex.Col, tileIndex.Level);
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(_url + addtionalUrl);
                WebResponse webResponse = webRequest.GetSyncResponse(_timeOut);
                if (webResponse.ContentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
                {
                    using (Stream stream = webResponse.GetResponseStream())
                    {
                        return StreamUtil.ReadFully(stream);
                    }
                }
                else
                {
                    Debug.Print(addtionalUrl + "错误的瓦片文件类型");
                    return null;
                }
            }
            catch (WebException e)
            {
                HttpWebResponse webResponse = (HttpWebResponse)e.Response;
                if (webResponse == null 
                    || webResponse.StatusCode == HttpStatusCode.NotFound) //处理404错误，这种说明这个瓦片不存在（注记存在这种情况）
                {
                    return null;
                }
                else
                {
                    Debug.Print(addtionalUrl + e.Message); 
                    return null;
                }
            }
            catch (Exception ex)
            {
                Debug.Print(addtionalUrl + ex.Message);
                return null;
            }
        }

        protected virtual string BuildUrl(int row, int col, string level)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("&TileMatrix=").Append(level);
            builder.Append("&TileRow=").Append(row);
            builder.Append("&TileCol=").Append(col);
            return builder.ToString();
        }

        /// <summary>
        /// 将Image转换为byte[]
        /// </summary>
        /// <param name="image">Image</param>
        /// <returns>byte[]</returns>
        public byte[] ConvertImage(Image image)
        {
            MemoryStream ms = new MemoryStream();
            image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            ms.Close();
            byte[] bytes = ms.ToArray();
            ms.Dispose();
            return bytes;
        }

        /// <summary>
        /// 获取改变服务通道的新Provider实例
        /// </summary>
        /// <param name="chanel"></param>
        /// <returns></returns>
        public virtual TiandituTileProvider GetChangeServiceChanelInstance(TiandituTileServiceChanel chanel)
        {
            return new TiandituTileProvider(_type, chanel, _tileCache);
        }
    }

    /// <summary>
    /// 天地图经纬度瓦片规则
    /// </summary>
    public class TiandituTileSchema : TileSchema
    {
        public TiandituTileSchema()
        {
            Height = 256;
            Width = 256;
            Name = "天地图经纬度瓦片";
            Extent = new Envelope(-180, -90, 180, 90);
            Format = "png";
            OriginX = -180;
            OriginY = 90;
            Axis = AxisDirection.InvertedY;
            Srs = "EPSG:4326"; //偏移不大，先采用这个

            //Resolutions.Add("1", new Resolution() { Id = "1", ScaleDenominator = 2.958293554545656E8, UnitsPerPixel = 0.703125, Top = 90, Left = -180, TileWidth = 256, TileHeight = 256, MatrixWidth = 2, MatrixHeight = 1 });
            Resolutions.Add("2", new Resolution() { Id = "2", ScaleDenominator = 1.479146777272828E8, UnitsPerPixel = 0.3515625, Top = 90, Left = -180, TileWidth = 256, TileHeight = 256, MatrixWidth = 4, MatrixHeight = 2 });
            Resolutions.Add("3", new Resolution() { Id = "3", ScaleDenominator = 7.39573388636414E7, UnitsPerPixel = 0.17578125, Top = 90, Left = -180, TileWidth = 256, TileHeight = 256, MatrixWidth = 8, MatrixHeight = 4 });
            Resolutions.Add("4", new Resolution() { Id = "4", ScaleDenominator = 3.69786694318207E7, UnitsPerPixel = 8.7890625e-2, Top = 90, Left = -180, TileWidth = 256, TileHeight = 256, MatrixWidth = 16, MatrixHeight = 8 });
            Resolutions.Add("5", new Resolution() { Id = "5", ScaleDenominator = 1.848933471591035E7, UnitsPerPixel = 4.39453125e-2, Top = 90, Left = -180, TileWidth = 256, TileHeight = 256, MatrixWidth = 32, MatrixHeight = 16 });
            Resolutions.Add("6", new Resolution() { Id = "6", ScaleDenominator = 9244667.357955175, UnitsPerPixel = 2.197265625e-2, Top = 90, Left = -180, TileWidth = 256, TileHeight = 256, MatrixWidth = 64, MatrixHeight = 32 });
            Resolutions.Add("7", new Resolution() { Id = "7", ScaleDenominator = 4622333.678977588, UnitsPerPixel = 1.0986328125e-2, Top = 90, Left = -180, TileWidth = 256, TileHeight = 256, MatrixWidth = 128, MatrixHeight = 64 });
            Resolutions.Add("8", new Resolution() { Id = "8", ScaleDenominator = 2311166.839488794, UnitsPerPixel = 5.4931646625e-3, Top = 90, Left = -180, TileWidth = 256, TileHeight = 256, MatrixWidth = 256, MatrixHeight = 128 });
            Resolutions.Add("9", new Resolution() { Id = "9", ScaleDenominator = 1155583.419744397, UnitsPerPixel = 2.74658203125e-3, Top = 90, Left = -180, TileWidth = 256, TileHeight = 256, MatrixWidth = 512, MatrixHeight = 256 });
            Resolutions.Add("10", new Resolution() { Id = "10", ScaleDenominator = 577791.7098721985, UnitsPerPixel = 1.373291015625e-3, Top = 90, Left = -180, TileWidth = 256, TileHeight = 256, MatrixWidth = 1024, MatrixHeight = 512 });
            Resolutions.Add("11", new Resolution() { Id = "11", ScaleDenominator = 288895.85493609926, UnitsPerPixel = 6.866455078125e-4, Top = 90, Left = -180, TileWidth = 256, TileHeight = 256, MatrixWidth = 2048, MatrixHeight = 1024 });
            Resolutions.Add("12", new Resolution() { Id = "12", ScaleDenominator = 144447.92746804963, UnitsPerPixel = 3.4332275390625e-4, Top = 90, Left = -180, TileWidth = 256, TileHeight = 256, MatrixWidth = 4096, MatrixHeight = 2048 });
            Resolutions.Add("13", new Resolution() { Id = "13", ScaleDenominator = 72223.96373402482, UnitsPerPixel = 1.71661376953125e-4, Top = 90, Left = -180, TileWidth = 256, TileHeight = 256, MatrixWidth = 8192, MatrixHeight = 4096 });
            Resolutions.Add("14", new Resolution() { Id = "14", ScaleDenominator = 36111.98186701241, UnitsPerPixel = 8.58306884765625e-5, Top = 90, Left = -180, TileWidth = 256, TileHeight = 256, MatrixWidth = 16384, MatrixHeight = 8192 });
            Resolutions.Add("15", new Resolution() { Id = "15", ScaleDenominator = 18055.990933506204, UnitsPerPixel = 4.29153442382812e-5, Top = 90, Left = -180, TileWidth = 256, TileHeight = 256, MatrixWidth = 32768, MatrixHeight = 16384 });
            Resolutions.Add("16", new Resolution() { Id = "16", ScaleDenominator = 9027.995466753102, UnitsPerPixel = 2.14576721191406e-5, Top = 90, Left = -180, TileWidth = 256, TileHeight = 256, MatrixWidth = 65536, MatrixHeight = 32768 });
            Resolutions.Add("17", new Resolution() { Id = "17", ScaleDenominator = 4513.997733376551, UnitsPerPixel = 1.07288360595703e-5, Top = 90, Left = -180, TileWidth = 256, TileHeight = 256, MatrixWidth = 131072, MatrixHeight = 65536 });
            Resolutions.Add("18", new Resolution() { Id = "18", ScaleDenominator = 2256.998866688275, UnitsPerPixel = 5.36441802978516e-6, Top = 90, Left = -180, TileWidth = 256, TileHeight = 256, MatrixWidth = 262144, MatrixHeight = 131072 });
            //Resolutions.Add("19", new Resolution() { Id = "19", ScaleDenominator = 1128.4994333441375, UnitsPerPixel = 2.68220901489258e-6, Top = 90, Left = -180, TileWidth = 256, TileHeight = 256, MatrixWidth = 524288, MatrixHeight = 262144 });
        }

        /// <summary>
        /// 获取底色
        /// </summary>
        public static Color GetBackColor(string level)
        {
            if(level == "11" || level == "12" || level == "14")
            {
                return Color.FromArgb(245, 244, 237);
            }
            else if (level == "13")
            {
                return Color.FromArgb(244, 244, 238);
            }            
            else
            {
                return Color.FromArgb(244, 244, 237);
            }
        }
    }

    /// <summary>
    /// 天地图球面墨卡托瓦片Provider
    /// </summary>
    public class TiandituMecatorTileProvider : TiandituTileProvider
    {
        #region URL

        private const string VEC_MAPURL = "http://{0}.tianditu.com/vec_w/wmts?request=GetTile&service=wmts&version=1.0.0&Layer=vec&Style=default&TileMatrixSet=c";
        private const string VEC_ANOURL = "http://{0}.tianditu.com/cva_w/wmts?request=GetTile&service=wmts&version=1.0.0&Layer=cva&Style=default&TileMatrixSet=c";
        private const string VEC_EANOURL = "http://{0}.tianditu.com/eva_w/wmts?request=GetTile&service=wmts&version=1.0.0&Layer=cva&Style=default&TileMatrixSet=c";
        private const string IMG_MAPURL = "http://{0}.tianditu.com/img_w/wmts?request=GetTile&service=wmts&version=1.0.0&Layer=img&Style=default&TileMatrixSet=c";
        private const string IMG_ANOURL = "http://{0}.tianditu.com/cia_w/wmts?request=GetTile&service=wmts&version=1.0.0&Layer=cia&Style=default&TileMatrixSet=c";
        private const string IMG_EANOURL = "http://{0}.tianditu.com/eia_w/wmts?request=GetTile&service=wmts&version=1.0.0&Layer=cia&Style=default&TileMatrixSet=c";
        private const string TER_MAPURL = "http://{0}.tianditu.com/ter_w/wmts?request=GetTile&service=wmts&version=1.0.0&Layer=ter&Style=default&TileMatrixSet=c";
        private const string TER_ANOURL = "http://{0}.tianditu.com/cta_w/wmts?request=GetTile&service=wmts&version=1.0.0&Layer=cta&Style=default&TileMatrixSet=c";
        private const string TER_EANOURL = "http://{0}.tianditu.com/eta_w/wmts?request=GetTile&service=wmts&version=1.0.0&Layer=cta&Style=default&TileMatrixSet=c";

        #endregion

        public TiandituMecatorTileProvider(TiandituTileType type = TiandituTileType.VecBack,
            TiandituTileServiceChanel chanel = TiandituTileServiceChanel.t0, 
            ITileCache<byte[]> tileCache = null)
            : base(type)
        {
        }

        protected override void SetUrl(TiandituTileType type)
        {
            switch (type)
            {
                case TiandituTileType.VecBack:
                    {
                        _url = string.Format(VEC_MAPURL, _chanel);
                        break;
                    }
                case TiandituTileType.VecAno:
                    {
                        _url = string.Format(VEC_ANOURL, _chanel);
                        break;
                    }
                case TiandituTileType.VecEano:
                    {
                        _url = string.Format(VEC_EANOURL, _chanel);
                        break;
                    }
                case TiandituTileType.ImgBack:
                    {
                        _url = string.Format(IMG_MAPURL, _chanel);
                        break;
                    }
                case TiandituTileType.ImgAno:
                    {
                        _url = string.Format(IMG_ANOURL, _chanel);
                        break;
                    }
                case TiandituTileType.ImgEano:
                    {
                        _url = string.Format(IMG_EANOURL, _chanel);
                        break;
                    }
                case TiandituTileType.TerBack:
                    {
                        _url = string.Format(TER_MAPURL, _chanel);
                        break;
                    }
                case TiandituTileType.TerAno:
                    {
                        _url = string.Format(TER_ANOURL, _chanel);
                        break;
                    }
                case TiandituTileType.TerEano:
                    {
                        _url = string.Format(TER_EANOURL, _chanel);
                        break;
                    }
            }
        }

        /// <summary>
        /// 获取改变服务通道的新Provider实例
        /// </summary>
        /// <param name="chanel"></param>
        /// <returns></returns>
        public override TiandituTileProvider GetChangeServiceChanelInstance(TiandituTileServiceChanel chanel)
        {
            return new TiandituMecatorTileProvider(_type, chanel, _tileCache);
        }
    }

    /// <summary>
    /// 天地图球面墨卡托瓦片规则
    /// </summary>
    public class TiandituMecatorTileSchema : TileSchema
    {
        public TiandituMecatorTileSchema()
        {
            Height = 256;
            Width = 256;
            Name = "天地图球面墨卡托瓦片";
            Extent = new Envelope(-20037508.3427892, -20037508.3427892, 20037508.3427892, 20037508.3427892);
            Format = "png";
            OriginX = -20037508.3427892;
            OriginY = 20037508.3427892;
            Axis = AxisDirection.InvertedY;
            Srs = "EPSG:3857";

            //Resolutions.Add("1", new Resolution() { Id = "1", ScaleDenominator = 2.958293554545656E8, UnitsPerPixel = 2.958293554545656e8, Left = -20037508.3427892, Top = 20037508.3427892, TileWidth = 256, TileHeight = 256, MatrixWidth = 2, MatrixHeight = 2 });
            Resolutions.Add("2", new Resolution() { Id = "2", ScaleDenominator = 1.479146777272828E8, UnitsPerPixel = 1.479146777272828e8, Left = -20037508.3427892, Top = 20037508.3427892, TileWidth = 256, TileHeight = 256, MatrixWidth = 4, MatrixHeight = 4 });
            Resolutions.Add("3", new Resolution() { Id = "3", ScaleDenominator = 7.39573388636414E7, UnitsPerPixel = 7.3957338636414e7, Left = -20037508.3427892, Top = 20037508.3427892, TileWidth = 256, TileHeight = 256, MatrixWidth = 8, MatrixHeight = 8 });
            Resolutions.Add("4", new Resolution() { Id = "4", ScaleDenominator = 3.69786694318207E7, UnitsPerPixel = 3.69786694318207e7, Left = -20037508.3427892, Top = 20037508.3427892, TileWidth = 256, TileHeight = 256, MatrixWidth = 16, MatrixHeight = 16 });
            Resolutions.Add("5", new Resolution() { Id = "5", ScaleDenominator = 1.848933471591035E7, UnitsPerPixel = 1.848933471591035e7, Left = -20037508.3427892, Top = 20037508.3427892, TileWidth = 256, TileHeight = 256, MatrixWidth = 32, MatrixHeight = 32 });
            Resolutions.Add("6", new Resolution() { Id = "6", ScaleDenominator = 9244667.357955175, UnitsPerPixel = 9244667.357955175, Left = -20037508.3427892, Top = 20037508.3427892, TileWidth = 256, TileHeight = 256, MatrixWidth = 64, MatrixHeight = 64 });
            Resolutions.Add("7", new Resolution() { Id = "7", ScaleDenominator = 4622333.678977588, UnitsPerPixel = 4622333.678977588, Left = -20037508.3427892, Top = 20037508.3427892, TileWidth = 256, TileHeight = 256, MatrixWidth = 128, MatrixHeight = 128 });
            Resolutions.Add("8", new Resolution() { Id = "8", ScaleDenominator = 2311166.839488794, UnitsPerPixel = 2311166.839488794, Left = -20037508.3427892, Top = 20037508.3427892, TileWidth = 256, TileHeight = 256, MatrixWidth = 256, MatrixHeight = 256 });
            Resolutions.Add("9", new Resolution() { Id = "9", ScaleDenominator = 1155583.419744397, UnitsPerPixel = 1155583.419744397, Left = -20037508.3427892, Top = 20037508.3427892, TileWidth = 256, TileHeight = 256, MatrixWidth = 512, MatrixHeight = 512 });
            Resolutions.Add("10", new Resolution() { Id = "10", ScaleDenominator = 577791.7098721985, UnitsPerPixel = 577791.7098721985, Left = -20037508.3427892, Top = 20037508.3427892, TileWidth = 256, TileHeight = 256, MatrixWidth = 1024, MatrixHeight = 1024 });
            Resolutions.Add("11", new Resolution() { Id = "11", ScaleDenominator = 288895.85493609926, UnitsPerPixel = 288895.85493609926, Left = -20037508.3427892, Top = 20037508.3427892, TileWidth = 256, TileHeight = 256, MatrixWidth = 2048, MatrixHeight = 2048 });
            Resolutions.Add("12", new Resolution() { Id = "12", ScaleDenominator = 144447.92746804963, UnitsPerPixel = 144447.92746804963, Left = -20037508.3427892, Top = 20037508.3427892, TileWidth = 256, TileHeight = 256, MatrixWidth = 4096, MatrixHeight = 4096 });
            Resolutions.Add("13", new Resolution() { Id = "13", ScaleDenominator = 72223.96373402482, UnitsPerPixel = 72223.96373402482, Left = -20037508.3427892, Top = 20037508.3427892, TileWidth = 256, TileHeight = 256, MatrixWidth = 8192, MatrixHeight = 8192 });
            Resolutions.Add("14", new Resolution() { Id = "14", ScaleDenominator = 36111.98186701241, UnitsPerPixel = 36111.98186701241, Left = -20037508.3427892, Top = 20037508.3427892, TileWidth = 256, TileHeight = 256, MatrixWidth = 16384, MatrixHeight = 16384 });
            Resolutions.Add("15", new Resolution() { Id = "15", ScaleDenominator = 18055.990933506204, UnitsPerPixel = 18055.990933506204, Left = -20037508.3427892, Top = 20037508.3427892, TileWidth = 256, TileHeight = 256, MatrixWidth = 32768, MatrixHeight = 32768 });
            Resolutions.Add("16", new Resolution() { Id = "16", ScaleDenominator = 9027.995466753102, UnitsPerPixel = 9027.995466753102, Left = -20037508.3427892, Top = 20037508.3427892, TileWidth = 256, TileHeight = 256, MatrixWidth = 65536, MatrixHeight = 65536 });
            Resolutions.Add("17", new Resolution() { Id = "17", ScaleDenominator = 4513.997733376551, UnitsPerPixel = 4513.997733376551, Left = -20037508.3427892, Top = 20037508.3427892, TileWidth = 256, TileHeight = 256, MatrixWidth = 131072, MatrixHeight = 131072 });
            Resolutions.Add("18", new Resolution() { Id = "18", ScaleDenominator = 2256.998866688275, UnitsPerPixel = 2256.998866688275, Left = -20037508.3427892, Top = 20037508.3427892, TileWidth = 256, TileHeight = 256, MatrixWidth = 262144, MatrixHeight = 262144 });
            //Resolutions.Add("19", new Resolution() { Id = "19", ScaleDenominator = 1128.4994333441375, UnitsPerPixel = 1128.4994333441375, Left = -20037508.3427892, Top = 20037508.3427892, TileWidth = 256, TileHeight = 256, MatrixWidth = 524288, MatrixHeight = 524288 });
        }
    }

    /// <summary>
    /// 通过天地图官网网页分析得到的天地图经纬度瓦片Provider
    /// </summary>
    public class OfficialTiandituTileProvider : TiandituTileProvider
    {
        #region URL

        private const string VEC_MAPURL = "http://{0}.tianditu.com/DataServer?T=vec_c";
        private const string VEC_ANOURL = "http://{0}.tianditu.com/DataServer?T=cva_c";
        private const string VEC_EANOURL = "http://{0}.tianditu.com/DataServer?T=eva_c";
        private const string IMG_MAPURL = "http://{0}.tianditu.com/DataServer?T=img_c";
        private const string IMG_ANOURL = "http://{0}.tianditu.com/DataServer?T=cia_c";
        private const string IMG_EANOURL = "http://{0}.tianditu.com/DataServer?T=eia_c";
        private const string TER_MAPURL = "http://{0}.tianditu.com/DataServer?T=ter_c";
        private const string TER_ANOURL = "http://{0}.tianditu.com/DataServer?T=cta_c";
        private const string TER_EANOURL = "http://{0}.tianditu.com/DataServer?T=eta_c";

        #endregion

        public OfficialTiandituTileProvider(TiandituTileType type = TiandituTileType.VecBack,
            TiandituTileServiceChanel chanel = TiandituTileServiceChanel.t0,
            ITileCache<byte[]> tileCache = null)
            : base(type, chanel, tileCache)
        {
        }

        protected override void SetUrl(TiandituTileType type)
        {
            switch (type)
            {
                case TiandituTileType.VecBack:
                    {
                        _url = string.Format(VEC_MAPURL, _chanel);                        
                        break;
                    }
                case TiandituTileType.VecAno:
                    {
                        _url = string.Format(VEC_ANOURL, _chanel);
                        break;
                    }
                case TiandituTileType.VecEano:
                    {
                        _url = string.Format(VEC_EANOURL, _chanel);
                        break;
                    }
                case TiandituTileType.ImgBack:
                    {
                        _url = string.Format(IMG_MAPURL, _chanel);
                        break;
                    }
                case TiandituTileType.ImgAno:
                    {
                        _url = string.Format(IMG_ANOURL, _chanel);
                        break;
                    }
                case TiandituTileType.ImgEano:
                    {
                        _url = string.Format(IMG_EANOURL, _chanel);
                        break;
                    }
                case TiandituTileType.TerBack:
                    {
                        _url = string.Format(TER_MAPURL, _chanel);
                        break;
                    }
                case TiandituTileType.TerAno:
                    {
                        _url = string.Format(TER_ANOURL, _chanel);
                        break;
                    }
                case TiandituTileType.TerEano:
                    {
                        _url = string.Format(TER_EANOURL, _chanel);
                        break;
                    }
            }
        }

        protected override string BuildUrl(int row, int col, string level)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("&x=").Append(col);
            builder.Append("&y=").Append(row);
            builder.Append("&l=").Append(level);
            return builder.ToString();
        }

        /// <summary>
        /// 获取改变服务通道的新Provider实例
        /// </summary>
        /// <param name="chanel"></param>
        /// <returns></returns>
        public override TiandituTileProvider GetChangeServiceChanelInstance(TiandituTileServiceChanel chanel)
        {
            return new OfficialTiandituTileProvider(_type, chanel, _tileCache);
        }
    }

    /// <summary>
    /// 通过天地图官网网页分析得到的天地图球面墨卡托瓦片Provider
    /// </summary>
    public class OfficialTiandituMecatorTileProvider : OfficialTiandituTileProvider
    {
        #region URL

        private const string VEC_MAPURL = "http://{0}.tianditu.com/DataServer?T=vec_w";
        private const string VEC_ANOURL = "http://{0}.tianditu.com/DataServer?T=cva_w";
        private const string VEC_EANOURL = "http://{0}.tianditu.com/DataServer?T=eva_w";
        private const string IMG_MAPURL = "http://{0}.tianditu.com/DataServer?T=img_w";
        private const string IMG_ANOURL = "http://{0}.tianditu.com/DataServer?T=cia_w";
        private const string IMG_EANOURL = "http://{0}.tianditu.com/DataServer?T=eia_w";
        private const string TER_MAPURL = "http://{0}.tianditu.com/DataServer?T=ter_w";
        private const string TER_ANOURL = "http://{0}.tianditu.com/DataServer?T=cta_w";
        private const string TER_EANOURL = "http://{0}.tianditu.com/DataServer?T=eta_w";

        #endregion

        public OfficialTiandituMecatorTileProvider(TiandituTileType type = TiandituTileType.VecBack,
            TiandituTileServiceChanel chanel = TiandituTileServiceChanel.t0, 
            ITileCache<byte[]> tileCache = null)
            : base(type)
        {
        }

        protected override void SetUrl(TiandituTileType type)
        {
            switch (type)
            {
                case TiandituTileType.VecBack:
                    {
                        _url = string.Format(VEC_MAPURL, _chanel);
                        break;
                    }
                case TiandituTileType.VecAno:
                    {
                        _url = string.Format(VEC_ANOURL, _chanel);
                        break;
                    }
                case TiandituTileType.VecEano:
                    {
                        _url = string.Format(VEC_EANOURL, _chanel);
                        break;
                    }
                case TiandituTileType.ImgBack:
                    {
                        _url = string.Format(IMG_MAPURL, _chanel);
                        break;
                    }
                case TiandituTileType.ImgAno:
                    {
                        _url = string.Format(IMG_ANOURL, _chanel);
                        break;
                    }
                case TiandituTileType.ImgEano:
                    {
                        _url = string.Format(IMG_EANOURL, _chanel);
                        break;
                    }
                case TiandituTileType.TerBack:
                    {
                        _url = string.Format(TER_MAPURL, _chanel);
                        break;
                    }
                case TiandituTileType.TerAno:
                    {
                        _url = string.Format(TER_ANOURL, _chanel);
                        break;
                    }
                case TiandituTileType.TerEano:
                    {
                        _url = string.Format(TER_EANOURL, _chanel);
                        break;
                    }
            }
        }

        /// <summary>
        /// 获取改变服务通道的新Provider实例
        /// </summary>
        /// <param name="chanel"></param>
        /// <returns></returns>
        public override TiandituTileProvider GetChangeServiceChanelInstance(TiandituTileServiceChanel chanel)
        {
            return new OfficialTiandituMecatorTileProvider(_type, chanel, _tileCache);
        }
    }
}
