﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Data;
using NetMap.Geometry;
using NetMap.Interfaces.Element;
using NetMap.Interfaces.Geometry;

namespace NetMap.Element
{
    public class Element : IElement
    {
        public virtual IGeometry Geometry
        {
            get;
            set;
        }

        public virtual geoElementType ElementType
        {
            get { return geoElementType.GEO_ELEMENT_SIMPLE; }
        }
    }

    public class TextElement : Element, ITextElement
    {
        private IDirectionPoint _point;

        public override IGeometry Geometry
        {
            get
            {
                return _point;
            }
            set
            {
                if (value.GeometryType == geoGeometryType.GEO_GEOMETRY_POINT)
                {
                    if (value is IDirectionPoint)
                    {
                        _point = value as IDirectionPoint;
                    }
                    else
                    {
                        IPoint point = value as IPoint;
                        _point = new DirectionPoint(point.X, point.Y, 0);
                    }
                }
                else
                {
                    throw new InvalidCastException("图形类型转换失败");
                }
            }
        }

        public IDirectionPoint Point
        {
            get
            {
                return _point;
            }
            set
            {
                _point = value;
            }
        }

        public string Text
        {
            get;
            set;
        }

        public override geoElementType ElementType
        {
            get
            {
                return geoElementType.GEO_ELEMENT_TEXT;
            }
        }
    }

    public class RasterElement : Element, IRasterElement
    {
        public override IGeometry Geometry
        {
            get
            {
                return Image.Bounds as IGeometry;
            }
            set
            {
                if (value.GeometryType == geoGeometryType.GEO_GEOMETRY_RECT)
                {
                    Image.Bounds = value as IEnvelope;
                }
                else
                {
                    throw new InvalidCastException("图形类型转换失败");
                }
            }
        }

        public SpaceImage Image
        {
            get;
            set;
        }

        public override geoElementType ElementType
        {
            get { return geoElementType.GEO_ELEMENT_RASTER; }
        }
    }
}
