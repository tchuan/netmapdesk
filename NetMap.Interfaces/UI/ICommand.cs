﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace NetMap.Interfaces
{
    /// <summary>
    /// 命令 直接执行操作
    /// </summary>
    public interface ICommand
    {
        /// <summary>
        /// 名字
        /// </summary>
        string Name { get; }
        /// <summary>
        /// 组名
        /// </summary>
        string GroupName { get; }
        /// <summary>
        /// 描述信息
        /// </summary>
        string Description { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
        Image Icon { get; }
        /// <summary>
        /// 操作是否可用
        /// </summary>
        bool IsEnable { get; }
        /// <summary>
        /// 初始化
        /// </summary>
        void Init();
        /// <summary>
        /// 执行
        /// </summary>
        void Execute();
        /// <summary>
        /// 获取和设置地图视图
        /// </summary>
        /// <returns></returns>
        IMapView View { get; set; }
        /// <summary>
        /// 用户数据
        /// </summary>
        byte[] UserData { get; set; }             
    }
}
