﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Fetcher;
using NetMap.Interfaces;
using NetMap.Interfaces.Geometry;
using System.Drawing;
using System.ComponentModel;
using System.Threading;
using System.Diagnostics;

//控制地图控件的绘制

namespace NetMap.UI
{
    partial class MapCtrl
    {
        //[异步展示]
        private readonly object _invalidationLock = new object();
        private DateTime _lastInvalidation = DateTime.Now;
        private BackgroundWorker _invalidator = null;
        private AutoResetEvent _renderEvent = new AutoResetEvent(false);
        private bool _holdInvalidation = false; //阻止立即刷新

        //[绘图控制]
        private bool _bRefreshMap = false;   //是否刷新地图

        private BackgroundWorker InitRender()
        {
            _invalidator = new BackgroundWorker();
            _invalidator.WorkerSupportsCancellation = true;
            _invalidator.WorkerReportsProgress = true;
            _invalidator.DoWork += new DoWorkEventHandler(invalidatorWatch);
            _invalidator.RunWorkerAsync();
            return _invalidator;
        }        

        //刷新监视
        private void invalidatorWatch(object sender, DoWorkEventArgs e)
        {
            var w = sender as BackgroundWorker;

            TimeSpan span = TimeSpan.FromMilliseconds(111);
            int spanMs = (int)span.TotalMilliseconds;
            bool skiped = false;
            TimeSpan delta;
            DateTime now = DateTime.Now;

            while(_renderEvent != null
                && (!skiped && _renderEvent.WaitOne() || (_renderEvent.WaitOne(spanMs, false) || true)))
            {
                if(w.CancellationPending)
                    break;

                now = DateTime.Now;
                lock(_invalidationLock)
                {
                    delta = now - _lastInvalidation;
                }

                if(delta > span)
                {
                    lock(_invalidationLock)
                    {
                        _lastInvalidation = now;
                    }
                    skiped = false;

                    w.ReportProgress(1);
                    Debug.WriteLine("刷新间隔: " + (int)delta.TotalMilliseconds + "ms");
                }
                else
                {
                    skiped = true;
                }
            }
        }

        private void invalidatorEngage(object sender, ProgressChangedEventArgs e)
        {
            base.Invalidate();
        }

        private void BindMapDataChange(IMapDocument map)
        {
            if (map == null)
            {
                return;
            }
            IMapDocumentEvent mapevent = map as IMapDocumentEvent;
            mapevent.DataChanged += DataChangeRefresh;
        }

        private void UnbindMapDataChange(IMapDocument map)
        {
            if (map == null)
            {
                return;
            }
            IMapDocumentEvent mapevent = map as IMapDocumentEvent;
            mapevent.DataChanged -= DataChangeRefresh;
        }

        //数据变化引起的更新
        private void DataChangeRefresh(IEnvelope box)
        {
            _bRefreshMap = true;
            Invalidate();
        }

        public new void Invalidate()
        {
            if (_renderEvent != null)
            {
                _renderEvent.Set();
            }
        }

        /// <summary>
        /// 调用这个来停止HoldInvalidation，执行单次强制刷新
        /// </summary>
        public override void Refresh()
        {
            _holdInvalidation = false;
            lock (_invalidationLock)
            {
                _lastInvalidation = DateTime.Now;
            }
            base.Refresh();
        }        
    }
}
