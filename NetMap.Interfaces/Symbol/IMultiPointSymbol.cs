﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetMap.Interfaces.Symbol
{
    /// <summary>
    /// 复合点符号接口
    /// </summary>
    public interface IMultiPointSymbol : IPointSymbol, IList<IPointSymbol>
    {
    }
}
