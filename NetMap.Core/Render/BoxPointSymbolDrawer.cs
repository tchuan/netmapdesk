﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using NetMap.Interfaces.Geometry;
using NetMap.Interfaces.Render;
using NetMap.Interfaces.Symbol;

namespace NetMap.Render
{
    /// <summary>
    /// Box点符号绘制
    /// </summary>
    internal class BoxPointSymbolDrawer : ISymbolDrawer
    {
        public void Draw(IDisplay display, 
            ISymbol symbol,
            IGeometry geometry, 
            string text)
        {
            if (geometry is IPoint)
            {
                TrueDraw(display, symbol as IBoxPointSymbol, geometry as IPoint);
            }
            else if (geometry is IMultiPoint)
            {
                IMultiPoint points = geometry as IMultiPoint;
                foreach (IPoint point in points)
                {
                    TrueDraw(display, symbol as IBoxPointSymbol, point as IPoint);
                }
            }
        }

        //具体绘制
        private void TrueDraw(IDisplay display,
            IBoxPointSymbol symbol,
            IPoint point)
        {
            double x = 0, y = 0;
            display.DisplayTransformation.FromMapPoint(point.X, point.Y, ref x, ref y);
            if (symbol.Fill)
            {
                System.Drawing.Brush brush = new SolidBrush(symbol.FillColor);
                display.G.FillRectangle(brush, (float)(x - 0.5 * symbol.Box.Width),
                    (float)(y - 0.5 * symbol.Box.Height), (float)symbol.Box.Width, (float)symbol.Box.Height);
            }
            if (symbol.Outline)
            {
                System.Drawing.Pen pen = new Pen(symbol.Color, symbol.Width);
                display.G.DrawRectangle(pen, (float)(x - 0.5 * symbol.Box.Width),
                    (float)(y - 0.5 * symbol.Box.Height), (float)symbol.Box.Width, (float)symbol.Box.Height);
            }
        }

        public void Draw(Graphics g, ISymbol symbol, IGeometry geometry, string text)
        {
            switch (geometry.GeometryType)
            {
                case geoGeometryType.GEO_GEOMETRY_POINT:
                    {
                        DirectlyDraw(g, symbol as IBoxPointSymbol, geometry as IPoint);
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_LINESTRING:
                    {                        
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_POLYGON:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTIPOINT:
                    {
                        IMultiPoint mp = geometry as IMultiPoint;
                        foreach (IPoint point in mp)
                        {
                            DirectlyDraw(g, symbol as IBoxPointSymbol, point);
                        }
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTILINESTRING:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_MULTIPOLYGON:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_PATH:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_RECT:
                    {
                        break;
                    }
                case geoGeometryType.GEO_GEOMETRY_DIRECTIONPOINT:
                    {
                        break;
                    }
            }
        }

        //具体绘制
        private void DirectlyDraw(Graphics g, IBoxPointSymbol symbol, IPoint point)
        {
            if (symbol.Fill)
            {
                System.Drawing.Brush brush = new SolidBrush(symbol.FillColor);
                g.FillRectangle(brush, (float)(point.X - 0.5 * symbol.Box.Width),
                    (float)(point.Y - 0.5 * symbol.Box.Height), (float)symbol.Box.Width, (float)symbol.Box.Height);
            }
            if (symbol.Outline)
            {
                System.Drawing.Pen pen = new Pen(symbol.Color, symbol.Width);
                g.DrawRectangle(pen, (float)(point.X - 0.5 * symbol.Box.Width),
                    (float)(point.Y - 0.5 * symbol.Box.Height), (float)symbol.Box.Width, (float)symbol.Box.Height);
            }
        }
    }
}
