﻿// Copyright (c) BruTile developers team. All rights reserved. See License.txt in the project root for license information.

using NetMap.Interfaces.Tile;
using NetMap.Tile;
using System;
using System.Net;

namespace NetMap.Tile.Wellknown
{
    public class BingTileSource : TileSource
    {
        public BingTileSource(String url, string token, BingMapType mapType)
            : this(new BingRequest(url, token, mapType))
        {
        }

        public BingTileSource(
                        BingRequest bingRequest, 
                        ITileCache<byte[]> tileCache = null)
            : base(new WebTileProvider(bingRequest, tileCache), new GlobalSphericalMercator("jpg", true, 1, 19, "Bing"))
        {
        }
    }
}
